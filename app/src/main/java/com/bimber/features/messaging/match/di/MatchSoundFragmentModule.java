package com.bimber.features.messaging.match.di;

import com.bimber.features.messaging.match.MatchSoundFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class MatchSoundFragmentModule {

    @ContributesAndroidInjector
    abstract MatchSoundFragment contribute();
}
