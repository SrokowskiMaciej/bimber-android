package com.bimber.features.messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.NotificationCompat;

import com.bimber.R;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.github.kevelbreh.androidunits.AndroidUnit;
import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maciek on 19.04.17.
 */
@AutoValue
public abstract class ChatNotification {

    public static final int GROUP_NOTIFICATION_MODIFIER = 1;

    public static final int ICON_SIZE = (int) AndroidUnit.DENSITY_PIXELS.toPixels(64);


    public abstract NotificationType notificationType();

    public abstract String chatId();

    public abstract ChatType chatType();

    public abstract String chatTitle();

    public int notificationId() {
        return notificationType().getNotificationId(chatId());
    }

    public abstract Notification notification();

    public static ChatNotification create(NotificationType notificationType, String chatId, ChatType chatType, String chatTitle,
                                          Notification notification) {
        return new AutoValue_ChatNotification(notificationType, chatId, chatType, chatTitle, notification);
    }

    public void showNotification(Context context) {
        //TODO WTF? Why is it not working on API 16?
        //NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        showBundledGroupNotification(context, notificationManager);
        notificationManager.notify(chatId(), notificationId(), notification());
    }

    private void showBundledGroupNotification(Context context, NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
            Intent chatIntent = ChatActivity.newInstance(context, chatId(), chatType());
            PendingIntent broadcast = TaskStackBuilder.create(context)
                    .addNextIntent(mainScreenIntent)
                    .addNextIntent(chatIntent)
                    .getPendingIntent(chatId().hashCode(), PendingIntent.FLAG_UPDATE_CURRENT);

            Notification groupNotification = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_bimber_notification)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setGroupSummary(true)
                    .setAutoCancel(true)
                    .setGroup(chatId())
                    .setContentIntent(broadcast)
                    .setContentInfo(chatTitle())
                    .setStyle(new android.support.v4.app.NotificationCompat.BigTextStyle().setSummaryText(chatTitle()))
                    .build();
            notificationManager.notify(chatId(), NotificationType.GROUPED_NOTIFICATION.getNotificationId(chatId()), groupNotification);
        }
    }

    public static void cancelNotification(Context context, NotificationType notificationType, String chatId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(chatId, notificationType.getNotificationId(chatId));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            List<StatusBarNotification> nonGroupNotificationsFromTheSameChat = new ArrayList<>();
            for (StatusBarNotification statusBarNotification : notificationManager.getActiveNotifications()) {
                if (statusBarNotification.getTag() == null || !statusBarNotification.getTag().equals(chatId)) {
                    continue;
                }
                NotificationType notificationType1 = NotificationType.getNotificationType(chatId, statusBarNotification.getId());
                if (notificationType1 == NotificationType.GROUPED_NOTIFICATION || notificationType1 == NotificationType.NONE) {
                    continue;
                }
                nonGroupNotificationsFromTheSameChat.add(statusBarNotification);

            }
            if (nonGroupNotificationsFromTheSameChat.isEmpty()) {
                notificationManager.cancel(chatId, NotificationType.GROUPED_NOTIFICATION.getNotificationId(chatId));
            }
        }

    }


}
