package com.bimber.features.messaging.match;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.MatchPushMessage;
import com.bimber.features.chat.NotificationSoundManager;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 02.06.17.
 */

public class MatchSnackbarFragment extends BaseFragment {

    public static final String HANDLING_PRIORITY = "HANDLING_PRIORITY";

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    NotificationSoundManager notificationSoundManager;

    private MatchBroadcastReceiver matchBroadcastReceiver;
    private ViewGroup container;
    private final CompositeDisposable subscribtions = new CompositeDisposable();

    public static MatchSnackbarFragment newInstance(HandlingPriority handlingPriority) {
        MatchSnackbarFragment matchSnackbarFragment = new MatchSnackbarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HANDLING_PRIORITY, handlingPriority.toString());
        matchSnackbarFragment.setArguments(bundle);
        return matchSnackbarFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.container = container;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        matchBroadcastReceiver = new MatchBroadcastReceiver();
        subscribtions.add(matchBroadcastReceiver.onMatchPushMessage()
                .filter(matchPushMessage -> matchPushMessage.likedUserId().equals(currentUserId))
                .subscribe(matchPushMessage -> {
                    String matchName = matchPushMessage.likingUserName() != null ? matchPushMessage.likingUserName()
                            : defaultValues.defaultUser(matchPushMessage.likingUserId()).displayName;
                    Snackbar.make(container, String.format(getString(R.string
                                    .match_message_snackbar_text), matchName),
                            Snackbar.LENGTH_LONG).show();
                    notificationSoundManager.play();
                }, throwable -> Timber.e(throwable, "Fatal error while showing match snackbar. No more snackbars will be shown " +
                        "from this stream")));


        HandlingPriority handlingPriority = HandlingPriority.valueOf(getArguments().getString(HANDLING_PRIORITY));
        IntentFilter matchIntentFilter = new IntentFilter(MatchCloudMessageDataHandler.MATCH_INTENT_ACTION);
        matchIntentFilter.setPriority(handlingPriority.order);
        getActivity().registerReceiver(matchBroadcastReceiver, matchIntentFilter);
    }


    @Override
    public void onStop() {
        getActivity().unregisterReceiver(matchBroadcastReceiver);
        subscribtions.clear();
        super.onStop();
    }

    private class MatchBroadcastReceiver extends BroadcastReceiver {

        private PublishSubject<MatchPushMessage> matchPushMessagePublishSubject = PublishSubject.create();

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("Match content broadcast received in GroupParticipationSnackbarFragment. Showing snackbar.");
            MatchPushMessage matchPushMessage = intent.getParcelableExtra(MatchCloudMessageDataHandler.MATCH_DATA_KEY);
            matchPushMessagePublishSubject.onNext(matchPushMessage);
            abortBroadcast();
        }

        public Observable<MatchPushMessage> onMatchPushMessage() {
            return matchPushMessagePublishSubject;
        }
    }
}
