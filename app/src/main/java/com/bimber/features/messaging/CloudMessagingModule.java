package com.bimber.features.messaging;

import com.bimber.features.messaging.groupmeeting.GroupMeetingCloudMessageDataHandler;
import com.bimber.features.messaging.groupparticipation.GroupParticipationCloudMessageDataHandler;
import com.bimber.features.messaging.match.MatchCloudMessageDataHandler;
import com.bimber.features.messaging.newchatmessages.NewChatMessageCloudMessageDataHandler;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

/**
 * Created by maciek on 10.05.17.
 */
@Module
public class CloudMessagingModule {

    @Provides
    @IntoSet
    public static CloudMessageDataHandler groupParticipationMessageDataHandler() {
        return new GroupParticipationCloudMessageDataHandler();
    }

    @Provides
    @IntoSet
    public static CloudMessageDataHandler matchMessageDataHandler() {
        return new MatchCloudMessageDataHandler();
    }

    @Provides
    @IntoSet
    public static CloudMessageDataHandler newChatMessageMessageDataHandler() {
        return new NewChatMessageCloudMessageDataHandler();
    }

    @Provides
    @IntoSet
    public static CloudMessageDataHandler groupMeetingMessageDataHandler() {
        return new GroupMeetingCloudMessageDataHandler();
    }
}
