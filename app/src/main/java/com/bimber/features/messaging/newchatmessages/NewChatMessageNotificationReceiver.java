package com.bimber.features.messaging.newchatmessages;

import android.content.Context;
import android.content.Intent;

import com.bimber.base.auth.BaseLoggedInBroadcastReceiver;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.NewChatMessagePushMessage;
import com.bimber.features.analytics.notifications.ChatNotificationAnalytics;
import com.crashlytics.android.answers.Answers;
import com.google.common.base.Throwables;
import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import io.reactivex.Maybe;
import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */

public class NewChatMessageNotificationReceiver extends BaseLoggedInBroadcastReceiver {

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    NewChatMessageNotificationProvider newChatMessageNotificationProvider;
    @Inject
    FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onReceiveLoggedIn(Context context, Intent intent) {
        NewChatMessagePushMessage newChatMessage = intent.getParcelableExtra(NewChatMessageCloudMessageDataHandler.NEW_MESSAGE_DATA_KEY);
        Maybe.just(newChatMessage)
                .flatMap(chatMessage -> newChatMessageNotificationProvider.chatMessageNotification(context, newChatMessage, true))
                .subscribe(chatNotification -> {
                            chatNotification.showNotification(context);
                        },
                        throwable -> {
                            Timber.e(throwable, "Failed to show chat message notification");
                        },
                        () -> {
                            Timber.d("Chat message notification shouldn't be shown");
                        });
    }

    @Override
    public void onReceiveLoggedOut(Context context, Intent intent) {

    }
}
