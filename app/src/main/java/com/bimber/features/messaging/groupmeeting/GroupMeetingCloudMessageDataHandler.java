package com.bimber.features.messaging.groupmeeting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.entities.GroupMeetingPushMessage;
import com.bimber.data.entities.GroupParticipationPushMessage;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import timber.log.Timber;

/**
 * Created by maciek on 05.04.17.
 */

public class GroupMeetingCloudMessageDataHandler implements CloudMessageDataHandler {


    public static final String GROUP_MEETING_DATA_KEY = "group_meeting_notification_data_key";
    public static final String GROUP_MEETING_INTENT_ACTION = "GROUP_MEETING_INTENT_ACTION";

    @Override
    public String dataKey() {
        return GROUP_MEETING_DATA_KEY;
    }

    @Override
    public void handleMessageData(Context context, String messageData) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        GroupMeetingPushMessage groupMeetingPushMessage = gson.fromJson(messageData, GroupMeetingPushMessage.class);
        Timber.d("New group meeting content: %s", groupMeetingPushMessage);
        Intent intent = new Intent(GROUP_MEETING_INTENT_ACTION);
        Bundle bundle = new Bundle();
        bundle.putParcelable(GROUP_MEETING_DATA_KEY, groupMeetingPushMessage);
        intent.putExtras(bundle);
        context.sendOrderedBroadcast(intent, null);
    }
}
