package com.bimber.features.messaging.groupparticipation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.entities.GroupParticipationPushMessage;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import timber.log.Timber;

/**
 * Created by maciek on 05.04.17.
 */

public class GroupParticipationCloudMessageDataHandler implements CloudMessageDataHandler {


    public static final String GROUP_PARTICIPATION_DATA_KEY = "group_participation_data_key";
    public static final String GROUP_PARTICIPATION_INTENT_ACTION = "GROUP_PARTICIPATION_INTENT_ACTION";

    @Override
    public String dataKey() {
        return GROUP_PARTICIPATION_DATA_KEY;
    }

    @Override
    public void handleMessageData(Context context, String messageData) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        GroupParticipationPushMessage groupParticipationPushMessage = gson.fromJson(messageData, GroupParticipationPushMessage.class);
        Timber.d("New group participation content: %s", groupParticipationPushMessage);
        Intent intent = new Intent(GROUP_PARTICIPATION_INTENT_ACTION);
        Bundle bundle = new Bundle();
        bundle.putParcelable(GROUP_PARTICIPATION_DATA_KEY, groupParticipationPushMessage);
        intent.putExtras(bundle);
        context.sendOrderedBroadcast(intent, null);
    }
}
