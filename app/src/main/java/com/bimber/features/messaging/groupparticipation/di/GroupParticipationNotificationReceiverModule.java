package com.bimber.features.messaging.groupparticipation.di;

import com.bimber.base.BroadcastScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.messaging.groupparticipation.GroupParticipationNotificationReceiver;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

@Module
public abstract class GroupParticipationNotificationReceiverModule {

    @Binds
    @BroadcastScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(GroupParticipationNotificationReceiver receiver);
}
