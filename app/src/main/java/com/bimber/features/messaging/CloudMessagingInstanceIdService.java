package com.bimber.features.messaging;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.bimber.base.application.BimberApplication;
import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.data.repositories.MessagingTokenRepository;
import com.bimber.data.sources.profile.user.UserNetworkSource;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 12.01.17.
 */

public class CloudMessagingInstanceIdService extends FirebaseInstanceIdService {

    public static final String REGISTRATION_ID = "registration_id";

    @Inject
    FirebaseAuthService firebaseAuthService;

    @Inject
    MessagingTokenRepository messagingTokenRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        BimberApplication.getAppComponent().inject(this);
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Timber.d("Refreshed token: %s", refreshedToken);
        saveTokenToPrefs(this, refreshedToken);
        firebaseAuthService.onUserLoggedInUid()
                .flatMapCompletable(currentUserId -> messagingTokenRepository.setToken(currentUserId, refreshedToken))
                .subscribe(() -> Timber.d("Token refreshed: %s", refreshedToken),
                        throwable -> Timber.e(throwable, "Failed to refresh token for user"));

    }

    public static void saveTokenToPrefs(Context context, String token) {
        // Access Shared Preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        // Save to SharedPreferences
        editor.putString(REGISTRATION_ID, token);
        editor.apply();
    }

    public static String getTokenFromPrefs() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(BimberApplication.getAppComponent().application());
        return preferences.getString(REGISTRATION_ID, null);
    }
}

