package com.bimber.features.messaging.newchatmessages.di;

import com.bimber.base.BroadcastScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.messaging.newchatmessages.NewChatMessageNotificationReceiver;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

@Module
public abstract class NewChatMessageBroadcastReceiverModule {
    @Binds
    @BroadcastScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(NewChatMessageNotificationReceiver receiver);
}
