package com.bimber.features.messaging.groupparticipation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.GroupParticipationPushMessage;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.features.chat.NotificationSoundManager;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 02.06.17.
 */

public class GroupParticipationSnackbarFragment extends BaseFragment {


    public static final String HANDLING_PRIORITY = "HANDLING_PRIORITY";

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    GroupRepository groupRepository;
    @Inject
    ChatMemberRepository chatMemberRepository;
    @Inject
    NotificationSoundManager notificationSoundManager;

    private GroupParticipationBroadcastReceiver groupParticipationBroadcastReceiver;
    private ViewGroup container;
    private final CompositeDisposable subscribtions = new CompositeDisposable();

    public static GroupParticipationSnackbarFragment newInstance(HandlingPriority handlingPriority) {
        GroupParticipationSnackbarFragment groupParticipationSnackbarFragment = new GroupParticipationSnackbarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HANDLING_PRIORITY, handlingPriority.toString());
        groupParticipationSnackbarFragment.setArguments(bundle);
        return groupParticipationSnackbarFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.container = container;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        groupParticipationBroadcastReceiver = new GroupParticipationBroadcastReceiver();

        subscribtions.add(groupParticipationBroadcastReceiver.onGroupParticipationPushMessage()
                .flatMapSingle(this::getGroupJoinRequestSnackbarText)
                .subscribe(groupParticipationSnackbarText -> {
                    Snackbar.make(container, groupParticipationSnackbarText, Snackbar.LENGTH_LONG).show();
                    notificationSoundManager.play();
                }, throwable -> Timber.e(throwable, "Fatal Error while trying to show group participation snackbar. No more " +
                        "snackbars wil lbe shown from this stream")));

        HandlingPriority handlingPriority = HandlingPriority.valueOf(getArguments().getString(HANDLING_PRIORITY));
        IntentFilter groupParticipationIntentFilter = new IntentFilter(GroupParticipationCloudMessageDataHandler
                .GROUP_PARTICIPATION_INTENT_ACTION);
        groupParticipationIntentFilter.setPriority(handlingPriority.order);
        getActivity().registerReceiver(groupParticipationBroadcastReceiver, groupParticipationIntentFilter);
    }


    @Override
    public void onStop() {
        getActivity().unregisterReceiver(groupParticipationBroadcastReceiver);
        subscribtions.clear();
        super.onStop();
    }

    private Single<String> getGroupJoinRequestSnackbarText(GroupParticipationPushMessage groupParticipationPushMessage) {
        return Single.just(groupParticipationPushMessage)
                .map(pushMessage -> {
                    switch (pushMessage.groupJoinRequest().status()) {
                        case MEMBERSHIP_REQUESTED:
                            return getPendingGroupJoinRequestSnackbarText(pushMessage);
                        case ACCEPTED:
                            return getGroupAcceptationSnackbarText(pushMessage);
                        case REMOVED:
                            return getGroupRemovalSnackbarText(pushMessage);
                        case NONE:
                        default:
                            throw new IllegalArgumentException("User should not receive other notifications than about" +
                                    "MEMBERSHIP_REQUESTED, ACCEPTED and REJECTED states ");
                    }
                });
    }

    private String getGroupRemovalSnackbarText(GroupParticipationPushMessage pushMessage) {
        return String.format(getString(R.string.group_participation_removed_snackbar_text),
                unpackActingUserName(pushMessage), pushMessage.groupName());
    }

    private String getGroupAcceptationSnackbarText(GroupParticipationPushMessage pushMessage) {
        return String.format(getString(R.string.group_participation_added_snackbar_text),
                unpackActingUserName(pushMessage), pushMessage.groupName());
    }

    private String getPendingGroupJoinRequestSnackbarText(GroupParticipationPushMessage pushMessage) {
        return String.format(getString(R.string.group_participation_join_request_snackbar_text),
                unpackParticipantName(pushMessage), pushMessage.groupName());
    }

    private String unpackActingUserName(GroupParticipationPushMessage pushMessage) {
        return pushMessage.actingUserName() != null ? pushMessage.actingUserName() :
                defaultValues.defaultUser(pushMessage.participantId()).displayName;
    }

    private String unpackParticipantName(GroupParticipationPushMessage pushMessage) {
        return pushMessage.participantName() != null ? pushMessage.participantName() :
                defaultValues.defaultUser(pushMessage.participantId()).displayName;
    }

    private static class GroupParticipationBroadcastReceiver extends BroadcastReceiver {

        private PublishSubject<GroupParticipationPushMessage> groupParticipationPushMessagePublishSubject = PublishSubject.create();

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("Group participation content received in GroupParticipationSnackbarFragment. Showing snackbar.");
            GroupParticipationPushMessage matchPushMessage = intent.getParcelableExtra(GroupParticipationCloudMessageDataHandler
                    .GROUP_PARTICIPATION_DATA_KEY);
            groupParticipationPushMessagePublishSubject.onNext(matchPushMessage);
            abortBroadcast();
        }

        public Observable<GroupParticipationPushMessage> onGroupParticipationPushMessage() {
            return groupParticipationPushMessagePublishSubject;

        }
    }
}
