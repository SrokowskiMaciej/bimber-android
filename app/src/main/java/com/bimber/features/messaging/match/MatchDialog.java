package com.bimber.features.messaging.match;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bumptech.glide.Glide;

/**
 * Created by srokowski.maciej@gmail.com on 15.01.17.
 */

public class MatchDialog extends DialogFragment {


    public static final String MATCHED_USER_MATCH_DATA_KEY = "MATCHED_USER_MATCH_DATA_KEY";
    public static final String CURRENT_USER_MATCH_DATA_KEY = "CURRENT_USER_MATCH_DATA_KEY";

    public static MatchDialog newInstance(ProfileDataThumbnail currentProfileDataThumbnail, ProfileDataThumbnail
            matchedProfileDataThumbnail) {
        final MatchDialog matchDialog = new MatchDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(CURRENT_USER_MATCH_DATA_KEY, currentProfileDataThumbnail);
        bundle.putSerializable(MATCHED_USER_MATCH_DATA_KEY, matchedProfileDataThumbnail);
        matchDialog.setArguments(bundle);
        return matchDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        ProfileDataThumbnail currentProfileDataThumbnail = (ProfileDataThumbnail) getArguments().getSerializable(CURRENT_USER_MATCH_DATA_KEY);
        ProfileDataThumbnail matchedProfileDataThumbnail = (ProfileDataThumbnail) getArguments().getSerializable(MATCHED_USER_MATCH_DATA_KEY);
        View view = getActivity().getLayoutInflater().inflate(R.layout.view_discover_dialog_match, null);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams
                .MATCH_PARENT));
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), R.color
                .colorTranslucentBackgroundDark)));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(false);

        final ImageView currentUserImageView = view.findViewById(R.id.imageViewCurrentUser);
        GlideApp.with(this).load(currentProfileDataThumbnail.photo.userPhotoUri).into(currentUserImageView);

        final ImageView matchedUserImageView = view.findViewById(R.id.imageViewMatchedUser);
        Glide.with(this).load(matchedProfileDataThumbnail.photo.userPhotoUri).into(matchedUserImageView);

        final Button button = view.findViewById(R.id.buttonOk);
        button.setOnClickListener(v -> dialog.dismiss());

        final TextView textView = view.findViewById(R.id.textViewMatchContent);
        textView.setText(String.format(getString(R.string.view_discover_match_dialog_content_text),
                matchedProfileDataThumbnail.user.displayName));
        return dialog;
    }
}
