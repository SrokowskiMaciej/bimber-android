package com.bimber.features.messaging;

/**
 * Created by maciek on 26.07.17.
 */
public enum NotificationType {
    NEW_MESSAGE(45),
    MATCH(46),
    GROUP_PARTICIPATION(47),
    GROUP_JOIN_REQUEST(48),
    GROUP_MEETING_TIME(49),
    GROUPED_NOTIFICATION(50),
    NONE(51);

    public static final NotificationType values[] = values();
    private int notificationIdModifier;

    NotificationType(int notificationIdModifier) {
        this.notificationIdModifier = notificationIdModifier;
    }

    public int getNotificationId(String chatId) {
        return chatId.hashCode() + notificationIdModifier;
    }

    public static NotificationType getNotificationType(String chatId, int id) {
        for (NotificationType notificationType : values) {
            if (id - chatId.hashCode() == notificationType.notificationIdModifier) {
                return notificationType;
            }
        }
        return NONE;
    }


}
