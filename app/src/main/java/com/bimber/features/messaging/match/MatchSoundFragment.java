package com.bimber.features.messaging.match;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.entities.MatchPushMessage;
import com.bimber.data.sources.profile.user.UserNetworkSource;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 02.06.17.
 */

public class MatchSoundFragment extends BaseFragment {

    public static final String HANDLING_PRIORITY = "HANDLING_PRIORITY";

    @Inject
    @LoggedInUserId
    String currentUserId;

    private MatchBroadcastReceiver matchBroadcastReceiver;
    private final CompositeDisposable subscribtions = new CompositeDisposable();

    public static MatchSoundFragment newInstance(HandlingPriority handlingPriority) {
        MatchSoundFragment matchSoundFragment = new MatchSoundFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HANDLING_PRIORITY, handlingPriority.toString());
        matchSoundFragment.setArguments(bundle);
        return matchSoundFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        matchBroadcastReceiver = new MatchBroadcastReceiver();
        subscribtions.add(matchBroadcastReceiver.onMatchPushMessage()
                .filter(matchPushMessage -> matchPushMessage.likedUserId().equals(currentUserId))
                .subscribe(likingUser -> {
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getContext().getApplicationContext(), notification);
                        r.play();
                    } catch (Exception exception) {
                        Timber.e(exception, "Failed to play new chat message sound");
                    }
                }, throwable -> Timber.e(throwable, "Fatal error while trying to play new chat message sound," +
                        "user won't be notified by sound further")));

        HandlingPriority handlingPriority = HandlingPriority.valueOf(getArguments().getString(HANDLING_PRIORITY));
        IntentFilter matchIntentFilter = new IntentFilter(MatchCloudMessageDataHandler.MATCH_INTENT_ACTION);
        matchIntentFilter.setPriority(handlingPriority.order);
        getActivity().registerReceiver(matchBroadcastReceiver, matchIntentFilter);
    }


    @Override
    public void onStop() {
        getActivity().unregisterReceiver(matchBroadcastReceiver);
        subscribtions.clear();
        super.onStop();
    }

    private class MatchBroadcastReceiver extends BroadcastReceiver {

        private PublishSubject<MatchPushMessage> matchPushMessagePublishSubject = PublishSubject.create();

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("Match content broadcast received in GroupParticipationSnackbarFragment. Showing snackbar.");
            MatchPushMessage matchPushMessage = intent.getParcelableExtra(MatchCloudMessageDataHandler.MATCH_DATA_KEY);
            matchPushMessagePublishSubject.onNext(matchPushMessage);
            abortBroadcast();
        }

        public Observable<MatchPushMessage> onMatchPushMessage() {
            return matchPushMessagePublishSubject;
        }
    }
}
