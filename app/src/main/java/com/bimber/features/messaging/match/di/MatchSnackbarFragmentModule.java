package com.bimber.features.messaging.match.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.messaging.match.MatchSnackbarFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class MatchSnackbarFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract MatchSnackbarFragment contribute();
}
