package com.bimber.features.messaging.mute;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.ContextThemeWrapper;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.NotificationMuteState;
import com.bimber.data.repositories.NotificationMuteStateRepository;
import com.bimber.utils.view.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by maciek on 22.08.17.
 */

public class MuteNotificationActivity extends BaseLoggedInActivity {

    private static final String CHAT_TYPE_KEY = "CHAT_TYPE_KEY";
    private static final String CHAT_ID_KEY = "CHAT_ID_KEY";
    private static final String CHAT_NAME_KEY = "CHAT_NAME_KEY";
    private static final String THEME_KEY = "THEME_KEY";

    @Inject
    NotificationMuteStateRepository notificationMuteStateRepository;


    public static Intent newInstance(Context context, String chatId, ChatType chatType, String chatName, @StyleRes int theme) {
        Intent intent = new Intent(context, MuteNotificationActivity.class);
        intent.putExtra(CHAT_TYPE_KEY, chatType.toString());
        intent.putExtra(CHAT_ID_KEY, chatId);
        intent.putExtra(CHAT_NAME_KEY, chatName);
        intent.putExtra(THEME_KEY, theme);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        ChatType chatType = ChatType.valueOf(getIntent().getStringExtra(CHAT_TYPE_KEY));
        String chatId = getIntent().getStringExtra(CHAT_ID_KEY);
        String chatName = getIntent().getStringExtra(CHAT_NAME_KEY);
        int theme = getIntent().getIntExtra(THEME_KEY, R.style.AppTheme);

        String contentText;
        if (chatType == ChatType.GROUP) {
            contentText = String.format(getString(R.string.notification_mute_group_content), chatName);
        } else {
            contentText = String.format(getString(R.string.notification_mute_dialog_content), chatName);
        }

        NotificationMuteState savedState = notificationMuteStateRepository.onState(chatId).blockingFirst();

        new MaterialDialog.Builder(new ContextThemeWrapper(this, theme))
                .title(R.string.notification_mute_title)
                .content(contentText)
                .items(getMuteOptions())
//                .positiveText(R.string.dialog_ok_button_text)
                .itemsCallbackSingleChoice(savedState.state().position(), new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        NotificationMuteState.State newState = NotificationMuteState.State.fromPosition(which);
                        notificationMuteStateRepository.storeState(chatId, NotificationMuteState.create(newState,
                                System.currentTimeMillis()));
                        return true;
                    }
                })
                //Its going to flicker otherwise because dilog closing animation didn't finish yet
                .dismissListener(dialog -> new Handler().postDelayed(() -> ViewUtils.finishActivitySafely(this), 300))
                .show();

    }

    @Override
    protected void onDestroy() {
        overridePendingTransition(0, 0);
        super.onDestroy();
    }

    public List<String> getMuteOptions() {
        List<String> options = new ArrayList<>();
        for (NotificationMuteState.State state : NotificationMuteState.State.values) {
            switch (state) {
                case NORMAL:
                    options.add(getString(R.string.notification_mute_status_normal));
                    break;
                case NO_SOUND:
                    options.add(getString(R.string.notification_mute_status_no_sound));
                    break;
                case MUTE_1_HOUR:
                    options.add(getString(R.string.notification_mute_status_mute_one_hour));
                    break;
                case MUTED_INDEFINITELY:
                    options.add(getString(R.string.notification_mute_status_muted_indefinitely));
                    break;
            }
        }
        return options;
    }
}
