package com.bimber.features.messaging.newchatmessages;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.NewChatMessagePushMessage;
import com.bimber.data.entities.NewChatMessagePushMessage.MessageInfo;
import com.bimber.data.entities.NotificationMuteState.State;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.LastSeenMessagesRepository;
import com.bimber.data.repositories.NotificationMuteStateRepository;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.messaging.ChatNotification;
import com.bimber.features.messaging.NotificationType;
import com.bimber.features.messaging.mute.MuteNotificationActivity;
import com.bimber.utils.images.BitmapUtils;
import com.google.auto.value.AutoValue;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;

/**
 * Created by maciek on 26.07.17.
 */
public class NewChatMessageNotificationProvider {

    private final String currentUserId;
    private final DefaultValues defaultValues;
    private final MessageRepository messageRepository;
    private final ChatMemberRepository chatMemberRepository;
    private final LastSeenMessagesRepository lastSeenMessagesRepository;
    private final NotificationMuteStateRepository notificationMuteStateRepository;

    @Inject
    public NewChatMessageNotificationProvider(@LoggedInUserId String currentUserId, DefaultValues defaultValues, MessageRepository
            messageRepository, ChatMemberRepository chatMemberRepository, LastSeenMessagesRepository lastSeenMessagesRepository,
                                              NotificationMuteStateRepository notificationMuteStateRepository) {

        this.currentUserId = currentUserId;
        this.defaultValues = defaultValues;
        this.messageRepository = messageRepository;
        this.chatMemberRepository = chatMemberRepository;
        this.lastSeenMessagesRepository = lastSeenMessagesRepository;
        this.notificationMuteStateRepository = notificationMuteStateRepository;
    }

    public Maybe<ChatNotification> chatMessageNotification(Context context, NewChatMessagePushMessage pushMessage, boolean soundAllowed) {

        Observable<MessageWithUserName> messages = notSeenMessages(pushMessage).share();

        Maybe<Boolean> shouldDisplayNotification = notificationMuteStateRepository.onState(pushMessage.chatId())
                .firstOrError()
                //Only those two states mean that notification should be shown
                .filter(muteState -> muteState.state() == State.NO_SOUND || muteState.state() == State.NORMAL)
                .flatMapSingleElement(notificationMuteState -> messages.isEmpty())
                //We shouldn't show notifications with no messages
                .filter(isMessageStreamEmpty -> !isMessageStreamEmpty);


        return shouldDisplayNotification.flatMap(shouldDisplay -> {
            Observable<NotificationCompat.MessagingStyle> messagingStyle = messages.compose(messagingStyle(context, pushMessage));
            Single<Bitmap> icon = icon(context, pushMessage);
            Single<Integer> notificationDefaults = notificationDefaults(pushMessage, soundAllowed);
            return Maybe.zip(messagingStyle.singleElement(), icon.toMaybe(), notificationDefaults.toMaybe(), (messaging, bitmap,
                                                                                                              defaults) -> {


                Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
                Intent chatIntent = ChatActivity.newInstance(context, pushMessage.chatId(), pushMessage.chatType());
                PendingIntent broadcast = TaskStackBuilder.create(context)
                        .addNextIntent(mainScreenIntent)
                        .addNextIntent(chatIntent)
                        .getPendingIntent(NotificationType.NEW_MESSAGE.getNotificationId(pushMessage.chatId()), PendingIntent
                                .FLAG_UPDATE_CURRENT);

                android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        .setContentTitle(pushMessage.chatName())
                        .setSmallIcon(R.drawable.ic_bimber_notification)
                        .setLargeIcon(bitmap)
                        .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setGroup(pushMessage.chatId())
                        .setSortKey("555")
                        .setNumber(messaging.getMessages().size())
                        .setStyle(messaging)
                        .setContentIntent(broadcast)
                        .setDefaults(defaults)
                        .setOnlyAlertOnce(true)
                        .setAutoCancel(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    builder = builder.addAction(getRemoteInputAction(context, pushMessage));
                }

                Intent muteActivity = MuteNotificationActivity.newInstance(context, pushMessage.chatId(), pushMessage.chatType(),
                        pushMessage.chatName(), R.style.AppTheme)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);

                PendingIntent muteBroadcast = PendingIntent.getActivity(context, pushMessage.chatId().hashCode(),
                        muteActivity, PendingIntent.FLAG_UPDATE_CURRENT);

                builder.addAction(R.drawable.ic_notifications_off_white_24dp, context.getString(R.string.notification_mute_text)
                        , muteBroadcast);

                Notification notification = builder.build();
                return ChatNotification.create(NotificationType.NEW_MESSAGE, pushMessage.chatId(), pushMessage.chatType(),
                        pushMessage.chatName(), notification);
            });
        });
    }

    private Single<Bitmap> icon(Context context, NewChatMessagePushMessage pushMessage) {
        switch (pushMessage.chatType()) {
            case DIALOG:
                return dialogIcon(context, pushMessage);
            case GROUP:
                return partyIcon(context, pushMessage);
            default:
                throw new IllegalArgumentException();
        }
    }

    private Single<Bitmap> dialogIcon(Context context, NewChatMessagePushMessage pushMessage) {
        return Observable.fromIterable(pushMessage.photos())
                .filter(photoInfo -> photoInfo.photoUri() != null)
                .firstOrError()
                .flatMap(photoInfo -> BitmapUtils.bitmap(context, photoInfo.photoUri(), ChatNotification.ICON_SIZE, ChatNotification
                        .ICON_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_person_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }

    private Single<Bitmap> partyIcon(Context context, NewChatMessagePushMessage pushMessage) {
        return Observable.fromIterable(pushMessage.photos())
                .map(photoInfo -> {
                    if (photoInfo.photoUri() != null) {
                        return photoInfo.photoUri();
                    } else {
                        return defaultValues.defaultUserPhoto(photoInfo.sourceId()).userPhotoUri;
                    }
                })
                .toList()
                .flatMap(photos -> BitmapUtils.bitmapCollage(context, photos, ChatNotification.ICON_SIZE, ChatNotification.ICON_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_group_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }

    private ObservableTransformer<MessageWithUserName, NotificationCompat.MessagingStyle> messagingStyle(Context context,
                                                                                                         NewChatMessagePushMessage
                                                                                                                 pushMessage) {
        return upstream -> upstream
                .collect(() -> {
                    String chatName = unpackChatName(pushMessage);
                    NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(context.getString(R.string
                            .notification_new_chat_message_user_name_display));
                    messagingStyle.setConversationTitle(chatName);
                    return messagingStyle;
                }, (messagingStyle, messageWithUserName) -> {
                    String senderName = !messageWithUserName.message().getUserId().equals(currentUserId) ?
                            messageWithUserName.userName() :
                            null;
                    messagingStyle.addMessage(MessageToTextConverter.stringifyMessage(context,
                            currentUserId, pushMessage.chatType(), messageWithUserName.userName(), messageWithUserName.message()),
                            messageWithUserName.message().getTimestamp(), senderName);
                })
                .toObservable();
    }

    private Observable<MessageWithUserName> notSeenMessages(NewChatMessagePushMessage pushMessage) {
        return chatMemberRepository.chatMembership(pushMessage.chatId(), currentUserId)
                //TODO Hmmm, cache may be a problem here
                .filter(chatVisibleChatMembership -> chatVisibleChatMembership.membershipStatus() == MembershipStatus.ACTIVE)
                .flatMapObservable(chatVisibleChatMembership ->
                        lastSeenMessagesRepository.chatUserLastSeenMessage(pushMessage.chatId(), currentUserId)
                                .flatMap(lastSeenMessage -> messageRepository.message(chatVisibleChatMembership,
                                        lastSeenMessage.value()))
                                .map(Message::getTimestamp)
                                .defaultIfEmpty(0l)
                                .flatMapObservable(lastSeenMessageTimestamp -> Observable.fromIterable(pushMessage.messages())
                                        .filter(messageInfo -> messageInfo.messageTimestamp() > lastSeenMessageTimestamp)
                                        .flatMapMaybe(messageInfo -> messageRepository.message(chatVisibleChatMembership, messageInfo
                                                .messageId())
                                                .map(message -> MessageWithUserName.create(unpackUserName(messageInfo), message)))))
                .sorted((o1, o2) -> Long.compare(o1.message().getTimestamp(), o2.message().getTimestamp()));
    }

    private Single<Integer> notificationDefaults(NewChatMessagePushMessage pushMessage, boolean soundAllowed) {
        Single<Boolean> allowedFromMuteState = notificationMuteStateRepository.onState(pushMessage.chatId())
                .map(notificationMuteState -> notificationMuteState.state() == State.NORMAL)
                .firstOrError();

        return Single.zip(Single.just(soundAllowed), allowedFromMuteState,
                (allowed1, allowed2) -> allowed1 && allowed2)
                .map(soundAllowedResult -> soundAllowedResult ? Notification.DEFAULT_ALL : Notification.DEFAULT_LIGHTS);
    }

    private String unpackUserName(MessageInfo messageInfo) {
        return messageInfo.userName() != null ? messageInfo.userName() :
                defaultValues.defaultUser(messageInfo.messageId()).displayName;
    }

    private String unpackChatName(NewChatMessagePushMessage pushMessage) {
        switch (pushMessage.chatType()) {
            case DIALOG:
                return pushMessage.chatName() != null ? pushMessage.chatName() :
                        defaultValues.defaultUser(pushMessage.chatId()).displayName;
            case GROUP:
                return pushMessage.chatName() != null ? pushMessage.chatName() :
                        defaultValues.getDefaultPartyName();
            default:
                throw new IllegalArgumentException();
        }
    }

    @AutoValue
    public static abstract class MessageWithUserName {
        public abstract String userName();

        public abstract Message message();

        public static MessageWithUserName create(String userName, Message message) {
            return new AutoValue_NewChatMessageNotificationProvider_MessageWithUserName(userName, message);
        }
    }

    private NotificationCompat.Action getRemoteInputAction(Context context, NewChatMessagePushMessage pushMessage) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(InlineReplyNotificationReceiver.PUSH_MESSAGE_KEY, pushMessage);

        RemoteInput remoteInput = new RemoteInput.Builder(InlineReplyNotificationReceiver.NEW_MESSAGE_TEXT_KEY)
                .setLabel(context.getString(R.string.notification_new_chat_message_reply_action))
                .build();

        Intent intent = new Intent(context, InlineReplyNotificationReceiver.class);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, pushMessage.chatId().hashCode(), intent, PendingIntent
                .FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Action.Builder(R.drawable
                .ic_send_white_24dp, context.getString(R.string.notification_new_chat_message_reply_action), pendingIntent)
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(true)
                .build();
    }

}
