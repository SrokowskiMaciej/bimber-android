package com.bimber.features.messaging.newchatmessages.di;

import com.bimber.base.BroadcastScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.messaging.newchatmessages.InlineReplyNotificationReceiver;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

@Module
public abstract class InlineReplyBroadcastReceiverModule {

    @Binds
    @BroadcastScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(InlineReplyNotificationReceiver receiver);
}
