package com.bimber.features.messaging.groupparticipation;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInBroadcastReceiver;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.GroupParticipationPushMessage;
import com.bimber.data.entities.NotificationMuteState;
import com.bimber.data.entities.NotificationMuteState.State;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.NotificationMuteStateRepository;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.groupcandidates.activity.GroupJoinCandidatesActivity;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.messaging.ChatNotification;
import com.bimber.features.messaging.NotificationType;
import com.bimber.features.messaging.mute.MuteNotificationActivity;
import com.bimber.utils.images.BitmapUtils;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.bimber.data.entities.Chattable.ChatType.GROUP;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */

public class GroupParticipationNotificationReceiver extends BaseLoggedInBroadcastReceiver {

    @Inject
    GetGroupDataUseCase getGroupDataUseCase;
    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    ChatMemberRepository chatMemberRepository;
    @Inject
    ProfileDataRepository profileDataRepository;
    @Inject
    NotificationMuteStateRepository notificationMuteStateRepository;

    @Override
    public void onReceiveLoggedIn(Context context, Intent intent) {
        GroupParticipationPushMessage groupParticipationPushMessage = intent.getParcelableExtra(GroupParticipationCloudMessageDataHandler
                .GROUP_PARTICIPATION_DATA_KEY);

        Maybe.defer(() -> Maybe.just(groupParticipationPushMessage))
                .flatMap(pushMessage -> {
                    switch (pushMessage.status()) {
                        case MEMBERSHIP_REQUESTED:
                            return incomingParticipationRequestNotificationObservable(context, pushMessage);
                        case ACCEPTED:
                        case REMOVED:
                            return participationRequestResultNotificationObservable(context, pushMessage);
                        default:
                            return Maybe.error(new IllegalArgumentException("User should not receive other notifications than about" +
                                    "MEMBERSHIP_REQUESTED, ACCEPTED and REMOVED states "));
                    }
                })
                .subscribe(chatNotification -> chatNotification.showNotification(context),
                        throwable -> Timber.e(throwable, "Failed to show group particiaption message"),
                        () -> Timber.d("This chat participation message shouldn't be shown"));
    }

    @Override
    public void onReceiveLoggedOut(Context context, Intent intent) {

    }

    public Maybe<ChatNotification> participationRequestResultNotificationObservable(Context context, GroupParticipationPushMessage
            pushMessage) {
        String actingUserName = pushMessage.actingUserName() != null ? pushMessage.actingUserName() :
                defaultValues.defaultUser(pushMessage.participantId()).displayName;

        Single<Bitmap> groupBitmapSingle = partyIcon(context, pushMessage);

        Maybe<NotificationMuteState> muteStateMaybe = notificationMuteStateRepository.onState(pushMessage.groupId())
                //Only those two states mean that notification should be shown
                .filter(state -> state.state() == State.NORMAL || state.state() == State.NO_SOUND)
                .firstElement();

        return muteStateMaybe.flatMap(notificationMuteState -> groupBitmapSingle.toMaybe()
                .map(groupBitmap -> {
                    Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
                    Intent chatIntent = ChatActivity.newInstance(context, pushMessage.groupId(), ChatType.GROUP);
                    PendingIntent broadcast = TaskStackBuilder.create(context)
                            .addNextIntent(mainScreenIntent)
                            .addNextIntent(chatIntent)
                            .getPendingIntent(pushMessage.groupId().hashCode(), PendingIntent.FLAG_UPDATE_CURRENT);

                    android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                            .setContentTitle(pushMessage.groupName())
                            .setSmallIcon(R.drawable.ic_bimber_notification)
                            .setLargeIcon(groupBitmap)
                            .setGroup(pushMessage.groupId())
                            .setPriority(Notification.PRIORITY_DEFAULT)
                            .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            .setSortKey("000")
                            .setContentIntent(broadcast)
                            .setAutoCancel(true)
                            .setCategory(NotificationCompat.CATEGORY_SOCIAL);

                    if (notificationMuteState.state() == State.NO_SOUND) {
                        builder.setDefaults(Notification.DEFAULT_LIGHTS);
                    } else {
                        builder.setDefaults(Notification.DEFAULT_ALL);
                    }

                    switch (pushMessage.status()) {
                        case ACCEPTED:
                            builder.setContentText(String.format(context.getString(R.string.notification_group_participation_added),
                                    actingUserName, pushMessage.groupName()));
                            break;
                        case REMOVED:
                            builder.setContentText(String.format(context.getString(R.string.notification_group_participation_removed),
                                    actingUserName, pushMessage.groupName()));
                            break;
                        default:
                            throw new IllegalStateException("We shouldn't get here");
                    }
                    Notification notification = builder.build();
                    return ChatNotification.create(NotificationType.GROUP_PARTICIPATION, pushMessage.groupId(), ChatType.GROUP,
                            pushMessage.groupName(), notification);
                }));
    }

    public Maybe<ChatNotification> incomingParticipationRequestNotificationObservable(Context context, GroupParticipationPushMessage
            pushMessage) {

        Single<Bitmap> candidateBitmapSingle = candidateIcon(context, pushMessage);

        String candidateName = pushMessage.participantName() != null ? pushMessage.participantName() :
                defaultValues.defaultUser(pushMessage.participantId()).displayName;

        Maybe<NotificationMuteState> muteStateMaybe = notificationMuteStateRepository.onState(pushMessage.groupId())
                //Only those two states mean that notification should be shown
                .filter(state -> state.state() == State.NORMAL || state.state() == State.NO_SOUND)
                .firstElement();

        return muteStateMaybe.flatMap(notificationMuteState -> candidateBitmapSingle.toMaybe()
                .map(candidateBitmap -> {
                    Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
                    Intent chatIntent = ChatActivity.newInstance(context, pushMessage.groupId(), GROUP);
                    Intent groupJoinRequestsIntent = GroupJoinCandidatesActivity.newInstance(context, pushMessage.groupId());
                    PendingIntent broadcast = TaskStackBuilder.create(context)
                            .addNextIntent(mainScreenIntent)
                            .addNextIntent(chatIntent)
                            .addNextIntent(groupJoinRequestsIntent)
                            .getPendingIntent(NotificationType.GROUP_JOIN_REQUEST.getNotificationId(pushMessage.groupId()),
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                    android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                            .setContentTitle(pushMessage.groupName())
                            .setContentText(String.format(context.getString(R.string.notification_group_participation_new_join_request), candidateName, pushMessage.groupName()))
                            .setSmallIcon(R.drawable.ic_bimber_notification)
                            .setLargeIcon(candidateBitmap)
                            .setPriority(Notification.PRIORITY_DEFAULT)
                            .setGroup(pushMessage.groupId())
                            .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            .setSortKey("000")
                            .setContentIntent(broadcast)
                            .setAutoCancel(true)
                            .setOnlyAlertOnce(true)
                            .setStyle(new BigTextStyle().bigText(String.format(context.getString(R.string
                                    .notification_group_participation_new_join_request), candidateName, pushMessage.groupName())))
                            .setCategory(NotificationCompat.CATEGORY_SOCIAL);

                    if (notificationMuteState.state() == State.NO_SOUND) {
                        builder.setDefaults(Notification.DEFAULT_LIGHTS);
                    } else {
                        builder.setDefaults(Notification.DEFAULT_ALL);
                    }

                    Intent muteActivity = MuteNotificationActivity.newInstance(context, pushMessage.groupId(), GROUP, pushMessage
                            .groupName(), R.style.AppTheme)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);

                    PendingIntent muteBroadcast = PendingIntent.getActivity(context, pushMessage.groupId().hashCode(),
                            muteActivity, PendingIntent.FLAG_UPDATE_CURRENT);

                    builder.addAction(R.drawable.ic_notifications_off_white_24dp, context.getString(R.string.notification_mute_text)
                            , muteBroadcast);

                    Notification notification = builder.build();

                    return ChatNotification.create(NotificationType.GROUP_JOIN_REQUEST, pushMessage.groupId(), ChatType.GROUP,
                            pushMessage.groupName(), notification);
                }));
    }

    private Single<Bitmap> partyIcon(Context context, GroupParticipationPushMessage pushMessage) {
        return Observable.fromIterable(pushMessage.photos())
                .map(photoInfo -> {
                    if (photoInfo.photoUri() != null) {
                        return photoInfo.photoUri();
                    } else {
                        return defaultValues.defaultUserPhoto(photoInfo.sourceId()).userPhotoUri;
                    }
                })
                .toList()
                .flatMap(photos -> BitmapUtils.bitmapCollage(context, photos, ChatNotification.ICON_SIZE, ChatNotification.ICON_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_group_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }

    private Single<Bitmap> candidateIcon(Context context, GroupParticipationPushMessage pushMessage) {
        String candidatePhoto = pushMessage.participantPhoto() != null ? pushMessage.participantPhoto() :
                defaultValues.defaultUserPhoto(pushMessage.participantId()).userPhotoUri;
        return BitmapUtils.bitmap(context, candidatePhoto, ChatNotification.ICON_SIZE, ChatNotification.ICON_SIZE)
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_person_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }
}

