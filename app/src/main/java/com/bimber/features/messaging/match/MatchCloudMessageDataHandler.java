package com.bimber.features.messaging.match;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.entities.MatchPushMessage;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

public class MatchCloudMessageDataHandler implements CloudMessageDataHandler {

    public static final String MATCH_INTENT_ACTION = "MATCH_INTENT_ACTION";
    public static final String MATCH_DATA_KEY = "match_data_key";

    @Override
    public String dataKey() {
        return MATCH_DATA_KEY;
    }

    @Override
    public void handleMessageData(Context context, String messageData) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        MatchPushMessage matchPushMessage = gson.fromJson(messageData, MatchPushMessage.class);
        Timber.d("New match content: %s", matchPushMessage);
        Intent intent = new Intent(MATCH_INTENT_ACTION);
        Bundle bundle = new Bundle();
        bundle.putParcelable(MATCH_DATA_KEY, matchPushMessage);
        intent.putExtras(bundle);
        context.sendOrderedBroadcast(intent, null);
    }
}
