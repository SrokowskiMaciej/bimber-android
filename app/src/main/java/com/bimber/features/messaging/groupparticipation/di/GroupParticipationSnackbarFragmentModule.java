package com.bimber.features.messaging.groupparticipation.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.messaging.groupparticipation.GroupParticipationSnackbarFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class GroupParticipationSnackbarFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract GroupParticipationSnackbarFragment contribute();
}
