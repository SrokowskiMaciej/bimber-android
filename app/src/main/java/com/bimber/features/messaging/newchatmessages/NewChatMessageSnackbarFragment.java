package com.bimber.features.messaging.newchatmessages;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.NewChatMessagePushMessage;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.sources.profile.user.UserNetworkSource;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.bimber.features.chat.NotificationSoundManager;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 02.06.17.
 */

public class NewChatMessageSnackbarFragment extends BaseFragment {


    private static final String HANDLING_PRIORITY = "HANDLING_PRIORITY";
    private static final String EXCLUDED_CHAT_ID = "EXCLUDED_CHAT_ID";
    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    GroupRepository groupRepository;
    @Inject
    NotificationSoundManager notificationSoundManager;

    private NewChatMessageBroadcastReceiver newChatMessageBroadcastReceiver;
    private ViewGroup container;
    private final CompositeDisposable subscribtions = new CompositeDisposable();
    private String excludedChatId;

    public static NewChatMessageSnackbarFragment newInstance(HandlingPriority handlingPriority) {
        NewChatMessageSnackbarFragment newChatMessageSnackbarFragment = new NewChatMessageSnackbarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HANDLING_PRIORITY, handlingPriority.toString());
        newChatMessageSnackbarFragment.setArguments(bundle);
        return newChatMessageSnackbarFragment;
    }

    public static NewChatMessageSnackbarFragment newInstance(HandlingPriority handlingPriority, String chatToExcludeId) {
        NewChatMessageSnackbarFragment newChatMessageSnackbarFragment = new NewChatMessageSnackbarFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HANDLING_PRIORITY, handlingPriority.toString());
        bundle.putString(EXCLUDED_CHAT_ID, chatToExcludeId);
        newChatMessageSnackbarFragment.setArguments(bundle);
        return newChatMessageSnackbarFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.container = container;
        this.excludedChatId = getArguments().getString(EXCLUDED_CHAT_ID);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        newChatMessageBroadcastReceiver = new NewChatMessageBroadcastReceiver(excludedChatId);
        subscribtions.add(newChatMessageBroadcastReceiver.onChatMessage()
                .map(this::getNewMessageSnackbarText)
                .subscribe(notificationText -> {
                    Snackbar.make(container, notificationText, Snackbar.LENGTH_SHORT).show();
                    notificationSoundManager.play();
                }, throwable -> Timber.e(throwable, "Fatal Error while showing new chat message snackbar," +
                        " no more messages will be shown")));


        HandlingPriority handlingPriority = HandlingPriority.valueOf(getArguments().getString(HANDLING_PRIORITY));
        IntentFilter newMessageIntentFilter = new IntentFilter(NewChatMessageCloudMessageDataHandler.NEW_CHAT_MESSAGE_INTENT_ACTION);
        newMessageIntentFilter.setPriority(handlingPriority.order);
        getActivity().registerReceiver(newChatMessageBroadcastReceiver, newMessageIntentFilter);

    }


    @Override
    public void onStop() {
        getActivity().unregisterReceiver(newChatMessageBroadcastReceiver);
        subscribtions.clear();
        super.onStop();
    }

    private String getNewMessageSnackbarText(NewChatMessagePushMessage pushMessage) {
        switch (pushMessage.chatType()) {
            case DIALOG:
                return String.format(getString(R.string.new_dialog_message_snackbar_text), unpackChatName(pushMessage));
            case GROUP:
                return String.format(getString(R.string.new_group_message_snackbar_text), unpackChatName(pushMessage));
            default:
                throw new IllegalArgumentException("Not handling any other types of chats besides DIALOG and GROUP");
        }
    }


    private String unpackChatName(NewChatMessagePushMessage pushMessage) {
        switch (pushMessage.chatType()) {
            case DIALOG:
                return pushMessage.chatName() != null ? pushMessage.chatName() :
                        defaultValues.defaultUser(pushMessage.chatId()).displayName;
            case GROUP:
                return pushMessage.chatName() != null ? pushMessage.chatName() :
                        defaultValues.getDefaultPartyName();
            default:
                throw new IllegalArgumentException();
        }
    }


    private static class NewChatMessageBroadcastReceiver extends BroadcastReceiver {

        @Nullable
        private final String excludedChatId;

        private PublishSubject<NewChatMessagePushMessage> newChatMessagePublishSubject = PublishSubject.create();

        private NewChatMessageBroadcastReceiver(@Nullable String excludedChatId) {
            this.excludedChatId = excludedChatId;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("New chat content received in BaseLoggedInActivity. Showing snackbar.");
            NewChatMessagePushMessage matchPushMessage = intent.getParcelableExtra(NewChatMessageCloudMessageDataHandler
                    .NEW_MESSAGE_DATA_KEY);
            if (excludedChatId == null) {
                newChatMessagePublishSubject.onNext(matchPushMessage);
                abortBroadcast();
            } else if (excludedChatId.equals(matchPushMessage.chatId())) {
                abortBroadcast();
            } else {
                // Show notifications for other chats
            }
        }

        public Observable<NewChatMessagePushMessage> onChatMessage() {
            return newChatMessagePublishSubject;

        }
    }
}
