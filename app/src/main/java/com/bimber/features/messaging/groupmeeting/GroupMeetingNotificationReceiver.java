package com.bimber.features.messaging.groupmeeting;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInBroadcastReceiver;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.Group;
import com.bimber.data.entities.GroupMeetingPushMessage;
import com.bimber.data.entities.NotificationMuteState;
import com.bimber.data.entities.NotificationMuteState.State;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.repositories.NotificationMuteStateRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.messaging.ChatNotification;
import com.bimber.features.messaging.NotificationType;
import com.bimber.features.messaging.mute.MuteNotificationActivity;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.images.BitmapUtils;

import java.text.DateFormat;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.bimber.data.entities.Chattable.ChatType.GROUP;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */

public class GroupMeetingNotificationReceiver extends BaseLoggedInBroadcastReceiver {

    @Inject
    GroupRepository groupRepository;
    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    ChatMemberRepository chatMemberRepository;
    @Inject
    ProfileDataRepository profileDataRepository;
    @Inject
    NotificationMuteStateRepository notificationMuteStateRepository;

    @Override
    public void onReceiveLoggedIn(Context context, Intent intent) {
        GroupMeetingPushMessage groupMeetingPushMessage = intent.getParcelableExtra(GroupMeetingCloudMessageDataHandler
                .GROUP_MEETING_DATA_KEY);
        Maybe.defer(() -> Maybe.just(groupMeetingPushMessage))
                .flatMap(pushMessage -> groupMeetingNotification(context, pushMessage))
                .subscribe(chatNotification -> chatNotification.showNotification(context),
                        throwable -> Timber.e(throwable, "Failed to show group meeting message"),
                        () -> Timber.d("Not showing group meeting message"));
    }

    @Override
    public void onReceiveLoggedOut(Context context, Intent intent) {

    }

    private Maybe<ChatNotification> groupMeetingNotification(Context context, GroupMeetingPushMessage pushMessage) {
        Single<Bitmap> groupBitmapSingle = partyIcon(context, pushMessage);

        Single<Key<Group>> groupSingle = groupRepository.group(pushMessage.groupId()).toSingle();

        Maybe<NotificationMuteState> muteStateMaybe = notificationMuteStateRepository.onState(pushMessage.groupId())
                .filter(state -> state.state() == State.NORMAL || state.state() == State.NO_SOUND)
                .firstElement();

        return muteStateMaybe
                .flatMap(muteState -> Single.zip(groupBitmapSingle, groupSingle, (bitmap, group) -> {
                            Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
                            Intent chatIntent = ChatActivity.newInstance(context, pushMessage.groupId(), ChatType.GROUP);
                            PendingIntent broadcast = TaskStackBuilder.create(context)
                                    .addNextIntent(mainScreenIntent)
                                    .addNextIntent(chatIntent)
                                    .getPendingIntent(NotificationType.GROUP_MEETING_TIME.getNotificationId(pushMessage.groupId()),
                                            PendingIntent.FLAG_UPDATE_CURRENT);

                            BigTextStyle bigTextStyle = new BigTextStyle();
                            android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
//                    .setContentTitle(publicGroupDataThumbnail.chat().value().name())
                                    .setSmallIcon(R.drawable.ic_bimber_notification)
                                    .setLargeIcon(bitmap)
                                    .setPriority(Notification.PRIORITY_DEFAULT)
                                    .setGroup(pushMessage.groupId())
                                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                                    .setSortKey("000")
                                    .setContentIntent(broadcast)
                                    .setAutoCancel(true)
                                    .setCategory(NotificationCompat.CATEGORY_SOCIAL);

                            if (muteState.state() == State.NO_SOUND) {
                                builder.setDefaults(Notification.DEFAULT_LIGHTS);
                            } else {
                                builder.setDefaults(Notification.DEFAULT_ALL);
                            }

                            String groupName = pushMessage.groupName();
                            switch (pushMessage.notificationType()) {
                                case REMINDER:
                                    CharSequence date = DateUtils.formatSameDayTime(group.value().meetingTime().value(),
                                            System.currentTimeMillis(), DateFormat.SHORT, DateFormat.SHORT);
                                    String reminderTitle = context.getString(R.string.notification_group_meeting_reminder_title);
                                    String reminderText = String.format(context.getString(R.string.notification_group_meeting_reminder),
                                            groupName, group.value().location().getNormalizedPlaceName(), date);
                                    bigTextStyle.bigText(reminderText);
                                    bigTextStyle.setBigContentTitle(reminderTitle);
                                    builder.setContentTitle(reminderTitle)
                                            .setContentText(reminderText)
                                            .setStyle(bigTextStyle);
                                    break;
                                case NOTICE:
                                    String noticeTitle = context.getString(R.string.notification_group_meeting_notice_title);
                                    String noticeText = String.format(context.getString(R.string.notification_group_meeting_notice),
                                            groupName, group.value().location().getNormalizedPlaceName());
                                    builder.setContentText(noticeText);
                                    bigTextStyle.setBigContentTitle(noticeTitle);
                                    builder.setContentTitle(noticeTitle)
                                            .setContentText(noticeText)
                                            .setStyle(bigTextStyle);

                                    break;
                                case DEACTIVATION:
                                    String deactivationTitle = context.getString(R.string.notification_group_meeting_deactivation_title);
                                    String deactivateText = String.format(context.getString(R.string
                                                    .notification_group_meeting_deactivation),
                                            groupName);
                                    builder.setContentText(deactivateText);
                                    bigTextStyle.setBigContentTitle(deactivationTitle);
                                    builder.setContentTitle(deactivationTitle)
                                            .setContentText(deactivateText)
                                            .setStyle(bigTextStyle);
                                    break;
                                default:
                                    throw new IllegalArgumentException("Non existing enum");
                            }

                            Intent muteActivity = MuteNotificationActivity.newInstance(context, pushMessage.groupId(), GROUP, groupName, R.style
                                    .AppTheme)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);

                            PendingIntent muteBroadcast = PendingIntent.getActivity(context, pushMessage.groupId().hashCode(),
                                    muteActivity,
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                            builder.addAction(R.drawable.ic_notifications_off_white_24dp, context.getString(R.string.notification_mute_text)
                                    , muteBroadcast);

                            return ChatNotification.create(NotificationType.GROUP_MEETING_TIME, pushMessage.groupId(), ChatType.GROUP,
                                    groupName, builder.build());
                        }).toMaybe()
                );
    }

    private Single<Bitmap> partyIcon(Context context, GroupMeetingPushMessage pushMessage) {
        return Observable.fromIterable(pushMessage.photos())
                .map(photoInfo -> {
                    if (photoInfo.photoUri() != null) {
                        return photoInfo.photoUri();
                    } else {
                        return defaultValues.defaultUserPhoto(photoInfo.sourceId()).userPhotoUri;
                    }
                })
                .toList()
                .flatMap(photos -> BitmapUtils.bitmapCollage(context, photos, ChatNotification.ICON_SIZE, ChatNotification.ICON_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_group_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }
}

