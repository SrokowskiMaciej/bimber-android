package com.bimber.features.messaging.groupmeeting.di;

import com.bimber.base.BroadcastScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.messaging.groupmeeting.GroupMeetingNotificationReceiver;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

@Module
public abstract class GroupMeetingNotificationReceiverModule {

    @Binds
    @BroadcastScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(GroupMeetingNotificationReceiver receiver);
}
