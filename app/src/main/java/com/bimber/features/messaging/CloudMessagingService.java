package com.bimber.features.messaging;

import com.bimber.base.application.BimberApplication;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 12.01.17.
 */

public class CloudMessagingService extends FirebaseMessagingService {

    @Inject
    Set<CloudMessageDataHandler> cloudMessageDataHandlers;

    @Override
    public void onCreate() {
        super.onCreate();
        BimberApplication.getAppComponent().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        final Map<String, String> data = remoteMessage.getData();
        Timber.d("Message received");
        Timber.d("Data handlers: %s", cloudMessageDataHandlers.size());
        for (CloudMessageDataHandler cloudMessageDataHandler : cloudMessageDataHandlers) {
            if (data.containsKey(cloudMessageDataHandler.dataKey())) {
                Timber.d("Message handled by: %s", cloudMessageDataHandler.getClass().getSimpleName());
                cloudMessageDataHandler.handleMessageData(this, data.get(cloudMessageDataHandler.dataKey()));
            }
        }
    }
}
