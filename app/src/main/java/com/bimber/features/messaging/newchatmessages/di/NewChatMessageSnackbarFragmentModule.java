package com.bimber.features.messaging.newchatmessages.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class NewChatMessageSnackbarFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract NewChatMessageSnackbarFragment contribute();
}
