package com.bimber.features.messaging.newchatmessages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bimber.base.application.BimberApplication;
import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.entities.NewChatMessagePushMessage;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import timber.log.Timber;

/**
 * Created by maciek on 05.04.17.
 */

public class NewChatMessageCloudMessageDataHandler implements CloudMessageDataHandler {


    public static final String NEW_MESSAGE_DATA_KEY = "new_message_data_key";
    public static final String NEW_CHAT_MESSAGE_INTENT_ACTION = "NEW_CHAT_MESSAGE_INTENT_ACTION";

    @Override
    public String dataKey() {
        return NEW_MESSAGE_DATA_KEY;
    }

    @Override
    public void handleMessageData(Context context, String messageData) {
        NewChatMessagePushMessage newChatMessagePushMessage = BimberApplication.gson.fromJson(messageData, NewChatMessagePushMessage.class);
        Timber.d("New chat content: %s", newChatMessagePushMessage);
        Intent intent = new Intent(NEW_CHAT_MESSAGE_INTENT_ACTION);
        Bundle bundle = new Bundle();
        bundle.putParcelable(NEW_MESSAGE_DATA_KEY, newChatMessagePushMessage);
        intent.putExtras(bundle);
        context.sendOrderedBroadcast(intent, null);
    }
}
