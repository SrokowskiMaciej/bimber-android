package com.bimber.features.messaging.newchatmessages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.RemoteInput;
import android.widget.Toast;

import com.bimber.base.auth.BaseLoggedInBroadcastReceiver;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.NewChatMessagePushMessage;
import com.bimber.data.entities.PhotoInfo;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.text.TextContent;
import com.bimber.data.entities.chat.messages.network.MessageModel;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.emoji.EmojiUtils;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 11.04.17.
 */

public class InlineReplyNotificationReceiver extends BaseLoggedInBroadcastReceiver {

    public static final String PUSH_MESSAGE_KEY = "CHAT_ID_KEY";
    public static final String NEW_MESSAGE_TEXT_KEY = "NEW_MESSAGE_TEXT_KEY";
    public static final int SEND_MESSAGE_TIMEOUT = 10;

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    MessageRepository messageRepository;
    @Inject
    ChatMemberRepository chatMemberRepository;
    @Inject
    ProfileDataRepository profileDataRepository;
    @Inject
    NewChatMessageNotificationProvider newChatMessageNotificationProvider;

    @Override
    public void onReceiveLoggedIn(Context context, Intent intent) {
        Bundle remoteReplyExtras = RemoteInput.getResultsFromIntent(intent);
        NewChatMessagePushMessage pushMessage = intent.getParcelableExtra(PUSH_MESSAGE_KEY);
        String newMessageText = remoteReplyExtras.getString(NEW_MESSAGE_TEXT_KEY);
        Timber.d("User sent content from notification. receivedChat: %s, text to send: %s", pushMessage.chatId(), newMessageText);

        Single.defer(() -> insertNewChatMessage(pushMessage, newMessageText))
                .timeout(SEND_MESSAGE_TIMEOUT, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(newMessageId -> Timber.d("Inserted new message"))
                .doOnError(throwable -> {
                    Timber.e(throwable, "Failed to send reply form notification. Resuming with old notification. ChatId: %s",
                            pushMessage.chatId());
                    Toast.makeText(context, "Failed to send reply", Toast.LENGTH_LONG).show();
                    ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);
                })
                .onErrorResumeNext(Single.just(pushMessage))
                .flatMapMaybe(newPushMessage -> newChatMessageNotificationProvider.chatMessageNotification(context, newPushMessage, false))
                .subscribe(chatNotification -> {
                    chatNotification.showNotification(context);
                }, throwable -> {
                    Timber.e(throwable, "Failed to show chat message notification");
                }, () -> {
                    Timber.d("Chat message notification shouldn't be shown");
                });
    }

    private Single<NewChatMessagePushMessage> insertNewChatMessage(NewChatMessagePushMessage pushMessage, String messageText) {


        return Single.zip(chatMemberRepository.chatMembership(pushMessage.chatId(), currentUserId)
                .toSingle(), profileDataRepository.userProfileDataThumbnail(currentUserId), ChatMembershipWithProfileThumbnail::create)
                .flatMap(values -> {
                    MessageModel newMessage = MessageModel.create(TextContent.create(EmojiUtils.emojify(messageText)), Timestamp.now(), currentUserId,
                            values.profileData().user.displayName);
                    return messageRepository.insertMessage(values.chatMembership(), newMessage)
                            .map(insertedMessage -> appendMessage(pushMessage, insertedMessage, values.profileData()));
                });
    }

    private NewChatMessagePushMessage appendMessage(NewChatMessagePushMessage pushMessage, String newMessageId, ProfileDataThumbnail
            profileData) {

        List<NewChatMessagePushMessage.MessageInfo> updatedMessages = pushMessage.messages();
        updatedMessages.add(NewChatMessagePushMessage.MessageInfo.create(newMessageId, profileData.user.displayName,
                System.currentTimeMillis()));

        List<PhotoInfo> updatedPhotos = Observable.just(pushMessage)
                .flatMap(newChatMessagePushMessage -> {
                    switch (newChatMessagePushMessage.chatType()) {
                        case DIALOG:
                            return Observable.fromIterable(pushMessage.photos());
                        case GROUP:
                            return Observable.fromIterable(pushMessage.photos())
                                    .startWith(PhotoInfo.create(profileData.user.uId, profileData.photo.userPhotoUri))
                                    .distinct(PhotoInfo::sourceId);
                        default:
                            throw new IllegalArgumentException("No such chat type");
                    }
                })
                .toList()
                .blockingGet();

        return NewChatMessagePushMessage.create(pushMessage.chatId(), pushMessage.chatName(),
                pushMessage.chatType(), updatedPhotos, updatedMessages, newMessageId, ContentType.TEXT,
                profileData.user.uId);
    }

    @Override
    public void onReceiveLoggedOut(Context context, Intent intent) {
        Timber.e("Notification inline reply received when user was logged out");

    }

    @AutoValue
    public static abstract class ChatMembershipWithProfileThumbnail {
        public abstract ChatVisibleChatMembership chatMembership();

        public abstract ProfileDataThumbnail profileData();

        public static ChatMembershipWithProfileThumbnail create(ChatVisibleChatMembership chatMembership, ProfileDataThumbnail
                profileData) {
            return new AutoValue_InlineReplyNotificationReceiver_ChatMembershipWithProfileThumbnail(chatMembership, profileData);
        }
    }
}
