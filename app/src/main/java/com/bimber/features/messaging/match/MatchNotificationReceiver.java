package com.bimber.features.messaging.match;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInBroadcastReceiver;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.MatchPushMessage;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.messaging.ChatNotification;
import com.bimber.features.messaging.NotificationType;
import com.bimber.utils.images.BitmapUtils;

import javax.inject.Inject;

import io.reactivex.Single;
import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */

public class MatchNotificationReceiver extends BaseLoggedInBroadcastReceiver {

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;

    @Override
    public void onReceiveLoggedIn(Context context, Intent intent) {
        MatchPushMessage matchPushMessage = intent.getParcelableExtra(MatchCloudMessageDataHandler.MATCH_DATA_KEY);
        Single.defer(() -> matchNotification(context, matchPushMessage))
                .subscribe(chatNotification -> chatNotification.showNotification(context),
                        throwable -> Timber.e(throwable, "Error while trying to show match notification to user"));
    }

    @Override
    public void onReceiveLoggedOut(Context context, Intent intent) {

    }


    private Single<ChatNotification> matchNotification(Context context, MatchPushMessage pushMessage) {
        String matchName;
        String matchPhoto;
        if (currentUserId.equals(pushMessage.likingUserId())) {
            matchName = pushMessage.likedUserName() != null ? pushMessage.likedUserName() :
                    defaultValues.defaultUser(pushMessage.likedUserId()).displayName;
            matchPhoto = pushMessage.likedUserName() != null ? pushMessage.likedUserPhoto() :
                    defaultValues.defaultUserPhoto(pushMessage.likedUserId()).userPhotoUri;
        } else if (currentUserId.equals(pushMessage.likedUserId())) {
            matchName = pushMessage.likingUserName() != null ? pushMessage.likingUserName() :
                    defaultValues.defaultUser(pushMessage.likingUserId()).displayName;
            matchPhoto = pushMessage.likingUserPhoto() != null ? pushMessage.likingUserPhoto() :
                    defaultValues.defaultUserPhoto(pushMessage.likingUserId()).userPhotoUri;
        } else {
            return Single.error(new IllegalArgumentException("Receiver is neither liking nor liked user from this notification"));
        }

        return dialogIcon(context, matchPhoto)
                .map(matchBitmap -> {
                    Intent mainScreenIntent = HomeActivity.newInstance(context, HomeNavigator.HomeScreen.CHATS);
                    Intent chatIntent = ChatActivity.newInstance(context, pushMessage.dialogId(), ChatType.DIALOG);
                    PendingIntent broadcast = TaskStackBuilder.create(context)
                            .addNextIntent(mainScreenIntent)
                            .addNextIntent(chatIntent)
                            .getPendingIntent(NotificationType.MATCH.getNotificationId(pushMessage.dialogId()),
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                    Notification notification = new NotificationCompat.Builder(context)
                            .setContentTitle(context.getString(R.string.notification_match_title_text))
                            .setSmallIcon(R.drawable.ic_bimber_notification)
                            .setLargeIcon(matchBitmap)
                            .setGroup(pushMessage.dialogId())
                            .setSortKey("000")
                            .setPriority(Notification.PRIORITY_DEFAULT)
                            .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            .setCategory(NotificationCompat.CATEGORY_SOCIAL)
                            .setContentText(String.format(context.getString(R.string.notification_match_content_text),
                                    matchName))
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setContentIntent(broadcast)
                            .setAutoCancel(true)
                            .setStyle(new BigTextStyle().bigText(String.format(context.getString(R.string.notification_match_content_text),
                                    matchName)))
                            .build();
                    return ChatNotification.create(NotificationType.MATCH, pushMessage.dialogId(), ChatType.DIALOG,
                            matchName, notification);
                });
    }

    private Single<Bitmap> dialogIcon(Context context, String photoUri) {
        return Single.just(photoUri)
                .flatMap(photo -> BitmapUtils.bitmap(context, photo, ChatNotification.ICON_SIZE, ChatNotification.ICON_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(context, R.drawable.ic_person_white_24dp, ChatNotification.ICON_SIZE,
                        ChatNotification.ICON_SIZE));
    }
}
