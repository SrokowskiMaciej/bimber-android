package com.bimber.features.messaging;

import android.content.Context;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

public interface CloudMessageDataHandler {

    String dataKey();

    void handleMessageData(Context context, String messageData);

    enum HandlingPriority {
        //This is equal to R.integer.broadcat_handling_order_extra_hight
        EXTRA_HIGH(4),
        //This is equal to R.integer.broadcat_handling_order_hight
        HIGH(3),
        //This is equal to R.integer.broadcat_handling_order_medium
        MEDIUM(2),
        //This is equal to R.integer.broadcat_handling_order_low
        LOW(1);
        public final int order;

        HandlingPriority(int order) {
            this.order = order;
        }
    }
}
