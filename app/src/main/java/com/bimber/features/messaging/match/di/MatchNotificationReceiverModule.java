package com.bimber.features.messaging.match.di;

import com.bimber.base.BroadcastScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.messaging.match.MatchNotificationReceiver;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 20.01.17.
 */

@Module
public abstract class MatchNotificationReceiverModule {
    @Binds
    @BroadcastScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(MatchNotificationReceiver receiver);

}
