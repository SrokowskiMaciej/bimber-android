package com.bimber.features.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.utils.rx.RxFirebaseUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

/**
 * Created by maciek on 22.04.17.
 */

public class ErrorFragment extends Fragment {


    public static final String ERROR_DATA_KEY = "ERROR_DATA_KEY";

    @BindView(R.id.errorView)
    ErrorView errorView;

    public static ErrorFragment newInstance() {
        ErrorData errorData = ErrorData.create(R.string.view_error_title_default_text,
                R.string.view_error_content_fault_text,
                R.string.view_error_action_try_again_text);
        return newInstance(errorData);
    }

    public static ErrorFragment newInstance(ErrorData errorData) {
        Bundle args = new Bundle();
        args.putParcelable(ERROR_DATA_KEY, errorData);
        ErrorFragment fragment = new ErrorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_error, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ErrorData errorData = getArguments().getParcelable(ERROR_DATA_KEY);
        errorView.setErrorTitle(errorData.errorTitle());
        errorView.setErrorContent(errorData.errorContent());
        errorView.setErrorButtonText(errorData.errorActionText());
    }

    public Observable<RxFirebaseUtils.Event> onErrorAction() {
        return errorView.onErrorAction();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
