package com.bimber.features.common;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.utils.rx.RxFirebaseUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 22.04.17.
 */

public class ErrorView extends ConstraintLayout {


    @BindView(R.id.textViewErrorTitle)
    TextView textViewErrorTitle;
    @BindView(R.id.textViewErrorContent)
    TextView textViewErrorContent;
    @BindView(R.id.buttonErrorAction)
    AppCompatButton buttonErrorAction;

    private PublishSubject<RxFirebaseUtils.Event> errorActionPublishSubject = PublishSubject.create();

    public ErrorView(Context context) {
        this(context, null);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.view_error, this, true);
        ButterKnife.bind(view);
    }


    @OnClick(R.id.buttonErrorAction)
    public void onViewClicked() {
        errorActionPublishSubject.onNext(RxFirebaseUtils.Event.INSTANCE);
    }

    public void setErrorTitle(String errorTitle) {
        textViewErrorTitle.setText(errorTitle);
    }

    public void setErrorTitle(@StringRes int errorTitle) {
        textViewErrorTitle.setText(errorTitle);
    }

    public void setErrorContent(String errorConetent) {
        textViewErrorContent.setText(errorConetent);
    }

    public void setErrorContent(@StringRes int errorContent) {
        textViewErrorContent.setText(errorContent);
    }

    public void setErrorButtonText(String errorButtonText) {
        buttonErrorAction.setText(errorButtonText);
    }

    public void setErrorButtonText(@StringRes int errorButtonText) {
        buttonErrorAction.setText(errorButtonText);
    }

    public Observable<RxFirebaseUtils.Event> onErrorAction() {
        return errorActionPublishSubject;
    }
}
