package com.bimber.features.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.bimber.R;

import butterknife.ButterKnife;

/**
 * Created by maciek on 03.05.17.
 */

public class ProgressView extends FrameLayout {
    public ProgressView(@NonNull Context context) {
        super(context);
    }

    public ProgressView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.view_progress, this, true);
        ButterKnife.bind(view);
        setBackgroundColor(ContextCompat.getColor(context, R.color.colorTranslucentBackgroundLight));
    }
}
