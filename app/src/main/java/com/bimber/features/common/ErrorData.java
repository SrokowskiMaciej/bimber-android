package com.bimber.features.common;

import android.os.Parcelable;
import android.support.annotation.StringRes;

import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 23.04.17.
 */
@AutoValue
public abstract class ErrorData implements Parcelable {

    @StringRes
    public abstract int errorTitle();

    @StringRes
    public abstract int errorContent();

    @StringRes
    public abstract int errorActionText();

    public static ErrorData create(@StringRes int errorTitle, @StringRes int errorContent, @StringRes int errorActionText) {
        return new AutoValue_ErrorData(errorTitle, errorContent, errorActionText);
    }
}
