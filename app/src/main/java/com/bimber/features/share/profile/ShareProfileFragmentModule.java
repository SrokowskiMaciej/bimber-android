package com.bimber.features.share.profile;

import com.bimber.base.profile.ProfileProvider;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class ShareProfileFragmentModule {
    @Binds
    abstract ShareProfileContract.IShareProfilePresenter presenter(ShareProfilePresenter presenter);

    @Binds
    abstract ProfileProvider profileProvider(ShareProfileView view);
}
