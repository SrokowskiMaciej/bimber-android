package com.bimber.features.share.group;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.ContextThemeWrapper;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.group.GroupProvider;
import com.bimber.features.share.group.ShareGroupContract.IShareGroupPresenter;
import com.bimber.features.share.group.ShareGroupContract.IShareGroupView;

import javax.inject.Inject;

/**
 * Created by maciek on 23.05.17.
 */

public class ShareGroupView extends BaseFragment implements IShareGroupView, GroupProvider {

    public static final String GROUP_ID_KEY = "GROUP_ID_KEY";
    private static final String THEME_KEY = "THEME_KEY";
    @Inject
    IShareGroupPresenter shareGroupPresenter;

    private MaterialDialog progressDialog;
    private MaterialDialog errorDialog;
    private String groupId;

    public static ShareGroupView newInstance(String groupId, @StyleRes int theme) {
        ShareGroupView shareGroupView = new ShareGroupView();
        Bundle bundle = new Bundle();
        bundle.putString(GROUP_ID_KEY, groupId);
        bundle.putInt(THEME_KEY, theme);
        shareGroupView.setArguments(bundle);
        return shareGroupView;
    }

    @Override
    public void onAttach(Context context) {
        groupId = getArguments().getString(GROUP_ID_KEY);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareGroupPresenter.bindView(this);
    }

    @Override
    public void onDestroy() {
        shareGroupPresenter.unbindView(this);
        super.onDestroy();
    }

    @Override
    public void shareDeeplink(String link, String groupName) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String deeplinkText = String.format(getString(R.string.share_party_text), groupName) + "\n" + link;
        sendIntent.putExtra(Intent.EXTRA_TEXT, deeplinkText);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_action_title)));
    }

    @Override
    public void showProgress(boolean show) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (show) {
            progressDialog = new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), getArguments().getInt(THEME_KEY)))
                    .content(R.string.share_party_loading_text)
                    .progress(true, 0)
                    .show();
        }
    }

    @Override
    public void showError(boolean show) {
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.dismiss();
        }
        if (show) {
            errorDialog = new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), getArguments().getInt(THEME_KEY)))
                    .title(R.string.view_error_title_default_text)
                    .content(R.string.share_party_failure)
                    .neutralText(R.string.dialog_ok_button_text)
                    .show();
        }
    }

    @Override
    public void dismiss() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public String provideGroupId() {
        return groupId;
    }
}
