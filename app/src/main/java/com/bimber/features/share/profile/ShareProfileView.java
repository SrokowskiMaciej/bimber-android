package com.bimber.features.share.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.ContextThemeWrapper;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.profile.ProfileProvider;

import javax.inject.Inject;

import static com.bimber.features.share.profile.ShareProfileContract.IShareProfilePresenter;
import static com.bimber.features.share.profile.ShareProfileContract.IShareProfileView;

/**
 * Created by maciek on 23.05.17.
 */

public class ShareProfileView extends BaseFragment implements IShareProfileView, ProfileProvider {

    public static final String PROFILE_ID_KEY = "GROUP_ID_KEY";
    private static final String THEME_KEY = "THEME_KEY";
    @Inject
    IShareProfilePresenter shareProfilePresenter;

    private MaterialDialog progressDialog;
    private MaterialDialog errorDialog;
    private String profileId;

    public static ShareProfileView newInstance(String profileId, @StyleRes int theme) {
        ShareProfileView shareProfileView = new ShareProfileView();
        Bundle bundle = new Bundle();
        bundle.putString(PROFILE_ID_KEY, profileId);
        bundle.putInt(THEME_KEY, theme);
        shareProfileView.setArguments(bundle);
        return shareProfileView;
    }

    @Override
    public void onAttach(Context context) {
        profileId = getArguments().getString(PROFILE_ID_KEY);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareProfilePresenter.bindView(this);
    }

    @Override
    public void onDestroy() {
        shareProfilePresenter.unbindView(this);
        super.onDestroy();
    }

    @Override
    public void shareDeeplink(String link, String usersName) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String deeplinkText = String.format(getString(R.string.share_profile_text), usersName) + "\n" + link;
        sendIntent.putExtra(Intent.EXTRA_TEXT, deeplinkText);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.share_action_title)));
    }

    @Override
    public void showProgress(boolean show) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (show) {
            progressDialog = new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), getArguments().getInt(THEME_KEY)))
                    .content(R.string.share_profile_loading_text)
                    .progress(true, 0)
                    .show();
        }
    }

    @Override
    public void showError(boolean show) {
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.dismiss();
        }
        if (show) {
            errorDialog = new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), getArguments().getInt(THEME_KEY)))
                    .title(R.string.view_error_title_default_text)
                    .content(R.string.share_profile_failure)
                    .neutralText(R.string.dialog_ok_button_text)
                    .show();
        }
    }

    @Override
    public void dismiss() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public String provideProfileId() {
        return profileId;
    }
}
