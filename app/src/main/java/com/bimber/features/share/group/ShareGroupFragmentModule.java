package com.bimber.features.share.group;

import com.bimber.base.group.GroupProvider;

import dagger.Binds;
import dagger.Module;

import static com.bimber.features.share.group.ShareGroupContract.IShareGroupPresenter;

/**
 * Created by maciek on 23.05.17.
 */

@Module
public abstract class ShareGroupFragmentModule {
    @Binds
    abstract IShareGroupPresenter presenter(ShareGroupPresenter presenter);

    @Binds
    abstract GroupProvider groupProvider(ShareGroupView view);
}
