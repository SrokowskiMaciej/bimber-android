package com.bimber.features.share.group;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 23.03.17.
 */

public interface ShareGroupContract {

    interface IShareGroupView {
        void shareDeeplink(String link, String groupName);

        void showProgress(boolean show);

        void showError(boolean show);

        void dismiss();
    }

    interface IShareGroupPresenter extends MvpPresenter<IShareGroupView> {
    }
}
