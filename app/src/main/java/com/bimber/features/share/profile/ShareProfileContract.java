package com.bimber.features.share.profile;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 23.03.17.
 */

public interface ShareProfileContract {

    interface IShareProfileView {
        void shareDeeplink(String link, String usersName);

        void showProgress(boolean show);

        void showError(boolean show);

        void dismiss();
    }

    interface IShareProfilePresenter extends MvpPresenter<IShareProfileView> {
    }
}
