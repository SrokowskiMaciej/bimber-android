package com.bimber.features.share.profile;

import com.bimber.base.profile.ProfileId;
import com.bimber.data.ServerValues;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.deeplinking.DeeplinkShortenerService;
import com.bimber.data.deeplinking.ProfileDeeplinkRequestFactory;
import com.bimber.data.deeplinking.model.ShortDeeplinkResponse;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.share.profile.ShareProfileContract.IShareProfileView;
import com.bimber.utils.mvp.MvpAbstractPresenter;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ShareEvent;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.bimber.data.analytics.BimberAnalytics.ShareContentType.PERSON;
import static com.bimber.data.analytics.BimberAnalytics.ShareMethod.DEEPLINK;
import static com.bimber.features.share.profile.ShareProfileContract.IShareProfilePresenter;

/**
 * Created by maciek on 23.05.17.
 */

public class ShareProfilePresenter extends MvpAbstractPresenter<IShareProfileView> implements IShareProfilePresenter {


    private final String profileId;
    private final ProfileDataRepository profileDataRepository;
    private final ProfileDeeplinkRequestFactory profileDeeplinkRequestFactory;
    private final DeeplinkShortenerService deeplinkShortenerService;
    private final ServerValues serverValues;
    private final BimberAnalytics bimberAnalytics;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ShareProfilePresenter(@ProfileId String profileId, ProfileDataRepository profileDataRepository, ProfileDeeplinkRequestFactory
            profileDeeplinkRequestFactory, DeeplinkShortenerService
                                         deeplinkShortenerService, ServerValues serverValues, BimberAnalytics bimberAnalytics) {
        this.profileId = profileId;
        this.profileDataRepository = profileDataRepository;
        this.profileDeeplinkRequestFactory = profileDeeplinkRequestFactory;
        this.deeplinkShortenerService = deeplinkShortenerService;
        this.serverValues = serverValues;
        this.bimberAnalytics = bimberAnalytics;
    }

    @Override
    protected void viewAttached(IShareProfileView view) {
        subscriptions.add(profileDataRepository.userProfileDataThumbnail(profileId)
                .doOnSubscribe(disposable -> view.showProgress(true))
                .flatMap(profileDataThumbnail -> deeplinkShortenerService.shortenDeeplink(profileDeeplinkRequestFactory
                                .buildDeeplinkRequest(profileDataThumbnail),
                        serverValues.getFirebaseWebApiToken())
                        .map(ShortDeeplinkResponse::shortLink)
                        .subscribeOn(Schedulers.io())
                        .doOnSuccess(deeplink -> {
                            bimberAnalytics.logShare(profileId, profileDataThumbnail.user.fullName, PERSON, DEEPLINK);
                            view.showProgress(false);
                            view.shareDeeplink(deeplink, profileDataThumbnail.user.displayName);
                            view.dismiss();
                        })
                        .doOnError(throwable -> {
                            view.showProgress(false);
                            view.showError(true);
                            view.dismiss();

                        }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deeplink -> Timber.d("Shared profile"), throwable -> Timber.e(throwable, "Couldn't get deeplink to profile")));


    }

    @Override
    protected void viewDetached(IShareProfileView view) {
        subscriptions.clear();
    }

}
