package com.bimber.features.share.group;

import com.bimber.base.group.GroupId;
import com.bimber.data.ServerValues;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.deeplinking.DeeplinkShortenerService;
import com.bimber.data.deeplinking.GroupDeeplinkRequestFactory;
import com.bimber.data.deeplinking.model.ShortDeeplinkResponse;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.features.share.group.ShareGroupContract.IShareGroupPresenter;
import com.bimber.features.share.group.ShareGroupContract.IShareGroupView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.bimber.data.analytics.BimberAnalytics.ShareContentType.PARTY;
import static com.bimber.data.analytics.BimberAnalytics.ShareMethod.DEEPLINK;

/**
 * Created by maciek on 23.05.17.
 */

public class ShareGroupPresenter extends MvpAbstractPresenter<IShareGroupView> implements IShareGroupPresenter {


    public static final String PROFILE_DEEPLINK = "profile";

    private final String groupId;
    private final GroupRepository groupRepository;
    private final GroupDeeplinkRequestFactory groupDeeplinkRequestFactory;
    private final DeeplinkShortenerService deeplinkShortenerService;
    private final ServerValues serverValues;
    private final BimberAnalytics bimberAnalytics;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ShareGroupPresenter(@GroupId String groupId, GroupRepository groupRepository, GroupDeeplinkRequestFactory
            groupDeeplinkRequestFactory, DeeplinkShortenerService
                                       deeplinkShortenerService, ServerValues serverValues, BimberAnalytics bimberAnalytics) {
        this.groupId = groupId;
        this.groupRepository = groupRepository;
        this.groupDeeplinkRequestFactory = groupDeeplinkRequestFactory;
        this.deeplinkShortenerService = deeplinkShortenerService;
        this.serverValues = serverValues;
        this.bimberAnalytics = bimberAnalytics;
    }

    @Override
    protected void viewAttached(IShareGroupView view) {
        subscriptions.add(groupRepository.group(groupId)
                .toSingle()
                .doOnSubscribe(disposable -> view.showProgress(true))
                .flatMap(group -> deeplinkShortenerService.shortenDeeplink(groupDeeplinkRequestFactory.buildDeeplinkRequest(group),
                        serverValues.getFirebaseWebApiToken())
                        .map(ShortDeeplinkResponse::shortLink)
                        .subscribeOn(Schedulers.io())
                        .doOnSuccess(deeplink -> {
                            bimberAnalytics.logShare(groupId, group.value().name(), PARTY, DEEPLINK);
                            view.showProgress(false);
                            view.shareDeeplink(deeplink, group.value().name());
                            view.dismiss();
                        })
                        .doOnError(throwable -> {
                            view.showProgress(false);
                            view.showError(true);
                            view.dismiss();
                        }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deeplink -> Timber.d("Shared group"), throwable -> Timber.e(throwable, "Couldn't get deeplink to group")));


    }

    @Override
    protected void viewDetached(IShareGroupView view) {
        subscriptions.clear();
    }

}
