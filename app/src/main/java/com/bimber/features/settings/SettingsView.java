package com.bimber.features.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.BuildConfig;
import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.features.location.activity.LocationPickerActivity;
import com.bimber.features.settings.SettingsContract.ISettingsPresenter;
import com.bimber.features.settings.SettingsContract.ISettingsView;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.FirebaseDatabase;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * Created by srokowski.maciej@gmail.com on 30.10.16.
 */

public class SettingsView extends BaseFragment implements ISettingsView {

    private static final int RC_SIGN_IN = 100;

    @Inject
    ISettingsPresenter settingsPresenter;
    @Inject
    FirebaseDatabase firebaseDatabase;
    @Inject
    @LoggedInUserId
    String currentUserId;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_settings, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        SettingsButtonsAdapter settingsButtonsAdapter = new SettingsButtonsAdapter();
        subscriptions.add(settingsButtonsAdapter.settingsActionCLicked().subscribe(settingsAction -> {
            switch (settingsAction) {
                case LOCATION:
                    startActivity(LocationPickerActivity.newInstance(getContext(), R.style.AppThemeLight, R.style.AppTheme, false));
                    break;
                case ABOUT:
                    new LibsBuilder()
                            .withActivityStyle(Libs.ActivityStyle.LIGHT)
                            .withAboutIconShown(true)
                            .withAboutAppName(getString(R.string.app_name))
                            .withVersionShown(true)
                            .withAboutVersionString(BuildConfig.VERSION_NAME)
                            .withAboutDescription(getString(R.string.view_settings_about_content))
                            .start(getActivity());
                    break;
                case TOS:
                    Intent tosIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bimber.fun/terms_of_service.html"));
                    startActivity(tosIntent);
                    break;
                case PRIVACY_POLICY:
                    Intent privacyPolicyIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bimber.fun/privacy_policy.html"));
                    startActivity(privacyPolicyIntent);
                    break;
                case LOG_OUT:
                    firebaseDatabase.goOffline();
                    new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme))
                            .title(R.string.view_settings_logout_prompr_title)
                            .content(R.string.view_settings_logout_prompt_content)
                            .positiveText(R.string.dialog_ok_button_text)
                            .onPositive((dialog, which) -> {
                                //TODO Handle this properly
                                AuthUI.getInstance().signOut(getActivity()).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Timber.e(e, "Failed to sign out");
                                    }
                                });
                            })
                            .negativeText(R.string.dialog_cancel_button_text)
                            .onNegative((dialog, which) -> {
                            })
                            .show();
                    break;
                case DELETE_ACCOUNT:
                    firebaseDatabase.goOffline();
                    new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme))
                            .title(R.string.view_settings_delete_account_prompt_title)
                            .content(R.string.view_settings_delete_account_prompt_content)
                            .positiveText(R.string.dialog_ok_button_text)
                            .onPositive((dialog, which) -> {
                                //TODO Handle this properly
                                AuthUI.getInstance().delete(getActivity()).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        handleAccountDeletionFailure();
                                    }
                                });
                            })
                            .negativeText(R.string.dialog_cancel_button_text)
                            .onNegative((dialog, which) -> {
                            })
                            .show();

                    break;
            }
        }));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable
                .shape_list_divider), true));
        recyclerView.setAdapter(settingsButtonsAdapter);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingsPresenter.bindView(this);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        settingsPresenter.unbindView(this);
        super.onDestroyView();
    }

    private void handleAccountDeletionFailure() {
        new MaterialDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AppTheme))
                .title(R.string.view_settings_reauth_prompt_title)
                .content(R.string.view_settings_reauth_prompt_content)
                .onPositive((dialog1, which1) -> {
                    Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                            .setTheme(R.style.AppTheme)
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                    new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()))
                            .setIsSmartLockEnabled(false)
                            .build();
                    startActivityForResult(intent, RC_SIGN_IN);
                })
                .positiveText(R.string.dialog_ok_button_text)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                AuthUI.getInstance().delete(getActivity());
                return;
            }
            new MaterialDialog.Builder(getContext())
                    .title(R.string.view_error_title_default_text)
                    .content(R.string.view_error_content_error_flow_text)
                    .neutralText(R.string.view_error_action_ok_text)
                    .show();
        }
    }
}
