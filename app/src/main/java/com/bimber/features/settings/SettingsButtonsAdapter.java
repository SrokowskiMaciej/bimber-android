package com.bimber.features.settings;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 25.05.17.
 */

public class SettingsButtonsAdapter extends RecyclerView.Adapter<SettingsButtonsAdapter.SettingsButtonViewHolder> {

    private PublishSubject<SettingsAction> settingsActionPublishSubject = PublishSubject.create();


    @Override
    public SettingsButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SettingsButtonViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_settings_button, parent, false));
    }

    @Override
    public void onBindViewHolder(SettingsButtonViewHolder holder, int position) {
        switch (position) {
            case 0:
                holder.populate(SettingsAction.LOCATION);
                break;
            case 1:
                holder.populate(SettingsAction.ABOUT);
                break;
            case 2:
                holder.populate(SettingsAction.TOS);
                break;
            case 3:
                holder.populate(SettingsAction.PRIVACY_POLICY);
                break;
            case 4:
                holder.populate(SettingsAction.LOG_OUT);
                break;
            case 5:
                holder.populate(SettingsAction.DELETE_ACCOUNT);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return SettingsAction.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public PublishSubject<SettingsAction> settingsActionCLicked() {
        return settingsActionPublishSubject;
    }

    public class SettingsButtonViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewAction)
        ImageView imageViewAction;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;
        @BindView(R.id.textViewSubtitle)
        TextView textViewSubtitle;

        private SettingsAction action;

        public SettingsButtonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void populate(SettingsAction settingsAction) {
            action = settingsAction;
            imageViewAction.setImageResource(settingsAction.icon);
            textViewTitle.setText(settingsAction.title);
            textViewSubtitle.setText(settingsAction.subtitle);
        }

        @OnClick(R.id.rootView)
        public void onViewClicked() {
            settingsActionPublishSubject.onNext(action);
        }
    }

    public enum SettingsAction {
        LOCATION(R.drawable.ic_location_on_white_24dp, R.string.view_settings_location_button_title, R.string
                .view_settings_location_button_subtitle),
        ABOUT(R.drawable.ic_info_white_24dp, R.string.view_settings_about_button_title, R.string
                .view_settings_about_button_subtitle),
        TOS(R.drawable.ic_event_note_white_24dp, R.string.view_settings_tos_button_title, R.string.view_settings_tos_button_subtitle),
        PRIVACY_POLICY(R.drawable.ic_verified_user_white_24dp, R.string.view_settings_privacy_policy_button_title,
                R.string.view_settings_privacy_policy_button_subtitle),
        LOG_OUT(R.drawable.ic_exit_to_app_white_24dp, R.string.view_settings_log_out_button_title, R.string
                .view_settings_log_out_button_subtitle),
        DELETE_ACCOUNT(R.drawable.ic_delete_white_24dp, R.string.view_settings_delete_account_button_title, R.string
                .view_settings_delete_account_button_subtitle);


        @DrawableRes
        private final int icon;

        @StringRes
        private final int title;

        @StringRes
        private final int subtitle;

        SettingsAction(int icon, int title, int subtitle) {
            this.icon = icon;
            this.title = title;
            this.subtitle = subtitle;
        }
    }
}
