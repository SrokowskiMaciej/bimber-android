package com.bimber.features.settings;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

public interface SettingsContract {
    interface ISettingsView {

    }

    interface ISettingsPresenter extends MvpPresenter<ISettingsView> {
        void logOut();

        void deleteAccount();
    }
}
