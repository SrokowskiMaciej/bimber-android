package com.bimber.features.settings;

import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.features.settings.SettingsContract.ISettingsView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import static com.bimber.features.settings.SettingsContract.ISettingsPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 30.10.16.
 */

public class SettingsPresenter extends MvpAbstractPresenter<ISettingsView> implements ISettingsPresenter {


    private final FirebaseAuthService firebaseAuthService;

    @Inject
    public SettingsPresenter(FirebaseAuthService firebaseAuthService) {
        this.firebaseAuthService = firebaseAuthService;
    }

    @Override
    protected void viewAttached(ISettingsView view) {

    }

    @Override
    protected void viewDetached(ISettingsView view) {

    }

    @Override
    public void logOut() {
    }

    @Override
    public void deleteAccount() {

    }
}
