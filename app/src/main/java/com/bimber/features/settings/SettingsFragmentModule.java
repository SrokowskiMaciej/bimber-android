package com.bimber.features.settings;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.bimber.features.settings.SettingsContract.ISettingsPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 30.10.16.
 */

@Module
public abstract class SettingsFragmentModule {

    @Binds
    public abstract ISettingsPresenter provideSettingsPresenter(SettingsPresenter settingsPresenter);

    @ContributesAndroidInjector
    abstract SettingsView contributeYourActivityInjector();
}
