package com.bimber.features.explore;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bimber.features.discovergroups.DiscoverGroupsView;
import com.bimber.features.discoverpeople.DiscoverPeopleView;

/**
 * Created by maciek on 12.05.17.
 */

public class ExplorePagerAdapter extends FragmentPagerAdapter {
    public ExplorePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DiscoverPeopleView();
            case 1:
                return new DiscoverGroupsView();
            default:
                throw new Error("We shouldn't get here");
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
