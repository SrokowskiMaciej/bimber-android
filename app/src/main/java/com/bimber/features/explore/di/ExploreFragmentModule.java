package com.bimber.features.explore.di;

import com.bimber.features.explore.ExploreView;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 26.10.16.
 */


@Module
public abstract class ExploreFragmentModule {

    @ContributesAndroidInjector(modules = {/*There should be */})
    abstract ExploreView contributeExploreView();

}
