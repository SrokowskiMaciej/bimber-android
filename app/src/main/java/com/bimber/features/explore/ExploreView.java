package com.bimber.features.explore;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.application.BimberApplication;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.features.deeplinking.DeeplinkNavigator;
import com.bimber.features.deeplinking.DeeplinkNavigator.Deeplink;
import com.bimber.features.home.ExploreButtonsProvider;
import com.bimber.features.home.ExploreButtonsProvider.ExploreButton;
import com.bimber.features.home.ExploreButtonsProvider.ExploreButtonConsumer;
import com.rahimlis.badgedtablayout.BadgedTabLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by srokowski.maciej@gmail.com on 16.11.16.
 */

public class ExploreView extends BaseFragment implements ExploreButtonConsumer {


    @BindView(R.id.toolbar)
    BadgedTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;

    @Inject
    DeeplinkNavigator deeplinkNavigator;
    @Inject
    ExploreButtonsProvider exploreButtonsProvider;

    private ExplorePagerAdapter adapter;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_explore, container, false);
        ButterKnife.bind(this, view);
        apply(tabLayout);
        adapter = new ExplorePagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
//        tabLayout.setupWithViewPager(viewPager);
        //Those two lines are here because setupWithViewPage removes my icons
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                exploreButtonsProvider.validateExploreButtons();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        return view;
    }

    protected void apply(ViewGroup vg) {
        Typeface type = BimberApplication.getDefaultTypeface();

        for (int i = 0; i < vg.getChildCount(); ++i) {
            View v = vg.getChildAt(i);
            if (v instanceof TextView)
                ((TextView) v).setTypeface(type);
            else if (v instanceof ViewGroup)
                apply((ViewGroup) v);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        subscriptions.add(deeplinkNavigator.deeplink()
                .firstElement()
                .subscribe(deeplink -> {
                    switch (deeplink) {
                        case PROFILE:
                            viewPager.setCurrentItem(0);
                            deeplinkNavigator.pushDeeplink(Deeplink.NONE);
                            break;
                        case GROUP:
                            viewPager.setCurrentItem(1);
                            deeplinkNavigator.pushDeeplink(Deeplink.NONE);
                            break;
                        default:
                    }
                }));
    }

    @Override
    public void onPause() {
        subscriptions.clear();
        super.onPause();
    }

    @Override
    public boolean shouldExploreButtonBeVisible(ExploreButton exploreButton) {
        if (viewPager != null && adapter != null) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag(getViewPagerFragment(viewPager, viewPager.getCurrentItem()));
            if (fragment == null) {
                return false;
            }
            if (exploreButton == ExploreButton.REVERT && viewPager.getCurrentItem() != 0) {
                return false;
            }
            if (exploreButton == ExploreButton.SKIP && viewPager.getCurrentItem() != 1) {
                return false;
            }
            return ((ExploreButtonConsumer) fragment).shouldExploreButtonBeVisible(exploreButton);
        }
        return false;
    }

    @Override
    public void onClickExploreButton(ExploreButton exploreButton, View button) {
        if (viewPager != null && adapter != null) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag(getViewPagerFragment(viewPager, viewPager.getCurrentItem()));
            if (fragment != null) {
                ((ExploreButtonConsumer) fragment).onClickExploreButton(exploreButton, button);
            }
        }
    }

    public void setBadgeGroupCount(int count) {
        if (tabLayout != null) {
            if (count > 0) {
                tabLayout.setBadgeText(1, String.valueOf(count));
            } else {
                tabLayout.setBadgeText(1, null);
            }

        }
    }

    private String getViewPagerFragment(ViewPager viewpager, int position) {
        return "android:switcher:" + String.valueOf(viewpager.getId()) + ":" + String.valueOf(position);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
