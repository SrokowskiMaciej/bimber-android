package com.bimber.features.launch;

import com.bimber.base.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 20.04.17.
 */
@Module
public abstract class LauncherActivityModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract LauncherActivity contributeYourActivityInjector();
}
