package com.bimber.features.launch;

import android.os.Bundle;

import com.bimber.R;
import com.bimber.base.activities.BaseActivity;
import com.bimber.features.home.HomeNavigator.HomeScreen;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.auth.FirebaseAuth;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;


public class LauncherActivity extends BaseActivity {

    public static final int ENTER_ANIMATION_TIMEOUT = 4;
    @Inject
    FirebaseAuth firebaseAuth;

    private PublishSubject<RxFirebaseUtils.Event> animationCompleteSubject = PublishSubject.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        firebaseAuth.signOut();

        //On some devices onEnterAnimationComplete is not called
        Single<RxFirebaseUtils.Event> delayedExecution = Single.just(RxFirebaseUtils.Event.INSTANCE)
                .delay(ENTER_ANIMATION_TIMEOUT, TimeUnit.SECONDS);
        Observable.merge(animationCompleteSubject, delayedExecution.toObservable())
                .firstOrError()
                .subscribe(event -> {
                    startActivity(HomeActivity.newInstance(this, HomeScreen.EXPLORE));
                    overridePendingTransition(0, R.anim.fade_out_splash_screen);
                });
    }

    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        animationCompleteSubject.onNext(RxFirebaseUtils.Event.INSTANCE);
    }
}
