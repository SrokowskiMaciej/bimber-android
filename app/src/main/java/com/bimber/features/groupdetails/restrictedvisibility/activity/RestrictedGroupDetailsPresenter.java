package com.bimber.features.groupdetails.restrictedvisibility.activity;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.chat.membership.ChatMembership;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.chats.group.GetGroupChatDataUseCase;
import com.bimber.domain.model.GroupDataWithEvaluations;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsPresenter;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.BehaviorProcessor;
import timber.log.Timber;

/**
 * Created by maciek on 22.03.17.
 */

public class RestrictedGroupDetailsPresenter extends MvpAbstractPresenter<RestrictedGroupDetailsContract.IRestrictedGroupDetailsView>
        implements

        RestrictedGroupDetailsContract.IRestrictedGroupDetailsPresenter {

    private static final int INITIAL_MEMBERS_COUNT = 3;

    private final String currentUserId;
    private final UserChatsRepository userChatsRepository;
    private final GetGroupChatDataUseCase getGroupChatDataUseCase;


    private final BehaviorProcessor<Boolean> showAllParticipantsButtonClicked = BehaviorProcessor.createDefault(false);

    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public RestrictedGroupDetailsPresenter(@LoggedInUserId String currentUserId, UserChatsRepository userChatsRepository,
                                           GetGroupChatDataUseCase getGroupChatDataUseCase) {
        this.currentUserId = currentUserId;
        this.userChatsRepository = userChatsRepository;
        this.getGroupChatDataUseCase = getGroupChatDataUseCase;
    }

    @Override
    protected void viewAttached(RestrictedGroupDetailsContract.IRestrictedGroupDetailsView view) {

    }

    @Override
    protected void viewDetached(RestrictedGroupDetailsContract.IRestrictedGroupDetailsView view) {
        subscriptions.clear();
    }

    @Override
    public void requestGroupData(String groupId) {
        requestGroupData(groupId, null);
    }

    @Override
    public void requestGroupData(String groupId, List<String> animatedPhotosUris) {
        Flowable<GroupDataWithEvaluations> groupDataSource = this.showAllParticipantsButtonClicked
                .flatMap(showAllParticipantsButtonClicked -> {
                    if (showAllParticipantsButtonClicked) {
                        return getGroupChatDataUseCase.onGroupDataWithEvaluations(currentUserId, groupId);
                    } else {
                        return getGroupChatDataUseCase.onGroupDataWithEvaluations(currentUserId, groupId,
                                INITIAL_MEMBERS_COUNT);
                    }
                })
                .flatMap(Flowable::fromIterable)
                .replay(1)
                .refCount();

        Flowable<List<String>> animatedPhotosSource = animatedPhotosUris != null ? Flowable.just(animatedPhotosUris) : Flowable.empty();
        subscriptions.add(animatedPhotosSource
                .switchIfEmpty(groupDataSource.flatMap(groupData -> groupData.photosUris().toList().toFlowable()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::showToolbarPictures,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource
                .map(groupData -> groupData.chat().value().name())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setToolbarTitle,
                        throwable -> Timber.e(throwable, "Error loading group name")));

        subscriptions.add(groupDataSource
                .map(groupData -> groupData.chat().value().membersCount())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setParticipantsCount,
                        throwable -> Timber.e(throwable, "Error loading participants count")));


        subscriptions.add(groupDataSource.map(groupData -> groupData.chat().value().description())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupDescription,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));


        subscriptions.add(groupDataSource.map(groupData -> groupData.chat().value().meetingTime().value())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingTime,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.map(groupData -> groupData.chat().value().location())
                .distinctUntilChanged()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingPlace,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.flatMapSingle(groupData -> Flowable.fromIterable(groupData.membersData())
                .map(chatMemberData -> ChatMemberWithRoleAndEvaluation.create(chatMemberData, currentUserId, groupData.chat().value()
                        .groupOwnerId()))
                .toList())
                .distinctUntilChanged()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupParticipants,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));


        subscriptions.add(this.userChatsRepository.onUserChatEvents
                (currentUserId, groupId)
                .flatMapIterable(userVisibleChatMemberships -> userVisibleChatMemberships)
                .distinctUntilChanged(ChatMembership::membershipStatus)
                .map(currentUserMembership -> currentUserMembership.membershipStatus() == MembershipStatus.ACTIVE)
                .distinctUntilChanged()
                .onErrorReturnItem(false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isCurrenUserActiveMember -> {
                            getView().enableEditGroupDescriptionButton(isCurrenUserActiveMember);
                            getView().enableGroupTimeChangeButton(isCurrenUserActiveMember);
                            getView().enableGroupLocationButton(isCurrenUserActiveMember);
                            getView().enableGroupManageUsersButton(isCurrenUserActiveMember);
                            getView().enableEditGroupNameAndImageMenu(isCurrenUserActiveMember);
                        },
                        throwable -> Timber.e(throwable, "Error while listening for chat membership")));

        Flowable<Integer> allMembersCountSource = groupDataSource
                .map(groupData -> groupData.chat().value().membersCount());

        Flowable<Integer> currentMembersCountSource = groupDataSource
                .map(groupData -> groupData.membersData().size());

        subscriptions.add(Flowable.combineLatest(allMembersCountSource, currentMembersCountSource, showAllParticipantsButtonClicked,
                (allMembersCount, currentMembersCount, showAllButtonClicked) -> PublicGroupDetailsPresenter.ShowAllMembersButtonState
                        .create(allMembersCount >
                                        currentMembersCount && !showAllButtonClicked, showAllButtonClicked && allMembersCount >
                                        currentMembersCount,
                                allMembersCount))
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(showAllMembersButtonState -> getView().setShowAllParticipantsButton(showAllMembersButtonState.buttonVisible(),
                        showAllMembersButtonState.progressVisible(), showAllMembersButtonState.membersCount()),
                        throwable -> Timber.e(throwable, "Error while listening for group data")));
    }

    @Override
    public void showAllParticipants() {
        this.showAllParticipantsButtonClicked.onNext(true);
    }
}
