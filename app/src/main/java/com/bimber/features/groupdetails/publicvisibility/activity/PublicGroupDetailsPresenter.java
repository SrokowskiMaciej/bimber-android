package com.bimber.features.groupdetails.publicvisibility.activity;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.domain.chats.group.GetGroupChatDataUseCase;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.domain.model.GroupData;
import com.bimber.domain.model.GroupDataWithEvaluations;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.BehaviorProcessor;
import timber.log.Timber;

/**
 * Created by maciek on 22.03.17.
 */

public class PublicGroupDetailsPresenter extends MvpAbstractPresenter<PublicGroupDetailsContract.IPublicGroupDetailsView>
        implements

        PublicGroupDetailsContract.IPublicGroupDetailsPresenter {

    private static final int INITIAL_MEMBERS_COUNT = 3;

    private final String currentUserId;
    private final GetGroupDataUseCase getGroupDataUseCase;


    private final BehaviorProcessor<Boolean> showAllParticipantsButtonClicked = BehaviorProcessor.createDefault(false);

    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public PublicGroupDetailsPresenter(@LoggedInUserId String currentUserId, GetGroupDataUseCase getGroupDataUseCase) {
        this.currentUserId = currentUserId;
        this.getGroupDataUseCase = getGroupDataUseCase;
    }

    @Override
    protected void viewAttached(PublicGroupDetailsContract.IPublicGroupDetailsView view) {

    }

    @Override
    protected void viewDetached(PublicGroupDetailsContract.IPublicGroupDetailsView view) {
        subscriptions.clear();
    }

    @Override
    public void requestGroupData(String groupId) {
        requestGroupData(groupId, null);
    }

    @Override
    public void requestGroupData(String groupId, List<String> animatedPhotosUris) {
        Flowable<GroupData> groupDataSource = this.showAllParticipantsButtonClicked
                .flatMap(showAllParticipantsButtonClicked -> {
                    if (showAllParticipantsButtonClicked) {
                        return getGroupDataUseCase.onGroupDataOptimized(groupId);
                    } else {
                        return getGroupDataUseCase.onGroupDataOptimized(groupId, INITIAL_MEMBERS_COUNT);
                    }
                })
                .flatMap(Flowable::fromIterable)
                .replay(1)
                .refCount();

        Flowable<List<String>> animatedPhotosSource = animatedPhotosUris != null ? Flowable.just(animatedPhotosUris) : Flowable.empty();
        subscriptions.add(animatedPhotosSource
                .switchIfEmpty(groupDataSource.flatMap(groupData -> groupData.photosUris().toList().toFlowable()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::showToolbarPictures,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource
                .map(groupData -> groupData.group().value().name())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setToolbarTitle,
                        throwable -> Timber.e(throwable, "Error loading group name")));

        subscriptions.add(groupDataSource
                .map(groupData -> groupData.group().value().membersCount())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setParticipantsCount,
                        throwable -> Timber.e(throwable, "Error loading participants count")));


        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().description())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupDescription,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));


        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().meetingTime().value())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingTime,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().location())
                .distinctUntilChanged()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingPlace,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.flatMapSingle(groupData -> Flowable.fromIterable(groupData.membersData())
                .map(chatMemberData -> ChatMemberWithRole.create(chatMemberData, currentUserId, groupData.group().value()
                        .groupOwnerId()))
                .toList())
                .distinctUntilChanged()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupParticipants,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        Flowable<Integer> allMembersCountSource = groupDataSource
                .map(groupData -> groupData.group().value().membersCount());

        Flowable<Integer> currentMembersCountSource = groupDataSource
                .map(groupData -> groupData.membersData().size());

        subscriptions.add(Flowable.combineLatest(allMembersCountSource, currentMembersCountSource, showAllParticipantsButtonClicked,
                (allMembersCount, currentMembersCount, showAllButtonClicked) -> com.bimber.features.groupdetails.publicvisibility
                        .PublicGroupDetailsPresenter.ShowAllMembersButtonState
                        .create(allMembersCount >
                                        currentMembersCount && !showAllButtonClicked, showAllButtonClicked && allMembersCount >
                                        currentMembersCount,
                                allMembersCount))
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(showAllMembersButtonState -> getView().setShowAllParticipantsButton(showAllMembersButtonState.buttonVisible(),
                        showAllMembersButtonState.progressVisible(), showAllMembersButtonState.membersCount()),
                        throwable -> Timber.e(throwable, "Error while listening for group data")));
    }

    @Override
    public void showAllParticipants() {
        this.showAllParticipantsButtonClicked.onNext(true);
    }
}
