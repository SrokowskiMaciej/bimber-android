package com.bimber.features.groupdetails.publicvisibility;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.domain.model.GroupData;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsContract.IPublicGroupDetailsPresenter;
import com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsContract.IPublicGroupDetailsView;
import com.bimber.utils.mvp.MvpAbstractPresenter;
import com.google.auto.value.AutoValue;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.BehaviorProcessor;
import timber.log.Timber;

/**
 * Created by maciek on 22.03.17.
 */

public class PublicGroupDetailsPresenter extends MvpAbstractPresenter<IPublicGroupDetailsView> implements IPublicGroupDetailsPresenter {

    private static final int INITIAL_MEMBERS_COUNT = 3;
    private final GetGroupDataUseCase getGroupDataUseCase;
    private final String currentUserId;

    private final BehaviorProcessor<Boolean> showAllParticipantsButtonClicked = BehaviorProcessor.createDefault(false);

    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public PublicGroupDetailsPresenter(@LoggedInUserId String currentUserId, GetGroupDataUseCase getGroupDataUseCase) {
        this.currentUserId = currentUserId;
        this.getGroupDataUseCase = getGroupDataUseCase;
    }

    @Override
    protected void viewAttached(IPublicGroupDetailsView view) {

    }

    @Override
    protected void viewDetached(IPublicGroupDetailsView view) {
        subscriptions.clear();
    }

    @Override
    public void requestGroupData(String groupId) {
        Flowable<GroupData> groupDataSource = showAllParticipantsButtonClicked
                .switchMap(showAllParticipantsButtonClicked -> {
                    if (showAllParticipantsButtonClicked) {
                        return getGroupDataUseCase.onGroupDataOptimized(groupId);
                    } else {
                        return getGroupDataUseCase.onGroupDataOptimized(groupId, INITIAL_MEMBERS_COUNT);
                    }
                })
                .flatMap(Flowable::fromIterable)
                .replay(1)
                .refCount();

        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().description())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupDescription,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));


        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().meetingTime().value())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingTime,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.map(groupData -> groupData.group().value().location())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupMeetingPlace,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        subscriptions.add(groupDataSource.flatMapSingle(groupData -> Flowable.fromIterable(groupData.membersData())
                .map(chatMemberData -> ChatMemberWithRole.create(chatMemberData, currentUserId, groupData.group().value().groupOwnerId()))
                .toList())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setGroupParticipants,
                        throwable -> Timber.e(throwable, "Error while listening for group data")));

        Flowable<Integer> allMembersCountSource = groupDataSource
                .map(groupData -> groupData.group().value().membersCount());

        Flowable<Integer> currentMembersCountSource = groupDataSource
                .map(groupData -> groupData.membersData().size());

        subscriptions.add(Flowable.combineLatest(allMembersCountSource, currentMembersCountSource, showAllParticipantsButtonClicked,
                (allMembersCount, currentMembersCount, showAllButtonClicked) -> ShowAllMembersButtonState.create(allMembersCount >
                                currentMembersCount && !showAllButtonClicked, showAllButtonClicked && allMembersCount > currentMembersCount,
                        allMembersCount))
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(showAllMembersButtonState -> getView().setShowAllParticipantsButton(showAllMembersButtonState.buttonVisible(),
                        showAllMembersButtonState.progressVisible(), showAllMembersButtonState.membersCount()),
                        throwable -> Timber.e(throwable, "Error while listening for group data")));
    }

    @Override
    public void showAllParticipants() {
        this.showAllParticipantsButtonClicked.onNext(true);
    }

    @AutoValue
    public abstract static class ShowAllMembersButtonState {
        public abstract boolean buttonVisible();

        public abstract boolean progressVisible();

        public abstract int membersCount();

        public static ShowAllMembersButtonState create(boolean buttonVisible, boolean progressVisible, int membersCount) {
            return new AutoValue_PublicGroupDetailsPresenter_ShowAllMembersButtonState(buttonVisible, progressVisible, membersCount);
        }
    }
}
