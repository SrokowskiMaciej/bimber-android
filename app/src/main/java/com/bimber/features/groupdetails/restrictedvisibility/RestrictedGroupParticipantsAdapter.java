package com.bimber.features.groupdetails.restrictedvisibility;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.PersonEvaluation.PersonEvaluationStatus;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 23.03.17.
 */

public class RestrictedGroupParticipantsAdapter extends RecyclerView.Adapter<RestrictedGroupParticipantsAdapter
        .GroupParticipantViewHolder> {

    private List<ChatMemberWithRoleAndEvaluation> participants = Collections.emptyList();

    private final String currentUserId;
    private final GlideRequests glide;

    public RestrictedGroupParticipantsAdapter(String currentUserId, GlideRequests glide) {
        this.currentUserId = currentUserId;
        this.glide = glide;
    }


    @Override
    public GroupParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_group_details_participant_item,
                parent, false);
        return new GroupParticipantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupParticipantViewHolder holder, int position) {
        Context context = holder.textViewParticipantName.getContext();
        ChatMemberWithRoleAndEvaluation chatMemberData = participants.get(position);
        ProfileDataThumbnail profileData = chatMemberData.chatMember().profileData();
        holder.textViewParticipantName.setText(profileData.user.displayName);
        holder.textViewParticipantAbout.setText(profileData.user.about);
        holder.rootLayout.setOnClickListener(v -> v.getContext().startActivity(ProfileActivity.newInstance(v
                        .getContext(), profileData.user.uId, true)));

        glide.load(profileData.photo.userPhotoUri)
                .apply(new RequestOptions().centerCrop())
                .into(holder.imageViewAvatar);
        if (profileData.user.uId.equals(currentUserId)) {
            holder.buttonAction.setVisibility(View.GONE);
            holder.textViewParticipantName.append(context.getString(R.string
                    .view_group_details_current_user_indicator));
        } else if (chatMemberData.chatMember().personEvaluation().status() == PersonEvaluationStatus.MATCHED &&
                chatMemberData.chatMember().personEvaluation().dialogId() != null) {
            holder.buttonAction.setVisibility(View.VISIBLE);
            holder.buttonAction.setImageResource(R.drawable.ic_chat_white_24dp);
            holder.buttonAction.setOnClickListener(v -> context.startActivity(ChatActivity.newInstance(context,
                    chatMemberData.chatMember().personEvaluation().dialogId(), Chattable.ChatType.DIALOG)));
        } else {
            holder.buttonAction.setVisibility(View.VISIBLE);
            holder.buttonAction.setImageResource(R.drawable.ic_track_changes_white_24dp);
            holder.buttonAction.setOnClickListener(v -> context.startActivity(HomeActivity.newInstanceWithProfile
                    (context,
                    profileData.user.uId)));
        }

        switch (chatMemberData.chatRole()) {
            case USER:
                holder.textViewAdminIndicator.setVisibility(View.GONE);
                break;
            case CURRENT_USER:
                holder.textViewAdminIndicator.setVisibility(View.GONE);
                break;
            case OWNER:
                holder.textViewAdminIndicator.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return participants.size();
    }


    public void setData(List<ChatMemberWithRoleAndEvaluation> chatMembers) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<ChatMemberWithRoleAndEvaluation>(this
                .participants,
                chatMembers) {

            @Override
            public boolean areItemsTheSame(ChatMemberWithRoleAndEvaluation oldItem, ChatMemberWithRoleAndEvaluation
                    newItem) {
                return oldItem.chatMember().chatMembership().uId().equals(newItem.chatMember().chatMembership().uId());
            }

            @Override
            public boolean areContentsTheSame(ChatMemberWithRoleAndEvaluation oldItem,
                                              ChatMemberWithRoleAndEvaluation newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.participants = chatMembers;
        diffResult.dispatchUpdatesTo(this);
    }

    public static class GroupParticipantViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rootLayout)
        ConstraintLayout rootLayout;
        @BindView(R.id.imageViewAvatar)
        ImageView imageViewAvatar;
        @BindView(R.id.textViewParticipantName)
        TextView textViewParticipantName;
        @BindView(R.id.textViewAdminIndicator)
        TextView textViewAdminIndicator;
        @BindView(R.id.textViewParticipantAbout)
        TextView textViewParticipantAbout;
        @BindView(R.id.buttonAction)
        ImageButton buttonAction;

        public GroupParticipantViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
