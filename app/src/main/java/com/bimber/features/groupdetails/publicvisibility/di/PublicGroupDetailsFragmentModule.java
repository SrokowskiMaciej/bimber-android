package com.bimber.features.groupdetails.publicvisibility.di;

import com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsPresenter;
import com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.bimber.features.groupdetails.publicvisibility.PublicGroupDetailsContract.*;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */
@Module
public abstract class PublicGroupDetailsFragmentModule {

    @ContributesAndroidInjector
    abstract PublicGroupDetailsView contributeGroupDetailsView();

    @Binds
    abstract IPublicGroupDetailsPresenter presenter(PublicGroupDetailsPresenter presenter);

}
