package com.bimber.features.groupdetails.publicvisibility.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.groupdetails.publicvisibility.activity.PublicGroupDetailsContract.IPublicGroupDetailsPresenter;
import com.bimber.features.groupdetails.restrictedvisibility.activity.RestrictedGroupDetailsContract.IRestrictedGroupDetailsPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class PublicGroupActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(PublicGroupActivity activity);


    @Binds
    abstract IPublicGroupDetailsPresenter presenter(PublicGroupDetailsPresenter presenter);
}
