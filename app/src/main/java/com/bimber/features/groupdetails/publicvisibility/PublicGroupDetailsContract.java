package com.bimber.features.groupdetails.publicvisibility;

import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 22.03.17.
 */

public interface PublicGroupDetailsContract {

    interface IPublicGroupDetailsView {

        void setGroupDescription(String description);

        void setGroupMeetingTime(long timestamp);

        void setGroupMeetingPlace(Place place);

        void setShowAllParticipantsButton(boolean buttonVisible, boolean progressVisible, int participantsCount);

        void setGroupParticipants(List<ChatMemberWithRole> groupParticipants);

    }

    interface IPublicGroupDetailsPresenter extends MvpPresenter<IPublicGroupDetailsView> {
        void requestGroupData(String groupId);

        void showAllParticipants();
    }
}
