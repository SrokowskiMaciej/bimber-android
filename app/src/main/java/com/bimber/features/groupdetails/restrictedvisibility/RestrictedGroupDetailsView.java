package com.bimber.features.groupdetails.restrictedvisibility;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.glide.GlideApp;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionViewBuilder;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationViewBuilder;
import com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameViewBuilder;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeViewBuilder;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.features.chat.dialogs.managegroupusers.activity.ManageGroupUsersActivity;
import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureViewBuilder;
import com.bimber.features.groupdetails.restrictedvisibility.RestrictedGroupDetailsContract.IRestrictedGroupDetailsPresenter;
import com.bimber.features.groupdetails.restrictedvisibility.RestrictedGroupDetailsContract.IRestrictedGroupDetailsView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maciek on 22.03.17.
 */

@FragmentWithArgs
public class RestrictedGroupDetailsView extends BaseGroupFragment implements IRestrictedGroupDetailsView {

    public static final String GROUP_NAME_DIALOG_TAG = "GROUP_NAME_DIALOG_TAG";
    public static final String GROUP_DESCRIPTION_DIALOG_TAG = "GROUP_DESCRIPTION_DIALOG_TAG";
    public static final String GROUP_LOCATION_DIALOG_TAG = "GROUP_LOCATION_DIALOG_TAG";
    public static final String GROUP_TIME_DIALOG_TAG = "GROUP_TIME_DIALOG_TAG";

    public static final String GROUP_IMAGE_DIALOG_TAG = "GROUP_IMAGE_DIALOG_TAG";


    @Inject
    IRestrictedGroupDetailsPresenter groupDetailsPresenter;
    @Inject
    @LoggedInUserId
    String currentUserId;

    @BindView(R.id.recyclerViewGroupParticipants)
    RecyclerView recyclerViewGroupParticipants;
    @BindView(R.id.textViewGroupDescriptionContent)
    TextView textViewGroupDescriptionContent;
    @BindView(R.id.textViewMeetingTimeContent)
    TextView textViewMeetingTimeContent;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.buttonEditDescription)
    Button buttonEditDescription;
    @BindView(R.id.buttonEditTime)
    Button buttonEditTime;
    @BindView(R.id.buttonEditLocation)
    Button buttonEditLocation;
    @BindView(R.id.buttonManageUsers)
    Button buttonManageUsers;
    @BindView(R.id.imageButtonDirections)
    ImageView imageButtonDirections;
    @BindView(R.id.imageButtonMap)
    ImageView imageButtonMap;
    @BindView(R.id.buttonShowAllUsers)
    Button buttonShowAllUsers;
    @BindView(R.id.progressBarShowAllUsers)
    ProgressBar progressBarShowAllUsers;

    private RestrictedGroupParticipantsAdapter restrictedGroupParticipantsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_group_details_restricted, container, false);
        ButterKnife.bind(this, view);
        MapsInitializer.initialize(mapView.getContext().getApplicationContext());
        mapView.onCreate(savedInstanceState);
        restrictedGroupParticipantsAdapter = new RestrictedGroupParticipantsAdapter(currentUserId, GlideApp.with(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerViewGroupParticipants.setLayoutManager(layoutManager);
        recyclerViewGroupParticipants.setAdapter(restrictedGroupParticipantsAdapter);
        recyclerViewGroupParticipants.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable
                .shape_list_divider_insets_72_dp), true));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        groupDetailsPresenter.bindView(this);
        groupDetailsPresenter.requestGroupData(groupId);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        groupDetailsPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    private MarkerOptions setupMarkerOptionsInfoWindow(MarkerOptions markerOptions, Place place) {
        if (place.isPlaceNameHumanReadable()) {
            markerOptions.title(place.name()).snippet(place.address());
        } else {
            markerOptions.title(place.getNormalizedPlaceName());
        }
        return markerOptions;
    }

    @Override
    public void setGroupDescription(String description) {
        textViewGroupDescriptionContent.setText(description);
    }

    @Override
    public void setGroupMeetingTime(long timestamp) {
        Date date = new Date(timestamp);
        String dateText = DateFormat.getLongDateFormat(getContext()).format(date) + "\n" + DateFormat.getTimeFormat(getContext()).format
                (date);
        textViewMeetingTimeContent.setText(dateText);
    }

    @Override
    public void setGroupMeetingPlace(Place place) {
        mapView.getMapAsync(googleMap -> {
            googleMap.clear();
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            googleMap.getUiSettings().setMapToolbarEnabled(false);

            LatLng placeLatLng = new LatLng(place.latitude(), place.longitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(placeLatLng)
                    .zoom(12)
                    .build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            final Marker marker = googleMap.addMarker(setupMarkerOptionsInfoWindow(new MarkerOptions().position(placeLatLng), place));
            marker.showInfoWindow();
            googleMap.setOnMapClickListener(latLng -> marker.showInfoWindow());
            googleMap.setOnMarkerClickListener(clickedMarker -> {
                marker.showInfoWindow();
                return false;
            });
            imageButtonDirections.setVisibility(View.VISIBLE);
            imageButtonDirections.setOnClickListener(v -> {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, place.getGoogleMapsDirectionsUri());
                getContext().startActivity(mapIntent);
            });
            imageButtonMap.setVisibility(View.VISIBLE);
            imageButtonMap.setOnClickListener(v -> {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, place.getGoogleMapsSearchUri());
                getContext().startActivity(mapIntent);
            });
        });
    }

    @Override
    public void setShowAllParticipantsButton(boolean buttonVisible, boolean progressVisible, int participantsCount) {
        if (buttonVisible) {
            buttonShowAllUsers.setVisibility(View.VISIBLE);
            buttonShowAllUsers.setText(String.format(getString(R.string.view_group_details_show_all_button_text), String.valueOf
                    (participantsCount)));
        } else {
            buttonShowAllUsers.setVisibility(View.INVISIBLE);
        }

        if (progressVisible) {
            progressBarShowAllUsers.setVisibility(View.VISIBLE);
        } else {
            progressBarShowAllUsers.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setGroupParticipants(List<ChatMemberWithRoleAndEvaluation> groupParticipants) {
        restrictedGroupParticipantsAdapter.setData(groupParticipants);
    }

    @Override
    public void enableEditGroupDescriptionButton(boolean enable) {
        buttonEditDescription.setEnabled(enable);
    }

    @Override
    public void enableGroupLocationButton(boolean enable) {
        buttonEditLocation.setEnabled(enable);
    }

    @Override
    public void enableGroupTimeChangeButton(boolean enable) {
        buttonEditTime.setEnabled(enable);
    }

    @Override
    public void enableGroupManageUsersButton(boolean enable) {
        buttonManageUsers.setEnabled(enable);
    }

    @Override
    public void enableEditGroupNameAndImageMenu(boolean enable) {
        setMenuVisibility(enable);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.message_set_group_name:
                getChildFragmentManager().beginTransaction()
                        .add(new ChangeGroupNameViewBuilder(groupId).build(), GROUP_NAME_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.message_set_group_image:
                getChildFragmentManager().beginTransaction()
                        .add(new SetGroupPictureViewBuilder(groupId).build(), GROUP_IMAGE_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;

        }
        return false;
    }

    @OnClick({R.id.buttonEditDescription, R.id.buttonEditTime, R.id.buttonEditLocation, R.id.buttonManageUsers, R.id.buttonShowAllUsers})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonEditDescription:
                getChildFragmentManager().beginTransaction()
                        .add(new ChangeGroupDescriptionViewBuilder(groupId).build(), GROUP_DESCRIPTION_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.buttonEditTime:
                getChildFragmentManager().beginTransaction()
                        .add(new ChangeGroupTimeViewBuilder(groupId).build(), GROUP_TIME_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.buttonEditLocation:
                getChildFragmentManager().beginTransaction()
                        .add(new ChangeGroupLocationViewBuilder(groupId).build(), GROUP_LOCATION_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.buttonManageUsers:
                startActivity(ManageGroupUsersActivity.newInstance(getContext(), groupId));
                break;
            case R.id.buttonShowAllUsers:
                groupDetailsPresenter.showAllParticipants();
                break;
        }
    }
}
