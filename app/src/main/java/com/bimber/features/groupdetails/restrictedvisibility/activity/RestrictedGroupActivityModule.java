package com.bimber.features.groupdetails.restrictedvisibility.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.groupdetails.restrictedvisibility.activity.RestrictedGroupDetailsContract.IRestrictedGroupDetailsPresenter;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class RestrictedGroupActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(RestrictedGroupActivity activity);


    @Binds
    abstract IRestrictedGroupDetailsPresenter presenter(RestrictedGroupDetailsPresenter presenter);
}
