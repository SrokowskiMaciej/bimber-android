package com.bimber.features.groupdetails.restrictedvisibility.di;

import com.bimber.features.groupdetails.restrictedvisibility.RestrictedGroupDetailsContract;
import com.bimber.features.groupdetails.restrictedvisibility.RestrictedGroupDetailsPresenter;
import com.bimber.features.groupdetails.restrictedvisibility.RestrictedGroupDetailsView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */
@Module
public abstract class RestrictedGroupDetailsFragmentModule {

    @ContributesAndroidInjector
    abstract RestrictedGroupDetailsView contributeGroupDetailsView();

    @Binds
    abstract RestrictedGroupDetailsContract.IRestrictedGroupDetailsPresenter presenter(RestrictedGroupDetailsPresenter presenter);
}
