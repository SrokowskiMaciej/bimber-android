package com.bimber.features.groupdetails.restrictedvisibility.activity;

import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 22.03.17.
 */

public interface RestrictedGroupDetailsContract {

    interface IRestrictedGroupDetailsView {
        void showToolbarPictures(List<String> uri);

        void setToolbarTitle(String title);

        void setParticipantsCount(int participantsCount);

        void setGroupDescription(String description);

        void setGroupMeetingTime(long timestamp);

        void setGroupMeetingPlace(Place place);

        void setShowAllParticipantsButton(boolean buttonVisible, boolean progressVisible, int participantsCount);

        void setGroupParticipants(List<ChatMemberWithRoleAndEvaluation> groupParticipants);

        void enableEditGroupDescriptionButton(boolean enable);

        void enableGroupLocationButton(boolean enable);

        void enableGroupTimeChangeButton(boolean enable);

        void enableGroupManageUsersButton(boolean enable);

        void enableEditGroupNameAndImageMenu(boolean enable);
    }

    interface IRestrictedGroupDetailsPresenter extends MvpPresenter<IRestrictedGroupDetailsView> {
        void requestGroupData(String groupId);

        void requestGroupData(String groupId, List<String> photosUris);

        void showAllParticipants();
    }
}
