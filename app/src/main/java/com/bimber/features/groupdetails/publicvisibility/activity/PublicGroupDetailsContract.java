package com.bimber.features.groupdetails.publicvisibility.activity;

import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRoleAndEvaluation;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 22.03.17.
 */

public interface PublicGroupDetailsContract {

    interface IPublicGroupDetailsView {
        void showToolbarPictures(List<String> uri);

        void setToolbarTitle(String title);

        void setParticipantsCount(int participantsCount);

        void setGroupDescription(String description);

        void setGroupMeetingTime(long timestamp);

        void setGroupMeetingPlace(Place place);

        void setShowAllParticipantsButton(boolean buttonVisible, boolean progressVisible, int participantsCount);

        void setGroupParticipants(List<ChatMemberWithRole> groupParticipants);
    }

    interface IPublicGroupDetailsPresenter extends MvpPresenter<IPublicGroupDetailsView> {
        void requestGroupData(String groupId);

        void requestGroupData(String groupId, List<String> photosUris);

        void showAllParticipants();
    }
}
