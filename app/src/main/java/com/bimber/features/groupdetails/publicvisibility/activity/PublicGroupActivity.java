package com.bimber.features.groupdetails.publicvisibility.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.BitmapPool.DrawablePool;
import com.bimber.base.application.BimberApplication;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.groupdetails.publicvisibility.PublicGroupParticipantsAdapter;
import com.bimber.features.groupdetails.publicvisibility.activity.PublicGroupDetailsContract.IPublicGroupDetailsPresenter;
import com.bimber.features.share.group.ShareGroupView;
import com.bimber.utils.TimeUtils;
import com.bimber.utils.images.BitmapUtils;
import com.bimber.utils.view.WeightedAppBarLayout;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class PublicGroupActivity extends BaseLoggedInActivity implements PublicGroupDetailsContract.IPublicGroupDetailsView {


    public static final String GROUP_ID_KEY = "GROUP_ID_KEY";
    private static final String ANIMATED_PHOTO_KEY = "ANIMATED_PHOTO_KEY";

    public static final String SHARE_GROUP_FRAGMENT_TAG = "SHARE_GROUP_FRAGMENT_TAG";
    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageViewGroupPhoto)
    ImageView imageViewGroupPhoto;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.textViewErrorTitle)
    TextView textViewErrorTitle;
    @BindView(R.id.textViewParticipantsCount)
    TextView textViewParticipantsCount;
    @BindView(R.id.textViewRemainingTime)
    TextView textViewRemainingTime;
    @BindView(R.id.recyclerViewGroupParticipants)
    RecyclerView recyclerViewGroupParticipants;
    @BindView(R.id.textViewGroupDescriptionContent)
    TextView textViewGroupDescriptionContent;
    @BindView(R.id.textViewMeetingTimeContent)
    TextView textViewMeetingTimeContent;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.imageButtonDirections)
    ImageView imageButtonDirections;
    @BindView(R.id.imageButtonMap)
    ImageView imageButtonMap;
    @BindView(R.id.buttonShowAllUsers)
    Button buttonShowAllUsers;
    @BindView(R.id.progressBarShowAllUsers)
    ProgressBar progressBarShowAllUsers;
    @BindView(R.id.buttonShare)
    FloatingActionButton buttonShare;
    @BindView(R.id.appBarView)
    WeightedAppBarLayout appBarView;

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    IPublicGroupDetailsPresenter publicGroupDetailsPresenter;
    @Inject
    DrawablePool drawablePool;
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private CompositeDisposable refreshMeetingTimeSubscription = new CompositeDisposable();

    private PublicGroupParticipantsAdapter publicGroupParticipantsAdapter;

    public static Intent newInstance(Context context, String groupId) {
        Intent intent = new Intent(context, PublicGroupActivity.class);
        intent.putExtra(GROUP_ID_KEY, groupId);
        return intent;
    }

    public static Intent newInstance(Context context, String groupId, ArrayList<String> animatedPhotoUri) {
        Intent intent = new Intent(context, PublicGroupActivity.class);
        intent.putExtra(GROUP_ID_KEY, groupId);
        intent.putStringArrayListExtra(ANIMATED_PHOTO_KEY, animatedPhotoUri);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ArrayList<String> animatedPhotosUris = getIntent().getStringArrayListExtra(ANIMATED_PHOTO_KEY);
        boolean isAnimated = animatedPhotosUris != null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details_public);
        ButterKnife.bind(this);
        MapsInitializer.initialize(mapView.getContext().getApplicationContext());
        mapView.onCreate(savedInstanceState);
        setupToolbar();
        publicGroupDetailsPresenter.bindView(this);
        if (isAnimated) {
            setupAnimations(animatedPhotosUris);
            publicGroupDetailsPresenter.requestGroupData(getIntent().getStringExtra(GROUP_ID_KEY), animatedPhotosUris);
        } else {
            publicGroupDetailsPresenter.requestGroupData(getIntent().getStringExtra(GROUP_ID_KEY));
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        setTitle("");
        final Typeface tf = Typeface.createFromAsset(getAssets(), BimberApplication.DEFAULT_FONT_ASSET_PATH);
        collapsingToolbar.setCollapsedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    
    private void setupAnimations(List<String> animatedPhotosUris) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            TransitionSet set = new TransitionSet()
                    .addTransition(new ChangeImageTransform().setInterpolator(new FastOutSlowInInterpolator()))
                    .addTransition(new ChangeBounds().setInterpolator(new FastOutSlowInInterpolator()));

            window.setSharedElementEnterTransition(set);
            window.setEnterTransition(new Slide(Gravity.BOTTOM)
                    .excludeTarget(android.R.id.statusBarBackground, true)
                    .excludeTarget(android.R.id.navigationBarBackground, true)
                    .excludeTarget(R.id.toolbar, true)
                    .setInterpolator(new FastOutSlowInInterpolator()));

            String photosHash = DrawablePool.hashCollage(animatedPhotosUris);
            ViewCompat.setTransitionName(imageViewGroupPhoto, photosHash);
            Drawable drawable = drawablePool.getDrawable(photosHash);
            if (drawable != null) {
                //In case we find something in drawable pool
                imageViewGroupPhoto.setImageDrawable(drawable);
            } else {
                ActivityCompat.postponeEnterTransition(this);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        subscriptions.clear();
        refreshMeetingTimeSubscription.clear();
        publicGroupDetailsPresenter.unbindView(this);
        mapView.onDestroy();
        super.onDestroy();
    }

    private MarkerOptions setupMarkerOptionsInfoWindow(MarkerOptions markerOptions, Place place) {
        if (place.isPlaceNameHumanReadable()) {
            markerOptions.title(place.name()).snippet(place.address());
        } else {
            markerOptions.title(place.getNormalizedPlaceName());
        }
        return markerOptions;
    }


    @Override
    public void showToolbarPictures(List<String> uris) {
        textViewErrorTitle.setVisibility(View.GONE);
        imageViewGroupPhoto.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageViewGroupPhoto.getViewTreeObserver().removeOnPreDrawListener(this);
                subscriptions.add(Single.just(uris)
                        .flatMap(photoUris -> BitmapUtils.bitmapCollage(imageViewGroupPhoto
                                        .getContext(), photoUris,
                                imageViewGroupPhoto.getWidth(), imageViewGroupPhoto.getHeight(),
                                DiskCacheStrategy.ALL))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bitmap -> {
                                    imageViewGroupPhoto.setImageBitmap(bitmap);
                                    ActivityCompat.startPostponedEnterTransition(PublicGroupActivity.this);
                                },
                                throwable -> {
                                    Timber.e(throwable, "Failed to set group image bitmap");
                                    ActivityCompat.startPostponedEnterTransition(PublicGroupActivity.this);
                                }));
                return true;
            }
        });
    }

    @Override
    public void setToolbarTitle(String title) {
        collapsingToolbar.setTitle(title);
    }

    @Override
    public void setParticipantsCount(int participantsCount) {
        textViewParticipantsCount.setText(String.format(getResources()
                .getQuantityString(R.plurals.group_members_count, participantsCount), participantsCount));
    }

    @Override
    public void setGroupDescription(String description) {
        textViewGroupDescriptionContent.setText(description);
    }

    @Override
    public void setGroupMeetingTime(long timestamp) {
        Date date = new Date(timestamp);
        String dateText = DateFormat.getLongDateFormat(this).format(date) + "\n" + DateFormat.getTimeFormat(this).format(date);
        textViewMeetingTimeContent.setText(dateText);

        refreshMeetingTimeSubscription.clear();
        refreshMeetingTimeSubscription.add(Observable.interval(0, 30, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(delay -> {
                    long now = System.currentTimeMillis();
                    if (now < timestamp) {
                        textViewRemainingTime.setText(String.format(getString(R.string.group_remaining_time_upcoming),
                                TimeUtils.formatDuration(this, timestamp, now)));
                    } else {
                        textViewRemainingTime.setText(String.format(getString(R.string.group_remaining_time_past),
                                TimeUtils.formatDuration(this, timestamp, now)));
                    }
                }));
    }

    @Override
    public void setGroupMeetingPlace(Place place) {
        mapView.getMapAsync(googleMap -> {
            googleMap.clear();
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
            googleMap.getUiSettings().setMapToolbarEnabled(false);

            LatLng placeLatLng = new LatLng(place.latitude(), place.longitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(placeLatLng)
                    .zoom(12)
                    .build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            final Marker marker = googleMap.addMarker(setupMarkerOptionsInfoWindow(new MarkerOptions().position(placeLatLng), place));
            marker.showInfoWindow();
            googleMap.setOnMapClickListener(latLng -> marker.showInfoWindow());
            googleMap.setOnMarkerClickListener(clickedMarker -> {
                marker.showInfoWindow();
                return false;
            });
            imageButtonDirections.setVisibility(View.VISIBLE);
            imageButtonDirections.setOnClickListener(v -> {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, place.getGoogleMapsDirectionsUri());
                startActivity(mapIntent);
            });
            imageButtonMap.setVisibility(View.VISIBLE);
            imageButtonMap.setOnClickListener(v -> {
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, place.getGoogleMapsSearchUri());
                startActivity(mapIntent);
            });
        });
    }

    @Override
    public void setShowAllParticipantsButton(boolean buttonVisible, boolean progressVisible, int participantsCount) {
        if (buttonVisible) {
            buttonShowAllUsers.setVisibility(View.VISIBLE);
            buttonShowAllUsers.setText(String.format(getString(R.string.view_group_details_show_all_button_text), String.valueOf
                    (participantsCount)));
        } else {
            buttonShowAllUsers.setVisibility(View.INVISIBLE);
        }

        if (progressVisible) {
            progressBarShowAllUsers.setVisibility(View.VISIBLE);
        } else {
            progressBarShowAllUsers.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setGroupParticipants(List<ChatMemberWithRole> groupParticipants) {
        if (publicGroupParticipantsAdapter == null) {
            publicGroupParticipantsAdapter = new PublicGroupParticipantsAdapter(GlideApp.with(this));
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerViewGroupParticipants.setLayoutManager(layoutManager);
            recyclerViewGroupParticipants.setAdapter(publicGroupParticipantsAdapter);
            recyclerViewGroupParticipants.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(this, R.drawable
                    .shape_list_divider_insets_72_dp), true));
        }
        publicGroupParticipantsAdapter.setParticipants(groupParticipants);
    }

    @OnClick({R.id.buttonShowAllUsers, R.id.buttonShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonShowAllUsers:
                publicGroupDetailsPresenter.showAllParticipants();
                break;
            case R.id.buttonShare:
                getSupportFragmentManager().beginTransaction().add(ShareGroupView.newInstance(getIntent().getStringExtra(GROUP_ID_KEY), R
                        .style.AppTheme), SHARE_GROUP_FRAGMENT_TAG).commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        appBarView.expand(this::setUpExit);
    }

    private void setUpExit() {
        ((CoordinatorLayout.LayoutParams) buttonShare.getLayoutParams()).setAnchorId(View.NO_ID);
        ActivityCompat.finishAfterTransition(this);
    }


}
