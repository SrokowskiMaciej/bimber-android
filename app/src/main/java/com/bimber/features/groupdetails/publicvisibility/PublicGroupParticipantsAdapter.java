package com.bimber.features.groupdetails.publicvisibility;

import android.support.constraint.ConstraintLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 23.03.17.
 */

public class PublicGroupParticipantsAdapter extends RecyclerView.Adapter<PublicGroupParticipantsAdapter
        .GroupParticipantViewHolder> {

    private List<ChatMemberWithRole> participants = Collections.emptyList();
    private final GlideRequests glide;

    public PublicGroupParticipantsAdapter(GlideRequests glide) {
        this.glide = glide;
    }

    @Override
    public GroupParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_group_details_participant_item,
                parent, false);
        return new GroupParticipantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupParticipantViewHolder holder, int position) {
        ChatMemberWithRole chatMemberData = participants.get(position);
        holder.textViewParticipantName.setText(chatMemberData.chatMember().profileData().user.displayName);
        holder.textViewParticipantAbout.setText(chatMemberData.chatMember().profileData().user.about);
        holder.rootLayout.setOnClickListener(v -> v.getContext().startActivity(ProfileActivity.newInstance(v
                .getContext(), chatMemberData.chatMember().profileData().user.uId, false)));

        glide.load(chatMemberData.chatMember().profileData().photo.userPhotoUri)
                .apply(new RequestOptions().centerCrop())
                .into(holder.imageViewAvatar);

        switch (chatMemberData.chatRole()) {
            case USER:
                holder.textViewAdminIndicator.setVisibility(View.GONE);
                break;
            case CURRENT_USER:
                holder.textViewAdminIndicator.setVisibility(View.GONE);
                break;
            case OWNER:
                holder.textViewAdminIndicator.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return participants.size();
    }


    public void setParticipants(List<ChatMemberWithRole> chatMembers) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<ChatMemberWithRole>(this
                .participants,
                chatMembers) {

            @Override
            public boolean areItemsTheSame(ChatMemberWithRole oldItem, ChatMemberWithRole newItem) {
                return oldItem.chatMember().chatMembership().uId().equals(newItem.chatMember().chatMembership().uId());
            }

            @Override
            public boolean areContentsTheSame(ChatMemberWithRole oldItem, ChatMemberWithRole newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.participants = chatMembers;
        diffResult.dispatchUpdatesTo(this);
    }

    public static class GroupParticipantViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rootLayout)
        ConstraintLayout rootLayout;
        @BindView(R.id.imageViewAvatar)
        ImageView imageViewAvatar;
        @BindView(R.id.textViewParticipantName)
        TextView textViewParticipantName;
        @BindView(R.id.textViewParticipantAbout)
        TextView textViewParticipantAbout;
        @BindView(R.id.textViewAdminIndicator)
        TextView textViewAdminIndicator;

        public GroupParticipantViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
