package com.bimber.features.profiledetails;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.profiledetails.ProfileDetailsContract.IProfileDetailsView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

import static com.bimber.features.profiledetails.ProfileDetailsContract.IProfileDetailsPresenter;

/**
 * Created by maciek on 23.03.17.
 */

public class ProfileDetailsPresenter extends MvpAbstractPresenter<IProfileDetailsView> implements IProfileDetailsPresenter {

    private final ProfileDataRepository profileDataRepository;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ProfileDetailsPresenter(ProfileDataRepository profileDataRepository, BackgroundSchedulerProvider
            backgroundSchedulerProvider) {
        this.profileDataRepository = profileDataRepository;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    @Override
    protected void viewAttached(IProfileDetailsView view) {

    }

    @Override
    protected void viewDetached(IProfileDetailsView view) {
        subscriptions.clear();
    }

    @Override
    public void requestProfileData(String uId) {
        subscriptions.add(profileDataRepository.onUserProfileDataFull(uId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(profileDataFull -> getView().setPersonData(profileDataFull)));
    }
}
