package com.bimber.features.profiledetails;

import android.content.Intent;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.utils.view.AbstractDiffUtilCallback;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by srokowski.maciej@gmail.com on 22.12.16.
 */

public class FavouritePlacesAdapter extends RecyclerView.Adapter<FavouritePlacesAdapter.FavouritePlaceViewHolder> {

    private List<FavouritePlace> favouritePlaces = Collections.emptyList();

    public FavouritePlacesAdapter() {
    }

    @Override
    public FavouritePlacesAdapter.FavouritePlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FavouritePlacesAdapter.FavouritePlaceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                .view_person_details_favourite_place_item, parent, false));
    }

    @Override
    public void onBindViewHolder(FavouritePlacesAdapter.FavouritePlaceViewHolder holder, int position) {
        final FavouritePlace favouritePlace = favouritePlaces.get(position);
        holder.textViewFavouritePlaceName.setText(favouritePlace.getNormalizedPlaceName());
        holder.imageButtonMap.setOnClickListener(v -> {
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, favouritePlace.getGoogleMapsSearchUri());
            v.getContext().startActivity(mapIntent);
        });
    }

    @Override
    public int getItemCount() {
        return favouritePlaces.size();
    }

    @Override
    public long getItemId(int position) {
        return favouritePlaces.get(position).favouritePlaceId.hashCode();
    }

    public void setFavouritePlaces(List<FavouritePlace> favouritePlaces) {
        this.favouritePlaces = favouritePlaces;
        notifyDataSetChanged();
    }


    public static class FavouritePlaceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewFavouritePlaceName)
        TextView textViewFavouritePlaceName;
        @BindView(R.id.imageButtonMap)
        ImageButton imageButtonMap;

        public FavouritePlaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}