package com.bimber.features.profiledetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.data.entities.profile.local.FavouriteTreat;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by srokowski.maciej@gmail.com on 21.12.16.
 */

public class AlcoholsAdapter extends RecyclerView.Adapter<AlcoholsAdapter.AlcoholViewHolder> {

    private List<FavouriteTreat> alcohols = Collections.emptyList();

    public AlcoholsAdapter() {
        setHasStableIds(true);
    }

    @Override
    public AlcoholViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlcoholViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_person_details_alcohol_item, parent,
                false));
    }

    @Override
    public void onBindViewHolder(AlcoholViewHolder holder, int position) {
        final FavouriteTreat alcohol = alcohols.get(position);
        holder.textViewAlcoholName.setText(alcohol.favouriteTreatName);
    }

    @Override
    public int getItemCount() {
        return alcohols.size();
    }

    @Override
    public long getItemId(int position) {
        return alcohols.get(position).favouriteTreatId.hashCode();
    }

    public void setAlcohols(List<FavouriteTreat> alcohols) {
        this.alcohols = alcohols;
        notifyDataSetChanged();
    }


    public static class AlcoholViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewAlcoholName)
        TextView textViewAlcoholName;

        public AlcoholViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
