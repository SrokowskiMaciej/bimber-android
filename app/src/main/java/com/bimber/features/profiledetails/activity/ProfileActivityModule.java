package com.bimber.features.profiledetails.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class ProfileActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(ProfileActivity activity);

    @Binds
    @ActivityScope
    abstract ProfileContract.IProfilePresenter presenter(ProfilePresenter presenter);
}
