package com.bimber.features.profiledetails.activity;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull;
import com.bimber.data.repositories.PersonEvaluationRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.profiledetails.activity.ProfileContract.IProfileView;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 28.12.16.
 */

public class ProfilePresenter extends MvpAbstractPresenter<IProfileView> implements ProfileContract.IProfilePresenter {

    private final String currentUserId;
    private final ProfileDataRepository profileDataRepository;
    private final PersonEvaluationRepository personEvaluationRepository;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ProfilePresenter(@LoggedInUserId String currentUserId, ProfileDataRepository profileDataRepository, PersonEvaluationRepository
            personEvaluationRepository, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.currentUserId = currentUserId;
        this.profileDataRepository = profileDataRepository;
        this.personEvaluationRepository = personEvaluationRepository;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    @Override
    protected void viewAttached(IProfileView view) {

    }

    @Override
    protected void viewDetached(IProfileView view) {
        subscriptions.clear();
    }

    @Override
    public void requestProfileData(String profileId, boolean showEvaluationStatus) {
        requestProfileData(profileId, null, showEvaluationStatus);
    }

    @Override
    public void requestProfileData(String profileId, String animatedPhotoUri, boolean showEvaluationStatus) {
        Flowable<ProfileDataFull> profileDataSource = profileDataRepository.onUserProfileDataFull(profileId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .cache();

        subscriptions.add(profileDataSource
                .map(profileDataThumbnail -> profileDataThumbnail.user.displayName)
                .observeOn(AndroidSchedulers.mainThread())
                .distinctUntilChanged()
                .subscribe(getView()::setProfileName,
                        throwable -> Timber.e(throwable, "Error loading user profile name in toolbar")));

        subscriptions.add(profileDataSource
                .map(profileData -> profileData.user)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setAbout,
                        throwable -> Timber.e(throwable, "Error loading user profile name in toolbar")));

        subscriptions.add(profileDataSource
                .map(profileDataThumbnail -> profileDataThumbnail.user.friends)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setFriendsCount,
                        throwable -> Timber.e(throwable, "Error loading user friends count")));

        subscriptions.add(profileDataSource
                .map(profileData -> profileData.photos)
                .startWith(Collections.<UserPhoto>emptyList())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setPhotos,
                        throwable -> Timber.e(throwable, "Error loading user parties count")));

        subscriptions.add(profileDataSource
                .map(profileData -> profileData.alcohols)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setAlcohols,
                        throwable -> Timber.e(throwable, "Error loading user parties count")));

        subscriptions.add(profileDataSource
                .map(profileData -> profileData.favouritePlaces)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setFavouritePlaces,
                        throwable -> Timber.e(throwable, "Error loading user parties count")));

        subscriptions.add(profileDataSource
                .map(profileDataThumbnail -> profileDataThumbnail.user.parties)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setPartiesCount,
                        throwable -> Timber.e(throwable, "Error loading user parties count")));

        Flowable<String> animatedPhotoSource = animatedPhotoUri != null ? Flowable.just(animatedPhotoUri) : Flowable.empty();
        subscriptions.add(animatedPhotoSource
                .switchIfEmpty(profileDataSource.map(profileDataThumbnail -> profileDataThumbnail.photos.get(0).userPhotoUri))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setProfilePhotoUri, throwable -> {
                    Timber.e(throwable, "Error loading user profile photo in toolbar");
                    getView().showErrorScreen();
                }));

        if (showEvaluationStatus) {
            subscriptions.add(personEvaluationRepository.onEvaluatedPerson(currentUserId, profileId)
                    .map(Key::value)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getView()::showEvaluationStatus, throwable -> {
                        Timber.e(throwable, "Error loading user evaluation status");
                        getView().showErrorScreen();
                    }));

        }
    }


}
