package com.bimber.features.profiledetails.activity;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.MenuItem;

import com.bimber.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Maciek on 23/02/2018.
 */

public enum ProfileMenuItem {
    REPORT(1, 1, R.string.view_profile_details_menu_item_report_title, R.drawable.ic_report_white_24dp, MenuItem.SHOW_AS_ACTION_ALWAYS),
    OPEN_CHAT(2, 2, R.string.view_profile_details_menu_item_explore_title, R.drawable.ic_chat_white_24dp, MenuItem.SHOW_AS_ACTION_ALWAYS),
    FIND_ON_EXPLORE_SCREEN(3, 3, R.string.view_profile_details_menu_item_chat_title, R.drawable.ic_track_changes_white_24dp, MenuItem.SHOW_AS_ACTION_ALWAYS);


    public final int id;

    public final int order;
    @StringRes
    public final int itemTitleRes;
    @DrawableRes
    public final int itemIconRes;

    public final int showAsAction;

    private static final Map<Integer, ProfileMenuItem> typesById = new HashMap<>();

    static {
        for (ProfileMenuItem chatMenuItem : ProfileMenuItem.values()) {
            typesById.put(chatMenuItem.id, chatMenuItem);
        }
    }

    ProfileMenuItem(int id, int order, int itemTitleRes, int itemIconRes, int showAsAction) {
        this.id = id;
        this.order = order;
        this.itemTitleRes = itemTitleRes;
        this.itemIconRes = itemIconRes;
        this.showAsAction = showAsAction;
    }

    public static ProfileMenuItem fromId(int id) {
        return typesById.get(id);
    }
}
