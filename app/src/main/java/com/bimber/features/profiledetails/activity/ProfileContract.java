package com.bimber.features.profiledetails.activity;

import com.bimber.data.entities.PersonEvaluation;
import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

public interface ProfileContract {

    interface IProfileView {

        void setProfileName(String uri);

        void setAbout(User user);

        void setFriendsCount(int count);

        void setPartiesCount(int count);

        void setProfilePhotoUri(String uri);

        void setPhotos(List<UserPhoto> photos);

        void setAlcohols(List<FavouriteTreat> alcohols);

        void setFavouritePlaces(List<FavouritePlace> favouritePlaces);

        void showEvaluationStatus(PersonEvaluation personEvaluation);

        void showErrorScreen();
    }

    interface IProfilePresenter extends MvpPresenter<IProfileView> {

        void requestProfileData(String profileId, boolean showEvaluationStatus);

        void requestProfileData(String profileId, String animatedPhotoUri, boolean showEvaluationStatus);
    }
}
