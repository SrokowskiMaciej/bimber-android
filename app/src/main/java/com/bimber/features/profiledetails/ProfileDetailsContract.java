package com.bimber.features.profiledetails;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 23.03.17.
 */

public interface ProfileDetailsContract {

    interface IProfileDetailsView {
        void setPersonData(ProfileDataFull personData);
    }

    interface IProfileDetailsPresenter extends MvpPresenter<IProfileDetailsView> {
        void requestProfileData(String uId);
    }
}
