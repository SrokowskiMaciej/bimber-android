package com.bimber.features.profiledetails.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.BitmapPool.DrawablePool;
import com.bimber.base.application.BimberApplication;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.PersonEvaluation;
import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.chat.menu.ChatMenuItem;
import com.bimber.features.editprofile.adapters.PhotoThumbnailsAdapter;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;
import com.bimber.features.photogallery.profile.activity.ProfilePhotoGalleryActivity;
import com.bimber.features.profiledetails.AlcoholsAdapter;
import com.bimber.features.profiledetails.FavouritePlacesAdapter;
import com.bimber.features.share.profile.ShareProfileView;
import com.bimber.utils.images.StartPostponedListener;
import com.bimber.utils.view.WeightedAppBarLayout;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import jp.wasabeef.recyclerview.animators.LandingAnimator;

import static com.bimber.features.profiledetails.activity.ProfileContract.IProfileView;

public class ProfileActivity extends BaseLoggedInActivity implements IProfileView {

    private static final String PROFILE_ID_KEY = "PROFILE_ID_KEY";
    private static final String SNACKBAR_CHAT_ID_KEY = "SNACKBAR_CHAT_ID_KEY";
    private static final String ANIMATED_PHOTO_KEY = "ANIMATED_PHOTO_KEY";
    private static final String SHOW_EVALUATION_STATUS_KEY = "SHOW_EVALUATION_STATUS_KEY";

    private static final String SHARE_PROFILE_FRAGMENT_TAG = "SHARE_GROUP_FRAGMENT_TAG";
    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";

    @BindView(R.id.buttonShare)
    FloatingActionButton buttonShare;
    @BindView(R.id.appBarView)
    WeightedAppBarLayout appBarView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageViewProfilePhoto)
    ImageView imageViewProfilePhoto;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.textViewFriendsCount)
    TextView textViewFriendsCount;
    @BindView(R.id.textViewPartiesCount)
    TextView textViewPartiesCount;
    @BindView(R.id.textViewAlcoholsEmpty)
    TextView textViewAlcoholsEmpty;
    @BindView(R.id.textViewPlacesEmpty)
    TextView textViewPlacesEmpty;
    @BindView(R.id.textViewAboutContent)
    TextView textViewAboutContent;
    @BindView(R.id.recyclerViewAlcohols)
    RecyclerView recyclerViewAlcohols;
    @BindView(R.id.recyclerViewFavouritePlaces)
    RecyclerView recyclerViewFavouritePlaces;
    @BindView(R.id.recyclerViewPhotosThumbnails)
    RecyclerView recyclerViewPhotosThumbnails;

    @Inject
    ProfileContract.IProfilePresenter profilePresenter;
    @Inject
    DrawablePool drawablePool;

    private String profileId;
    protected PhotoThumbnailsAdapter userPhotosAdapter;
    private AlcoholsAdapter alcoholsAdapter;
    private FavouritePlacesAdapter favouritePlacesAdapter;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    // MENU
    private Set<ProfileMenuItem> shownMenuItems = new HashSet<>();
    private PersonEvaluation personEvaluation;

    public static Intent newInstance(Context context, String profileId, boolean showEvaluationStatus) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(PROFILE_ID_KEY, profileId);
        intent.putExtra(SHOW_EVALUATION_STATUS_KEY, showEvaluationStatus);
        return intent;
    }

    public static Intent newInstance(Context context, String profileId,
                                     String animatedPhotoUri, boolean showEvaluationStatus) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(PROFILE_ID_KEY, profileId);
        intent.putExtra(ANIMATED_PHOTO_KEY, animatedPhotoUri);
        intent.putExtra(SHOW_EVALUATION_STATUS_KEY, showEvaluationStatus);
        return intent;
    }

    public static Intent newInstance(Context context, String profileId,
                                     String animatedPhotoUri,
                                     String snackbarChatId,
                                     boolean showEvaluationStatus) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(PROFILE_ID_KEY, profileId);
        intent.putExtra(ANIMATED_PHOTO_KEY, animatedPhotoUri);
        intent.putExtra(SNACKBAR_CHAT_ID_KEY, snackbarChatId);
        intent.putExtra(SHOW_EVALUATION_STATUS_KEY, showEvaluationStatus);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        profileId = getIntent().getStringExtra(PROFILE_ID_KEY);
        String animatedPhotoUri = getIntent().getStringExtra(ANIMATED_PHOTO_KEY);
        boolean isAnimated = animatedPhotoUri != null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setupToolbar();
        profilePresenter.bindView(this);
        boolean showEvaluationStatus = getIntent().getBooleanExtra(SHOW_EVALUATION_STATUS_KEY, false);
        if (isAnimated) {
            setupAnimations(animatedPhotoUri);
            profilePresenter.requestProfileData(profileId, animatedPhotoUri, showEvaluationStatus);
        } else {
            profilePresenter.requestProfileData(profileId, showEvaluationStatus);
        }
        setUpSnackbarFragments();
    }

    @Override
    protected void onDestroy() {
        profilePresenter.unbindView(this);
        super.onDestroy();
    }

    private void setUpSnackbarFragments() {
        String chatId = getIntent().getStringExtra(SNACKBAR_CHAT_ID_KEY);
        if (chatId != null && getSupportFragmentManager().findFragmentByTag(NEW_CHAT_MESSASGE_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.rootLayout, NewChatMessageSnackbarFragment.newInstance(HandlingPriority.MEDIUM, chatId),
                            NEW_CHAT_MESSASGE_TAG)
                    .commit();
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        setTitle("");
        final Typeface tf = Typeface.createFromAsset(getAssets(), BimberApplication.DEFAULT_FONT_ASSET_PATH);
        collapsingToolbar.setCollapsedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        shownMenuItems.add(ProfileMenuItem.REPORT);
    }

    private void setupAnimations(String animatedPhotoUri) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            TransitionSet set = null;

            set = new TransitionSet()
                    .addTransition(new ChangeImageTransform().setInterpolator(new FastOutSlowInInterpolator()))
                    .addTransition(new ChangeBounds().setInterpolator(new FastOutSlowInInterpolator()));


            window.setSharedElementEnterTransition(set);
            window.setEnterTransition(new Slide(Gravity.BOTTOM)
                    .excludeTarget(android.R.id.statusBarBackground, true)
                    .excludeTarget(android.R.id.navigationBarBackground, true)
                    .excludeTarget(R.id.toolbar, true)
                    .setInterpolator(new FastOutSlowInInterpolator()));


            ViewCompat.setTransitionName(imageViewProfilePhoto, animatedPhotoUri);
            Drawable drawable = drawablePool.getDrawable(animatedPhotoUri);
            if (drawable != null) {
                //In case we find something in drawable pool
                imageViewProfilePhoto.setImageDrawable(drawable);
            } else {
                ActivityCompat.postponeEnterTransition(this);
            }
        }
    }



    @OnClick(R.id.buttonShare)
    public void onViewClicked() {
        getSupportFragmentManager().beginTransaction().add(ShareProfileView.newInstance(profileId, R.style.AppTheme),
                SHARE_PROFILE_FRAGMENT_TAG).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        for (ProfileMenuItem chatMenuItem : shownMenuItems) {
            MenuItem menuItem = menu.add(0, chatMenuItem.id, chatMenuItem.order, chatMenuItem.itemTitleRes);
            menuItem.setIcon(chatMenuItem.itemIconRes);
            menuItem.setShowAsAction(chatMenuItem.showAsAction);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (ProfileMenuItem.fromId(item.getItemId())){
            case REPORT:
                showReportDialog();
                return true;
            case OPEN_CHAT:
                if(personEvaluation!=null){
                    startActivity(ChatActivity.newInstance(this, personEvaluation.dialogId(), Chattable.ChatType
                            .DIALOG));
                }
                return true;
            case FIND_ON_EXPLORE_SCREEN:
                startActivity(HomeActivity.newInstanceWithProfile(this, profileId));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        appBarView.expand(this::setUpExit);
    }

    private void setUpExit() {
        ((CoordinatorLayout.LayoutParams) buttonShare.getLayoutParams()).setAnchorId(View.NO_ID);
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    public void setProfilePhotoUri(String uri) {
        GlideApp.with(this)
                .load(uri)
                .apply(new RequestOptions().centerCrop())
//                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(new StartPostponedListener(this))
                .into(imageViewProfilePhoto);
    }

    @Override
    public void setPhotos(List<UserPhoto> photos) {
        if (userPhotosAdapter == null) {
            userPhotosAdapter = new PhotoThumbnailsAdapter(GlideApp.with(this));
            recyclerViewPhotosThumbnails.setNestedScrollingEnabled(false);
            recyclerViewPhotosThumbnails.setLayoutManager(new FlexboxLayoutManager(this, FlexDirection.ROW));
            recyclerViewPhotosThumbnails.setItemAnimator(new LandingAnimator());
            recyclerViewPhotosThumbnails.setAdapter(userPhotosAdapter);
            compositeDisposable.add(userPhotosAdapter.clickedPhotos().subscribe(clickedPhotoAction -> {
                ViewCompat.setTransitionName(clickedPhotoAction.clickedView, clickedPhotoAction.photo.photoId);
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                        clickedPhotoAction.clickedView, clickedPhotoAction.photo.photoId);
                startActivity(ProfilePhotoGalleryActivity.newInstance(this, profileId, clickedPhotoAction.photo, R
                        .style.AppTheme), activityOptionsCompat.toBundle());
            }));
        }
        userPhotosAdapter.setPhotos(photos);
    }

    @Override
    public void setProfileName(String name) {
        collapsingToolbar.setTitle(name);
        textViewAlcoholsEmpty.setText(String.format(getString(R.string.view_profile_details_empty_alcohols_text),
                name));
        textViewPlacesEmpty.setText(String.format(getString(R.string.view_profile_details_empty_places_text), name));
    }

    @Override
    public void setAbout(User user) {
        if (user.about.isEmpty()) {
            textViewAboutContent.setText(String.format(getString(R.string.view_profile_details_empty_about_text),
                    user.displayName));
        } else {
            textViewAboutContent.setText(user.about);
        }
    }

    @Override
    public void setFriendsCount(int count) {
        textViewFriendsCount.setText(String.format(getResources().getQuantityString(R.plurals.profile_friends_count,
                count), count));
    }

    @Override
    public void setPartiesCount(int count) {
        textViewPartiesCount.setText(String.format(getResources().getQuantityString(R.plurals.profile_parties_count,
                count), count));
    }

    @Override
    public void setAlcohols(List<FavouriteTreat> alcohols) {
        if (alcohols.isEmpty()) {
            textViewAlcoholsEmpty.setVisibility(View.VISIBLE);
        } else {
            textViewAlcoholsEmpty.setVisibility(View.GONE);

        }
        if (alcoholsAdapter == null) {
            alcoholsAdapter = new AlcoholsAdapter();
            recyclerViewAlcohols.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewAlcohols.setAdapter(alcoholsAdapter);
        }
        alcoholsAdapter.setAlcohols(alcohols);
    }

    @Override
    public void setFavouritePlaces(List<FavouritePlace> favouritePlaces) {
        if (favouritePlaces.isEmpty()) {
            textViewPlacesEmpty.setVisibility(View.VISIBLE);
        } else {
            textViewPlacesEmpty.setVisibility(View.GONE);
        }
        if (favouritePlacesAdapter == null) {
            recyclerViewFavouritePlaces.setLayoutManager(new LinearLayoutManager(this));
            favouritePlacesAdapter = new FavouritePlacesAdapter();
            recyclerViewFavouritePlaces.setAdapter(favouritePlacesAdapter);
        }
        favouritePlacesAdapter.setFavouritePlaces(favouritePlaces);
    }

    @Override
    public void showEvaluationStatus(PersonEvaluation personEvaluation) {
        switch (personEvaluation.status()) {
            case MATCHED:
                shownMenuItems.add(ProfileMenuItem.OPEN_CHAT);
                shownMenuItems.remove(ProfileMenuItem.FIND_ON_EXPLORE_SCREEN);
                this.personEvaluation = personEvaluation;
                break;
            default:
                shownMenuItems.add(ProfileMenuItem.FIND_ON_EXPLORE_SCREEN);
                shownMenuItems.remove(ProfileMenuItem.OPEN_CHAT);

        }
        invalidateOptionsMenu();
    }

    private void showReportDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.view_profile_details_dialog_report_title)
                .content(R.string.view_profile_details_dialog_report_content)
                .positiveText(R.string.dialog_ok_button_text)
                .negativeText(R.string.dialog_cancel_button_text)
                .onPositive((dialog, which) -> sendReport())
                .show();
    }

    private void sendReport() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "bimber.service@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, String.format(getString(R.string.view_profile_details_menu_report_subject), profileId));
        emailIntent.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.view_profile_details_menu_report_body), profileId));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @Override
    public void showErrorScreen() {

    }
}
