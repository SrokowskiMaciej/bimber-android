package com.bimber.features.signin.gdprconsent

import android.arch.lifecycle.ViewModel
import com.bimber.features.signin.SignInRouter
import com.bimber.features.signin.SignInRouter.SignInPhase.*
import javax.inject.Inject

class GdprConsentViewModel
@Inject
constructor(private val signInRouter: SignInRouter) : ViewModel() {


    fun userAgreed() {
        signInRouter.setSignInPhase(AUTH)

    }

    fun userDisagreed() {
        signInRouter.setSignInPhase(CANCELLED)
    }
}