package com.bimber.features.signin.auth

import android.app.Activity
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModel
import android.content.Intent
import com.bimber.data.entities.InitialUserCredentials
import com.bimber.data.repositories.FirebaseAuthService
import com.bimber.data.repositories.InitialUserCredentialsRepository
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource.GDPRConsentStatus.*
import com.bimber.features.signin.SignInRouter
import com.bimber.features.signin.SignInRouter.SignInPhase.*
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.LoginEvent
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class AuthViewModel
@Inject
constructor(
        private val signInRouter: SignInRouter,
        private val authService: FirebaseAuthService,
        private val initialUserCredentialsRepository: InitialUserCredentialsRepository,
        private val gdprConsentNetworkSource: GDPRConsentNetworkSource
) : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    fun userSingInFinished(resultCode: Int, data: Intent?) {

        val idpResponse = IdpResponse.fromResultIntent(data)
        if (resultCode == Activity.RESULT_OK && idpResponse != null) {
            disposables.add(authService.onUserLoggedIn()
                                .map {
                                    val initialUserCredentials = InitialUserCredentials.create(idpResponse.providerType, idpResponse.idpToken)
                                    initialUserCredentialsRepository.setInitialUserCredentials(it.uid, initialUserCredentials)
                                    gdprConsentNetworkSource.setGDPRConsentStatus(it.uid, AGREED_V1)
                                }
                                .subscribeBy(onNext = {
                                    Answers.getInstance().logLogin(LoginEvent().putMethod(idpResponse.providerType).putSuccess(true))
                                    signInRouter.setSignInPhase(SUCCESS)
                                }, onError = {
                                    signInRouter.setSignInPhase(ERROR_UNKNOWN)
                                    Answers.getInstance().logLogin(
                                        LoginEvent().putMethod(idpResponse.providerType)
                                            .putCustomAttribute("error_code", idpResponse.errorCode)
                                            .putSuccess(false)
                                    )
                                })
            )
        } else {
            // Sign in failed
            if (idpResponse == null) {
                // User pressed back button
                signInRouter.setSignInPhase(CANCELLED)
                return
            }
            try {
                Answers.getInstance().logLogin(
                    LoginEvent().putMethod(idpResponse.providerType)
                        .putCustomAttribute("error_code", idpResponse.errorCode)
                        .putSuccess(false)
                )
            } catch (e: Exception) {
                Timber.e(e, "Couldn't log login failure event")
            }


            if (idpResponse.errorCode == ErrorCodes.NO_NETWORK) {
                signInRouter.setSignInPhase(ERROR_NO_INTERNET)
                return
            }
            signInRouter.setSignInPhase(ERROR_UNKNOWN)

        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}