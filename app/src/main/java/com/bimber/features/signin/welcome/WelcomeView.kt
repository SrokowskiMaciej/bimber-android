package com.bimber.features.signin.welcome

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bimber.R
import com.bimber.base.fragments.BaseFragment
import com.bimber.features.signin.SignInSlidesPagerAdapter
import com.viewpagerindicator.CirclePageIndicator
import javax.inject.Inject

class WelcomeView : BaseFragment() {

    @BindView(R.id.buttonSignIn)
    lateinit var buttonSignIn: Button
    @BindView(R.id.navigationViewPager)
    lateinit var viewPager: ViewPager
    @BindView(R.id.tabIndicator)
    lateinit var tabPageIndicator: CirclePageIndicator

    @Inject
    lateinit var welcomeViewModel: WelcomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_welcome, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager.adapter = SignInSlidesPagerAdapter(activity, childFragmentManager)
        tabPageIndicator.setViewPager(viewPager)
    }


    @OnClick(R.id.buttonSignIn)
    fun onSignInClicked() {
        welcomeViewModel.signInClicked()
    }
}