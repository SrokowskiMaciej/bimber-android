package com.bimber.features.signin

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bimber.data.repositories.FirebaseAuthService
import com.bimber.data.sources.profile.location.UserLocationNetworkSource
import com.bimber.features.signin.SignInRouter.SignInPhase
import com.bimber.features.signin.SignInRouter.SignInPhase.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SignInViewModel
@Inject
constructor(private val signInRouter: SignInRouter,
            private val authService: FirebaseAuthService,
            private val locationNetworkSource: UserLocationNetworkSource) : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    val nextScreen: MutableLiveData<NextScreen> = MutableLiveData()
    val signInPhase: MutableLiveData<SignInPhase> = MutableLiveData()

    fun load() {
        signInRouter.setSignInPhase(WELCOME)
        disposables.add(signInRouter.currentSingInPhase()
                .subscribeBy(onNext = {
                    signInPhase.postValue(it)
                }))
    }

    fun signalAuthSuccess() {
        disposables.add(authService.onUserLoggedInUid()
                .firstOrError()
                .flatMap { locationNetworkSource.userHasAnyLocations(it) }
                .timeout(6, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onSuccess = {
                    if (it) {
                        nextScreen.postValue(NextScreen.HOME_SCREEN)
                    } else {
                        nextScreen.postValue(NextScreen.LOCATION_PICKER)
                    }
                }, onError = {
                    signInRouter.setSignInPhase(WELCOME)
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    enum class NextScreen {
        NONE,
        LOCATION_PICKER,
        HOME_SCREEN
    }

}