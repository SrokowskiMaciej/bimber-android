package com.bimber.features.signin;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bimber.R;

/**
 * Pager adapter showing three fragments on a Sign In screen
 * Created by srokowski.maciej@gmail.com on 23.10.16.
 */
public class SignInSlidesPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public SignInSlidesPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_explore_people),
                        R.drawable.img_promotional_people_discovery);
            case 1:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_match),
                        R.drawable.img_promotional_match);
            case 2:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_chat_person),
                        R.drawable.img_promotional_person_chat);
            case 3:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_explore_group),
                        R.drawable.img_promotional_group_discovery);
            case 4:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_chat_group),
                        R.drawable.img_promotional_group_chat);
            case 5:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_group_join_request),
                        R.drawable.img_promotional_group_join_requests);
            case 6:
                return TitleImageFragment.newInstance(context.getString(R.string.view_sign_in_promotional_text_chat_group),
                        R.drawable.img_promotional_profile);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 7;
    }
}
