package com.bimber.features.signin.gdprconsent

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bimber.R
import com.bimber.base.ViewModelFactory
import com.bimber.base.fragments.BaseFragment
import com.bimber.base.getViewModel
import com.bimber.base.withViewModel
import com.bimber.utils.io.readTextAndClose
import javax.inject.Inject


class GdprConsentView : BaseFragment() {


    @BindView(R.id.buttonDisagree)
    lateinit var buttonDisagree: Button
    @BindView(R.id.buttonAgree)
    lateinit var buttonAgree: Button
    @BindView(R.id.webViewDataProtectionContent)
    lateinit var webViewDataProtectionContent: WebView

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_gdpr_sign_in, container, false)
        ButterKnife.bind(this, view)
        webViewDataProtectionContent.loadData(resources.openRawResource(R.raw.gdpr_consent).readTextAndClose(), "text/html; charset=utf-8", "UTF-8")
        return view
    }

    @OnClick(R.id.buttonDisagree, R.id.buttonAgree)
    fun onButtonClicked(view: View) {
        when (view.id) {
            R.id.buttonAgree -> getViewModel<GdprConsentViewModel>(viewModelFactory).userAgreed()
            R.id.buttonDisagree -> getViewModel<GdprConsentViewModel>(viewModelFactory).userDisagreed()
        }
    }
}