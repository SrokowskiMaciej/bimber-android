package com.bimber.features.signin.auth

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.widget.Toast
import com.bimber.R
import com.bimber.base.ViewModelFactory
import com.bimber.base.fragments.BaseFragment
import com.bimber.base.getViewModel
import com.firebase.ui.auth.AuthUI
import java.util.*
import javax.inject.Inject

class AuthView : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setTheme(R.style.AppTheme)
                //                .setLogo(R.drawable.ic_bimber_144dp)
                .setAvailableProviders(Arrays.asList<AuthUI.IdpConfig>(
                        AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                        AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()))

                .setIsSmartLockEnabled(false)
                .build()
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivityForResult(intent, RC_SIGN_IN)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            getViewModel<AuthViewModel>(viewModelFactory).userSingInFinished(resultCode, data)
            return
        }

    }

    companion object {
        private const val RC_SIGN_IN = 100
    }
}