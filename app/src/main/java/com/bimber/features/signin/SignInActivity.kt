package com.bimber.features.signin

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.FrameLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.bimber.R
import com.bimber.base.ViewModelFactory
import com.bimber.base.activities.BaseActivity
import com.bimber.base.getViewModel
import com.bimber.base.observe
import com.bimber.base.withViewModel
import com.bimber.features.home.HomeNavigator
import com.bimber.features.home.HomeNavigator.HomeScreen.*
import com.bimber.features.home.activity.HomeActivity
import com.bimber.features.location.activity.LocationPickerActivity
import com.bimber.features.signin.SignInRouter.SignInPhase.*
import com.bimber.features.signin.auth.AuthView
import com.bimber.features.signin.gdprconsent.GdprConsentView
import com.bimber.features.signin.welcome.WelcomeView
import com.bimber.utils.arch.nonNull
import com.bimber.utils.arch.observe
import javax.inject.Inject

class SignInActivity : BaseActivity() {


    @BindView(R.id.content)
    lateinit var content: FrameLayout

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        ButterKnife.bind(this)
        getViewModel<SignInViewModel>(viewModelFactory).load()
        withViewModel<SignInViewModel>(viewModelFactory) {
            signInPhase.nonNull().observe(this@SignInActivity, {
                when (it) {
                    NONE -> {
                        supportFragmentManager.beginTransaction()
                                .addToBackStack(NONE.name)
                                .commit()
                    }
                    WELCOME -> {
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.content, WelcomeView())
                                .addToBackStack(WELCOME.name)
                                .commit()
                    }
                    GDPR_CONSENT -> {
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.content, GdprConsentView())
                                .addToBackStack(GDPR_CONSENT.name)
                                .commit()
                    }
                    AUTH -> {
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.content, AuthView())
                                .addToBackStack(AUTH.name)
                                .commit()
                    }
                    SUCCESS -> {
                        supportFragmentManager.popBackStack(NONE.name, 0)
                        this.signalAuthSuccess()
                    }
                    CANCELLED -> {
                        supportFragmentManager.popBackStack(WELCOME.name, 0)
                    }
                    ERROR_NO_INTERNET -> {
                        supportFragmentManager.popBackStack(WELCOME.name, 0)
                    }
                    ERROR_UNKNOWN -> {
                        supportFragmentManager.popBackStack(WELCOME.name, 0)
                    }
                }
            })

            nextScreen.nonNull().observe(this@SignInActivity, {
                when (it) {
                    SignInViewModel.NextScreen.NONE -> {

                    }
                    SignInViewModel.NextScreen.LOCATION_PICKER -> {
                        startActivity(LocationPickerActivity.newInstance(this@SignInActivity, R.style.AppThemeLight, R.style.AppTheme, true))
                        finish()
                    }
                    SignInViewModel.NextScreen.HOME_SCREEN -> {
                        finish()
                    }
                }
            })

        }

    }

    override fun onBackPressed() {
        ActivityCompat.finishAffinity(this)
        super.onBackPressed()
    }
}
