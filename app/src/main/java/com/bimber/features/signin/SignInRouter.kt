package com.bimber.features.signin

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SignInRouter
@Inject
constructor() {

    private val signInPhase: BehaviorSubject<SignInPhase> = BehaviorSubject.createDefault(SignInPhase.NONE)

    fun currentSingInPhase(): Observable<SignInPhase> {
        return signInPhase
    }

    fun setSignInPhase(phase: SignInPhase) {
        signInPhase.onNext(phase)
    }

    enum class SignInPhase {
        NONE,
        WELCOME,
        GDPR_CONSENT,
        AUTH,
        SUCCESS,
        CANCELLED,
        ERROR_NO_INTERNET,
        ERROR_UNKNOWN,
    }

}