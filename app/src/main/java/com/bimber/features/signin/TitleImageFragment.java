package com.bimber.features.signin;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment displaying simple Title and image duo.
 * Created by srokowski.maciej@gmail.com on 22.10.16.
 */

public class TitleImageFragment extends Fragment {


    public static final String EXTRA_IMAGE_RESOURCE = "EXTRA_IMAGE_RESOURCE";
    public static final String EXTRA_TITLE = "EXTRA_TITLE";
    @BindView(R.id.titleTextView)
    TextView titleTextView;
    @BindView(R.id.imageView)
    ImageView imageView;

    public static TitleImageFragment newInstance(String title, @DrawableRes int imageResource) {

        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_IMAGE_RESOURCE, imageResource);

        TitleImageFragment fragment = new TitleImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_title_image, null);
        ButterKnife.bind(this, view);
        String title = getArguments().getString(EXTRA_TITLE);
        int imageResource = getArguments().getInt(EXTRA_IMAGE_RESOURCE);
        titleTextView.setText(title);
        Glide.with(this).load(imageResource).into(imageView);
        return view;
    }
}
