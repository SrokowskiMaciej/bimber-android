package com.bimber.features.signin.welcome

import android.arch.lifecycle.ViewModel
import com.bimber.features.signin.SignInRouter
import com.bimber.features.signin.SignInRouter.SignInPhase.*
import javax.inject.Inject


class WelcomeViewModel
@Inject
constructor(private val signInRouter: SignInRouter): ViewModel() {

    fun signInClicked() {
        signInRouter.setSignInPhase(GDPR_CONSENT)
    }

}