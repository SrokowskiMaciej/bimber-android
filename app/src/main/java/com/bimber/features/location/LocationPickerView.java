package com.bimber.features.location;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.entities.profile.network.LocationType;
import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.data.geocoding.ReverseGeocodingRepository;
import com.bimber.features.common.ProgressView;
import com.bimber.features.location.LocationPickerContract.ILocationChooserView;
import com.bimber.features.location.LocationPickerContract.ILocationPickerPresenter;
import com.bimber.utils.view.ViewUtils;
import com.github.kevelbreh.androidunits.AndroidUnit;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.patloew.rxlocation.RxLocation;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by srokowski.maciej@gmail.com on 15.11.16.
 */

public class LocationPickerView extends BaseFragment implements ILocationChooserView {

    private static final int PLACE_PICKER_REQUEST_CODE = 666;
    private static final int LOCATION_SETTINGS_REQUEST_CODE = 999;
    private static final String THEME_KEY = "THEME_KEY";

    private static final LocationRequest LOCATION_REQUEST = LocationRequest.create()
            .setNumUpdates(1)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    public static final String SHOW_BOTTOM_NAVIGATION_KEY = "SHOW_BOTTOM_NAVIGATION_KEY";

    @Inject
    ILocationPickerPresenter locationChooserPresenter;
    @Inject
    ReverseGeocodingRepository reverseGeocodingRepository;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottomNavigationFrame)
    FrameLayout bottomNavigationFrame;
    @BindView(R.id.buttonNext)
    Button buttonNext;
    @BindView(R.id.progressView)
    ProgressView progressView;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.buttonRange)
    Button buttonRange;
    @BindView(R.id.switchCurrentLocation)
    Switch switchCurrentLocation;
    @BindView(R.id.switchCustomLocation)
    Switch switchCustomLocation;
    @BindView(R.id.textViewSelectedLocation)
    TextView textViewSelectedLocation;
    @BindView(R.id.progressViewLocationLoading)
    ProgressView progressViewLocationLoading;

    private CompositeDisposable subscriptions = new CompositeDisposable();
    private Circle circle;

    public static LocationPickerView newInstance(@StyleRes int theme, boolean showBottomNavigation) {
        Bundle bundle = new Bundle();
        bundle.putInt(THEME_KEY, theme);
        bundle.putBoolean(SHOW_BOTTOM_NAVIGATION_KEY, showBottomNavigation);
        LocationPickerView locationPickerView = new LocationPickerView();
        locationPickerView.setArguments(bundle);
        return locationPickerView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getActivity(), getArguments().getInt(THEME_KEY));
        inflater = inflater.cloneInContext(contextThemeWrapper);
        View view = inflater.inflate(R.layout.view_location_picker, container, false);
        ButterKnife.bind(this, view);
        ViewUtils.applyFixForDrawableLeftTint(buttonRange, R.color.colorPrimary);
        setupToolbar();
        setupBottomNavigation();
        MapsInitializer.initialize(getContext().getApplicationContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(googleMap -> googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL));
        locationChooserPresenter.bindView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mapView.onDestroy();
        subscriptions.clear();
        locationChooserPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void setLocation(List<UserLocationModel> userLocationModel) {
        progressView.setVisibility(View.GONE);

        if (!userLocationModel.isEmpty()) {
            UserLocationModel nonEmptyUserLocationModel = userLocationModel.get(0);
            textViewSelectedLocation.setText(nonEmptyUserLocationModel.locationName());
            mapView.getMapAsync(googleMap -> {
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.setOnMapClickListener(latLng -> {
                });
                LatLng latLng = new LatLng(nonEmptyUserLocationModel.latitude(), nonEmptyUserLocationModel.longitude());
                int circleColor = ContextCompat.getColor(mapView.getContext(), R.color.colorTranslucentMapCircle);
                if (circle != null) {
                    circle.remove();
                    circle = null;
                }
                circle = googleMap.addCircle(new CircleOptions().center(latLng)
                        .radius(nonEmptyUserLocationModel.range() * 1000)
                        .fillColor(circleColor)
                        .strokeColor(circleColor));
                mapView.post(() -> googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(ViewUtils.calculateBounds(latLng,
                        nonEmptyUserLocationModel.range() * 1000), 0)));
            });
            buttonRange.setVisibility(View.VISIBLE);
            buttonRange.setText(String.format(getString(R.string.view_location_picker_range_button),
                    String.valueOf(nonEmptyUserLocationModel.range())));
            buttonRange.setOnClickListener(view -> showRangePickerDialog(nonEmptyUserLocationModel));
            switch (nonEmptyUserLocationModel.type()) {
                case CUSTOM_LOCATION:
                    clearCurrentLocation();
                    setCustomLocation();

                    break;
                case CURRENT_LOCATION:
                    clearCustomLocation();
                    setCurrentLocation();
                    break;
            }


        } else {

            mapView.getMapAsync(googleMap -> {
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                if (circle != null) {
                    circle.remove();
                    circle = null;
                }
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                googleMap.setOnMapClickListener(latLng -> {
                });
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0.0, 0.0), 1));
            });
            buttonRange.setVisibility(View.GONE);
            buttonRange.setOnClickListener(null);
            textViewSelectedLocation.setText(R.string.view_location_picker_no_location_title);
            clearCustomLocation();
            clearCurrentLocation();
        }
    }

    private void setupBottomNavigation() {
        if (getArguments().getBoolean(SHOW_BOTTOM_NAVIGATION_KEY)) {
            bottomNavigationFrame.setVisibility(View.VISIBLE);
            ViewUtils.applyFixForDrawableRightTintColor(buttonNext, buttonNext.getCurrentTextColor());
        }
    }

    private void setupToolbar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle("");
        if (!getArguments().getBoolean(SHOW_BOTTOM_NAVIGATION_KEY)) {
            bottomNavigationFrame.setVisibility(View.GONE);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(backView -> activity.onBackPressed());
        }
    }


    private void setCurrentLocation() {
        switchCurrentLocation.setOnCheckedChangeListener(null);
        switchCurrentLocation.setChecked(true);
        switchCurrentLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchCurrentLocation.setOnCheckedChangeListener(null);
            locationChooserPresenter.removeLocation();
        });
    }

    private void setCustomLocation() {
        switchCustomLocation.setOnCheckedChangeListener(null);
        switchCustomLocation.setChecked(true);
        switchCustomLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchCustomLocation.setOnCheckedChangeListener(null);
            locationChooserPresenter.removeLocation();
        });
    }

    private void clearCurrentLocation() {
        switchCurrentLocation.setOnCheckedChangeListener(null);
        switchCurrentLocation.setChecked(false);
        switchCurrentLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchCurrentLocation.setOnCheckedChangeListener(null);
            startCurrentLocationFlow();
        });
    }

    private void clearCustomLocation() {
        switchCustomLocation.setOnCheckedChangeListener(null);
        switchCustomLocation.setChecked(false);
        switchCustomLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchCustomLocation.setOnCheckedChangeListener(null);
            startPlacePicker();
        });

    }

    private void showRangePickerDialog(UserLocationModel userLocationModel) {
        final String rangeMessageFormat = getString(R.string.view_location_picker_range_content);

        Context context = getView().getContext();

        final LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        final AppCompatSeekBar seekBar = new AppCompatSeekBar(context);
        seekBar.setMax(19);
        seekBar.setProgress(userLocationModel.range() - 1);


        final TextView textView = new TextView(context);
        textView.setText(String.format(rangeMessageFormat, userLocationModel.range()));
        textView.setGravity(Gravity.CENTER);
        int textMargins = (int) AndroidUnit.DENSITY_PIXELS.toPixels(8);
        textView.setPadding(0, textMargins, 0, textMargins);

        linearLayout.addView(seekBar);
        linearLayout.addView(textView);

        MaterialDialog materialDialog = new MaterialDialog.Builder(context)
                .customView(linearLayout, false)
                .title(R.string.view_location_picker_range_title)
                .positiveText(R.string.dialog_ok_button_text)
                .onPositive((dialog, which) -> {
                    locationChooserPresenter.setLocation(userLocationModel.type(), userLocationModel.latitude(),
                            userLocationModel.longitude(), seekBar.getProgress() + 1, userLocationModel.locationName());
                })
                .build();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textView.setText(String.format(rangeMessageFormat, i + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        materialDialog.show();

    }

    @Override
    public void showErrorScreen() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_fault_text)
                .onNeutral((dialog, which) -> ViewUtils.finishActivitySafely(getActivity()))
                .neutralText(R.string.view_error_action_ok_text)
                .show();

    }

    @Override
    public void showAddingLocationError(LocationType locationType, Throwable throwable) {
        switch (locationType) {
            case CUSTOM_LOCATION:
                Toast.makeText(getContext(), "Adding location failed :/", Toast.LENGTH_LONG).show();
                clearCustomLocation();
                break;
            case CURRENT_LOCATION:
                Toast.makeText(getContext(), "Setting current location failed :/", Toast.LENGTH_LONG).show();
                clearCurrentLocation();
                break;
        }
    }

    public void startPlacePicker() {
        try {
            PlacePicker.IntentBuilder placePickerIntentBuilder = new PlacePicker.IntentBuilder();
            Intent intent = placePickerIntentBuilder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Timber.e(e, "failed to start place picker");
            showAddingLocationError(LocationType.CUSTOM_LOCATION, e);
        }

    }

    public void startCurrentLocationFlow() {
        subscriptions.add(getLocation()
                .doOnSubscribe(disposable -> progressViewLocationLoading.setVisibility(View.VISIBLE))
                .flatMap(location -> reverseGeocodingRepository.geocode(getContext(), location.getLatitude(), location.getLongitude())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSuccess(address -> locationChooserPresenter.setLocation(LocationType.CURRENT_LOCATION, location
                                .getLatitude(), location.getLongitude(), 25, address)))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent((addresses, throwable) -> progressViewLocationLoading.setVisibility(View.GONE))
                .subscribe(address -> Timber.d("Location obtained"),
                        throwable -> {
                            showAddingLocationError(LocationType.CURRENT_LOCATION, throwable);
                            Timber.e(throwable, "Failed to obtain current location");
                        }));
    }

    @SuppressWarnings({"MissingPermission"})
    private Single<android.location.Location> getLocation() {
        final RxLocation rxLocation = new RxLocation(getContext());
        return new RxPermissions(getActivity())
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribeOn(Schedulers.io())
                .flatMapSingle(permissionProvided -> {
                    if (permissionProvided) {
                        return rxLocation.settings().checkAndHandleResolution(LOCATION_REQUEST);
                    } else {
                        throw new IllegalStateException("Location permission not provided");
                    }
                })
                .flatMap(locationSettingsResult -> rxLocation.location().updates(LOCATION_REQUEST))
                .take(1)
                .timeout(10, TimeUnit.SECONDS)
                .singleOrError();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SETTINGS_REQUEST_CODE:
                //TODO Handle locations settings
                switch (resultCode) {
                    case RESULT_OK:
                        Timber.e("User agreed to locations settings request");
                        break;
                    case RESULT_CANCELED:
                        clearCurrentLocation();
                        Timber.e("User cancelled locations settings request");
                        break;
                    default:
                        break;
                }
                break;
            case PLACE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    final LatLng latLng = PlacePicker.getPlace(getActivity(), data).getLatLng();
                    subscriptions.add(reverseGeocodingRepository.geocode(getContext(), latLng.latitude, latLng.longitude)
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSubscribe(disposable -> progressViewLocationLoading.setVisibility(View.VISIBLE))
                            .doOnSuccess(address -> locationChooserPresenter.setLocation(LocationType.CUSTOM_LOCATION, latLng.latitude,
                                    latLng.longitude, 20, address))
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnEvent((addresses, throwable) -> progressViewLocationLoading.setVisibility(View.GONE))
                            .subscribe(location -> Timber.i("Custom location obtained"),
                                    throwable -> {
                                        showAddingLocationError(LocationType.CUSTOM_LOCATION, throwable);
                                        Timber.e(throwable, "Error while inserting custom locations");
                                    }));
                } else {
                    clearCustomLocation();
                }
                break;
        }
    }

    @OnClick(R.id.buttonNext)
    public void onViewClicked() {
        ViewUtils.finishActivitySafely(getActivity());
    }
}
