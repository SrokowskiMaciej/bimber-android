package com.bimber.features.location.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.location.LocationPickerContract.ILocationPickerPresenter;
import com.bimber.features.location.LocationPickerPresenter;
import com.bimber.features.location.LocationPickerView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 15.11.16.
 */

@Module
public abstract class LocationPickerModule {

    @Binds
    abstract ILocationPickerPresenter locationPickerPresenter(LocationPickerPresenter locationPickerPresenter);


    @FragmentScope
    @ContributesAndroidInjector
    abstract LocationPickerView contributeYourActivityInjector();
}
