package com.bimber.features.location;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.entities.profile.network.LocationType;
import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.data.sources.profile.location.UserLocationNetworkSource;
import com.bimber.features.location.LocationPickerContract.ILocationChooserView;
import com.bimber.features.location.LocationPickerContract.ILocationPickerPresenter;
import com.bimber.utils.mvp.MvpAbstractPresenter;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * ILocationPickerPresenter managing screen where currentUserId chooses locations
 * Created by srokowski.maciej@gmail.com on 15.11.16.
 */

public class LocationPickerPresenter extends MvpAbstractPresenter<ILocationChooserView> implements ILocationPickerPresenter {

    private final String currentUserId;
    private final UserLocationNetworkSource locationRepository;
    private final BimberAnalytics bimberAnalytics;

    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public LocationPickerPresenter(@LoggedInUserId String currentUserId, UserLocationNetworkSource locationRepository, BimberAnalytics bimberAnalytics) {
        this.currentUserId = currentUserId;
        this.locationRepository = locationRepository;
        this.bimberAnalytics = bimberAnalytics;
    }

    @Override
    protected void viewAttached(ILocationChooserView view) {
        subscriptions.add(locationRepository.onLocationEvents(currentUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(view::setLocation)
                .subscribe(locations -> Timber.d("Successfully delivered locations"), throwable -> getView().showErrorScreen()));
    }

    @Override
    protected void viewDetached(ILocationChooserView locationChooserView) {
        subscriptions.clear();
    }

    @Override
    public void setLocation(LocationType locationType, double latitude, double longitude, int range, String formattedLocation) {
        bimberAnalytics.logLocationSet(locationType.toString(), formattedLocation);
        subscriptions.add(Single.just(UserLocationModel.create(locationType, currentUserId, latitude, longitude, range,
                formattedLocation))
                .flatMap(location -> locationRepository.setLocation(currentUserId, location))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locationKey -> Timber.d("Added new location"),
                        throwable -> {
                            Timber.e(throwable, "Failed to setDiscoverableUsers new location");
                            getView().showAddingLocationError(locationType, throwable);
                        }));

    }

    @Override
    public void removeLocation() {
        subscriptions.add(locationRepository.removeLocation(currentUserId)
                .subscribe(() -> Timber.i("Location removed"),
                        throwable -> Timber.e(throwable, "Failed to remove location")));
    }

}
