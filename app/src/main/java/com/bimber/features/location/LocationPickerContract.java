package com.bimber.features.location;

import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.data.entities.profile.network.LocationType;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by srokowski.maciej@gmail.com on 03.12.16.
 */

public interface LocationPickerContract {
    interface ILocationChooserView {
        void setLocation(List<UserLocationModel> userLocationModel);

        void showErrorScreen();

        void showAddingLocationError(LocationType locationType, Throwable throwable);
    }

    interface ILocationPickerPresenter extends MvpPresenter<ILocationChooserView> {

        void setLocation(LocationType locationType, double latitude, double longitude, int range, String formattedLocation);

        void removeLocation();
    }

}
