package com.bimber.features.location.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.location.LocationPickerView;

/**
 * Created by maciek on 20.06.17.
 */

public class LocationPickerActivity extends BaseLoggedInActivity {


    private static final String ACTIVITY_THEME_KEY = "ACTIVITY_THEME_KEY";

    public static final String SHOW_BOTTOM_NAVIGATION_KEY = "SHOW_BOTTOM_NAVIGATION_KEY";
    private static final String VIEW_THEME_KEY = "VIEW_THEME_KEY";

    public static Intent newInstance(Context context, @StyleRes int activityTheme, @StyleRes int viewTheme, boolean showBottomNavigation) {
        Intent intent = new Intent(context, LocationPickerActivity.class);
        intent.putExtra(ACTIVITY_THEME_KEY, activityTheme);
        intent.putExtra(VIEW_THEME_KEY, viewTheme);
        intent.putExtra(SHOW_BOTTOM_NAVIGATION_KEY, showBottomNavigation);
        return intent;
    }


    public static final String LOCATION_PICKER_FRAGMENT = "LOCATION_PICKER_FRAGMENT";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(getIntent().getIntExtra(ACTIVITY_THEME_KEY, R.style.AppTheme));
        setContentView(R.layout.activity_single_pane);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (getSupportFragmentManager().findFragmentByTag(LOCATION_PICKER_FRAGMENT) == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, LocationPickerView.newInstance(getIntent().getIntExtra(VIEW_THEME_KEY, R.style.AppTheme),
                            getIntent().getBooleanExtra(SHOW_BOTTOM_NAVIGATION_KEY, false)), LOCATION_PICKER_FRAGMENT)
                    .commit();
        }
    }
}
