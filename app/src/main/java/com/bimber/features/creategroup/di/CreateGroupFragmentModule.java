package com.bimber.features.creategroup.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.creategroup.CreateGroupView;
import com.bimber.features.creategroup.steps.description.DescriptionStepModule;
import com.bimber.features.creategroup.steps.location.LocationStepModule;
import com.bimber.features.creategroup.steps.name.NameStepModule;
import com.bimber.features.creategroup.steps.participants.ParticipantsStepModule;
import com.bimber.features.creategroup.steps.time.TimeStepModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class CreateGroupFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = {CreateGroupModule.class, NameStepModule.class, DescriptionStepModule
            .class, LocationStepModule.class, TimeStepModule.class, ParticipantsStepModule.class})
    abstract CreateGroupView createGroupView();
}
