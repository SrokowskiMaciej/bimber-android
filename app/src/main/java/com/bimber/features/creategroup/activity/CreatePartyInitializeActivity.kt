package com.bimber.features.creategroup.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bimber.R
import com.bimber.base.activities.BaseActivity
import com.bimber.data.analytics.BimberAnalytics
import com.bimber.data.analytics.BimberAnalytics.CreatePartyStage.CREATE_PARTY_SET_NAME
import kotlinx.android.synthetic.main.activity_create_party_initialize.*
import javax.inject.Inject

class CreatePartyInitializeActivity : BaseActivity(), View.OnClickListener {


    @Inject
    lateinit var bimberAnalytics: BimberAnalytics;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_party_initialize)
        buttonNext.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        bimberAnalytics.logPartyCreationStage(CREATE_PARTY_SET_NAME)
        val preAddedUId: String? = intent.getStringExtra(PRE_ADDED_UID_KEY)
        if (preAddedUId != null) {
            startActivity(CreateGroupActivity.newInstance(this, preAddedUId))
        } else {
            startActivity(CreateGroupActivity.newInstance(this))
        }
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left)
    }

    companion object {

        val PRE_ADDED_UID_KEY = "PRE_ADDED_UID_KEY"

        fun newInstance(context: Context): Intent {
            return Intent(context, CreatePartyInitializeActivity::class.java)
        }

        @JvmStatic
        fun newInstance(context: Context, preAddedUid: String): Intent {
            val intent = Intent(context, CreatePartyInitializeActivity::class.java)
            intent.putExtra(PRE_ADDED_UID_KEY, preAddedUid)
            return intent
        }
    }

}
