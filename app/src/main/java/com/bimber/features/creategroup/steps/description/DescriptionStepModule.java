package com.bimber.features.creategroup.steps.description;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class DescriptionStepModule {

    @ContributesAndroidInjector
    abstract DescriptionStepView createGroupView();
}
