package com.bimber.features.creategroup;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.data.entities.Place;
import com.bimber.features.creategroup.steps.time.Date;
import com.bimber.features.creategroup.steps.time.Time;

/**
 * Created by maciek on 16.03.17.
 */
public class CreateGroupStepperStateManager {

    public static final String DESCRIPTION_KEY = "DESCRIPTION_KEY";
    public static final String NAME_KEY = "NAME_KEY";
    public static final String TIME_KEY = "TIME_KEY";
    public static final String DATE_KEY = "DATE_KEY";
    public static final String PLACE_KEY = "PLACE_KEY";
    private Time time;
    private Date date;
    private Place meetingPlace;
    private String groupDescription;
    private String groupName;

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public void setMeetingPlace(Place meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Nullable
    public Time getTime() {
        return time;
    }

    @Nullable
    public Date getDate() {
        return date;
    }

    @Nullable
    public Place getMeetingPlace() {
        return meetingPlace;
    }

    @Nullable
    public String getGroupDescription() {
        return groupDescription;
    }

    @Nullable
    public String getGroupName() {
        return groupName;
    }

    public void saveInstanceState(Bundle outState) {
        outState.putString(NAME_KEY, groupName);
        outState.putString(DESCRIPTION_KEY, groupDescription);
        outState.putParcelable(TIME_KEY, time);
        outState.putParcelable(DATE_KEY, date);
        outState.putParcelable(PLACE_KEY, meetingPlace);

    }

    public void restoreInstanceState(Bundle savedInstanceState) {
        groupName = savedInstanceState.getString(NAME_KEY);
        groupDescription = savedInstanceState.getString(DESCRIPTION_KEY);
        time = savedInstanceState.getParcelable(TIME_KEY);
        date = savedInstanceState.getParcelable(DATE_KEY);
        meetingPlace = savedInstanceState.getParcelable(PLACE_KEY);

    }
}
