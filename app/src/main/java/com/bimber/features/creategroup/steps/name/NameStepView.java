package com.bimber.features.creategroup.steps.name;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import io.reactivex.subjects.PublishSubject;

import static com.bimber.data.analytics.BimberAnalytics.CreatePartyStage.CREATE_PARTY_SET_DESCRIPTION;

/**
 * Created by maciek on 13.03.17.
 */

public class NameStepView extends BaseFragment implements Step {


    @BindView(R.id.textInputEditTextGroupName)
    TextInputEditText textInputEditTextGroupName;
    @BindView(R.id.textInputLayoutGroupName)
    TextInputLayout textInputLayoutGroupName;

    private final PublishSubject<RxFirebaseUtils.Event> onProceed = PublishSubject.create();

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;
    @Inject
    BimberAnalytics bimberAnalytics;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group_set_name_step, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public VerificationError verifyStep() {
        String groupName = textInputEditTextGroupName.getText().toString();
        if (TextUtils.isEmpty(groupName)) {
            return new VerificationError(getString(R.string.view_create_group_step_name_error_text));
        } else {
            createGroupStepperStateManager.setGroupName(groupName);
            bimberAnalytics.logPartyCreationStage(CREATE_PARTY_SET_DESCRIPTION);
            return null;
        }
    }

    @Override
    public void onSelected() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            textInputEditTextGroupName.requestFocus();
            textInputEditTextGroupName.requestFocusFromTouch();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(textInputEditTextGroupName, InputMethodManager.SHOW_IMPLICIT);
        }, 100);
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        textInputLayoutGroupName.setError(error.getErrorMessage());
        ObjectAnimator
                .ofFloat(textInputLayoutGroupName, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                .setDuration(300)
                .start();
    }

    @OnEditorAction(R.id.textInputEditTextGroupName)
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onProceed.onNext(RxFirebaseUtils.Event.INSTANCE);
        }
        return true;
    }

    public PublishSubject<RxFirebaseUtils.Event> onProceed() {
        return onProceed;
    }
}
