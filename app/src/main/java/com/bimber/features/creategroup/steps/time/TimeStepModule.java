package com.bimber.features.creategroup.steps.time;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class TimeStepModule {

    @ContributesAndroidInjector
    abstract TimeStepView createGroupView();
}
