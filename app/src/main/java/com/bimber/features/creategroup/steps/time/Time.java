package com.bimber.features.creategroup.steps.time;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 15.03.17.
 */
@AutoValue
public abstract class Time implements Parcelable {

    public static Time create(int hourOfDay, int minute, int second) {
        return new AutoValue_Time(hourOfDay, minute, second);
    }

    public abstract int hourOfDay();

    public abstract int minute();

    public abstract int second();
}
