package com.bimber.features.creategroup.steps.location;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.entities.Place;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.bimber.data.analytics.BimberAnalytics.CreatePartyStage.CREATE_PARTY_SET_TIME;

/**
 * Created by maciek on 13.03.17.
 */

public class LocationStepView extends BaseFragment implements Step {

    public static final int PLACE_PICKER_REQUEST_CODE = 7849;
    public static final String SELECTED_PLACE = "SELECTED_PLACE";
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.buttonLocationName)
    Button buttonLocationName;
    @BindView(R.id.textViewLocationContent)
    TextView textViewLocationContent;

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;
    @Inject
    BimberAnalytics bimberAnalytics;

    private Place place;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group_set_location_step, container, false);
        ButterKnife.bind(this, view);
        MapsInitializer.initialize(mapView.getContext().getApplicationContext());
        mapView.onCreate(savedInstanceState);
        mapView.setClickable(false);
        mapView.setOnTouchListener(null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapView.getMapAsync(googleMap -> {
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setAllGesturesEnabled(false);
            googleMap.setOnMapClickListener(latLng -> {
            });
            if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_PLACE)) {
                place = savedInstanceState.getParcelable(SELECTED_PLACE);
                showPlace(googleMap, place);
            } else {
                showWholeWorld(googleMap);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
        if (place != null) {
            outState.putParcelable(SELECTED_PLACE, place);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public VerificationError verifyStep() {
        if (place == null) {
            return new VerificationError("You have to chose a location where you will meet");
        } else {
            createGroupStepperStateManager.setMeetingPlace(place);
            bimberAnalytics.logPartyCreationStage(CREATE_PARTY_SET_TIME);
            return null;
        }
    }

    @Override
    public void onSelected() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            textViewLocationContent.requestFocus();
            textViewLocationContent.requestFocusFromTouch();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(textViewLocationContent.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }, 100);
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        ObjectAnimator
                .ofFloat(buttonLocationName, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                .setDuration(300)
                .start();
        buttonLocationName.setError(error.getErrorMessage());
    }

    @OnClick({R.id.buttonLocationName})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLocationName:
                startPlacePicker();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PLACE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    com.google.android.gms.location.places.Place placePickerPlace = PlacePicker.getPlace(getContext(), data);
                    place = Place.create(String.valueOf(placePickerPlace.getName()), String.valueOf(placePickerPlace.getAddress()),
                            String.valueOf(placePickerPlace.getId()), placePickerPlace.getLatLng().longitude,
                            placePickerPlace.getLatLng().latitude);
                    mapView.getMapAsync(googleMap -> showPlace(googleMap, place));
                }
                break;
        }
    }

    private void showPlace(GoogleMap googleMap, Place place) {
        buttonLocationName.setError(null);
        buttonLocationName.setText(place.name() + "\n" + place.address());
        mapView.setVisibility(View.VISIBLE);
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(new LatLng(place.latitude(), place.longitude())).title(place.name()));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(place.latitude(), place.longitude()))
                .zoom(12)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1000, null);
    }

    private void showWholeWorld(GoogleMap googleMap) {
        googleMap.clear();
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(1.30f));
    }


    private void startPlacePicker() {
        try {
            startActivityForResult(new PlacePicker.IntentBuilder().build(getActivity()), PLACE_PICKER_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
}
