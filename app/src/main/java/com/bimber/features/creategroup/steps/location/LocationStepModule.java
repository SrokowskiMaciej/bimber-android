package com.bimber.features.creategroup.steps.location;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class LocationStepModule {

    @ContributesAndroidInjector
    abstract LocationStepView createGroupView();
}
