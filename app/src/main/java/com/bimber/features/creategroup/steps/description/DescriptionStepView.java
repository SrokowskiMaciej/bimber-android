package com.bimber.features.creategroup.steps.description;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import io.reactivex.subjects.PublishSubject;

import static com.bimber.data.analytics.BimberAnalytics.CreatePartyStage.CREATE_PARTY_SET_LOCATION;

/**
 * Created by maciek on 13.03.17.
 */

public class DescriptionStepView extends BaseFragment implements Step {


    @BindView(R.id.textInputEditTextGroupDescription)
    TextInputEditText textInputEditTextGroupDescription;
    @BindView(R.id.textInputLayoutGroupDescription)
    TextInputLayout textInputLayoutGroupDescription;

    private final PublishSubject<RxFirebaseUtils.Event> onProceed = PublishSubject.create();

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;
    @Inject
    BimberAnalytics bimberAnalytics;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group_set_description_step, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public VerificationError verifyStep() {
        String groupName = textInputEditTextGroupDescription.getText().toString();
        createGroupStepperStateManager.setGroupDescription(groupName);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        bimberAnalytics.logPartyCreationStage(CREATE_PARTY_SET_LOCATION);
        return null;
    }

    @Override
    public void onSelected() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            textInputEditTextGroupDescription.requestFocus();
            textInputEditTextGroupDescription.requestFocusFromTouch();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(textInputEditTextGroupDescription, InputMethodManager.SHOW_IMPLICIT);
        }, 100);

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        textInputLayoutGroupDescription.setError(error.getErrorMessage());
        ObjectAnimator
                .ofFloat(textInputLayoutGroupDescription, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                .setDuration(300)
                .start();
    }

    @OnEditorAction(R.id.textInputEditTextGroupDescription)
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onProceed.onNext(RxFirebaseUtils.Event.INSTANCE);
        }
        return true;
    }

    public PublishSubject<RxFirebaseUtils.Event> onProceed() {
        return onProceed;
    }
}
