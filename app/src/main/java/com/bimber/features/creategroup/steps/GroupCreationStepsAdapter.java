package com.bimber.features.creategroup.steps;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.bimber.R;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersViewBuilder;
import com.bimber.features.creategroup.steps.description.DescriptionStepView;
import com.bimber.features.creategroup.steps.location.LocationStepView;
import com.bimber.features.creategroup.steps.name.NameStepView;
import com.bimber.features.creategroup.steps.participants.ParticipantsStepView;
import com.bimber.features.creategroup.steps.participants.ParticipantsStepViewBuilder;
import com.bimber.features.creategroup.steps.time.TimeStepView;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.StepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by maciek on 13.03.17.
 */

public class GroupCreationStepsAdapter extends FragmentStatePagerAdapter implements StepAdapter {

    private FragmentManager fm;
    private Context context;
    @Nullable
    private String preAddedUid;

    public GroupCreationStepsAdapter(@NonNull FragmentManager fm, @NonNull Context context, @Nullable String preAddedUid) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.preAddedUid = preAddedUid;
    }

    @Override
    public Step createStep(@IntRange(from = 0L) int position) {
        switch (position) {
            case 0:
                return new NameStepView();
            case 1:
                return new DescriptionStepView();
            case 2:
                return new LocationStepView();
            case 3:
                return new TimeStepView();
            case 4:
                return new ParticipantsStepViewBuilder().preAddedUid(preAddedUid).build();
            default:
                return null;
        }
    }

    @Override
    public Step findStep(@IntRange(from = 0L) int position) {
        ViewPager viewPager = ((Activity) context).findViewById(R.id.ms_stepPager);
        return (Step) instantiateItem(viewPager, position);
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public PagerAdapter getPagerAdapter() {
        return this;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0L) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context);
        switch (position) {
            case 0:
                return builder.setTitle(R.string.view_create_group_name_step).create();
            case 1:
                return builder.setTitle(R.string.view_create_group_description_step).create();
            case 2:
                return builder.setTitle(R.string.view_create_group_location_step).create();
            case 3:
                return builder.setTitle(R.string.view_create_group_time_step).create();
            case 4:
                return builder.setTitle(R.string.view_create_group_participants_step).create();
            default:
                return builder.create();
        }
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) createStep(position);
    }
}
