package com.bimber.features.creategroup.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public class CreateGroupModule {

    @Provides
    @FragmentScope
    public CreateGroupStepperStateManager createGroupStepperStateManager() {
        return new CreateGroupStepperStateManager();
    }
}
