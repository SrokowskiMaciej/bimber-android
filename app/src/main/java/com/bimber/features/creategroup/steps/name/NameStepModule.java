package com.bimber.features.creategroup.steps.name;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class NameStepModule {

    @ContributesAndroidInjector
    abstract NameStepView createGroupView();
}
