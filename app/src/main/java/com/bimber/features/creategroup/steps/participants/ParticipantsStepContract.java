package com.bimber.features.creategroup.steps.participants;

import com.bimber.data.entities.Group;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 15.03.17.
 */

public interface ParticipantsStepContract {

    interface ISetGroupParticipantsStepView {

        void setImmediateFriends(List<ChatMemberData> friends);

        void setCurrentUserData(ProfileDataThumbnail profileDataThumbnail);

        void showSpinner();

        void hideSpinner();

        void showError(boolean show);

        void startGroupChatActivity(Key<Group> group);

    }

    interface ISetGroupParticipantsStepPresenter extends MvpPresenter<ISetGroupParticipantsStepView> {
        void insertNewGroup(Group group, List<String> chatParticipantsIds);
    }
}
