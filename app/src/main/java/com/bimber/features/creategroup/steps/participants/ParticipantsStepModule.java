package com.bimber.features.creategroup.steps.participants;

import com.bimber.features.creategroup.steps.participants.ParticipantsStepContract.ISetGroupParticipantsStepPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 13.03.17.
 */
@Module
public abstract class ParticipantsStepModule {

    @Binds
    public abstract ISetGroupParticipantsStepPresenter setGroupParticipantsStepPresenter(ParticipantsStepPresenter
                                                                                                 participantsStepPresenter);

    @ContributesAndroidInjector
    abstract ParticipantsStepView setGroupParticipantsStepView();
}
