package com.bimber.features.creategroup.steps.participants.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 16.03.17.
 */
public class ParticipantViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.imageViewAvatar)
    ImageView imageViewAvatar;
    @BindView(R.id.textViewFriendName)
    TextView textViewParticipantName;
    @BindView(R.id.buttonRemove)
    ImageView buttonRemove;

    public ParticipantViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
