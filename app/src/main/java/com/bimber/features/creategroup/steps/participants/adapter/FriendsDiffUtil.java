package com.bimber.features.creategroup.steps.participants.adapter;

import android.support.v7.util.DiffUtil;

import com.bimber.domain.model.ChatMemberData;

import java.util.List;

/**
 * Created by maciek on 16.03.17.
 */
class FriendsDiffUtil extends DiffUtil.Callback {

    private final List<ChatMemberData> oldList;
    private final List<ChatMemberData> newList;

    FriendsDiffUtil(List<ChatMemberData> oldList, List<ChatMemberData> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }


    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).profileData().user.uId.equals(newList.get(newItemPosition).profileData().user.uId);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }
}
