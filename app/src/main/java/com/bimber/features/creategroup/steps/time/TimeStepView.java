package com.bimber.features.creategroup.steps.time;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bimber.data.analytics.BimberAnalytics.CreatePartyStage.CREATE_PARTY_SET_PARTICIPANTS;

/**
 * Created by maciek on 13.03.17.
 */

public class TimeStepView extends BaseFragment implements Step, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String DATE_PICKER_DIALOG = "DATE_PICKER_DIALOG";
    private static final String TIME_PICKER_DIALOG = "TIME_PICKER_DIALOG";
    public static final String TIME_KEY = "TIME_KEY";
    public static final String DATE_KEY = "DATE_KEY";
    @BindView(R.id.buttonChooseDate)
    AppCompatButton buttonChooseDate;
    @BindView(R.id.buttonChooseTime)
    AppCompatButton buttonChooseTime;

    private Date date;
    private Time time;

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;
    @Inject
    BimberAnalytics bimberAnalytics;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group_set_time_step, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(DATE_KEY)) {
                date = savedInstanceState.getParcelable(DATE_KEY);
                setDateText(date);
            }
            if (savedInstanceState.containsKey(TIME_KEY)) {
                time = savedInstanceState.getParcelable(TIME_KEY);
                setTimeText(time);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getChildFragmentManager().findFragmentByTag(DATE_PICKER_DIALOG);
        TimePickerDialog tpd = (TimePickerDialog) getChildFragmentManager().findFragmentByTag(TIME_PICKER_DIALOG);

        if (tpd != null) tpd.setOnTimeSetListener(this);
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (date != null) {
            outState.putParcelable(DATE_KEY, date);
        }
        if (time != null) {
            outState.putParcelable(TIME_KEY, time);
        }
    }

    @Override
    public VerificationError verifyStep() {
        if (date == null) {
            return new VerificationError("Date must be chosen");
        }
        if (time == null) {
            return new VerificationError("Time must be chosen");
        }
        createGroupStepperStateManager.setDate(date);
        createGroupStepperStateManager.setTime(time);
        bimberAnalytics.logPartyCreationStage(CREATE_PARTY_SET_PARTICIPANTS);
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        if (date == null) {
            buttonChooseDate.setFocusableInTouchMode(true);
            buttonChooseDate.setError(error.getErrorMessage());
            buttonChooseDate.requestFocusFromTouch();
            buttonChooseDate.setFocusableInTouchMode(false);
            ObjectAnimator
                    .ofFloat(buttonChooseDate, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                    .setDuration(300)
                    .start();
        } else if (time == null) {
            buttonChooseTime.setFocusableInTouchMode(true);
            buttonChooseTime.setError(error.getErrorMessage());
            buttonChooseTime.requestFocusFromTouch();
            buttonChooseTime.setFocusableInTouchMode(false);
            ObjectAnimator
                    .ofFloat(buttonChooseTime, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                    .setDuration(300)
                    .start();
        }

    }

    @OnClick({R.id.buttonChooseDate, R.id.buttonChooseTime})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonChooseDate:
                showDatePickerDialog();
                break;
            case R.id.buttonChooseTime:
                showTimePickerDialog();
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = Date.create(year, monthOfYear, dayOfMonth);
        setDateText(date);

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        time = Time.create(hourOfDay, minute, second);
        setTimeText(time);
    }

    private void showDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMinDate(now);
        datePickerDialog.show(getChildFragmentManager(), DATE_PICKER_DIALOG);
    }

    private void showTimePickerDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true);
        timePickerDialog.show(getChildFragmentManager(), TIME_PICKER_DIALOG);
    }


    private void setDateText(Date date) {
        buttonChooseDate.setError(null);
        Calendar calendar = Calendar.getInstance();
        calendar.set(date.year(), date.monthOfYear(), date.dayOfMonth());
        buttonChooseDate.setText(DateFormat.getDateInstance().format(calendar.getTime()));
    }

    private void setTimeText(Time time) {
        buttonChooseTime.setError(null);
        Calendar calendar = Calendar.getInstance();
        calendar.set(0, 0, 0, time.hourOfDay(), time.minute());
        buttonChooseTime.setText(DateFormat.getTimeInstance().format(calendar.getTime()));
    }
}
