package com.bimber.features.creategroup.steps.time;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 15.03.17.
 */
@AutoValue
public abstract class Date implements Parcelable {

    public static Date create(int year, int monthOfYear, int dayOfMonth) {
        return new AutoValue_Date(year, monthOfYear, dayOfMonth);
    }

    public abstract int year();

    public abstract int monthOfYear();

    public abstract int dayOfMonth();
}
