package com.bimber.features.creategroup.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.creategroup.CreateGroupView;
import com.bimber.features.creategroup.CreateGroupViewBuilder;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.bimber.features.messaging.groupparticipation.GroupParticipationSnackbarFragment;
import com.bimber.features.messaging.match.MatchSnackbarFragment;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;

import butterknife.ButterKnife;

/**
 * Created by maciek on 02.03.17.
 */

public class CreateGroupActivity extends BaseLoggedInActivity {

    private static final String CREATE_GROUP_FRAGMENT_TAG = "CREATE_GROUP_FRAGMENT_TAG";
    public static final String PRE_ADDED_UID_KEY = "PRE_ADDED_UID_KEY";

    private CreateGroupView currentFragment;

    public static Intent newInstance(Context context) {
        Intent intent = new Intent(context, CreateGroupActivity.class);
        return intent;
    }

    public static Intent newInstance(Context context, String preAddedUid) {
        Intent intent = new Intent(context, CreateGroupActivity.class);
        intent.putExtra(PRE_ADDED_UID_KEY, preAddedUid);
        return intent;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        currentFragment = (CreateGroupView) getSupportFragmentManager().findFragmentByTag(CREATE_GROUP_FRAGMENT_TAG);
        if (currentFragment == null) {
            currentFragment = new CreateGroupViewBuilder().preAddedUid(getIntent().getStringExtra(PRE_ADDED_UID_KEY)).build();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, currentFragment, CREATE_GROUP_FRAGMENT_TAG)
                    .commit();
        }
    }


    @Override
    public void onBackPressed() {
        if (currentFragment.onBackPressed())
            super.onBackPressed();
    }
}
