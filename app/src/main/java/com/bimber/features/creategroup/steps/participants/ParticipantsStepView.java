package com.bimber.features.creategroup.steps.participants;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Group;
import com.bimber.data.entities.Place;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.creategroup.CreateGroupStepperStateManager;
import com.bimber.features.creategroup.steps.participants.adapter.FriendsAdapter;
import com.bimber.features.creategroup.steps.participants.adapter.ParticipantsAdapter;
import com.bimber.features.creategroup.steps.time.Date;
import com.bimber.features.creategroup.steps.time.Time;
import com.bimber.features.home.HomeNavigator.HomeScreen;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

import static com.bimber.features.creategroup.steps.participants.ParticipantsStepContract.ISetGroupParticipantsStepPresenter;
import static com.bimber.features.creategroup.steps.participants.ParticipantsStepContract.ISetGroupParticipantsStepView;

/**
 * Created by maciek on 13.03.17.
 */
@FragmentWithArgs
public class ParticipantsStepView extends BaseFragment implements Step, ISetGroupParticipantsStepView {

    public static final String SELECTED_FRIENDS = "SELECTED_FRIENDS";
    @BindView(R.id.participantsRecyclerView)
    RecyclerView participantsRecyclerView;
    @BindView(R.id.immediateFriendsRecyclerView)
    RecyclerView immediateFriendsRecyclerView;

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;
    @Inject
    ISetGroupParticipantsStepPresenter setGroupParticipantsStepPresenter;
    @BindView(R.id.textViewNoImmediateFriends)
    TextView textViewNoImmediateFriends;
    @BindView(R.id.progressBarImmediateFriends)
    ProgressBar progressBarImmediateFriends;

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Arg(required = false)
    String preAddedUid;
    @BindView(R.id.switchAdministeredParty)
    Switch switchAdministeredParty;


    private FriendsAdapter immediateFriendsAdapter;
    private ParticipantsAdapter participantsAdapter;

    private CompositeDisposable subscriptions = new CompositeDisposable();

    private Set<String> chosenParticipantsIds = new HashSet<>();

    private MaterialDialog completeIndeterminateDialog;
    private MaterialDialog errorDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group_set_participants_step, container, false);
        ButterKnife.bind(this, view);


        participantsAdapter = new ParticipantsAdapter(GlideApp.with(this));
        participantsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        participantsRecyclerView.setAdapter(participantsAdapter);

        immediateFriendsAdapter = new FriendsAdapter(GlideApp.with(this));
        immediateFriendsRecyclerView.setLayoutManager(new FlexboxLayoutManager(getContext(), FlexDirection.COLUMN, FlexWrap.WRAP));
        immediateFriendsRecyclerView.setAdapter(immediateFriendsAdapter);

        subscriptions.add(participantsAdapter.onFriendUnselected()
                .subscribe(unselectedFriend -> {
                    chosenParticipantsIds.remove(unselectedFriend.profileData().user.uId);
                    List<String> selectedFriendsIds = Collections.unmodifiableList(new ArrayList<>(chosenParticipantsIds));
                    participantsAdapter.setSelectedFriendsIds(selectedFriendsIds);
                    immediateFriendsAdapter.setSelectedFriendsIds(selectedFriendsIds);
                }));
        subscriptions.add(immediateFriendsAdapter.onFriendSelected()
                .subscribe(selectedFriend -> {
                    chosenParticipantsIds.add(selectedFriend.profileData().user.uId);
                    List<String> selectedFriendsIds = Collections.unmodifiableList(new ArrayList<>(chosenParticipantsIds));
                    participantsAdapter.setSelectedFriendsIds(selectedFriendsIds);
                    immediateFriendsAdapter.setSelectedFriendsIds(selectedFriendsIds);
                }));

        subscriptions.add(participantsAdapter.onPotentialParticipantClickedAction()
                .subscribe(participantClicked -> {
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), participantClicked.avatar(), participantClicked.photoUrl());
                    startActivity(ProfileActivity.newInstance(getContext(), participantClicked.uId(),
                            participantClicked.photoUrl(), false), options.toBundle());
                }));
        subscriptions.add(immediateFriendsAdapter.onFriendClicked()
                .subscribe(firendClicked -> {
                    String uri = firendClicked.chatMember().profileData().photo.userPhotoUri;
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), firendClicked.avatar(), uri);
                    startActivity(ProfileActivity.newInstance(getContext(), firendClicked.chatMember().profileData().user.uId,
                            uri, false), options.toBundle());
                }));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_FRIENDS)) {
            chosenParticipantsIds = new HashSet<>(savedInstanceState.getStringArrayList(SELECTED_FRIENDS));
            ArrayList<String> selectedFriendsIds = new ArrayList<>(chosenParticipantsIds);
            participantsAdapter.setSelectedFriendsIds(selectedFriendsIds);
            immediateFriendsAdapter.setSelectedFriendsIds(selectedFriendsIds);
        } else if (preAddedUid != null) {
            chosenParticipantsIds.add(preAddedUid);
            List<String> selectedFriendsIds = Collections.unmodifiableList(new ArrayList<>(chosenParticipantsIds));
            participantsAdapter.setSelectedFriendsIds(selectedFriendsIds);
            immediateFriendsAdapter.setSelectedFriendsIds(selectedFriendsIds);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setGroupParticipantsStepPresenter.bindView(this);
    }

    @Override
    public void onStop() {
        setGroupParticipantsStepPresenter.unbindView(this);
        super.onStop();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(SELECTED_FRIENDS, new ArrayList<>(chosenParticipantsIds));
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        super.onDestroyView();
    }

    @Override
    public VerificationError verifyStep() {
        String groupName = createGroupStepperStateManager.getGroupName();
        if (groupName == null) {
            return new VerificationError("Group name is empty");
        }

        String groupDescription = createGroupStepperStateManager.getGroupDescription();
        if (groupDescription == null) {
            return new VerificationError("Group description is empty");
        }
        Place meetingPlace = createGroupStepperStateManager.getMeetingPlace();
        if (meetingPlace == null) {
            return new VerificationError("Meeting location is empty");
        }
        Date date = createGroupStepperStateManager.getDate();
        if (date == null) {
            return new VerificationError("Date is empty");
        }
        Time time = createGroupStepperStateManager.getTime();
        if (time == null) {
            return new VerificationError("Time is empty");
        }
        List<String> participants = new ArrayList<>(chosenParticipantsIds.size() + 1);
        participants.add(currentUserId);
        for (String participantId : chosenParticipantsIds) {
            participants.add(participantId);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(date.year(), date.monthOfYear(), date.dayOfMonth(), time.hourOfDay(), time.minute(), time.second());
        Timestamp timestamp = Timestamp.create(calendar.getTimeInMillis());
        Group newGroup = Group.create(groupName, groupDescription, meetingPlace, Timestamp.now(), timestamp, currentUserId, currentUserId, switchAdministeredParty.isChecked());
        setGroupParticipantsStepPresenter.insertNewGroup(newGroup, participants);
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setImmediateFriends(List<ChatMemberData> friends) {
        immediateFriendsAdapter.setFriends(friends);
        participantsAdapter.setImmediateFriends(friends);
        if (friends.isEmpty()) {
            textViewNoImmediateFriends.setVisibility(View.VISIBLE);
        } else {
            textViewNoImmediateFriends.setVisibility(View.GONE);
        }
        progressBarImmediateFriends.setVisibility(View.GONE);
    }

    @Override
    public void setCurrentUserData(ProfileDataThumbnail profileDataThumbnail) {
        participantsAdapter.setCurrentUserProfileData(profileDataThumbnail);
    }

    @Override
    public void showSpinner() {
        completeIndeterminateDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.view_create_group_set_participants_creating_dialog_title)
                .content(R.string.view_create_group_set_participants_creating_dialog_content)
                .progress(true, 0)
                .show();

    }

    @Override
    public void hideSpinner() {
        if (completeIndeterminateDialog != null) {
            completeIndeterminateDialog.dismiss();
        }
    }

    @Override
    public void showError(boolean show) {
        errorDialog = new MaterialDialog.Builder(getContext())
                .title("Error")
                .content(R.string.view_creat_group_set_participants_error_dialog_content)
                .neutralText("OK")
                .show();
    }

    @Override
    public void startGroupChatActivity(Key<Group> group) {
        Intent mainScreenIntent = HomeActivity.newInstance(getContext(), HomeScreen.CHATS);
        Intent chatIntent = ChatActivity.newInstance(getContext(), group.key(), group.value().chatType());
        TaskStackBuilder.create(getContext())
                .addNextIntent(mainScreenIntent)
                .addNextIntent(chatIntent)
                .startActivities();
    }
}
