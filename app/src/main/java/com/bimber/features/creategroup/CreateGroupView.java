package com.bimber.features.creategroup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.features.creategroup.steps.GroupCreationStepsAdapter;
import com.bimber.features.creategroup.steps.description.DescriptionStepView;
import com.bimber.features.creategroup.steps.name.NameStepView;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by srokowski.maciej@gmail.com on 28.10.16.
 */
@FragmentWithArgs
public class CreateGroupView extends BaseFragment implements StepperLayout.StepperListener {

    public static final String CURRENT_STEPPER_POSITION = "CURRENT_STEPPER_POSITION";

    @BindView(R.id.stepperLayoutCreateGroup)
    StepperLayout stepperLayoutCreateGroup;

    @Inject
    CreateGroupStepperStateManager createGroupStepperStateManager;

    @Arg(required = false)
    String preAddedUid;

    private final CompositeDisposable subscriptions = new CompositeDisposable();
    private int currentStepPosition;


    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (childFragment instanceof NameStepView) {
            subscriptions.add(((NameStepView) childFragment).onProceed()
                    .subscribe(event -> stepperLayoutCreateGroup.proceed()));
        }
        if (childFragment instanceof DescriptionStepView) {
            subscriptions.add(((DescriptionStepView) childFragment).onProceed()
                    .subscribe(event -> stepperLayoutCreateGroup.proceed()));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_create_group, container, false);
        ButterKnife.bind(this, view);
        stepperLayoutCreateGroup.setAdapter(new GroupCreationStepsAdapter(getChildFragmentManager(), getActivity(), preAddedUid));
        stepperLayoutCreateGroup.setListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(CURRENT_STEPPER_POSITION)) {
            currentStepPosition = savedInstanceState.getInt(CURRENT_STEPPER_POSITION);
            stepperLayoutCreateGroup.setCurrentStepPosition(currentStepPosition);
            createGroupStepperStateManager.restoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_STEPPER_POSITION, currentStepPosition);
        createGroupStepperStateManager.saveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        super.onDestroyView();
    }

    public boolean onBackPressed() {
        //TODO Ugh
        if (stepperLayoutCreateGroup.getCurrentStepPosition() > 0) {
            stepperLayoutCreateGroup.new OnBackClickedCallback().goToPrevStep();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onCompleted(View completeButton) {
        //Nothing interesting here
    }

    @Override
    public void onError(VerificationError verificationError) {
        //Nothing interesting here
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        currentStepPosition = newStepPosition;
    }

    @Override
    public void onReturn() {
        //Nothing interesting here
    }
}
