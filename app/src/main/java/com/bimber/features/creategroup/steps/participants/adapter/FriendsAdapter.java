package com.bimber.features.creategroup.steps.participants.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.domain.model.ChatMemberData;
import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 16.03.17.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendViewHolder> {

    private List<ChatMemberData> allFriends = Collections.emptyList();
    private List<ChatMemberData> nonSelectedFriends = Collections.emptyList();
    private List<String> selectedFriendsIds = Collections.emptyList();

    private PublishSubject<ChatMemberData> selectedFriendPublishSubject = PublishSubject.create();
    private PublishSubject<ChatMemberClickedAction> clickedFriendPublishSubject = PublishSubject.create();

    private final GlideRequests glide;

    public FriendsAdapter(GlideRequests glide) {
        this.glide = glide;
    }


    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .view_user_thumbnail_add, parent,
                false));
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        ChatMemberData friend = nonSelectedFriends.get(position);
        glide.load(friend.profileData().photo.userPhotoUri)
                .into(holder.imageViewAvatar);
        holder.imageViewAvatar.setOnClickListener(view -> {
            clickedFriendPublishSubject.onNext(ChatMemberClickedAction.create(holder.imageViewAvatar, friend));
        });
        holder.textViewFriendName.setText(friend.profileData().user.displayName);
        holder.buttonAdd.setOnClickListener(v -> {
            holder.buttonAdd.setOnClickListener(null);
            selectedFriendPublishSubject.onNext(friend);
        });
    }

    @Override
    public int getItemCount() {
        return nonSelectedFriends.size();
    }

    public void setFriends(List<ChatMemberData> friends) {
        allFriends = friends;
        List<ChatMemberData> newNonSelectedFriends = filterOutSelectedFriends(allFriends);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new FriendsDiffUtil(this.nonSelectedFriends,
                newNonSelectedFriends));
        this.nonSelectedFriends = newNonSelectedFriends;
        diffResult.dispatchUpdatesTo(this);
    }

    public void setSelectedFriendsIds(List<String> selectedFriendsIds) {
        this.selectedFriendsIds = selectedFriendsIds;
        setFriends(allFriends);
    }

    public Observable<ChatMemberData> onFriendSelected() {
        return selectedFriendPublishSubject;
    }

    public Observable<ChatMemberClickedAction> onFriendClicked() {
        return clickedFriendPublishSubject;
    }

    private List<ChatMemberData> filterOutSelectedFriends(List<ChatMemberData> friends) {
        List<ChatMemberData> nonSelectedFriends = new ArrayList<>();
        for (ChatMemberData friend : friends) {
            if (!isSelected(friend)) {
                nonSelectedFriends.add(friend);
            }
        }
        return Collections.unmodifiableList(nonSelectedFriends);
    }

    private boolean isSelected(ChatMemberData friend) {
        for (String selectedFriendId : selectedFriendsIds) {
            if (selectedFriendId.equals(friend.profileData().user.uId)) {
                return true;
            }
        }
        return false;
    }

    @AutoValue
    public abstract static class ChatMemberClickedAction {
        public abstract ImageView avatar();

        public abstract ChatMemberData chatMember();

        public static ChatMemberClickedAction create(ImageView avatar, ChatMemberData chatMember) {
            return new AutoValue_FriendsAdapter_ChatMemberClickedAction(avatar, chatMember);
        }
    }
}
