package com.bimber.features.creategroup.steps.participants.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.util.ListUpdateCallback;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 16.03.17.
 */

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantViewHolder> {

    private List<ChatMemberData> immediateFriends = Collections.emptyList();
    private List<ChatMemberData> groupFriends = Collections.emptyList();
    private List<ChatMemberData> selectedFriends = Collections.emptyList();
    private List<String> selectedFriendsIds = Collections.emptyList();

    private ProfileDataThumbnail currentUserProfileData;

    private PublishSubject<ChatMemberData> unselectedFriendPublishSubject = PublishSubject.create();
    private PublishSubject<PotentialParticipantClickedAction> potentialParticipantClickedActionPublishSubject =
            PublishSubject.create();

    private final GlideRequests glide;

    public ParticipantsAdapter(GlideRequests glide) {
        this.glide = glide;
    }

    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ParticipantViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .view_user_thumbnail_remove,
                parent, false));
    }

    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        if (position == 0) {
            if (currentUserProfileData != null) {
                glide.load(currentUserProfileData.photo.userPhotoUri)
                        .into(holder.imageViewAvatar);
                holder.imageViewAvatar.setOnClickListener(view -> {
                    potentialParticipantClickedActionPublishSubject.onNext(PotentialParticipantClickedAction.create
                            (currentUserProfileData.user.uId,
                                    currentUserProfileData.photo.userPhotoUri, holder.imageViewAvatar));
                });
            }
            holder.textViewParticipantName.setText(R.string.view_create_group_participant_current_user_indicator_text);
            holder.buttonRemove.setVisibility(View.INVISIBLE);
        } else {
            ChatMemberData friend = selectedFriends.get(position - 1);
            glide.load(friend.profileData().photo.userPhotoUri)
                    .into(holder.imageViewAvatar);
            holder.textViewParticipantName.setText(friend.profileData().user.displayName);
            holder.buttonRemove.setVisibility(View.VISIBLE);
            holder.buttonRemove.setOnClickListener(v -> {
                holder.buttonRemove.setOnClickListener(null);
                unselectedFriendPublishSubject.onNext(friend);
            });
        }

    }

    @Override
    public int getItemCount() {
        return selectedFriends.size() + 1;
    }

    public void setCurrentUserProfileData(ProfileDataThumbnail currentUserProfileData) {
        this.currentUserProfileData = currentUserProfileData;
        notifyItemChanged(0);
    }

    public void setImmediateFriends(List<ChatMemberData> friends) {
        immediateFriends = friends;
        setFriends();
    }

    public void setGroupFriends(List<ChatMemberData> friends) {
        groupFriends = friends;
        setFriends();
    }

    public void setSelectedFriendsIds(List<String> selectedFriendsIds) {
        this.selectedFriendsIds = selectedFriendsIds;
        setFriends();
    }


    private void setFriends() {
        List<ChatMemberData> selectedFriends = getSelectedFriends();
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new FriendsDiffUtil(this.selectedFriends,
                selectedFriends));
        this.selectedFriends = selectedFriends;
        diffResult.dispatchUpdatesTo(new ListUpdateCallback() {
            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position + 1, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position + 1, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition + 1, toPosition + 1);
            }

            @Override
            public void onChanged(int position, int count, Object payload) {
                notifyItemRangeChanged(position + 1, count, payload);
            }
        });
    }


    public Observable<ChatMemberData> onFriendUnselected() {
        return unselectedFriendPublishSubject;
    }

    public Observable<PotentialParticipantClickedAction> onPotentialParticipantClickedAction() {
        return potentialParticipantClickedActionPublishSubject;
    }

    private List<ChatMemberData> getSelectedFriends() {
        List<ChatMemberData> selectedFriends = new ArrayList<>();
        for (ChatMemberData friend : immediateFriends) {
            if (isSelected(friend)) {
                selectedFriends.add(friend);
            }
        }
        for (ChatMemberData friend : groupFriends) {
            if (isSelected(friend)) {
                selectedFriends.add(friend);
            }
        }
        return Collections.unmodifiableList(selectedFriends);
    }

    private boolean isSelected(ChatMemberData friend) {
        for (String selectedFriendId : selectedFriendsIds) {
            if (selectedFriendId.equals(friend.profileData().user.uId)) {
                return true;
            }
        }
        return false;
    }

    @AutoValue
    public abstract static class PotentialParticipantClickedAction {
        public abstract String uId();

        public abstract String photoUrl();

        public abstract ImageView avatar();

        public static PotentialParticipantClickedAction create(String uId, String photoUrl, ImageView avatar) {
            return new AutoValue_ParticipantsAdapter_PotentialParticipantClickedAction(uId, photoUrl, avatar);
        }
    }
}
