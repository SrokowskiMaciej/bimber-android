package com.bimber.features.creategroup.steps.participants;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.entities.Group;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.domain.friends.GetFriendsDataUseCase;
import com.bimber.domain.groupjoin.EvaluateGroupCandidateUseCase;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.features.creategroup.steps.participants.ParticipantsStepContract.ISetGroupParticipantsStepPresenter;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.bimber.data.entities.Chattable.ChatType.DIALOG;
import static com.bimber.data.entities.Chattable.ChatType.GROUP;
import static com.bimber.features.creategroup.steps.participants.ParticipantsStepContract.ISetGroupParticipantsStepView;

/**
 * Created by maciek on 15.03.17.
 */

public class ParticipantsStepPresenter extends MvpAbstractPresenter<ISetGroupParticipantsStepView> implements
        ISetGroupParticipantsStepPresenter {

    private final String currentUserId;
    private final GetFriendsDataUseCase getFriendsDataUseCase;
    private final ProfileDataRepository profileDataRepository;
    private final GroupRepository groupRepository;
    private final EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase;
    private final DbConnectionService dbConnectionService;
    private final BimberAnalytics bimberAnalytics;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ParticipantsStepPresenter(@LoggedInUserId String currentUserId, GetFriendsDataUseCase getFriendsDataUseCase,
                                     ProfileDataRepository profileDataRepository, GroupRepository groupRepository,
                                     EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase, DbConnectionService dbConnectionService, BimberAnalytics bimberAnalytics) {
        this.getFriendsDataUseCase = getFriendsDataUseCase;
        this.currentUserId = currentUserId;
        this.profileDataRepository = profileDataRepository;
        this.groupRepository = groupRepository;
        this.evaluateGroupCandidateUseCase = evaluateGroupCandidateUseCase;
        this.dbConnectionService = dbConnectionService;
        this.bimberAnalytics = bimberAnalytics;
    }

    @Override
    protected void viewAttached(ISetGroupParticipantsStepView view) {
        subscriptions.add(profileDataRepository.userProfileDataThumbnail(currentUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setCurrentUserData,
                        //TODO Handle
                        throwable -> Timber.e(throwable, "Error while getting current user data")));

        subscriptions.add(getFriendsDataUseCase.onUserFriends(currentUserId)
                .sample(600, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(friends -> getView().setImmediateFriends(friends),
                        //TODO Handle
                        throwable -> Timber.e(throwable, "Error while listening to user friends")));
    }

    @Override
    protected void viewDetached(ISetGroupParticipantsStepView view) {
        subscriptions.clear();
    }

    @Override
    public void insertNewGroup(Group group, List<String> chatParticipantsIds) {
        getView().showSpinner();
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(groupRepository.insertGroup(group))
                .flatMap(insertedGroup -> evaluateGroupCandidateUseCase.setInitialGroupJoinRequests(currentUserId, insertedGroup
                        .key(), chatParticipantsIds)
                        .toSingleDefault(insertedGroup))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(insertedGroup -> {
                    bimberAnalytics.logPartyCreationFinal(chatParticipantsIds.size(),
                            group.location().name(),
                            group.location().latitude(),
                            group.location().longitude(),
                            true);
                    getView().hideSpinner();
                    getView().startGroupChatActivity(insertedGroup);
                }, throwable -> {
                    bimberAnalytics.logPartyCreationFinal(chatParticipantsIds.size(),
                            group.location().name(),
                            group.location().latitude(),
                            group.location().longitude(),
                            false);
                    Timber.e(throwable, "Error creating new group");
                    getView().hideSpinner();
                    getView().showError(true);
                }));
    }
}
