package com.bimber.features.home;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 17.06.17.
 */

public class HomeNavigator {

    private PublishSubject<HomeScreen> mainScreenNavigationEvents = PublishSubject.create();

    public void showScreen(HomeScreen homeScreen) {
        mainScreenNavigationEvents.onNext(homeScreen);
    }

    public Observable<HomeScreen> screenShown() {
        return mainScreenNavigationEvents;
    }

    public enum HomeScreen {
        PROFILE,
        EXPLORE,
        CHATS
    }

}
