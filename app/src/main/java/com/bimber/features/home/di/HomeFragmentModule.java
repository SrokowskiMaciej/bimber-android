package com.bimber.features.home.di;

import com.bimber.base.FragmentScope;
import com.bimber.features.chatlist.di.PeopleChatListFragmentModule;
import com.bimber.features.discovergroups.di.DiscoverGroupsFragmentModule;
import com.bimber.features.discoverpeople.di.DiscoverPeopleFragmentModule;
import com.bimber.features.editprofile.EditProfileFragmentModule;
import com.bimber.features.explore.di.ExploreFragmentModule;
import com.bimber.features.home.ChatButtonsProvider;
import com.bimber.features.home.ExploreButtonsProvider;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.HomeView;
import com.bimber.features.settings.SettingsFragmentModule;
import com.bimber.features.userprofile.UserProfileFragmentModule;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 26.10.16.
 */


@Module
public abstract class HomeFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = {HomeModule.class, UserProfileFragmentModule.class, ExploreFragmentModule.class,
            PeopleChatListFragmentModule.class, DiscoverPeopleFragmentModule.class, DiscoverGroupsFragmentModule.class, SettingsFragmentModule.class, EditProfileFragmentModule.class})
    abstract HomeView contributeHomeView();

    @Module
    @FragmentScope
    public static class HomeModule {

        @Provides
        @FragmentScope
        public static ChatButtonsProvider contributeChatButtonsProvider(HomeView homeView) {
            return homeView;
        }

        @Provides
        @FragmentScope
        public static ExploreButtonsProvider contributeExploreButtonsProvider(HomeView homeView) {
            return homeView;
        }


        @Provides
        @FragmentScope
        public static HomeNavigator contributeHomeNavigator() {
            return new HomeNavigator();
        }
    }


}
