package com.bimber.features.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.airbnb.deeplinkdispatch.DeepLink;
import com.bimber.R;
import com.bimber.base.activities.BackPressedManager;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.sources.discovery.groups.GroupDeepLinkRepository;
import com.bimber.data.repositories.MessagingTokenRepository;
import com.bimber.data.sources.discovery.users.UserDeepLinkRepository;
import com.bimber.features.deeplinking.DeeplinkNavigator;
import com.bimber.features.home.HomeNavigator.HomeScreen;
import com.bimber.features.home.HomeView;
import com.bimber.features.home.HomeViewBuilder;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class HomeActivity extends BaseLoggedInActivity {

    public static final String ACTION_DEEP_LINK_PROFILE = "ACTION_DEEP_LINK_PROFILE";
    public static final String ACTION_DEEP_LINK_GROUP = "ACTION_DEEP_LINK_GROUP";
    public static final String HOME_SCREEN_KEY = "HOME_SCREEN_KEY";
    public static final String HOME_SCREEN_TAG = "HOME_SCREEN_TAG";
    public static final String ID_KEY = "id";
    @Inject
    BackPressedManager backPressedManager;
    @Inject
    GroupDeepLinkRepository groupDeepLinkRepository;
    @Inject
    UserDeepLinkRepository userDeepLinkRepository;
    @Inject
    MessagingTokenRepository messagingTokenRepository;
    @Inject
    DeeplinkNavigator deeplinkNavigator;
    @Inject
    @LoggedInUserId
    String currentUserId;

    public static Intent newInstance(Context context, HomeScreen homeScreen) {
        return new Intent(context, HomeActivity.class)
                .putExtra(HOME_SCREEN_KEY, homeScreen.toString());
    }

    public static Intent newInstanceWithGroup(Context context, String groupId) {
        return newInstance(context, HomeScreen.EXPLORE)
                .setAction(ACTION_DEEP_LINK_GROUP)
                .putExtra(DeepLink.IS_DEEP_LINK, true)
                .putExtra(ID_KEY, groupId);
    }

    public static Intent newInstanceWithProfile(Context context, String uId) {
        return newInstance(context, HomeScreen.EXPLORE)
                .setAction(ACTION_DEEP_LINK_PROFILE)
                .putExtra(DeepLink.IS_DEEP_LINK, true)
                .putExtra(ID_KEY, uId);
    }

    @DeepLink("https://bimber.fun/profile/{id}")
    public static Intent newInstanceProfileDeeplink(Context context) {
        return newInstance(context, HomeScreen.EXPLORE)
                .setAction(ACTION_DEEP_LINK_PROFILE);
    }


    @DeepLink("https://bimber.fun/group/{id}")
    public static Intent newInstanceGroupDeeplink(Context context) {
        return newInstance(context, HomeScreen.EXPLORE)
                .setAction(ACTION_DEEP_LINK_GROUP);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState == null) {
            handleDeeplinks(getIntent());
        }
        if (getSupportFragmentManager().findFragmentByTag(HOME_SCREEN_TAG) == null) {
            HomeView homeView = new HomeViewBuilder(HomeScreen.valueOf(getIntent().getStringExtra(HOME_SCREEN_KEY))).build();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentMain, homeView, HOME_SCREEN_TAG)
                    .commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (isFinishing()) return;
        handleDeeplinks(intent);
        HomeView homeView = new HomeViewBuilder(HomeScreen.valueOf(intent.getStringExtra(HOME_SCREEN_KEY))).build();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentMain, homeView, HOME_SCREEN_TAG)
                .commit();
    }

    private void handleDeeplinks(Intent intent) {
        if (intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false)) {
            Bundle parameters = intent.getExtras();
            if (ACTION_DEEP_LINK_PROFILE.equals(intent.getAction())) {
                String profileId = parameters.getString(ID_KEY);
                Timber.d("Deeplink - profileId: %s", profileId);
                userDeepLinkRepository.setDeepLinkUser(profileId);
                deeplinkNavigator.pushDeeplink(DeeplinkNavigator.Deeplink.PROFILE);
            } else if (ACTION_DEEP_LINK_GROUP.equals(intent.getAction())) {
                String groupId = parameters.getString("id");
                Timber.d("Deeplink - groupId: %s", groupId);
                groupDeepLinkRepository.putDeepLinkGroupId(groupId);
                deeplinkNavigator.pushDeeplink(DeeplinkNavigator.Deeplink.GROUP);
            } else {
                Timber.e("Deeplink - Action unknown");
            }
            // Do something with idString
        }
    }


}
