package com.bimber.features.home;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.features.chatlist.ChatListView;
import com.bimber.features.deeplinking.DeeplinkNavigator;
import com.bimber.features.explore.ExploreView;
import com.bimber.features.home.HomeNavigator.HomeScreen;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;
import com.bimber.features.messaging.groupparticipation.GroupParticipationSnackbarFragment;
import com.bimber.features.messaging.match.MatchSnackbarFragment;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;
import com.bimber.features.userprofile.UserProfileView;
import com.github.kevelbreh.androidunits.AndroidUnit;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.State;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by srokowski.maciej@gmail.com on 16.11.16.
 */
@FragmentWithArgs
public class HomeView extends BaseFragment implements ExploreButtonsProvider, ChatButtonsProvider {

    public static final String PROFILE_VIEW_TAG = "GROUP_VIEW_TAG";
    public static final String EXPLORE_VIEW_TAG = "EXPLORE_VIEW_TAG";
    public static final String CHATS_VIEW_TAG = "CHATS_VIEW_TAG";
    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";
    public static final String PARTICIPATION_TAG = "PARTICIPATION_TAG";
    public static final String MATCH_TAG = "MATCH_TAG";

    public static final String HOME_SCREEN_KEY = "HOME_SCREEN_KEY";

    @BindView(R.id.bottomBar)
    BottomBar bottomBar;
    @BindView(R.id.buttonLike)
    FloatingActionButton buttonLike;
    @BindView(R.id.buttonAdd)
    FloatingActionButton buttonAdd;
    @BindView(R.id.buttonRevert)
    FloatingActionButton buttonRevert;
    @BindView(R.id.buttonSkip)
    FloatingActionButton buttonSkip;
    @BindView(R.id.buttonDislike)
    FloatingActionButton buttonDislike;
    @BindView(R.id.profileView)
    FrameLayout profileView;
    @BindView(R.id.exploreView)
    FrameLayout exploreView;
    @BindView(R.id.chatsView)
    FrameLayout chatsView;

    @Inject
    DeeplinkNavigator deeplinkNavigator;
    @Inject
    HomeNavigator homeNavigator;
    @Arg
    @State
    HomeScreen homeScreen;


    private CompositeDisposable subscriptions = new CompositeDisposable();
    private UserProfileView userProfileViewFragment;
    private ChatListView chatListViewFragment;
    private ExploreView exploreViewFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_home, container, false);
        ButterKnife.bind(this, view);

        if (getChildFragmentManager().findFragmentByTag(NEW_CHAT_MESSASGE_TAG) == null) {
            getChildFragmentManager().beginTransaction()
                    .add(R.id.homeView, NewChatMessageSnackbarFragment.newInstance(HandlingPriority.HIGH), NEW_CHAT_MESSASGE_TAG)
                    .add(R.id.homeView, MatchSnackbarFragment.newInstance(HandlingPriority.HIGH), MATCH_TAG)
                    .add(R.id.homeView, GroupParticipationSnackbarFragment.newInstance(HandlingPriority.HIGH), PARTICIPATION_TAG)
                    .commit();
        }


        userProfileViewFragment = (UserProfileView) getChildFragmentManager().findFragmentByTag(PROFILE_VIEW_TAG);
        if (userProfileViewFragment == null) {
            userProfileViewFragment = new UserProfileView();
            getChildFragmentManager().beginTransaction().add(R.id.profileView, userProfileViewFragment, PROFILE_VIEW_TAG).commit();
        }

        exploreViewFragment = (ExploreView) getChildFragmentManager().findFragmentByTag(EXPLORE_VIEW_TAG);
        if (exploreViewFragment == null) {
            exploreViewFragment = new ExploreView();
            getChildFragmentManager().beginTransaction().add(R.id.exploreView, exploreViewFragment, EXPLORE_VIEW_TAG).commit();
        }
        chatListViewFragment = (ChatListView) getChildFragmentManager().findFragmentByTag(CHATS_VIEW_TAG);
        if (chatListViewFragment == null) {
            chatListViewFragment = new ChatListView();
            getChildFragmentManager().beginTransaction().add(R.id.chatsView, chatListViewFragment, CHATS_VIEW_TAG).commit();
        }

        switch (homeScreen) {
            case PROFILE:
                bottomBar.setDefaultTab(R.id.tab_profile);
                break;
            case EXPLORE:
                bottomBar.setDefaultTab(R.id.tab_explore);
                break;
            case CHATS:
                bottomBar.setDefaultTab(R.id.tab_chats);
                break;
        }
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_profile:
                        profileView.setVisibility(View.VISIBLE);
                        exploreView.setVisibility(View.INVISIBLE);
                        chatsView.setVisibility(View.INVISIBLE);
                        homeScreen = HomeScreen.PROFILE;
                        validateExploreButtons();
                        validateChatButtons();
                        break;
                    case R.id.tab_explore:
                        exploreView.setVisibility(View.VISIBLE);
                        userProfileViewFragment.scrollToTop();
                        profileView.setVisibility(View.INVISIBLE);
                        chatsView.setVisibility(View.INVISIBLE);
                        homeScreen = HomeScreen.EXPLORE;
                        validateExploreButtons();
                        validateChatButtons();
                        break;
                    case R.id.tab_chats:
                        chatsView.setVisibility(View.VISIBLE);
                        userProfileViewFragment.scrollToTop();
                        profileView.setVisibility(View.INVISIBLE);
                        exploreView.setVisibility(View.INVISIBLE);
                        homeScreen = HomeScreen.CHATS;
                        validateExploreButtons();
                        validateChatButtons();
                        break;
                }

            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subscriptions.add(homeNavigator.screenShown()
                .subscribe(homeScreenShown -> {
                    homeScreen = homeScreenShown;
                    switch (homeScreen) {
                        case PROFILE:
                            bottomBar.selectTabWithId(R.id.tab_profile);
                            break;
                        case EXPLORE:
                            bottomBar.selectTabWithId(R.id.tab_explore);
                            break;
                        case CHATS:
                            bottomBar.selectTabWithId(R.id.tab_chats);
                            break;
                    }
                }));

    }

    @Override
    public void validateExploreButtons() {
        switch (homeScreen) {
            case EXPLORE:
                boolean dislikeButtonVisible = exploreViewFragment.shouldExploreButtonBeVisible(ExploreButton.DISLIKE);
                boolean likeButtonVisible = exploreViewFragment.shouldExploreButtonBeVisible(ExploreButton.LIKE);
                boolean addButtonVisible = exploreViewFragment.shouldExploreButtonBeVisible(ExploreButton.ADD);
                boolean revertButtonVisible = exploreViewFragment.shouldExploreButtonBeVisible(ExploreButton.REVERT);
                boolean skipButtonVisible = exploreViewFragment.shouldExploreButtonBeVisible(ExploreButton.SKIP);
                if (dislikeButtonVisible) {
                    buttonDislike.show();
                } else
                    buttonDislike.hide();
                if (addButtonVisible) {
                    buttonAdd.show();
                } else
                    buttonAdd.hide();
                if (likeButtonVisible) {
                    buttonLike.show();
                } else
                    buttonLike.hide();
                if (revertButtonVisible) {
                    buttonRevert.show();
                } else {
                    buttonRevert.hide();
                }
                if (skipButtonVisible) {
                    buttonSkip.show();
                } else {
                    buttonSkip.hide();
                }
                if (revertButtonVisible || skipButtonVisible) {
                    animateLeftMargin(buttonAdd, (int) AndroidUnit.DENSITY_PIXELS.toPixels(52));
                } else {
                    animateLeftMargin(buttonAdd, (int) AndroidUnit.DENSITY_PIXELS.toPixels(0));
                }
                break;
            default:
                buttonDislike.hide();
                buttonAdd.hide();
                buttonLike.hide();
                buttonRevert.hide();
                buttonSkip.hide();
        }
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        super.onDestroyView();
    }

    @OnClick({R.id.buttonLike, R.id.buttonAdd, R.id.buttonSkip, R.id.buttonRevert, R.id.buttonDislike})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonLike:
                exploreViewFragment.onClickExploreButton(ExploreButton.LIKE, view);
                break;
            case R.id.buttonAdd:
                exploreViewFragment.onClickExploreButton(ExploreButton.ADD, view);
                break;
            case R.id.buttonDislike:
                exploreViewFragment.onClickExploreButton(ExploreButton.DISLIKE, view);
                break;
            case R.id.buttonRevert:
                exploreViewFragment.onClickExploreButton(ExploreButton.REVERT, view);
                break;
            case R.id.buttonSkip:
                exploreViewFragment.onClickExploreButton(ExploreButton.SKIP, view);
                break;
        }
    }

    @Override
    public void validateChatButtons() {
        //TODO
    }

    private void animateLeftMargin(FloatingActionButton button, int targetMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) button.getLayoutParams();
        if (layoutParams.leftMargin != targetMargin) {
            ValueAnimator animator = ValueAnimator.ofInt(layoutParams.leftMargin, targetMargin);
            animator.addUpdateListener(valueAnimator -> {
                layoutParams.leftMargin = (int) valueAnimator.getAnimatedValue();
                button.requestLayout();
            });
            animator.setDuration(200);
            animator.start();
        }
    }
}
