package com.bimber.features.home.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;

import dagger.Binds;
import dagger.Module;

/**
 * Created by srokowski.maciej@gmail.com on 16.11.16.
 */
@Module
public abstract class HomeActivityModule {
    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(HomeActivity activity);
}
