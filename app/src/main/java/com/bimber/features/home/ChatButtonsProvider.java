package com.bimber.features.home;

/**
 * Created by maciek on 18.06.17.
 */

public interface ChatButtonsProvider {

    void validateChatButtons();

    interface ChatButtonConsumer {
        boolean shouldChatButtonBeVisible(ChatButton chatButton);

        void onClickChatButton(ChatButton chatButton);
    }

    enum ChatButton {
        NEW_GROUP_JOIN_REQUESTS
    }
}
