package com.bimber.features.home;

import android.view.View;

/**
 * Created by maciek on 16.06.17.
 */

public interface ExploreButtonsProvider {

    void validateExploreButtons();

    interface ExploreButtonConsumer {
        boolean shouldExploreButtonBeVisible(ExploreButton exploreButton);

        void onClickExploreButton(ExploreButton exploreButton, View button);
    }

    enum ExploreButton {
        LIKE,
        DISLIKE,
        ADD,
        REVERT,
        SKIP
    }
}
