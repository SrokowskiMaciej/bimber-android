package com.bimber.features.chatlist;

import com.bimber.domain.chats.GetUserChatsDataUseCase.DetailedChatData;
import com.bimber.domain.chats.search.SearchForChatsUseCase.ChatSearchResults;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

public interface ChatListContract {
    interface IChatListView {
        void setChats(List<DetailedChatData> chats);

        void setSearchResults(ChatSearchResults searchResults);

        void setProgressVisible(boolean visible);

    }

    interface IChatListPresenter extends MvpPresenter<IChatListView> {
        void requestChats(int requestedChatsCount);

        void searchForChats(String searchTerm);
    }
}
