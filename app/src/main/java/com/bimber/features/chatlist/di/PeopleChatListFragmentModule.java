package com.bimber.features.chatlist.di;

import com.bimber.features.chatlist.ChatListContract;
import com.bimber.features.chatlist.ChatListPresenter;
import com.bimber.features.chatlist.ChatListView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */
@Module
public abstract class PeopleChatListFragmentModule {

    @Binds
    abstract ChatListContract.IChatListPresenter discoverPresenter(ChatListPresenter peopleChatListPresenter);

    @ContributesAndroidInjector
    abstract ChatListView contribute();


}
