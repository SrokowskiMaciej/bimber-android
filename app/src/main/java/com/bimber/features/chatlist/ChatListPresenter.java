package com.bimber.features.chatlist;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.domain.chats.GetUserChatsDataUseCase;
import com.bimber.domain.chats.GetUserChatsDataUseCase.DetailedChatData;
import com.bimber.domain.chats.search.SearchForChatsUseCase;
import com.bimber.domain.chats.search.SearchForChatsUseCase.SearchResult;
import com.bimber.features.chatlist.ChatListContract.IChatListPresenter;
import com.bimber.features.chatlist.ChatListContract.IChatListView;
import com.bimber.utils.mvp.MvpAbstractPresenter;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.auto.value.AutoValue;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.PublishProcessor;
import timber.log.Timber;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

public class ChatListPresenter extends MvpAbstractPresenter<IChatListView> implements IChatListPresenter {

    private final String currentUserId;
    private final GetUserChatsDataUseCase getUserChatsDataUseCase;
    private final SearchForChatsUseCase searchForChatsUseCase;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    private final PublishProcessor<Integer> requestChatsPublishSubject = PublishProcessor.create();
    private final PublishProcessor<String> searchForChatsPublishSubject = PublishProcessor.create();

    @Inject
    public ChatListPresenter(@LoggedInUserId String currentUserId, GetUserChatsDataUseCase getUserChatsDataUseCase, SearchForChatsUseCase
            searchForChatsUseCase) {
        this.currentUserId = currentUserId;
        this.getUserChatsDataUseCase = getUserChatsDataUseCase;
        this.searchForChatsUseCase = searchForChatsUseCase;
    }

    @Override
    protected void viewAttached(IChatListView view) {
        subscriptions.add(
                requestChatsPublishSubject
                        .distinct()
                        .switchMap(integer -> getUserChatsDataUseCase.onUserChatsEventsOptimized(currentUserId, integer)
                                .map(detailedChatDatas -> UserChatsResult.create(true, detailedChatDatas))
                                .startWith(UserChatsResult.create(false, Collections.emptyList())))
                        .sample(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(chatsResult -> {
                            if (chatsResult.ready()) {
                                view.setChats(chatsResult.chatList());
                                view.setProgressVisible(false);
                            } else {
                                view.setProgressVisible(true);
                            }
                        }, throwable -> {
                            Timber.e(throwable, "Failed to load chats data");
                            view.setProgressVisible(false);
                            //TODO Handle
                        }));

        subscriptions.add(searchForChatsUseCase.searchForChats(currentUserId, searchForChatsPublishSubject)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchResults -> {
                    getView().setSearchResults(searchResults);
                    Timber.d("Search results: %s", searchResults.searchResults().size());
                }));

    }

    @Override
    protected void viewDetached(IChatListView view) {
        subscriptions.clear();
    }


    @Override
    public void requestChats(int requestedChatsCount) {
        requestChatsPublishSubject.onNext(requestedChatsCount);
    }

    @Override
    public void searchForChats(String searchTerm) {
        searchForChatsPublishSubject.onNext(searchTerm);
    }

    @AutoValue
    public static abstract class ChatListData {
        public abstract List<DetailedChatData> userChats();

        public abstract boolean userChatsProgressVisible();

        public abstract List<SearchResult> searchResults();

        public abstract boolean searchResultsProgressVisible();

        public abstract boolean errorVisible();

        public static ChatListData create(List<DetailedChatData> userChats, boolean userChatsProgressVisible, List<SearchResult>
                searchResults, boolean searchResultsProgressVisible, boolean errorVisible) {
            return new AutoValue_ChatListPresenter_ChatListData(userChats, userChatsProgressVisible, searchResults,
                    searchResultsProgressVisible, errorVisible);
        }
    }

    //TODO This should be sealed class. Wait for kotlin
    @AutoValue
    public static abstract class UserChatsResult {
        public abstract boolean ready();

        public abstract List<DetailedChatData> chatList();

        public static UserChatsResult create(boolean ready, List<DetailedChatData> chatList) {
            return new AutoValue_ChatListPresenter_UserChatsResult(ready, chatList);
        }
    }


}
