package com.bimber.features.chatlist;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.domain.chats.GetUserChatsDataUseCase.DetailedChatData;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.domain.model.DialogChatData;
import com.bimber.domain.model.GroupChatData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;
import com.bimber.features.common.ProgressView;
import com.bimber.utils.images.BitmapUtils;
import com.bimber.utils.view.AbstractDiffUtilCallback;

import java.text.DateFormat;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;


/**
 * Created by maciek on 22.02.17.
 */

public class ChatsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int CHAT_ITEM_TYPE = 0;
    public static final int REFRESH_ITEM_TYPE = 1;
    private final Context context;
    private final String currentUserId;
    private final DefaultValues defaultValues;
    private final GlideRequests glide;

    private List<DetailedChatData> chats = Collections.emptyList();
    private PublishSubject<DetailedChatData> chatDataThumbnailPublishSubject = PublishSubject.create();

    private boolean isRefreshVisible;

    public ChatsAdapter(Context context, String currentUserId, DefaultValues defaultValues, GlideRequests glide) {
        this.context = context;
        this.currentUserId = currentUserId;
        this.defaultValues = defaultValues;
        this.glide = glide;
        setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CHAT_ITEM_TYPE) {
            return new ChatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_list_item,
                    parent, false));
        } else {
            return new RefreshItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .view_chat_list_item_refresh, parent,
                    false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ((holder instanceof RefreshItemViewHolder)) {
            RefreshItemViewHolder refreshItemViewHolder = (RefreshItemViewHolder) holder;
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) refreshItemViewHolder
                    .background.getLayoutParams();
            if (chats.isEmpty() && isRefreshVisible) {
                layoutParams.height = ViewGroup.MarginLayoutParams.MATCH_PARENT;
            } else {
                layoutParams.height = ViewGroup.MarginLayoutParams.WRAP_CONTENT;
            }
            refreshItemViewHolder.setRefreshing(isRefreshVisible);
            refreshItemViewHolder.background.requestLayout();
            return;
        }
        DetailedChatData chat = chats.get(position);
        switch (chat.data().chatType()) {
            case DIALOG:
                ((ChatViewHolder) holder).onBindDialogViewHolder(chat);
                break;
            case GROUP:
                ((ChatViewHolder) holder).onBindGroupViewHolder(chat);
                break;
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof ChatViewHolder) {
            ((ChatViewHolder) holder).onUnbind();
        }
    }

    @Override
    public int getItemCount() {
        return chats.size() + 1;
    }

    @Override
    public long getItemId(int position) {
        if (position < chats.size()) {
            return chats.get(position).data().chat().key().hashCode();
        } else {
            return hashCode();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < chats.size()) {
            return CHAT_ITEM_TYPE;
        } else {
            return REFRESH_ITEM_TYPE;
        }
    }

    public boolean isRefreshVisible() {
        return isRefreshVisible;
    }

    public void setRefreshVisible(boolean refreshVisible) {
        isRefreshVisible = refreshVisible;
        notifyItemChanged(chats.size());
    }

    public void setChats(List<DetailedChatData> chats) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<DetailedChatData>(this
                .chats, chats) {
            @Override
            public boolean areItemsTheSame(DetailedChatData oldItem, DetailedChatData newItem) {
                return oldItem.data().chat().key().equals(newItem.data().chat().key());
            }

            @Override
            public boolean areContentsTheSame(DetailedChatData oldItem, DetailedChatData newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.chats = chats;
        diffResult.dispatchUpdatesTo(this);
    }

    public Observable<DetailedChatData> chatClickedObserver() {
        return chatDataThumbnailPublishSubject;
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        @BindView(R.id.imageViewAvatar)
        ImageView imageViewAvatar;
        @BindView(R.id.textViewMatchName)
        TextView textViewMatchName;
        @BindView(R.id.textViewLastMessageDate)
        TextView textViewLastMessageDate;
        @BindView(R.id.imageViewLastMessageAvatar)
        ImageView imageViewLastMessageAvatar;
        @BindView(R.id.textViewLastMessage)
        TextView textViewLastMessage;
        @BindView(R.id.imageViewNewMessageIndicator)
        ImageView imageViewNewMessageIndicator;
        @BindView(R.id.imageViewNewGroupCandidate)
        ImageView imageViewNewGroupCandidate;
        @BindView(R.id.imageViewMuteIndicator)
        ImageView imageViewMuteIndicator;

        private DetailedChatData chat;
        private CompositeDisposable subscriptions = new CompositeDisposable();

        public ChatViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            rootView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }


        public void onBindDialogViewHolder(DetailedChatData chatData) {
            chat = chatData;
            DialogChatData dialog = (DialogChatData) chatData.data();
            ProfileDataThumbnail interlocutorData = Observable.fromIterable(dialog.membersData())
                    .filter(chatMemberProfileData -> !chatMemberProfileData.profileData().user.uId.equals
                            (currentUserId))
                    .map(ChatMemberData::profileData)
                    .single(defaultValues.deletedProfileDataThumbnail())
                    .blockingGet();

            textViewMatchName.setText(interlocutorData.user.displayName);
            imageViewAvatar.setImageDrawable(null);
            glide.load(interlocutorData.photo.userPhotoUri)
                    .thumbnail(glide.load(interlocutorData.photo.userPhotoThumb))
                    .into(imageViewAvatar);

            List<Message> lastMessages = chatData.lastMessages();
            if (lastMessages.isEmpty()) {
                textViewLastMessage.setVisibility(View.INVISIBLE);
                imageViewLastMessageAvatar.setVisibility(View.INVISIBLE);
                textViewLastMessageDate.setText(getLastMessageDateFormatted(chatData.data().currentUserChatMembership
                        ().lastInteraction()));
                imageViewNewMessageIndicator.setVisibility(View.GONE);
            } else {
                Message lastMessage = lastMessages.get(lastMessages.size() - 1);
                ProfileDataThumbnail lastMessageUserData = Observable.fromIterable(dialog.membersData())
                        .filter(chatMemberProfileData -> chatMemberProfileData.profileData().user.uId.equals
                                (lastMessage.getUserId()))
                        .map(ChatMemberData::profileData)
                        .single(defaultValues.defaultProfileDataThumbnail(lastMessage.getUserId()))
                        .blockingGet();

                textViewLastMessageDate.setText(getLastMessageDateFormatted(lastMessage.getTimestamp()));

                if (MessageToTextConverter.isStringifiable(lastMessage.getContentType())) {
                    textViewLastMessage.setVisibility(View.VISIBLE);
                    textViewLastMessage.setText(MessageToTextConverter.stringifyMessage(context, currentUserId,
                            Chattable
                                    .ChatType.DIALOG, lastMessageUserData.user.displayName, lastMessage));
                    glide.load(lastMessageUserData.photo.userPhotoUri)
                            .into(imageViewLastMessageAvatar);
                    if (chatData.currentUserLastSeenMessage().size() > 0 &&
                            chatData.currentUserLastSeenMessage().get(0).value().equals(lastMessage.getMessageId()) ||
                            lastMessage.getUserId().equals(currentUserId)) {
                        imageViewNewMessageIndicator.setVisibility(View.GONE);
                    } else {
                        imageViewNewMessageIndicator.setVisibility(View.VISIBLE);
                    }

                    // SPECIAL CASE
                    if (lastMessage.getContentType() == ContentType.INITIAL_MESSAGE) {
                        textViewLastMessage.setVisibility(View.VISIBLE);
                        textViewLastMessage.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
                        imageViewLastMessageAvatar.setVisibility(View.GONE);
                    } else {
                        textViewLastMessage.setTextColor(ContextCompat.getColor(context, R.color
                                .colorSecondaryTextInverse));
                        imageViewLastMessageAvatar.setVisibility(View.VISIBLE);
                    }
                } else {
                    textViewLastMessage.setVisibility(View.INVISIBLE);
                    imageViewLastMessageAvatar.setVisibility(View.INVISIBLE);
                    imageViewNewMessageIndicator.setVisibility(View.GONE);

                }
            }
            imageViewNewGroupCandidate.setVisibility(View.GONE);
            switch (chatData.notificationMuteState().state()) {
                case NORMAL:
                    imageViewMuteIndicator.setVisibility(View.GONE);
                    break;
                case NO_SOUND:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_volume_off_white_18dp);
                    break;
                case MUTE_1_HOUR:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_notifications_paused_white_18dp);
                    break;
                case MUTED_INDEFINITELY:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_notifications_off_white_18dp);
                    break;
            }
        }

        public void onBindGroupViewHolder(DetailedChatData chatData) {
            chat = chatData;
            GroupChatData group = (GroupChatData) chatData.data();
            textViewMatchName.setText(group.chat().value().name());

            List<Message> lastMessages = chatData.lastMessages();
            if (lastMessages.isEmpty()) {
                textViewLastMessage.setVisibility(View.INVISIBLE);
                imageViewLastMessageAvatar.setVisibility(View.INVISIBLE);
                textViewLastMessageDate.setText(getLastMessageDateFormatted(group.chat().value()
                        .groupCreationTimestamp().value()));
                imageViewNewMessageIndicator.setVisibility(View.GONE);
            } else {
                Message lastMessage = lastMessages.get(lastMessages.size() - 1);
                ProfileDataThumbnail lastMessageUserData = Observable.fromIterable(group.membersData())
                        .filter(chatMemberProfileData -> chatMemberProfileData.profileData().user.uId.equals
                                (lastMessage.getUserId()))
                        .map(ChatMemberData::profileData)
                        .single(defaultValues.defaultProfileDataThumbnail(lastMessage.getUserId()))
                        .blockingGet();

                textViewLastMessageDate.setText(getLastMessageDateFormatted(lastMessage.getTimestamp()));
                if (MessageToTextConverter.isStringifiable(lastMessage.getContentType())) {
                    textViewLastMessage.setVisibility(View.VISIBLE);
                    imageViewLastMessageAvatar.setVisibility(View.VISIBLE);
                    textViewLastMessage.setText(MessageToTextConverter.stringifyMessage(context, currentUserId,
                            Chattable.ChatType.GROUP, lastMessageUserData.user.displayName, lastMessage));
                    glide.load(lastMessageUserData.photo.userPhotoUri)
                            .into(imageViewLastMessageAvatar);
                    if (chatData.currentUserLastSeenMessage().size() > 0 &&
                            chatData.currentUserLastSeenMessage().get(0).value().equals(lastMessage.getMessageId()) ||
                            lastMessage.getUserId().equals(currentUserId)) {
                        imageViewNewMessageIndicator.setVisibility(View.GONE);
                    } else {
                        imageViewNewMessageIndicator.setVisibility(View.VISIBLE);
                    }
                } else {
                    textViewLastMessage.setVisibility(View.INVISIBLE);
                    imageViewLastMessageAvatar.setVisibility(View.INVISIBLE);
                    imageViewNewMessageIndicator.setVisibility(View.GONE);
                }
                textViewLastMessage.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryTextInverse));
            }
            imageViewAvatar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    imageViewAvatar.getViewTreeObserver().removeOnPreDrawListener(this);
                    imageViewAvatar.setImageDrawable(null);
                    subscriptions.add(group.photos()
                            .toList()
                            .flatMapObservable(photoUris -> BitmapUtils.collage(imageViewAvatar.getContext(), photoUris,
                                    imageViewAvatar.getWidth(), imageViewAvatar.getHeight()))
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(bitmap -> imageViewAvatar
                                            .setImageBitmap(bitmap),
                                    throwable -> Timber.e(throwable, "Failed to set group image bitmap")));
                    return true;
                }
            });
            if (chat.hasJoinRequests() && chat.data().currentUserChatMembership().membershipStatus() ==
                    MembershipStatus.ACTIVE) {
                imageViewNewGroupCandidate.setVisibility(View.VISIBLE);
            } else {
                imageViewNewGroupCandidate.setVisibility(View.GONE);
            }
            switch (chatData.notificationMuteState().state()) {
                case NORMAL:
                    imageViewMuteIndicator.setVisibility(View.GONE);
                    break;
                case NO_SOUND:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_volume_off_white_18dp);
                    break;
                case MUTE_1_HOUR:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_notifications_paused_white_18dp);
                    break;
                case MUTED_INDEFINITELY:
                    imageViewMuteIndicator.setVisibility(View.VISIBLE);
                    imageViewMuteIndicator.setImageResource(R.drawable.ic_notifications_off_white_18dp);
                    break;
            }
        }

        public void onUnbind() {
            subscriptions.clear();
            //TODO This crashes
//            Glide.with(imageViewAvatar).clear(imageViewAvatar);
            imageViewAvatar.setImageBitmap(null);
            imageViewLastMessageAvatar.setImageBitmap(null);
        }


        private String getLastMessageDateFormatted(long lastMessageTimestamp) {
            return DateUtils.formatSameDayTime(lastMessageTimestamp, System.currentTimeMillis(),
                    DateFormat.MEDIUM, DateFormat.SHORT).toString();
        }

        @Override
        public void onClick(View v) {
            if (chat != null) {
                chatDataThumbnailPublishSubject.onNext(chat);
            }
        }
    }

    public static class RefreshItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.background)
        View background;

        @BindView(R.id.progressView)
        ProgressBar progressView;

        public RefreshItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setRefreshing(boolean refreshing) {
            if (refreshing) {
                background.setVisibility(View.VISIBLE);
            } else {
                background.setVisibility(View.GONE);
            }
        }
    }
}
