package com.bimber.features.chatlist.search;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.domain.chats.search.SearchForChatsUseCase.DialogSearchResult;
import com.bimber.domain.chats.search.SearchForChatsUseCase.GroupSearchResult;
import com.bimber.domain.chats.search.SearchForChatsUseCase.SearchResult;
import com.bimber.utils.images.BitmapUtils;
import com.bimber.utils.view.AbstractDiffUtilCallback;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 13.10.17.
 */

public class ChatSearchAdapter extends RecyclerView.Adapter<ChatSearchAdapter.SearchResultViewHolder> {

    private final PublishSubject<SearchResult> clickedSearchResultSubject = PublishSubject.create();


    private List<SearchResult> searchResults = Collections.emptyList();
    private final GlideRequests glide;

    public ChatSearchAdapter(GlideRequests glide) {
        this.glide = glide;
    }

    @Override
    public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_list_search_result_item,
                parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchResultViewHolder holder, int position) {
        SearchResult searchResult = searchResults.get(position);

        if (searchResult instanceof DialogSearchResult) {
            holder.onBindDialogViewHolder((DialogSearchResult) searchResult);
        } else if (searchResult instanceof GroupSearchResult) {
            holder.onBindGroupViewHolder((GroupSearchResult) searchResult);
        }

    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    public void setSearchResults(List<SearchResult> searchResults) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<SearchResult>(this
                .searchResults,
                searchResults) {

            @Override
            public boolean areItemsTheSame(SearchResult oldItem, SearchResult newItem) {
                if (oldItem instanceof DialogSearchResult && newItem instanceof DialogSearchResult) {
                    return ((DialogSearchResult) oldItem).result().user.uId
                            .equals(((DialogSearchResult) newItem).result().user.uId);

                } else if (oldItem instanceof GroupSearchResult && newItem instanceof GroupSearchResult) {
                    return ((GroupSearchResult) oldItem).result().group().key()
                            .equals(((GroupSearchResult) newItem).result().group().key());
                } else {
                    return false;
                }
            }

            @Override
            public boolean areContentsTheSame(SearchResult oldItem, SearchResult newItem) {
                return oldItem.result().equals(newItem.result());
            }
        });
        this.searchResults = searchResults;
        diffResult.dispatchUpdatesTo(this);
    }

    public Observable<SearchResult> searchResultClickedObservable() {
        return clickedSearchResultSubject;
    }

    public class SearchResultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        @BindView(R.id.imageViewAvatar)
        ImageView imageViewAvatar;
        @BindView(R.id.textViewChatName)
        TextView textViewChatName;

        private SearchResult searchResult;
        private CompositeDisposable subscriptions = new CompositeDisposable();

        public SearchResultViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            rootView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }


        public void onBindDialogViewHolder(DialogSearchResult searchResult) {
            this.searchResult = searchResult;
            textViewChatName.setText(searchResult.result().user.displayName);
            glide.load(searchResult.result().photo.userPhotoUri).into(imageViewAvatar);
        }

        public void onBindGroupViewHolder(GroupSearchResult searchResult) {
            this.searchResult = searchResult;
            textViewChatName.setText(searchResult.result().group().value().name());

            imageViewAvatar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    imageViewAvatar.getViewTreeObserver().removeOnPreDrawListener(this);
                    subscriptions.add(searchResult.result().photos()
                            .toList()
                            .flatMapObservable(photoUris -> BitmapUtils.collage(imageViewAvatar.getContext(), photoUris,
                                    imageViewAvatar.getWidth(), imageViewAvatar.getHeight()))
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(bitmap -> imageViewAvatar
                                            .setImageBitmap(bitmap),
                                    throwable -> Timber.e(throwable, "Failed to set group image bitmap")));
                    return true;
                }
            });
        }

        public void onUnbind() {
            subscriptions.clear();
            imageViewAvatar.setImageBitmap(null);
        }

        @Override
        public void onClick(View v) {
            if (searchResult != null) {
                clickedSearchResultSubject.onNext(searchResult);
            }
        }
    }
}
