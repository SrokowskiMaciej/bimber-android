package com.bimber.features.chatlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.domain.chats.GetUserChatsDataUseCase.DetailedChatData;
import com.bimber.domain.chats.search.SearchForChatsUseCase;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.chatlist.ChatListContract.IChatListPresenter;
import com.bimber.features.chatlist.ChatListContract.IChatListView;
import com.bimber.features.chatlist.search.ChatSearchAdapter;
import com.bimber.features.home.ChatButtonsProvider;
import com.bimber.features.home.ChatButtonsProvider.ChatButton;
import com.bimber.features.home.ChatButtonsProvider.ChatButtonConsumer;
import com.bimber.utils.firebase.Key;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.lapism.searchview.Search;
import com.lapism.searchview.widget.SearchView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by srokowski.maciej@gmail.com on 28.10.16.
 */

public class ChatListView extends BaseFragment implements IChatListView, ChatButtonConsumer {
    public static final String SHARE_INTENT_KEY = "SHARE_INTENT_KEY";

    public static final int CHAT_BATCH_COUNT = 8;
    @Inject
    IChatListPresenter chatPresenter;
    @Inject
    ChatButtonsProvider chatButtonsProvider;
    @BindView(R.id.recyclerViewChats)
    RecyclerView recyclerViewChats;
    @BindView(R.id.noChatsView)
    LinearLayout noChatsView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.recyclerSearchResults)
    RecyclerView recyclerSearchResults;
    @BindView(R.id.progressBarSearch)
    ProgressBar progressBarSearch;
    @BindView(R.id.searchChatsLayout)
    FrameLayout searchChatsLayout;


    private ChatsAdapter chatsAdapter;
    private ChatSearchAdapter chatSearchAdapter;

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DefaultValues defaultValues;

    private LinearLayoutManager layoutManager;
    private RecyclerView.OnScrollListener requestMoreChatsScrollListener;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    public static ChatListView newInstanceWithShare(Intent intent) {
        ChatListView chatListView = new ChatListView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SHARE_INTENT_KEY, intent);
        chatListView.setArguments(bundle);
        return chatListView;
    }

    public static ChatListView newInstance() {
        return new ChatListView();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.view_chat_list, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        //TODO Uh, making this better shouldn't be too hard but I'm tired now
        if (getArguments() != null && getArguments().containsKey(SHARE_INTENT_KEY)) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(toolbar);
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setTitle(R.string.share_toolbar_text);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(toolbar -> activity.onBackPressed());
        } else {
            setupSearchView();
            toolbar.inflateMenu(R.menu.menu_chat_list);
            toolbar.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.search:
                        searchView.open(item);
                        break;
                }

                return false;
            });
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        chatsAdapter = new ChatsAdapter(getContext(), currentUserId, defaultValues, GlideApp.with(this));
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewChats.setLayoutManager(layoutManager);
        recyclerViewChats.setAdapter(chatsAdapter);
        recyclerViewChats.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R
                .drawable.shape_list_divider_insets_80_dp), true));
        chatSearchAdapter = new ChatSearchAdapter(GlideApp.with(this));
        recyclerSearchResults.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
                                                                       false));
        recyclerSearchResults.setAdapter(chatSearchAdapter);
        recyclerSearchResults.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext()
                , R.drawable.shape_list_divider_insets_80_dp), true));
        requestMoreChatsScrollListener = setupRequestMoreChatsScrollListener();
        chatPresenter.bindView(this);
        chatPresenter.requestChats(chatsAdapter.getItemCount() + CHAT_BATCH_COUNT);
        subscriptions.add(chatsAdapter.chatClickedObserver()
                                  .map(dialogDataThumbnail -> Key.of(dialogDataThumbnail.data().chat().key(), dialogDataThumbnail.data
                                          ().chatType()))
                                  .subscribe(this::startChatActivity));
        subscriptions.add(chatSearchAdapter.searchResultClickedObservable()
                                  .map(searchResult -> Key.of(searchResult.chatId(), searchResult.chatType()))
                                  .subscribe(this::startChatActivity));
    }

    @NonNull
    private RecyclerView.OnScrollListener setupRequestMoreChatsScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (layoutManager.findLastVisibleItemPosition() >= chatsAdapter.getItemCount() - 1) {
                    chatPresenter.requestChats(chatsAdapter.getItemCount() + CHAT_BATCH_COUNT);
                }

            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
//        searchView.close();
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        chatPresenter.unbindView(this);
        super.onDestroyView();
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new Search.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(CharSequence query) {
                return false;
            }

            @Override
            public void onQueryTextChange(CharSequence newText) {
                chatPresenter.searchForChats(newText.toString());
            }
        });
    }

    @Override
    public void setChats(List<DetailedChatData> chats) {
        boolean shouldScrollToLastMessage = layoutManager.findFirstCompletelyVisibleItemPosition() == 0;
        chatsAdapter.setChats(chats);
        if (shouldScrollToLastMessage) {
            layoutManager.scrollToPosition(0);
        }
        if (chats.isEmpty()) {
            noChatsView.setVisibility(View.VISIBLE);
        } else {
            noChatsView.setVisibility(View.GONE);
        }
        recyclerViewChats.clearOnScrollListeners();
        recyclerViewChats.addOnScrollListener(requestMoreChatsScrollListener);
    }

    @Override
    public void setSearchResults(SearchForChatsUseCase.ChatSearchResults searchResults) {
        if (searchResults.searchPhrase().isEmpty()) {
            searchChatsLayout.setVisibility(View.GONE);
        } else {
            searchChatsLayout.setVisibility(View.VISIBLE);
            if (searchResults.searchResults().isEmpty()) {
                progressBarSearch.setVisibility(View.VISIBLE);
            } else {
                progressBarSearch.setVisibility(View.GONE);
            }
        }
        chatSearchAdapter.setSearchResults(searchResults.searchResults());
    }

    @Override
    public void setProgressVisible(boolean visible) {
        chatsAdapter.setRefreshVisible(visible);
    }


    private void startChatActivity(Key<Chattable.ChatType> chat) {
        if (getArguments() != null && getArguments().containsKey(SHARE_INTENT_KEY)) {
            getActivity().startActivity(ChatActivity.newInstance(getActivity(), chat.key(), chat.value(),
                                                                 getArguments().getParcelable(SHARE_INTENT_KEY)));
        } else {
            getActivity().startActivity(ChatActivity.newInstance(getActivity(), chat.key(), chat.value()));
        }
    }

    @Override
    public boolean shouldChatButtonBeVisible(ChatButton chatButton) {
        return false;
    }

    @Override
    public void onClickChatButton(ChatButton chatButton) {

    }
}
