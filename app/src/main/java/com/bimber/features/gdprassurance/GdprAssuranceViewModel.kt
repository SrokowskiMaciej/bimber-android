package com.bimber.features.gdprassurance

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bimber.data.repositories.FirebaseAuthService
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource.GDPRConsentStatus.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class GdprAssuranceViewModel
@Inject
constructor(private val authService: FirebaseAuthService,
            private val gdprConsentNetworkSource: GDPRConsentNetworkSource) : ViewModel() {

    private val disposables = CompositeDisposable()


    val closeViewSignal = MutableLiveData<Boolean>()


    fun userAgreed() {
        disposables.add(authService.onUserLoggedInUid()
                .firstElement()
                .map {
                    gdprConsentNetworkSource.setGDPRConsentStatus(it, AGREED_V1)
                    it
                }
                .subscribeBy(onSuccess = {
                    closeViewSignal.postValue(true)
                }))

    }

    fun userDisagreed() {
        disposables.add(authService.onUserLoggedInUid()
                .firstElement()
                .map {
                    gdprConsentNetworkSource.setGDPRConsentStatus(it, REFUSED)
                    it
                }
                .subscribeBy(onSuccess = {
                    closeViewSignal.postValue(true)
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}