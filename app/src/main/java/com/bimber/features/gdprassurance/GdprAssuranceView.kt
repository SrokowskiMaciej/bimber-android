package com.bimber.features.gdprassurance

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bimber.R
import com.bimber.base.ViewModelFactory
import com.bimber.base.activities.BaseActivity
import com.bimber.base.getViewModel
import com.bimber.base.withViewModel
import com.bimber.utils.arch.nonNull
import com.bimber.utils.arch.observe
import com.bimber.utils.io.readTextAndClose
import javax.inject.Inject

class GdprAssuranceView : BaseActivity() {

    @BindView(R.id.buttonDisagree)
    lateinit var buttonDisagree: Button
    @BindView(R.id.buttonAgree)
    lateinit var buttonAgree: Button
    @BindView(R.id.webViewDataProtectionContent)
    lateinit var webViewDataProtectionContent: WebView

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var gdprAssuranceViewModel: GdprAssuranceViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_gdpr_assurance)
        ButterKnife.bind(this)
        webViewDataProtectionContent.loadData(resources.openRawResource(R.raw.gdpr_consent).readTextAndClose(), "text/html; charset=utf-8", "UTF-8")
        gdprAssuranceViewModel = getViewModel(viewModelFactory)
        withViewModel<GdprAssuranceViewModel>(viewModelFactory) {
            closeViewSignal.nonNull().observe(this@GdprAssuranceView, {
                if (it) {
                    finish()
                }
            })
        }
    }


    @OnClick(R.id.buttonDisagree, R.id.buttonAgree)
    fun onButtonClicked(view: View) {
        when (view.id) {
            R.id.buttonAgree -> gdprAssuranceViewModel.userAgreed()
            R.id.buttonDisagree -> gdprAssuranceViewModel.userDisagreed()
        }
    }
}