package com.bimber.features.sharereceiver;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.home.ChatButtonsProvider;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class ShareReceiverActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(ShareReceiverActivity activity);

    @Provides
    @ActivityScope
    public static ChatButtonsProvider contributeChatButtonsProvider(ShareReceiverActivity shareReceiverActivity) {
        return shareReceiverActivity;
    }

}
