package com.bimber.features.sharereceiver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.chatlist.ChatListView;
import com.bimber.features.home.ChatButtonsProvider;
import com.bimber.utils.media.MediaUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 29.05.17.
 */

public class ShareReceiverActivity extends BaseLoggedInActivity implements ChatButtonsProvider {

    public static final String CHAT_LIST_VIEW_TAG = "CHAT_LIST_VIEW_TAG";

    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        handleShareIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (isFinishing()) return;
        handleShareIntent(intent);
    }

    @Override
    protected void onDestroy() {
        subscriptions.clear();
        super.onDestroy();
    }

    private void handleShareIntent(Intent shareIntent) {
        subscriptions.add(repackageIntent(shareIntent)
                .subscribe(this::showChatList, throwable -> {
                    Timber.e(throwable, "Failed to unpack share intent");
                    Toast.makeText(this, "Cannot share this", Toast.LENGTH_SHORT).show();
                    finish();
                }));

    }

    private Single<Intent> repackageIntent(Intent shareIntent) {
        String action = shareIntent.getAction();
        String type = shareIntent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                //No need to modify anything
                return Single.just(new Intent(shareIntent));
            } else if (type.startsWith("image/")) {
                return MediaUtils.writeToLocalTempFile(this, shareIntent.getParcelableExtra(Intent.EXTRA_STREAM))
                        .map(uri -> {
                            Intent intent = new Intent(shareIntent);
                            intent.putExtra(Intent.EXTRA_STREAM, uri);
                            return intent;
                        });
            } else {
                return Single.error(new IllegalArgumentException("Unsupported media type"));
            }
        } else {
            return Single.error(new IllegalArgumentException("Unsupported send action"));
        }
    }

    private void showChatList(Intent shareIntent) {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, ChatListView.newInstanceWithShare(shareIntent),
                CHAT_LIST_VIEW_TAG).commit();
    }

    @Override
    public void validateChatButtons() {

    }
}

