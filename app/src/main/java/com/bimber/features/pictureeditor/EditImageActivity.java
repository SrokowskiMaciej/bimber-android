package com.bimber.features.pictureeditor;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.activities.BaseActivity;
import com.bimber.base.glide.GlideApp;
import com.bimber.utils.view.TouchConsumerView;
import com.bimber.utils.view.ViewUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.rm.freedrawview.FreeDrawView;
import com.thebluealliance.spectrum.SpectrumDialog;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.BitmapLoadUtils;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.OutputStream;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 12.07.17.
 */

public class EditImageActivity extends BaseActivity implements SpectrumDialog.OnColorSelectedListener {

    public static final String EXTRA_INPUT_URI = "EXTRA_INPUT_URI";
    public static final String EXTRA_OUTPUT_URI = "EXTRA_OUTPUT_URI";
    public static final String THEME_KEY = "THEME_KEY";

    public static final String EXTRA_OUTPUT_CROP_ASPECT_RATIO = "EXTRA_OUTPUT_CROP_ASPECT_RATIO";
    public static final String EXTRA_ERROR = "EXTRA_ERROR";

    public static final String ASPECT_RATIO_KEY = "ASPECT_RATIO_KEY";
    public static final String CURRENT_DRAW_COLOR_KEY = "CURRENT_DRAW_COLOR_KEY";
    public static final String LAST_ACCEPTED_DRAW_COUNT_KEY = "LAST_ACCEPTED_DRAW_COUNT_KEY";
    public static final String INPUT_URI_KEY = "INPUT_URI_KEY";
    public static final String OUTPUT_URI_KEY = "OUTPUT_URI_KEY";


    public static final int RESULT_ERROR = 243;
    public static final String COLOR_PICKER_DIALOG_TAG = "COLOR_PICKER_DIALOG_TAG";

    @BindView(R.id.freeDrawView)
    FreeDrawView freeDrawView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.floatingActionButtonSend)
    FloatingActionButton floatingActionButtonSend;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.touchConsumerView)
    TouchConsumerView touchConsumerView;
    @BindView(R.id.drawToolboxLayout)
    LinearLayout drawToolboxLayout;
    @BindView(R.id.buttonColor)
    AppCompatImageButton buttonColor;
    @BindView(R.id.buttonDotWidth)
    Button buttonDotWidth;
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;

    private float aspectRatio;
    private Uri inputUri;
    private Uri outputUri;
    private int lastAcceptedDrawCount;
    @ColorInt
    private int currentDrawColorRes;
    private final CompositeDisposable subscribtions = new CompositeDisposable();
    private MaterialDialog progressBarDialog;

    public static Intent newInstance(Context context, Uri inputUri, Uri outputUri, @StyleRes int theme) {
        Intent intent = new Intent(context, EditImageActivity.class);
        intent.putExtra(EXTRA_INPUT_URI, inputUri);
        intent.putExtra(EXTRA_OUTPUT_URI, outputUri);
        intent.putExtra(THEME_KEY, theme);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setTheme(intent.getIntExtra(THEME_KEY, R.style.AppTheme));
        setContentView(R.layout.activity_image_edit);
        ButterKnife.bind(this);
        setupToolbar();
        setupProgressDialog();

        if (savedInstanceState != null) {
            aspectRatio = savedInstanceState.getFloat(ASPECT_RATIO_KEY);
            inputUri = savedInstanceState.getParcelable(INPUT_URI_KEY);
            outputUri = savedInstanceState.getParcelable(OUTPUT_URI_KEY);
            lastAcceptedDrawCount = savedInstanceState.getInt(LAST_ACCEPTED_DRAW_COUNT_KEY);
            currentDrawColorRes = savedInstanceState.getInt(CURRENT_DRAW_COLOR_KEY);
        } else {
            inputUri = intent.getParcelableExtra(EXTRA_INPUT_URI);
            outputUri = intent.getParcelableExtra(EXTRA_OUTPUT_URI);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(inputUri.getPath(), options);
            aspectRatio = ((float) options.outWidth / (float) options.outHeight);
            currentDrawColorRes = ContextCompat.getColor(this, R.color.md_pink_500);
        }
        buttonColor.setColorFilter(currentDrawColorRes);
        freeDrawView.setPaintColor(currentDrawColorRes);

        buttonDotWidth.setText(R.string.view_edit_image_brush_size_8p);
        freeDrawView.setPaintWidthDp(8);

        Fragment colorPicker = getSupportFragmentManager().findFragmentByTag(COLOR_PICKER_DIALOG_TAG);
        if (colorPicker != null) {
            ((SpectrumDialog) colorPicker).setOnColorSelectedListener(this);
        }
        setImageViewAspectRation(aspectRatio);
        loadInputImage();
    }

    private void setImageViewAspectRation(float aspectRatio) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(rootLayout);
        constraintSet.setDimensionRatio(R.id.imageView, String.valueOf(aspectRatio));
        constraintSet.applyTo(rootLayout);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_ACCEPTED_DRAW_COUNT_KEY, lastAcceptedDrawCount);
        outState.putFloat(ASPECT_RATIO_KEY, aspectRatio);
        outState.putParcelable(INPUT_URI_KEY, inputUri);
        outState.putParcelable(OUTPUT_URI_KEY, outputUri);
    }

    @Override
    public void onBackPressed() {
        if (drawToolboxLayout.getVisibility() == View.VISIBLE) {
            setDrawingState(false);
        } else {
            setResult(RESULT_CANCELED);
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        subscribtions.clear();
        imageView.setImageBitmap(null);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == RESULT_OK) {
                Uri output = UCrop.getOutput(data);
                if (output != null) {
                    inputUri = output;
                    aspectRatio = data.getFloatExtra(UCrop.EXTRA_OUTPUT_CROP_ASPECT_RATIO, aspectRatio);
                    setImageViewAspectRation(aspectRatio);
                    loadInputImage();
                } else {
                    Toast.makeText(this, "Couldn't crop image", Toast.LENGTH_LONG).show();
                    Timber.e("Something went wrong during cropping image, null output");

                }
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Throwable error = UCrop.getError(data);
                Toast.makeText(this, "Couldn't crop image", Toast.LENGTH_LONG).show();
                Timber.e(error, "Something went wrong during cropping image");
            } else if (resultCode == RESULT_CANCELED) {
                //Well that's normal
            } else {
                Timber.d("Something went wrong during cropping image");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_edit_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.item_brush:
                setDrawingState(true);
                return true;
            case R.id.item_crop:
                startCropActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean wasImageEdited() {
        return freeDrawView.getUndoCount() > 0;
    }

    private void setDrawingState(boolean drawing) {
        if (drawing) {
            showDrawingToolbox();
            touchConsumerView.setVisibility(View.INVISIBLE);
            floatingActionButtonSend.hide();
        } else {
            hideDrawingToolbox();
            touchConsumerView.setVisibility(View.VISIBLE);
            floatingActionButtonSend.show();
        }
    }

    private void showDrawingToolbox() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[] brushViewCenter = ViewUtils.getViewCenter(findViewById(R.id.item_brush));
            ViewAnimationUtils.createCircularReveal(drawToolboxLayout, brushViewCenter[0], brushViewCenter[1],
                    0, drawToolboxLayout.getWidth()).start();
            drawToolboxLayout.setVisibility(View.VISIBLE);
        } else {
            ViewCompat.setAlpha(drawToolboxLayout, 0);
            drawToolboxLayout.setVisibility(View.VISIBLE);
            ViewCompat.animate(drawToolboxLayout)
                    .alpha(1)
                    .start();
        }
    }

    private void hideDrawingToolbox() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[] brushViewCenter = ViewUtils.getViewCenter(findViewById(R.id.item_brush));
            Animator circularHide = ViewAnimationUtils.createCircularReveal(drawToolboxLayout, brushViewCenter[0], brushViewCenter[1],
                    drawToolboxLayout.getWidth(), 0);
            circularHide.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    drawToolboxLayout.setVisibility(View.INVISIBLE);
                }
            });
            circularHide.start();
        } else {
            ViewCompat.setAlpha(drawToolboxLayout, 1);
            ViewCompat.animate(drawToolboxLayout)
                    .alpha(0)
                    .setListener(new ViewPropertyAnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(View view) {
                            ViewCompat.animate(view).setListener(null);
                            drawToolboxLayout.setVisibility(View.GONE);
                            ViewCompat.setAlpha(drawToolboxLayout, 1);

                        }
                    })
                    .start();
        }
    }

    private void loadInputImage() {
        freeDrawView.clearDrawAndHistory();
        GlideApp.with(this)
                .load(inputUri)
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                .into(imageView);
    }

    private void startCropActivity() {
        subscribtions.add((Single.defer(() -> Single.just(freeDrawView))
                .doOnSubscribe(disposable -> progressBarDialog.show())
                .subscribeOn(AndroidSchedulers.mainThread())
                .flatMap(freeDrawView -> {
                    if (wasImageEdited()) {
                        return drawOverlayOnOriginalImage()
                                .flatMap(bitmap -> saveImageToFile(outputUri, bitmap)
                                        .doFinally(bitmap::recycle));
                    } else {
                        return Single.just(inputUri);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    progressBarDialog.dismiss();
                    UCrop.Options options = new UCrop.Options();
                    options.setFreeStyleCropEnabled(true);
                    options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                    options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                    options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
                    UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), UUID.randomUUID().toString())))
                            .withOptions(options)
                            .start(this);
                }, throwable -> {
                    progressBarDialog.dismiss();
                    Timber.e(throwable, "Failed to draw overlay on original image");
                    setResultError(throwable);
                    finish();
                })));

    }

    private void setupProgressDialog() {
        progressBarDialog = new MaterialDialog.Builder(this)
                .progress(true, 0)
                .content(R.string.view_image_edit_progress_text)
                .cancelable(false)
                .build();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(backView -> onBackPressed());
    }

    private void startColorPicker() {
        SpectrumDialog spectrumDialog = new SpectrumDialog.Builder(this)
                .setColors(R.array.brush_colors)
                .setSelectedColor(currentDrawColorRes)
                .build();
        spectrumDialog.setOnColorSelectedListener(this);
        spectrumDialog.show(getSupportFragmentManager(), COLOR_PICKER_DIALOG_TAG);
    }

    private void startDotWidthPicker() {
        PopupMenu popupMenu = new PopupMenu(this, buttonDotWidth);
        popupMenu.inflate(R.menu.menu_edit_image_dot_width);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_dot_width_4:
                        buttonDotWidth.setText(R.string.view_edit_image_brush_size_4p);
                        freeDrawView.setPaintWidthDp(4);
                        break;
                    case R.id.item_dot_width_8:
                        buttonDotWidth.setText(R.string.view_edit_image_brush_size_8p);
                        freeDrawView.setPaintWidthDp(8);
                        break;
                    case R.id.item_dot_width_16:
                        buttonDotWidth.setText(R.string.view_edit_image_brush_size_16p);
                        freeDrawView.setPaintWidthDp(16);
                        break;
                    case R.id.item_dot_width_32:
                        buttonDotWidth.setText(R.string.view_edit_image_brush_size_32p);
                        freeDrawView.setPaintWidthDp(32);
                        break;
                    case R.id.item_dot_width_64:
                        buttonDotWidth.setText(R.string.view_edit_image_brush_size_64p);
                        freeDrawView.setPaintWidthDp(64);
                        break;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    protected void setResultUri(Uri uri, float resultAspectRatio) {
        setResult(RESULT_OK, new Intent()
                .putExtra(EXTRA_OUTPUT_URI, uri)
                .putExtra(EXTRA_OUTPUT_CROP_ASPECT_RATIO, resultAspectRatio)
        );
    }

    protected void setResultError(Throwable throwable) {
        setResult(RESULT_ERROR, new Intent().putExtra(UCrop.EXTRA_ERROR, throwable));
    }

    private void sendImage() {
        subscribtions.add(Single.defer(() -> Single.just(freeDrawView))
                .doOnSubscribe(disposable -> progressBarDialog.show())
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.io())
                .flatMap(freeDrawView -> {
                    if (wasImageEdited()) {
                        return drawOverlayOnOriginalImage()
                                .flatMap(bitmap -> saveImageToFile(outputUri, bitmap)
                                        .doFinally(bitmap::recycle));
                    } else {

                        return copyFiles(inputUri, outputUri);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    progressBarDialog.dismiss();
                    setResultUri(uri, aspectRatio);
                    finish();
                }, throwable -> {
                    progressBarDialog.dismiss();
                    Timber.e(throwable, "Failed to edit image");
                    setResultError(throwable);
                    finish();
                }));
    }

    private Single<Uri> copyFiles(Uri inputUri, Uri outputUri) {
        return Single.defer(() -> {
            FileUtils.copyFile(inputUri.getPath(), outputUri.getPath());
            return Single.just(outputUri);
        });
    }


    private Single<Uri> saveImageToFile(Uri uri, Bitmap bitmap) {
        return Single.just(uri)
                .map(uri1 -> {
                    OutputStream outputStream = null;
                    try {
                        outputStream = getContentResolver().openOutputStream(uri);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        bitmap.recycle();
                    } finally {
                        BitmapLoadUtils.close(outputStream);
                    }
                    return uri;
                });
    }

    private Single<Bitmap> getCurrentDrawOverlay() {
        return Single.<Bitmap>create(e -> {
            freeDrawView.getDrawScreenshot(new FreeDrawView.DrawCreatorListener() {
                @Override
                public void onDrawCreated(Bitmap draw) {
                    e.onSuccess(draw);
                }

                @Override
                public void onDrawCreationError() {
                    e.onError(new IllegalStateException("Failed to obtain drawign screenshot"));
                }
            });
        }).subscribeOn(Schedulers.io());
    }


    private Single<Bitmap> getOriginalImage() {
        return Single.fromFuture(GlideApp.with(this)
                .asBitmap()
                .load(inputUri)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
                .subscribeOn(Schedulers.io());

    }

    private Single<Bitmap> drawOverlayOnOriginalImage() {
        return Single.zip(getOriginalImage(), getCurrentDrawOverlay(),
                (originalBitmap, overlayBitmap) -> {
                    Bitmap resultBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), originalBitmap
                            .getConfig());
                    Bitmap scaledOverlayBitmap = Bitmap.createScaledBitmap(overlayBitmap, originalBitmap.getWidth(), originalBitmap
                            .getHeight(), true);
                    Canvas canvas = new Canvas(resultBitmap);
                    canvas.drawBitmap(originalBitmap, 0, 0, null);
                    canvas.drawBitmap(scaledOverlayBitmap, 0, 0, null);
                    originalBitmap.recycle();
                    overlayBitmap.recycle();
                    scaledOverlayBitmap.recycle();
                    return resultBitmap;
                })
                .subscribeOn(Schedulers.computation());
    }

    @OnClick({R.id.floatingActionButtonSend, R.id.buttonDotWidth, R.id.buttonColor, R.id.buttonUndoDrawing, R.id.buttonCancelDrawing,
            R.id.buttonDrawingDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonDotWidth:
                startDotWidthPicker();
                break;
            case R.id.buttonColor:
                startColorPicker();
                break;
            case R.id.buttonUndoDrawing:
                freeDrawView.undoLast();
                break;
            case R.id.buttonCancelDrawing:
                setDrawingState(false);
                while (freeDrawView.getUndoCount() > lastAcceptedDrawCount) freeDrawView.undoLast();
                break;
            case R.id.buttonDrawingDone:
                setDrawingState(false);
                lastAcceptedDrawCount = freeDrawView.getUndoCount();
                break;
            case R.id.floatingActionButtonSend:
                sendImage();
                break;
        }
    }

    @Override
    public void onColorSelected(boolean positiveResult, @ColorInt int color) {
        currentDrawColorRes = color;
        buttonColor.setColorFilter(currentDrawColorRes);
        freeDrawView.setPaintColor(currentDrawColorRes);
    }
}
