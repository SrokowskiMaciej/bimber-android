package com.bimber.features.photogallery.profile;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 08.07.17.
 */

public class ThumbnailsAdapter extends RecyclerView.Adapter<ThumbnailsAdapter.ThumbnailViewHolder> {

    private List<UserPhoto> photos = Collections.emptyList();

    private final PublishSubject<SelectionChange> selectedPhotoPublishSubject = PublishSubject.create();

    private UserPhoto selectedPhoto;
    private final GlideRequests glide;

    public ThumbnailsAdapter(GlideRequests glide) {
        super();
        this.glide = glide;
        setHasStableIds(true);
    }

    @Override
    public ThumbnailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_gallery_thumbnail_item, parent,
                false);
        return new ThumbnailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ThumbnailViewHolder holder, int position) {
        UserPhoto photo = photos.get(position);
        glide.load(photo.userPhotoUri)
                .apply(new RequestOptions().centerCrop())
                .into(holder.imageView);
        if (selectedPhoto != null && photo.photoId.equals(selectedPhoto.photoId)) {
            holder.viewSelectionBorder.setVisibility(View.VISIBLE);
        } else {
            holder.viewSelectionBorder.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).photoId.hashCode();
    }

    public void setPhotos(List<UserPhoto> photos) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<UserPhoto>(this.photos, photos) {
            @Override
            public boolean areItemsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.photoId.equals(newItem.photoId);
            }

            @Override
            public boolean areContentsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.photos = photos;
        diffResult.dispatchUpdatesTo(this);
        setSelectedPhoto(this.selectedPhoto);
    }

    public void setSelectedPhoto(UserPhoto selectedPhoto) {
        if (this.selectedPhoto != null && selectedPhoto.photoId.equals(this.selectedPhoto.photoId))
            return;
        int previousSelectedPosition = getItemPosition(this.selectedPhoto);
        this.selectedPhoto = selectedPhoto;
        int newSelectedPosition = getItemPosition(this.selectedPhoto);
        notifyItemChanged(previousSelectedPosition);
        notifyItemChanged(newSelectedPosition);
    }

    public int getItemPosition(UserPhoto photo) {
        for (int i = 0; i < photos.size(); i++) {
            if (photo != null && photos.get(i).photoId.equals(photo.photoId)) {
                return i;
            }
        }
        return 0;
    }


    public UserPhoto getPhotoAtPosition(int position) {
        return photos.get(position);
    }

    public UserPhoto getSelectedPhoto() {
        return selectedPhoto;
    }

    public PublishSubject<SelectionChange> selectedPhoto() {
        return selectedPhotoPublishSubject;
    }

    public static class SelectionChange {
        public final int previousPosition;
        public final int newPosition;
        public final UserPhoto newPhoto;

        private SelectionChange(int previousPosition, int newPosition, UserPhoto newPhoto) {
            this.previousPosition = previousPosition;
            this.newPosition = newPosition;
            this.newPhoto = newPhoto;
        }
    }

    public class ThumbnailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.viewSelectionBorder)
        View viewSelectionBorder;

        public ThumbnailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.imageView)
        public void onViewClicked() {
            int adapterPosition = getAdapterPosition();
            int itemPosition = getItemPosition(selectedPhoto);
            selectedPhotoPublishSubject.onNext(new SelectionChange(itemPosition, adapterPosition, photos.get
                    (adapterPosition)));
        }
    }
}
