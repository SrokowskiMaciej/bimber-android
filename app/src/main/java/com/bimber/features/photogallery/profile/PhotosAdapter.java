package com.bimber.features.photogallery.profile;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.features.common.ProgressView;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 08.07.17.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder> {

    private List<UserPhoto> photos = Collections.emptyList();
    private final Activity animatedActivity;
    private final GlideRequests glide;

    public PhotosAdapter(Activity animatedActivity, GlideRequests glide) {
        super();
        this.animatedActivity = animatedActivity;
        this.glide = glide;
        setHasStableIds(true);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .view_photo_gallery_chat_messages_photo_item, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        UserPhoto photo = photos.get(position);
        holder.imageView.setScale(holder.imageView.getMinimumScale());
        holder.imageView.setAllowParentInterceptOnEdge(true);
        holder.progressView.setVisibility(View.VISIBLE);
        ViewCompat.setTransitionName(holder.imageView, photo.photoId);
        glide.load(photo.userPhotoUri)
                .apply(RequestOptions.noAnimation())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable>
                            target, boolean isFirstResource) {
                        ActivityCompat.startPostponedEnterTransition(animatedActivity);
                        holder.progressView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        ActivityCompat.startPostponedEnterTransition(animatedActivity);
                        holder.progressView.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).photoId.hashCode();
    }

    public void setAnimatedPhoto(UserPhoto messagePhoto) {
        this.photos = Collections.singletonList(messagePhoto);
        notifyItemInserted(0);
    }

    public void setPhotos(List<UserPhoto> photos) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<UserPhoto>(this.photos, photos) {
            @Override
            public boolean areItemsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.photoId.equals(newItem.photoId);
            }

            @Override
            public boolean areContentsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.photos = photos;
        diffResult.dispatchUpdatesTo(this);
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        PhotoView imageView;

        @BindView(R.id.progressView)
        ProgressView progressView;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imageView.setOnScaleChangeListener((scale, focusX, focusY) ->
                    imageView.setAllowParentInterceptOnEdge(imageView.getScale() <= /*Choosen by testing*/1.1f));
        }
    }
}
