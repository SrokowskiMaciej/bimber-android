package com.bimber.features.photogallery.chat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.ShareProvider;
import com.bimber.base.chat.ChatProvider;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.domain.model.GroupData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryContract.IChatPhotoGalleryPresenter;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryContract.IChatPhotoGalleryView;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryPresenter.ChatPhoto;
import com.bimber.features.sharereceiver.ShareReceiverActivity;
import com.bimber.utils.images.BitmapUtils;
import com.bimber.utils.view.RichOnScrollListener;
import com.bimber.utils.view.ViewUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.github.kevelbreh.androidunits.AndroidUnit;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 07.07.17.
 */

public class ChatPhotoGalleryView extends BaseFragment implements IChatPhotoGalleryView, ChatProvider {

    public static final int LOGO_SIZE = (int) AndroidUnit.DENSITY_PIXELS.toPixels(32);

    public static final String MESSAGE_PHOTO_KEY = "MESSAGE_PHOTO_KEY";
    public static final String INITIAL_PHOTO_COUNT_KEY = "INITIAL_PHOTO_COUNT_KEY";
    public static final String CHAT_ID_KEY = "CHAT_ID_KEY";
    public static final String CHAT_TYPE_KEY = "CHAT_TYPE_KEY";

    public static final String LAYOUT_MANAGER_THUMBNAILS_STATE_KEY = "LAYOUT_MANAGER_THUMBNAILS_STATE_KEY";
    public static final String LAYOUT_MANAGER_PHOTOS_STATE_KEY = "LAYOUT_MANAGER_PHOTOS_STATE_KEY";
    public static final String REQUESTED_PHOTOS_KEY = "REQUESTED_PHOTOS_KEY";
    public static final int REQUESTED_PHOTOS_INCREMENT = 10;
    public static final String SELECTED_PHOTO_KEY = "SELECTED_PHOTO_KEY";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewPhotos)
    RecyclerView recyclerViewPhotos;
    @BindView(R.id.recyclerViewThumbnails)
    RecyclerView recyclerViewThumbnails;

    @Inject
    IChatPhotoGalleryPresenter chatPhotoGalleryPresenter;
    @BindView(R.id.imageViewChatLogo)
    CircleImageView imageViewChatLogo;
    @BindView(R.id.textViewChatTitle)
    TextView textViewChatTitle;

    private PhotosAdapter photosAdapter;
    private LinearLayoutManager photosLayoutManager;
    private ThumbnailsAdapter thumbnailsAdapter;
    private LinearLayoutManager thumbnailsLayoutManager;

    private Parcelable layoutManagerStatePhotos;
    private Parcelable layoutManagerStateThumbnails;
    private int requestedPhotos = REQUESTED_PHOTOS_INCREMENT;

    private ChatType chatType;
    private String chatId;
    private Message selectedPhotoMessage;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    public static ChatPhotoGalleryView newInstance(String chatId, ChatType chatType, Message photoMessage, int initialPhotoCount) {
        ChatPhotoGalleryView chatPhotoGalleryView = new ChatPhotoGalleryView();
        Bundle bundle = new Bundle();
        bundle.putString(CHAT_ID_KEY, chatId);
        bundle.putString(CHAT_TYPE_KEY, chatType.toString());
        bundle.putSerializable(MESSAGE_PHOTO_KEY, photoMessage);
        bundle.putInt(INITIAL_PHOTO_COUNT_KEY, initialPhotoCount);
        chatPhotoGalleryView.setArguments(bundle);
        return chatPhotoGalleryView;
    }

    @Override
    public void onAttach(Context context) {
        chatId = getArguments().getString(CHAT_ID_KEY);
        chatType = ChatType.valueOf(getArguments().getString(CHAT_TYPE_KEY));
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_photo_gallery_chat, container, false);
        ButterKnife.bind(this, view);
        setupToolbar();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMainPhotosRecyclerView();
        setupPhotosScrollListener();
        setupRequestMorePhotosListener();
        setupThumbnailsRecyclerView();

        if (savedInstanceState != null) {
            selectedPhotoMessage = (Message) savedInstanceState.getSerializable(SELECTED_PHOTO_KEY);
            requestedPhotos = savedInstanceState.getInt(REQUESTED_PHOTOS_KEY);
        } else {
            selectedPhotoMessage = (Message) getArguments().getSerializable(MESSAGE_PHOTO_KEY);
            requestedPhotos = Math.max(getArguments().getInt(INITIAL_PHOTO_COUNT_KEY), requestedPhotos);
        }

        subscriptions.add(thumbnailsAdapter.onPhotoClicked().subscribe(photoClickedAction -> {
            chatPhotoGalleryPresenter.setSelectedPhoto(photoClickedAction.photo().photoMessage());
            recyclerViewPhotos.smoothScrollToPosition(photoClickedAction.position());
            this.selectedPhotoMessage = photoClickedAction.photo().photoMessage();
        }));

        chatPhotoGalleryPresenter.setSelectedPhoto(selectedPhotoMessage);
        chatPhotoGalleryPresenter.requestPhotos(requestedPhotos);
        chatPhotoGalleryPresenter.bindView(this);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            layoutManagerStatePhotos = savedInstanceState.getParcelable(LAYOUT_MANAGER_PHOTOS_STATE_KEY);
            layoutManagerStateThumbnails = savedInstanceState.getParcelable(LAYOUT_MANAGER_THUMBNAILS_STATE_KEY);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_share_image, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_share:
                handleShareAction(selectedPhotoMessage, false);
                break;
            case R.id.item_forward:
                handleShareAction(selectedPhotoMessage, true);
                break;

        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SELECTED_PHOTO_KEY, selectedPhotoMessage);
        outState.putParcelable(LAYOUT_MANAGER_PHOTOS_STATE_KEY, photosLayoutManager.onSaveInstanceState());
        outState.putParcelable(LAYOUT_MANAGER_THUMBNAILS_STATE_KEY, thumbnailsLayoutManager.onSaveInstanceState());
        outState.putInt(REQUESTED_PHOTOS_KEY, requestedPhotos);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        chatPhotoGalleryPresenter.unbindView(this);
        layoutManagerStatePhotos = photosLayoutManager.onSaveInstanceState();
        layoutManagerStateThumbnails = thumbnailsLayoutManager.onSaveInstanceState();
        super.onDestroyView();
    }

    private void setupToolbar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }

    private void setupMainPhotosRecyclerView() {
        photosLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        photosLayoutManager.setStackFromEnd(true);
        recyclerViewPhotos.setLayoutManager(photosLayoutManager);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPhotos);
        RecyclerView.ItemAnimator animator = recyclerViewPhotos.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        photosAdapter = new PhotosAdapter(getActivity(), GlideApp.with(this));
        recyclerViewPhotos.setAdapter(photosAdapter);
    }

    private void setupThumbnailsRecyclerView() {
        thumbnailsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        thumbnailsLayoutManager.setStackFromEnd(true);
        recyclerViewThumbnails.setLayoutManager(thumbnailsLayoutManager);
        thumbnailsAdapter = new ThumbnailsAdapter(GlideApp.with(this));
        recyclerViewThumbnails.setAdapter(thumbnailsAdapter);
    }

    private void setupPhotosScrollListener() {
        recyclerViewPhotos.addOnScrollListener(new RichOnScrollListener() {

            boolean movingForward;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState, boolean isUserInitiated) {
                if (isUserInitiated && (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_SETTLING)) {
                    int selectedPosition;
                    if (movingForward) {
                        selectedPosition = photosLayoutManager.findLastVisibleItemPosition();
                    } else {
                        selectedPosition = photosLayoutManager.findFirstVisibleItemPosition();
                    }
                    if (selectedPosition >= 0) {
                        if (selectedPosition < thumbnailsLayoutManager.findFirstCompletelyVisibleItemPosition() ||
                                selectedPosition > thumbnailsLayoutManager.findLastCompletelyVisibleItemPosition()) {
                            recyclerViewThumbnails.smoothScrollToPosition(selectedPosition);
                        }
                        ChatPhoto photoAtPosition = thumbnailsAdapter.getPhotoAtPosition(selectedPosition);
                        if (photoAtPosition != null) {
                            selectedPhotoMessage = photoAtPosition.photoMessage();
                            chatPhotoGalleryPresenter.setSelectedPhoto(photoAtPosition.photoMessage());
                        }
                    }

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy, boolean isUserInitiated) {
                movingForward = dx > 0;
            }
        });
    }

    private void setupRequestMorePhotosListener() {
        recyclerViewThumbnails.addOnScrollListener(new RichOnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy, boolean isUserInitiated) {
                if (isUserInitiated && thumbnailsLayoutManager.findFirstVisibleItemPosition() <= 3) {
                    requestedPhotos = thumbnailsAdapter.getItemCount() + REQUESTED_PHOTOS_INCREMENT;
                    chatPhotoGalleryPresenter.requestPhotos(requestedPhotos);
                }
            }
        });
    }

    private void handleShareAction(Message photoMessage, boolean forwardMode) {
        ImageContent imageContent = (ImageContent) photoMessage.getContent();
        subscriptions.add(prepareImageForSharing(imageContent)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                            .setStream(uri)
                            .getIntent();

                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.setType(ShareProvider.SHARE_MIME_TYPE_IMAGE_JPEG);
                    if (forwardMode) {
                        shareIntent.setClass(getActivity().getApplicationContext(), ShareReceiverActivity.class);
                    }
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share_action_title)));
                }, throwable -> {
                    Timber.e(throwable, "Couldn't share image");
                    Toast.makeText(getContext(), "Couldn't share image", Toast.LENGTH_LONG).show();
                }));
    }

    private Single<Uri> prepareImageForSharing(ImageContent imageContent) {
        return Single.defer(() -> Single.just(imageContent))
                .subscribeOn(Schedulers.io())
                .flatMap(content -> {
                    try {
                        return Single.just(Glide.with(this)
                                .load(imageContent.uri())
                                .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                .get());
                    } catch (InterruptedException | ExecutionException e) {
                        throw Exceptions.propagate(e);
                    }
                })
                .flatMap(file -> Single.just(FileProvider.getUriForFile(getContext(), getString(R.string.file_prvider_authority), file)));
    }

    @Override
    public void setPhotoMessages(List<ChatPhoto> photoMessages) {
        photosAdapter.setMessagePhotos(photoMessages);
        thumbnailsAdapter.setMessagePhotos(photoMessages);

        if (layoutManagerStateThumbnails != null) {
            thumbnailsLayoutManager.onRestoreInstanceState(layoutManagerStateThumbnails);
            layoutManagerStateThumbnails = null;
        }
        if (layoutManagerStatePhotos != null) {
            photosLayoutManager.onRestoreInstanceState(layoutManagerStatePhotos);
            layoutManagerStatePhotos = null;
        }
    }

    @Override
    public void showErrorScreen() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .onNeutral((dialog, which) -> ViewUtils.finishActivitySafely(getActivity()))
                .show();
    }

    @Override
    public void showDialogInterlocutorInfo(ProfileDataThumbnail interlocutorProfileData) {
        textViewChatTitle.setText(interlocutorProfileData.user.displayName);
        Glide.with(this).load(interlocutorProfileData.photo.userPhotoUri)
                .into(imageViewChatLogo);
    }

    @Override
    public void showGroupInfo(GroupData publicGroupDataId) {
        textViewChatTitle.setText(publicGroupDataId.group().value().name());
        subscriptions.add(publicGroupDataId.photosUris()
                .toList()
                .flatMap(membersPhotos -> BitmapUtils.bitmapCollage(getContext(), membersPhotos, LOGO_SIZE, LOGO_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(getContext(), R.drawable.ic_group_white_24dp, LOGO_SIZE, LOGO_SIZE))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bitmap -> imageViewChatLogo.setImageBitmap(bitmap)));
    }

    @Override
    public String provideChatId() {
        return chatId;
    }

    @Override
    public ChatType provideChatType() {
        return chatType;
    }

    @OnClick(R.id.imageViewBack)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }
}
