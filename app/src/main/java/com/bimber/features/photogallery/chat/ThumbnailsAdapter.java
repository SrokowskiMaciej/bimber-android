package com.bimber.features.photogallery.chat;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryPresenter.ChatPhoto;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.request.RequestOptions;
import com.google.auto.value.AutoValue;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 08.07.17.
 */

public class ThumbnailsAdapter extends RecyclerView.Adapter<ThumbnailsAdapter.ThumbnailViewHolder> {

    private List<ChatPhoto> messagePhotos = Collections.emptyList();

    private final GlideRequests glide;
    private final PublishSubject<ChatPhotoClickedAction> clickedPhotoPublishSubject = PublishSubject.create();


    public ThumbnailsAdapter(GlideRequests glide) {
        super();
        this.glide = glide;
        setHasStableIds(true);
    }

    @Override
    public ThumbnailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_gallery_thumbnail_item, parent,
                false);
        return new ThumbnailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ThumbnailViewHolder holder, int position) {
        ChatPhoto chatPhoto = messagePhotos.get(position);
        ImageContent content = (ImageContent) chatPhoto.photoMessage().getContent();

        glide.load(content.uri())
                .apply(new RequestOptions().centerCrop())
                .into(holder.imageView);

        if (chatPhoto.isSelected()) {
            holder.viewSelectionBorder.setVisibility(View.VISIBLE);
        } else {
            holder.viewSelectionBorder.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return messagePhotos.size();
    }

    @Override
    public long getItemId(int position) {
        return messagePhotos.get(position).photoMessage().getMessageId().hashCode();
    }

    public void setMessagePhotos(List<ChatPhoto> messagePhotos) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<ChatPhoto>(this
                .messagePhotos, messagePhotos) {
            @Override
            public boolean areItemsTheSame(ChatPhoto oldItem, ChatPhoto newItem) {
                return oldItem.photoMessage().getMessageId().equals(newItem.photoMessage().getMessageId());
            }

            @Override
            public boolean areContentsTheSame(ChatPhoto oldItem, ChatPhoto newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.messagePhotos = messagePhotos;
        diffResult.dispatchUpdatesTo(this);
    }

    @Nullable
    public ChatPhoto getPhotoAtPosition(int position) {
        if (messagePhotos.size() > position) {
            return messagePhotos.get(position);
        } else {
            return null;
        }
    }

    public PublishSubject<ChatPhotoClickedAction> onPhotoClicked() {
        return clickedPhotoPublishSubject;
    }

    @AutoValue
    public static abstract class ChatPhotoClickedAction {

        public abstract ChatPhoto photo();

        public abstract int position();

        public static ChatPhotoClickedAction create(ChatPhoto photo, int position) {
            return new AutoValue_ThumbnailsAdapter_ChatPhotoClickedAction(photo, position);
        }
    }

    public class ThumbnailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.viewSelectionBorder)
        View viewSelectionBorder;

        public ThumbnailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.imageView)
        public void onViewClicked() {
            int adapterPosition = getAdapterPosition();
            clickedPhotoPublishSubject.onNext(ChatPhotoClickedAction.create(messagePhotos.get(adapterPosition),
                    adapterPosition));
        }
    }
}
