package com.bimber.features.photogallery.edit

import android.graphics.drawable.Drawable
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bimber.R
import com.bimber.base.glide.GlideRequests
import com.bimber.data.entities.profile.local.UserPhoto
import com.bimber.data.entities.profile.network.UploadStatus
import com.bimber.utils.view.AbstractDiffUtilCallback
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemViewHolder
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by srokowski.maciej@gmail.com on 21.12.16.
 */

class EditUserPhotosAdapter(private val glide: GlideRequests) : RecyclerView.Adapter<EditUserPhotosAdapter.PhotoViewHolder>(), DraggableItemAdapter<EditUserPhotosAdapter.PhotoViewHolder>, DraggableItemConstants {

    private var photos = emptyList<UserPhoto>()
    private val photosReorderedSubject = PublishSubject.create<Map<String, Int>>()
    private val photoRemovedSubject = PublishSubject.create<String>()
    private val photoClickedSubject = PublishSubject.create<ClickedPhotoAction>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return PhotoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout
                .view_photo_gallery_edit_user_photo_item,
                parent, false))
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = photos[position]
        holder.textViewPhotoPosition!!.text = (position + 1).toString()
        holder.progressBar!!.visibility = View.VISIBLE
        if (photo.userPhotoUploadStatus == UploadStatus.FINISHED) {
            glide.load(photos[position].userPhotoUri)
                    .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                            holder.progressBar!!.visibility = View.VISIBLE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>,
                                                     dataSource: DataSource,
                                                     isFirstResource: Boolean): Boolean {
                            holder.progressBar!!.visibility = View.INVISIBLE
                            return false
                        }
                    })
                    .into(holder.imageViewPhoto!!)
        } else {
            holder.imageViewPhoto?.setImageDrawable(null)
        }
        holder.imageButtonDelete!!.setOnClickListener { onItemRemoveButtonClicked(photo) }
    }

    override fun getItemCount(): Int {
        return photos.size
    }


    override fun onCheckCanStartDrag(holder: PhotoViewHolder, position: Int, x: Int, y: Int): Boolean {
        return true
    }

    override fun onGetItemDraggableRange(holder: PhotoViewHolder, position: Int): ItemDraggableRange? {
        return null
    }

    override fun onMoveItem(fromPosition: Int, toPosition: Int) {
        if (fromPosition == toPosition) {
            return
        }

        val movedPhoto = photos[fromPosition]
        val rearrangedPhotoList = ArrayList(photos)
        rearrangedPhotoList.removeAt(fromPosition)
        rearrangedPhotoList.add(toPosition, movedPhoto)
        val rearrangedPhotos = rearrangedPhotoList.filter { it.userPhotoUploadStatus == UploadStatus.FINISHED }
                .mapIndexed { index, userPhoto ->
                    UserPhoto(userPhoto.photoUserId, userPhoto.photoId,
                            index, userPhoto.userPhotoUri, userPhoto.userPhotoThumb, userPhoto.userPhotoUploadStatus)

                }

        val alteredPhotoOrder = rearrangedPhotos
                .fold(HashMap<String, Int>(), { acc, userPhoto ->
                    acc[userPhoto.photoId] = userPhoto.userPhotoIndex
                    acc
                })
        photosReorderedSubject.onNext(alteredPhotoOrder)
        setPhotos(rearrangedPhotos)
    }


    private fun onItemRemoveButtonClicked(photo: UserPhoto) {
        photoRemovedSubject.onNext(photo.photoId)
    }


    override fun onCheckCanDrop(draggingPosition: Int, dropPosition: Int): Boolean {
        return true
    }

    override fun onItemDragStarted(position: Int) {
        notifyDataSetChanged()
    }

    override fun onItemDragFinished(fromPosition: Int, toPosition: Int, result: Boolean) {
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return photos[position].photoId.hashCode().toLong()
    }

    fun setPhotos(photos: List<UserPhoto>) {
        val diffResult = DiffUtil.calculateDiff(object : AbstractDiffUtilCallback<UserPhoto>(this.photos, photos) {
            override fun areItemsTheSame(oldItem: UserPhoto, newItem: UserPhoto): Boolean {
                return oldItem.photoId == newItem.photoId
            }

            override fun areContentsTheSame(oldItem: UserPhoto, newItem: UserPhoto): Boolean {
                return oldItem == newItem
            }
        })
        this.photos = photos
        diffResult.dispatchUpdatesTo(this)
    }

    fun onPhotosReordered(): Observable<Map<String, Int>> {
        return photosReorderedSubject
    }

    fun onPhotoRemoved(): Observable<String> {
        return photoRemovedSubject
    }

    fun onPhotoClicked(): Observable<ClickedPhotoAction> {
        return photoClickedSubject
    }

    inner class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), DraggableItemViewHolder {
        @BindView(R.id.imageViewPhoto)
        @JvmField
        internal var imageViewPhoto: ImageView? = null
        @BindView(R.id.textViewPhotoPosition)
        @JvmField
        internal var textViewPhotoPosition: TextView? = null
        @BindView(R.id.imageButtonDelete)
        @JvmField
        internal var imageButtonDelete: ImageButton? = null
        @BindView(R.id.progressBar)
        @JvmField
        internal var progressBar: ProgressBar? = null

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun setDragStateFlags(flags: Int) {

        }

        override fun getDragStateFlags(): Int {
            return 0
        }

        @OnClick(R.id.imageViewPhoto)
        fun onViewClicked(view: View) {
            photoClickedSubject.onNext(ClickedPhotoAction(imageViewPhoto!!, photos[adapterPosition]))

        }
    }

    class ClickedPhotoAction(val clickedView: ImageView, val photo: UserPhoto)
}
