package com.bimber.features.photogallery.profile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.app.ActivityCompat;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.network.UserPhotoModel;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryView;
import com.bimber.utils.firebase.Key;

/**
 * Created by maciek on 02.03.17.
 */

public class ProfilePhotoGalleryActivity extends BaseLoggedInActivity {

    private static final String PROFILE_ID_ARG = "PROFILE_ID_ARG";
    public static final String PHOTO_KEY = "PHOTO_KEY";
    public static final String THEME_KEY = "THEME_KEY";

    public static final String PHOTO_GALLERY_VIEW_FRAGEMNT_TAG = "PHOTO_GALLERY_VIEW_FRAGEMNT_TAG";

    public static Intent newInstance(Context context, String profileId, UserPhoto photo, @StyleRes int theme) {
        Intent intent = new Intent(context, ProfilePhotoGalleryActivity.class);
        intent.putExtra(PROFILE_ID_ARG, profileId);
        intent.putExtra(PHOTO_KEY, photo);
        intent.putExtra(THEME_KEY, theme);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(getIntent().getExtras().getInt(THEME_KEY));
        setContentView(R.layout.activity_single_pane);
        ActivityCompat.postponeEnterTransition(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementsUseOverlay(false);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String profileId = getIntent().getStringExtra(PROFILE_ID_ARG);
        UserPhoto photo = (UserPhoto) getIntent().getSerializableExtra(PHOTO_KEY);
        if (getSupportFragmentManager().findFragmentByTag(PHOTO_GALLERY_VIEW_FRAGEMNT_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, ProfilePhotoGalleryView.newInstance(profileId, photo),
                            PHOTO_GALLERY_VIEW_FRAGEMNT_TAG)
                    .commit();
        }
    }
}
