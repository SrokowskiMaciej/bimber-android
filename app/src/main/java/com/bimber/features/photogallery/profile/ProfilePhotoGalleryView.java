package com.bimber.features.photogallery.profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.ShareProvider;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.base.profile.ProfileProvider;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryContract.IProfilePhotoGalleryPresenter;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryContract.IProfilePhotoGalleryView;
import com.bimber.features.sharereceiver.ShareReceiverActivity;
import com.bimber.utils.view.ViewUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.Collections;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 07.07.17.
 */

public class ProfilePhotoGalleryView extends BaseFragment implements IProfilePhotoGalleryView, ProfileProvider {

    public static final String PHOTO_KEY = "PHOTO_KEY";
    public static final String PROFILE_ID_KEY = "PROFILE_ID_KEY";

    public static final String LAYOUT_MANAGER_THUMBNAILS_STATE_KEY = "LAYOUT_MANAGER_THUMBNAILS_STATE_KEY";
    public static final String LAYOUT_MANAGER_PHOTOS_STATE_KEY = "LAYOUT_MANAGER_PHOTOS_STATE_KEY";
    public static final String SELECTED_PHOTO_KEY = "SELECTED_PHOTO_KEY";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewPhotos)
    RecyclerView recyclerViewPhotos;
    @BindView(R.id.recyclerViewThumbnails)
    RecyclerView recyclerViewThumbnails;

    @Inject
    IProfilePhotoGalleryPresenter profilePhotoGalleryPresenter;
    @BindView(R.id.imageViewChatLogo)
    CircleImageView imageViewChatLogo;
    @BindView(R.id.textViewChatTitle)
    TextView textViewChatTitle;

    private PhotosAdapter photosAdapter;
    private LinearLayoutManager photosLayoutManager;
    private ThumbnailsAdapter thumbnailsAdapter;
    private LinearLayoutManager thumbnailsLayoutManager;

    private Parcelable layoutManagerStatePhotos;
    private Parcelable layoutManagerStateThumbnails;

    private String profileId;

    private final CompositeDisposable subscriptions = new CompositeDisposable();


    public static ProfilePhotoGalleryView newInstance(String profileId, UserPhoto photo) {
        ProfilePhotoGalleryView profilePhotoGalleryView = new ProfilePhotoGalleryView();
        Bundle bundle = new Bundle();
        bundle.putString(PROFILE_ID_KEY, profileId);
        bundle.putSerializable(PHOTO_KEY, photo);
        profilePhotoGalleryView.setArguments(bundle);
        return profilePhotoGalleryView;
    }

    @Override
    public void onAttach(Context context) {
        profileId = getArguments().getString(PROFILE_ID_KEY);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_photo_gallery_chat, container, false);
        ButterKnife.bind(this, view);
        setupToolbar();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMainPhotosRecyclerView();
        addThumbnailDrivingScrollListenerToPhotosRecyclerView();
        setupThumbnailsRecyclerView();

        UserPhoto photo = (UserPhoto) getArguments().getSerializable(PHOTO_KEY);
        thumbnailsAdapter.setSelectedPhoto(photo);

        photosAdapter.setAnimatedPhoto(photo);
        thumbnailsAdapter.setPhotos(Collections.singletonList(photo));

        subscriptions.add(thumbnailsAdapter.selectedPhoto().subscribe(selectionChange -> {
            photosAdapter.notifyItemChanged(selectionChange.previousPosition);
            recyclerViewPhotos.scrollToPosition(selectionChange.newPosition);
            setSelectedThumbnailPhoto(selectionChange.newPhoto);
        }));

        profilePhotoGalleryPresenter.bindView(this);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            thumbnailsAdapter.setSelectedPhoto(savedInstanceState.getParcelable(SELECTED_PHOTO_KEY));
            layoutManagerStatePhotos = savedInstanceState.getParcelable(LAYOUT_MANAGER_PHOTOS_STATE_KEY);
            layoutManagerStateThumbnails = savedInstanceState.getParcelable(LAYOUT_MANAGER_THUMBNAILS_STATE_KEY);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_share_image, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_share:
                handleShareAction(thumbnailsAdapter.getSelectedPhoto(), false);
                break;
            case R.id.item_forward:
                handleShareAction(thumbnailsAdapter.getSelectedPhoto(), true);
                break;

        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SELECTED_PHOTO_KEY, thumbnailsAdapter.getSelectedPhoto());
        outState.putParcelable(LAYOUT_MANAGER_PHOTOS_STATE_KEY, photosLayoutManager.onSaveInstanceState());
        outState.putParcelable(LAYOUT_MANAGER_THUMBNAILS_STATE_KEY, thumbnailsLayoutManager.onSaveInstanceState());
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        profilePhotoGalleryPresenter.unbindView(this);
        layoutManagerStatePhotos = photosLayoutManager.onSaveInstanceState();
        layoutManagerStateThumbnails = thumbnailsLayoutManager.onSaveInstanceState();
        super.onDestroyView();
    }

    private void setupToolbar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }

    private void setupMainPhotosRecyclerView() {
        photosLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        photosLayoutManager.setStackFromEnd(true);
        recyclerViewPhotos.setLayoutManager(photosLayoutManager);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewPhotos);
        photosAdapter = new PhotosAdapter(getActivity(), GlideApp.with(this));
        recyclerViewPhotos.setAdapter(photosAdapter);
    }

    private void setupThumbnailsRecyclerView() {
        thumbnailsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        thumbnailsLayoutManager.setStackFromEnd(true);
        recyclerViewThumbnails.setLayoutManager(thumbnailsLayoutManager);
        thumbnailsAdapter = new ThumbnailsAdapter(GlideApp.with(this));
        recyclerViewThumbnails.setAdapter(thumbnailsAdapter);
    }

    private void setSelectedThumbnailPhoto(UserPhoto selectedPhoto) {
        thumbnailsAdapter.setSelectedPhoto(selectedPhoto);
    }

    private void addThumbnailDrivingScrollListenerToPhotosRecyclerView() {
        recyclerViewPhotos.addOnScrollListener(new RecyclerView.OnScrollListener() {

            boolean movingForward;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_SETTLING) {
                    int selectedPosition;
                    if (movingForward) {
                        selectedPosition = photosLayoutManager.findLastVisibleItemPosition();
                    } else {
                        selectedPosition = photosLayoutManager.findFirstVisibleItemPosition();
                    }
                    setSelectedThumbnailPhoto(thumbnailsAdapter.getPhotoAtPosition(selectedPosition));
                    setThumbnailsToPosition(selectedPosition);

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                movingForward = dx > 0;
            }
        });
    }

    private void setThumbnailsToPosition(int position) {
        if (position < thumbnailsLayoutManager.findFirstCompletelyVisibleItemPosition() ||
                position > thumbnailsLayoutManager.findLastCompletelyVisibleItemPosition()) {
            recyclerViewThumbnails.smoothScrollToPosition(position);
        }
    }

    private void handleShareAction(UserPhoto photo, boolean forwardMode) {
        subscriptions.add(prepareImageForSharing(photo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                            .setStream(uri)
                            .getIntent();

                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.setType(ShareProvider.SHARE_MIME_TYPE_IMAGE_JPEG);
                    if (forwardMode) {
                        shareIntent.setClass(getActivity().getApplicationContext(), ShareReceiverActivity.class);
                    }
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share_action_title)));
                }, throwable -> {
                    Timber.e(throwable, "Couldn't share image");
                    Toast.makeText(getContext(), "Couldn't share image", Toast.LENGTH_LONG).show();
                }));
    }

    private Single<Uri> prepareImageForSharing(UserPhoto photo) {
        return Single.defer(() -> Single.just(photo))
                .subscribeOn(Schedulers.io())
                .flatMap(content -> {
                    try {
                        return Single.just(Glide.with(this)
                                .load(photo.userPhotoUri)
                                .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                .get());
                    } catch (InterruptedException | ExecutionException e) {
                        throw Exceptions.propagate(e);
                    }
                })
                .flatMap(file -> Single.just(FileProvider.getUriForFile(getContext(), getString(R.string.file_prvider_authority), file)));
    }

    @Override
    public void showProfileInfo(ProfileDataFull profileData) {
        photosAdapter.setPhotos(profileData.photos);
        thumbnailsAdapter.setPhotos(profileData.photos);
        if (layoutManagerStateThumbnails != null) {
            thumbnailsLayoutManager.onRestoreInstanceState(layoutManagerStateThumbnails);
            layoutManagerStateThumbnails = null;
        }
        if (layoutManagerStatePhotos != null) {
            photosLayoutManager.onRestoreInstanceState(layoutManagerStatePhotos);
            layoutManagerStatePhotos = null;
        }

        textViewChatTitle.setText(profileData.user.displayName);
        Glide.with(this).load(profileData.photos.get(0).userPhotoUri)
                .into(imageViewChatLogo);
    }

    @Override
    public void showErrorScreen() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .onNeutral((dialog, which) -> ViewUtils.finishActivitySafely(getActivity()))
                .show();
    }

    @Override
    public String provideProfileId() {
        return profileId;
    }

    @OnClick(R.id.imageViewBack)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }
}
