package com.bimber.features.photogallery.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryView;

/**
 * Created by maciek on 02.03.17.
 */

public class ChatPhotoGalleryActivity extends BaseLoggedInActivity {

    private static final String CHAT_ID_ARG = "CHAT_ID_ARG";
    private static final String CHAT_TYPE_ARG = "CHAT_TYPE_ARG";
    public static final String MESSAGE_PHOTO_KEY = "MESSAGE_PHOTO_KEY";

    public static final String PHOTO_GALLERY_VIEW_FRAGEMNT_TAG = "PHOTO_GALLERY_VIEW_FRAGEMNT_TAG";
    public static final String INITIAL_PHOTO_COUNT_KEY = "INITIAL_PHOTO_COUNT_KEY";

    public static Intent newInstance(Context context, String chatId, ChatType chatType, Message photoMessage, int initialPhotoCount) {
        Intent intent = new Intent(context, ChatPhotoGalleryActivity.class);
        intent.putExtra(CHAT_ID_ARG, chatId);
        intent.putExtra(CHAT_TYPE_ARG, chatType.toString());
        intent.putExtra(MESSAGE_PHOTO_KEY, photoMessage);
        intent.putExtra(INITIAL_PHOTO_COUNT_KEY, initialPhotoCount);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementsUseOverlay(false);
        }
        ActivityCompat.postponeEnterTransition(this);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String chatId = getIntent().getStringExtra(CHAT_ID_ARG);
        ChatType chatType = ChatType.valueOf(getIntent().getStringExtra(CHAT_TYPE_ARG));
        Message photoMessage = (Message) getIntent().getSerializableExtra(MESSAGE_PHOTO_KEY);
        int initialPhotoCount = getIntent().getIntExtra(INITIAL_PHOTO_COUNT_KEY, ChatPhotoGalleryView.REQUESTED_PHOTOS_INCREMENT);
        if (getSupportFragmentManager().findFragmentByTag(PHOTO_GALLERY_VIEW_FRAGEMNT_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, ChatPhotoGalleryView.newInstance(chatId, chatType, photoMessage, initialPhotoCount),
                            PHOTO_GALLERY_VIEW_FRAGEMNT_TAG)
                    .commit();
        }
    }
}
