package com.bimber.features.photogallery.profile;

import com.bimber.base.profile.ProfileModule;
import com.bimber.base.profile.ProfileProvider;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryContract.IProfilePhotoGalleryPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 08.07.17.
 */
@Module
public abstract class ProfilePhotoGalleryFragmentModule {

    @ContributesAndroidInjector(modules = ProfilePhotoGalleryModule.class)
    abstract ProfilePhotoGalleryView contribute();

    @Module(includes = ProfileModule.class)
    public abstract class ProfilePhotoGalleryModule {

        @Binds
        abstract ProfileProvider profileProvider(ProfilePhotoGalleryView profilePhotoGalleryView);

        @Binds
        abstract IProfilePhotoGalleryPresenter discoverPresenter(ProfilePhotoGalleryPresenter profilePhotoGalleryPresenter);
    }
}
