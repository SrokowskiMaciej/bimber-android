package com.bimber.features.photogallery.profile;

import com.bimber.base.profile.ProfileId;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryContract.IProfilePhotoGalleryPresenter;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryContract.IProfilePhotoGalleryView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 07.07.17.
 */

public class ProfilePhotoGalleryPresenter extends MvpAbstractPresenter<IProfilePhotoGalleryView> implements IProfilePhotoGalleryPresenter {

    private final String profileId;
    private final ProfileDataRepository profileDataRepository;

    @Inject
    public ProfilePhotoGalleryPresenter(@ProfileId String profileId, ProfileDataRepository profileDataRepository) {
        this.profileId = profileId;
        this.profileDataRepository = profileDataRepository;
    }

    private final CompositeDisposable subscribtions = new CompositeDisposable();

    @Override
    protected void viewAttached(IProfilePhotoGalleryView view) {
        subscribtions.add(profileDataRepository.onUserProfileDataFull(profileId)
                //FIXME Lol, dirty, do it as it is done in chat messages gallery
                .delay(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showProfileInfo,
                        throwable -> {
                            Timber.e(throwable, "Error loading user data");
                            view.showErrorScreen();
                        }));
    }

    @Override
    protected void viewDetached(IProfilePhotoGalleryView view) {
        subscribtions.clear();
    }
}
