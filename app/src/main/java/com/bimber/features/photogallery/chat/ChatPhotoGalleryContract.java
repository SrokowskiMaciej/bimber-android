package com.bimber.features.photogallery.chat;

import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.domain.model.GroupData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 07.07.17.
 */

public interface ChatPhotoGalleryContract {

    interface IChatPhotoGalleryView {
        void setPhotoMessages(List<ChatPhotoGalleryPresenter.ChatPhoto> chatPhotos);

        void showErrorScreen();

        void showDialogInterlocutorInfo(ProfileDataThumbnail interlocutorProfileData);

        void showGroupInfo(GroupData publicGroupData);
    }

    interface IChatPhotoGalleryPresenter extends MvpPresenter<IChatPhotoGalleryView> {

        void setSelectedPhoto(Message photoMessage);

        void requestPhotos(int photosCount);
    }

}
