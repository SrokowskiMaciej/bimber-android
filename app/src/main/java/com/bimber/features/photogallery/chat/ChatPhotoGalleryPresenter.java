package com.bimber.features.photogallery.chat;

import android.os.Parcelable;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.base.chat.ChatType;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryContract.IChatPhotoGalleryPresenter;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryContract.IChatPhotoGalleryView;
import com.bimber.utils.mvp.MvpAbstractPresenter;
import com.google.auto.value.AutoValue;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.BehaviorProcessor;
import timber.log.Timber;

/**
 * Created by maciek on 07.07.17.
 */

public class ChatPhotoGalleryPresenter extends MvpAbstractPresenter<IChatPhotoGalleryView> implements IChatPhotoGalleryPresenter {


    private final String currentUserId;
    private final String currentChatId;
    private final Chattable.ChatType currentChatType;
    private final DefaultValues defaultValues;
    private final MessageRepository messageRepository;
    private final ChatMemberRepository chatMemberRepository;
    private final ProfileDataRepository profileDataRepository;
    private final GetGroupDataUseCase getGroupDataUseCase;
    private final BehaviorProcessor<Integer> requestPhotoMessagesProcessor = BehaviorProcessor.create();
    private final BehaviorProcessor<Message> selectedPhotoMessagesProcessor = BehaviorProcessor.create();


    @Inject
    public ChatPhotoGalleryPresenter(@LoggedInUserId String currentUserId, @ChatId String currentChatId,
                                     @ChatType Chattable.ChatType currentChatType, DefaultValues defaultValues, MessageRepository
                                             messageRepository,
                                     ChatMemberRepository chatMemberRepository, ProfileDataRepository profileDataRepository,
                                     GetGroupDataUseCase getGroupDataUseCase) {
        this.currentUserId = currentUserId;
        this.currentChatId = currentChatId;
        this.currentChatType = currentChatType;
        this.defaultValues = defaultValues;
        this.messageRepository = messageRepository;
        this.chatMemberRepository = chatMemberRepository;
        this.profileDataRepository = profileDataRepository;
        this.getGroupDataUseCase = getGroupDataUseCase;
    }

    private final CompositeDisposable subscribtions = new CompositeDisposable();

    @Override
    protected void viewAttached(IChatPhotoGalleryView view) {
        subscribtions.add(Flowable.combineLatest(requestPhotoMessagesProcessor.distinctUntilChanged(),
                chatMemberRepository.onChatMembershipEvents(currentChatId, currentUserId).flatMap(Flowable::fromIterable),
                (photosRequestedCount, chatMembership) -> messageRepository.onChatMessages(chatMembership, photosRequestedCount, ContentType.IMAGE))
                .switchMap(photosFlowable -> photosFlowable
                        .flatMap(photos -> selectedPhotoMessagesProcessor.flatMapSingle(selectedPhoto -> Flowable.fromIterable(photos)
                                .map(photo -> ChatPhoto.create(photo, photo.getMessageId().equals(selectedPhoto.getMessageId())))
                                .concatWith(selectedPhotoMessagesProcessor.map(defaultSelection -> ChatPhoto.create(defaultSelection, true)).firstElement().toFlowable())
                                .distinct(chatPhoto -> chatPhoto.photoMessage().getMessageId())
                                .toList())))
                .startWith(selectedPhotoMessagesProcessor.map(selectedPhoto -> Collections.singletonList(ChatPhoto.create(selectedPhoto, true))).firstElement().toFlowable())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::setPhotoMessages,
                        throwable -> {
                            Timber.e(throwable, "Failed to load chat photos");
                            getView().showErrorScreen();
                        }));

        switch (currentChatType) {
            case DIALOG:
                subscribtions.add(chatMemberRepository.chatMemberships(currentChatId)
                        .flatMapPublisher(Flowable::fromIterable)
                        .filter(chatMembership -> !chatMembership.uId().equals(currentUserId))
                        .map(ChatVisibleChatMembership::uId)
                        .flatMapSingle(profileDataRepository::userProfileDataThumbnail)
                        .singleOrError()
                        .toObservable()
                        .doOnError(throwable -> Timber.e(throwable, "Error obtaining interlocutor. Substituting unknown user id"))
                        .defaultIfEmpty(defaultValues.deletedProfileDataThumbnail())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::showDialogInterlocutorInfo,
                                throwable -> Timber.e(throwable, "Error while trying to load interlocutor info")));

                break;
            case GROUP:
                subscribtions.add(getGroupDataUseCase.onGroupDataOptimized(currentChatId, 3)
                        .flatMap(Flowable::fromIterable)
                        .distinctUntilChanged(publicGroupDataThumbnails -> publicGroupDataThumbnails.group().value().name() +
                                String.valueOf(publicGroupDataThumbnails.group().value().image()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::showGroupInfo,
                                throwable -> Timber.e(throwable, "Error while listening to group info")));
                break;
        }
    }

    @Override
    protected void viewDetached(IChatPhotoGalleryView view) {
        subscribtions.clear();
    }

    @Override
    public void setSelectedPhoto(Message photoMessage) {
        selectedPhotoMessagesProcessor.onNext(photoMessage);

    }

    @Override
    public void requestPhotos(int photosCount) {
        requestPhotoMessagesProcessor.onNext(photosCount);
    }

    @AutoValue
    public abstract static class ChatPhoto implements Parcelable {
        public abstract Message photoMessage();

        public abstract boolean isSelected();

        public static ChatPhoto create(Message photoMessage, boolean isSelected) {
            return new AutoValue_ChatPhotoGalleryPresenter_ChatPhoto(photoMessage, isSelected);
        }
    }
}
