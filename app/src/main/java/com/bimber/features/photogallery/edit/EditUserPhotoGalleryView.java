package com.bimber.features.photogallery.edit;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.features.common.ErrorView;
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryContract.IEditUserPhotoGalleryPresenter;
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryContract.IEditUserPhotoGalleryView;
import com.bimber.features.photogallery.profile.activity.ProfilePhotoGalleryActivity;
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * Created by srokowski.maciej@gmail.com on 24.12.16.
 */

public class EditUserPhotoGalleryView extends BaseFragment implements IEditUserPhotoGalleryView {
    public static final int GET_PHOTO_TYPE = 7890;

    @BindView(R.id.floatingActionButtonAddFromGallery)
    FloatingActionButton floatingActionButtonAddFromGallery;
    @BindView(R.id.floatingActionButtonAddPhoto)
    FloatingActionButton floatingActionButtonAddPhoto;
    @BindView(R.id.recyclerViewPhotos)
    RecyclerView recyclerViewPhotos;
    @BindView(R.id.errorView)
    ErrorView errorView;
    @BindView(R.id.progressBarBackground)
    View progressBarBackground;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    IEditUserPhotoGalleryPresenter photoGalleryPresenter;
    @Inject
    @LoggedInUserId
    String currentUserId;


    private Disposable errorScreenSubscribtion;
    private CompositeDisposable subscribtions = new CompositeDisposable();
    private EditUserPhotosAdapter editUserPhotosAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_photo_gallery_edit_user, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerViewDragDropManager recyclerViewDragDropManager = new RecyclerViewDragDropManager();
        recyclerViewDragDropManager.setDraggingItemShadowDrawable((NinePatchDrawable) ContextCompat.getDrawable(getContext(),
                R.drawable.material_shadow));
        recyclerViewDragDropManager.setInitiateOnLongPress(true);
        recyclerViewDragDropManager.setInitiateOnMove(true);
        recyclerViewPhotos.setLayoutManager(new GridLayoutManager(getContext(), 2));
        editUserPhotosAdapter = new EditUserPhotosAdapter(GlideApp.with(this));
        recyclerViewPhotos.setAdapter(recyclerViewDragDropManager.createWrappedAdapter(editUserPhotosAdapter));
        recyclerViewPhotos.setItemAnimator(new DraggableItemAnimator());
        recyclerViewDragDropManager.attachRecyclerView(recyclerViewPhotos);
        photoGalleryPresenter.bindView(this);
        subscribtions.add(editUserPhotosAdapter.onPhotosReordered().subscribe(newPhotoOrder -> photoGalleryPresenter.reorderPhotos(newPhotoOrder)));
        subscribtions.add(editUserPhotosAdapter.onPhotoRemoved().subscribe(photoId -> photoGalleryPresenter.removePhoto(photoId)));
        subscribtions.add(editUserPhotosAdapter.onPhotoClicked().subscribe(clickedPhotoAction -> {
            ViewCompat.setTransitionName(clickedPhotoAction.getClickedView(), clickedPhotoAction.getPhoto().photoId);
            ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    clickedPhotoAction.getClickedView(), clickedPhotoAction.getPhoto().photoId);
            startActivity(ProfilePhotoGalleryActivity.newInstance(getContext(), currentUserId, clickedPhotoAction.getPhoto(),
                    R.style.AppTheme),
                    activityOptionsCompat.toBundle());
        }));
    }

    @Override
    public void onDestroyView() {
        subscribtions.clear();
        photoGalleryPresenter.unbindView(this);
        super.onDestroyView();
    }

    private Observable<Boolean> photoPermissionsObservable() {
        return new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    @OnClick({R.id.floatingActionButtonAddFromGallery, R.id.floatingActionButtonAddPhoto})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButtonAddFromGallery:
                photoPermissionsObservable().subscribe(permissionsProvided -> {
                    if (permissionsProvided) {
                        EasyImage.openGallery(this, GET_PHOTO_TYPE);
                    } else {
                        Toast.makeText(getContext(), "Permissions not provided", Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case R.id.floatingActionButtonAddPhoto:
                photoPermissionsObservable().subscribe(permissionsProvided -> {
                    if (permissionsProvided) {
                        EasyImage.openCamera(this, GET_PHOTO_TYPE);
                    } else {
                        Toast.makeText(getContext(), "Permissions not provided", Toast.LENGTH_LONG).show();

                    }
                });
                break;
        }
    }

    @Override
    public void setPhotos(List<UserPhoto> photos) {
        progressBarBackground.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        editUserPhotosAdapter.setPhotos(photos);
        floatingActionButtonAddPhoto.setVisibility(View.VISIBLE);
        floatingActionButtonAddFromGallery.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorPage() {
        progressBarBackground.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        floatingActionButtonAddPhoto.setVisibility(View.GONE);
        floatingActionButtonAddFromGallery.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        if (errorScreenSubscribtion != null && !errorScreenSubscribtion.isDisposed()) {
            errorScreenSubscribtion.dispose();
        }
        errorScreenSubscribtion = errorView.onErrorAction()
                .doOnNext(event -> errorView.setVisibility(View.GONE))
                .doOnNext(event -> progressBarBackground.setVisibility(View.VISIBLE))
                .doOnNext(event -> progressBar.setVisibility(View.VISIBLE))
                .delay(1, TimeUnit.SECONDS)
                .subscribe(event -> {
                    if (photoGalleryPresenter.isViewBound()) {
                        photoGalleryPresenter.unbindView(this);
                        photoGalleryPresenter.bindView(this);
                    }
                });


    }

    @Override
    public void showErrorToast(String message) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                UCrop.Options options = new UCrop.Options();
                options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                options.setToolbarColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                options.setActiveWidgetColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                UCrop.of(Uri.fromFile(imageFile), Uri.fromFile(new File(getContext().getCacheDir(), UUID.randomUUID().toString())))
                        .withAspectRatio(1.0f, 1.0f)
                        .withOptions(options)
                        .start(getContext(), EditUserPhotoGalleryView.this);
            }
        });
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == RESULT_OK) {
                Uri output = UCrop.getOutput(data);
                if (output != null) {
                    photoGalleryPresenter.addPhoto(new File(output.getPath()), editUserPhotosAdapter.getItemCount());
                } else {
                    Timber.e("Something went wrong during cropping image, null output");
                }
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Throwable error = UCrop.getError(data);
                Timber.e(error, "Something went wrong during cropping image");
            }
        }
    }


}
