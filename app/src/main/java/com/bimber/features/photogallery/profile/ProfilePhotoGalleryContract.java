package com.bimber.features.photogallery.profile;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 07.07.17.
 */

public interface ProfilePhotoGalleryContract {

    interface IProfilePhotoGalleryView {

        void showProfileInfo(ProfileDataFull profileData);

        void showErrorScreen();


    }

    interface IProfilePhotoGalleryPresenter extends MvpPresenter<IProfilePhotoGalleryView> {

    }
}
