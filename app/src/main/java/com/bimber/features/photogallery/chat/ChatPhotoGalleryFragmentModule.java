package com.bimber.features.photogallery.chat;

import com.bimber.base.chat.ChatModule;
import com.bimber.base.chat.ChatProvider;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryContract.IChatPhotoGalleryPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 08.07.17.
 */
@Module
public abstract class ChatPhotoGalleryFragmentModule {

    @ContributesAndroidInjector(modules = ChatPhotoGalleryModule.class)
    abstract ChatPhotoGalleryView contribute();

    @Module(includes = ChatModule.class)
    public abstract class ChatPhotoGalleryModule {

        @Binds
        abstract ChatProvider chatProvider(ChatPhotoGalleryView chatPhotoGalleryView);

        @Binds
        abstract IChatPhotoGalleryPresenter discoverPresenter(ChatPhotoGalleryPresenter chatPhotoGalleryPresenter);
    }
}
