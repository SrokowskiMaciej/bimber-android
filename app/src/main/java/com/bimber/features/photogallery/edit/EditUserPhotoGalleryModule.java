package com.bimber.features.photogallery.edit;

import com.bimber.features.photogallery.edit.EditUserPhotoGalleryContract.IEditUserPhotoGalleryPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

@Module
public abstract class EditUserPhotoGalleryModule {

    @Binds
    abstract IEditUserPhotoGalleryPresenter discoverPresenter(EditUserPhotoGalleryPresenter editUserPhotoGalleryPresenter);

    @ContributesAndroidInjector
    abstract EditUserPhotoGalleryView contribute();
}
