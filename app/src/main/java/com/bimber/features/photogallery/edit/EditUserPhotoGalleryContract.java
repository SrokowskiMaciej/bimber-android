package com.bimber.features.photogallery.edit;

import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.utils.mvp.MvpPresenter;
import com.bimber.utils.mvp.OnActivityResultConsumer;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

public interface EditUserPhotoGalleryContract {

    interface IEditUserPhotoGalleryView extends OnActivityResultConsumer {

        void setPhotos(List<UserPhoto> photos);

        void showErrorPage();

        void showErrorToast(String message);
    }

    interface IEditUserPhotoGalleryPresenter extends MvpPresenter<IEditUserPhotoGalleryView> {

        void addPhoto(File file, int currentPhotoCount);

        void reorderPhotos(Map<String, Integer> newPhotoOrder);

        void removePhoto(String photoId);
    }
}
