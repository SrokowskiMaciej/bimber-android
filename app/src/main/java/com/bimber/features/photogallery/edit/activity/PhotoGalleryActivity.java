package com.bimber.features.photogallery.edit.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by srokowski.maciej@gmail.com on 23.12.16.
 */

public class PhotoGalleryActivity extends BaseLoggedInActivity {


    public static final String PHOTO_GALLERY_VIEW_KEY = "PHOTO_GALLERY_VIEW_KEY";

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
        ButterKnife.bind(this);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (getSupportFragmentManager().findFragmentByTag(PHOTO_GALLERY_VIEW_KEY) == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.content, new EditUserPhotoGalleryView(), PHOTO_GALLERY_VIEW_KEY)
                    .commit();
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        getSupportActionBar().setTitle("Photos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
