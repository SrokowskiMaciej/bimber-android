package com.bimber.features.photogallery.edit

import com.bimber.base.auth.LoggedInUserId
import com.bimber.data.entities.isDefault
import com.bimber.data.entities.profile.local.UserPhoto
import com.bimber.data.sources.profile.photos.UsersPhotosRepository
import com.bimber.domain.photos.RemoveProfilePhotoUseCase
import com.bimber.domain.photos.UploadProfilePhotosUseCase
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryContract.IEditUserPhotoGalleryPresenter
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryContract.IEditUserPhotoGalleryView
import com.bimber.utils.mvp.MvpAbstractPresenter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

class EditUserPhotoGalleryPresenter @Inject
constructor(@param:LoggedInUserId private val currentUserId: String, private val usersPhotosRepository: UsersPhotosRepository,
            private val uploadProfilePhotosUseCase: UploadProfilePhotosUseCase,
            private val removeProfilePhotoUseCase: RemoveProfilePhotoUseCase) : MvpAbstractPresenter<IEditUserPhotoGalleryView>(), IEditUserPhotoGalleryPresenter {

    private val subscriptions = CompositeDisposable()

    override fun viewAttached(view: IEditUserPhotoGalleryView) {


        subscriptions.add(Flowable.combineLatest(uploadProfilePhotosUseCase.onUploadingPhotos(),
                usersPhotosRepository.onAllUserPhotos(currentUserId),
                BiFunction<List<UserPhoto>, List<UserPhoto>, List<UserPhoto>> { uploadingPhotos, uploadedPhotos ->
                    uploadingPhotos + uploadedPhotos
                })
                .map { photos -> photos.filter { !it.isDefault() } }
                .map { photos -> photos.distinctBy { it.photoId } }
//                .sample(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { view.setPhotos(it) },
                        onError = { throwable ->
                            Timber.e(throwable, "Error loading user photos");
                            view.showErrorPage()
                        }))
    }

    override fun viewDetached(view: IEditUserPhotoGalleryView) {
        subscriptions.clear()
    }

    override fun addPhoto(file: File, currentPhotoCount: Int) {
        uploadProfilePhotosUseCase.insertUserPhoto(currentUserId, file)
    }

    override fun removePhoto(photoId: String) {
        subscriptions.add(removeProfilePhotoUseCase.removeUserPhoto(currentUserId, photoId)
                .subscribeBy(onError = { throwable ->
                    Timber.e(throwable, "Error removing user photo");
                }))
    }

    override fun reorderPhotos(newPhotoOrder: Map<String, Int>) {
        subscriptions.add(usersPhotosRepository.reorderPhotos(currentUserId, newPhotoOrder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Photos with successfully rearranged") }
                ) { throwable ->
                    Timber.e(throwable, "Failed to update photos")
                    view.showErrorToast("Failed to update photos :(")
                })
    }
}
