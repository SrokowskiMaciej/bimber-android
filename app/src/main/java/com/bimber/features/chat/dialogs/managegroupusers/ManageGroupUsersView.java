package com.bimber.features.chat.dialogs.managegroupusers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.glide.GlideApp;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersContract.IManageGroupUsersPresenter;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersContract.IManageGroupUsersView;
import com.bimber.features.chat.dialogs.managegroupusers.adapters.FriendsAdapter;
import com.bimber.features.chat.dialogs.managegroupusers.adapters.ParticipantsAdapter;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class ManageGroupUsersView extends BaseGroupFragment implements IManageGroupUsersView {


    @Inject
    IManageGroupUsersPresenter manageGroupUsersPresenter;

    @BindView(R.id.participantsRecyclerView)
    RecyclerView participantsRecyclerView;
    @BindView(R.id.immediateFriendsRecyclerView)
    RecyclerView immediateFriendsRecyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textViewNoImmediateFriends)
    TextView textViewNoImmediateFriends;
    @BindView(R.id.progressBarChatMembers)
    ProgressBar progressBarChatMembers;
    @BindView(R.id.progressBarImmediateFriends)
    ProgressBar progressBarImmediateFriends;
    @BindView(R.id.imageViewAdministrated)
    ImageView imageViewAdministrated;
    @BindView(R.id.textViewAdministratedTitle)
    TextView textViewAdministratedTitle;
    @BindView(R.id.textViewAdministratedSubtitle)
    TextView textViewAdministratedSubtitle;

    private ParticipantsAdapter participantsAdapter;
    private FriendsAdapter immediateFriendsAdapter;

    Unbinder unbinder;

    private MaterialDialog progressBarDialog;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.view_chat_dialog_manage_users, container, false);

        unbinder = ButterKnife.bind(this, view);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.setTitle(R.string.view_chat_menu_dialog_group_manage_users_title_text);
        toolbar.setNavigationOnClickListener(toolbar -> activity.onBackPressed());
        toolbar.setTitleTextColor(ContextCompat.getColor(getContext(), R.color.colorForeground));
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);

        participantsAdapter = new ParticipantsAdapter(GlideApp.with(this));
        participantsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager
                .HORIZONTAL, false));
        participantsRecyclerView.setAdapter(participantsAdapter);
        subscriptions.add(participantsAdapter.onParticipantRemoved().subscribe(chatMemberProfileData ->
                manageGroupUsersPresenter
                        .removeParticipant(groupId, chatMemberProfileData.chatMember().profileData().user.uId)));
        subscriptions.add(participantsAdapter.onChatMemberClicked().subscribe(clickedAction -> {
            String uri = clickedAction.chatMember().chatMember().profileData().photo.userPhotoUri;
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    clickedAction.avatar(), uri);
            startActivity(ProfileActivity.newInstance(getContext(), clickedAction.chatMember().chatMember()
                            .profileData().user.uId,
                    uri, true), options.toBundle());
        }));



        immediateFriendsAdapter = new FriendsAdapter(GlideApp.with(this));
        immediateFriendsRecyclerView.setLayoutManager(new FlexboxLayoutManager(getContext(), FlexDirection.COLUMN, FlexWrap.WRAP));
        immediateFriendsRecyclerView.setAdapter(immediateFriendsAdapter);
        subscriptions.add(immediateFriendsAdapter.onFriendAdded().subscribe(friend -> manageGroupUsersPresenter
                .addUser(groupId, friend.chatMember().profileData().user.uId)));
        subscriptions.add(immediateFriendsAdapter.onChatMemberClicked().subscribe(clickedAction -> {
            String uri = clickedAction.chatMember().chatMember().profileData().photo.userPhotoUri;
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    clickedAction.avatar(), uri);
            startActivity(ProfileActivity.newInstance(getContext(), clickedAction.chatMember().chatMember().profileData().user.uId,
                    uri, true), options.toBundle());
        }));
        manageGroupUsersPresenter.bindView(this);
        manageGroupUsersPresenter.requestMembersData(groupId);
        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        manageGroupUsersPresenter.unbindView(this);
        if (progressBarDialog != null) {
            progressBarDialog.dismiss();
        }
        super.onDestroyView();
    }

    @Override
    public void showChatMembers(List<ManagedChatMember> chatMembers) {
        participantsAdapter.setChatMembers(chatMembers);
        participantsRecyclerView.setVisibility(View.VISIBLE);
        progressBarChatMembers.setVisibility(View.GONE);
    }

    @Override
    public void showFriends(List<ManagedFriend> friends) {
        immediateFriendsAdapter.setFriends(friends);
        if (friends.isEmpty()) {
            textViewNoImmediateFriends.setVisibility(View.VISIBLE);
            immediateFriendsRecyclerView.setVisibility(View.INVISIBLE);
        } else {
            textViewNoImmediateFriends.setVisibility(View.INVISIBLE);
            immediateFriendsRecyclerView.setVisibility(View.VISIBLE);
        }
        progressBarImmediateFriends.setVisibility(View.GONE);
    }

    @Override
    public void showIfGroupIsSecure(boolean secure) {
        if (secure) {
            textViewAdministratedTitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_secure_title);
            textViewAdministratedSubtitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_secure_subtitle);
            imageViewAdministrated.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPositiveAction));
        } else {
            textViewAdministratedTitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_unsecure_title);
            textViewAdministratedSubtitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_unsecure_subtitle);
            imageViewAdministrated.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorNegativeAction));
        }
    }

    @Override
    public void showProgressBar() {
        progressBarDialog = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                .cancelable(false)
                .show();

    }

    @Override
    public void hideProgressBar() {
        if (progressBarDialog != null) {
            progressBarDialog.dismiss();
            progressBarDialog = null;
        }
    }

    @Override
    public void showManageUsersDataLoadFailure() {
        Toast.makeText(getContext(), R.string.view_chat_menu_dialog_group_manage_users_error, Toast.LENGTH_SHORT).show();
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void showManageUsersUserStatusChangeFailure() {
        if (progressBarDialog != null) {
            progressBarDialog.dismiss();
            progressBarDialog = null;
        }
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }
}
