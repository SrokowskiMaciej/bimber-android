package com.bimber.features.chat.dialogs.managegroupusers.adapters;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.features.chat.dialogs.managegroupusers.ChatMemberWithRole;
import com.bimber.features.chat.dialogs.managegroupusers.ManagedChatMember;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.google.auto.value.AutoValue;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 16.03.17.
 */

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantViewHolder> {

    private List<ManagedChatMember> chatMembers = Collections.emptyList();
    private PublishSubject<ManagedChatMember> removedChatMembersPublishSubject = PublishSubject.create();
    private PublishSubject<ManagedChatMemberClickedAction> chatMemberClickedPublishSubject = PublishSubject.create();
    private final GlideRequests glide;

    public ParticipantsAdapter(GlideRequests glide) {
        this.glide = glide;
    }

    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ParticipantViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .view_user_thumbnail_remove,
                parent, false));
    }

    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        ManagedChatMember managedChatMember = chatMembers.get(position);
        ChatMemberData chatMember = managedChatMember.chatMember();
        glide.load(chatMember.profileData().photo.userPhotoUri)
                .into(holder.imageViewAvatar);
        holder.imageViewAvatar.setOnClickListener(v -> {
            chatMemberClickedPublishSubject.onNext(ManagedChatMemberClickedAction.create(holder.imageViewAvatar,
                    managedChatMember));
        });
        if (managedChatMember.chatRole() == ManagedChatMember.ChatRole.CURRENT_USER) {
            holder.textViewParticipantName.setText(R.string.view_chat_menu_dialog_group_manage_users_current_user);
        } else {
            holder.textViewParticipantName.setText(chatMember.profileData().user.displayName);
        }
        if (managedChatMember.availableToChange()) {
            holder.buttonRemove.setVisibility(View.VISIBLE);
        } else {
            holder.buttonRemove.setVisibility(View.GONE);
        }

        holder.buttonRemove.setOnClickListener(v -> {
            removedChatMembersPublishSubject.onNext(managedChatMember);
        });

    }

    @Override
    public int getItemCount() {
        return chatMembers.size();
    }

    public void setChatMembers(List<ManagedChatMember> friends) {

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<ManagedChatMember>
                (chatMembers,
                        friends) {
            @Override
            public boolean areItemsTheSame(ManagedChatMember oldItem, ManagedChatMember newItem) {
                return oldItem.chatMember().profileData().user.uId.equals(newItem.chatMember().profileData().user.uId);
            }

            @Override
            public boolean areContentsTheSame(ManagedChatMember oldItem, ManagedChatMember newItem) {
                return oldItem.equals(newItem);
            }
        });
        chatMembers = friends;
        diffResult.dispatchUpdatesTo(this);
    }

    public Observable<ManagedChatMember> onParticipantRemoved() {
        return removedChatMembersPublishSubject;
    }

    public Observable<ManagedChatMemberClickedAction> onChatMemberClicked() {
        return chatMemberClickedPublishSubject;
    }

    @AutoValue
    public static abstract class ManagedChatMemberClickedAction {
        public abstract ImageView avatar();

        public abstract ManagedChatMember chatMember();

        public static ManagedChatMemberClickedAction create(ImageView avatar, ManagedChatMember chatMember) {
            return new AutoValue_ParticipantsAdapter_ManagedChatMemberClickedAction(avatar, chatMember);
        }
    }
}
