package com.bimber.features.chat.adapter.viewholders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.common.group.GroupImageSetContent;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by maciek on 26.04.17.
 */

public class GroupImageSetViewHolder extends BaseMessageViewHolder {

    @BindView(R.id.textViewContent)
    TextView textViewContent;
    @BindView(R.id.imageViewGroupPhoto)
    CircleImageView imageViewGroupPhoto;

    public GroupImageSetViewHolder(View itemView, InteractionListener interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }

    public static GroupImageSetViewHolder create(ViewGroup parent, InteractionListener interactionListener,
                                                 GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_message_group_image_set_item,
                parent, false);
        return new GroupImageSetViewHolder(view, interactionListener, glide);

    }


    @Override
    public void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage aggregatedMessage,
                            boolean
            selected, DefaultValues defaultValues) {
        textViewContent.setText(MessageToTextConverter.stringifyMessage(textViewContent.getContext(), currentUserId,
                chatType,
                aggregatedMessage.sendingUserProfile().user.displayName, aggregatedMessage.message()));

        GroupImageSetContent imageSetContent = (GroupImageSetContent) aggregatedMessage.message()
                .getContent();
        glide.load(imageSetContent.newImageUrl())
                .into(imageViewGroupPhoto);

    }

    @Override
    public void recycle() {
        imageViewGroupPhoto.setImageBitmap(null);
        super.recycle();
    }
}
