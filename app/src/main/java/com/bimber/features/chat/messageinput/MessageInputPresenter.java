package com.bimber.features.chat.messageinput;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.base.chat.ChatType;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.analytics.BimberAnalytics.MessageType;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.domain.messages.SendImageMessageUseCase;
import com.bimber.domain.messages.SendTextMessageUseCase;
import com.bimber.features.chat.messageinput.MessageInputContract.IMessageInputPresenter;
import com.bimber.features.chat.messageinput.MessageInputContract.IMessageInputView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 02.03.17.
 */

public class MessageInputPresenter extends MvpAbstractPresenter<IMessageInputView> implements IMessageInputPresenter {

    private final String currentUserId;
    private final String currentChatId;
    private final Chattable.ChatType currentChatType;
    private final ChatMemberRepository chatMemberRepository;
    private final SendTextMessageUseCase sendTextMessageUseCase;
    private final SendImageMessageUseCase sendImageMessageUseCase;
    private final BimberAnalytics bimberAnalytics;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public MessageInputPresenter(@LoggedInUserId String currentUserId, @ChatId String currentChatId, @ChatType Chattable.ChatType currentChatType, ChatMemberRepository
            chatMemberRepository, SendTextMessageUseCase sendTextMessageUseCase, SendImageMessageUseCase sendImageMessageUseCase, BimberAnalytics bimberAnalytics) {
        this.currentUserId = currentUserId;
        this.currentChatId = currentChatId;
        this.currentChatType = currentChatType;
        this.chatMemberRepository = chatMemberRepository;
        this.sendTextMessageUseCase = sendTextMessageUseCase;
        this.sendImageMessageUseCase = sendImageMessageUseCase;
        this.bimberAnalytics = bimberAnalytics;
    }


    @Override
    protected void viewAttached(IMessageInputView view) {

    }

    @Override
    protected void viewDetached(IMessageInputView view) {
        subscriptions.clear();
    }

    @Override
    public void sendTextMessage(String text) {
        bimberAnalytics.logSendMessage(MessageType.TEXT, currentChatType);
        subscriptions.add(chatMemberRepository.chatMembership(currentChatId, currentUserId)
                .flatMap(chatMembership -> {
                    if (chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.EXPIRED) {
                        getView().showSendMessageErrorExpiredMembershipToast();
                        return Maybe.empty();
                    } else {
                        return Maybe.just(chatMembership);
                    }

                })
                .flatMapSingle(chatMembership -> sendTextMessageUseCase.sendTextMessage(chatMembership, currentUserId, text))
                .subscribe(messageKey -> Timber.d("Sent content: %s", messageKey),
                        throwable -> {
                            Timber.e(throwable, "Failed to sent text message content: %s", text);
                            getView().showGenericSendMessageErrorToast();
                        }));
    }

    @Override
    public void sendImageMessage(File image, float dimensionRatio) {
        bimberAnalytics.logSendMessage(MessageType.PICTURE, currentChatType);
        subscriptions.add(chatMemberRepository.chatMembership(currentChatId, currentUserId)
                .flatMap(chatMembership -> {
                    if (chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.EXPIRED) {
                        getView().showSendMessageErrorExpiredMembershipToast();
                        return Maybe.empty();
                    } else {
                        return Maybe.just(chatMembership);
                    }

                })
                .flatMapPublisher(chatMembership -> sendImageMessageUseCase.sendImageMessage(chatMembership, currentUserId, image,
                        dimensionRatio))
                .subscribe(messageKey -> Timber.d("Sent content: %s", messageKey),
                        throwable -> {
                            Timber.e(throwable, "Failed to sent image message content");
                            getView().showGenericSendMessageErrorToast();
                        }));
    }
}
