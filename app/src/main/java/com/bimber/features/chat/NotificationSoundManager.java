package com.bimber.features.chat;

import android.annotation.TargetApi;
import android.app.Application;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import com.bimber.R;
import com.bimber.base.application.BimberApplication;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by maciek on 20.10.17.
 */
@Singleton
public class NotificationSoundManager {

    private final Application application;

    private SoundPool soundPool;
    private int soundId;

    @Inject
    public NotificationSoundManager(BimberApplication application) {
        this.application = application;
    }


    //It may be played only from main thread and does need to be thread safe
    public void play() {
        if (soundPool == null) {
            soundPool = createSoundPool();
            soundId = soundPool.load(application, R.raw.chat_message, 1);
        }
        soundPool.play(soundId, 0.1f, 0.1f, 1, 0, 1);

    }


    public static SoundPool createSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return createNewSoundPool();
        } else {
            return createOldSoundPool();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static SoundPool createNewSoundPool() {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_COMMUNICATION_INSTANT)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        return new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
    }

    @SuppressWarnings("deprecation")
    private static SoundPool createOldSoundPool() {
        return new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
    }
}
