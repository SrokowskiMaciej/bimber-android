package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.group.GroupNameSetContent;

/**
 * Created by maciek on 28.04.17.
 */

public class GroupNameSetMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();

        GroupNameSetContent content = (GroupNameSetContent) message.getContent();

        if (currentUserId.equals(actingUserId)) {
            return String.format(context.getString(R.string.view_chat_message_group_name_set_first_person_text), content.newGroupName());
        } else {
            return String.format(context.getString(R.string.view_chat_message_group_name_set_third_person_text), userName, content.newGroupName());
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
