package com.bimber.features.chat.dialogs.changegroupname;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameContract.IChangeGroupNameView;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;

import icepick.State;

import static com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameContract.IChangeGroupNamePresenter;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class ChangeGroupNameView extends BaseGroupFragment implements IChangeGroupNameView {

    @Inject
    IChangeGroupNamePresenter changeGroupNamePresenter;
    @State
    String groupName;

    private MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        changeGroupNamePresenter.bindView(this);
        changeGroupNamePresenter.requestGroupName(groupId);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        changeGroupNamePresenter.unbindView(this);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        super.onDestroyView();
    }

    @Override
    public void showGroupName(final String groupName) {
        if (this.groupName == null) {
            this.groupName = groupName;
        }
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getContext())
                .alwaysCallInputCallback()
                .autoDismiss(false)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                .title(R.string.view_chat_menu_dialog_group_name_title)
                .negativeText(R.string.dialog_cancel_button_text)
                .onNegative((dialog1, which) -> close())
                .positiveText(R.string.dialog_ok_button_text)
                .onPositive((dialog, which) -> changeGroupNamePresenter.changeGroupName(groupId, this.groupName))
                .input(getString(R.string.view_chat_menu_dialog_group_name_input_hint), this.groupName, false,
                        (dialog1, input) -> this.groupName = input.toString());
        dialog = dialogBuilder.build();
        dialog.getInputEditText().setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                changeGroupNamePresenter.changeGroupName(groupId, this.groupName);
            }
            return false;
        });
        dialog.show();
    }

    @Override
    public void showChangeGroupNameFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog1 -> close())
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }
}
