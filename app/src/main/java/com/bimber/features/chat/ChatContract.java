package com.bimber.features.chat;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.domain.model.GroupData;
import com.bimber.utils.mvp.MvpPresenter;
import com.google.auto.value.AutoValue;

import io.reactivex.Completable;

/**
 * Created by maciek on 02.03.17.
 */

public interface ChatContract {
    interface IChatView {
        void showDialogInterlocutorInfo(DialogState dialogState);

        void showGroupInfo(GroupState groupState);
    }

    interface IChatPresenter extends MvpPresenter<IChatView> {
        public Completable setPartySecure(boolean secure);
    }

    @AutoValue
    abstract class GroupState {
        public abstract GroupData publicGroupDataThumbnail();

        public abstract boolean showLeaveGroupButton();

        public abstract boolean showDiscoveryInfo();

        public abstract boolean showInfoButton();

        public abstract boolean showJoinRequestsButton();

        public abstract boolean showSecurityButton();

        public boolean showNotificationsButton() {
            return showLeaveGroupButton();
        }

        public static GroupState create(String currentUserId, GroupData publicGroupData, boolean isCurrentUserActive, boolean
                showJoinRequestsButton) {
            return new AutoValue_ChatContract_GroupState(publicGroupData, isCurrentUserActive,
                    isCurrentUserActive, isCurrentUserActive, showJoinRequestsButton && isCurrentUserActive,
                    currentUserId.equals(publicGroupData.group().value().groupOwnerId()));
        }


    }

    @AutoValue
    abstract class DialogState {
        public abstract ProfileDataThumbnail interlocutorData();

        public abstract boolean showCreateGroupButton();

        public abstract boolean showUnmatchButton();

        public abstract boolean showNotificationsButton();

        public abstract boolean showInfoButton();

        public static DialogState create(ProfileDataThumbnail interlocutorData, boolean isCurrentUserActive, boolean
                isInterlocutorAccountExisting) {
            boolean bothUsersExistAndAreActive = isCurrentUserActive && isInterlocutorAccountExisting;
            return new AutoValue_ChatContract_DialogState(interlocutorData, bothUsersExistAndAreActive,
                    bothUsersExistAndAreActive, bothUsersExistAndAreActive, isInterlocutorAccountExisting);
        }
    }
}
