package com.bimber.features.chat.dialogs.changegrouplocation;

import com.bimber.data.entities.Place;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface ChangeGroupLocationContract {
    interface IChangeGroupLocationView {

        void showChangeGroupLocationFailure();

        void close();
    }

    interface IChangeGroupLocationPresenter extends MvpPresenter<IChangeGroupLocationView> {
        void changeGroupLocation(String groupId, Place newGroupLocation);
    }
}
