package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.group.GroupTimeSetContent;

import java.util.Date;

/**
 * Created by maciek on 28.04.17.
 */

public class GroupTimeSetMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();

        GroupTimeSetContent content = (GroupTimeSetContent) message.getContent();

        Date date = new Date(content.timestamp());
        String dateText = android.text.format.DateFormat.getLongDateFormat(context).format(date) + " " + android.text.format
                .DateFormat.getTimeFormat(context).format(date);
        if (currentUserId.equals(actingUserId)) {
            return String.format(context.getString(R.string.view_chat_message_group_time_set_first_person_title), dateText);
        } else {
            return String.format(context.getString(R.string.view_chat_message_group_time_set_third_person_title), userName, dateText);
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
