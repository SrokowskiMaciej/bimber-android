package com.bimber.features.chat;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.kevelbreh.androidunits.AndroidUnit;

/**
 * Created by maciek on 19.10.17.
 */

public class PartyStatusesAdapter extends RecyclerView.Adapter<PartyStatusesAdapter.SimpleViewHolder> {

    private final static int PADDING = (int) AndroidUnit.DENSITY_PIXELS.toPixels(8);

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimpleViewHolder(new TextView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        PartyStatus partyStatus = PartyStatus.values()[position];
        holder.item.setText(partyStatus.getStateExplanation(holder.item.getContext()));
        holder.item.setCompoundDrawablesWithIntrinsicBounds(partyStatus.getIcon(holder.item.getContext()), null, null, null);
    }

    @Override
    public int getItemCount() {
        return PartyStatus.values().length;
    }

    static class SimpleViewHolder extends RecyclerView.ViewHolder {

        final TextView item;

        SimpleViewHolder(TextView itemView) {
            super(itemView);
            item = itemView;
            item.setCompoundDrawablePadding(PADDING);
            item.setPadding(PADDING, PADDING, PADDING, PADDING);
        }
    }
}
