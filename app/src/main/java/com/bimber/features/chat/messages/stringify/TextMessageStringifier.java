package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.text.TextContent;

/**
 * Created by maciek on 28.04.17.
 */

public class TextMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        TextContent content = (TextContent) message.getContent();
        return content.text();
    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
