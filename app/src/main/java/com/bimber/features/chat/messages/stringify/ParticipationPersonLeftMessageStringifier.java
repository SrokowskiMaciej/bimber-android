package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonLeftContent;

/**
 * Created by maciek on 28.04.17.
 */

public class ParticipationPersonLeftMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();
        ParticipationPersonLeftContent content = (ParticipationPersonLeftContent) message.getContent();
        switch (chatType) {
            case DIALOG:
                if (currentUserId.equals(actingUserId)) {
                    return context.getString(R.string.view_chat_message_participation_person_left_dialog_first_person_text);
                } else {
                    return String.format(context.getString(R.string
                            .view_chat_message_participation_person_left_dialog_third_person_text), content.personLeftName());
                }
            case GROUP:
                if (currentUserId.equals(actingUserId)) {
                    return context.getString(R.string.view_chat_message_participation_person_left_group_first_person_text);
                } else {
                    return String.format(context.getString(R.string.view_chat_message_participation_person_left_group_third_person_text),
                            content.personLeftName());
                }
            default:
                throw new IllegalStateException("Oops");
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return !(((ParticipationPersonLeftContent) message.getContent()).personLeftId().equals(currentUserId));
    }
}
