package com.bimber.features.chat.dialogs.leavegroup;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface LeaveGroupContract {
    interface ILeaveGroupView {
        void showProgressBar();

        void showLeaveGroupFailure();

        void close();
    }

    interface ILeaveGroupPresenter extends MvpPresenter<ILeaveGroupView> {
        void leaveGroup(String groupId);
    }
}
