package com.bimber.features.chat.iteminteraction;

import android.support.annotation.IdRes;

/**
 * Created by maciek on 25.04.17.
 */

public interface ActionModePlaceHolderProvider {

    @IdRes
    int provideActionModePlaceholder();
}
