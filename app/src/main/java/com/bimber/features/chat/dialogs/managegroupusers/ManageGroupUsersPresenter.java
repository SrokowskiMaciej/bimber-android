package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.domain.chats.GetChatMembersDataUseCase;
import com.bimber.domain.friends.GetFriendsDataUseCase;
import com.bimber.domain.groupjoin.EvaluateGroupCandidateUseCase;
import com.bimber.domain.groupjoin.RemoveUserFromGroupUseCase;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersContract.IManageGroupUsersPresenter;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersContract.IManageGroupUsersView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 05.05.17.
 */

public class ManageGroupUsersPresenter extends MvpAbstractPresenter<IManageGroupUsersView> implements IManageGroupUsersPresenter {

    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final GetChatMembersDataUseCase getChatMembersDataUseCase;
    private final GetFriendsDataUseCase getFriendsDataUseCase;
    private final EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase;
    private final RemoveUserFromGroupUseCase removeUserFromGroupUseCase;
    private final GroupRepository groupRepository;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ManageGroupUsersPresenter(@LoggedInUserId String currentUserId, DbConnectionService dbConnectionService,
                                     GetChatMembersDataUseCase getChatMembersDataUseCase, GetFriendsDataUseCase getFriendsDataUseCase,
                                     EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase, RemoveUserFromGroupUseCase
                                             removeUserFromGroupUseCase, GroupRepository groupRepository) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.getChatMembersDataUseCase = getChatMembersDataUseCase;
        this.getFriendsDataUseCase = getFriendsDataUseCase;
        this.evaluateGroupCandidateUseCase = evaluateGroupCandidateUseCase;
        this.removeUserFromGroupUseCase = removeUserFromGroupUseCase;
        this.groupRepository = groupRepository;
    }

    @Override
    protected void viewAttached(IManageGroupUsersView view) {

    }

    @Override
    protected void viewDetached(IManageGroupUsersView view) {
        subscriptions.clear();
    }

    @Override
    public void requestMembersData(String groupId) {
        //TODO FFS this flow is too complicated for presenter. Add some logging in here
        Flowable<List<ChatMemberData>> chatMembers = getChatMembersDataUseCase.onChatMembersThumbnails(groupId,
                ChatVisibleChatMembership.MembershipStatus.ACTIVE)
                .replay(1)
                .refCount();

        Flowable<String> groupOwnerIdSource = groupRepository.groupOwnerId(groupId)
                .toFlowable()
                .replay(1)
                .refCount();
        Flowable<Boolean> isGroupSecureSource = groupRepository.isGroupSecure(groupId)
                .replay(1)
                .refCount();

        Flowable<List<ManagedChatMember>> chatParticipantsWithRoleAssignedObservable = Flowable.combineLatest(chatMembers,
                groupOwnerIdSource, isGroupSecureSource,
                (chatMemberProfiles, groupOwnerId, isGroupSecure) -> Flowable.fromIterable(chatMemberProfiles)
                        .map(chatMemberProfileData -> ManagedChatMember.create(chatMemberProfileData, currentUserId, groupOwnerId, isGroupSecure))
                        .toList()
                        .toFlowable())
                .flatMap(flowable -> flowable);


        subscriptions.add(chatParticipantsWithRoleAssignedObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getView()::showChatMembers, throwable -> getView().showManageUsersDataLoadFailure()));

        Flowable<List<String>> currentChatMembersIdsObservable = chatMembers
                .flatMapSingle(chatMemberProfileDatas -> Flowable.fromIterable(chatMemberProfileDatas)
                        .map(chatMemberProfileData -> chatMemberProfileData.profileData().user.uId)
                        .toList());

        subscriptions.add(Flowable.combineLatest(getFriendsDataUseCase.onUserFriends(currentUserId), currentChatMembersIdsObservable,
                groupOwnerIdSource, isGroupSecureSource,
                (friends, currentChatMembersIds, groupOwnerId, isGroupSecure) -> Flowable.fromIterable(friends)
                        .filter(friend -> !currentChatMembersIds.contains(friend.profileData().user.uId))
                        .map(chatMemberData -> ManagedFriend.create(chatMemberData, currentUserId, groupOwnerId, isGroupSecure))
                        .toList())
                .flatMapSingle(friendsSingle -> friendsSingle)
                .sample(600, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(friends -> getView().showFriends(friends), throwable -> getView()
                        .showManageUsersDataLoadFailure()));

        subscriptions.add(isGroupSecureSource
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isGroupSecure -> getView().showIfGroupIsSecure(isGroupSecure), throwable ->
                        Timber.e(throwable, "Failed to get group security data")));
    }

    @Override
    public void addUser(String groupId, String participantId) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(evaluateGroupCandidateUseCase.acceptGroupJoinRequest(currentUserId, participantId, groupId))
                .doOnSubscribe(subscribtion -> getView().showProgressBar())
                .timeout(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().hideProgressBar(),
                        throwable -> {
                            Timber.e(throwable, "Couldn't set user to group");
                            getView().showManageUsersUserStatusChangeFailure();
                        }));

    }

    @Override
    public void removeParticipant(String groupId, String participantId) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(removeUserFromGroupUseCase.removeUserFromGroup(currentUserId, participantId, groupId))
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .timeout(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().hideProgressBar(),
                        throwable -> {
                            Timber.e(throwable, "Couldn't remove user from group");
                            getView().showManageUsersUserStatusChangeFailure();
                        }));
    }


}
