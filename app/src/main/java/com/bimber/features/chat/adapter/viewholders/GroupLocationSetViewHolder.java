package com.bimber.features.chat.adapter.viewholders;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.common.group.GroupLocationSetContent;
import com.bimber.data.entities.Place;
import com.bimber.data.entities.chat.messages.common.group.NewGroupLocation;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;
import com.bimber.utils.view.ViewUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 26.04.17.
 */

public class GroupLocationSetViewHolder extends BaseMessageViewHolder {

    @BindView(R.id.textViewContent)
    TextView textViewContent;
    @BindView(R.id.mapView)
    MapView mapView;

    public GroupLocationSetViewHolder(View itemView, InteractionListener
            interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }

    public static GroupLocationSetViewHolder create(ViewGroup parent, InteractionListener interactionListener,
                                                    GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .view_chat_message_group_location_set_item, parent, false);
        return new GroupLocationSetViewHolder(view, interactionListener, glide);

    }


    @Override
    public void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage aggregatedMessage,
                            boolean
                                    selected, DefaultValues defaultValues) {

        textViewContent.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(textViewContent
                .getContext(), R.drawable
                .ic_location_on_white_24dp), null, null);
        ViewUtils.applyFixForDrawableTopTint(textViewContent, R.color.colorDivider);

        textViewContent.setText(MessageToTextConverter.stringifyMessage(textViewContent.getContext(), currentUserId,
                chatType,
                aggregatedMessage.sendingUserProfile().user.displayName, aggregatedMessage.message()));

        if (mapView != null) {
            GroupLocationSetContent content = (GroupLocationSetContent) aggregatedMessage.message().getContent();
            NewGroupLocation meetingPlace = content.meetingPlace();
            MapsInitializer.initialize(mapView.getContext().getApplicationContext());
            mapView.setClickable(true);
            mapView.onCreate(null);
            mapView.onResume();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mapView.setClipToOutline(true);
            }
            mapView.getMapAsync(googleMap -> {
                googleMap.clear();
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                googleMap.setOnMapClickListener(latLng -> {
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, meetingPlace.getGoogleMapsSearchUri());
                    textViewContent.getContext().startActivity(mapIntent);
                });
                LatLng latLng = new LatLng(meetingPlace.latitude(), meetingPlace.longitude());
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(15)
                        .build()));
                googleMap.addMarker(new MarkerOptions().position(latLng));
            });
        }
    }

    @Override
    public void recycle() {
        super.recycle();
        if (mapView != null) {
            mapView.getMapAsync(googleMap -> {
                googleMap.clear();
                googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
            });
        }
    }
}
