package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonDeletedAccountContent;

/**
 * Created by maciek on 28.04.17.
 */

public class ParticipationPersonDeletedAccountMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();
        ParticipationPersonDeletedAccountContent content = (ParticipationPersonDeletedAccountContent) message.getContent();


        if (currentUserId.equals(actingUserId)) {
            return context.getString(R.string.view_chat_message_participation_person_deleted_account_dialog_first_person_text);
        } else {
            return String.format(context.getString(R.string
                    .view_chat_message_participation_person_deleted_account_dialog_third_person_text), content.personDeletedAccountName());
        }
    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return false;
    }
}
