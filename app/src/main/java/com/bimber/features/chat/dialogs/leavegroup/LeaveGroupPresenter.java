package com.bimber.features.chat.dialogs.leavegroup;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.domain.groupjoin.LeaveGroupUseCase;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 05.05.17.
 */

public class LeaveGroupPresenter extends MvpAbstractPresenter<LeaveGroupContract.ILeaveGroupView> implements LeaveGroupContract
        .ILeaveGroupPresenter {


    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final LeaveGroupUseCase leaveGroupUseCase;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public LeaveGroupPresenter(@LoggedInUserId String currentUserId, DbConnectionService dbConnectionService, LeaveGroupUseCase
            leaveGroupUseCase) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.leaveGroupUseCase = leaveGroupUseCase;
    }


    @Override
    public void leaveGroup(String groupId) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(leaveGroupUseCase.leaveGroup(currentUserId, groupId))
                .timeout(10, TimeUnit.SECONDS)
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .doOnError(error -> Timber.d(error, "Unable to leave group: %s", groupId))
                .subscribe(() -> getView().close(), throwable -> getView().showLeaveGroupFailure()));
    }

    @Override
    protected void viewAttached(LeaveGroupContract.ILeaveGroupView view) {

    }

    @Override
    protected void viewDetached(LeaveGroupContract.ILeaveGroupView view) {
        subscriptions.clear();

    }
}
