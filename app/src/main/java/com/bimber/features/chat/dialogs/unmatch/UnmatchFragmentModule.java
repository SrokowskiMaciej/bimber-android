package com.bimber.features.chat.dialogs.unmatch;

import com.bimber.features.chat.dialogs.unmatch.UnmatchContract.IUnmatchPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class UnmatchFragmentModule {

    @ContributesAndroidInjector
    abstract UnmatchView contributeView();

    @Binds
    abstract IUnmatchPresenter unmatchPresenter(UnmatchPresenter unmatchPresenter);
}
