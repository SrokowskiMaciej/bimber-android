package com.bimber.features.chat.messageinput;

import com.bimber.utils.mvp.MvpPresenter;

import java.io.File;

/**
 * Created by maciek on 25.04.17.
 */

public class MessageInputContract {
    public interface IMessageInputView {
        void showSendMessageErrorExpiredMembershipToast();

        void showGenericSendMessageErrorToast();
    }

    public interface IMessageInputPresenter extends MvpPresenter<IMessageInputView> {
        void sendTextMessage(String text);

        void sendImageMessage(File image, float dimensionRatio);
    }
}
