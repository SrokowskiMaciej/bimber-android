package com.bimber.features.chat.iteminteraction;

/**
 * Created by maciek on 09.03.17.
 */

public enum SelectionActions {
    REMOVE,
    // I don't think letting user edit content sent to another user is good UX, better delete content and sent new one so that the other
    // user knows about it
    // EDIT,
    COPY,
    VIEW_DETAILS,
    FORWARD,
    SHARE
}
