package com.bimber.features.chat.dialogs.changegroupname;

import com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameContract.IChangeGroupNamePresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class ChangeGroupNameFragmentModule {

    @ContributesAndroidInjector
    abstract ChangeGroupNameView contributeView();

    @Binds
    abstract IChangeGroupNamePresenter groupNamePresenter(ChangeGroupNamePresenter changeGroupNamePresenter);
}
