package com.bimber.features.chat.dialogs.setgrouppicture;

import com.bimber.utils.mvp.MvpPresenter;

import java.io.File;

/**
 * Created by maciek on 02.03.17.
 */

public interface SetGroupPictureContract {
    interface ISetGroupPictureView {

        void showProgressBar(boolean show);

        void showSetGroupPictureFailure(boolean show);

        void close();
    }

    interface ISetGroupPicturePresenter extends MvpPresenter<ISetGroupPictureView> {
        void setPicture(String groupId, File file);
    }
}
