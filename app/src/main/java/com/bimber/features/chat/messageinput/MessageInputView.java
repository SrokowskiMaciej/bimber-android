/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.bimber.features.chat.messageinput;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bimber.R;
import com.bimber.base.activities.BackPressedListener;
import com.bimber.base.activities.BackPressedManager;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.features.chat.messageinput.MessageInputContract.IMessageInputPresenter;
import com.bimber.features.chat.messageinput.MessageInputContract.IMessageInputView;
import com.bimber.features.pictureeditor.EditImageActivity;
import com.bimber.utils.emoji.EmojiUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.akarnokd.rxjava.interop.RxJavaInterop;
import id.zelory.compressor.Compressor;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Component for input outcoming messages
 */
public class MessageInputView extends BaseFragment implements IMessageInputView, TextWatcher, View.OnTouchListener,
        BackPressedListener, View.OnLayoutChangeListener, OnEmojiPopupShownListener, OnEmojiPopupDismissListener {

    public static final String SHARE_INTENT_KEY = "MessageInputView.SHARE_INTENT_KEY";

    public static final int GET_PHOTO_TYPE = 7890;
    public static final String HANDLED_SHARE_KEY = "HANDLED_SHARE_KEY";
    public static final int EDIT_IMAGE_REQUEST_CODE = 745;


    Unbinder unbinder;

    @Inject
    @LoggedInUserId
    String currentUserId;

    @Inject
    IMessageInputPresenter messageInputPresenter;
    @Inject
    BackPressedManager backPressedManager;

    @BindView(R.id.editTextMessageContent)
    EmojiEditText editTextMessageContent;
    @BindView(R.id.buttonSendMessageEnabled)
    FloatingActionButton buttonSendMessageEnabled;
    @BindView(R.id.buttonSendMessageDisabled)
    FloatingActionButton buttonSendMessageDisabled;
    @BindView(R.id.buttonOptions)
    FloatingActionButton buttonOptions;
    @BindView(R.id.buttonTakePhoto)
    FloatingActionButton buttonTakePhoto;
    @BindView(R.id.buttonAddPhoto)
    FloatingActionButton buttonAddPhoto;
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;
    @BindView(R.id.buttonEmojiToggle)
    ImageButton buttonEmojiToggle;

    private boolean handledShare;
    private final CompositeDisposable subscriptions = new CompositeDisposable();
    private boolean optionsShown;
    private PublishSubject<Integer> editTextHeightSubject = PublishSubject.create();
    private EmojiPopup emojiPopup;

    public static MessageInputView newInstanceWithShare(Intent shareIntent) {
        MessageInputView messageInputView = new MessageInputView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SHARE_INTENT_KEY, shareIntent);
        messageInputView.setArguments(bundle);
        return messageInputView;
    }

    public static MessageInputView newInstance() {
        return new MessageInputView();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        container.setClipChildren(false);
        container.setClipToPadding(false);
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.view_message_input, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            handledShare = savedInstanceState.getBoolean(HANDLED_SHARE_KEY, false);
        }
        view.setClipChildren(false);
        view.setClipToPadding(false);
        buttonSendMessageEnabled.setSoundEffectsEnabled(false);
        buttonSendMessageDisabled.setSoundEffectsEnabled(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emojiPopup = EmojiPopup.Builder.fromRootView(rootLayout)
                .setOnEmojiPopupShownListener(this)
                .setOnEmojiPopupDismissListener(this)
                .build(editTextMessageContent);
        editTextMessageContent.addTextChangedListener(this);
        editTextMessageContent.setText("");
        messageInputPresenter.bindView(this);
        handleShareIntent();
        backPressedManager.registerBackPressedListener(this);
        editTextMessageContent.addOnLayoutChangeListener(this);

    }

    @Override
    public void onPause() {
        showOptionsMenu(false);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        messageInputPresenter.unbindView(this);
        super.onDestroyView();
        unbinder.unbind();
    }


    /**
     * This method is called to notify you that, within s,
     * the count characters beginning at start have just replaced old text that had length before
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int count, int after) {
        if (s.length() > 0) {
            buttonSendMessageDisabled.hide();
            buttonSendMessageEnabled.show();
        } else {
            buttonSendMessageEnabled.hide();
            buttonSendMessageDisabled.show();
        }
    }

    /**
     * This method is called to notify you that, within s,
     * the count characters beginning at start are about to be replaced by new text with length after.
     */
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    /**
     * This method is called to notify you that, somewhere within s, the text has been changed.
     */
    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void showSendMessageErrorExpiredMembershipToast() {
        Toast.makeText(getContext(), R.string.view_chat_message_input_failed_to_send_message_exipred, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showGenericSendMessageErrorToast() {
        Toast.makeText(getContext(), R.string.view_chat_message_input_failed_to_send_message, Toast.LENGTH_SHORT).show();
    }

    private void handleShareIntent() {
        if (getArguments() != null && getArguments().containsKey(SHARE_INTENT_KEY) && !handledShare) {
            Intent shareIntent = getArguments().getParcelable(SHARE_INTENT_KEY);
            String action = shareIntent.getAction();
            String type = shareIntent.getType();
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(shareIntent);
                } else if (type.startsWith("image/")) {
                    handleSendImage(shareIntent);
                }
            }
        }
    }

    private void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            editTextMessageContent.setText(sharedText);
            editTextMessageContent.requestFocus();
            handledShare = true;
        }
    }

    private void handleSendImage(Intent intent) {
        Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        editImage(imageUri);
    }

    private void editImage(Uri uri) {
        Compressor compressor = new Compressor.Builder(getContext())
                .setBitmapConfig(Bitmap.Config.RGB_565)
                .setQuality(100)
                .build();
        subscriptions.add(RxJavaInterop.toV2Observable(compressor.compressToFileAsObservable(new File(uri.getPath())))
                .subscribeOn(Schedulers.computation())
                .map(Uri::fromFile)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(inputUri -> {
                    Uri outputUri = Uri.fromFile(new File(getContext().getCacheDir(), UUID.randomUUID().toString()));
                    Intent intent = EditImageActivity.newInstance(getContext(), inputUri, outputUri, R.style.AppTheme);
                    startActivityForResult(intent, EDIT_IMAGE_REQUEST_CODE);
                }, throwable -> {
                    Toast.makeText(getContext(), "Couldn't obtain image", Toast.LENGTH_LONG).show();
                    Timber.e("Something went wrong during compressing image");
                }));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                editImage(Uri.fromFile(imageFile));
            }
        });
        if (requestCode == EDIT_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                float ratio = data.getFloatExtra(EditImageActivity.EXTRA_OUTPUT_CROP_ASPECT_RATIO, 1.0f);
                Uri output = data.getParcelableExtra(EditImageActivity.EXTRA_OUTPUT_URI);
                if (output != null) {
                    messageInputPresenter.sendImageMessage(new File(output.getPath()), ratio);
                } else {
                    Toast.makeText(getContext(), "Couldn't send image", Toast.LENGTH_LONG).show();
                    Timber.e("Something went wrong during cropping image, null output");

                }
            } else if (resultCode == EditImageActivity.RESULT_ERROR) {
                Throwable error = (Throwable) data.getSerializableExtra(EditImageActivity.EXTRA_ERROR);
                Toast.makeText(getContext(), "Couldn't send image", Toast.LENGTH_LONG).show();
                Timber.e(error, "Something went wrong during cropping image");
            } else if (resultCode == RESULT_CANCELED) {
                //Well that's normal
            } else {
                Timber.d("Something went wrong during cropping image");
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(HANDLED_SHARE_KEY, handledShare);
    }

    private Observable<Boolean> photoPermissionsObservable() {
        return new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    private void showOptionsMenu(boolean show) {
        if (show) {
            rootLayout.setFocusableInTouchMode(true);
            rootLayout.setFocusable(true);
            rootLayout.setOnTouchListener(this);
            buttonOptions.animate().rotation(45).setInterpolator(new OvershootInterpolator()).start();
            buttonTakePhoto.show();
            buttonAddPhoto.show();
        } else {
            rootLayout.setFocusableInTouchMode(false);
            rootLayout.setFocusable(false);
            rootLayout.setOnTouchListener(null);
            buttonOptions.animate().rotation(0).setInterpolator(new OvershootInterpolator()).start();
            buttonTakePhoto.hide();
            buttonAddPhoto.hide();
        }
        optionsShown = show;
    }


    @OnClick({R.id.touchGuard, R.id.buttonEmojiToggle, R.id.buttonOptions, R.id.buttonTakePhoto, R.id.buttonAddPhoto, R.id
            .buttonSendMessageEnabled})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonOptions:
                showOptionsMenu(!optionsShown);
                break;
            case R.id.buttonTakePhoto:
                showOptionsMenu(false);
                photoPermissionsObservable().subscribe(permissionsProvided -> {
                    if (permissionsProvided) {
                        EasyImage.openCamera(this, GET_PHOTO_TYPE);
                    } else {
                        Toast.makeText(getContext(), "Permissions not provided", Toast.LENGTH_LONG).show();

                    }
                });
                break;
            case R.id.buttonAddPhoto:
                showOptionsMenu(false);
                photoPermissionsObservable().subscribe(permissionsProvided -> {
                    if (permissionsProvided) {
                        EasyImage.openGallery(this, GET_PHOTO_TYPE);
                    } else {
                        Toast.makeText(getContext(), "Permissions not provided", Toast.LENGTH_LONG).show();
                    }
                });

                break;
            case R.id.buttonSendMessageEnabled:
                showOptionsMenu(false);
                String messageText = editTextMessageContent.getText().toString();
                if (!messageText.isEmpty()) {
                    messageInputPresenter.sendTextMessage(EmojiUtils.emojify(messageText));
                    editTextMessageContent.setText("");
                }
                break;
            case R.id.editTextMessageContent:
                showOptionsMenu(false);
            case R.id.buttonEmojiToggle:
                emojiPopup.toggle();
                showOptionsMenu(false);
                break;
            case R.id.touchGuard:
                //This only prevents messages undernaht from being clicked
                showOptionsMenu(false);
                break;
        }
    }

    public PublishSubject<Integer> onEditTextHeightChanges() {
        return editTextHeightSubject;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        showOptionsMenu(false);
        return true;
    }

    @Override
    public String key() {
        return getClass().getSimpleName();
    }

    @Override
    public boolean shouldBackButtonProceed() {
        if (optionsShown) {
            showOptionsMenu(false);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) editTextMessageContent.getLayoutParams();
        editTextHeightSubject.onNext(bottom - top + layoutParams.topMargin + layoutParams.bottomMargin);

    }

    @Override
    public void onEmojiPopupShown() {
        buttonEmojiToggle.setImageResource(R.drawable.ic_keyboard_white_24dp);
    }

    @Override
    public void onEmojiPopupDismiss() {
        buttonEmojiToggle.setImageResource(R.drawable.ic_insert_emoticon_white_24dp);
    }
}
