package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.ContentType;

/**
 * Created by maciek on 28.04.17.
 */

public class MessageToTextConverter {

    private final static InitialMessageStringifier initialMessageStringifier = new InitialMessageStringifier();
    private final static RemovedMessageStringifier removedMessageStringifier = new RemovedMessageStringifier();
    private final static UnknownMessageStringifier unknownMessageStringifier = new UnknownMessageStringifier();
    private final static TextMessageStringifier textMessageStringifier = new TextMessageStringifier();
    private final static GroupDescriptionSetMessageStringifier groupDescriptionSetMessageStringifier = new
            GroupDescriptionSetMessageStringifier();
    private final static GroupNameSetMessageStringifier groupNameSetMessageStringifier = new GroupNameSetMessageStringifier();
    private final static GroupTimeSetMessageStringifier groupTimeSetMessageStringifier = new GroupTimeSetMessageStringifier();
    private final static GroupImageSetMessageStringifier groupImageSetMessageStringifier = new GroupImageSetMessageStringifier();
    private final static GroupLocationSetMessageStringifier groupLocationSetMessageStringifier = new GroupLocationSetMessageStringifier();
    private final static ParticipationPersonAddedMessageStringifier participationPersonAddedMessageStringifier = new
            ParticipationPersonAddedMessageStringifier();
    private final static ParticipationPersonLeftMessageStringifier participationPersonLeftMessageStringifier = new
            ParticipationPersonLeftMessageStringifier();
    private final static ParticipationPersonDeletedAccountMessageStringifier participationPersonDeletedAccountMessageStringifier = new
            ParticipationPersonDeletedAccountMessageStringifier();
    private final static ParticipationPersonRemovedMessageStringifier participationPersonRemovedMessageStringifier = new
            ParticipationPersonRemovedMessageStringifier();
    private final static ImageMessageStringifier imageMessageStringifier = new ImageMessageStringifier();


    public static String stringifyMessage(Context context, String currentUserId, ChatType chatType, String userName, Message message) {
        switch (message.getContentType()) {
            case IMAGE:
                return imageMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case INITIAL_MESSAGE:
                return initialMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case TEXT:
                return textMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case REMOVED:
                return removedMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case PARTICIPATION_PERSON_ADDED:
                return participationPersonAddedMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case PARTICIPATION_PERSON_REMOVED:
                return participationPersonRemovedMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case PARTICIPATION_PERSON_LEFT:
                return participationPersonLeftMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return participationPersonDeletedAccountMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName,
                        message);
            case GROUP_TIME_SET:
                return groupTimeSetMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case GROUP_LOCATION_SET:
                return groupLocationSetMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case GROUP_NAME_SET:
                return groupNameSetMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case GROUP_DESCRIPTION_SET:
                return groupDescriptionSetMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case GROUP_IMAGE_SET:
                return groupImageSetMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);
            case UNKNOWN:
            default:
                return unknownMessageStringifier.stringifyMessage(context, currentUserId, chatType, userName, message);

        }
    }

    public static boolean isStringifiable(ContentType contentType) {
        switch (contentType) {
            case IMAGE:
                return imageMessageStringifier.isStringifiable();
            case INITIAL_MESSAGE:
                return initialMessageStringifier.isStringifiable();
            case TEXT:
                return textMessageStringifier.isStringifiable();
            case REMOVED:
                return removedMessageStringifier.isStringifiable();
            case PARTICIPATION_PERSON_ADDED:
                return participationPersonAddedMessageStringifier.isStringifiable();
            case PARTICIPATION_PERSON_REMOVED:
                return participationPersonRemovedMessageStringifier.isStringifiable();
            case PARTICIPATION_PERSON_LEFT:
                return participationPersonLeftMessageStringifier.isStringifiable();
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return participationPersonDeletedAccountMessageStringifier.isStringifiable();
            case GROUP_TIME_SET:
                return groupTimeSetMessageStringifier.isStringifiable();
            case GROUP_LOCATION_SET:
                return groupLocationSetMessageStringifier.isStringifiable();
            case GROUP_NAME_SET:
                return groupNameSetMessageStringifier.isStringifiable();
            case GROUP_DESCRIPTION_SET:
                return groupDescriptionSetMessageStringifier.isStringifiable();
            case GROUP_IMAGE_SET:
                return groupImageSetMessageStringifier.isStringifiable();
            case UNKNOWN:
                return unknownMessageStringifier.isStringifiable();
            default:
                return false;
        }
    }

    public static boolean isNotificationFriendly(String currentUserId, Message message) {
        switch (message.getContentType()) {
            case IMAGE:
                return imageMessageStringifier.isNotificationFriendly(currentUserId, message);
            case INITIAL_MESSAGE:
                return initialMessageStringifier.isNotificationFriendly(currentUserId, message);
            case TEXT:
                return textMessageStringifier.isNotificationFriendly(currentUserId, message);
            case REMOVED:
                return removedMessageStringifier.isNotificationFriendly(currentUserId, message);
            case PARTICIPATION_PERSON_ADDED:
                return participationPersonAddedMessageStringifier.isNotificationFriendly(currentUserId, message);
            case PARTICIPATION_PERSON_REMOVED:
                return participationPersonRemovedMessageStringifier.isNotificationFriendly(currentUserId, message);
            case PARTICIPATION_PERSON_LEFT:
                return participationPersonLeftMessageStringifier.isNotificationFriendly(currentUserId, message);
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return participationPersonDeletedAccountMessageStringifier.isNotificationFriendly(currentUserId, message);
            case GROUP_TIME_SET:
                return groupTimeSetMessageStringifier.isNotificationFriendly(currentUserId, message);
            case GROUP_LOCATION_SET:
                return groupLocationSetMessageStringifier.isNotificationFriendly(currentUserId, message);
            case GROUP_NAME_SET:
                return groupNameSetMessageStringifier.isNotificationFriendly(currentUserId, message);
            case GROUP_DESCRIPTION_SET:
                return groupDescriptionSetMessageStringifier.isNotificationFriendly(currentUserId, message);
            case GROUP_IMAGE_SET:
                return groupImageSetMessageStringifier.isNotificationFriendly(currentUserId, message);
            case UNKNOWN:
                return unknownMessageStringifier.isNotificationFriendly(currentUserId, message);
            default:
                return false;
        }
    }

}
