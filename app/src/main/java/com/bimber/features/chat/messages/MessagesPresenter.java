package com.bimber.features.chat.messages;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.LastSeenMessagesRepository;
import com.bimber.domain.messages.GetAggregatedMessagesUseCase;
import com.bimber.domain.messages.RemoveImageMessageUseCase;
import com.bimber.domain.messages.RemoveTextMessageUseCase;
import com.bimber.features.chat.messages.MessagesContract.IMessagesPresenter;
import com.bimber.features.chat.messages.MessagesContract.IMessagesView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 02.03.17.
 */

public class MessagesPresenter extends MvpAbstractPresenter<IMessagesView> implements IMessagesPresenter {

    private final String currentUserId;
    private final String currentChatId;
    private final GetAggregatedMessagesUseCase getAggregatedMessagesUseCase;
    private final ChatMemberRepository chatMemberRepository;
    private final RemoveTextMessageUseCase removeTextMessageUseCase;
    private final RemoveImageMessageUseCase removeImageMessageUseCase;
    private final LastSeenMessagesRepository lastSeenMessagesRepository;
    private final PublishProcessor<Integer> requestMessagesPublishSubject = PublishProcessor.create();
    private CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public MessagesPresenter(@LoggedInUserId String currentUserId, @ChatId String currentChatId,
                             GetAggregatedMessagesUseCase getAggregatedMessagesUseCase, ChatMemberRepository chatMemberRepository,
                             RemoveTextMessageUseCase removeTextMessageUseCase, RemoveImageMessageUseCase removeImageMessageUseCase,
                             LastSeenMessagesRepository lastSeenMessagesRepository) {
        this.currentUserId = currentUserId;
        this.currentChatId = currentChatId;
        this.getAggregatedMessagesUseCase = getAggregatedMessagesUseCase;
        this.chatMemberRepository = chatMemberRepository;
        this.removeTextMessageUseCase = removeTextMessageUseCase;
        this.removeImageMessageUseCase = removeImageMessageUseCase;
        this.lastSeenMessagesRepository = lastSeenMessagesRepository;
    }

    @Override
    protected void viewAttached(IMessagesView view) {
        Flowable<ChatVisibleChatMembership> chatMembershipFlowable = chatMemberRepository
                .onChatMembershipEvents(currentChatId, currentUserId)
                //TODO Remove doOnNext and change this to background thread
                .subscribeOn(AndroidSchedulers.mainThread())
                .concatMap(Flowable::fromIterable)
                .doOnNext(chatMembership -> view.showExpiredChatInfo(chatMembership.membershipStatus() != MembershipStatus.ACTIVE))
                .distinctUntilChanged(ChatVisibleChatMembership::membershipStatus);

        subscriptions.add(Flowable.combineLatest(chatMembershipFlowable, requestMessagesPublishSubject.distinctUntilChanged(),
                PaginationRequest::new)
                .switchMap(paginationRequest -> getAggregatedMessagesUseCase.onAggregatedMessages(paginationRequest
                        .chatVisibleChatMembership, paginationRequest.messagesCount)
                        .doOnError(throwable -> {
                            Timber.e(throwable, "Error while showing messages");
                            view.showMessagesErrorScreen();
                        })
                        .onErrorReturnItem(Collections.emptyList()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setMessages, throwable -> {
                    Timber.e(throwable, "Error while showing messages");
                    view.showMessagesErrorScreen();
                }));
    }

    @Override
    protected void viewDetached(IMessagesView view) {
        subscriptions.clear();
    }


    @Override
    public void deleteTextMessage(Message message) {
        subscriptions.add(chatMemberRepository.chatMembership(currentChatId, currentUserId)
                .flatMap(chatMembership -> {
                    if (chatMembership.membershipStatus() == MembershipStatus.EXPIRED) {
                        getView().showUpdateMessageErrorExpiredMembershipToast();
                        return Maybe.empty();
                    } else {
                        return Maybe.just(chatMembership);
                    }

                })
                .flatMapCompletable(chatMembership -> removeTextMessageUseCase.removeTextMessage(chatMembership, message.getMessageId()))
                .subscribe(() -> Timber.d("Removed text message: %s", message.getMessageId()),
                        throwable -> {
                            Timber.e(throwable, "Failed to remove text message: %s", message);
                            getView().showUpdateMessageGenericErrorToast();
                        }));
    }

    @Override
    public void deleteImageMessage(Message message) {
        subscriptions.add(chatMemberRepository.chatMembership(currentChatId, currentUserId)
                .flatMap(chatMembership -> {
                    if (chatMembership.membershipStatus() == MembershipStatus.EXPIRED) {
                        getView().showUpdateMessageErrorExpiredMembershipToast();
                        return Maybe.empty();
                    } else {
                        return Maybe.just(chatMembership);
                    }

                })
                .flatMapCompletable(chatMembership -> removeImageMessageUseCase.removeImageMessage(chatMembership, currentUserId, message.getMessageId()))
                .subscribe(() -> Timber.d("Removed image message: %s", message.getMessageId()),
                        throwable -> {
                            Timber.e(throwable, "Failed to remove image message: %s", message);
                            getView().showUpdateMessageGenericErrorToast();
                        }));
    }

    @Override
    public void setLastSeenMessage(String messageId) {
        subscriptions.add(lastSeenMessagesRepository.setChatUserLastSeenMessage(currentChatId, currentUserId, messageId)
                .subscribe(() -> Timber.d("Last seen message set to: %s", messageId),
                        throwable -> Timber.e(throwable, "Failed to set last seen message to: %s", messageId)));
    }

    @Override
    public void requestMessages(int messagesCount) {
        requestMessagesPublishSubject.onNext(messagesCount);

    }


    private static class PaginationRequest {
        public final ChatVisibleChatMembership chatVisibleChatMembership;
        public final int messagesCount;

        private PaginationRequest(ChatVisibleChatMembership chatVisibleChatMembership, int messagesCount) {
            this.chatVisibleChatMembership = chatVisibleChatMembership;
            this.messagesCount = messagesCount;
        }
    }

}
