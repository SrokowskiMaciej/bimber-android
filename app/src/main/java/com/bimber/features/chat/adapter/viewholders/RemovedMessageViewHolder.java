package com.bimber.features.chat.adapter.viewholders;

import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;

import java.text.DateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maciek on 03.03.17.
 */

public class RemovedMessageViewHolder extends BaseMessageViewHolder implements View.OnLongClickListener {

    @BindView(R.id.imageViewAvatar)
    ImageView imageViewAvatar;
    @BindView(R.id.textViewMessageContent)
    TextView textViewMessageContent;
    @BindView(R.id.textViewMessageTime)
    TextView textViewMessageTime;
    @BindView(R.id.viewMessageBackground)
    View viewMessageBackground;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;

    private final ConstraintSet constraintSet = new ConstraintSet();
    private AggregatedMessage shownMessage;

    private RemovedMessageViewHolder(View itemView, InteractionListener interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }


    public static RemovedMessageViewHolder create(ViewGroup parent, InteractionListener interactionListener,
                                                  GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_message_removed, parent, false);
        return new RemovedMessageViewHolder(view, interactionListener, glide);
    }

    @Override
    public void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage aggregatedMessage,
                            boolean
                                    selected, DefaultValues defaultValues) {
        shownMessage = aggregatedMessage;
        setMessageGroupMargins(aggregatedMessage.groupPosition());
        ProfileDataThumbnail userProfileData = aggregatedMessage.sendingUserProfile();
        boolean isCurrentUserMessage = aggregatedMessage.message().getUserId().equals(currentUserId);
        setPositionBasingOnUser(isCurrentUserMessage, aggregatedMessage.groupPosition());
        textViewMessageContent.setText(MessageToTextConverter.stringifyMessage(textViewMessageContent.getContext(),
                currentUserId,
                chatType, userProfileData.user.displayName, aggregatedMessage.message()));
        textViewMessageTime.setText(DateUtils.formatSameDayTime(aggregatedMessage.message().getTimestamp(),
                System.currentTimeMillis(), DateFormat.SHORT, DateFormat.SHORT));

        setMessageBackground(isCurrentUserMessage, aggregatedMessage.groupPosition());

        if (isCurrentUserMessage) {
            textViewMessageContent.setTextColor(ContextCompat.getColor(textViewMessageContent.getContext(), R.color.white));
        } else {
            textViewMessageContent.setTextColor(ContextCompat.getColor(textViewMessageContent.getContext(), R.color.colorPrimary));
        }

        boolean shouldShowAvatar = !isCurrentUserMessage &&
                (aggregatedMessage.groupPosition() == AggregatedMessage.MessagesGroupPosition.SINGLE ||
                        aggregatedMessage.groupPosition() == AggregatedMessage.MessagesGroupPosition.LEADING);

        if (shouldShowAvatar) {
            glide.load(userProfileData.photo.userPhotoUri)
                    .thumbnail(glide.load(userProfileData.photo.userPhotoThumb))
                    .into(imageViewAvatar);
        }
        rootView.setOnLongClickListener(this);
        rootView.setActivated(selected);
    }

    @Override
    public void recycle() {
        super.recycle();
        imageViewAvatar.setImageBitmap(null);
    }


    private void setPositionBasingOnUser(boolean isCurrentUserMessage, AggregatedMessage.MessagesGroupPosition
            messagesGroupPosition) {
        if (isCurrentUserMessage) {
            constraintSet.clone(rootView);
            constraintSet.setVisibility(imageViewAvatar.getId(), ConstraintSet.GONE);
            constraintSet.setHorizontalBias(imageViewAvatar.getId(), 1.0f);
            constraintSet.applyTo(rootView);
        } else {

            constraintSet.clone(rootView);
            if (messagesGroupPosition != AggregatedMessage.MessagesGroupPosition.LEADING && messagesGroupPosition !=
                    AggregatedMessage
                            .MessagesGroupPosition
                            .SINGLE) {
                constraintSet.setVisibility(imageViewAvatar.getId(), View.INVISIBLE);
            } else {
                constraintSet.setVisibility(imageViewAvatar.getId(), View.VISIBLE);
            }
            constraintSet.setHorizontalBias(imageViewAvatar.getId(), 0.0f);
            constraintSet.applyTo(rootView);
        }
    }


    private void setMessageBackground(boolean isCurrentUser, AggregatedMessage.MessagesGroupPosition
            messagesGroupPosition) {
        switch (messagesGroupPosition) {
            case SINGLE:
                if (isCurrentUser) {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_outgoing_single);
                } else {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_incoming_single);
                }
                break;
            case LEADING:
                if (isCurrentUser) {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_outgoing_top);
                } else {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_incoming_top);
                }
                break;
            case MIDDLE:
                if (isCurrentUser) {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_outgoing_middle);
                } else {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_incoming_middle);
                }
                break;
            case TRAILING:
                if (isCurrentUser) {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_outgoing_bottom);
                } else {
                    viewMessageBackground.setBackgroundResource(R.drawable.shape_message_incoming_bottom);
                }
                break;
        }
    }


    private void setMessageGroupMargins(AggregatedMessage.MessagesGroupPosition messagesGroupPosition) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) rootView.getLayoutParams();
        switch (messagesGroupPosition) {
            case NON_AGGREGABLE:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
            case SINGLE:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
            case LEADING:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = 0;
                break;
            case MIDDLE:
                marginLayoutParams.topMargin = 0;
                marginLayoutParams.bottomMargin = 0;
                break;
            case TRAILING:
                marginLayoutParams.topMargin = 0;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        interactionListener.longClicked(textViewMessageContent, shownMessage);
        return false;
    }

    @OnClick({R.id.imageViewAvatar, R.id.rootView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewAvatar:
                interactionListener.avatarClicked(imageViewAvatar, shownMessage);
                break;
            case R.id.rootView:
                interactionListener.clicked(textViewMessageContent, shownMessage);
                break;
        }
    }
}
