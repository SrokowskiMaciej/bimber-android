package com.bimber.features.chat.dialogs.changegrouptime;

import com.bimber.utils.firebase.Timestamp;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface ChangeGroupTimeContract {
    interface IChangeGroupTimeView {


        void showChangeGroupMeetingTimeFailure();

        void close();
    }

    interface IChangeGroupTimePresenter extends MvpPresenter<IChangeGroupTimeView> {
        void changeGroupMeetingTime(String groupId, Timestamp newTimestamp);
    }
}
