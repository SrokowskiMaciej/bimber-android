package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.domain.model.ChatMemberData;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 11.05.17.
 */
@AutoValue
public abstract class ChatMemberWithRole {


    public abstract ChatMemberData chatMember();

    public abstract ChatRole chatRole();

    public static ChatMemberWithRole create(ChatMemberData chatMember, String currentUserId, String groupOwnerId) {
        String uId = chatMember.profileData().user.uId;
        if (uId.equals(currentUserId)) {
            return new AutoValue_ChatMemberWithRole(chatMember, ChatRole.CURRENT_USER);
        } else if (uId.equals(groupOwnerId)) {
            return new AutoValue_ChatMemberWithRole(chatMember, ChatRole.OWNER);
        } else {
            return new AutoValue_ChatMemberWithRole(chatMember, ChatRole.USER);
        }
    }

    public enum ChatRole {
        USER,
        CURRENT_USER,
        OWNER
    }
}
