package com.bimber.features.chat.activity;

import android.support.v7.app.AppCompatActivity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.features.chat.iteminteraction.ActionModePlaceHolderProvider;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class ChatActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(ChatActivity activity);

    @Binds
    abstract ActionModePlaceHolderProvider actionModePlaceHolderProvider(ChatActivity chatActivity);

    @Binds
    abstract AppCompatActivity appCompatActivity(ChatActivity chatActivity);
}
