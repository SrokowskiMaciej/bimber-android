package com.bimber.features.chat.dialogs.changegrouplocation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.data.entities.Place;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationContract.IChangeGroupLocationPresenter;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationContract.IChangeGroupLocationView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class ChangeGroupLocationView extends BaseGroupFragment implements IChangeGroupLocationView {

    private static final int PLACE_PICKER_REQUEST_CODE = 475;
    @Inject
    IChangeGroupLocationPresenter changeGroupLocationPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        changeGroupLocationPresenter.bindView(this);
        if (savedInstanceState == null) {
            try {
                startActivityForResult(new PlacePicker.IntentBuilder().build(getActivity()), PLACE_PICKER_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PLACE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    com.google.android.gms.location.places.Place placePickerPlace = PlacePicker.getPlace(getContext(), data);
                    Place groupLocation = Place.create(String.valueOf(placePickerPlace.getName()), String.valueOf(placePickerPlace
                            .getAddress()), String.valueOf(placePickerPlace.getId()), placePickerPlace.getLatLng().longitude,
                            placePickerPlace.getLatLng().latitude);
                    changeGroupLocationPresenter.changeGroupLocation(groupId, groupLocation);
                } else {
                    close();
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("DUMMY_DATA", "DUMMY_DATA");
    }

    @Override
    public void onDestroyView() {
        changeGroupLocationPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void showChangeGroupLocationFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog1 -> close())
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }
}
