package com.bimber.features.chat.dialogs.changegroupname;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameContract.IChangeGroupNamePresenter;
import static com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameContract.IChangeGroupNameView;

/**
 * Created by maciek on 05.05.17.
 */

public class ChangeGroupNamePresenter extends MvpAbstractPresenter<IChangeGroupNameView> implements IChangeGroupNamePresenter {


    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final GroupRepository groupRepository;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ChangeGroupNamePresenter(@LoggedInUserId String currentUserId, DbConnectionService dbConnectionService, GroupRepository
            groupRepository) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.groupRepository = groupRepository;
    }

    @Override
    protected void viewAttached(IChangeGroupNameView view) {

    }

    @Override
    protected void viewDetached(IChangeGroupNameView view) {
        subscriptions.clear();
    }

    @Override
    public void requestGroupName(String groupId) {
        subscriptions.add(groupRepository.groupName(groupId)
                .toSingle()
                .subscribe(getView()::showGroupName, throwable -> getView().showChangeGroupNameFailure()));
    }

    @Override
    public void changeGroupName(String groupId, String newGroupName) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(groupRepository.setGroupName(currentUserId, groupId, newGroupName))
                .subscribe(() -> getView().close(), throwable -> getView().showChangeGroupNameFailure()));
    }
}
