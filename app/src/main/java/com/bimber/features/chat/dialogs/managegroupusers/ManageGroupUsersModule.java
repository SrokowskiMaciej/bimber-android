package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersContract.IManageGroupUsersPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class ManageGroupUsersModule {

    @ContributesAndroidInjector
    abstract ManageGroupUsersView contributeView();

    @Binds
    abstract IManageGroupUsersPresenter groupTimePresenter(ManageGroupUsersPresenter manageGroupUsersPresenter);
}
