package com.bimber.features.chat.dialogs.changegroupname;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface ChangeGroupNameContract {
    interface IChangeGroupNameView {
        void showGroupName(String groupName);

        void showChangeGroupNameFailure();

        void close();
    }

    interface IChangeGroupNamePresenter extends MvpPresenter<IChangeGroupNameView> {
        void requestGroupName(String groupId);

        void changeGroupName(String groupId, String newGroupName);
    }
}
