package com.bimber.features.chat.adapter.viewholders;

import com.bimber.data.entities.chat.messages.common.ContentType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maciek on 26.04.17.
 */

public enum ViewHolderType {
    IMAGE(9),
    INITIAL_MESSAGE(10),
    TEXT(11),
    REMOVED(12),
    PARTICIPATION_PERSON_ADDED(13),
    PARTICIPATION_PERSON_REMOVED(14),
    PARTICIPATION_PERSON_DELETED_ACCOUNT(22),
    PARTICIPATION_PERSON_LEFT(15),
    GROUP_TIME_SET(16),
    GROUP_LOCATION_SET(17),
    GROUP_NAME_SET(18),
    GROUP_DESCRIPTION_SET(19),
    GROUP_IMAGE_SET(20),
    UNKNOWN(21);

    public final int value;

    ViewHolderType(int value) {
        this.value = value;
    }

    private static final Map<Integer, ViewHolderType> typesByValue = new HashMap<Integer, ViewHolderType>();

    static {
        for (ViewHolderType type : ViewHolderType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    public static ViewHolderType fromValue(int value) {
        ViewHolderType viewHolderType = typesByValue.get(value);
        if (viewHolderType == null) {
            return UNKNOWN;
        }
        return viewHolderType;
    }

    public static ViewHolderType fromMessageType(ContentType contentType) {
        switch (contentType) {
            case IMAGE:
                return IMAGE;
            case INITIAL_MESSAGE:
                return INITIAL_MESSAGE;
            case TEXT:
                return TEXT;
            case REMOVED:
                return REMOVED;
            case PARTICIPATION_PERSON_ADDED:
                return PARTICIPATION_PERSON_ADDED;
            case PARTICIPATION_PERSON_REMOVED:
                return PARTICIPATION_PERSON_REMOVED;
            case PARTICIPATION_PERSON_LEFT:
                return PARTICIPATION_PERSON_LEFT;
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return PARTICIPATION_PERSON_DELETED_ACCOUNT;
            case GROUP_TIME_SET:
                return GROUP_TIME_SET;
            case GROUP_LOCATION_SET:
                return GROUP_LOCATION_SET;
            case GROUP_NAME_SET:
                return GROUP_NAME_SET;
            case GROUP_DESCRIPTION_SET:
                return GROUP_DESCRIPTION_SET;
            case GROUP_IMAGE_SET:
                return GROUP_IMAGE_SET;
            case UNKNOWN:
                return UNKNOWN;
            default:
                return UNKNOWN;
        }
    }
}
