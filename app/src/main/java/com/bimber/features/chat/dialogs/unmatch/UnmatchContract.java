package com.bimber.features.chat.dialogs.unmatch;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface UnmatchContract {
    interface IUnmatchView {
        void showProgressBar();

        void showLeaveGroupFailure();

        void close();
    }

    interface IUnmatchPresenter extends MvpPresenter<IUnmatchView> {
        void leaveGroup();
    }
}
