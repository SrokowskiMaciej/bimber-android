package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.local.Message;

/**
 * Created by maciek on 28.04.17.
 */

public interface Stringifier {
    String stringifyMessage(Context context, String currentUserId, ChatType chatType, String userName, Message message);

    boolean isStringifiable();

    boolean isNotificationFriendly(String currentUserId, Message message);
}
