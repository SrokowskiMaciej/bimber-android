package com.bimber.features.chat.dialogs.changegrouplocation;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.group.GroupId;
import com.bimber.data.entities.Place;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationContract.IChangeGroupLocationPresenter;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationContract.IChangeGroupLocationView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 05.05.17.
 */

public class ChangeGroupLocationPresenter extends MvpAbstractPresenter<IChangeGroupLocationView> implements IChangeGroupLocationPresenter {

    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final GroupRepository groupRepository;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ChangeGroupLocationPresenter(@LoggedInUserId String currentUserId, DbConnectionService
            dbConnectionService, GroupRepository groupRepository) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.groupRepository = groupRepository;
    }

    @Override
    public void changeGroupLocation(String groupId, Place newGroupLocation) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(groupRepository.setGroupLocation(currentUserId, groupId, newGroupLocation))
                .subscribe(() -> getView().close(), throwable -> {
                    Timber.e(throwable, "Couldn't change group location");
                    getView().showChangeGroupLocationFailure();
                }));
    }

    @Override
    protected void viewAttached(IChangeGroupLocationView view) {

    }

    @Override
    protected void viewDetached(IChangeGroupLocationView view) {
        subscriptions.clear();
    }
}
