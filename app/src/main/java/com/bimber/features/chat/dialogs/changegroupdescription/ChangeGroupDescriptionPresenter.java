package com.bimber.features.chat.dialogs.changegroupdescription;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionContract.IChangeGroupDescriptionPresenter;
import static com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionContract.IChangeGroupDescriptionView;

/**
 * Created by maciek on 05.05.17.
 */

public class ChangeGroupDescriptionPresenter extends MvpAbstractPresenter<IChangeGroupDescriptionView>
        implements IChangeGroupDescriptionPresenter {

    private final String currentUserId;
    private final GroupRepository groupRepository;
    private final DbConnectionService dbConnectionService;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ChangeGroupDescriptionPresenter(@LoggedInUserId String currentUserId, GroupRepository groupRepository, DbConnectionService
            dbConnectionService) {
        this.currentUserId = currentUserId;
        this.groupRepository = groupRepository;
        this.dbConnectionService = dbConnectionService;
    }


    @Override
    protected void viewAttached(IChangeGroupDescriptionView view) {

    }

    @Override
    protected void viewDetached(IChangeGroupDescriptionView view) {
        subscriptions.clear();
    }

    @Override
    public void requestGroupDescription(String groupId) {
        subscriptions.add(groupRepository.groupDescription(groupId)
                .toSingle()
                .subscribe(getView()::showGroupDescription, throwable -> getView().showChangeGroupDescriptionFailure()));
    }

    @Override
    public void changeGroupDescription(String groupId, String newGroupName) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(groupRepository.setGroupDescription(currentUserId, groupId, newGroupName))
                .subscribe(() -> getView().close(), throwable -> getView().showChangeGroupDescriptionFailure()));
    }
}
