package com.bimber.features.chat.dialogs.setgrouppicture;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureContract.ISetGroupPicturePresenter;
import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureContract.ISetGroupPictureView;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class SetGroupPictureView extends BaseGroupFragment implements ISetGroupPictureView {

    public static final int GET_PHOTO_TYPE = 7890;

    @Inject
    ISetGroupPicturePresenter setGroupPicturePresenter;

    private MaterialDialog progressBarDialog;
    private MaterialDialog failureDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setGroupPicturePresenter.bindView(this);
        if (savedInstanceState == null) {
            photoPermissionsObservable().subscribe(permissionsProvided -> {
                if (permissionsProvided) {
                    EasyImage.openChooserWithGallery(this, getString(R.string.view_set_group_image_chooser_text), GET_PHOTO_TYPE);
                } else {
                    Toast.makeText(getContext(), "Permissions not provided", Toast.LENGTH_LONG).show();

                }
            });
        }
        progressBarDialog = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                .cancelable(false)
                .build();
        failureDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog -> close())
                .build();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("DUMMY_DATA", "DUMMY_DATA");
    }

    @Override
    public void onDestroyView() {
        progressBarDialog.dismiss();
        failureDialog.dismiss();
        setGroupPicturePresenter.unbindView(this);
        super.onDestroyView();
    }

    private Observable<Boolean> photoPermissionsObservable() {
        return new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    @Override
    public void showProgressBar(boolean show) {
        if (show) {
            progressBarDialog.show();
        } else {
            progressBarDialog.hide();
        }
    }

    @Override
    public void showSetGroupPictureFailure(boolean show) {
        if (show) {
            failureDialog.show();
        } else {
            failureDialog.hide();
        }
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                UCrop.Options options = new UCrop.Options();
                options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                options.setToolbarColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                options.setActiveWidgetColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                UCrop.of(Uri.fromFile(imageFile), Uri.fromFile(new File(getContext().getCacheDir(), UUID.randomUUID().toString())))
                        .withAspectRatio(1.0f, 1.0f)
                        .withOptions(options)
                        .start(getContext(), SetGroupPictureView.this);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                close();
                super.onCanceled(source, type);
            }
        });
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == RESULT_OK) {
                Uri output = UCrop.getOutput(data);
                if (output != null) {
                    setGroupPicturePresenter.setPicture(groupId, new File(output.getPath()));
                } else {
                    Timber.e("Something went wrong during cropping image, null output");
                    showSetGroupPictureFailure(true);
                }
            } else if (resultCode == UCrop.RESULT_ERROR) {
                Throwable error = UCrop.getError(data);
                Timber.e(error, "Something went wrong during cropping image");
                showSetGroupPictureFailure(true);
            }
        }
    }
}
