package com.bimber.features.chat.adapter.viewholders;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.messages.stringify.MessageToTextConverter;
import com.bimber.utils.view.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 26.04.17.
 */

public class ParticipationPersonLeftViewHolder extends BaseMessageViewHolder {

    @BindView(R.id.textViewContent)
    TextView textViewContent;

    private ParticipationPersonLeftViewHolder(View itemView, InteractionListener
            interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }

    public static ParticipationPersonLeftViewHolder create(ViewGroup parent, InteractionListener interactionListener,
                                                           GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_message_base_system, parent,
                false);
        return new ParticipationPersonLeftViewHolder(view, interactionListener, glide);

    }

    @Override
    public void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage aggregatedMessage,
                            boolean
                                    selected, DefaultValues defaultValues) {

        textViewContent.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(textViewContent
                .getContext(), R.drawable
                .ic_person_left_white_24dp), null, null);
        ViewUtils.applyFixForDrawableTopTint(textViewContent, R.color.colorDivider);

        textViewContent.setText(MessageToTextConverter.stringifyMessage(textViewContent.getContext(), currentUserId,
                chatType,
                aggregatedMessage.sendingUserProfile().user.displayName, aggregatedMessage.message()));

    }
}
