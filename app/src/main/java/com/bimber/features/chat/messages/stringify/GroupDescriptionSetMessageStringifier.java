package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.group.GroupDescriptionSetContent;

/**
 * Created by maciek on 28.04.17.
 */

public class GroupDescriptionSetMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();
        GroupDescriptionSetContent content = (GroupDescriptionSetContent) message.getContent();

        if (currentUserId.equals(actingUserId)) {
            return String.format(context.getString(R.string.view_chat_message_group_description_set_first_person_text),
                    content.newGroupDescription());
        } else {
            return String.format(context.getString(R.string.view_chat_message_group_description_set_third_person_text), userName, content.newGroupDescription());
        }
    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
