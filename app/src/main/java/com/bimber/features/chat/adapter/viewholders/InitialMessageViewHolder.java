package com.bimber.features.chat.adapter.viewholders;

import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.DefaultValues;
import com.bimber.domain.model.AggregatedMessage;

import java.text.DateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 26.04.17.
 */

public class InitialMessageViewHolder extends BaseMessageViewHolder {

    @BindView(R.id.textViewDate)
    TextView textViewDate;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.textViewContent)
    TextView textViewContent;

    private InitialMessageViewHolder(View itemView, InteractionListener
            interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }

    public static InitialMessageViewHolder create(ViewGroup parent, InteractionListener
            interactionListener, GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_chat_message_initial, parent, false);
        return new InitialMessageViewHolder(view, interactionListener, glide);

    }

    @Override
    public void bindMessage(String currentUserId, ChatType chatType, AggregatedMessage aggregatedMessage, boolean
            selected, DefaultValues defaultValues) {
        textViewDate.setText(DateUtils.formatSameDayTime(aggregatedMessage.message().getTimestamp(), System
                .currentTimeMillis(), DateFormat.MEDIUM, DateFormat.SHORT));
        switch (chatType) {
            case DIALOG:
                textViewTitle.setText(textViewContent.getContext().getString(R.string
                        .view_chat_message_initial_dialog_title));
                textViewContent.setText(textViewContent.getContext().getString(R.string
                        .view_chat_message_initial_dialog_content));
                break;
            case GROUP:
                textViewTitle.setText(String.format(textViewContent.getContext().getString(R.string
                                .view_chat_message_initial_group_title),
                        aggregatedMessage.sendingUserProfile().user.displayName));
                textViewContent.setText(textViewContent.getContext().getString(R.string
                        .view_chat_message_initial_group_content));
                break;
        }
    }
}
