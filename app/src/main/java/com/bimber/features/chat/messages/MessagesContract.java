package com.bimber.features.chat.messages;

import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 25.04.17.
 */

public interface MessagesContract {
    interface IMessagesView {
        void setMessages(List<AggregatedMessage> latestMessages);

        void showExpiredChatInfo(boolean show);

        void showMessagesErrorScreen();

        void showUpdateMessageErrorExpiredMembershipToast();

        void showUpdateMessageGenericErrorToast();
    }

    interface IMessagesPresenter extends MvpPresenter<IMessagesView> {

        void deleteTextMessage(Message message);

        void deleteImageMessage(Message message);

        void setLastSeenMessage(String messageId);

        void requestMessages(int messagesCount);

    }
}
