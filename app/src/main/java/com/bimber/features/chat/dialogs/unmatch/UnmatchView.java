package com.bimber.features.chat.dialogs.unmatch;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.features.chat.dialogs.unmatch.UnmatchContract.IUnmatchPresenter;
import com.bimber.utils.di.DaggerDialogFragment;

import javax.inject.Inject;

/**
 * Created by maciek on 05.05.17.
 */

public class UnmatchView extends DaggerDialogFragment implements UnmatchContract.IUnmatchView {

    @Inject
    IUnmatchPresenter unmatchPresenter;


    private MaterialDialog progressBarDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        unmatchPresenter.bindView(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getContext())
                .alwaysCallInputCallback()
                .autoDismiss(false)
                .content(R.string.view_chat_menu_dialog_unmatch_title_text)
                .negativeText(R.string.view_chat_menu_dialog_unmatch_negative_text)
                .onNegative((dialog1, which) -> dismiss())
                .positiveText(R.string.view_chat_menu_dialog_unmatch_positive_text)
                .onPositive((dialog12, which) -> unmatchPresenter.leaveGroup());
        return dialogBuilder.build();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        unmatchPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void showProgressBar() {
        progressBarDialog = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                .cancelable(false)
                .show();
    }

    @Override
    public void showLeaveGroupFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .show();
        close();
    }

    @Override
    public void close() {
        if (progressBarDialog != null) {
            progressBarDialog.dismiss();
            progressBarDialog = null;
        }
        dismiss();
    }
}
