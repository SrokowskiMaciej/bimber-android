package com.bimber.features.chat.adapter.viewholders;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.view.ViewCompat;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.domain.model.AggregatedMessage.MessagesGroupPosition;
import com.bimber.features.common.ProgressView;
import com.bimber.utils.view.RoundedCornersView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.kevelbreh.androidunits.AndroidUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maciek on 03.03.17.
 */

public class ImageMessageViewHolder extends BaseMessageViewHolder implements View.OnLongClickListener,
        RequestListener<Drawable> {
    @BindView(R.id.imageViewAvatar)
    ImageView imageViewAvatar;
    @BindView(R.id.imageViewMessageContent)
    ImageView imageViewMessageContent;
    @BindView(R.id.textViewMessageTime)
    TextView textViewMessageTime;
    @BindView(R.id.textViewNameHint)
    TextView textViewNameHint;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;
    @BindView(R.id.contentCard)
    RoundedCornersView contentCard;

    public static final float IMAGE_CONTENT_SIZE = AndroidUnit.DENSITY_PIXELS.toPixels(256);
    public static final float IMAGE_CONTENT_SIZE_GROWTH_RATE = 10;
    public static final int CARD_RADIUS = (int) AndroidUnit.DENSITY_PIXELS.toPixels(16);

    @BindView(R.id.progressView)
    ProgressView progressView;
    private final ConstraintSet constraintSet = new ConstraintSet();

    private AggregatedMessage shownMessage;

    private ImageMessageViewHolder(View itemView, InteractionListener interactionListener, GlideRequests glide) {
        super(itemView, interactionListener, glide);
        ButterKnife.bind(this, itemView);
    }


    public static ImageMessageViewHolder create(ViewGroup parent, InteractionListener interactionListener,
                                                GlideRequests glide) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_message_image, parent, false);
        return new ImageMessageViewHolder(view, interactionListener, glide);

    }

    @Override
    public void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage aggregatedMessage,
                            boolean
                                    selected, DefaultValues defaultValues) {
        shownMessage = aggregatedMessage;
        ImageContent content = (ImageContent) aggregatedMessage.message().getContent();
        setMessageGroupMargins(aggregatedMessage.groupPosition());
        progressView.setVisibility(View.VISIBLE);
        switch (content.uploadStatus()) {
            case FINISHED:
                rootView.setVisibility(View.VISIBLE);
                break;
            default:
                if (aggregatedMessage.message().getUserId().equals(currentUserId)) {
                    rootView.setVisibility(View.VISIBLE);
                    progressView.setVisibility(View.VISIBLE);
                } else {
                    rootView.setVisibility(View.GONE);
                    progressView.setVisibility(View.VISIBLE);
                    return;
                }
        }
        boolean isCurrentUserMessage = aggregatedMessage.message().getUserId().equals(currentUserId);

        ViewGroup.LayoutParams layoutParams = contentCard.getLayoutParams();
        int contentSize = calculateContentSize(content.dimensionRatio());
        layoutParams.width = contentSize;
        contentCard.setLayoutParams(layoutParams);

        constraintSet.clone(rootView);
        constraintSet.setDimensionRatio(contentCard.getId(), String.valueOf(content.dimensionRatio()));
        constraintSet.applyTo(rootView);

        ViewCompat.setTransitionName(imageViewMessageContent, aggregatedMessage.message().getMessageId());
        glide.load(content.uri())
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter())
                .listener(this)
                .into(imageViewMessageContent);
        setPositionBasingOnUser(isCurrentUserMessage, aggregatedMessage.groupPosition());

        if (isCurrentUserMessage) {
            ((FrameLayout.LayoutParams) textViewMessageTime.getLayoutParams()).gravity = Gravity.RIGHT | Gravity.BOTTOM;
        } else {
            ((FrameLayout.LayoutParams) textViewMessageTime.getLayoutParams()).gravity = Gravity.LEFT | Gravity.BOTTOM;
        }
        textViewMessageTime.setText(DateUtils.formatDateTime(textViewMessageTime.getContext(), aggregatedMessage
                .message()
                .getTimestamp(), (DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME | DateUtils
                .FORMAT_NUMERIC_DATE)));

        setMessageBackground(isCurrentUserMessage, aggregatedMessage.groupPosition());

        boolean shouldShowAvatar = !isCurrentUserMessage &&
                (aggregatedMessage.groupPosition() == MessagesGroupPosition.SINGLE ||
                        aggregatedMessage.groupPosition() == MessagesGroupPosition.LEADING);

        if (shouldShowAvatar) {
            glide.load(aggregatedMessage.sendingUserProfile().photo.userPhotoUri)
                    .thumbnail(glide.load(aggregatedMessage.sendingUserProfile().photo.userPhotoThumb))
                    .into(imageViewAvatar);
        }

        if (shouldShowAvatar && chatType == Chattable.ChatType.GROUP) {
            textViewNameHint.setVisibility(View.VISIBLE);
            textViewNameHint.setText(aggregatedMessage.sendingUserProfile().user.displayName);
        } else {
            textViewNameHint.setVisibility(View.GONE);
        }

        imageViewMessageContent.setOnLongClickListener(this);
        rootView.setOnLongClickListener(this);
        rootView.setActivated(selected);
    }


    @Override
    public void recycle() {
        super.recycle();
        imageViewMessageContent.setImageBitmap(null);
        imageViewAvatar.setImageBitmap(null);
    }

    private int calculateContentSize(float dimensionRatio) {
        float base = (float) (IMAGE_CONTENT_SIZE_GROWTH_RATE * Math.log(1.0f + dimensionRatio));
        float i = base / (1.0f + base);
        return (int) (i * IMAGE_CONTENT_SIZE);
    }

    private void setPositionBasingOnUser(boolean isCurrentUserMessage, MessagesGroupPosition messagesGroupPosition) {
        if (isCurrentUserMessage) {
            constraintSet.clone(rootView);
            constraintSet.setVisibility(imageViewAvatar.getId(), ConstraintSet.GONE);
            constraintSet.setHorizontalBias(imageViewAvatar.getId(), 1.0f);
            constraintSet.applyTo(rootView);
            textViewMessageTime.setBackgroundResource(R.drawable.bg_date_guard_top_left_corner_16dp);
        } else {

            constraintSet.clone(rootView);
            if (messagesGroupPosition != MessagesGroupPosition.LEADING && messagesGroupPosition !=
                    MessagesGroupPosition.SINGLE) {
                constraintSet.setVisibility(imageViewAvatar.getId(), View.INVISIBLE);
            } else {
                constraintSet.setVisibility(imageViewAvatar.getId(), View.VISIBLE);
            }
            constraintSet.setHorizontalBias(imageViewAvatar.getId(), 0.0f);
            constraintSet.applyTo(rootView);
            textViewMessageTime.setBackgroundResource(R.drawable.bg_date_guard_top_right_corner_16dp);
        }
    }

    private void setMessageBackground(boolean isCurrentUser, MessagesGroupPosition messagesGroupPosition) {
        switch (messagesGroupPosition) {
            case SINGLE:
                contentCard.setCorners(CARD_RADIUS, CARD_RADIUS, CARD_RADIUS, CARD_RADIUS);
                break;
            case LEADING:
                if (isCurrentUser) {
                    contentCard.setCorners(CARD_RADIUS, CARD_RADIUS, CARD_RADIUS, 0);
                } else {
                    contentCard.setCorners(CARD_RADIUS, CARD_RADIUS, 0, CARD_RADIUS);
                }
                break;
            case MIDDLE:
                if (isCurrentUser) {
                    contentCard.setCorners(CARD_RADIUS, 0, CARD_RADIUS, 0);
                } else {
                    contentCard.setCorners(0, CARD_RADIUS, 0, CARD_RADIUS);
                }
                break;
            case TRAILING:
                if (isCurrentUser) {
                    contentCard.setCorners(CARD_RADIUS, 0, CARD_RADIUS, CARD_RADIUS);
                } else {
                    contentCard.setCorners(0, CARD_RADIUS, CARD_RADIUS, CARD_RADIUS);
                }
                break;
        }
    }

    private void setMessageGroupMargins(MessagesGroupPosition messagesGroupPosition) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) rootView.getLayoutParams();
        switch (messagesGroupPosition) {
            case NON_AGGREGABLE:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
            case SINGLE:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
            case LEADING:
                marginLayoutParams.topMargin = MESSAGE_GROUP_MARGIN;
                marginLayoutParams.bottomMargin = 0;
                break;
            case MIDDLE:
                marginLayoutParams.topMargin = 0;
                marginLayoutParams.bottomMargin = 0;
                break;
            case TRAILING:
                marginLayoutParams.topMargin = 0;
                marginLayoutParams.bottomMargin = MESSAGE_GROUP_MARGIN;
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        interactionListener.longClicked(imageViewMessageContent, shownMessage);
        return false;
    }

    @OnClick({R.id.imageViewAvatar, R.id.imageViewMessageContent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewAvatar:
                interactionListener.avatarClicked(imageViewAvatar, shownMessage);
                break;
            case R.id.imageViewMessageContent:
                interactionListener.clicked(imageViewMessageContent, shownMessage);
                break;
        }
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean
            isFirstResource) {
        return false;
    }

    @Override
    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource,
                                   boolean
                                           isFirstResource) {
        if (((ImageContent) shownMessage.message().getContent()).uploadStatus() == ImageContent.UploadStatus.FINISHED) {
            progressView.setVisibility(View.GONE);
        }
        return false;
    }
}
