package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.domain.model.ChatMemberData;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 02.03.17.
 */

public interface ManageGroupUsersContract {
    interface IManageGroupUsersView {

        void showChatMembers(List<ManagedChatMember> chatMembers);

        void showFriends(List<ManagedFriend> friend);

        void showIfGroupIsSecure(boolean secure);

        void showProgressBar();

        void hideProgressBar();

        void showManageUsersDataLoadFailure();

        void showManageUsersUserStatusChangeFailure();

        void close();
    }

    interface IManageGroupUsersPresenter extends MvpPresenter<IManageGroupUsersView> {
        void requestMembersData(String groupId);

        void addUser(String groupId, String participantId);

        void removeParticipant(String groupId, String participantId);
    }
}
