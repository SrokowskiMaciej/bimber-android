package com.bimber.features.chat.dialogs.managegroupusers.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersViewBuilder;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;

/**
 * Created by maciek on 02.03.17.
 */

public class ManageGroupUsersActivity extends BaseLoggedInActivity {

    private static final String MANAGE_GROUP_USERS_FRAGMENT_TAG = "MANAGE_GROUP_USERS_FRAGMENT_TAG";
    private static final String GROUP_ID_ARG = "GROUP_ID_ARG";

    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";

    private String groupId;

    public static Intent newInstance(Context context, String groupId) {
        Intent intent = new Intent(context, ManageGroupUsersActivity.class);
        intent.putExtra(GROUP_ID_ARG, groupId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        groupId = getIntent().getStringExtra(GROUP_ID_ARG);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (getSupportFragmentManager().findFragmentByTag(NEW_CHAT_MESSASGE_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, NewChatMessageSnackbarFragment.newInstance(CloudMessageDataHandler.HandlingPriority
                            .MEDIUM), NEW_CHAT_MESSASGE_TAG)
                    .commit();
        }
        if (getSupportFragmentManager().findFragmentByTag(MANAGE_GROUP_USERS_FRAGMENT_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, new ManageGroupUsersViewBuilder(groupId).build(), MANAGE_GROUP_USERS_FRAGMENT_TAG)
                    .commit();
        }
    }
}
