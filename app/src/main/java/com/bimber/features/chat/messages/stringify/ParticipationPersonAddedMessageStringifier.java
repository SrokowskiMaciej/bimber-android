package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonAddedContent;

/**
 * Created by maciek on 28.04.17.
 */

public class ParticipationPersonAddedMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();
        ParticipationPersonAddedContent content = (ParticipationPersonAddedContent) message.getContent();


        switch (chatType) {
            case DIALOG:
                if (currentUserId.equals(actingUserId)) {
                    return context.getString(R.string.view_chat_message_participation_person_dialog_rejoined_first_person_text);
                } else {
                    return String.format(context.getString(R.string
                            .view_chat_message_participation_person_dialog_rejoined_third_person_text), content.addedPersonName());

                }
            case GROUP:
                if (currentUserId.equals(actingUserId)) {
                    return String.format(context.getString(R.string
                            .view_chat_message_participation_person_added_group_third_person_added_first_person_adding_text), content
                            .addedPersonName());

                } else {
                    if (currentUserId.equals(content.addedPersonId())) {
                        return String.format(context.getString(R.string
                                        .view_chat_message_participation_person_added_group_first_person_added_third_person_adding_text), userName);
                    } else {

                        return String.format(context.getString(R.string
                                        .view_chat_message_participation_person_added_group_third_person_added_third_person_adding_text), userName, content.addedPersonName());

                    }
                }
            default:
                throw new IllegalStateException("Oops");
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return !(((ParticipationPersonAddedContent) message.getContent()).addedPersonId().equals(currentUserId));
    }
}
