package com.bimber.features.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Barrier;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.BitmapPool.DrawablePool;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatProvider;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.data.analytics.BimberAnalytics;
import com.bimber.data.analytics.BimberAnalytics.CreatePartyEntryPoint;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.domain.model.GroupData;
import com.bimber.features.chat.ChatContract.DialogState;
import com.bimber.features.chat.ChatContract.GroupState;
import com.bimber.features.chat.ChatContract.IChatPresenter;
import com.bimber.features.chat.ChatContract.IChatView;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationViewBuilder;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeViewBuilder;
import com.bimber.features.chat.dialogs.leavegroup.LeaveGroupViewBuilder;
import com.bimber.features.chat.dialogs.managegroupusers.activity.ManageGroupUsersActivity;
import com.bimber.features.chat.dialogs.unmatch.UnmatchView;
import com.bimber.features.chat.menu.ChatMenuItem;
import com.bimber.features.chat.messageinput.MessageInputView;
import com.bimber.features.chat.messages.MessagesView;
import com.bimber.features.creategroup.activity.CreatePartyInitializeActivity;
import com.bimber.features.groupcandidates.activity.GroupJoinCandidatesActivity;
import com.bimber.features.groupdetails.restrictedvisibility.activity.RestrictedGroupActivity;
import com.bimber.features.messaging.CloudMessageDataHandler.HandlingPriority;
import com.bimber.features.messaging.mute.MuteNotificationActivity;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.utils.TimeUtils;
import com.bimber.utils.images.BitmapUtils;
import com.bimber.utils.view.ViewUtils;
import com.bumptech.glide.Glide;
import com.github.kevelbreh.androidunits.AndroidUnit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by maciek on 02.03.17.
 */

public class ChatView extends BaseFragment implements IChatView, ChatProvider {

    public static final String CHAT_ID_KEY = "CHAT_ID_KEY";
    public static final String CHAT_TYPE_KEY = "CHAT_TYPE_KEY";

    public static final String SHARE_INTENT_KEY = "ChatView.SHARE_INTENT_KEY";

    public static final String MESSAGE_INPUT_VIEW_TAG = "MESSAGE_INPUT_VIEW_TAG";
    public static final String MESSAGES_VIEW_TAG = "MESSAGES_VIEW_TAG";

    public static final String GROUP_LEAVE_DIALOG_TAG = "GROUP_LEAVE_DIALOG_TAG";
    public static final String UNMATCH_DIALOG_TAG = "UNMATCH_DIALOG_TAG";

    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";
    public static final int LOGO_SIZE = (int) AndroidUnit.DENSITY_PIXELS.toPixels(32);
    public static final String GROUP_TIME_DIALOG_TAG = "GROUP_TIME_DIALOG_TAG";
    public static final String GROUP_LOCATION_DIALOG_TAG = "GROUP_LOCATION_DIALOG_TAG";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    IChatPresenter chatPresenter;
    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    DrawablePool drawablePool;
    @Inject
    BimberAnalytics bimberAnalytics;

    @BindView(R.id.imageViewChatLogo)
    CircleImageView imageViewChatLogo;
    @BindView(R.id.textViewChatTitle)
    TextView textViewChatTitle;
    @BindView(R.id.rootLayout)
    ConstraintLayout rootLayout;

    @BindView(R.id.imageViewBack)
    ImageButton imageViewBack;
    @BindView(R.id.viewStubCab)
    ViewStub viewStubCab;
    @BindView(R.id.messagesViewContainer)
    FrameLayout messagesViewContainer;
    @BindView(R.id.messageInputViewContainer)
    FrameLayout messageInputViewContainer;

    @BindView(R.id.discoverableStatus)
    TextView discoverableStatus;
    @BindView(R.id.discoverableLocationInfo)
    TextView discoverableLocationInfo;
    @BindView(R.id.discoverableTimeInfo)
    TextView discoverableTimeInfo;
    @BindView(R.id.discoverableMembersCountInfo)
    TextView discoverableMembersCountInfo;
    @BindView(R.id.discoverableExpiredGroupSetTime)
    TextView discoverableExpiredGroupSetTime;
    @BindView(R.id.discoverableViewBackground)
    View discoverableViewBackground;
    @BindView(R.id.discoverableCreateParty)
    View discoverableCreateParty;
    @BindView(R.id.discoverableCreatePartyContent)
    TextView discoverableCreatePartyContent;
    @BindView(R.id.chatTitleLayout)
    FrameLayout chatTitleLayout;
    @BindView(R.id.barrierDiscoverView)
    Barrier barrierDiscoverView;
    private String chatId;
    private ChatType chatType;

    private Set<ChatMenuItem> shownMenuItems = new HashSet<>();
    //TODO Check for config changes
    private ProfileDataThumbnail interlocutorData;
    private GroupData groupData;
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private CompositeDisposable groupInfoUpdateSubscriptions = new CompositeDisposable();

    public static ChatView newInstanceWithShare(String chatId, ChatType chatType, Intent shareIntent) {
        ChatView chatListView = new ChatView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SHARE_INTENT_KEY, shareIntent);
        bundle.putString(CHAT_ID_KEY, chatId);
        bundle.putString(CHAT_TYPE_KEY, chatType.toString());
        chatListView.setArguments(bundle);
        return chatListView;
    }

    public static ChatView newInstance(String chatId, ChatType chatType) {
        ChatView chatListView = new ChatView();
        Bundle bundle = new Bundle();
        bundle.putString(CHAT_ID_KEY, chatId);
        bundle.putString(CHAT_TYPE_KEY, chatType.toString());
        chatListView.setArguments(bundle);
        return chatListView;
    }

    @Override
    public void onAttach(Context context) {
        chatId = getArguments().getString(CHAT_ID_KEY);
        chatType = ChatType.valueOf(getArguments().getString(CHAT_TYPE_KEY));
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_chat, container, false);
        ButterKnife.bind(this, view);
        ViewUtils.applyFixForDrawableTopTint(discoverableMembersCountInfo, R.color.colorDivider);
        ViewUtils.applyFixForDrawableTopTint(discoverableTimeInfo, R.color.colorDivider);
        ViewUtils.applyFixForDrawableTopTint(discoverableLocationInfo, R.color.colorDivider);
        ViewUtils.applyFixForDrawableLeftTint(discoverableCreatePartyContent, R.color.colorPrimary);

        if (getChildFragmentManager().findFragmentByTag(NEW_CHAT_MESSASGE_TAG) == null) {
            getChildFragmentManager().beginTransaction()
                    .add(R.id.messagesViewContainer, NewChatMessageSnackbarFragment.newInstance(HandlingPriority.MEDIUM, chatId),
                            NEW_CHAT_MESSASGE_TAG)
                    .commit();
        }

        MessagesView messagesView;
        MessageInputView messageInputView;
        if (getArguments() != null && getArguments().containsKey(SHARE_INTENT_KEY)) {
            messagesView = new MessagesView();
            messageInputView = MessageInputView.newInstanceWithShare(getArguments().getParcelable(SHARE_INTENT_KEY));
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.messagesViewContainer, messagesView, MESSAGES_VIEW_TAG)
                    .replace(R.id.messageInputViewContainer, messageInputView, MESSAGE_INPUT_VIEW_TAG)
                    .commit();
        } else if (getChildFragmentManager().findFragmentByTag(MESSAGES_VIEW_TAG) == null &&
                getChildFragmentManager().findFragmentByTag(MESSAGE_INPUT_VIEW_TAG) == null) {
            messagesView = new MessagesView();
            messageInputView = MessageInputView.newInstance();
            getChildFragmentManager().beginTransaction()
                    .add(R.id.messagesViewContainer, messagesView, MESSAGES_VIEW_TAG)
                    .add(R.id.messageInputViewContainer, messageInputView, MESSAGE_INPUT_VIEW_TAG)
                    .commit();
        } else {
            messagesView = (MessagesView) getChildFragmentManager().findFragmentByTag(MESSAGES_VIEW_TAG);
            messageInputView = (MessageInputView) getChildFragmentManager().findFragmentByTag(MESSAGE_INPUT_VIEW_TAG);
        }
        subscriptions.add(messageInputView.onEditTextHeightChanges().subscribe(messagesView::setMessagesBottomPadding));
        setupToolbar();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        chatPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        groupInfoUpdateSubscriptions.clear();
        chatPresenter.unbindView(this);
        super.onDestroyView();
    }

    private void setupToolbar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        // Set empty item until Profile loads
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }


    @Override
    public void showDialogInterlocutorInfo(DialogState dialogState) {
        ProfileDataThumbnail interlocutorData = dialogState.interlocutorData();
        this.interlocutorData = interlocutorData;
        textViewChatTitle.setText(interlocutorData.user.displayName);
        Glide.with(this).load(interlocutorData.photo.userPhotoUri)
                .into(imageViewChatLogo);
        if (dialogState.showCreateGroupButton()) {
            showDialogDiscoveryInfo(interlocutorData);
        } else {
            hideDiscoveryInfo();
        }
        if (dialogState.showUnmatchButton()) {
            shownMenuItems.add(ChatMenuItem.UNMATCH);
        } else {
            shownMenuItems.remove(ChatMenuItem.UNMATCH);
        }
        if (dialogState.showInfoButton()) {
            shownMenuItems.add(ChatMenuItem.INFO);
            chatTitleLayout.setClickable(true);
            chatTitleLayout.setOnClickListener(view -> {
                String animatedPhotoUri = interlocutorData.photo.userPhotoUri;
                drawablePool.putDrawable(animatedPhotoUri, imageViewChatLogo.getDrawable());
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(getActivity(), imageViewChatLogo, animatedPhotoUri);
                startActivity(ProfileActivity.newInstance(getContext(), interlocutorData.user.uId,
                        animatedPhotoUri, chatId, false),
                        options.toBundle());
            });
        } else {
            shownMenuItems.remove(ChatMenuItem.INFO);
            chatTitleLayout.setClickable(true);
            chatTitleLayout.setOnClickListener(null);
        }
        if (dialogState.showNotificationsButton()) {
            shownMenuItems.add(ChatMenuItem.NOTIFICATIONS);
        } else {
            shownMenuItems.remove(ChatMenuItem.NOTIFICATIONS);
        }
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void showGroupInfo(GroupState groupState) {
        GroupData groupData = groupState.publicGroupDataThumbnail();
        this.groupData = groupData;

        textViewChatTitle.setText(groupData.group().value().name());
        subscriptions.add(groupData.photosUris()
                .toList()
                .flatMap(membersPhotos -> BitmapUtils.bitmapCollage(getContext(), membersPhotos, LOGO_SIZE, LOGO_SIZE))
                .onErrorResumeNext(BitmapUtils.bitmap(getContext(), R.drawable.ic_group_white_24dp, LOGO_SIZE, LOGO_SIZE))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bitmap -> imageViewChatLogo.setImageBitmap(bitmap)));

        if (groupState.showDiscoveryInfo()) {
            showPartyDiscoveryInfo(groupData);
        } else {
            hideDiscoveryInfo();
        }
        if (groupState.showLeaveGroupButton()) {
            shownMenuItems.add(ChatMenuItem.GROUP_LEAVE);
        } else {
            shownMenuItems.remove(ChatMenuItem.GROUP_LEAVE);
        }
        if (groupState.showJoinRequestsButton()) {
            shownMenuItems.add(ChatMenuItem.GROUP_JOIN_CANDIDATES_COUNT);
        } else {
            shownMenuItems.remove(ChatMenuItem.GROUP_JOIN_CANDIDATES_COUNT);
        }
        if (groupState.showNotificationsButton()) {
            shownMenuItems.add(ChatMenuItem.NOTIFICATIONS);
        } else {
            shownMenuItems.remove(ChatMenuItem.NOTIFICATIONS);
        }
        if (groupState.showSecurityButton()) {
            shownMenuItems.add(ChatMenuItem.PARTY_SECURITY);
        } else {
            shownMenuItems.remove(ChatMenuItem.PARTY_SECURITY);
        }
        if (groupState.showInfoButton()) {
            shownMenuItems.add(ChatMenuItem.INFO);
            chatTitleLayout.setClickable(true);
            chatTitleLayout.setOnClickListener(view -> {
                List<String> imageUris = groupData.photosUris().toList().blockingGet();
                String collageHash = DrawablePool.hashCollage(imageUris);
                drawablePool.putDrawable(collageHash, imageViewChatLogo.getDrawable());

                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(getActivity(), imageViewChatLogo, collageHash);
                startActivity(RestrictedGroupActivity.newInstance(getContext(), chatId, (ArrayList<String>) imageUris, chatId), options
                        .toBundle());
            });
        } else {
            shownMenuItems.remove(ChatMenuItem.INFO);
            chatTitleLayout.setClickable(false);
            chatTitleLayout.setOnClickListener(null);
        }
        getActivity().invalidateOptionsMenu();
    }

    private void showDialogDiscoveryInfo(ProfileDataThumbnail interlocutorData) {
        discoverableViewBackground.setVisibility(View.VISIBLE);
        setDiscoverableCreateParty(true, interlocutorData);
    }

    private void showPartyDiscoveryInfo(GroupData groupData) {
        discoverableViewBackground.setVisibility(View.VISIBLE);
        switch (PartyStatus.from(groupData.group().value())) {
            case UPCOMING:
            case ACTIVE:
                setDiscoverableStatus(true, groupData);
                setDiscoverableLocationInfo(true, groupData);
                setDiscoverableTimeInfo(true, groupData);
                setDiscoverableMembersCountInfo(true, groupData);
                setDiscoverableExpiredGroupSetTime(false, groupData);
                break;
            case ONGOING:
                setDiscoverableStatus(true, groupData);
                setDiscoverableLocationInfo(true, groupData);
                setDiscoverableTimeInfo(false, groupData);
                setDiscoverableMembersCountInfo(true, groupData);
                setDiscoverableExpiredGroupSetTime(false, groupData);
                break;
            case EXPIRED:
                setDiscoverableStatus(true, groupData);
                setDiscoverableLocationInfo(false, groupData);
                setDiscoverableTimeInfo(false, groupData);
                setDiscoverableMembersCountInfo(true, groupData);
                setDiscoverableExpiredGroupSetTime(true, groupData);
                break;
        }
    }

    private void hideDiscoveryInfo() {
        discoverableViewBackground.setVisibility(View.GONE);
        setDiscoverableStatus(false, groupData);
        setDiscoverableLocationInfo(false, groupData);
        setDiscoverableTimeInfo(false, groupData);
        setDiscoverableMembersCountInfo(false, groupData);
        setDiscoverableExpiredGroupSetTime(false, groupData);
        setDiscoverableCreateParty(false, interlocutorData);
    }

    public void setDiscoverableStatus(boolean visible, GroupData groupData) {
        if (visible) {
            discoverableStatus.setVisibility(View.VISIBLE);
            PartyStatus partyStatus = PartyStatus.from(groupData.group().value());
            discoverableStatus.setText(partyStatus.getStateName(getContext()));
            discoverableStatus.setCompoundDrawablesWithIntrinsicBounds(null, partyStatus.getIcon(getContext()), null, null);
            discoverableStatus.setOnClickListener(v -> {
                String dialogTitleBeginning = getString(R.string.view_chat_discovery_status_dialog_title);
                String dialogTitleStatus = partyStatus.getStateName(getContext());
                String dialogTitle = dialogTitleBeginning + dialogTitleStatus;
                SpannableStringBuilder dialogTitleBuilder = new SpannableStringBuilder(dialogTitle);
                dialogTitleBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), partyStatus.getStateColor())),
                        dialogTitleBeginning.length(),
                        dialogTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                new MaterialDialog.Builder(getContext())
                        .title(dialogTitleBuilder)
                        .content(R.string.view_chat_discovery_status_dialog_content)
                        .adapter(new PartyStatusesAdapter(), null)
                        .negativeText(R.string.dialog_ok_button_text)
                        .onNegative((dialog1, which) -> {
                            dialog1.dismiss();
                        })
                        .show();
            });
        } else {
            discoverableStatus.setVisibility(View.GONE);
        }
    }

    public void setDiscoverableLocationInfo(boolean visible, GroupData groupData) {
        if (visible) {
            discoverableLocationInfo.setVisibility(View.VISIBLE);
            discoverableLocationInfo.setText(groupData.group().value().location().getNormalizedPlaceName());
            discoverableLocationInfo.setOnClickListener(v -> {
                new MaterialDialog.Builder(getContext())
                        .title(R.string.view_chat_discovery_location_dialog_title)
                        .content(groupData.group().value().location().getNormalizedPlaceName())
                        .positiveText(R.string.view_chat_discovery_location_dialog_change_button)
                        .onPositive((dialog1, which) -> {
                            getChildFragmentManager().beginTransaction()
                                    .add(new ChangeGroupLocationViewBuilder(groupData.group().key()).build(), GROUP_LOCATION_DIALOG_TAG)
                                    .addToBackStack(null)
                                    .commit();
                        })
                        .negativeText(R.string.view_chat_discovery_location_dialog_open_map_button)
                        .onNegative((dialog1, which) -> {
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, groupData.group().value().location()
                                    .getGoogleMapsDirectionsUri());
                            getContext().startActivity(mapIntent);
                        })
                        .show();

            });
        } else {
            discoverableLocationInfo.setVisibility(View.GONE);
        }
    }

    public void setDiscoverableTimeInfo(boolean visible, GroupData groupData) {
        if (visible) {
            discoverableTimeInfo.setVisibility(View.VISIBLE);
            groupInfoUpdateSubscriptions.clear();
            groupInfoUpdateSubscriptions.add(Observable.interval(0, 30, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(delay -> {
                        long now = System.currentTimeMillis();
                        long meetingTime = groupData.group().value().meetingTime().value();
                        if (now < meetingTime) {
                            discoverableTimeInfo.setText(String.format(getContext().getString(R.string.group_remaining_time_upcoming),
                                    TimeUtils.formatDuration(getContext(), meetingTime, now)));
                        } else {
                            discoverableTimeInfo.setText(String.format(getContext().getString(R.string.group_remaining_time_past),
                                    TimeUtils.formatDuration(getContext(), meetingTime, now)));
                        }
                    }));
            discoverableTimeInfo.setOnClickListener(v -> {
                Date date = new Date(groupData.group().value().meetingTime().value());
                String dateText = DateFormat.getLongDateFormat(getContext()).format(date) + "\n" +
                        DateFormat.getTimeFormat(getContext()).format(date);
                new MaterialDialog.Builder(getContext())
                        .title(R.string.view_chat_discovery_time_dialog_title)
                        .content(dateText)
                        .positiveText(R.string.view_chat_discovery_time_dialog_change_button)
                        .onPositive((dialog1, which) -> {
                            getChildFragmentManager().beginTransaction()
                                    .add(new ChangeGroupTimeViewBuilder(groupData.group().key()).build(), GROUP_TIME_DIALOG_TAG)
                                    .addToBackStack(null)
                                    .commit();
                        })
                        .show();
            });

        } else {
            discoverableTimeInfo.setVisibility(View.GONE);
        }
    }

    public void setDiscoverableMembersCountInfo(boolean visible, GroupData groupData) {
        if (visible) {
            discoverableMembersCountInfo.setVisibility(View.VISIBLE);
            discoverableMembersCountInfo.setText(String.format(getContext().getResources().getQuantityString(R.plurals.group_members_count,
                    groupData.group().value().membersCount()), groupData.group().value().membersCount()));
            discoverableMembersCountInfo.setOnClickListener(v -> {
                startActivity(ManageGroupUsersActivity.newInstance(getContext(), groupData.group().key()));
            });
        } else {
            discoverableMembersCountInfo.setVisibility(View.GONE);
        }
    }

    public void setDiscoverableExpiredGroupSetTime(boolean visible, GroupData groupData) {
        if (visible) {
            discoverableExpiredGroupSetTime.setVisibility(View.VISIBLE);
            discoverableExpiredGroupSetTime.setText(R.string.view_chat_discovery_set_new_meeting_time);
            discoverableExpiredGroupSetTime.setOnClickListener(v -> {
                getChildFragmentManager().beginTransaction()
                        .add(new ChangeGroupTimeViewBuilder(groupData.group().key()).build(), GROUP_TIME_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
            });
        } else {
            discoverableExpiredGroupSetTime.setVisibility(View.GONE);
        }
    }

    public void setDiscoverableCreateParty(boolean visible, ProfileDataThumbnail interlocutorData) {
        if (visible) {
            discoverableCreateParty.setVisibility(View.VISIBLE);
            discoverableCreatePartyContent.setText(String.format(getString(R.string.view_chat_discovery_create_a_party), interlocutorData.
                    user.displayName));
            discoverableCreateParty.setOnClickListener(v -> {
                bimberAnalytics.logPartyCreationInit(CreatePartyEntryPoint.CHAT);
                ActivityOptionsCompat clipRevealAnimation = ActivityOptionsCompat.makeClipRevealAnimation(discoverableCreatePartyContent,
                        0, 0, discoverableCreatePartyContent.getWidth(), discoverableCreatePartyContent.getHeight());
                startActivity(CreatePartyInitializeActivity.newInstance(getActivity(), interlocutorData.user.uId), clipRevealAnimation.toBundle());
            });
        } else {
            discoverableCreateParty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        for (ChatMenuItem chatMenuItem : shownMenuItems) {
            MenuItem menuItem = menu.add(0, chatMenuItem.id, chatMenuItem.order, chatMenuItem.itemTitleRes);
            menuItem.setIcon(chatMenuItem.itemIconRes);
            menuItem.setShowAsAction(chatMenuItem.showAsAction);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (ChatMenuItem.fromId(item.getItemId())) {
            case NOTIFICATIONS:
                String chatName = "";
                if (chatType == ChatType.GROUP) {
                    chatName = groupData.group().value().name();
                } else if (chatType == ChatType.DIALOG) {
                    chatName = interlocutorData.user.displayName;
                }
                startActivity(MuteNotificationActivity.newInstance(getContext(), chatId, chatType, chatName, R.style.AppTheme));
                break;
            case PARTY_SECURITY:
                Drawable securityIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_security_white_24dp).mutate();
                DrawableCompat.setTint(securityIcon, ContextCompat.getColor(getContext(), R.color.colorSecondaryTextInverse));
                new MaterialDialog.Builder(getContext())
                        .icon(securityIcon)
                        .title(R.string.view_chat_menu_security_action_title)
                        .content(R.string.view_chat_menu_security_action_subtitle)
                        .checkBoxPromptRes(R.string.view_chat_menu_security_action_checkbox_prompt, groupData.group().value().secure(), null)
                        .positiveText(R.string.dialog_ok_button_text)
                        .onPositive((dialog, which) -> {
                            boolean secureParty = dialog.isPromptCheckBoxChecked();
                            subscriptions.add(chatPresenter.setPartySecure(secureParty)
                                    .subscribe(() -> {
                                        if (secureParty) {
                                            Toast.makeText(getContext(), R.string.view_chat_menu_security_action_changed_to_secure, Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getContext(), R.string.view_chat_menu_security_action_changed_to_unsecure, Toast.LENGTH_LONG).show();
                                        }
                                    }, throwable -> Toast.makeText(getContext(), R.string.view_chat_menu_security_action_change_failed, Toast.LENGTH_LONG).show()));
                        })
                        .show();
                break;
            case GROUP_LEAVE:
                getChildFragmentManager().beginTransaction()
                        .add(new LeaveGroupViewBuilder(chatId).build(), GROUP_LEAVE_DIALOG_TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case GROUP_JOIN_CANDIDATES_COUNT:
                startActivity(GroupJoinCandidatesActivity.newInstance(getContext(), chatId));
                break;
            case UNMATCH:
                new UnmatchView().show(getChildFragmentManager(), UNMATCH_DIALOG_TAG);
                break;
            case INFO:
                if (chatType == ChatType.GROUP) {
                    List<String> imageUris = groupData.photosUris().toList().blockingGet();
                    String collageHash = DrawablePool.hashCollage(imageUris);
                    drawablePool.putDrawable(collageHash, imageViewChatLogo.getDrawable());

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(getActivity(), imageViewChatLogo, collageHash);
                    startActivity(RestrictedGroupActivity.newInstance(getContext(), chatId, (ArrayList<String>) imageUris, chatId), options
                            .toBundle());
                } else if (chatType == ChatType.DIALOG) {
                    String animatedPhotoUri = interlocutorData.photo.userPhotoUri;
                    drawablePool.putDrawable(animatedPhotoUri, imageViewChatLogo.getDrawable());
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(getActivity(), imageViewChatLogo, animatedPhotoUri);
                    startActivity(ProfileActivity.newInstance(getContext(), interlocutorData.user.uId,
                            animatedPhotoUri, chatId, false),
                            options.toBundle());
                }
                break;
        }
        return false;
    }

    @Override
    public String provideChatId() {
        return chatId;
    }

    @Override
    public ChatType provideChatType() {
        return chatType;
    }

    @OnClick({R.id.imageViewBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                getActivity().onBackPressed();
                break;
        }
    }
}
