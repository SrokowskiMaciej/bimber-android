package com.bimber.features.chat.adapter.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.domain.model.AggregatedMessage;
import com.github.kevelbreh.androidunits.AndroidUnit;

/**
 * Created by maciek on 02.03.17.
 */

public abstract class BaseMessageViewHolder extends RecyclerView.ViewHolder {

    public static final int MESSAGE_GROUP_MARGIN = (int) AndroidUnit.DENSITY_PIXELS.toPixels(8);
    protected final InteractionListener interactionListener;
    protected final GlideRequests glide;


    public BaseMessageViewHolder(View itemView, InteractionListener interactionListener, GlideRequests glide) {
        super(itemView);
        this.interactionListener = interactionListener;
        this.glide = glide;
    }

    public abstract void bindMessage(String currentUserId, Chattable.ChatType chatType, AggregatedMessage
            aggregatedMessage, boolean
            selected,
                                     DefaultValues defaultValues);

    public void recycle() {

    }

}
