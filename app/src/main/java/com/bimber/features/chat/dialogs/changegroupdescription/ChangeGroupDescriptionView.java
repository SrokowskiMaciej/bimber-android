package com.bimber.features.chat.dialogs.changegroupdescription;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionContract.IChangeGroupDescriptionPresenter;
import com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionContract.IChangeGroupDescriptionView;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;
import icepick.State;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class ChangeGroupDescriptionView extends BaseGroupFragment implements IChangeGroupDescriptionView {

    @Inject
    IChangeGroupDescriptionPresenter changeGroupDescriptionPresenter;
    @State
    String groupDescription;

    private MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        changeGroupDescriptionPresenter.bindView(this);
        changeGroupDescriptionPresenter.requestGroupDescription(groupId);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        dialog.dismiss();
        changeGroupDescriptionPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void showGroupDescription(String groupDescription) {
        if (this.groupDescription == null) {
            this.groupDescription = groupDescription;
        }
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(getContext())
                .alwaysCallInputCallback()
                .autoDismiss(false)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                .title(R.string.view_chat_menu_dialog_group_description_title)
                .negativeText(R.string.view_chat_menu_dialog_group_description_negative_text)
                .onNegative((dialog1, which) -> close())
                .positiveText(R.string.view_chat_menu_dialog_group_description_positive_text)
                .onPositive((dialog12, which) -> changeGroupDescriptionPresenter.changeGroupDescription(groupId, this.groupDescription))
                .input(getString(R.string.view_chat_menu_dialog_group_description_input_hint), this.groupDescription,
                        (dialog1, input) -> this.groupDescription = input.toString());

        dialog = dialogBuilder.build();
        dialog.getInputEditText().setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                changeGroupDescriptionPresenter.changeGroupDescription(groupId, this.groupDescription);
            }
            return false;
        });
        dialog.show();
    }

    @Override
    public void showChangeGroupDescriptionFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog1 -> close())
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }
}
