package com.bimber.features.chat.di;

import android.support.v7.app.AppCompatActivity;

import com.bimber.base.ActivityScope;
import com.bimber.base.FragmentScope;
import com.bimber.base.activities.BackPressedManager;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatModule;
import com.bimber.base.chat.ChatProvider;
import com.bimber.data.entities.DefaultValues;
import com.bimber.features.chat.ChatContract.IChatPresenter;
import com.bimber.features.chat.ChatPresenter;
import com.bimber.features.chat.ChatView;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationFragmentModule;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeModule;
import com.bimber.features.chat.dialogs.leavegroup.LeaveGroupFragmentModule;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersModule;
import com.bimber.features.chat.dialogs.unmatch.UnmatchFragmentModule;
import com.bimber.features.chat.iteminteraction.ActionModeManager;
import com.bimber.features.chat.iteminteraction.ActionModePlaceHolderProvider;
import com.bimber.features.chat.messageinput.MessageInputContract.IMessageInputPresenter;
import com.bimber.features.chat.messageinput.MessageInputPresenter;
import com.bimber.features.chat.messageinput.MessageInputView;
import com.bimber.features.chat.messages.MessagesContract.IMessagesPresenter;
import com.bimber.features.chat.messages.MessagesPresenter;
import com.bimber.features.chat.messages.MessagesView;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 21.04.17.
 */
@Module
public abstract class ChatViewFragmentModule {


    @FragmentScope
    @ContributesAndroidInjector(modules = {ChatViewModule.class, LeaveGroupFragmentModule.class, UnmatchFragmentModule.class,
            ChangeGroupTimeModule.class, ChangeGroupLocationFragmentModule.class, ManageGroupUsersModule.class})
    abstract ChatView contributeChatView();


    @Provides
    @ActivityScope
    static ActionModeManager actionModeManager(@LoggedInUserId String currentUserId, AppCompatActivity activity,
                                               ActionModePlaceHolderProvider actionModePlaceHolderProvider, BackPressedManager
                                                       backPressedManager, DefaultValues defaultValues) {
        return new ActionModeManager(currentUserId, activity, actionModePlaceHolderProvider, backPressedManager, defaultValues);
    }

    @Module(includes = ChatModule.class)
    public abstract class ChatViewModule {
        @Binds
        abstract ChatProvider chatProvider(ChatView chatView);

        @Binds
        abstract IChatPresenter chatPresenter(ChatPresenter chatPresenter);

        @ContributesAndroidInjector
        abstract MessagesView contributeMessagesView();

        @ContributesAndroidInjector
        abstract MessageInputView contributeMessageInput();

        @Binds
        abstract IMessagesPresenter messagesPresenter(MessagesPresenter messagesPresenter);

        @Binds
        abstract IMessageInputPresenter messageInputPresenter(MessageInputPresenter messageInputPresenter);
    }
}
