package com.bimber.features.chat;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.base.chat.ChatType;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.domain.chats.CheckUserMembershipValidityUseCase;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.domain.groupjoin.GetGroupJoinCandidatesUseCase;
import com.bimber.domain.model.GroupData;
import com.bimber.features.chat.ChatContract.DialogState;
import com.bimber.features.chat.ChatContract.GroupState;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.bimber.features.chat.ChatContract.IChatPresenter;
import static com.bimber.features.chat.ChatContract.IChatView;

/**
 * Created by maciek on 02.03.17.
 */

public class ChatPresenter extends MvpAbstractPresenter<IChatView> implements IChatPresenter {

    private final String currentUserId;
    private final String currentChatId;
    private final DefaultValues defaultValues;
    private final Chattable.ChatType currentChatType;
    private final ChatMemberRepository chatMemberRepository;
    private final GetGroupJoinCandidatesUseCase getGroupJoinCandidatesUseCase;
    private final CheckUserMembershipValidityUseCase checkUserMembershipValidityUseCase;
    private final ProfileDataRepository profileDataRepository;
    private final GetGroupDataUseCase getGroupDataUseCase;
    private final GroupRepository groupRepository;
    private CompositeDisposable subscribtions = new CompositeDisposable();

    @Inject
    public ChatPresenter(@LoggedInUserId String currentUserId, @ChatId String currentChatId, DefaultValues defaultValues, @ChatType
            Chattable.ChatType currentChatType, ChatMemberRepository chatMemberRepository, GetGroupJoinCandidatesUseCase
                                 getGroupJoinCandidatesUseCase, CheckUserMembershipValidityUseCase checkUserMembershipValidityUseCase,
                         ProfileDataRepository
                                 profileDataRepository, GetGroupDataUseCase getGroupDataUseCase, GroupRepository groupRepository) {
        this.currentUserId = currentUserId;
        this.currentChatId = currentChatId;
        this.defaultValues = defaultValues;
        this.currentChatType = currentChatType;
        this.chatMemberRepository = chatMemberRepository;
        this.getGroupJoinCandidatesUseCase = getGroupJoinCandidatesUseCase;
        this.checkUserMembershipValidityUseCase = checkUserMembershipValidityUseCase;
        this.profileDataRepository = profileDataRepository;
        this.getGroupDataUseCase = getGroupDataUseCase;
        this.groupRepository = groupRepository;
    }


    @Override
    protected void viewAttached(IChatView view) {

        Flowable<Boolean> isActiveUser = checkUserMembershipValidityUseCase.onUserActiveChatMemberStatus(currentChatId, currentUserId)
                .onErrorReturnItem(false)
                .replay(1)
                .refCount();

        switch (currentChatType) {
            case DIALOG:
                Flowable<List<ChatVisibleChatMembership>> interlocutorMembership = chatMemberRepository.onChatMembershipsEvents
                        (currentChatId)
                        .flatMapSingle(memberships -> Flowable.fromIterable(memberships)
                                .filter(chatMembership -> !chatMembership.uId().equals(currentUserId))
                                .toList())
                        .replay(1)
                        .refCount();

                Flowable<Boolean> isInterlocutorAccountDeleted = interlocutorMembership.map((interlocutorMembershipData) ->
                        !interlocutorMembershipData.isEmpty());

                Flowable<ProfileDataThumbnail> interlocutorData = interlocutorMembership
                        .flatMapSingle(chatVisibleChatMemberships -> {
                            if (chatVisibleChatMemberships.isEmpty()) {
                                return Single.just(defaultValues.deletedProfileDataThumbnail());
                            } else {
                                return profileDataRepository.userProfileDataThumbnail(chatVisibleChatMemberships.get(0).uId());
                            }
                        })
                        .doOnError(throwable -> Timber.e(throwable, "Error obtaining interlocutor. Substituting unknown user id"));


                subscribtions.add(Flowable.combineLatest(interlocutorData, isActiveUser, isInterlocutorAccountDeleted, DialogState::create)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::showDialogInterlocutorInfo,
                                throwable -> Timber.e(throwable, "Error while listening to dialog state")));

                break;
            case GROUP:
                Flowable<GroupData> groupData = getGroupDataUseCase.onGroupDataOptimized(currentChatId, 3)
                        .flatMap(Flowable::fromIterable);

                Flowable<Boolean> hasGroupJoinRequests = getGroupJoinCandidatesUseCase.onGroupJoinRequestsCount(currentChatId)
                        .map(integer -> integer > 0)
                        .onErrorReturnItem(false)
                        .defaultIfEmpty(false);

                subscribtions.add(Flowable.combineLatest(Flowable.just(currentUserId), groupData, isActiveUser, hasGroupJoinRequests, GroupState::create)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::showGroupInfo,
                                throwable -> Timber.e(throwable, "Error while listening to group state")));
                break;
        }
    }

    @Override
    protected void viewDetached(IChatView view) {
        subscribtions.clear();
    }

    public Completable setPartySecure(boolean secure) {
        return groupRepository.setGroupSecurity(currentUserId, currentChatId, secure);
    }


}
