package com.bimber.features.chat.dialogs.setgrouppicture;

import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureContract.ISetGroupPicturePresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class SetGroupPictureFragmentModule {

    @ContributesAndroidInjector
    abstract SetGroupPictureView contributeView();

    @Binds
    abstract ISetGroupPicturePresenter groupNamePresenter(SetGroupPicturePresenter setGroupPicturePresenter);
}
