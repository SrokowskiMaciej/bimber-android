package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;

/**
 * Created by maciek on 16.05.17.
 */

public class ImageMessageStringifier implements Stringifier {
    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        if (currentUserId.equals(message.getUserId())) {
            return context.getString(R.string.view_chat_message_image_first_person);
        } else {
            return String.format(context.getString(R.string.view_chat_message_image_third_person), userName);
        }
    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
