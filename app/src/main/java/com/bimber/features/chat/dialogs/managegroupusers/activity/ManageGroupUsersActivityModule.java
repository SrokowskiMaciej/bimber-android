package com.bimber.features.chat.dialogs.managegroupusers.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;
import com.bimber.base.group.GroupProvider;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class ManageGroupUsersActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(ManageGroupUsersActivity activity);
}
