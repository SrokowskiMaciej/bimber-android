package com.bimber.features.chat.messages;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.base.chat.ChatType;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.NotificationSoundManager;
import com.bimber.features.chat.adapter.MessagesAdapter;
import com.bimber.features.chat.iteminteraction.ActionModeManager;
import com.bimber.features.chat.messages.MessagesContract.IMessagesPresenter;
import com.bimber.features.chat.messages.MessagesContract.IMessagesView;
import com.bimber.features.common.ErrorView;
import com.bimber.features.common.ProgressView;
import com.bimber.features.messaging.ChatNotification;
import com.bimber.features.messaging.NotificationType;
import com.bimber.features.photogallery.chat.activity.ChatPhotoGalleryActivity;
import com.bimber.features.profiledetails.activity.ProfileActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by maciek on 21.03.17.
 */

public class MessagesView extends BaseFragment implements IMessagesView {

    public static final String LAYOUT_MANAGER_STATE_KEY = "LAYOUT_MANAGER_STATE_KEY";
    public static final String REQUESTED_MESSAGES_KEY = "REQUESTED_MESSAGES_KEY";
    public static final int MESSAGE_PAGE_INCREMENT = 50;

    @BindView(R.id.recyclerViewMessages)
    RecyclerView recyclerViewMessages;

    @Inject
    @LoggedInUserId
    String currentUserId;
    @Inject
    @ChatType
    Chattable.ChatType chatType;
    @Inject
    @ChatId
    String chatId;
    @Inject
    DefaultValues defaultValues;
    @Inject
    IMessagesPresenter messagesPresenter;
    @Inject
    ActionModeManager actionModeManager;
    @Inject
    NotificationSoundManager notificationSoundManager;
    @BindView(R.id.swipeContainerView)
    SwipeRefreshLayout swipeContainerView;
    @BindView(R.id.errorViewMessages)
    ErrorView errorViewMessages;
    @BindView(R.id.progressView)
    ProgressView progressView;
    @BindView(R.id.textViewExpiredChatMembership)
    TextView textViewExpiredChatMembership;

    private MessagesAdapter messagesAdapter;
    private LinearLayoutManager layoutManager;
    private Parcelable layoutManagerState;
    private int requestedMessages = MESSAGE_PAGE_INCREMENT;

    private CompositeDisposable subscriptions = new CompositeDisposable();
    private Disposable errorViewMessagesSubscription;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.view_messages, container, false);
        ButterKnife.bind(this, view);
        messagesAdapter = new MessagesAdapter(GlideApp.with(this), currentUserId, chatType, defaultValues);
        recyclerViewMessages.setAdapter(messagesAdapter);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recyclerViewMessages.setLayoutManager(layoutManager);
        recyclerViewMessages.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                int lastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition();
                if (firstVisibleItemPosition < 5 &&
                        !swipeContainerView.isRefreshing() &&
                        !messagesAdapter.startsWithInitialMessage()) {
                    swipeContainerView.setRefreshing(true);
                    requestedMessages = messagesAdapter.getItemCount() + MESSAGE_PAGE_INCREMENT;
                    messagesPresenter.requestMessages(requestedMessages);
                }
                if (messagesAdapter.getItemCount() > 0 &&
                        lastCompletelyVisibleItemPosition == messagesAdapter.getItemCount() - 1) {
                    messagesPresenter.setLastSeenMessage(messagesAdapter.getMessage
                            (lastCompletelyVisibleItemPosition).message().getMessageId());
                }

            }
        });
        swipeContainerView.setOnRefreshListener(() -> swipeContainerView.setRefreshing(false));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        messagesPresenter.bindView(this);
        subscriptions.add(messagesAdapter.selectedMessages().subscribe(actionModeManager::handleSelectedMessages));
        subscriptions.add(messagesAdapter.clickedMessages().subscribe(messageClickedAction -> {
            if (messageClickedAction.message().message().getContentType() == ContentType.IMAGE) {
                int photoCount = Observable.fromIterable(messagesAdapter.getMessages())
                        .filter(message -> message.message().getContentType() == ContentType.IMAGE)
                        .count()
                        .blockingGet()
                        .intValue();
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation
                        (getActivity(),
                        messageClickedAction.clickedImageView(), messageClickedAction.message().message()
                                        .getMessageId());
                //TODO Change chat photo gallery activity
                startActivity(ChatPhotoGalleryActivity.newInstance(getContext(), chatId, chatType,
                        messageClickedAction.message().message(),
                        photoCount), activityOptionsCompat.toBundle());
            }
        }));
        subscriptions.add(actionModeManager.onTextMessageDeleted().subscribe(messagesPresenter::deleteTextMessage));
        subscriptions.add(actionModeManager.onImageMessageDeleted().subscribe(messagesPresenter::deleteImageMessage));
        subscriptions.add(actionModeManager.onClearSelection().subscribe(event -> messagesAdapter.clearSelection()));
        subscriptions.add(messagesAdapter.clickedAvatars().subscribe(avatarClickedAction -> {
            boolean shouldShowEvaluationStatus = chatType != Chattable.ChatType.DIALOG;
            String uri = avatarClickedAction.message().sendingUserProfile().photo.userPhotoUri;
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    avatarClickedAction
                    .avatarView(), uri);
            startActivity(ProfileActivity.newInstance(getContext(), avatarClickedAction.message().sendingUserProfile
                            ().user.uId,
                    uri, chatId, shouldShowEvaluationStatus), options.toBundle());
        }));

        if (savedInstanceState != null) {
            requestedMessages = savedInstanceState.getInt(REQUESTED_MESSAGES_KEY);
        }
        messagesPresenter.requestMessages(requestedMessages);
    }

    @Override
    public void onStart() {
        super.onStart();
        ChatNotification.cancelNotification(getContext(), NotificationType.NEW_MESSAGE, chatId);
        ChatNotification.cancelNotification(getContext(), NotificationType.MATCH, chatId);
        ChatNotification.cancelNotification(getContext(), NotificationType.GROUP_PARTICIPATION, chatId);
        ChatNotification.cancelNotification(getContext(), NotificationType.GROUP_MEETING_TIME, chatId);
        ChatNotification.cancelNotification(getContext(), NotificationType.GROUP_JOIN_REQUEST, chatId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            messagesAdapter.restoreState(savedInstanceState);
            layoutManagerState = savedInstanceState.getParcelable(LAYOUT_MANAGER_STATE_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        messagesAdapter.saveState(outState);
        outState.putParcelable(LAYOUT_MANAGER_STATE_KEY, layoutManager.onSaveInstanceState());
        outState.putInt(REQUESTED_MESSAGES_KEY, requestedMessages);
    }

    @Override
    public void onDestroyView() {
        subscriptions.clear();
        messagesPresenter.unbindView(this);
        layoutManagerState = layoutManager.onSaveInstanceState();
        super.onDestroyView();
    }


    @Override
    public void setMessages(List<AggregatedMessage> messages) {
        progressView.setVisibility(View.GONE);
        swipeContainerView.setRefreshing(false);
        if (isResumed() && messagesAdapter.getItemCount() > 0 && !messages.isEmpty()) {
            AggregatedMessage newLastMessage = messages.get(messages.size() - 1);
            AggregatedMessage oldLastMessage = messagesAdapter.getMessage(messagesAdapter.getItemCount() - 1);
            if (!newLastMessage.message().getMessageId().equals(oldLastMessage.message().getMessageId())) {
                notificationSoundManager.play();
            }
        }

        boolean shouldScrollToLastMessage = layoutManager.findLastVisibleItemPosition() >= messagesAdapter
                .getItemCount() - 3;
        messagesAdapter.setMessages(messages);
        //We do this after populating the adapter with new data to be able to scroll to the bottom
        if (shouldScrollToLastMessage) {
            layoutManager.scrollToPosition(messagesAdapter.getItemCount() - 1);
        }
        if (layoutManagerState != null) {
            layoutManager.onRestoreInstanceState(layoutManagerState);
            layoutManagerState = null;
        }

    }

    @Override
    public void showExpiredChatInfo(boolean show) {
        if (show) {
            textViewExpiredChatMembership.setVisibility(View.VISIBLE);
        } else {
            textViewExpiredChatMembership.setVisibility(View.GONE);
        }

    }

    @Override
    public void showMessagesErrorScreen() {
        if (errorViewMessagesSubscription != null && !errorViewMessagesSubscription.isDisposed()) {
            errorViewMessagesSubscription.dispose();
        }
        errorViewMessages.setVisibility(View.VISIBLE);
        errorViewMessagesSubscription = errorViewMessages.onErrorAction()
                .doOnNext(event -> {
                    progressView.setVisibility(View.VISIBLE);
                    errorViewMessages.setVisibility(View.GONE);
                })
                .delay(2, TimeUnit.SECONDS)
                .subscribe(event -> {
                    if (messagesPresenter.isViewBound()) {
                        messagesPresenter.unbindView(MessagesView.this);
                        messagesPresenter.bindView(MessagesView.this);
                    }
                }, throwable -> Timber.e(throwable, "Error loading messages"));

    }

    public void setMessagesBottomPadding(int bottomPadding) {
        recyclerViewMessages.setPadding(0, 0, 0, bottomPadding);
    }

    @Override
    public void showUpdateMessageErrorExpiredMembershipToast() {
        Toast.makeText(getContext(), R.string.view_chat_message_view_failed_to_update_message_exipred, Toast
                .LENGTH_SHORT).show();
    }

    @Override
    public void showUpdateMessageGenericErrorToast() {
        Toast.makeText(getContext(), R.string.view_chat_message_view_failed_to_update_message, Toast.LENGTH_SHORT)
                .show();
    }

}
