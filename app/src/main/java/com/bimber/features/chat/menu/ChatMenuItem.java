package com.bimber.features.chat.menu;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.MenuItem;

import com.bimber.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maciek on 04.05.17.
 */

public enum ChatMenuItem {
    GROUP_JOIN_CANDIDATES_COUNT(1, 1, R.string.view_chat_menu_item_group_join_candidates, R.drawable.ic_group_add_white_24dp,
            MenuItem.SHOW_AS_ACTION_ALWAYS),
    INFO(3, 3, R.string.view_chat_menu_item_info, R.drawable.ic_info_outline_white_24dp, MenuItem.SHOW_AS_ACTION_NEVER),
    NOTIFICATIONS(4, 4, R.string.view_chat_menu_item_notifications, R.drawable.ic_notifications_off_white_24dp, MenuItem.SHOW_AS_ACTION_NEVER),
    PARTY_SECURITY(5, 5, R.string.view_chat_menu_item_administration, R.drawable.ic_security_white_24dp, MenuItem.SHOW_AS_ACTION_NEVER),
    GROUP_LEAVE(7, 7, R.string.view_chat_menu_item_group_leave, R.drawable.ic_exit_to_app_white_24dp, MenuItem.SHOW_AS_ACTION_NEVER),
    UNMATCH(8, 8, R.string.view_chat_menu_item_unmatch, R.drawable.ic_exit_to_app_white_24dp, MenuItem.SHOW_AS_ACTION_NEVER);

    public final int id;

    public final int order;
    @StringRes
    public final int itemTitleRes;
    @DrawableRes
    public final int itemIconRes;

    public final int showAsAction;

    private static final Map<Integer, ChatMenuItem> typesById = new HashMap<>();

    static {
        for (ChatMenuItem chatMenuItem : ChatMenuItem.values()) {
            typesById.put(chatMenuItem.id, chatMenuItem);
        }
    }

    ChatMenuItem(int id, int order, int itemTitleRes, int itemIconRes, int showAsAction) {
        this.id = id;
        this.order = order;
        this.itemTitleRes = itemTitleRes;
        this.itemIconRes = itemIconRes;
        this.showAsAction = showAsAction;
    }

    public static ChatMenuItem fromId(int id) {
        return typesById.get(id);
    }
}
