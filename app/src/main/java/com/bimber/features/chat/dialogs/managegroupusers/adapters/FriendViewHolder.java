package com.bimber.features.chat.dialogs.managegroupusers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 16.03.17.
 */
public class FriendViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.imageViewAvatar)
    ImageView imageViewAvatar;
    @BindView(R.id.textViewFriendName)
    TextView textViewFriendName;
    @BindView(R.id.buttonAdd)
    ImageView buttonAdd;


    public FriendViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
