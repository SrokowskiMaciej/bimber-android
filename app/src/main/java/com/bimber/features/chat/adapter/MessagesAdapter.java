package com.bimber.features.chat.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.features.chat.adapter.viewholders.BaseMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.GroupDescriptionSetViewHolder;
import com.bimber.features.chat.adapter.viewholders.GroupImageSetViewHolder;
import com.bimber.features.chat.adapter.viewholders.GroupLocationSetViewHolder;
import com.bimber.features.chat.adapter.viewholders.GroupNameSetViewHolder;
import com.bimber.features.chat.adapter.viewholders.GroupTimeSetViewHolder;
import com.bimber.features.chat.adapter.viewholders.ImageMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.InitialMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.InteractionListener;
import com.bimber.features.chat.adapter.viewholders.ParticipationPersonAddedViewHolder;
import com.bimber.features.chat.adapter.viewholders.ParticipationPersonDeletedAccountViewHolder;
import com.bimber.features.chat.adapter.viewholders.ParticipationPersonLeftViewHolder;
import com.bimber.features.chat.adapter.viewholders.ParticipationPersonRemovedViewHolder;
import com.bimber.features.chat.adapter.viewholders.RemovedMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.TextMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.UnknownMessageViewHolder;
import com.bimber.features.chat.adapter.viewholders.ViewHolderType;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


/**
 * Created by maciek on 02.03.17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<BaseMessageViewHolder> implements InteractionListener {

    private static final String SELECTED_ITEMS_KEY = "SELECTED_ITEMS_KEY";
    private final GlideRequests glide;
    private final String currentUserId;
    private final Chattable.ChatType chatType;
    private final DefaultValues defaultValues;

    private List<AggregatedMessage> messages = Collections.emptyList();

    private ArrayList<String> selectedMessagesIds = new ArrayList<>();
    private PublishSubject<List<AggregatedMessage>> selectedMessagesPublishSubject = PublishSubject.create();
    private PublishSubject<MessageClickedAction> messageClickedPublishSubject = PublishSubject.create();
    private PublishSubject<AvatarClickedAction> avatarClickedPublishSubject = PublishSubject.create();


    public MessagesAdapter(GlideRequests glide, String currentUserId, Chattable.ChatType chatType,
                           DefaultValues defaultValues) {
        this.glide = glide;
        this.currentUserId = currentUserId;
        this.chatType = chatType;
        this.defaultValues = defaultValues;
        setHasStableIds(true);
    }

    @Override
    public BaseMessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolderType viewHolderType = ViewHolderType.fromValue(viewType);

        switch (viewHolderType) {
            case IMAGE:
                return ImageMessageViewHolder.create(parent, this, glide);
            case INITIAL_MESSAGE:
                return InitialMessageViewHolder.create(parent, this, glide);
            case TEXT:
                return TextMessageViewHolder.create(parent, this, glide);
            case REMOVED:
                return RemovedMessageViewHolder.create(parent, this, glide);
            case PARTICIPATION_PERSON_ADDED:
                return ParticipationPersonAddedViewHolder.create(parent, this, glide);
            case PARTICIPATION_PERSON_REMOVED:
                return ParticipationPersonRemovedViewHolder.create(parent, this, glide);
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return ParticipationPersonDeletedAccountViewHolder.create(parent, this, glide);
            case PARTICIPATION_PERSON_LEFT:
                return ParticipationPersonLeftViewHolder.create(parent, this, glide);
            case GROUP_TIME_SET:
                return GroupTimeSetViewHolder.create(parent, this, glide);
            case GROUP_LOCATION_SET:
                return GroupLocationSetViewHolder.create(parent, this, glide);
            case GROUP_NAME_SET:
                return GroupNameSetViewHolder.create(parent, this, glide);
            case GROUP_DESCRIPTION_SET:
                return GroupDescriptionSetViewHolder.create(parent, this, glide);
            case GROUP_IMAGE_SET:
                return GroupImageSetViewHolder.create(parent, this, glide);
            case UNKNOWN:
                return UnknownMessageViewHolder.create(parent, this, glide);
            default:
                return UnknownMessageViewHolder.create(parent, this, glide);
        }
    }

    @Override
    public void onBindViewHolder(BaseMessageViewHolder holder, int position) {
        AggregatedMessage message = messages.get(position);
        holder.bindMessage(currentUserId, chatType, message, selectedMessagesIds.contains(message.message()
                .getMessageId()), defaultValues);
    }

    @Override
    public void onViewRecycled(BaseMessageViewHolder holder) {
        super.onViewRecycled(holder);
        holder.recycle();
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public long getItemId(int position) {
        return messages.get(position).message().getMessageId().hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return ViewHolderType.fromMessageType(messages.get(position).message().getContentType()).value;

    }

    public boolean startsWithInitialMessage() {
        return !messages.isEmpty() && messages.get(0).message().getContentType() == ContentType.INITIAL_MESSAGE;
    }

    private void addMessageSelection(AggregatedMessage selectedMessage) {
        selectedMessagesIds.add(selectedMessage.message().getMessageId());
        //TODO
        notifyItemChanged(messages.indexOf(selectedMessage));
        selectedMessagesPublishSubject.onNext(extractSelectedMessages());
    }

    private void removeMessageSelection(AggregatedMessage unselectedMessage) {
        selectedMessagesIds.remove(unselectedMessage.message().getMessageId());
        //TODO
        notifyItemChanged(messages.indexOf(unselectedMessage));
        selectedMessagesPublishSubject.onNext(extractSelectedMessages());
    }

    private List<AggregatedMessage> extractSelectedMessages() {
        List<AggregatedMessage> selectedMessages = new ArrayList<>(selectedMessagesIds.size());
        for (AggregatedMessage message : messages) {
            if (selectedMessagesIds.contains(message.message().getMessageId())) {
                selectedMessages.add(message);
            }
            if (selectedMessages.size() == selectedMessagesIds.size()) {
                break;
            }
        }
        return selectedMessages;
    }

    public void setMessages(List<AggregatedMessage> messages) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<AggregatedMessage>(this
                .messages, messages) {

            @Override
            public boolean areItemsTheSame(AggregatedMessage oldItem, AggregatedMessage newItem) {
                return oldItem.message().getMessageId().equals(newItem.message().getMessageId());
            }

            @Override
            public boolean areContentsTheSame(AggregatedMessage oldItem, AggregatedMessage newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.messages = messages;
        diffResult.dispatchUpdatesTo(this);
        selectedMessagesPublishSubject.onNext(extractSelectedMessages());
    }

    public AggregatedMessage getMessage(int position) {
        return messages.get(position);
    }

    public List<AggregatedMessage> getMessages() {
        return messages;
    }

    public void clearSelection() {
        for (int position = 0; position < messages.size(); position++) {
            AggregatedMessage message = messages.get(position);
            if (this.selectedMessagesIds.contains(message.message().getMessageId())) {
                this.selectedMessagesIds.remove(message.message().getMessageId());
                notifyItemChanged(position);
            }
        }
        this.selectedMessagesIds.clear();
    }

    public Observable<List<AggregatedMessage>> selectedMessages() {
        return selectedMessagesPublishSubject;
    }

    public Observable<MessageClickedAction> clickedMessages() {
        return messageClickedPublishSubject;
    }

    public Observable<AvatarClickedAction> clickedAvatars() {
        return avatarClickedPublishSubject;
    }

    public void restoreState(Bundle in) {
        selectedMessagesIds = in.getStringArrayList(SELECTED_ITEMS_KEY);
        notifyDataSetChanged();
    }

    public void saveState(Bundle out) {
        out.putStringArrayList(SELECTED_ITEMS_KEY, selectedMessagesIds);
    }

    @Override
    public void avatarClicked(@NonNull View view, @NonNull AggregatedMessage message) {
        avatarClickedPublishSubject.onNext(AvatarClickedAction.create(view, message));
    }

    @Override
    public void clicked(@Nullable View view, @NonNull AggregatedMessage message) {
        if (selectedMessagesIds.isEmpty()) {
            messageClickedPublishSubject.onNext(MessageClickedAction.create(view, message));
        } else if (selectedMessagesIds.contains(message.message().getMessageId())) {
            removeMessageSelection(message);
        } else {
            addMessageSelection(message);
        }
    }

    @Override
    public void longClicked(@NonNull View view, AggregatedMessage message) {
        if (selectedMessagesIds.isEmpty()) {
            addMessageSelection(message);
        } else if (selectedMessagesIds.contains(message.message().getMessageId())) {
            removeMessageSelection(message);
        } else {
            addMessageSelection(message);
        }
    }

    @AutoValue
    public static abstract class MessageClickedAction {

        @Nullable
        public abstract View clickedImageView();

        public abstract AggregatedMessage message();

        public static MessageClickedAction create(View clickedImageView, AggregatedMessage message) {
            return new AutoValue_MessagesAdapter_MessageClickedAction(clickedImageView, message);
        }
    }

    @AutoValue
    public static abstract class AvatarClickedAction {
        public abstract View avatarView();

        public abstract AggregatedMessage message();

        public static AvatarClickedAction create(View avatarView, AggregatedMessage message) {
            return new AutoValue_MessagesAdapter_AvatarClickedAction(avatarView, message);
        }
    }
}
