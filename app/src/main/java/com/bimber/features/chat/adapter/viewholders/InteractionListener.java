package com.bimber.features.chat.adapter.viewholders;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.bimber.domain.model.AggregatedMessage;

/**
 * Created by maciek on 09.03.17.
 */

public interface InteractionListener {

    void avatarClicked(@NonNull View view, AggregatedMessage message);

    void clicked(@Nullable View view, @NonNull AggregatedMessage message);

    void longClicked(@NonNull View view, AggregatedMessage message);
}
