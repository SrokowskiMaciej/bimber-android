package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;

/**
 * Created by maciek on 28.04.17.
 */

public class GroupImageSetMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();

        if (currentUserId.equals(actingUserId)) {
            return context.getString(R.string.view_chat_message_group_image_set_first_person_text);
        } else {
            return String.format(context.getString(R.string.view_chat_message_group_image_set_third_person_text), userName);
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return true;
    }
}
