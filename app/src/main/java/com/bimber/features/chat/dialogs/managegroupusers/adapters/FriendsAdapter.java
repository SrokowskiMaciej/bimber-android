package com.bimber.features.chat.dialogs.managegroupusers.adapters;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.features.chat.dialogs.managegroupusers.ManagedFriend;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.google.auto.value.AutoValue;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 16.03.17.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendViewHolder> {

    private List<ManagedFriend> friends = Collections.emptyList();

    private PublishSubject<ManagedFriend> selectedFriendPublishSubject = PublishSubject.create();
    private PublishSubject<ManagedFriendClickedAction> chatMemberClickedPublishSubject = PublishSubject.create();

    private final GlideRequests glide;

    public FriendsAdapter(GlideRequests glide) {
        this.glide = glide;
    }


    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_user_thumbnail_add, parent,
                false));
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        ManagedFriend managedFriend = friends.get(position);
        glide.load(managedFriend.chatMember().profileData().photo.userPhotoUri).into(holder.imageViewAvatar);
        holder.imageViewAvatar.setOnClickListener(v -> {
            chatMemberClickedPublishSubject.onNext(ManagedFriendClickedAction.create(holder.imageViewAvatar, managedFriend));
        });
        holder.textViewFriendName.setText(managedFriend.chatMember().profileData().user.displayName);
        holder.buttonAdd.setOnClickListener(v -> {
            holder.buttonAdd.setOnClickListener(null);
            selectedFriendPublishSubject.onNext(managedFriend);
        });
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public void setFriends(List<ManagedFriend> friends) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<ManagedFriend>(this.friends, friends) {
            @Override
            public boolean areItemsTheSame(ManagedFriend oldItem, ManagedFriend newItem) {
                return oldItem.chatMember().profileData().user.uId.equals(newItem.chatMember().profileData().user.uId);
            }

            @Override
            public boolean areContentsTheSame(ManagedFriend oldItem, ManagedFriend newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.friends = friends;
        diffResult.dispatchUpdatesTo(this);
    }

    public Observable<ManagedFriend> onFriendAdded() {
        return selectedFriendPublishSubject;
    }

    public Observable<ManagedFriendClickedAction> onChatMemberClicked() {
        return chatMemberClickedPublishSubject;
    }

    @AutoValue
    public abstract static class ManagedFriendClickedAction {
        public abstract ImageView avatar();

        public abstract ManagedFriend chatMember();

        public static ManagedFriendClickedAction create(ImageView avatar, ManagedFriend managedFriend) {
            return new AutoValue_FriendsAdapter_ManagedFriendClickedAction(avatar, managedFriend);
        }
    }
}
