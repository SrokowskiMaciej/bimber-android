package com.bimber.features.chat.dialogs.leavegroup;

import com.bimber.features.chat.dialogs.leavegroup.LeaveGroupContract.ILeaveGroupPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class LeaveGroupFragmentModule {

    @ContributesAndroidInjector
    abstract LeaveGroupView contributeView();

    @Binds
    abstract ILeaveGroupPresenter leaveGroupPresenter(LeaveGroupPresenter leaveGroupPresenter);
}
