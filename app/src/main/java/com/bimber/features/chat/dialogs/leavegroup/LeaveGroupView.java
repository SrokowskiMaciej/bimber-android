package com.bimber.features.chat.dialogs.leavegroup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.features.chat.dialogs.leavegroup.LeaveGroupContract.ILeaveGroupPresenter;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import javax.inject.Inject;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class LeaveGroupView extends BaseGroupFragment implements LeaveGroupContract.ILeaveGroupView {

    @Inject
    ILeaveGroupPresenter leaveGroupPresenter;


    private MaterialDialog progressBarDialog;
    private MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        leaveGroupPresenter.bindView(this);
        dialog = new MaterialDialog.Builder(getContext())
                .alwaysCallInputCallback()
                .autoDismiss(false)
                .content(R.string.view_chat_menu_dialog_group_leave_title_text)
                .negativeText(R.string.view_chat_menu_dialog_group_leave_negative_text)
                .onNegative((dialog1, which) -> close())
                .positiveText(R.string.view_chat_menu_dialog_group_leave_positive_text)
                .onPositive((dialog12, which) -> leaveGroupPresenter.leaveGroup(groupId))
                .build();
        dialog.show();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        if(progressBarDialog!=null && progressBarDialog.isShowing()){
            progressBarDialog.dismiss();
        }
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
        leaveGroupPresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void showProgressBar() {

        progressBarDialog = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                .cancelable(false)
                .show();
    }

    @Override
    public void showLeaveGroupFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog1 -> close())
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }
}
