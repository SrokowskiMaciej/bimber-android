package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;

/**
 * Created by maciek on 28.04.17.
 */

public class InitialMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        switch (chatType) {
            case DIALOG:
                return context.getString(R.string.view_chat_message_initial_dialog_thumbnail);
            case GROUP:
                return context.getString(R.string.view_chat_message_initial_group_thumbnail);
        }
        return "";

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return false;
    }
}
