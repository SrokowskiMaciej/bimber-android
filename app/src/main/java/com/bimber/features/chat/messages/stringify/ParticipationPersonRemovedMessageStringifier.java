package com.bimber.features.chat.messages.stringify;

import android.content.Context;

import com.bimber.R;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonRemovedContent;

/**
 * Created by maciek on 28.04.17.
 */

public class ParticipationPersonRemovedMessageStringifier implements Stringifier {

    @Override
    public String stringifyMessage(Context context, String currentUserId, Chattable.ChatType chatType, String
            userName, Message message) {
        String actingUserId = message.getUserId();
        ParticipationPersonRemovedContent content = (ParticipationPersonRemovedContent) message.getContent();


        switch (chatType) {
            case GROUP:
                if (currentUserId.equals(actingUserId)) {
                    return String.format(context.getString(R.string
                                    .view_chat_message_participation_person_removed_group_third_person_removed_first_person_removing_text),
                            content.removedPersonName());
                } else {
                    if (currentUserId.equals(content.removedPersonId())) {
                        return String.format(context.getString(R.string
                                        .view_chat_message_participation_person_removed_group_first_person_removed_third_person_removing_text), userName);
                    } else {
                        return String.format(context.getString(R.string
                                        .view_chat_message_participation_person_removed_group_third_person_removed_third_person_removing_text), userName, content.removedPersonName());
                    }
                }
            case DIALOG:
                return "";
            default:
                throw new IllegalStateException("Oops");
        }

    }

    @Override
    public boolean isStringifiable() {
        return true;
    }

    @Override
    public boolean isNotificationFriendly(String currentUserId, Message message) {
        return !(((ParticipationPersonRemovedContent) message.getContent()).removedPersonId().equals(currentUserId));
    }
}
