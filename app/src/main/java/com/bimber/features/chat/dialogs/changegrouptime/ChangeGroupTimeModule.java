package com.bimber.features.chat.dialogs.changegrouptime;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeContract.*;

/**
 * Created by maciek on 31.07.17.
 */
@Module
public abstract class ChangeGroupTimeModule {

    @ContributesAndroidInjector
    abstract ChangeGroupTimeView contributeView();

    @Binds
    abstract IChangeGroupTimePresenter groupTimePresenter(ChangeGroupTimePresenter changeGroupTimePresenter);
}
