package com.bimber.features.chat.dialogs.changegroupdescription;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by maciek on 02.03.17.
 */

public interface ChangeGroupDescriptionContract {
    interface IChangeGroupDescriptionView {
        void showGroupDescription(String groupDescription);

        void showChangeGroupDescriptionFailure();

        void close();
    }

    interface IChangeGroupDescriptionPresenter extends MvpPresenter<IChangeGroupDescriptionView> {
        void requestGroupDescription(String groupId);

        void changeGroupDescription(String groupId, String newGroupName);
    }
}
