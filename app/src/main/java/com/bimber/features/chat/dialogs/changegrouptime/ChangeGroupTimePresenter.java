package com.bimber.features.chat.dialogs.changegrouptime;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeContract.IChangeGroupTimeView;
import com.bimber.utils.firebase.Timestamp;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeContract.IChangeGroupTimePresenter;

/**
 * Created by maciek on 05.05.17.
 */

public class ChangeGroupTimePresenter extends MvpAbstractPresenter<IChangeGroupTimeView> implements IChangeGroupTimePresenter {

    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final GroupRepository groupRepository;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public ChangeGroupTimePresenter(@LoggedInUserId String currentUserId, DbConnectionService dbConnectionService, GroupRepository
            groupRepository) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.groupRepository = groupRepository;
    }

    @Override
    public void changeGroupMeetingTime(String groupId, Timestamp newGroupTimestamp) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(groupRepository.setGroupMeetingTime(currentUserId, groupId, newGroupTimestamp))
                .subscribe(() -> getView().close(), throwable -> getView().showChangeGroupMeetingTimeFailure()));
    }

    @Override
    protected void viewAttached(IChangeGroupTimeView view) {

    }

    @Override
    protected void viewDetached(IChangeGroupTimeView view) {
        subscriptions.clear();
    }
}
