package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.domain.model.ChatMemberData;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 11.05.17.
 */
@AutoValue
public abstract class ManagedFriend {


    public abstract ChatMemberData chatMember();

    public abstract boolean availableToChange();

    public static ManagedFriend create(ChatMemberData chatMember, String currentUserId, String groupOwnerId, boolean isPartySecure) {
        return new AutoValue_ManagedFriend(chatMember, !isPartySecure || groupOwnerId.equals(currentUserId));

    }
}
