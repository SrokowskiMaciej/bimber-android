package com.bimber.features.chat.dialogs.changegrouplocation;

import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationContract.IChangeGroupLocationPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class ChangeGroupLocationFragmentModule {

    @ContributesAndroidInjector
    abstract ChangeGroupLocationView contributeView();

    @Binds
    abstract IChangeGroupLocationPresenter groupLocationPresenter(ChangeGroupLocationPresenter changeGroupLocationPresenter);
}
