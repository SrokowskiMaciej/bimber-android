package com.bimber.features.chat.iteminteraction;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialcab.MaterialCab;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.ShareProvider;
import com.bimber.base.activities.BackPressedListener;
import com.bimber.base.activities.BackPressedManager;
import com.bimber.base.application.BimberApplication;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.data.entities.chat.messages.common.text.TextContent;
import com.bimber.domain.model.AggregatedMessage;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.sharereceiver.ShareReceiverActivity;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by maciek on 10.03.17.
 */

public class ActionModeManager implements BackPressedListener {

    private static final String COPY_ACTION_CLIPBOARD_LABEL = "Bimber conversation";
    private static final String SHARE_MIME_TYPE_TEXT_PLAIN = "text/plain";

    private static final String MESSAGE_FORMAT = "[%s] %s: %s";
    private static final String MESSAGE_FORMAT_NEW_LINE = "\n";

    private final String currentUserId;
    private final AppCompatActivity activity;
    private final DefaultValues defaultValues;

    private final MaterialCab materialCab;

    private final PublishSubject<RxFirebaseUtils.Event> clearSelectionPublishSubject = PublishSubject.create();
    private final PublishSubject<Message> textMessageDeletedPublishSubject = PublishSubject.create();
    private final PublishSubject<Message> imageMessageDeletedPublishSubject = PublishSubject.create();

    public ActionModeManager(String currentUserId, AppCompatActivity activity, ActionModePlaceHolderProvider
            actionModePlaceHolderProvider, BackPressedManager backPressedManager, DefaultValues defaultValues) {
        this.currentUserId = currentUserId;
        this.activity = activity;
        this.defaultValues = defaultValues;
        materialCab = new MaterialCab(activity, actionModePlaceHolderProvider.provideActionModePlaceholder());
        backPressedManager.registerBackPressedListener(this);
    }


    public void handleSelectedMessages(List<AggregatedMessage> selectedMessages) {
        if (selectedMessages.isEmpty()) {
            materialCab.finish();
        } else {
            materialCab
                    .reset()
                    .setMenu(R.menu.menu_chat_message_action)
                    .setCloseDrawableRes(R.drawable.ic_close_white_24dp)
                    .start(new MaterialCab.Callback() {
                        @Override
                        public boolean onCabCreated(MaterialCab cab, Menu menu) {
                            materialCab.setTitle(String.valueOf(selectedMessages.size()));
                            removeInvalidMenuItems(menu, selectedMessages);
                            if (!cab.isActive()) {
                                cab.getToolbar().setAlpha(0.0f);
                                cab.getToolbar().setVisibility(View.VISIBLE);
                                cab.getToolbar().animate()
                                        .alpha(1.0f)
                                        .setDuration(350)
                                        .setInterpolator(new FastOutSlowInInterpolator())
                                        .start();
                            }
                            return true;
                        }

                        @Override
                        public boolean onCabItemClicked(MenuItem item) {
                            handleAction(item, selectedMessages);
                            materialCab.finish();
                            return true;
                        }

                        @Override
                        public boolean onCabFinished(MaterialCab cab) {
                            clearSelectionPublishSubject.onNext(RxFirebaseUtils.Event.INSTANCE);
                            cab.getToolbar().setAlpha(1.0f);
                            cab.getToolbar().animate()
                                    .alpha(0.0f)
                                    .setDuration(350)
                                    .setInterpolator(new FastOutSlowInInterpolator())
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            cab.getToolbar().animate().setListener(null);
                                            cab.start(null).finish();
                                        }
                                    }).start();
                            return false;
                        }
                    });
        }
    }

    public PublishSubject<RxFirebaseUtils.Event> onClearSelection() {
        return clearSelectionPublishSubject;
    }

    public PublishSubject<Message> onTextMessageDeleted() {
        return textMessageDeletedPublishSubject;
    }

    public PublishSubject<Message> onImageMessageDeleted() {
        return imageMessageDeletedPublishSubject;
    }

    private void removeInvalidMenuItems(Menu menu, List<AggregatedMessage> messsages) {
        Set<SelectionActions> actionsToRemove = new HashSet<>();
        if (messsages.size() > 1) {
            actionsToRemove.add(SelectionActions.VIEW_DETAILS);
        }
        for (AggregatedMessage message : messsages) {
            switch (message.message().getContentType()) {
                case IMAGE:
                    actionsToRemove.add(SelectionActions.COPY);
                    if (messsages.size() > 1) {
                        actionsToRemove.add(SelectionActions.SHARE);
                        actionsToRemove.add(SelectionActions.FORWARD);
                    }
                    break;
                case REMOVED:
                    actionsToRemove.add(SelectionActions.REMOVE);
                    actionsToRemove.add(SelectionActions.COPY);
                    break;
                case TEXT:
                    break;
                case INITIAL_MESSAGE:
                case PARTICIPATION_PERSON_ADDED:
                case PARTICIPATION_PERSON_REMOVED:
                case PARTICIPATION_PERSON_LEFT:
                case GROUP_TIME_SET:
                case GROUP_LOCATION_SET:
                case GROUP_NAME_SET:
                case GROUP_DESCRIPTION_SET:
                case UNKNOWN:
                    actionsToRemove.addAll(Arrays.asList(SelectionActions.values()));
                    break;
            }
            if (!message.message().getUserId().equals(currentUserId)) {
                actionsToRemove.add(SelectionActions.REMOVE);
            }
        }
        for (SelectionActions selectionAction : actionsToRemove) {
            switch (selectionAction) {
                case REMOVE:
                    menu.removeItem(R.id.message_action_remove);
                    break;
                case COPY:
                    menu.removeItem(R.id.message_action_copy_text);
                    break;
                case VIEW_DETAILS:
                    menu.removeItem(R.id.message_action_view_details);
                    break;
                case SHARE:
                    menu.removeItem(R.id.message_action_share);
                case FORWARD:
                    menu.removeItem(R.id.message_action_forward);
                    break;
            }
        }
    }

    private void handleAction(MenuItem menuItem, List<AggregatedMessage> messages) {
        switch (menuItem.getItemId()) {
            case R.id.message_action_remove:
                handleRemoveAction(messages);
                break;
            case R.id.message_action_copy_text:
                handleCopyAction(messages);
                break;
            case R.id.message_action_view_details:
                handleViewDetailsAction(messages);
                break;
            case R.id.message_action_share:
                handleShareAction(messages, false);
                break;
            case R.id.message_action_forward:
                handleShareAction(messages, true);
                break;
        }

    }

    private void handleRemoveAction(List<AggregatedMessage> messages) {
        for (AggregatedMessage message : messages) {
            switch (message.message().getContentType()) {
                case IMAGE:
                    imageMessageDeletedPublishSubject.onNext(message.message());
                    break;
                case TEXT:
                    textMessageDeletedPublishSubject.onNext(message.message());
                    break;
            }

        }
    }

    private void handleCopyAction(List<AggregatedMessage> messages) {
        String formattedMessages = formatSelectedMessages(messages);
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(COPY_ACTION_CLIPBOARD_LABEL, formattedMessages);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(activity, R.string.view_chat_messages_copy_action_toast_text, Toast.LENGTH_SHORT).show();
    }

    private void handleViewDetailsAction(List<AggregatedMessage> messages) {
        if (messages.size() == 1) {
            AggregatedMessage message = messages.get(0);
            ProfileDataThumbnail userProfileData = message.sendingUserProfile();

            Drawable infoIcon = ContextCompat.getDrawable(activity, R.drawable.ic_info_outline_white_24dp).mutate();
            DrawableCompat.setTint(infoIcon, ContextCompat.getColor(activity, R.color.colorPrimary));

            String dateTime = DateFormat.getDateTimeInstance().format(new Date(message.message().getTimestamp()));
            String dateTimeItem = String.format(activity.getString(R.string.view_chat_action_details_date_time_format), dateTime);

            String sentByItem = String.format(activity.getString(R.string.view_chat_action_details_sent_by_format), userProfileData.user.displayName);

            new MaterialDialog.Builder(activity)
                    .icon(infoIcon)
                    .typeface(BimberApplication.getDefaultTypeface(), BimberApplication.getDefaultTypeface())
                    .buttonsGravity(GravityEnum.CENTER)
                    .title(R.string.view_chat_action_details_dialog_title)
                    .items(Arrays.asList(new String[]{dateTimeItem, sentByItem}))
                    .neutralText(R.string.view_chat_action_details_neutral_button_text)
                    .show();
        } else {
            //TODO
        }
    }


    private void handleShareAction(List<AggregatedMessage> messages, boolean forwardMode) {
        if (messages.size() == 1 && messages.get(0).message().getContentType() == ContentType.IMAGE) {
            ImageContent imageContent = (ImageContent) messages.get(0).message().getContent();
            prepareImageForSharing(imageContent)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(uri -> {
                        Intent shareIntent = ShareCompat.IntentBuilder.from(activity)
                                .setStream(uri)
                                .getIntent();

                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        shareIntent.setType(ShareProvider.SHARE_MIME_TYPE_IMAGE_JPEG);
                        if (forwardMode) {
                            shareIntent.setClass(activity.getApplicationContext(), ShareReceiverActivity.class);
                        }
                        activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string
                                .share_action_title)));
                    }, throwable -> {
                        Timber.e(throwable, "Couldn't share image");
                        Toast.makeText(activity, "Couldn't share image", Toast.LENGTH_LONG).show();
                    });
        } else if (messages.size() > 0) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, formatSelectedMessages(messages));
            shareIntent.setType(SHARE_MIME_TYPE_TEXT_PLAIN);
            if (forwardMode) {
                shareIntent.setClass(activity.getApplicationContext(), ShareReceiverActivity.class);
            }
            activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string
                    .share_action_title)));
        }
    }

    private Single<Uri> prepareImageForSharing(ImageContent imageContent) {
        return Single.defer(() -> Single.just(imageContent))
                .subscribeOn(Schedulers.io())
                .flatMap(content -> {
                    try {
                        return Single.just(Glide.with(activity)
                                .load(imageContent.uri())
                                .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                .get());
                    } catch (InterruptedException | ExecutionException e) {
                        throw Exceptions.propagate(e);
                    }
                })
                .flatMap(file -> Single.just(FileProvider.getUriForFile(activity,
                        activity.getString(R.string.file_prvider_authority), file)));
    }

    private String formatSelectedMessages(List<AggregatedMessage> messages) {
        StringBuilder stringBuilder = new StringBuilder();
        for (AggregatedMessage message : messages) {
            if (message.message().getContentType() == ContentType.TEXT) {
                stringBuilder.append(formatTextMessage(message));
                stringBuilder.append(MESSAGE_FORMAT_NEW_LINE);
            }
        }
        return stringBuilder.toString();
    }

    private String formatTextMessage(AggregatedMessage textMessage) {
        String date = DateUtils.formatDateTime(activity, textMessage.message().getTimestamp(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
        ProfileDataThumbnail userProfileData = textMessage.sendingUserProfile();
        String textContent = ((TextContent) textMessage.message().getContent()).text();
        return String.format(MESSAGE_FORMAT, date, userProfileData.user.displayName, textContent);
    }

    @Override
    public String key() {
        return getClass().getSimpleName();
    }

    @Override
    public boolean shouldBackButtonProceed() {
        if (materialCab.isActive()) {
            materialCab.finish();
            return false;
        } else {
            return true;
        }
    }
}
