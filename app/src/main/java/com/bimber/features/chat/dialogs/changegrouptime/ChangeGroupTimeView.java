package com.bimber.features.chat.dialogs.changegrouptime;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.group.BaseGroupFragment;
import com.bimber.base.group.GroupProvider;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeContract.IChangeGroupTimePresenter;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeContract.IChangeGroupTimeView;
import com.bimber.features.creategroup.steps.time.Date;
import com.bimber.features.creategroup.steps.time.Time;
import com.bimber.utils.firebase.Timestamp;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import javax.inject.Inject;

import icepick.State;

/**
 * Created by maciek on 05.05.17.
 */
@FragmentWithArgs
public class ChangeGroupTimeView extends BaseGroupFragment implements IChangeGroupTimeView, DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, DialogInterface.OnCancelListener{

    private static final String DATE_PICKER_DIALOG = "DATE_PICKER_DIALOG";
    private static final String TIME_PICKER_DIALOG = "TIME_PICKER_DIALOG";

    @Inject
    IChangeGroupTimePresenter changeGroupTimePresenter;
    @State
    Date date;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        changeGroupTimePresenter.bindView(this);
        DatePickerDialog datePickerDialog = (DatePickerDialog) getChildFragmentManager().findFragmentByTag(DATE_PICKER_DIALOG);
        TimePickerDialog timePickerDialog = (TimePickerDialog) getChildFragmentManager().findFragmentByTag(TIME_PICKER_DIALOG);

        if (datePickerDialog != null) {
            datePickerDialog.setOnDateSetListener(this);
            datePickerDialog.setOnCancelListener(this);
        } else if (date == null) {
            Calendar now = Calendar.getInstance();
            datePickerDialog = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setMinDate(now);
            datePickerDialog.setOnCancelListener(this);
            datePickerDialog.show(getChildFragmentManager(), DATE_PICKER_DIALOG);

        } else if (timePickerDialog != null) {
            timePickerDialog.setOnTimeSetListener(this);
            timePickerDialog.setOnCancelListener(this);
        } else {
            Calendar now = Calendar.getInstance();
            timePickerDialog = TimePickerDialog.newInstance(this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true);
            timePickerDialog.setOnCancelListener(this);
            timePickerDialog.show(getChildFragmentManager(), TIME_PICKER_DIALOG);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        changeGroupTimePresenter.unbindView(this);
        super.onDestroyView();
    }

    @Override
    public void showChangeGroupMeetingTimeFailure() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_error_title_default_text)
                .content(R.string.view_error_content_error_flow_text)
                .neutralText(R.string.view_error_action_ok_text)
                .dismissListener(dialog1 -> close())
                .show();
    }

    @Override
    public void close() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = Date.create(year, monthOfYear, dayOfMonth);
        Calendar now = Calendar.getInstance();
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true);
        timePickerDialog.setOnCancelListener(this);
        timePickerDialog.show(getChildFragmentManager(), TIME_PICKER_DIALOG);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        Time time = Time.create(hourOfDay, minute, second);
        Calendar calendar = Calendar.getInstance();
        calendar.set(date.year(), date.monthOfYear(), date.dayOfMonth(), time.hourOfDay(), time.minute(), time.second());
        Timestamp timestamp = Timestamp.create(calendar.getTimeInMillis());
        changeGroupTimePresenter.changeGroupMeetingTime(groupId, timestamp);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        close();
    }
}
