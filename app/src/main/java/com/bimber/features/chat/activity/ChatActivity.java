package com.bimber.features.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.features.chat.ChatView;
import com.bimber.features.chat.iteminteraction.ActionModeManager;
import com.bimber.features.chat.iteminteraction.ActionModePlaceHolderProvider;

import javax.inject.Inject;

/**
 * Created by maciek on 02.03.17.
 */

public class ChatActivity extends BaseLoggedInActivity implements ActionModePlaceHolderProvider {

    private static final String CHAT_ID_ARG = "CHAT_ID_ARG";
    private static final String CHAT_TYPE_ARG = "CHAT_TYPE_ARG";
    public static final String SHARE_INTENT_KEY = "SHARE_INTENT_KEY";
    public static final String CHAT_VIEW_FRAGEMNT_TAG = "CHAT_VIEW_FRAGEMNT_TAG";

    @Inject
    ActionModeManager actionModeManager;


    public static Intent newInstance(Context context, String chatId, ChatType chatType) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(CHAT_ID_ARG, chatId);
        intent.putExtra(CHAT_TYPE_ARG, chatType.toString());
        return intent;
    }

    public static Intent newInstance(Context context, String chatId, ChatType chatType, Intent shareIntent) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(CHAT_ID_ARG, chatId);
        intent.putExtra(CHAT_TYPE_ARG, chatType.toString());
        intent.putExtra(SHARE_INTENT_KEY, shareIntent);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementsUseOverlay(false);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String chatId = getIntent().getStringExtra(CHAT_ID_ARG);
        ChatType chatType = ChatType.valueOf(getIntent().getStringExtra(CHAT_TYPE_ARG));
        if (getSupportFragmentManager().findFragmentByTag(CHAT_VIEW_FRAGEMNT_TAG) == null) {
            ChatView chatView;
            if (getIntent().getExtras().containsKey(SHARE_INTENT_KEY)) {
                chatView = ChatView.newInstanceWithShare(chatId, chatType, getIntent().getParcelableExtra(SHARE_INTENT_KEY));
            } else {
                chatView = ChatView.newInstance(chatId, chatType);
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, chatView, CHAT_VIEW_FRAGEMNT_TAG)
                    .commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (isFinishing()) return;
        String chatId = intent.getStringExtra(CHAT_ID_ARG);
        ChatType chatType = ChatType.valueOf(intent.getStringExtra(CHAT_TYPE_ARG));
        ChatView chatView;
        if (intent.getExtras().containsKey(SHARE_INTENT_KEY)) {
            chatView = ChatView.newInstanceWithShare(chatId, chatType, intent.getParcelableExtra(SHARE_INTENT_KEY));
        } else {
            chatView = ChatView.newInstance(chatId, chatType);
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, chatView, CHAT_VIEW_FRAGEMNT_TAG)
                .commit();
    }

    @Override
    public int provideActionModePlaceholder() {
        return R.id.viewStubCab;
    }
}
