package com.bimber.features.chat.dialogs.managegroupusers;

import com.bimber.domain.model.ChatMemberData;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 11.05.17.
 */
@AutoValue
public abstract class ManagedChatMember {


    public abstract ChatMemberData chatMember();

    public abstract ChatRole chatRole();

    public abstract boolean availableToChange();

    public static ManagedChatMember create(ChatMemberData chatMember, String currentUserId, String groupOwnerId, boolean isPartySecure) {
        String uId = chatMember.profileData().user.uId;
        if (uId.equals(currentUserId)) {
            return new AutoValue_ManagedChatMember(chatMember, ChatRole.CURRENT_USER, false);
        } else if (uId.equals(groupOwnerId)) {
            return new AutoValue_ManagedChatMember(chatMember, ChatRole.OWNER, false);
        } else {
            return new AutoValue_ManagedChatMember(chatMember, ChatRole.USER, !isPartySecure || groupOwnerId.equals(currentUserId));
        }
    }

    public enum ChatRole {
        USER,
        CURRENT_USER,
        OWNER
    }
}
