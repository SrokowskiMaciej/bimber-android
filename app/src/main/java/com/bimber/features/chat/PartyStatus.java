package com.bimber.features.chat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import com.bimber.R;
import com.bimber.data.entities.Group;

/**
 * Created by maciek on 31.07.17.
 */

public enum PartyStatus {
    UPCOMING(R.drawable.ic_date_range_white_24dp, R.color.colorPartyUpcoming, R.string.view_chat_discovery_status_upcoming, R.string
            .view_chat_discovery_status_upcoming_explanation),
    ACTIVE(R.drawable.ic_date_range_white_24dp, R.color.colorPartyActive, R.string.view_chat_discovery_status_active, R.string
            .view_chat_discovery_status_active_explanation),
    //    ACTIVE_REMINDER_SENT,
    ONGOING(R.drawable.ic_stars_white_24dp, R.color.colorPartyActive, R.string.view_chat_discovery_status_ongoing, R.string
            .view_chat_discovery_status_ongoing_explanation),
    EXPIRED(R.drawable.ic_event_busy_white_24dp, R.color.colorPartyExpired, R.string.view_chat_discovery_status_expired, R.string
            .view_chat_discovery_status_expired_explanation);

    @DrawableRes
    private final int icon;
    @ColorRes
    private final int color;

    @StringRes
    private final int name;

    @StringRes
    private final int explanation;

    PartyStatus(int icon, int color, int name, int explanation) {
        this.icon = icon;
        this.color = color;
        this.name = name;
        this.explanation = explanation;
    }

    public static PartyStatus from(Group group) {
        if (group.discoverable()) {
            if (group.meetingNoticeSent()) {
                return ONGOING;
            } else {
//                if (group.meetingReminderSent()) {
//                    return ACTIVE_REMINDER_SENT;
//                } else {
                return ACTIVE;
//                }
            }
        } else {
            if (group.deactivated()) {
                return EXPIRED;
            } else {
                return UPCOMING;
            }
        }
    }

    @ColorRes
    public int getStateColor() {
        return color;
    }

    public Drawable getIcon(Context context) {
        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), icon, context.getTheme());
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, color));
        return drawable;
    }

    public String getStateName(Context context) {
        return context.getString(name);
    }

    public CharSequence getStateExplanation(Context context) {
        String stateName = getStateName(context);
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(String.format(context.getString(explanation),
                stateName));
        stringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, stateName.length(), Spannable
                .SPAN_EXCLUSIVE_EXCLUSIVE);
        return stringBuilder;
    }
}
