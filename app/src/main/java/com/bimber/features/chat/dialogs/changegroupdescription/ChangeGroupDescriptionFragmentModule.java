package com.bimber.features.chat.dialogs.changegroupdescription;

import com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionContract.IChangeGroupDescriptionPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 05.05.17.
 */

@Module
public abstract class ChangeGroupDescriptionFragmentModule {

    @ContributesAndroidInjector
    abstract ChangeGroupDescriptionView contributeView();

    @Binds
    abstract IChangeGroupDescriptionPresenter groupDescriptionPresenter(ChangeGroupDescriptionPresenter changeGroupDescriptionPresenter);
}
