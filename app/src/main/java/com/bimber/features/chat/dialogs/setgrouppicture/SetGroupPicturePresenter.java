package com.bimber.features.chat.dialogs.setgrouppicture;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.domain.group.SetGroupPictureUseCase;
import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureContract.ISetGroupPicturePresenter;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 05.05.17.
 */

public class SetGroupPicturePresenter extends MvpAbstractPresenter<SetGroupPictureContract.ISetGroupPictureView> implements
        ISetGroupPicturePresenter {


    private final String currentUserId;
    private final DbConnectionService dbConnectionService;
    private final SetGroupPictureUseCase setGroupPictureUseCase;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public SetGroupPicturePresenter(@LoggedInUserId String currentUserId, DbConnectionService dbConnectionService, SetGroupPictureUseCase
            setGroupPictureUseCase) {
        this.currentUserId = currentUserId;
        this.dbConnectionService = dbConnectionService;
        this.setGroupPictureUseCase = setGroupPictureUseCase;
    }

    @Override
    protected void viewAttached(SetGroupPictureContract.ISetGroupPictureView view) {

    }

    @Override
    protected void viewDetached(SetGroupPictureContract.ISetGroupPictureView view) {
        subscriptions.clear();
    }

    @Override
    public void setPicture(String groupId, File file) {
        subscriptions.add(dbConnectionService.connectedThrow()
                .doOnSubscribe(disposable -> getView().showProgressBar(true))
                .andThen(setGroupPictureUseCase.setGroupPicture(currentUserId, groupId, file))
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().close(),
                        throwable -> {
                            Timber.e(throwable, "Failed to set group picture");
                            getView().showProgressBar(false);
                            getView().showSetGroupPictureFailure(true);
                        }));
    }
}
