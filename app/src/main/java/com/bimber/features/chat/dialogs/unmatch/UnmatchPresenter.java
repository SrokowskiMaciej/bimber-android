package com.bimber.features.chat.dialogs.unmatch;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.chat.ChatId;
import com.bimber.data.repositories.DbConnectionService;
import com.bimber.domain.matching.UnmatchUserUseCase;
import com.bimber.features.chat.dialogs.unmatch.UnmatchContract.IUnmatchPresenter;
import com.bimber.features.chat.dialogs.unmatch.UnmatchContract.IUnmatchView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by maciek on 05.05.17.
 */

public class UnmatchPresenter extends MvpAbstractPresenter<IUnmatchView> implements IUnmatchPresenter {


    private final String currentUserId;
    private final String chatId;
    private final DbConnectionService dbConnectionService;
    private final UnmatchUserUseCase unmatchUserUseCase;
    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public UnmatchPresenter(@LoggedInUserId String currentUserId, @ChatId String chatId, DbConnectionService dbConnectionService,
                            UnmatchUserUseCase unmatchUserUseCase) {
        this.currentUserId = currentUserId;
        this.chatId = chatId;
        this.dbConnectionService = dbConnectionService;
        this.unmatchUserUseCase = unmatchUserUseCase;
    }


    @Override
    public void leaveGroup() {
        subscriptions.add(dbConnectionService.connectedThrow()
                .andThen(unmatchUserUseCase.unmatchUserFromDialog(currentUserId, chatId))
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .subscribe(() -> getView().close(), throwable -> getView().showLeaveGroupFailure()));
    }

    @Override
    protected void viewAttached(IUnmatchView view) {

    }

    @Override
    protected void viewDetached(IUnmatchView view) {
        subscriptions.clear();
    }
}
