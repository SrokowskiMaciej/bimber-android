package com.bimber.features.chat.dialogs.changegrouptime;

import com.bimber.base.group.GroupModule;
import com.bimber.base.group.GroupProvider;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 31.07.17.
 */
@Module
public abstract class ChangeGroupTimeStandaloneModule {
    @ContributesAndroidInjector
    abstract ChangeGroupTimeView contributeView();

    @Binds
    abstract ChangeGroupTimeContract.IChangeGroupTimePresenter groupTimePresenter(ChangeGroupTimePresenter changeGroupTimePresenter);

}
