package com.bimber.features.userprofile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.application.BimberApplication;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.features.editprofile.EditProfileView;
import com.bimber.features.settings.SettingsView;
import com.bimber.features.share.profile.ShareProfileView;
import com.bimber.features.userprofile.UserProfileContract.IUserProfilePresenter;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by srokowski.maciej@gmail.com on 25.10.16.
 */

public class UserProfileView extends BaseFragment implements UserProfileContract.IUserProfileView {

    public static final String SHARE_PROFILE_VIEW_TAG = "SHARE_PROFILE_VIEW_TAG";
    public static final String SETTINGS_TAG = "SETTINGS_TAG";
    public static final String EDIT_PROFILE_TAG = "EDIT_PROFILE_TAG";
    @Inject
    @LoggedInUserId
    String currentUserId;
    @BindView(R.id.content)
    NestedScrollView content;
    @BindView(R.id.appBarView)
    AppBarLayout appBarView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageViewProfilePhoto)
    ImageView imageViewProfilePhoto;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.textViewFriendsCount)
    TextView textViewFriendsCount;
    @BindView(R.id.textViewPartiesCount)
    TextView textViewPartiesCount;
    @BindView(R.id.rootView)
    CoordinatorLayout rootView;


    @Inject
    IUserProfilePresenter userProfilePresenter;

    private boolean settingsMode;
    private String userName = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_user_profile, container, false);
        ButterKnife.bind(this, view);

        if (getChildFragmentManager().findFragmentByTag(EDIT_PROFILE_TAG) == null) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.content, new EditProfileView(), EDIT_PROFILE_TAG)
                    .commit();
            settingsMode = false;
            getActivity().invalidateOptionsMenu();
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar();
        userProfilePresenter.bindView(this);
        userProfilePresenter.requestProfileData(currentUserId);
    }

    @Override
    public void onDestroyView() {
        userProfilePresenter.unbindView(this);
        super.onDestroyView();
    }

    @OnClick({R.id.buttonShare})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonShare:
                getFragmentManager().beginTransaction().add(ShareProfileView.newInstance(currentUserId, R.style.AppTheme),
                        SHARE_PROFILE_VIEW_TAG).commit();

                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_user_profile, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (settingsMode) {
            menu.removeItem(R.id.item_settings);
        } else {
            menu.removeItem(R.id.item_edit_profile);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_settings:
                startSettingsScreen();
                settingsMode = true;
                getActivity().invalidateOptionsMenu();
                break;
            case R.id.item_edit_profile:
                startProfileEditScreen();
                settingsMode = false;
                getActivity().invalidateOptionsMenu();
                break;
            case R.id.change_user_name:
                new MaterialDialog.Builder(getContext())
                        .title(R.string.user_profile_change_name_dialog_title)
                        .input(getString(R.string.user_profile_change_name_dialog_hint), collapsingToolbar.getTitle(), false, (dialog, input) -> {
                            userProfilePresenter.changeUserName(currentUserId, input.toString());
                        })
                        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE | InputType
                                .TYPE_TEXT_FLAG_CAP_SENTENCES)
                        .negativeText(R.string.dialog_cancel_button_text)
                        .negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                        .positiveText(R.string.dialog_ok_button_text)
                        .positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                        .show();

                break;
        }
        return true;
    }


    public void scrollToTop() {
        if (appBarView != null && !isRemoving() && !getActivity().isFinishing()) {
            appBarView.setExpanded(true, false);
            content.scrollTo(0, 0);

        }
    }

    public void startProfileEditScreen() {
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left)
                .replace(R.id.content, new EditProfileView(), EDIT_PROFILE_TAG)
                .commit();
    }

    public void startSettingsScreen() {
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right)
                .replace(R.id.content, new SettingsView(), SETTINGS_TAG)
                .commit();
    }


    private void setupToolbar() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        activity.setSupportActionBar(toolbar);
        // Set empty title until Profile loads
        activity.setTitle("");
        final Typeface tf = Typeface.createFromAsset(activity.getAssets(), BimberApplication.DEFAULT_FONT_ASSET_PATH);
        collapsingToolbar.setCollapsedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleTypeface(tf);
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
    }

    @Override
    public void setProfilePhotoUri(String uri) {
        GlideApp.with(getContext())
                .load(uri)
                .apply(new RequestOptions().centerCrop())
//                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageViewProfilePhoto);
    }

    @Override
    public void setProfileName(String name) {
        collapsingToolbar.setTitle(name);
    }

    @Override
    public void setFriendsCount(int count) {
        textViewFriendsCount.setText(String.format(getContext().getResources()
                .getQuantityString(R.plurals.profile_friends_count, count), count));
    }

    @Override
    public void setPartiesCount(int count) {
        textViewPartiesCount.setText(String.format(getContext().getResources()
                .getQuantityString(R.plurals.profile_parties_count, count), count));
    }

    @Override
    public void showErrorScreen() {
        //TODO
    }
}
