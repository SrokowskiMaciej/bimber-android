package com.bimber.features.userprofile;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */
@Module
public abstract class UserProfileFragmentModule {

    @ContributesAndroidInjector
    abstract UserProfileView contributeView();

    @Binds
    abstract UserProfileContract.IUserProfilePresenter userProfilePresenter(UserProfilePresenter userProfilePresenter);
}
