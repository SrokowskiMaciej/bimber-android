package com.bimber.features.userprofile;

import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

public interface UserProfileContract {

    interface IUserProfileView {

        void setProfilePhotoUri(String uri);

        void setProfileName(String uri);

        void setFriendsCount(int count);

        void setPartiesCount(int count);

        void showErrorScreen();
    }

    interface IUserProfilePresenter extends MvpPresenter<IUserProfileView> {

        void requestProfileData(String profileId);

        void changeUserName(String profileId, String newName);
    }
}
