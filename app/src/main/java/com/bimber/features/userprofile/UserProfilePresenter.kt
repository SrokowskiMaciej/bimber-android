package com.bimber.features.userprofile

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository
import com.bimber.data.sources.profile.user.UsersRepository
import com.bimber.features.userprofile.UserProfileContract.IUserProfilePresenter
import com.bimber.features.userprofile.UserProfileContract.IUserProfileView
import com.bimber.utils.mvp.MvpAbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 28.12.16.
 */

class UserProfilePresenter @Inject
constructor(private val usersRepository: UsersRepository, private val profileDataRepository: ProfileDataRepository, private val backgroundSchedulerProvider: BackgroundSchedulerProvider) : MvpAbstractPresenter<IUserProfileView>(), IUserProfilePresenter {
    private val subscriptions = CompositeDisposable()

    override fun viewAttached(view: IUserProfileView) {

    }

    override fun viewDetached(view: IUserProfileView) {
        subscriptions.clear()
    }

    override fun requestProfileData(profileId: String) {
        val profileDataThumbnailFlowable = profileDataRepository.onUserProfileDataThumbnail(profileId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .replay(1)
                .refCount()

        subscriptions.add(profileDataThumbnailFlowable
                .map { profileDataThumbnail -> profileDataThumbnail.user.displayName }
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.setProfileName(it) }
                ) { throwable -> Timber.e(throwable, "Error loading user profile name in toolbar") })

        subscriptions.add(profileDataThumbnailFlowable
                .map { profileDataThumbnail -> profileDataThumbnail.user.friends }
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.setFriendsCount(it) }
                ) { throwable -> Timber.e(throwable, "Error loading user friends count") })

        subscriptions.add(profileDataThumbnailFlowable
                .map { profileDataThumbnail -> profileDataThumbnail.user.parties }
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.setPartiesCount(it) }
                ) { throwable -> Timber.e(throwable, "Error loading user parties count") })

        subscriptions.add(profileDataThumbnailFlowable
                .filter { it.photo.userPhotoUri != null }
                .map { profileDataThumbnail -> profileDataThumbnail.photo.userPhotoUri }
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.setProfilePhotoUri(it) }) { throwable ->
                    Timber.e(throwable, "Error loading user profile photo in toolbar")
                    view.showErrorScreen()
                })
    }

    override fun changeUserName(profileId: String, newName: String) {
        subscriptions.add(usersRepository.updateUserName(profileId, newName)
                .subscribeBy(onError = { Timber.e(it, "Failed to change user name") }))
    }
}
