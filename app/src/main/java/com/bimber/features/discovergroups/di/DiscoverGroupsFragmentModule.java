package com.bimber.features.discovergroups.di;

import com.bimber.features.discovergroups.DiscoverGroupsContract;
import com.bimber.features.discovergroups.DiscoverGroupsPresenter;
import com.bimber.features.discovergroups.DiscoverGroupsView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */
@Module
public abstract class DiscoverGroupsFragmentModule {

    @Binds
    abstract DiscoverGroupsContract.IDiscoverGroupsPresenter discoverPresenter(DiscoverGroupsPresenter discoverGroupsPresenter);

    @ContributesAndroidInjector
    abstract DiscoverGroupsView contribute();
}
