package com.bimber.features.discovergroups

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.widget.TextViewCompat
import android.support.v7.util.DiffUtil
import android.support.v7.util.DiffUtil.calculateDiff
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bimber.R
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupData

import com.bimber.utils.TimeUtils
import com.bimber.utils.images.BitmapUtils
import com.bimber.utils.maps.SphericalUtil
import com.bimber.utils.view.AbstractDiffUtilCallback
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

class GroupsAdapter() : RecyclerView.Adapter<GroupsAdapter.GroupViewHolder>() {

    private var nearbyGroups = Collections.emptyList<NearbyGroupData>()
    private val groupClickEvents = PublishSubject.create<GroupClickEvent>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        return GroupViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_discover_groups_card, parent, false))
    }

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        val nearbyGroupData = nearbyGroups[position]
        val group = nearbyGroupData.groupData.group().value()
        val context: Context = holder?.textViewDistance?.context!!
        holder.nearbyGroupData = nearbyGroupData
        holder.textViewGroupInfo?.text = group.name()
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(holder.textViewGroupInfo!!, 5, 36, 2, TypedValue.COMPLEX_UNIT_DIP)
        if (group.description().isEmpty()) {
            holder.textViewGroupDescription?.setVisibility(View.GONE)
        } else {
            holder.textViewGroupDescription?.setVisibility(View.VISIBLE)
            holder.textViewGroupDescription!!.setText(String.format(context.getString(R.string.view_discover_card_quote_wrapper), group.description()))
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(holder.textViewGroupDescription!!, 5, 32, 2, TypedValue.COMPLEX_UNIT_DIP)
        }

        val referenceLocation = nearbyGroupData.referenceLocation
        val groupLocation = group.location()

        if (referenceLocation == null) {
            holder.textViewLocation?.setText(R.string.view_discover_card_unknown_location)
            holder.textViewDistance?.visibility = GONE
        } else {
            holder.textViewLocation?.text = groupLocation.getNormalizedPlaceName()
            val distance = SphericalUtil.computeDistanceBetween(LatLng(groupLocation.latitude(), groupLocation.longitude()),
                    LatLng(referenceLocation.latitude(), referenceLocation.longitude())) / 1000.0
            holder.textViewDistance?.visibility = VISIBLE
            holder.textViewDistance?.text = String.format(context.getString(R.string.view_discover_card_distance),
                    Math.max(MINIMUM_DISTANCE.toLong(), Math.round(distance)).toString())
        }

        val now = System.currentTimeMillis()
        val meetingTime = group.meetingTime().value()!!
        if (now < meetingTime) {
            holder.textViewTime?.text = String.format(context.getString(R.string.group_remaining_time_upcoming),
                    TimeUtils.formatDuration(context, meetingTime, now))
        } else {
            holder.textViewTime?.text = String.format(context.getString(R.string.group_remaining_time_past),
                    TimeUtils.formatDuration(context, meetingTime, now))
        }

        val membersCount = nearbyGroupData.groupData.group().value().membersCount()
        holder.textViewParticipantsNumber?.text = String.format(context.resources
                .getQuantityString(R.plurals.group_members_count, membersCount), membersCount)

        holder.background?.setBackgroundResource(nearbyGroupData.backgroundResource)
        holder.imageViewGroupPhoto?.viewTreeObserver?.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                holder.imageViewGroupPhoto?.viewTreeObserver?.removeOnPreDrawListener(this)
                holder.subscriptions.add(nearbyGroupData.groupData.photos()
                        .toList()
                        .flatMapObservable({ thumbnailedPhotos -> BitmapUtils.collage(context, thumbnailedPhotos, holder.imageViewGroupPhoto!!.width, holder.imageViewGroupPhoto!!.height) })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ bitmap ->
                            holder.imageViewGroupPhoto?.setImageBitmap(bitmap)
                        },
                                { throwable -> Timber.e(throwable, "Failed to set group image bitmap") }))
                return true
            }
        })
    }

    override fun onViewDetachedFromWindow(holder: GroupViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clear()
    }

    override fun getItemCount(): Int {
        return nearbyGroups.size
    }

    override fun getItemId(position: Int): Long {
        return nearbyGroups[position].skippedGroupId
    }

    fun setGroups(nearbyGroups: List<NearbyGroupData>) {
        val diffResult = calculateDiff(object : AbstractDiffUtilCallback<NearbyGroupData>(this.nearbyGroups, nearbyGroups) {

            override fun areItemsTheSame(oldItem: NearbyGroupData, newItem: NearbyGroupData): Boolean {
                return oldItem.skippedGroupId == newItem.skippedGroupId
            }

            override fun areContentsTheSame(oldItem: NearbyGroupData, newItem: NearbyGroupData): Boolean {
                return oldItem == newItem
            }
        })
        this.nearbyGroups = nearbyGroups
        diffResult.dispatchUpdatesTo(this)
    }

    fun clickEvents(): Observable<GroupClickEvent> {
        return groupClickEvents;
    }

    inner class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        @BindView(R.id.imageViewGroupPhoto)
        @JvmField
        public var imageViewGroupPhoto: ImageView? = null
        @BindView(R.id.textViewGroupInfo)
        @JvmField
        public var textViewGroupInfo: TextView? = null
        @BindView(R.id.textViewLocation)
        @JvmField
        public var textViewLocation: TextView? = null
        @BindView(R.id.textViewParticipantsNumber)
        @JvmField
        public var textViewParticipantsNumber: TextView? = null
        @BindView(R.id.textViewTime)
        @JvmField
        public var textViewTime: TextView? = null
        @BindView(R.id.textViewDrink)
        @JvmField
        public var textViewDrink: TextView? = null
        @BindView(R.id.textViewPass)
        @JvmField
        public var textViewPass: TextView? = null
        @BindView(R.id.textViewSkip)
        @JvmField
        public var textViewSkip: TextView? = null
        @BindView(R.id.textViewGroupDescription)
        @JvmField
        public var textViewGroupDescription: TextView? = null
        @BindView(R.id.textViewDistance)
        @JvmField
        public var textViewDistance: TextView? = null
        @BindView(R.id.background)
        @JvmField
        public var background: ConstraintLayout? = null

        var nearbyGroupData: NearbyGroupData? = null
        val subscriptions = CompositeDisposable()

        init {
            ButterKnife.bind(this, itemView)
            background!!.setOnClickListener(this)
        }


        override fun onClick(view: View) {
            nearbyGroupData?.let { groupClickEvents.onNext(GroupClickEvent(this, it)) }
        }

        fun clear() {
            subscriptions.clear();
        }
    }


    companion object {
        val MINIMUM_DISTANCE = 1
    }

    data class GroupClickEvent(val viewHolder: GroupViewHolder, val groupData: NearbyGroupData)

}
