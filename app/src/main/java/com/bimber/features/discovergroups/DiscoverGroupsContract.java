package com.bimber.features.discovergroups;

import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupData;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

public interface DiscoverGroupsContract {
    interface IDiscoverGroupsView {
        void showModelData(DiscoverGroupsPresenter.DiscoverGroupsModel model);
    }

    interface IDiscoverGroupsPresenter extends MvpPresenter<IDiscoverGroupsView> {
        void likeGroup(NearbyGroupData group);

        void dislikeGroup(NearbyGroupData group);

        void skipGroup(NearbyGroupData group);

        void deepLinkHandled();
    }
}
