package com.bimber.features.discovergroups

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.afollestad.materialdialogs.MaterialDialog
import com.bimber.R
import com.bimber.base.BitmapPool.DrawablePool
import com.bimber.base.fragments.BaseFragment
import com.bimber.data.entities.Chattable
import com.bimber.features.chat.activity.ChatActivity
import com.bimber.features.discovergroups.DiscoverGroupsContract.IDiscoverGroupsPresenter
import com.bimber.features.discovergroups.DiscoverGroupsContract.IDiscoverGroupsView
import com.bimber.features.groupdetails.publicvisibility.activity.PublicGroupActivity
import com.bimber.features.home.ExploreButtonsProvider
import com.bimber.features.home.ExploreButtonsProvider.ExploreButtonConsumer
import com.bimber.features.location.activity.LocationPickerActivity
import com.bimber.utils.view.RadarView

import java.util.ArrayList

import javax.inject.Inject

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bimber.data.analytics.BimberAnalytics
import com.bimber.data.analytics.BimberAnalytics.CreatePartyEntryPoint.*
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupData
import com.bimber.features.creategroup.activity.CreatePartyInitializeActivity
import com.bimber.features.discovergroups.DiscoverGroupsPresenter.DiscoverGroupsModel
import com.bimber.features.explore.ExploreView
import com.bimber.features.home.ExploreButtonsProvider.ExploreButton
import com.bimber.features.home.ExploreButtonsProvider.ExploreButton.*
import com.bimber.utils.view.swipe.StackLayoutManager
import com.bimber.utils.view.swipe.SwipeItemTouchHelper
import com.bimber.utils.view.swipe.SwipeItemTouchHelper.SwipeDirection.*
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by srokowski.maciej@gmail.com on 28.10.16.
 */

class DiscoverGroupsView : BaseFragment(), IDiscoverGroupsView, ExploreButtonConsumer, SwipeItemTouchHelper.OnSwipeListener {


    @BindView(R.id.recyclerView)
    lateinit var recyclerView: RecyclerView
    @BindView(R.id.discoverViewRootLayout)
    lateinit var discoverViewRootLayout: ConstraintLayout
    @BindView(R.id.imageViewNoLocation)
    lateinit var imageViewNoLocation: ImageView
    @BindView(R.id.textViewNoLocationContent)
    lateinit var textViewNoLocationContent: TextView
    @BindView(R.id.buttonAddLocation)
    lateinit var buttonAddLocation: Button
    @BindView(R.id.radarView)
    lateinit var radarView: RadarView
    @BindView(R.id.textViewRadarContent)
    lateinit var textViewRadarContent: TextView

    @Inject
    lateinit var discoverGroupsPresenter: IDiscoverGroupsPresenter
    @Inject
    lateinit var exploreButtonsProvider: ExploreButtonsProvider
    @Inject
    lateinit var drawablePool: DrawablePool
    @Inject
    lateinit var bimberAnalytics: BimberAnalytics

    private lateinit var groupsAdapter: GroupsAdapter
    private lateinit var swipeItemTouchHelper: SwipeItemTouchHelper
    private lateinit var stackLayoutManager: StackLayoutManager

    private var progressBarDialog: MaterialDialog? = null

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_discover_groups, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        groupsAdapter = GroupsAdapter()
        swipeItemTouchHelper = SwipeItemTouchHelper(SwipeItemTouchHelper.LEFT_RIGHT_BOTTOM_DIRECTIONS_PROVIDER)
        swipeItemTouchHelper.setOnSwipeListener(this)
        swipeItemTouchHelper.attachToRecyclerView(recyclerView)
        stackLayoutManager = StackLayoutManager()
        recyclerView.layoutManager = stackLayoutManager
        recyclerView.adapter = groupsAdapter
        subscriptions.add(groupsAdapter.clickEvents().subscribe({ clickEvent ->
            val groupImage = clickEvent.viewHolder.imageViewGroupPhoto!!
            val nearbyGroupData = clickEvent.groupData
            val imageUris = nearbyGroupData.groupData.photosUris().toList().blockingGet()
            val collageHash = DrawablePool.hashCollage(imageUris)
            drawablePool.putDrawable(collageHash, groupImage.drawable)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, groupImage, collageHash)
            startActivity(PublicGroupActivity.newInstance(context, nearbyGroupData.groupData.group().key(),
                    imageUris as ArrayList<String>), options.toBundle())
        }))
        discoverGroupsPresenter.bindView(this)
    }

    override fun onStart() {
        super.onStart()
        exploreButtonsProvider.validateExploreButtons()

    }

    override fun onDestroyView() {
        subscriptions.clear()
        discoverGroupsPresenter.unbindView(this)
        super.onDestroyView()
    }


    override fun showModelData(model: DiscoverGroupsModel) {
        when (model) {
            is DiscoverGroupsModel.DeepLinkMemberGroupModel -> showGroupChatActivity(model.groupId)
            is DiscoverGroupsModel.DeepLinkErrorModel -> showDeepLinkGroupError()
            is DiscoverGroupsModel.ErrorModel -> showError()
        }
        showNearbyGroups(model.nearbyGroups)
        showNoLocationsWarning(model.showNoLocation)
        showProcessing(model.showProcessing)
        exploreButtonsProvider.validateExploreButtons()
        if (skipButtonsAvailable()) {
            swipeItemTouchHelper.setSwipeDirectionsProvider(SwipeItemTouchHelper.LEFT_RIGHT_BOTTOM_DIRECTIONS_PROVIDER)
        } else{
            swipeItemTouchHelper.setSwipeDirectionsProvider(SwipeItemTouchHelper.LEFT_RIGHT_DIRECTIONS_PROVIDER)
        }
    }


    private fun showNearbyGroups(nearbyGroups: List<NearbyGroupData>) {
        groupsAdapter.setGroups(nearbyGroups)
        exploreButtonsProvider.validateExploreButtons()
        if (nearbyGroups.isNotEmpty()) {
            recyclerView.visibility = VISIBLE
        } else {
            recyclerView.visibility = GONE

        }
        val exploreView: ExploreView? = parentFragment as? ExploreView
        exploreView?.setBadgeGroupCount(nearbyGroups.size)
    }

    private fun showNoLocationsWarning(show: Boolean) {
        if (show) {
            imageViewNoLocation.visibility = View.VISIBLE
            textViewNoLocationContent.visibility = View.VISIBLE
            buttonAddLocation.visibility = View.VISIBLE
            radarView.visibility = View.GONE
            textViewRadarContent.visibility = View.GONE
        } else {
            imageViewNoLocation.visibility = View.GONE
            textViewNoLocationContent.visibility = View.GONE
            buttonAddLocation.visibility = View.GONE
            radarView.visibility = View.VISIBLE
            textViewRadarContent.visibility = View.VISIBLE
        }
        exploreButtonsProvider.validateExploreButtons()
    }

    private fun showProcessing(show: Boolean) {
        if (show && progressBarDialog != null) {
            progressBarDialog = MaterialDialog.Builder(context!!)
                    .progress(true, 0)
                    .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                    .cancelable(false)
                    .build()
            progressBarDialog!!.show()
        } else if (progressBarDialog != null) {
            progressBarDialog!!.dismiss()
            progressBarDialog = null;
        }
    }

    private fun showGroupChatActivity(groupId: String) {
        discoverGroupsPresenter.deepLinkHandled()
        startActivity(ChatActivity.newInstance(context, groupId, Chattable.ChatType.GROUP))
    }

    private fun showDeepLinkGroupError() {
        Toast.makeText(context, "Uuuuuuuu, we couldn't find this party", Toast.LENGTH_LONG).show()
    }

    private fun showError() {
        Toast.makeText(context, "Error, we're sorry", Toast.LENGTH_LONG).show()
    }

    override fun shouldExploreButtonBeVisible(exploreButton: ExploreButton): Boolean {
        return when (exploreButton) {
            LIKE -> ratingButtonsAvailable()
            DISLIKE -> ratingButtonsAvailable()
            ADD -> true
            REVERT -> false
            SKIP -> skipButtonsAvailable()
        }
    }

    override fun onClickExploreButton(exploreButton: ExploreButton, button: View) {
        when {
            exploreButton == LIKE && ratingButtonsAvailable() -> swipeItemTouchHelper.swipe(RIGHT)
            exploreButton == DISLIKE && ratingButtonsAvailable() -> swipeItemTouchHelper.swipe(LEFT)
            exploreButton == ADD -> {
                bimberAnalytics.logPartyCreationInit(DISCOVER_PARTIES)
                val clipRevealAnimation = ActivityOptionsCompat.makeClipRevealAnimation(button, 0, 0, button.width, button.height)
                activity!!.startActivity(CreatePartyInitializeActivity.newInstance(activity!!), clipRevealAnimation.toBundle())
            }
            exploreButton == SKIP && skipButtonsAvailable() -> {
                swipeItemTouchHelper.swipe(BOTTOM)
            }
        }
    }

    private fun ratingButtonsAvailable() =
            groupsAdapter != null && recyclerView != null && groupsAdapter.itemCount > 0

    private fun skipButtonsAvailable() =
            groupsAdapter != null && recyclerView != null && groupsAdapter.itemCount > 1


    @OnClick(R.id.buttonAddLocation)
    fun onViewClicked() {
        startActivity(LocationPickerActivity.newInstance(context, R.style.AppThemeLight, R.style.AppTheme, false))
    }


    override fun onSwipeProgress(viewHolder: RecyclerView.ViewHolder?, swipeProgress: Float) {
    }

    override fun onSwipeProgressLeft(viewHolder: RecyclerView.ViewHolder?, swipeProgress: Float) {
        val groupViewHolder = viewHolder as GroupsAdapter.GroupViewHolder
        groupViewHolder.textViewPass!!.alpha = swipeProgress
    }

    override fun onSwipeProgressTop(viewHolder: RecyclerView.ViewHolder?, swipeProgress: Float) {
    }

    override fun onSwipeProgressRight(viewHolder: RecyclerView.ViewHolder?, swipeProgress: Float) {
        val groupViewHolder = viewHolder as GroupsAdapter.GroupViewHolder
        groupViewHolder.textViewDrink!!.alpha = swipeProgress
    }

    override fun onSwipeProgressBottom(viewHolder: RecyclerView.ViewHolder?, swipeProgress: Float) {
        val groupViewHolder = viewHolder as GroupsAdapter.GroupViewHolder
        groupViewHolder.textViewSkip!!.alpha = swipeProgress
    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: SwipeItemTouchHelper.SwipeDirection) {
        val groupViewHolder = viewHolder as GroupsAdapter.GroupViewHolder
        val nearbyGroupData = groupViewHolder.nearbyGroupData
        if (nearbyGroupData != null) {
            when (swipeDirection) {
                LEFT -> discoverGroupsPresenter.dislikeGroup(nearbyGroupData)
                RIGHT -> discoverGroupsPresenter.likeGroup(nearbyGroupData)
                BOTTOM -> discoverGroupsPresenter.skipGroup(nearbyGroupData)
            }
        }
    }
}
