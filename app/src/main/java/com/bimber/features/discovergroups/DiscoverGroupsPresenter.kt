package com.bimber.features.discovergroups

import com.bimber.base.auth.LoggedInUserId
import com.bimber.data.analytics.BimberAnalytics
import com.bimber.data.analytics.BimberAnalytics.Rating.DISLIKE
import com.bimber.data.analytics.BimberAnalytics.Rating.LIKE
import com.bimber.data.analytics.BimberAnalytics.RatingContentType.PARTY
import com.bimber.data.sources.discovery.groups.GroupDeepLinkRepository
import com.bimber.data.sources.discovery.groups.SkippedGroupsRepository
import com.bimber.domain.discovery.groups.GetDeepLinkGroupDataUseCase
import com.bimber.domain.discovery.groups.GetDeepLinkGroupDataUseCase.DeepLinkGroupResult
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupData
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupsResult
import com.bimber.domain.groupjoin.RequestGroupJoinUseCase
import com.bimber.features.discovergroups.DiscoverGroupsContract.IDiscoverGroupsPresenter
import com.bimber.features.discovergroups.DiscoverGroupsContract.IDiscoverGroupsView
import com.bimber.utils.mvp.MvpAbstractPresenter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

class DiscoverGroupsPresenter @Inject
constructor(@param:LoggedInUserId private val currentUserId: String,
            private val getNearbyDiscoverableGroupsUseCase: GetNearbyDiscoverableGroupsUseCase,
            private val groupDeepLinkRepository: GroupDeepLinkRepository,
            private val skippedGroupsRepository: SkippedGroupsRepository,
            private val getDeepLinkGroupDataUseCase: GetDeepLinkGroupDataUseCase,
            private val requestGroupJoinUseCase: RequestGroupJoinUseCase,
            private val bimberAnalytics: BimberAnalytics) : MvpAbstractPresenter<IDiscoverGroupsView>(), IDiscoverGroupsPresenter {

    private val subscriptions = CompositeDisposable()

    override fun viewAttached(view: IDiscoverGroupsView) {
        subscriptions.add(Flowable.combineLatest(getNearbyDiscoverableGroupsUseCase.onDiscoverableGroups(currentUserId)
                //TODO Check if sample needs adjustment or if it doesn't delay progress on deeplinking
                .sample(1, TimeUnit.SECONDS),
                getDeepLinkGroupDataUseCase.onDeepLinkGroup(currentUserId),
                classifyResultsAsModel())
                .onErrorReturnItem(DiscoverGroupsModel.ErrorModel())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showModelData))
    }

    private fun classifyResultsAsModel(): BiFunction<NearbyGroupsResult, DeepLinkGroupResult, DiscoverGroupsModel> {
        return BiFunction { nearbyGroupsResult, deepLinkResult ->
            val discoverGroupsModel = when (nearbyGroupsResult) {
                is NearbyGroupsResult.Success -> when (deepLinkResult) {
                    is DeepLinkGroupResult.LoadingDeepLinkGroupResult -> DiscoverGroupsModel.DeepLinkLoadingModel(false, nearbyGroupsResult.nearbyGroups)
                    is DeepLinkGroupResult.NoDeepLinkGroupResult -> DiscoverGroupsModel.NearbyGroupsModel(false, nearbyGroupsResult.nearbyGroups)
                    is DeepLinkGroupResult.AlreadyGroupMemberDeepLinkResult -> DiscoverGroupsModel.DeepLinkMemberGroupModel(deepLinkResult.groupId, false, nearbyGroupsResult.nearbyGroups)
                    is DeepLinkGroupResult.SuccessDeepLinkGroupResult -> DiscoverGroupsModel.NearbyGroupsModel(false, listOf(deepLinkResult.deepLinkGroup) + nearbyGroupsResult.nearbyGroups.filter { it.groupData.group().key() != deepLinkResult.deepLinkGroup.groupData.group().key() })
                    is DeepLinkGroupResult.ErrorDeepLinkGroupResult -> DiscoverGroupsModel.DeepLinkErrorModel(false, nearbyGroupsResult.nearbyGroups)
                }
                is NearbyGroupsResult.NoLocationChosen -> when (deepLinkResult) {
                    is DeepLinkGroupResult.LoadingDeepLinkGroupResult -> DiscoverGroupsModel.DeepLinkLoadingModel(true, emptyList())
                    is DeepLinkGroupResult.NoDeepLinkGroupResult -> DiscoverGroupsModel.NearbyGroupsModel(true, emptyList())
                    is DeepLinkGroupResult.AlreadyGroupMemberDeepLinkResult -> DiscoverGroupsModel.DeepLinkMemberGroupModel(deepLinkResult.groupId, true, emptyList())
                    is DeepLinkGroupResult.SuccessDeepLinkGroupResult -> DiscoverGroupsModel.NearbyGroupsModel(true, listOf(deepLinkResult.deepLinkGroup))
                    is DeepLinkGroupResult.ErrorDeepLinkGroupResult -> DiscoverGroupsModel.DeepLinkErrorModel(true, emptyList())
                }
            }
            discoverGroupsModel
        }
    }

    sealed class DiscoverGroupsModel(open val showProcessing: Boolean,
                                     open val showNoLocation: Boolean,
                                     open val nearbyGroups: List<NearbyGroupData>) {
        data class NearbyGroupsModel(override val showNoLocation: Boolean, override val nearbyGroups: List<NearbyGroupData>) : DiscoverGroupsModel(false, showNoLocation, nearbyGroups)
        data class DeepLinkLoadingModel(override val showNoLocation: Boolean, override val nearbyGroups: List<NearbyGroupData>) : DiscoverGroupsModel(true, showNoLocation, nearbyGroups)
        data class DeepLinkMemberGroupModel(val groupId: String, override val showNoLocation: Boolean, override val nearbyGroups: List<NearbyGroupData>) : DiscoverGroupsModel(false, showNoLocation, nearbyGroups)
        data class DeepLinkErrorModel(override val showNoLocation: Boolean, override val nearbyGroups: List<NearbyGroupData>) : DiscoverGroupsModel(false, showNoLocation, nearbyGroups)
        class ErrorModel : DiscoverGroupsModel(false, false, emptyList())
    }

    override fun viewDetached(view: IDiscoverGroupsView) {
        subscriptions.clear()
    }

    override fun likeGroup(group: NearbyGroupData) {
        bimberAnalytics.logRate(LIKE, PARTY)
        skippedGroupsRepository.clearSkippedGroup(group.groupData.group().key())
        groupDeepLinkRepository.removeDeepLinkGroupId()
        subscriptions.add(requestGroupJoinUseCase.likeGroup(currentUserId, group.groupData.group().key())
                .subscribe({ Timber.d("Requested to join group: %s", group) }
                ) { throwable -> Timber.e(throwable, "Failed to request group join") })
    }

    override fun dislikeGroup(group: NearbyGroupData) {
        bimberAnalytics.logRate(DISLIKE, PARTY)
        skippedGroupsRepository.clearSkippedGroup(group.groupData.group().key())
        groupDeepLinkRepository.removeDeepLinkGroupId()
        subscriptions.add(requestGroupJoinUseCase.dislikeGroup(currentUserId, group.groupData.group().key())
                .subscribe({ Timber.d("Disliked group: %s", group) }
                ) { throwable -> Timber.e(throwable, "Failed to dislike group") })
    }

    override fun skipGroup(group: NearbyGroupData) {
        skippedGroupsRepository.addSkippedGroup(group.groupData.group().key())
    }

    override fun deepLinkHandled() {
        groupDeepLinkRepository.removeDeepLinkGroupId()
    }
}
