package com.bimber.features.discoverpeople;

import com.bimber.domain.model.NearbyUserData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.mvp.MvpPresenter;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

public interface DiscoverPeopleContract {
    interface IDiscoverPeopleView {
        void showModelData(DiscoverPeoplePresenter.DiscoverPeopleModel model);

        void userMatched(ProfileDataThumbnail currentProfileDataThumbnail, ProfileDataThumbnail matchedUserMatchDat);
    }

    interface IDiscoverPeoplePresenter extends MvpPresenter<IDiscoverPeopleView> {
        void likeUser(NearbyUserData userId);

        void dislikeUser(NearbyUserData userId);

        void revertUser(NearbyUserData userId);

        void deepLinkHandled();
    }
}
