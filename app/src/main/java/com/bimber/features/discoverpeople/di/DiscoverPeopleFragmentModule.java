package com.bimber.features.discoverpeople.di;

import com.bimber.features.discoverpeople.DiscoverPeopleContract.IDiscoverPeoplePresenter;
import com.bimber.features.discoverpeople.DiscoverPeoplePresenter;
import com.bimber.features.discoverpeople.DiscoverPeopleView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

@Module
public abstract class DiscoverPeopleFragmentModule {

    @Binds
    abstract IDiscoverPeoplePresenter discoverPresenter(DiscoverPeoplePresenter discoverPeoplePresenter);

    @ContributesAndroidInjector
    abstract DiscoverPeopleView contribute();
}
