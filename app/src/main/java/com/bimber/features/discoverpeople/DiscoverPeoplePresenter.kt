package com.bimber.features.discoverpeople

import com.bimber.base.auth.LoggedInUserId
import com.bimber.data.analytics.BimberAnalytics
import com.bimber.data.analytics.BimberAnalytics.Rating.DISLIKE
import com.bimber.data.analytics.BimberAnalytics.Rating.LIKE
import com.bimber.data.analytics.BimberAnalytics.RatingContentType.PERSON
import com.bimber.data.sources.discovery.users.RecentlyRejectedUserRepository
import com.bimber.data.sources.discovery.users.UserDeepLinkRepository
import com.bimber.domain.discovery.users.GetDeepLinkUserProfileUseCase
import com.bimber.domain.discovery.users.GetDeepLinkUserProfileUseCase.DeepLinkUserResult
import com.bimber.domain.discovery.users.GetDeepLinkUserProfileUseCase.DeepLinkUserResult.*
import com.bimber.domain.discovery.users.GetNearbyDiscoverableUsersUseCase
import com.bimber.domain.discovery.users.GetNearbyDiscoverableUsersUseCase.NearbyUsersResult
import com.bimber.domain.discovery.users.GetRecentlyRejectedUserUseCase
import com.bimber.domain.discovery.users.GetRecentlyRejectedUserUseCase.RecentlyRejectedUserResult
import com.bimber.domain.discovery.users.GetRecentlyRejectedUserUseCase.RecentlyRejectedUserResult.*
import com.bimber.domain.matching.MatchUserUseCase
import com.bimber.domain.model.NearbyUserData
import com.bimber.features.discoverpeople.DiscoverPeopleContract.IDiscoverPeoplePresenter
import com.bimber.features.discoverpeople.DiscoverPeopleContract.IDiscoverPeopleView
import com.bimber.features.discoverpeople.DiscoverPeoplePresenter.DiscoverPeopleModel.*
import com.bimber.utils.mvp.MvpAbstractPresenter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function3
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 14.11.16.
 */

class DiscoverPeoplePresenter
@Inject
constructor(@param:LoggedInUserId private val currentUserId: String,
            private val getNearbyDiscoverableUsersUseCase: GetNearbyDiscoverableUsersUseCase,
            private val getDeepLinkUserProfileUseCase: GetDeepLinkUserProfileUseCase,
            private val getRecentlyRejectedUserUseCase: GetRecentlyRejectedUserUseCase,
            private val userDeepLinkRepository: UserDeepLinkRepository,
            private val recentlyRejectedUserRepository: RecentlyRejectedUserRepository,
            private val matchUserUseCase: MatchUserUseCase,
            private val bimberAnalytics: BimberAnalytics) : MvpAbstractPresenter<IDiscoverPeopleView>(), IDiscoverPeoplePresenter {


    private val subscriptions = CompositeDisposable()

    override fun viewAttached(view: IDiscoverPeopleView) {
        recentlyRejectedUserRepository.clear()
        subscriptions.add(Flowable.combineLatest(
                getRecentlyRejectedUserUseCase.onRecentlyRejectedUser(),
                getDeepLinkUserProfileUseCase.onDeepLinkProfile(currentUserId),
                getNearbyDiscoverableUsersUseCase.onDiscoverableUsers(currentUserId),
                classifyResultsAsModel())
                .doOnError { Timber.e(it, "Filed to obtain nearby discoverable users data") }
                .onErrorReturnItem(ErrorModel(false, emptyList(), null))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showModelData))

    }

    private fun classifyResultsAsModel(): Function3<RecentlyRejectedUserResult, DeepLinkUserResult, NearbyUsersResult, DiscoverPeopleModel> {
        return Function3 { recentlyRejectedResult, deepLinkResult, nearbyUsersResult ->
            when (recentlyRejectedResult) {
                is NoRecentlyRejectedUserResult -> classifyNoRecentlyRejectedUser(deepLinkResult, nearbyUsersResult)
                is HasRejectedUserResult -> classifyWithRejectedUser(recentlyRejectedResult.rejectedUser, nearbyUsersResult)
                is HasRevertedUserResult -> classifyWithRevertedUser(recentlyRejectedResult.revertedUser, nearbyUsersResult)
            }
        }
    }

    private fun classifyNoRecentlyRejectedUser(deepLinkResult: DeepLinkUserResult, nearbyUsersResult: NearbyUsersResult): DiscoverPeopleModel {
        return when (nearbyUsersResult) {
            is NearbyUsersResult.Loading -> when (deepLinkResult) {
                is LoadingDeepLinkUserResult -> DeepLinkLoadingModel(false, emptyList())
                is NoDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers, null)
                is CurrentUserDeepLink -> DeepLinkCurrentProfileModel(false, nearbyUsersResult.remainingUsers)
                is AlreadyMatchedUserDeepLink -> DeepLinkMatchedUserModel(deepLinkResult.dialogId, false, nearbyUsersResult.remainingUsers)
                is SuccessDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(deepLinkResult.deepLinkUser), null)
                is ErrorDeepLinkUserResult -> DeepLinkErrorModel(false, nearbyUsersResult.remainingUsers)
            }
            is NearbyUsersResult.Success -> when (deepLinkResult) {
                is LoadingDeepLinkUserResult -> DeepLinkLoadingModel(false, nearbyUsersResult.nearbyUsers)
                is NoDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.nearbyUsers, null)
                is CurrentUserDeepLink -> DeepLinkCurrentProfileModel(false, nearbyUsersResult.nearbyUsers)
                is AlreadyMatchedUserDeepLink -> DeepLinkMatchedUserModel(deepLinkResult.dialogId, false, nearbyUsersResult.nearbyUsers)
                is SuccessDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.nearbyUsers.with(deepLinkResult.deepLinkUser), null)
                is ErrorDeepLinkUserResult -> DeepLinkErrorModel(false, nearbyUsersResult.nearbyUsers)
            }
            is NearbyUsersResult.NoLocationChosen -> when (deepLinkResult) {
                is LoadingDeepLinkUserResult -> DeepLinkLoadingModel(true, emptyList())
                is NoDeepLinkUserResult -> NearbyUsersModel(true, emptyList(), null)
                is CurrentUserDeepLink -> DeepLinkCurrentProfileModel(true, emptyList())
                is AlreadyMatchedUserDeepLink -> DeepLinkMatchedUserModel(deepLinkResult.dialogId, true, emptyList())
                is SuccessDeepLinkUserResult -> NearbyUsersModel(true, listOf(deepLinkResult.deepLinkUser), null)
                is ErrorDeepLinkUserResult -> DeepLinkErrorModel(true, emptyList())
            }
            is NearbyUsersResult.NoMoreUsersInTheArea -> when (deepLinkResult) {
                is LoadingDeepLinkUserResult -> DeepLinkLoadingModel(false, emptyList())
                is NoDeepLinkUserResult -> NoMoreNearbyUsersModel(false, nearbyUsersResult.remainingUsers, null)
                is CurrentUserDeepLink -> DeepLinkCurrentProfileModel(false, nearbyUsersResult.remainingUsers)
                is AlreadyMatchedUserDeepLink -> DeepLinkMatchedUserModel(deepLinkResult.dialogId, false, nearbyUsersResult.remainingUsers)
                is SuccessDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(deepLinkResult.deepLinkUser), null)
                is ErrorDeepLinkUserResult -> DeepLinkErrorModel(false, nearbyUsersResult.remainingUsers)
            }
            is NearbyUsersResult.Error -> when (deepLinkResult) {
                is LoadingDeepLinkUserResult -> DeepLinkLoadingModel(false, nearbyUsersResult.remainingUsers)
                is NoDeepLinkUserResult -> ErrorModel(false, nearbyUsersResult.remainingUsers, null)
                is CurrentUserDeepLink -> DeepLinkCurrentProfileModel(false, nearbyUsersResult.remainingUsers)
                is AlreadyMatchedUserDeepLink -> DeepLinkMatchedUserModel(deepLinkResult.dialogId, false, nearbyUsersResult.remainingUsers)
                is SuccessDeepLinkUserResult -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(deepLinkResult.deepLinkUser), null)
                is ErrorDeepLinkUserResult -> DeepLinkErrorModel(false, nearbyUsersResult.remainingUsers)
            }
        }
    }

    private fun classifyWithRejectedUser(recentlyRejectedUser: NearbyUserData, nearbyUsersResult: NearbyUsersResult): DiscoverPeopleModel {
        return when (nearbyUsersResult) {
            is NearbyUsersResult.Loading -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(recentlyRejectedUser), recentlyRejectedUser)
            is NearbyUsersResult.Success -> NearbyUsersModel(false, nearbyUsersResult.nearbyUsers.with(recentlyRejectedUser), recentlyRejectedUser)
            is NearbyUsersResult.NoLocationChosen -> NearbyUsersModel(true, emptyList(), null)
            is NearbyUsersResult.NoMoreUsersInTheArea -> NoMoreNearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(recentlyRejectedUser), recentlyRejectedUser)
            is NearbyUsersResult.Error -> ErrorModel(false, nearbyUsersResult.remainingUsers.with(recentlyRejectedUser), recentlyRejectedUser)
        }
    }

    private fun classifyWithRevertedUser(revertedUser: NearbyUserData, nearbyUsersResult: NearbyUsersResult): DiscoverPeopleModel {
        return when (nearbyUsersResult) {
            is NearbyUsersResult.Loading -> NearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(revertedUser), null)
            is NearbyUsersResult.Success -> NearbyUsersModel(false, nearbyUsersResult.nearbyUsers.with(revertedUser), null)
            is NearbyUsersResult.NoLocationChosen -> NearbyUsersModel(true, emptyList(), null)
            is NearbyUsersResult.NoMoreUsersInTheArea -> NoMoreNearbyUsersModel(false, nearbyUsersResult.remainingUsers.with(revertedUser), null)
            is NearbyUsersResult.Error -> ErrorModel(false, nearbyUsersResult.remainingUsers.with(revertedUser), null)
        }
    }

    sealed class DiscoverPeopleModel(open val showNoLocation: Boolean,
                                     open val nearbyUsers: List<NearbyUserData>) {
        data class NearbyUsersModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>, val recentlyRejectedUser: NearbyUserData?) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class NoMoreNearbyUsersModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>, val recentlyRejectedUser: NearbyUserData?) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class ErrorModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>, val recentlyRejectedUser: NearbyUserData?) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class DeepLinkLoadingModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class DeepLinkCurrentProfileModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class DeepLinkMatchedUserModel(val dialogId: String, override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
        data class DeepLinkErrorModel(override val showNoLocation: Boolean, override val nearbyUsers: List<NearbyUserData>) : DiscoverPeopleModel(showNoLocation, nearbyUsers)
    }

    override fun viewDetached(view: IDiscoverPeopleView) {
        subscriptions.clear()
    }

    override fun likeUser(user: NearbyUserData) {
        bimberAnalytics.logRate(LIKE, PERSON)
        recentlyRejectedUserRepository.clear()
        userDeepLinkRepository.clearDeepLinkUser()
        subscriptions.add(matchUserUseCase.likeUser(currentUserId, user.profileData().user.uId)
                .subscribe({ matchData ->
                    bimberAnalytics.logMatch()
                    view.userMatched(matchData.matchingUser(), matchData.matchedUser())
                }
                ) { throwable -> Timber.e(throwable, "Failed to like user") })
    }

    override fun dislikeUser(user: NearbyUserData) {
        bimberAnalytics.logRate(DISLIKE, PERSON)
        recentlyRejectedUserRepository.setRecentlyRejectedUser(user)
        userDeepLinkRepository.clearDeepLinkUser()
        subscriptions.add(matchUserUseCase.dislikeUser(currentUserId, user.profileData().user.uId)
                .subscribe({ }) { throwable -> Timber.e(throwable, "Failed to dislike user") })
    }

    override fun revertUser(userId: NearbyUserData) {
        recentlyRejectedUserRepository.setRevertedUser(userId)
    }

    override fun deepLinkHandled() {
        userDeepLinkRepository.clearDeepLinkUser()
    }

    private fun List<NearbyUserData>.with(startingUser: NearbyUserData): List<NearbyUserData> {
        return listOf(startingUser) + this.filter { it.key() != startingUser.key() }
    }
}
