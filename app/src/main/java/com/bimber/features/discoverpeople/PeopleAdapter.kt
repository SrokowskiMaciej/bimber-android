package com.bimber.features.discoverpeople

import android.support.constraint.ConstraintLayout
import android.support.v4.view.ViewCompat
import android.support.v4.widget.TextViewCompat
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bimber.R
import com.bimber.base.glide.GlideApp
import com.bimber.domain.model.NearbyUserData
import com.bimber.utils.maps.SphericalUtil
import com.bimber.utils.view.AbstractDiffUtilCallback
import com.bimber.utils.view.SquareCardView
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

class PeopleAdapter : RecyclerView.Adapter<PeopleAdapter.PersonViewHolder>() {

    private var nearbyUsers = emptyList<NearbyUserData>()
    private val personClickEvents = PublishSubject.create<PersonClickEvent>()

    init {
        setHasStableIds(true)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        return PersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_discover_people_card, parent, false))
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {

        val nearbyUserData = nearbyUsers[position]
        val profileData = nearbyUserData.profileData()
        val user = profileData.user
        holder.nearbyUserData = nearbyUserData
        holder.textViewUserInfo!!.text = user.displayName
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(holder.textViewUserInfo!!, 5, 36, 2, TypedValue.COMPLEX_UNIT_DIP)
        holder.textViewFriendsCount!!.text = String.format(holder.textViewFriendsCount!!.context.resources.getQuantityString(R.plurals.profile_friends_count, user
                .friends), user.friends)
        holder.textViewPartiesCount!!.text = String.format(holder.textViewPartiesCount!!.context.resources.getQuantityString(R.plurals.profile_parties_count, user
                .parties), user.parties)
        if (user.about.isEmpty()) {
            holder.textViewAbout!!.visibility = GONE
        } else {
            holder.textViewAbout!!.visibility = VISIBLE
            holder.textViewAbout!!.text = String.format(holder.textViewAbout!!.context.getString(R.string.view_discover_card_quote_wrapper), user.about)
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(holder.textViewAbout!!, 5, 32, 2, TypedValue.COMPLEX_UNIT_DIP)
        }

        val userLocation = nearbyUserData.userLocation()
        val referenceLocation = nearbyUserData.referenceLocation()
        if (userLocation == null || referenceLocation == null) {
            holder.textViewLocation!!.setText(R.string.view_discover_card_unknown_location)
            holder.textViewDistance!!.visibility = GONE
        } else {
            holder.textViewLocation!!.text = userLocation.locationName()
            val distance = SphericalUtil.computeDistanceBetween(LatLng(userLocation.latitude(), userLocation.longitude()),
                    LatLng(referenceLocation.latitude(), referenceLocation.longitude())) / 1000.0
            holder.textViewDistance!!.visibility = VISIBLE
            holder.textViewDistance!!.text = String.format(holder.textViewDistance!!.context.getString(R.string.view_discover_card_distance),
                    Math.max(MINIMUM_DISTANCE.toLong(), Math.round(distance)).toString())
        }
        val photo = profileData.photo
        holder.background!!.setBackgroundResource(nearbyUserData.backgroundResource())
        ViewCompat.setTransitionName(holder.imageViewProfilePhoto, photo.userPhotoUri)
        GlideApp.with(holder.imageViewProfilePhoto)
                .load(photo.userPhotoUri)
                .thumbnail(GlideApp.with(holder.imageViewProfilePhoto)
                        .load(photo.userPhotoThumb)
                        .thumbnail(GlideApp.with(holder.imageViewProfilePhoto)
                                .load(R.drawable.ic_default_profile)))
                .apply(RequestOptions().centerCrop())
                .into(holder.imageViewProfilePhoto!!)

    }

    override fun getItemId(position: Int): Long {
        return nearbyUsers[position].profileData().user.uId.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return nearbyUsers.size
    }

    inner class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        @BindView(R.id.imageViewProfilePhoto)
        @JvmField
        public var imageViewProfilePhoto: ImageView? = null
        @BindView(R.id.textViewUserInfo)
        @JvmField
        public var textViewUserInfo: TextView? = null
        @BindView(R.id.textViewLocation)
        @JvmField
        public var textViewLocation: TextView? = null
        @BindView(R.id.textViewDrink)
        @JvmField
        public var textViewDrink: TextView? = null
        @BindView(R.id.textViewPass)
        @JvmField
        public var textViewPass: TextView? = null
        @BindView(R.id.textViewFriendsCount)
        @JvmField
        public var textViewFriendsCount: TextView? = null
        @BindView(R.id.textViewAbout)
        @JvmField
        public var textViewAbout: TextView? = null
        @BindView(R.id.textViewPartiesCount)
        @JvmField
        public var textViewPartiesCount: TextView? = null
        @BindView(R.id.textViewDistance)
        @JvmField
        public var textViewDistance: TextView? = null
        @BindView(R.id.background)
        @JvmField
        public var background: ConstraintLayout? = null
        @BindView(R.id.root)
        @JvmField
        public var root: SquareCardView? = null

        var nearbyUserData: NearbyUserData? = null

        init {
            ButterKnife.bind(this, itemView)
            root!!.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            nearbyUserData?.let { personClickEvents.onNext(PersonClickEvent(this@PersonViewHolder, it)) }
        }
    }

    fun clickEvents(): Observable<PersonClickEvent> {
        return personClickEvents
    }

    fun setDiscoverableUsers(nearbyUsers: List<NearbyUserData>) {
        val diffResult = DiffUtil.calculateDiff(object : AbstractDiffUtilCallback<NearbyUserData>(this.nearbyUsers, nearbyUsers) {

            override fun areItemsTheSame(oldItem: NearbyUserData, newItem: NearbyUserData): Boolean {
                return oldItem.profileData().user.uId == newItem.profileData().user.uId
            }

            override fun areContentsTheSame(oldItem: NearbyUserData, newItem: NearbyUserData): Boolean {
                return oldItem == newItem
            }
        })
        this.nearbyUsers = nearbyUsers
        diffResult.dispatchUpdatesTo(this)
    }

    companion object {
        val MINIMUM_DISTANCE = 1
    }

    data class PersonClickEvent(val viewHolder: PersonViewHolder, val userData: NearbyUserData)
}
