package com.bimber.features.discoverpeople;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.domain.model.NearbyUserData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.maps.SphericalUtil;
import com.bumptech.glide.request.RequestOptions;
import com.github.kevelbreh.androidunits.AndroidUnit;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by maciek on 16.02.17.
 */

public class PersonCardView extends CardView {

    public static final int MINIMUM_DISTANCE = 1;
    @BindView(R.id.imageViewProfilePhoto)
    ImageView imageViewProfilePhoto;
    @BindView(R.id.textViewUserInfo)
    TextView textViewUserInfo;
    @BindView(R.id.textViewLocation)
    TextView textViewLocation;
    @BindView(R.id.textViewDrink)
    TextView textViewDrink;
    @BindView(R.id.textViewPass)
    TextView textViewPass;

    @BindView(R.id.textViewFriendsCount)
    TextView textViewFriendsCount;
    @BindView(R.id.textViewAbout)
    TextView textViewAbout;
    @BindView(R.id.textViewPartiesCount)
    TextView textViewPartiesCount;
    @BindView(R.id.textViewDistance)
    TextView textViewDistance;
    @BindView(R.id.background)
    ConstraintLayout background;

    public PersonCardView(Context context) {
        super(context);
        init(context);
    }

    public PersonCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_discover_people_card, this, true);
        ButterKnife.bind(view);
        setUseCompatPadding(true);
        setContentPadding(0, 0, 0, 0);
        setPreventCornerOverlap(false);
        textViewDrink.setAlpha(0);
        textViewPass.setAlpha(0);
        setRadius(AndroidUnit.DENSITY_PIXELS.toPixels(6));
        setCardElevation(AndroidUnit.DENSITY_PIXELS.toPixels(6));
    }

    public void showUserData(NearbyUserData nearbyUserData) {
        ProfileDataThumbnail profileData = nearbyUserData.profileData();
        User user = profileData.user;
        textViewUserInfo.setText(user.displayName);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(textViewUserInfo, 5, 36, 2, TypedValue.COMPLEX_UNIT_DIP);
        textViewFriendsCount.setText(String.format(getContext().getResources().getQuantityString(R.plurals.profile_friends_count, user
                .friends), user.friends));
        textViewPartiesCount.setText(String.format(getContext().getResources().getQuantityString(R.plurals.profile_parties_count, user
                .parties), user.parties));
        if (user.about.isEmpty()) {
            textViewAbout.setVisibility(GONE);
        } else {
            textViewAbout.setVisibility(VISIBLE);
            textViewAbout.setText(String.format(getContext().getString(R.string.view_discover_card_quote_wrapper), user.about));
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(textViewAbout, 5, 32, 2, TypedValue.COMPLEX_UNIT_DIP);
        }

        UserLocationModel userUserLocationModel = nearbyUserData.userLocation();
        UserLocationModel referenceUserLocationModel = nearbyUserData.referenceLocation();
        if (userUserLocationModel == null || referenceUserLocationModel == null) {
            textViewLocation.setText(R.string.view_discover_card_unknown_location);
        } else {
            textViewLocation.setText(userUserLocationModel.locationName());
            double distance = SphericalUtil.computeDistanceBetween(new LatLng(userUserLocationModel.latitude(), userUserLocationModel.longitude()),
                    new LatLng(referenceUserLocationModel.latitude(), referenceUserLocationModel.longitude())) / 1000.0;
            textViewDistance.setText(String.format(getContext().getString(R.string.view_discover_card_distance),
                    String.valueOf(Math.max(MINIMUM_DISTANCE, Math.round(distance)))));
        }
        UserPhoto photo = profileData.photo;
        background.setBackgroundResource(nearbyUserData.backgroundResource());
        GlideApp.with(imageViewProfilePhoto)
                .load(photo.userPhotoUri)
                .thumbnail(GlideApp.with(imageViewProfilePhoto)
                        .load(photo.userPhotoThumb)
                        .thumbnail(GlideApp.with(imageViewProfilePhoto)
                                .load(R.drawable.ic_default_profile)))
                .apply(new RequestOptions().centerCrop())
                .into(imageViewProfilePhoto);
    }

    public void setScroll(float scroll) {
        if (scroll > 0.0f) {
            textViewPass.setAlpha(0.0f);
            textViewDrink.setAlpha(scroll);
        } else {
            textViewDrink.setAlpha(0.0f);
            textViewPass.setAlpha(-scroll);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}