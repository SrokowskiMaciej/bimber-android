package com.bimber.features.discoverpeople;

import com.bimber.R;

import java.util.Random;

/**
 * Created by maciek on 12.10.17.
 */

public class CardBackgroundRandomizer {

    private static final Random random = new Random();
    private static final int[] backgrounds = {
            R.drawable.bg_card_1,
            R.drawable.bg_card_2,
            R.drawable.bg_card_3,
            R.drawable.bg_card_4,
            R.drawable.bg_card_5,
            R.drawable.bg_card_6
    };

    public static int randomizeCardBackground() {
        return backgrounds[random.nextInt(6)];
    }
}
