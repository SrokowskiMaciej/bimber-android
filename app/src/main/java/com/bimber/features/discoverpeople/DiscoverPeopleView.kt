package com.bimber.features.discoverpeople


import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.bimber.R
import com.bimber.base.BitmapPool.DrawablePool
import com.bimber.base.fragments.BaseFragment
import com.bimber.data.analytics.BimberAnalytics
import com.bimber.data.analytics.BimberAnalytics.CreatePartyEntryPoint.DISCOVER_PEOPLE
import com.bimber.data.entities.Chattable
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail
import com.bimber.domain.model.NearbyUserData
import com.bimber.features.chat.activity.ChatActivity
import com.bimber.features.creategroup.activity.CreatePartyInitializeActivity
import com.bimber.features.discoverpeople.DiscoverPeopleContract.IDiscoverPeoplePresenter
import com.bimber.features.discoverpeople.DiscoverPeopleContract.IDiscoverPeopleView
import com.bimber.features.discoverpeople.DiscoverPeoplePresenter.DiscoverPeopleModel
import com.bimber.features.discoverpeople.DiscoverPeoplePresenter.DiscoverPeopleModel.*
import com.bimber.features.discoverpeople.PeopleAdapter.PersonViewHolder
import com.bimber.features.home.ExploreButtonsProvider
import com.bimber.features.home.ExploreButtonsProvider.ExploreButton
import com.bimber.features.home.ExploreButtonsProvider.ExploreButton.*
import com.bimber.features.home.ExploreButtonsProvider.ExploreButtonConsumer
import com.bimber.features.home.HomeNavigator
import com.bimber.features.home.HomeNavigator.HomeScreen
import com.bimber.features.location.activity.LocationPickerActivity
import com.bimber.features.messaging.match.MatchDialog
import com.bimber.features.profiledetails.activity.ProfileActivity
import com.bimber.utils.view.RadarView
import com.bimber.utils.view.swipe.StackLayoutManager
import com.bimber.utils.view.swipe.SwipeItemTouchHelper
import com.bimber.utils.view.swipe.SwipeItemTouchHelper.SwipeDirection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 28.10.16.
 */

class DiscoverPeopleView : BaseFragment(), IDiscoverPeopleView, ExploreButtonConsumer, SwipeItemTouchHelper.OnSwipeListener {

    @BindView(R.id.cardStackView)
    lateinit var recyclerView: RecyclerView
    @BindView(R.id.imageViewNoLocation)
    lateinit var imageViewNoLocation: ImageView
    @BindView(R.id.textViewNoLocationContent)
    lateinit var textViewNoLocationContent: TextView
    @BindView(R.id.buttonAddLocation)
    lateinit var buttonAddLocation: Button
    @BindView(R.id.radarView)
    lateinit var radarView: RadarView
    @BindView(R.id.textViewRadarContent)
    lateinit var textViewRadarContent: TextView


    @Inject
    lateinit var homeNavigator: HomeNavigator
    @Inject
    lateinit var discoverPeoplePresenter: IDiscoverPeoplePresenter
    @Inject
    lateinit var exploreButtonsProvider: ExploreButtonsProvider
    @Inject
    lateinit var drawablePool: DrawablePool
    @Inject
    lateinit var bimberAnalytics: BimberAnalytics

    private lateinit var peopleAdapter: PeopleAdapter
    private lateinit var swipeItemTouchHelper: SwipeItemTouchHelper
    private lateinit var stackLayoutManager: StackLayoutManager
    private var progressBarDialog: MaterialDialog? = null
    private var recentlyRejectedUser: NearbyUserData? = null

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_discover_people, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        peopleAdapter = PeopleAdapter()
        swipeItemTouchHelper = SwipeItemTouchHelper(SwipeItemTouchHelper.LEFT_RIGHT_DIRECTIONS_PROVIDER)
        swipeItemTouchHelper.setOnSwipeListener(this)
        swipeItemTouchHelper.attachToRecyclerView(recyclerView)
        stackLayoutManager = StackLayoutManager()
        recyclerView.layoutManager = stackLayoutManager
        recyclerView.adapter = peopleAdapter
        subscriptions.add(peopleAdapter.clickEvents().subscribe({ clickEvent ->
            val animatedPhotoUri = clickEvent.userData.profileData().photo.userPhotoUri;
            drawablePool.putDrawable(animatedPhotoUri, clickEvent.viewHolder.imageViewProfilePhoto!!.getDrawable())
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, clickEvent.viewHolder.imageViewProfilePhoto!!, animatedPhotoUri!!);
            startActivity(ProfileActivity.newInstance(context, clickEvent.userData.profileData().user.uId,
                    animatedPhotoUri, false), options.toBundle());

        }))
        discoverPeoplePresenter.bindView(this)
    }

    override fun onStart() {
        super.onStart()
        exploreButtonsProvider.validateExploreButtons()
    }

    override fun onDestroyView() {
        subscriptions.clear()
        discoverPeoplePresenter.unbindView(this)
        super.onDestroyView()
    }

    override fun showModelData(model: DiscoverPeopleModel) {
        when (model) {
            is NearbyUsersModel -> {
                showProcessing(false)
                this.recentlyRejectedUser = model.recentlyRejectedUser
            }
            is NoMoreNearbyUsersModel -> {
                showProcessing(false)
                showNoMorePeopleMessage()
                this.recentlyRejectedUser = model.recentlyRejectedUser
            }
            is ErrorModel -> {
                showProcessing(false)
                showError()
                this.recentlyRejectedUser = model.recentlyRejectedUser
            }
            is DeepLinkLoadingModel -> {
                showProcessing(false)
                showProcessing(true)
            }

            is DeepLinkCurrentProfileModel -> {
                showProcessing(false)
                showCurrentUserProfilePage()
            }
            is DeepLinkMatchedUserModel -> {
                showProcessing(false)
                showChatWithMatchedUser(model.dialogId)
            }
            is DeepLinkErrorModel -> {
                showProcessing(false)
                showDeepLinkProfileError()
            }
        }
        showDiscoverableUsers(model.nearbyUsers)
        showNoLocationsWarning(model.showNoLocation)
        exploreButtonsProvider.validateExploreButtons()
    }

    private fun showDiscoverableUsers(discoverableUsersData: List<NearbyUserData>) {
        peopleAdapter.setDiscoverableUsers(discoverableUsersData)
        if (discoverableUsersData.isNotEmpty()) {
            recyclerView.visibility = VISIBLE
        } else {
            recyclerView.visibility = GONE

        }
    }

    private fun showNoLocationsWarning(show: Boolean) {
        if (show) {
            imageViewNoLocation.visibility = View.VISIBLE
            textViewNoLocationContent.visibility = View.VISIBLE
            buttonAddLocation.visibility = View.VISIBLE
            radarView.visibility = View.GONE
            textViewRadarContent.visibility = View.GONE
        } else {
            imageViewNoLocation.visibility = View.GONE
            textViewNoLocationContent.visibility = View.GONE
            buttonAddLocation.visibility = View.GONE
            radarView.visibility = View.VISIBLE
            textViewRadarContent.visibility = View.VISIBLE
        }
    }

    private fun showProcessing(show: Boolean) {
        if (show && progressBarDialog != null) {
            progressBarDialog = MaterialDialog.Builder(context!!)
                    .progress(true, 0)
                    .content(R.string.view_chat_menu_dialog_group_manage_users_action_processing_text)
                    .cancelable(false)
                    .build()
            progressBarDialog!!.show()
        } else if (progressBarDialog != null) {
            progressBarDialog!!.dismiss()
            progressBarDialog = null;
        }
    }

    override fun userMatched(currentProfileDataThumbnail: ProfileDataThumbnail, matchedProfileDataThumbnail: ProfileDataThumbnail) {
        MatchDialog.newInstance(currentProfileDataThumbnail, matchedProfileDataThumbnail).show(activity!!.supportFragmentManager,
                MATCH_DIALOG_KEY)
    }

    private fun showCurrentUserProfilePage() {
        discoverPeoplePresenter.deepLinkHandled()
        homeNavigator.showScreen(HomeScreen.PROFILE)
    }

    private fun showChatWithMatchedUser(chatId: String) {
        discoverPeoplePresenter.deepLinkHandled()
        startActivity(ChatActivity.newInstance(context, chatId, Chattable.ChatType.DIALOG))
    }

    private fun showNoMorePeopleMessage() {
        //TODO
    }

    private fun showDeepLinkProfileError() {
        Toast.makeText(context, "Uuuuuuuu, we couldn't find this user", Toast.LENGTH_LONG).show()
    }

    private fun showError() {
        Toast.makeText(context, "Error, we're sorry", Toast.LENGTH_LONG).show()
    }

    override fun shouldExploreButtonBeVisible(exploreButton: ExploreButton): Boolean {
        return when (exploreButton) {
            LIKE -> ratingButtonsAvailable()
            DISLIKE -> ratingButtonsAvailable()
            ADD -> true
            REVERT -> recentlyRejectedUser != null
            SKIP -> false
        }
    }

    override fun onClickExploreButton(exploreButton: ExploreButton, button: View) {

        when {
            exploreButton == LIKE && ratingButtonsAvailable() -> swipeItemTouchHelper.swipe(SwipeDirection.RIGHT)
            exploreButton == DISLIKE && ratingButtonsAvailable() -> swipeItemTouchHelper.swipe(SwipeDirection.LEFT)
            exploreButton == ADD -> {
                bimberAnalytics.logPartyCreationInit(DISCOVER_PEOPLE)
                val clipRevealAnimation = ActivityOptionsCompat.makeClipRevealAnimation(button, 0, 0, button.width, button.height)
                activity!!.startActivity(CreatePartyInitializeActivity.newInstance(activity!!), clipRevealAnimation.toBundle())
            }
            exploreButton == REVERT -> {
                val user = recentlyRejectedUser
                if (user != null) {
                    swipeItemTouchHelper.revert(user.key())
                    discoverPeoplePresenter.revertUser(user)
                }
            }
        }
    }

    private fun ratingButtonsAvailable() =
            peopleAdapter != null && recyclerView != null && peopleAdapter.itemCount > 0

    @OnClick(R.id.buttonAddLocation)
    fun onViewClicked() {
        startActivity(LocationPickerActivity.newInstance(context, R.style.AppThemeLight, R.style.AppTheme, false))
    }

    override fun onSwipeProgress(viewHolder: RecyclerView.ViewHolder, swipeProgress: Float) {}

    override fun onSwipeProgressLeft(viewHolder: RecyclerView.ViewHolder, swipeProgress: Float) {
        val personViewHolder = viewHolder as PersonViewHolder
        personViewHolder.textViewPass!!.alpha = swipeProgress
    }

    override fun onSwipeProgressTop(viewHolder: RecyclerView.ViewHolder, swipeProgress: Float) {

    }

    override fun onSwipeProgressRight(viewHolder: RecyclerView.ViewHolder, swipeProgress: Float) {
        val personViewHolder = viewHolder as PersonViewHolder
        personViewHolder.textViewDrink!!.alpha = swipeProgress
    }

    override fun onSwipeProgressBottom(viewHolder: RecyclerView.ViewHolder, swipeProgress: Float) {

    }

    override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: SwipeDirection) {
        val personViewHolder = viewHolder as PersonViewHolder
        val nearbyUserData = personViewHolder.nearbyUserData
        if (nearbyUserData != null) {
            when (swipeDirection) {
                SwipeItemTouchHelper.SwipeDirection.LEFT -> discoverPeoplePresenter.dislikeUser(nearbyUserData)
                SwipeItemTouchHelper.SwipeDirection.RIGHT -> discoverPeoplePresenter.likeUser(nearbyUserData)
            }
        }

    }

    companion object {
        const val MATCH_DIALOG_KEY = "MATCH_DIALOG_KEY"
    }
}
