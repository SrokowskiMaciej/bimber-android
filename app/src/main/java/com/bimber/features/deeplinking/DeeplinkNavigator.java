package com.bimber.features.deeplinking;


import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by maciek on 12.05.17.
 */

public class DeeplinkNavigator {

    private final BehaviorSubject<Deeplink> deeplinkBehaviorSubject = BehaviorSubject.createDefault(Deeplink.NONE);


    public void pushDeeplink(Deeplink deeplink) {
        deeplinkBehaviorSubject.onNext(deeplink);
    }

    public Observable<Deeplink> deeplink() {
        return deeplinkBehaviorSubject;
    }

    public enum Deeplink {
        PROFILE,
        GROUP,
        CHAT,
        NONE
    }
}
