package com.bimber.features.analytics.notifications;

import com.crashlytics.android.answers.CustomEvent;

/**
 * Created by maciek on 05.09.17.
 */

public class ChatNotificationAnalytics extends CustomEvent {


    public static final String CHAT_ID = "chat_id";
    public static final String MESSAGE_ID = "message_id";

    public ChatNotificationAnalytics(String eventName, String chatId, String messageId) {
        super(eventName);
        putCustomAttribute(CHAT_ID, chatId);
        putCustomAttribute(MESSAGE_ID, messageId);
    }
}
