package com.bimber.features.groupcandidates.di;

import com.bimber.base.FragmentScope;
import com.bimber.base.group.GroupModule;
import com.bimber.base.group.GroupProvider;
import com.bimber.features.groupcandidates.single.SingleGroupJoinCandidatesContract.ISingleGroupJoinCandidatesPresenter;
import com.bimber.features.groupcandidates.single.SingleSingleGroupJoinCandidatesPresenter;
import com.bimber.features.groupcandidates.single.SingleSingleGroupJoinCandidatesView;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 31.03.17.
 */
@Module
public abstract class GroupJoinCandidatesFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = {SingleGroupJoinCandidatesModule.class, GroupModule.class})
    abstract SingleSingleGroupJoinCandidatesView groupJoinCandidatesView();

    @Module
    abstract class SingleGroupJoinCandidatesModule {

        @Binds
        abstract ISingleGroupJoinCandidatesPresenter groupJoinRequestsPresenter(SingleSingleGroupJoinCandidatesPresenter
                                                                                        singleGroupJoinCandidatesPresenter);

        @Binds
        abstract GroupProvider profileProvider(SingleSingleGroupJoinCandidatesView singleSingleGroupJoinCandidatesView);
    }
}
