package com.bimber.features.groupcandidates;

import com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 03.04.17.
 */
@AutoValue
public abstract class CandidateEvaluationResult {

    public static CandidateEvaluationResult create(String groupId, String candidateId, GroupJoinStatus evaluationStatus) {
        return new AutoValue_CandidateEvaluationResult(groupId, candidateId, evaluationStatus);
    }

    public abstract String groupId();

    public abstract String candidateId();

    public abstract GroupJoinStatus evaluationStatus();
}
