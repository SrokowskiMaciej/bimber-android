package com.bimber.features.groupcandidates.activity;

import com.bimber.base.ActivityScope;
import com.bimber.base.auth.LoggedInAndroidComponent;

import dagger.Binds;
import dagger.Module;

/**
 * Created by maciek on 02.03.17.
 */
@Module
public abstract class GroupJoinCandidatesActivityModule {

    @Binds
    @ActivityScope
    abstract LoggedInAndroidComponent loggedInAndroidComponent(GroupJoinCandidatesActivity activity);
}
