package com.bimber.features.groupcandidates.single;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.base.group.GroupProvider;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.common.ProgressView;
import com.bimber.features.groupcandidates.single.SingleGroupJoinCandidatesContract.ISingleGroupJoinCandidatesPresenter;
import com.bimber.features.groupcandidates.single.SingleGroupJoinCandidatesContract.ISingleGroupJoinCandidatesView;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.SwipeDismissItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.touchguard.RecyclerViewTouchActionGuardManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 31.03.17.
 */

public class SingleSingleGroupJoinCandidatesView extends BaseFragment implements GroupProvider, ISingleGroupJoinCandidatesView {

    public static final String GROUP_ID_KEY = "GROUP_ID_KEY";
    @Inject
    ISingleGroupJoinCandidatesPresenter groupJoinRequestsPresenter;
    @Inject
    DefaultValues defaultValues;
    @BindView(R.id.recyclerViewGroupsJoinRequests)
    RecyclerView recyclerViewGroupsJoinCandidates;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressView)
    ProgressView progressView;
    @BindView(R.id.imageViewAdministrated)
    ImageView imageViewAdministrated;
    @BindView(R.id.textViewAdministratedTitle)
    TextView textViewAdministratedTitle;
    @BindView(R.id.textViewAdministratedSubtitle)
    TextView textViewAdministratedSubtitle;
    @BindView(R.id.textViewSwipeInstruction)
    TextView textViewSwipeInstruction;

    private RecyclerView.LayoutManager mLayoutManager;
    private SingleGroupCandidatesAdapter singleGroupCandidatesAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewSwipeManager mRecyclerViewSwipeManager;
    private RecyclerViewTouchActionGuardManager mRecyclerViewTouchActionGuardManager;
    private Unbinder unbinder;
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private String groupId;

    public static SingleSingleGroupJoinCandidatesView newInstance(String groupId) {
        SingleSingleGroupJoinCandidatesView groupJoinRequestsView = new SingleSingleGroupJoinCandidatesView();
        Bundle bundle = new Bundle();
        bundle.putString(GROUP_ID_KEY, groupId);
        groupJoinRequestsView.setArguments(bundle);
        return groupJoinRequestsView;
    }

    @Override
    public void onAttach(Context context) {
        groupId = getArguments().getString(GROUP_ID_KEY);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_group_join_requests, container, false);
        unbinder = ButterKnife.bind(this, view);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setTitle(R.string.view_group_join_candidates_toolbar_text);
        toolbar.setNavigationOnClickListener(view1 -> appCompatActivity.onBackPressed());
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        singleGroupCandidatesAdapter = new SingleGroupCandidatesAdapter(groupId, GlideApp.with(this));

        // swipe manager
        mRecyclerViewSwipeManager = new RecyclerViewSwipeManager();

        recyclerViewGroupsJoinCandidates.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(),
                R.drawable.shape_list_divider), true));

        // touch guard manager  (this class is required to suppress scrolling while swipe-dismiss animation is running)
        mRecyclerViewTouchActionGuardManager = new RecyclerViewTouchActionGuardManager();
        mRecyclerViewTouchActionGuardManager.setInterceptVerticalScrollingWhileAnimationRunning(true);
        mRecyclerViewTouchActionGuardManager.setEnabled(true);

        // wrap for swiping
        mWrappedAdapter = mRecyclerViewSwipeManager.createWrappedAdapter(singleGroupCandidatesAdapter);


        final GeneralItemAnimator animator = new SwipeDismissItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Disable the change animation in order to make turning back animation of swiped item works properly.
        // Also need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);

        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewGroupsJoinCandidates.setLayoutManager(mLayoutManager);
        recyclerViewGroupsJoinCandidates.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        recyclerViewGroupsJoinCandidates.setItemAnimator(animator);
        recyclerViewGroupsJoinCandidates.setHasFixedSize(false);
        // NOTE:
        // The initialization order is very important! This order determines the priority of touch event handling.
        //
        // priority: TouchActionGuard > Swipe > DragAndDrop > ExpandableItem
        mRecyclerViewTouchActionGuardManager.attachRecyclerView(recyclerViewGroupsJoinCandidates);
        mRecyclerViewSwipeManager.attachRecyclerView(recyclerViewGroupsJoinCandidates);
        subscriptions.add(singleGroupCandidatesAdapter.onCandidateEvaluated().subscribe(candidateEvaluationResult ->
                groupJoinRequestsPresenter.candidateEvaluated(candidateEvaluationResult)));
        groupJoinRequestsPresenter.bindView(this);
    }


    @Override
    public void onDestroyView() {
        subscriptions.clear();
        groupJoinRequestsPresenter.unbindView(this);

        if (mRecyclerViewSwipeManager != null) {
            mRecyclerViewSwipeManager.release();
            mRecyclerViewSwipeManager = null;
        }

        if (mRecyclerViewTouchActionGuardManager != null) {
            mRecyclerViewTouchActionGuardManager.release();
            mRecyclerViewTouchActionGuardManager = null;
        }

        if (recyclerViewGroupsJoinCandidates != null) {
            recyclerViewGroupsJoinCandidates.setItemAnimator(null);
            recyclerViewGroupsJoinCandidates.setAdapter(null);
            recyclerViewGroupsJoinCandidates = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        singleGroupCandidatesAdapter = null;
        mLayoutManager = null;
        unbinder.unbind();
        super.onDestroyView();
    }


    @Override
    public void setGroupsJoinCandidates(List<ProfileDataThumbnail> groupCandidates) {
        Timber.d("New Group join requests: %s", groupCandidates);
        progressView.setVisibility(View.GONE);
        singleGroupCandidatesAdapter.setGroupCandidates(groupCandidates);
    }

    @Override
    public void showIfGroupIsSecure(boolean secure) {
        if (secure) {
            textViewAdministratedTitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_secure_title);
            textViewAdministratedSubtitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_secure_subtitle);
            imageViewAdministrated.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPositiveAction));
        } else {
            textViewAdministratedTitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_unsecure_title);
            textViewAdministratedSubtitle.setText(R.string.view_chat_menu_dialog_group_manage_users_security_unsecure_subtitle);
            imageViewAdministrated.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorNegativeAction));
        }
    }

    @Override
    public void allowCurrentUserToEvaluate(boolean allowEvaluation) {
        if (allowEvaluation) {
            textViewSwipeInstruction.setVisibility(View.VISIBLE);
        } else {
            textViewSwipeInstruction.setVisibility(View.GONE);
        }
        singleGroupCandidatesAdapter.setSwipingEnabled(allowEvaluation);
    }

    @Override
    public String provideGroupId() {
        return groupId;
    }
}
