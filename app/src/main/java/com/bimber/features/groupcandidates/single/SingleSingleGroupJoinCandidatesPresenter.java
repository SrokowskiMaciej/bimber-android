package com.bimber.features.groupcandidates.single;

import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.group.GroupId;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.domain.groupjoin.EvaluateGroupCandidateUseCase;
import com.bimber.domain.groupjoin.GetGroupJoinCandidatesUseCase;
import com.bimber.features.groupcandidates.CandidateEvaluationResult;
import com.bimber.features.groupcandidates.single.SingleGroupJoinCandidatesContract.ISingleGroupJoinCandidatesPresenter;
import com.bimber.features.groupcandidates.single.SingleGroupJoinCandidatesContract.ISingleGroupJoinCandidatesView;
import com.bimber.utils.mvp.MvpAbstractPresenter;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Created by maciek on 31.03.17.
 */

public class SingleSingleGroupJoinCandidatesPresenter extends MvpAbstractPresenter<ISingleGroupJoinCandidatesView> implements
        ISingleGroupJoinCandidatesPresenter {

    private final String currentUserId;
    private final String groupId;
    private final GetGroupJoinCandidatesUseCase getGroupJoinCandidatesUseCase;
    private final EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase;
    private final GroupRepository groupRepository;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    @Inject
    public SingleSingleGroupJoinCandidatesPresenter(@LoggedInUserId String currentUserId, @GroupId String groupId,
                                                    GetGroupJoinCandidatesUseCase getGroupJoinCandidatesUseCase,
                                                    EvaluateGroupCandidateUseCase evaluateGroupCandidateUseCase, GroupRepository groupRepository) {
        this.currentUserId = currentUserId;
        this.groupId = groupId;
        this.getGroupJoinCandidatesUseCase = getGroupJoinCandidatesUseCase;
        this.evaluateGroupCandidateUseCase = evaluateGroupCandidateUseCase;
        this.groupRepository = groupRepository;
    }

    @Override
    protected void viewAttached(ISingleGroupJoinCandidatesView view) {
        Flowable<Boolean> groupSecure = groupRepository.isGroupSecure(groupId)
                .replay(1)
                .refCount();

        subscriptions.add(groupSecure
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showIfGroupIsSecure, throwable ->
                Timber.e(throwable, "Couldn't obtain group security rule")));

        subscriptions.add(Flowable.combineLatest(groupRepository.groupOwnerId(groupId).toFlowable(),
                groupSecure, (groupOwnerId, isGroupSecure) -> !isGroupSecure || groupOwnerId.equals(currentUserId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::allowCurrentUserToEvaluate, throwable ->
                        Timber.e(throwable, "Couldn't obtain group security rule")));


        subscriptions.add(getGroupJoinCandidatesUseCase.onGroupJoinCandidates(groupId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setGroupsJoinCandidates,
                        throwable -> Timber.e(throwable, "Error while listening for group join candidates")));

    }

    @Override
    protected void viewDetached(ISingleGroupJoinCandidatesView view) {
        subscriptions.clear();
    }

    @Override
    public void candidateEvaluated(CandidateEvaluationResult evaluationResult) {
        switch (evaluationResult.evaluationStatus()) {
            case ACCEPTED:
                subscriptions.add(evaluateGroupCandidateUseCase.acceptGroupJoinRequest(currentUserId, evaluationResult.candidateId(), evaluationResult.groupId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> Timber.d("Successfully evaluated group join request"),
                                throwable -> Timber.e(throwable, "Failed to evaluate group join request")));
                break;
            case REJECTED:
                subscriptions.add(evaluateGroupCandidateUseCase.rejectGroupJoinRequest(currentUserId, evaluationResult.candidateId(), evaluationResult.groupId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> Timber.d("Successfully evaluated group join request"),
                                throwable -> Timber.e(throwable, "Failed to evaluate group join request")));
                break;
            default:
                throw new Error("We shouldn't get here");
        }


    }
}
