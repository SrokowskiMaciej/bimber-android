package com.bimber.features.groupcandidates.single;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.groupcandidates.CandidateEvaluationResult;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.utils.view.ViewUtils;
import com.bumptech.glide.request.RequestOptions;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 01.04.17.
 */

public class SingleGroupCandidatesAdapter extends RecyclerView.Adapter<SingleGroupCandidatesAdapter.CandidateViewHolder>
        implements SwipeableItemAdapter<SingleGroupCandidatesAdapter.CandidateViewHolder>, SwipeableItemConstants {

    private List<ProfileDataThumbnail> groupCandidates = Collections.emptyList();

    private final PublishSubject<CandidateEvaluationResult> evaluationResultPublishSubject = PublishSubject.create();

    private final String groupId;
    private final GlideRequests glide;
    private boolean swipingEnabled = true;

    public SingleGroupCandidatesAdapter(String groupId, GlideRequests glide) {
        this.groupId = groupId;
        this.glide = glide;
        setHasStableIds(true);
    }


    public void setSwipingEnabled(boolean swipingEnabled) {
        this.swipingEnabled = swipingEnabled;
    }

    public void setGroupCandidates(List<ProfileDataThumbnail> groupCandidates) {
        this.groupCandidates = groupCandidates;
        notifyDataSetChanged();
    }

    public PublishSubject<CandidateEvaluationResult> onCandidateEvaluated() {
        return evaluationResultPublishSubject;
    }

    @Override
    public int onGetSwipeReactionType(CandidateViewHolder holder, int position, int x, int y) {
        if(swipingEnabled){
            return REACTION_CAN_SWIPE_BOTH_H;
        } else  {
            return REACTION_CAN_NOT_SWIPE_ANY;
        }
    }

    @Override
    public void onSwipeItemStarted(CandidateViewHolder holder, int position) {
        notifyDataSetChanged();

    }

    @Override
    public void onSetSwipeBackground(CandidateViewHolder holder, int position, int type) {
        int bgResId = 0;
        switch (type) {
            case DRAWABLE_SWIPE_NEUTRAL_BACKGROUND:
                bgResId = R.color.colorForeground;
                break;
            case DRAWABLE_SWIPE_LEFT_BACKGROUND:
                bgResId = R.drawable.bg_swipe_item_left;
                break;
            case DRAWABLE_SWIPE_RIGHT_BACKGROUND:
                bgResId = R.drawable.bg_swipe_item_right;
                break;
        }

        holder.itemView.setBackgroundResource(bgResId);
    }

    @Override
    public SwipeResultAction onSwipeItem(CandidateViewHolder holder, int position, int result) {
        ProfileDataThumbnail profileDataThumbnail = groupCandidates.get(position);
        switch (result) {
            // swipe right
            case RESULT_SWIPED_RIGHT:
                return new CandidateSwipeRightAction(profileDataThumbnail);
            case RESULT_SWIPED_LEFT:
                return new CandidateSwipeLeftAction(profileDataThumbnail);
            case RESULT_CANCELED:
            default:
                return null;
        }
    }

    @Override
    public CandidateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .view_group_join_requests_candidate_item_top, parent, false);
        return new CandidateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CandidateViewHolder holder, int position) {
        ProfileDataThumbnail profileDataThumbnail = groupCandidates.get(position);
        holder.setProfileData(profileDataThumbnail, glide);
    }

    @Override
    public int getItemCount() {
        return groupCandidates.size();
    }

    @Override
    public long getItemId(int position) {
        return groupCandidates.get(position).user.uId.hashCode();
    }

    public static class CandidateViewHolder extends AbstractSwipeableItemViewHolder implements View.OnClickListener {
        @BindView(R.id.imageViewAvatar)
        ImageView imageViewAvatar;
        @BindView(R.id.textViewCandidateName)
        TextView textViewCandidateName;
        @BindView(R.id.textViewAbout)
        TextView textViewAbout;
        @BindView(R.id.textViewFriendsCount)
        TextView textViewFriendsCount;
        @BindView(R.id.textViewPartiesCount)
        TextView textViewPartiesCount;
        @BindView(R.id.swipeContainerView)
        ConstraintLayout swipeContainerView;

        private ProfileDataThumbnail profileDataThumbnail;

        public CandidateViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeContainerView.setOnClickListener(this);
        }

        public void setProfileData(ProfileDataThumbnail profileDataThumbnail, GlideRequests glide) {
            this.profileDataThumbnail = profileDataThumbnail;
            glide.load(profileDataThumbnail.photo.userPhotoUri)
                    .apply(new RequestOptions().centerCrop())
                    .into(imageViewAvatar);
            User user = profileDataThumbnail.user;
            textViewCandidateName.setText(user.displayName);
            if (user.about.isEmpty()) {
                textViewAbout.setVisibility(View.INVISIBLE);
            } else {
                textViewAbout.setVisibility(View.VISIBLE);
                textViewAbout.setText(user.about);
            }
            ViewUtils.applyFixForDrawableTopTint(textViewFriendsCount, R.color.colorDivider);
            ViewUtils.applyFixForDrawableTopTint(textViewPartiesCount, R.color.colorDivider);
            textViewFriendsCount.setText(String.format(textViewFriendsCount.getContext().getResources()
                    .getQuantityString(R.plurals.profile_friends_count, user.friends), user.friends));
            textViewPartiesCount.setText(String.format(textViewPartiesCount.getContext().getResources()
                    .getQuantityString(R.plurals.profile_parties_count, user.parties), user.parties));
        }

        @Override
        public View getSwipeableContainerView() {
            return swipeContainerView;
        }

        @Override
        public void onClick(View v) {
            if (profileDataThumbnail != null) {
                swipeContainerView.getContext().startActivity(ProfileActivity.newInstance(swipeContainerView
                                .getContext(),
                        profileDataThumbnail.user.uId, false));
            }

        }
    }

    private class CandidateSwipeLeftAction extends AbstractSwipeAction {

        public CandidateSwipeLeftAction(ProfileDataThumbnail profileDataThumbnail) {
            super(profileDataThumbnail);
        }

        @Override
        protected void onPerformAction(ProfileDataThumbnail profileDataThumbnail) {
            evaluationResultPublishSubject.onNext(CandidateEvaluationResult.create(groupId, profileDataThumbnail.user.uId,
                    GroupJoinRequest.GroupJoinStatus.REJECTED));

        }
    }


    private class CandidateSwipeRightAction extends AbstractSwipeAction {

        public CandidateSwipeRightAction(ProfileDataThumbnail profileDataThumbnail) {
            super(profileDataThumbnail);
        }

        @Override
        protected void onPerformAction(ProfileDataThumbnail profileDataThumbnail) {
            evaluationResultPublishSubject.onNext(CandidateEvaluationResult.create(groupId, profileDataThumbnail.user.uId,
                    GroupJoinRequest.GroupJoinStatus.ACCEPTED));
        }
    }

    private abstract class AbstractSwipeAction extends SwipeResultAction {

        private final ProfileDataThumbnail profileDataThumbnail;

        private AbstractSwipeAction(ProfileDataThumbnail profileDataThumbnail) {
            super(AFTER_SWIPE_REACTION_MOVE_TO_SWIPED_DIRECTION);
            this.profileDataThumbnail = profileDataThumbnail;
        }

        @Override
        protected void onPerformAction() {
            super.onPerformAction();
            onPerformAction(profileDataThumbnail);
        }

        protected abstract void onPerformAction(ProfileDataThumbnail profileDataThumbnail);
    }
}
