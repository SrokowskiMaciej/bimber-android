package com.bimber.features.groupcandidates.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.R;
import com.bimber.base.auth.BaseLoggedInActivity;
import com.bimber.features.groupcandidates.single.SingleSingleGroupJoinCandidatesView;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.bimber.features.messaging.newchatmessages.NewChatMessageSnackbarFragment;

public class GroupJoinCandidatesActivity extends BaseLoggedInActivity {

    private static final String GROUP_CANDIDATES_FRAGMENT_TAG = "GROUP_CANDIDATES_FRAGMENT_TAG";
    public static final String NEW_CHAT_MESSASGE_TAG = "NEW_CHAT_MESSASGE_TAG";
    public static final String GROUP_ID = "GROUP_ID";

    public static Intent newInstance(Context context, String groupId) {
        Intent intent = new Intent(context, GroupJoinCandidatesActivity.class);
        intent.putExtra(GROUP_ID, groupId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_pane);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (getSupportFragmentManager().findFragmentByTag(NEW_CHAT_MESSASGE_TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, NewChatMessageSnackbarFragment.newInstance(CloudMessageDataHandler.HandlingPriority.MEDIUM),
                            NEW_CHAT_MESSASGE_TAG)
                    .commit();
        }

        if (getSupportFragmentManager().findFragmentByTag(GROUP_CANDIDATES_FRAGMENT_TAG) == null) {
            String singleGroupId = getIntent().getStringExtra(GROUP_ID);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, SingleSingleGroupJoinCandidatesView.newInstance(singleGroupId), GROUP_CANDIDATES_FRAGMENT_TAG)
                    .commit();
        }
    }
}
