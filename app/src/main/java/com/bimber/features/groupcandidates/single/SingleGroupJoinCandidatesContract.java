package com.bimber.features.groupcandidates.single;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.features.groupcandidates.CandidateEvaluationResult;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by maciek on 31.03.17.
 */

public interface SingleGroupJoinCandidatesContract {
    interface ISingleGroupJoinCandidatesView {
        void setGroupsJoinCandidates(List<ProfileDataThumbnail> groupCandidates);

        void showIfGroupIsSecure(boolean secure);
        void allowCurrentUserToEvaluate(boolean allowEvaluation);

    }

    interface ISingleGroupJoinCandidatesPresenter extends MvpPresenter<ISingleGroupJoinCandidatesView> {
        void candidateEvaluated(CandidateEvaluationResult candidateEvaluationResult);

    }
}
