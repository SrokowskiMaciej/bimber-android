package com.bimber.features.editprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bimber.R;
import com.bimber.base.auth.LoggedInUserId;
import com.bimber.base.fragments.BaseFragment;
import com.bimber.base.glide.GlideApp;
import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.network.FavouritePlaceModel;
import com.bimber.data.entities.profile.network.FavouriteTreatModel;
import com.bimber.data.entities.profile.network.Gender;
import com.bimber.features.editprofile.EditProfileContract.IEditProfilePresenter;
import com.bimber.features.editprofile.EditProfileContract.IEditProfileView;
import com.bimber.features.editprofile.adapters.AlcoholsAdapter;
import com.bimber.features.editprofile.adapters.FavouritePlacesAdapter;
import com.bimber.features.editprofile.adapters.PhotoThumbnailsAdapter;
import com.bimber.features.photogallery.edit.activity.PhotoGalleryActivity;
import com.bimber.features.photogallery.profile.activity.ProfilePhotoGalleryActivity;
import com.bimber.utils.firebase.Key;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

import static android.app.Activity.RESULT_OK;

/**
 * Created by srokowski.maciej@gmail.com on 28.12.16.
 */

public class EditProfileView extends BaseFragment implements IEditProfileView {


    public static final int PLACE_PICKER_REQUEST_CODE = 789;
    //TODO Change per country
    public static final int MINIMUM_DRINKING_AGE = 18;
    @BindView(R.id.textViewAboutContent)
    TextView textViewAboutContent;
    @BindView(R.id.textViewPhotosContent)
    TextView textViewPhotosContent;
    @BindView(R.id.recyclerViewPhotosThumbnails)
    RecyclerView recyclerViewPhotosThumbnails;
    @BindView(R.id.recyclerViewAlcohols)
    RecyclerView recyclerViewAlcohols;
    @BindView(R.id.buttonAddPlace)
    Button imageButtonAddPlace;
    @BindView(R.id.radioGroupGenderPreference)
    RadioGroup radioGroupGender;
    @BindView(R.id.textViewAgeContent)
    TextView textViewAgeContent;
    @BindView(R.id.seekBarAge)
    SeekBar seekBarAge;
    @BindView(R.id.radioButtonGenderPreferenceMale)
    RadioButton radioButtonMale;
    @BindView(R.id.radioButtonGenderPreferenceFemale)
    RadioButton radioButtonFemale;
    @BindView(R.id.recyclerViewFavouritePlaces)
    RecyclerView recyclerViewFavouritePlaces;
    private PhotoThumbnailsAdapter photoThumbnailsAdapter;
    private AlcoholsAdapter alcoholsAdapter;
    private FavouritePlacesAdapter favouritePlacesAdapter;

    @Inject
    IEditProfilePresenter editProfilePresenter;
    @Inject
    @LoggedInUserId
    String currentUserId;

    private CompositeDisposable subscribtions = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.view_edit_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewDefaultValues();
        initPhotosRecyclerView();
        initAlcoholsRecyclerView();
        initFavouritePlacesRecyclerView();
        initGenderViews();
        initAgeViews();
        subscribtions.add(alcoholsAdapter.onAlcoholsChanged().subscribe(alcohols -> editProfilePresenter
                .changeAlcohols(alcohols)));
        subscribtions.add(favouritePlacesAdapter.onFavouritePlacesChanged().subscribe(favouritePlaces ->
                editProfilePresenter.changeFavouritePlaces(favouritePlaces)));
        subscribtions.add(photoThumbnailsAdapter.clickedPhotos().subscribe(clickedPhotoAction -> {
            ViewCompat.setTransitionName(clickedPhotoAction.clickedView, clickedPhotoAction.photo.photoId);
            ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation
                    (getActivity(), clickedPhotoAction.clickedView, clickedPhotoAction.photo.photoId);
            startActivity(ProfilePhotoGalleryActivity.newInstance(getContext(), currentUserId, clickedPhotoAction.photo,
                    R.style.AppTheme),
                    activityOptionsCompat.toBundle());
        }));
        editProfilePresenter.bindView(this);
    }

    @Override
    public void onDestroyView() {
        subscribtions.clear();
        editProfilePresenter.unbindView(this);
        super.onDestroyView();
    }

    private void initViewDefaultValues() {
        setPhotoCount(0);
        setAboutContent("");
        setAge(MINIMUM_DRINKING_AGE);
        setGender(Gender.FEMALE);
    }

    private void initPhotosRecyclerView() {
        photoThumbnailsAdapter = new PhotoThumbnailsAdapter(GlideApp.with(this));
        recyclerViewPhotosThumbnails.setLayoutManager(new FlexboxLayoutManager(getContext(), FlexDirection.ROW));
        recyclerViewPhotosThumbnails.setAdapter(photoThumbnailsAdapter);
    }


    private void initAlcoholsRecyclerView() {
        alcoholsAdapter = new AlcoholsAdapter();
        RecyclerViewDragDropManager recyclerViewDragDropManager = new RecyclerViewDragDropManager();
        recyclerViewAlcohols.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewAlcohols.setAdapter(recyclerViewDragDropManager.createWrappedAdapter(alcoholsAdapter));
        recyclerViewAlcohols.setItemAnimator(new DraggableItemAnimator());
        recyclerViewDragDropManager.attachRecyclerView(recyclerViewAlcohols);
    }

    private void initFavouritePlacesRecyclerView() {
        favouritePlacesAdapter = new FavouritePlacesAdapter();
        RecyclerViewDragDropManager recyclerViewDragDropManager = new RecyclerViewDragDropManager();
        recyclerViewFavouritePlaces.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewFavouritePlaces.setAdapter(recyclerViewDragDropManager.createWrappedAdapter
                (favouritePlacesAdapter));
        recyclerViewFavouritePlaces.setItemAnimator(new DraggableItemAnimator());
        recyclerViewDragDropManager.attachRecyclerView(recyclerViewFavouritePlaces);
    }

    private void initGenderViews() {
        radioGroupGender.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioButtonGenderPreferenceMale:
                    if (radioButtonMale.isPressed()) {
                        editProfilePresenter.setGender(Gender.MALE);
                    }
                    break;
                case R.id.radioButtonGenderPreferenceFemale:
                    if (radioButtonFemale.isPressed()) {
                        editProfilePresenter.setGender(Gender.FEMALE);
                    }
                    break;
            }
        });
    }

    private void initAgeViews() {
        seekBarAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int currentAge = 18;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentAge = progress + MINIMUM_DRINKING_AGE;
                textViewAgeContent.setText(String.format(getContext().getString(R.string
                        .view_edit_profile_age_content_text), currentAge));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                editProfilePresenter.setAge(currentAge);

            }
        });
    }

    @Override
    public void setGender(Gender gender) {
        switch (gender) {
            case MALE:
                radioButtonMale.setChecked(true);
                break;
            case FEMALE:
                radioButtonFemale.setChecked(true);
                break;
        }
    }

    @Override
    public void setAge(int age) {
        seekBarAge.setProgress(age - MINIMUM_DRINKING_AGE);
    }

    @Override
    public void setPhotos(List<UserPhoto> photos) {
        photoThumbnailsAdapter.setPhotos(photos);
    }

    @Override
    public void setPhotoCount(int photoCount) {
        textViewPhotosContent.setText(String.format(getResources()
                .getQuantityString(R.plurals.view_edit_profile_photos_content_text, photoCount), photoCount));
    }

    @Override
    public void setAboutContent(String about) {
        textViewAboutContent.setText(about);
    }

    @Override
    public void setAlcohols(List<FavouriteTreat> alcohols) {
        alcoholsAdapter.setAlcohols(alcohols);
    }

    @Override
    public void setFavouritePlaces(List<FavouritePlace> favouritePlaces) {
        favouritePlacesAdapter.setFavouritePlaces(favouritePlaces);
    }

    @Override
    public void showPlacePicker() {

        try {
            AppCompatActivity activity = (AppCompatActivity) getContext();
            PlacePicker.IntentBuilder placePickerIntentBuilder = new PlacePicker.IntentBuilder();
            Intent intent = placePickerIntentBuilder.build(activity);
            startActivityForResult(intent, PLACE_PICKER_REQUEST_CODE, ActivityOptionsCompat.makeClipRevealAnimation
                    (imageButtonAddPlace,
                            0, 0, 0, 0).toBundle());
            //TODO Handle errors
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showErrorScreen() {
        //TODO

    }

    @Override
    public void showErrorToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.buttonEditPhotos, R.id.buttonAddAlcohol, R.id.buttonAddPlace, R.id.buttonEditAbout})
    public void onClick(View view) {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        switch (view.getId()) {
            case R.id.buttonEditAbout:
                startEditAboutDialog();
                break;
            case R.id.buttonEditPhotos:
                activity.startActivity(new Intent(activity, PhotoGalleryActivity.class));
                break;
            case R.id.buttonAddAlcohol:
                startAddAlcoholDialog();
                break;
            case R.id.buttonAddPlace:
                showPlacePicker();
                break;
        }
    }

    private void startAddAlcoholDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_edit_profile_alcohols_dialog_title)
                .content(R.string.view_edit_profile_alcohols_dialog_content)
                .input(R.string.view_edit_profile_alcohols_dialog_hint, R.string.empty, false, (dialog, input) ->
                        editProfilePresenter.addAlcohol(FavouriteTreatModel.create(alcoholsAdapter.getItemCount(), input.toString
                                ())))
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE | InputType
                        .TYPE_TEXT_FLAG_CAP_SENTENCES)
                .negativeText(R.string.dialog_cancel_button_text)
                .negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .positiveText(R.string.dialog_ok_button_text)
                .positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .show();
    }

    private void startEditAboutDialog() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.view_edit_profile_about_title_text)
                .input(getString(R.string.view_edit_profile_about_hint), textViewAboutContent.getText().toString(),
                        true,
                        (dialog, input) -> editProfilePresenter.setAboutContent(input.toString()))
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE | InputType
                        .TYPE_TEXT_FLAG_CAP_SENTENCES)
                .negativeText(R.string.dialog_cancel_button_text)
                .negativeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .positiveText(R.string.dialog_ok_button_text)
                .positiveColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PLACE_PICKER_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    final Place place = PlacePicker.getPlace(getContext(), data);
                    final FavouritePlaceModel favouritePlaceModel = FavouritePlaceModel.create(
                            favouritePlacesAdapter.getItemCount(), String.valueOf(place.getName()), String.valueOf
                                    (place.getAddress()),
                            String.valueOf(place.getId()), place.getLatLng().longitude, place.getLatLng().latitude);
                    editProfilePresenter.addPlace(favouritePlaceModel);
                }
                break;
        }
    }
}
