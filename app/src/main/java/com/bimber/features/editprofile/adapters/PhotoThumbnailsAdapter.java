package com.bimber.features.editprofile.adapters;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bimber.R;
import com.bimber.base.glide.GlideRequests;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.utils.view.AbstractDiffUtilCallback;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by srokowski.maciej@gmail.com on 17.12.16.
 */

public class PhotoThumbnailsAdapter extends RecyclerView.Adapter<PhotoThumbnailsAdapter.PhotoViewHolder> {


    private List<UserPhoto> photos = Collections.emptyList();

    private PublishSubject<ClickedPhotoAction> clickedPhotoActionPublishSubject = PublishSubject.create();

    private final GlideRequests glide;

    public PhotoThumbnailsAdapter(GlideRequests glide) {
        this.glide = glide;
    }


    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .view_edit_profile_photo_thumbnail_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        UserPhoto userPhotoModel = photos.get(position);
        glide.load(userPhotoModel.userPhotoUri)
                .thumbnail(glide.load(userPhotoModel.userPhotoThumb))
                .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.imageViewPhoto);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(List<UserPhoto> photos) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AbstractDiffUtilCallback<UserPhoto>(this.photos, photos) {
            @Override
            public boolean areItemsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.photoId.equals(newItem.photoId);
            }

            @Override
            public boolean areContentsTheSame(UserPhoto oldItem, UserPhoto newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.photos = photos;
        diffResult.dispatchUpdatesTo(this);
    }


    public Observable<ClickedPhotoAction> clickedPhotos() {
        return clickedPhotoActionPublishSubject;
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewPhoto)
        ImageView imageViewPhoto;

        PhotoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.imageViewPhoto)
        public void onViewClicked() {
            clickedPhotoActionPublishSubject.onNext(new ClickedPhotoAction(imageViewPhoto, photos.get
                    (getAdapterPosition())));
        }

    }

    public static class ClickedPhotoAction {
        public final View clickedView;
        public final UserPhoto photo;

        public ClickedPhotoAction(View clickedView, UserPhoto photo) {
            this.clickedView = clickedView;
            this.photo = photo;
        }
    }

}
