package com.bimber.features.editprofile.adapters

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bimber.R
import com.bimber.data.entities.profile.local.FavouriteTreat
import com.bimber.utils.view.AbstractDiffUtilCallback
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemViewHolder
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*

/**
 * Created by srokowski.maciej@gmail.com on 21.12.16.
 */

class AlcoholsAdapter : RecyclerView.Adapter<AlcoholsAdapter.AlcoholViewHolder>(), DraggableItemAdapter<AlcoholsAdapter.AlcoholViewHolder>, DraggableItemConstants {

    private var alcohols = emptyList<FavouriteTreat>()
    private val alcoholsChangedSubject = PublishSubject.create<List<FavouriteTreat>>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlcoholViewHolder {
        return AlcoholViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_edit_profile_alcohol_item, parent,
                false))
    }

    override fun onBindViewHolder(holder: AlcoholViewHolder, position: Int) {
        val alcohol = alcohols[position]
        holder.textViewAlcoholName!!.text = alcohol.favouriteTreatName
        holder.imageButtonDelete!!.setOnClickListener { onItemRemoveButtonClicked(alcohol) }
    }

    override fun getItemCount(): Int {
        return alcohols.size
    }

    override fun onCheckCanStartDrag(holder: AlcoholsAdapter.AlcoholViewHolder, position: Int, x: Int, y: Int): Boolean {
        val handleX = holder.imageButtonDragHandle!!.left
        val handleY = holder.imageButtonDragHandle!!.top
        val handleW = holder.imageButtonDragHandle!!.width
        val handleH = holder.imageButtonDragHandle!!.height

        return x >= handleX && x < handleX + handleW && y >= handleY && y < handleY + handleH
    }

    override fun onGetItemDraggableRange(holder: AlcoholsAdapter.AlcoholViewHolder, position: Int): ItemDraggableRange? {
        return null
    }

    override fun onMoveItem(fromPosition: Int, toPosition: Int) {
        if (fromPosition == toPosition) {
            return
        }

        val movedAlcohol = alcohols[fromPosition]
        val rearrangedAlcoholsList = ArrayList(alcohols)
        rearrangedAlcoholsList.removeAt(fromPosition)
        rearrangedAlcoholsList.add(toPosition, movedAlcohol)

        val alteredAlcohols = ArrayList(rearrangedAlcoholsList)
                .withIndex()
                .map {
                    FavouriteTreat(it.value.favouriteTreatId,
                            it.value.favouriteTreatUserId, it.index, it.value.favouriteTreatName)
                }
        alcoholsChangedSubject.onNext(Collections.unmodifiableList(alteredAlcohols))
    }

    private fun onItemRemoveButtonClicked(favouriteTreat: FavouriteTreat) {
        val alteredFavouritePlaces = ArrayList(alcohols)
                .filter { it.favouriteTreatId != favouriteTreat.favouriteTreatId }
                .withIndex()
                .map {
                    FavouriteTreat(it.value.favouriteTreatId,
                            it.value.favouriteTreatUserId, it.index, it.value.favouriteTreatName)
                }
        alcoholsChangedSubject.onNext(alteredFavouritePlaces)
    }

    override fun onCheckCanDrop(draggingPosition: Int, dropPosition: Int): Boolean {
        return true
    }

    override fun onItemDragStarted(position: Int) {
        notifyDataSetChanged()
    }

    override fun onItemDragFinished(fromPosition: Int, toPosition: Int, result: Boolean) {
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return alcohols[position].favouriteTreatId.hashCode().toLong()
    }

    fun setAlcohols(alcohols: List<FavouriteTreat>) {
        val diffResult = DiffUtil.calculateDiff(object : AbstractDiffUtilCallback<FavouriteTreat>(this.alcohols, alcohols) {
            override fun areItemsTheSame(oldItem: FavouriteTreat, newItem: FavouriteTreat): Boolean {
                return oldItem.favouriteTreatId == newItem.favouriteTreatId
            }

            override fun areContentsTheSame(oldItem: FavouriteTreat, newItem: FavouriteTreat): Boolean {
                return oldItem == newItem
            }
        })
        this.alcohols = alcohols
        diffResult.dispatchUpdatesTo(this)
    }

    fun onAlcoholsChanged(): Observable<List<FavouriteTreat>> {
        return alcoholsChangedSubject
    }

    class AlcoholViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), DraggableItemViewHolder {
        @BindView(R.id.textViewAlcoholName)
        @JvmField
        internal var textViewAlcoholName: TextView? = null
        @BindView(R.id.imageButtonDelete)
        @JvmField
        internal var imageButtonDelete: ImageButton? = null
        @BindView(R.id.imageButtonDragHandle)
        @JvmField
        internal var imageButtonDragHandle: ImageButton? = null

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun setDragStateFlags(flags: Int) {

        }

        override fun getDragStateFlags(): Int {
            return 0
        }
    }
}
