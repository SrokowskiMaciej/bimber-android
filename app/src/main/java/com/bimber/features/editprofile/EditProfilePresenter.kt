package com.bimber.features.editprofile

import com.bimber.base.auth.LoggedInUserId
import com.bimber.data.entities.isDefault
import com.bimber.data.entities.profile.local.FavouritePlace
import com.bimber.data.entities.profile.local.FavouriteTreat
import com.bimber.data.entities.profile.network.FavouritePlaceModel
import com.bimber.data.entities.profile.network.FavouriteTreatModel
import com.bimber.data.entities.profile.network.Gender
import com.bimber.data.sources.profile.photos.UsersPhotosRepository
import com.bimber.data.sources.profile.places.FavouritePlacesRepository
import com.bimber.data.sources.profile.treats.FavouriteTreatsRepository
import com.bimber.data.sources.profile.user.UsersRepository
import com.bimber.features.editprofile.EditProfileContract.IEditProfilePresenter
import com.bimber.utils.mvp.MvpAbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

class EditProfilePresenter @Inject
constructor(@param:LoggedInUserId private val currentUserId: String, private val usersRepository: UsersRepository, private val usersPhotosRepository: UsersPhotosRepository,
            private val favouritePlacesRepository: FavouritePlacesRepository, private val favouriteTreatsRepository: FavouriteTreatsRepository) : MvpAbstractPresenter<EditProfileContract.IEditProfileView>(), IEditProfilePresenter {

    private val subscriptions = CompositeDisposable()

    override fun viewAttached(view: EditProfileContract.IEditProfileView) {
        subscriptions.add(usersRepository.onUser(currentUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { user ->
                    view.setAboutContent(user.about)
                    view.setGender(user.gender)
                    view.setAge(user.age)
                }, onError = { throwable ->
                    Timber.e(throwable, "Error user data")
                    view.showErrorScreen()
                }))
        subscriptions.add(usersPhotosRepository.onAllUserPhotos(currentUserId)
                .map { photos -> photos.filter { !it.isDefault() } }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { photosList ->
                    view.setPhotos(photosList)
                    view.setPhotoCount(photosList.size)
                }, onError = { throwable ->
                    Timber.e(throwable, "Error loading user photos")
                    view.showErrorScreen()
                }))
        subscriptions.add(favouriteTreatsRepository.onFavouriteTreats(currentUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { view.setAlcohols(it) },
                        onError = { throwable ->
                            Timber.e(throwable, "Error loading alcohols")
                            view.showErrorScreen()
                        }))

        subscriptions.add(favouritePlacesRepository.onFavouritePlaces(currentUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { view.setFavouritePlaces(it) },
                        onError = { throwable ->
                            Timber.e(throwable, "Error loading favourite places")
                            view.showErrorScreen()
                        }))
    }

    override fun viewDetached(view: EditProfileContract.IEditProfileView) {
        subscriptions.clear()
    }

    override fun setAboutContent(about: String) {
        subscriptions.add(usersRepository.updateUserAbout(currentUserId, about)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Abount successfully updated: %s", about) }) { throwable ->
                    Timber.e(throwable, "Failed to update user's about section")
                    view.showErrorToast("Failed to update about section :(")
                })

    }

    override fun addAlcohol(newFavouriteTreatModel: FavouriteTreatModel) {
        subscriptions.add(favouriteTreatsRepository.insertFavouriteTreat(currentUserId, newFavouriteTreatModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("FavouriteTreatModel with successfully inserted") }) { throwable ->
                    Timber.e(throwable, "Failed to setDiscoverableUsers alcohol")
                    view.showErrorToast("Failed to setDiscoverableUsers alcohol :(")
                })
    }

    override fun changeAlcohols(alcohols: List<FavouriteTreat>) {
        subscriptions.add(favouriteTreatsRepository.updateFavouriteTreats(currentUserId, alcohols)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Alcohols successfully rearranged") }) { throwable ->
                    Timber.e(throwable, "Failed to update alcohol")
                    view.showErrorToast("Failed to update alcohol :(")
                })
    }

    override fun addPlace(favouritePlaceModel: FavouritePlaceModel) {
        subscriptions.add(favouritePlacesRepository.insertFavouritePlace(currentUserId, favouritePlaceModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Favourite location with successfully inserted: %s", favouritePlaceModel) }
                ) { throwable ->
                    Timber.e(throwable, "Failed to setDiscoverableUsers favourite location")
                    view.showErrorToast("Failed to setDiscoverableUsers favourite location :(")
                })
    }

    override fun changeFavouritePlaces(places: List<FavouritePlace>) {
        subscriptions.add(favouritePlacesRepository.updateFavouritePlaces(currentUserId, places)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Favourite places successfully rearranged") }) { throwable ->
                    Timber.e(throwable, "Failed to update favourite location")
                    view.showErrorToast("Failed to update favourite location :(")
                })
    }

    override fun setGender(gender: Gender) {
        subscriptions.add(usersRepository.updateUserGender(currentUserId, gender)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Gender successfully updated: %s", gender) }) { throwable ->
                    Timber.e(throwable, "Failed to update user's gender")
                    view.showErrorToast("Failed to update gender :(")
                })
    }

    override fun setAge(age: Int) {
        subscriptions.add(usersRepository.updateUserAge(currentUserId, age)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i("Age successfully updated: %s", age) }) { throwable ->
                    Timber.e(throwable, "Failed to update user's age")
                    view.showErrorToast("Failed to update age :(")
                })
    }
}
