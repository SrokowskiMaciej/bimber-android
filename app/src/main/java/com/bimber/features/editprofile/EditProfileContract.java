package com.bimber.features.editprofile;

import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.network.FavouritePlaceModel;
import com.bimber.data.entities.profile.network.FavouriteTreatModel;
import com.bimber.data.entities.profile.network.Gender;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.mvp.MvpPresenter;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

public interface EditProfileContract {

    interface IEditProfileView {
        void setPhotos(List<UserPhoto> photos);

        void setPhotoCount(int photoCount);

        void setGender(Gender gender);

        void setAge(int age);

        void setAboutContent(String about);

        void setAlcohols(List<FavouriteTreat> alcohols);

        void setFavouritePlaces(List<FavouritePlace> favouritePlaces);

        void showPlacePicker();

        void showErrorScreen();

        void showErrorToast(String message);
    }

    interface IEditProfilePresenter extends MvpPresenter<IEditProfileView> {

        void setAboutContent(String about);

        void addAlcohol(FavouriteTreatModel favouriteTreatModel);

        void changeAlcohols(List<FavouriteTreat> alcohols);

        void addPlace(FavouritePlaceModel place);

        void changeFavouritePlaces(List<FavouritePlace> places);

        void setGender(Gender gender);

        void setAge(int age);
    }
}
