package com.bimber.features.editprofile.adapters

import android.content.Intent
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bimber.R
import com.bimber.data.entities.profile.local.FavouritePlace
import com.bimber.utils.view.AbstractDiffUtilCallback
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemViewHolder
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by srokowski.maciej@gmail.com on 22.12.16.
 */

class FavouritePlacesAdapter : RecyclerView.Adapter<FavouritePlacesAdapter.FavouritePlaceViewHolder>(), DraggableItemAdapter<FavouritePlacesAdapter.FavouritePlaceViewHolder>, DraggableItemConstants {

    private var favouritePlaces = emptyList<FavouritePlace>()
    private val changedFavouritePlaces = PublishSubject.create<List<FavouritePlace>>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritePlacesAdapter.FavouritePlaceViewHolder {
        return FavouritePlacesAdapter.FavouritePlaceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout
                .view_edit_profile_favourite_place_item, parent, false))
    }

    override fun onBindViewHolder(holder: FavouritePlacesAdapter.FavouritePlaceViewHolder, position: Int) {
        val favouritePlace = favouritePlaces[position]
        holder.textViewFavouritePlaceName!!.text = favouritePlace.normalizedPlaceName
        holder.imageButtonDelete!!.setOnClickListener { onItemRemoveButtonClicked(favouritePlace) }
        holder.imageButtonMap!!.setOnClickListener { v ->
            val mapIntent = Intent(Intent.ACTION_VIEW, favouritePlace.googleMapsSearchUri)
            v.context.startActivity(mapIntent)
        }
    }


    override fun getItemCount(): Int {
        return favouritePlaces.size
    }

    override fun onCheckCanStartDrag(holder: FavouritePlacesAdapter.FavouritePlaceViewHolder, position: Int, x: Int, y: Int): Boolean {
        val handleX = holder.imageButtonDragHandle!!.left
        val handleY = holder.imageButtonDragHandle!!.top
        val handleW = holder.imageButtonDragHandle!!.width
        val handleH = holder.imageButtonDragHandle!!.height

        return x >= handleX && x < handleX + handleW && y >= handleY && y < handleY + handleH
    }

    override fun onGetItemDraggableRange(holder: FavouritePlacesAdapter.FavouritePlaceViewHolder, position: Int): ItemDraggableRange? {
        return null
    }

    override fun onMoveItem(fromPosition: Int, toPosition: Int) {
        if (fromPosition == toPosition) {
            return
        }

        val movedFavouritePlace = favouritePlaces[fromPosition]
        val rearrangedFavouritePlacesList = ArrayList(favouritePlaces)
        rearrangedFavouritePlacesList.removeAt(fromPosition)
        rearrangedFavouritePlacesList.add(toPosition, movedFavouritePlace)

        val alteredFavouritePlaces = rearrangedFavouritePlacesList.withIndex()
                .map {
                    FavouritePlace(it.value.favouritePlaceId,
                            it.value.favouritePlaceUserId, it.index, it.value.name,
                            it.value.address, it.value.googleMapsId,
                            it.value.longitude, it.value.latitude)
                }
        changedFavouritePlaces.onNext(Collections.unmodifiableList(alteredFavouritePlaces))
        setFavouritePlaces(alteredFavouritePlaces)
    }

    private fun onItemRemoveButtonClicked(favouritePlace: FavouritePlace) {
        val alteredFavouritePlaces = ArrayList(favouritePlaces)
                .filter { it.favouritePlaceId != favouritePlace.favouritePlaceId }
                .withIndex()
                .map {
                    FavouritePlace(it.value.favouritePlaceId,
                            it.value.favouritePlaceUserId, it.index, it.value.name,
                            it.value.address, it.value.googleMapsId,
                            it.value.longitude, it.value.latitude)
                }
        changedFavouritePlaces.onNext(alteredFavouritePlaces)
    }

    override fun onCheckCanDrop(draggingPosition: Int, dropPosition: Int): Boolean {
        return true
    }

    override fun onItemDragStarted(position: Int) {
        notifyDataSetChanged()
    }

    override fun onItemDragFinished(fromPosition: Int, toPosition: Int, result: Boolean) {
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return favouritePlaces[position].favouritePlaceId.hashCode().toLong()
    }

    fun setFavouritePlaces(favouritePlaces: List<FavouritePlace>) {
        val diffResult = DiffUtil.calculateDiff(object : AbstractDiffUtilCallback<FavouritePlace>(this.favouritePlaces, favouritePlaces) {
            override fun areItemsTheSame(oldItem: FavouritePlace, newItem: FavouritePlace): Boolean {
                return oldItem.favouritePlaceId == newItem.favouritePlaceId
            }

            override fun areContentsTheSame(oldItem: FavouritePlace, newItem: FavouritePlace): Boolean {
                return oldItem == newItem
            }
        })
        this.favouritePlaces = favouritePlaces
        diffResult.dispatchUpdatesTo(this)
    }

    fun onFavouritePlacesChanged(): Observable<List<FavouritePlace>> {
        return changedFavouritePlaces
    }


    class FavouritePlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), DraggableItemViewHolder {
        @BindView(R.id.textViewFavouritePlaceName)
        @JvmField
        internal var textViewFavouritePlaceName: TextView? = null
        @BindView(R.id.imageButtonMap)
        @JvmField
        internal var imageButtonMap: ImageButton? = null
        @BindView(R.id.imageButtonDelete)
        @JvmField
        internal var imageButtonDelete: ImageButton? = null
        @BindView(R.id.imageButtonDragHandle)
        @JvmField
        internal var imageButtonDragHandle: ImageButton? = null

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun setDragStateFlags(flags: Int) {

        }

        override fun getDragStateFlags(): Int {
            return 0
        }

    }
}