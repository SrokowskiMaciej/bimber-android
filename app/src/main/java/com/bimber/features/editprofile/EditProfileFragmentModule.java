package com.bimber.features.editprofile;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import static com.bimber.features.editprofile.EditProfileContract.IEditProfilePresenter;

/**
 * Created by srokowski.maciej@gmail.com on 18.12.16.
 */

@Module
public abstract class EditProfileFragmentModule {

    @Binds
    abstract IEditProfilePresenter discoverPresenter(EditProfilePresenter editProfilePresenter);

    @ContributesAndroidInjector
    abstract EditProfileView contribute();
}
