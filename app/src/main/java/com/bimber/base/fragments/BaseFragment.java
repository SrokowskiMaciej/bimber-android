package com.bimber.base.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bimber.base.application.BimberApplication;
import com.hannesdorfmann.fragmentargs.FragmentArgs;

import dagger.android.support.DaggerFragment;
import icepick.Icepick;

/**
 * Created by maciek on 19.07.17.
 */

public class BaseFragment extends DaggerFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BimberApplication.getRefWatcher(getContext()).watch(this);
    }
}
