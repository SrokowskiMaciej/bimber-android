package com.bimber.base.auth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import dagger.android.AndroidInjection;

/**
 * Created by maciek on 20.04.17.
 */

public abstract class BaseLoggedInBroadcastReceiver extends BroadcastReceiver implements LoggedInAndroidComponent {

    private String currentUserUid;

    @Override
    final public void onReceive(Context context, Intent intent) {
        String currentUserUid = loggedInUserId();
        if (currentUserUid != null) {
            AndroidInjection.inject(this, context);
            onReceiveLoggedIn(context, intent);
        } else {
            onReceiveLoggedOut(context, intent);
        }

    }

    @Override
    final public String loggedInUserId() {
        if (currentUserUid != null) {
            return currentUserUid;
        }
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            return currentUserUid;
        } else {
            return null;
        }
    }

    public abstract void onReceiveLoggedIn(Context context, Intent intent);

    public abstract void onReceiveLoggedOut(Context context, Intent intent);
}
