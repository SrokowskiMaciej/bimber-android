package com.bimber.base.auth;

import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 21.04.17.
 */
@Module
public abstract class LoggedInModule {

    @Provides
    @LoggedInUserId
    public static String loggedInUserId(LoggedInAndroidComponent loggedInAndroidComponent) {
        return loggedInAndroidComponent.loggedInUserId();
    }
}
