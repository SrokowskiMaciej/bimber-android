package com.bimber.base.auth;

/**
 * Created by maciek on 20.04.17.
 */

public interface LoggedInAndroidComponent {

    String loggedInUserId();
}
