package com.bimber.base.auth;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import dagger.android.AndroidInjection;

/**
 * Created by maciek on 03.07.17.
 */

public abstract class BaseLoggedInJobService extends JobService implements LoggedInAndroidComponent {

    private String currentUserUid;

    @Override
    public boolean onStartJob(JobParameters job) {
        String currentUserUid = loggedInUserId();
        if (currentUserUid != null) {
            AndroidInjection.inject(this);
            return onStartJobLoggedIn(job);
        } else {
            return onStartJobLoggedOut(job);
        }
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        String currentUserUid = loggedInUserId();
        if (currentUserUid != null) {
            AndroidInjection.inject(this);
            return onStopJobLoggedIn(job);
        } else {
            return onStopJobLoggedOut(job);
        }
    }

    public abstract boolean onStartJobLoggedIn(JobParameters job);

    public abstract boolean onStartJobLoggedOut(JobParameters job);

    public abstract boolean onStopJobLoggedIn(JobParameters job);

    public abstract boolean onStopJobLoggedOut(JobParameters job);

    @Override
    final public String loggedInUserId() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
        } else {
            currentUserUid = null;
        }
        return currentUserUid;
    }
}
