package com.bimber.base.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;

import com.bimber.base.activities.BackPressedManager;
import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.data.sources.deletedUser.DeletedUserNetworkSource;
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource;
import com.bimber.data.sources.profile.lastSeen.LastUserActivityNetworkSource;
import com.bimber.features.gdprassurance.GdprAssuranceView;
import com.bimber.features.home.HomeNavigator;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.signin.SignInActivity;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by srokowski.maciej@gmail.com on 14.01.17.
 */

public abstract class BaseLoggedInActivity extends AppCompatActivity implements HasFragmentInjector, HasSupportFragmentInjector,
        LoggedInAndroidComponent, GoogleApiClient.OnConnectionFailedListener {

    @Inject
    LastUserActivityNetworkSource lastUserActivityNetworkSource;
    @Inject
    DeletedUserNetworkSource deletedUserNetworkSource;
    @Inject
    GDPRConsentNetworkSource gdprConsentNetworkSource;
    @Inject
    FirebaseAuthService firebaseAuthService;

    @Inject
    @LoggedInUserId
    String loggedInUserId;

    @Inject
    DispatchingAndroidInjector<Fragment> supportFragmentInjector;
    @Inject
    DispatchingAndroidInjector<android.app.Fragment> frameworkFragmentInjector;
    @Inject
    BackPressedManager backPressedManager;
    @Inject
    FirebaseAnalytics firebaseAnalytics;

    private String currentUserUid;
    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        String currentUserUid = loggedInUserId();
        if (currentUserUid != null) {
            setCrashliticsUserInfo();
            AndroidInjection.inject(this);
            firebaseAnalytics.setUserId(currentUserUid);
            super.onCreate(savedInstanceState);
        } else {
            //Not injecting anything
            super.onCreate(savedInstanceState);
            startSignInActivityCurrentParent();
            finish();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build()
                .connect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAnalytics.setCurrentScreen(this, getClass().getSimpleName(), null);
        disposables.add(firebaseAuthService.onUserLoggedOut()
                .subscribe(aVoid -> {
                    firebaseAnalytics.setUserId(null);
                    startSignInActivityWithHomeParent();
                    finish();
                }, throwable -> Timber.e(throwable, "Failed to obtain user logged in status")));
        disposables.add(deletedUserNetworkSource.isUserDeleted(loggedInUserId)
                .subscribe(isUserDeleted -> {
                    if (isUserDeleted) {
                        firebaseAuthService.logOut();
                    }
                }, throwable -> Timber.e(throwable, "Failed to obtain user deletion status")));
        disposables.add(lastUserActivityNetworkSource.updateLastSeen(loggedInUserId)
                .subscribe(() -> {
                }, throwable -> Timber.e(throwable, "Failed to update user last seen value")));

        disposables.add(gdprConsentNetworkSource.userGdprConsentStatus(loggedInUserId)
                .subscribe(consentStatus -> {
                    switch (consentStatus) {
                        case NONE:
                            startActivity(new Intent(BaseLoggedInActivity.this, GdprAssuranceView.class));
                            break;
                        case REFUSED:
                            firebaseAuthService.logOut();
                            break;
                        case AGREED_V1:
                            break;
                    }
                }, throwable -> Timber.e(throwable, "Failed to obtain user gdpr consent status")));
    }

    private void startSignInActivityWithHomeParent() {
        Intent signInIntent = new Intent(this, SignInActivity.class);
        Intent mainScreenIntent = HomeActivity.newInstance(this, HomeNavigator.HomeScreen.EXPLORE);
        TaskStackBuilder.create(this)
                .addNextIntent(mainScreenIntent)
                .addNextIntent(signInIntent)
                .startActivities();
    }

    private void startSignInActivityCurrentParent() {
        Intent intent = new Intent(this, SignInActivity.class);
        TaskStackBuilder.create(this)
                .addNextIntent(getIntent())
                .addNextIntent(intent)
                .startActivities();
    }


    @Override
    protected void onStop() {
        disposables.clear();
        super.onStop();
    }

    private void setCrashliticsUserInfo() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            Crashlytics.setUserIdentifier(currentUser.getUid());
        }
    }

    @Override
    final public String loggedInUserId() {
        if (currentUserUid != null) {
            return currentUserUid;
        }
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            return currentUserUid;
        } else {
            return null;
        }
    }

    public AndroidInjector<Fragment> supportFragmentInjector() {
        return this.supportFragmentInjector;
    }

    public AndroidInjector<android.app.Fragment> fragmentInjector() {
        return this.frameworkFragmentInjector;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (backPressedManager.shouldBackButtonProceed()) {
            super.onBackPressed();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        finish();
    }
}
