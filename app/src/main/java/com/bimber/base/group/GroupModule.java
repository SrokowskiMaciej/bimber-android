package com.bimber.base.group;

import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 15.04.17.
 */
@Module
public class GroupModule {

    @Provides
    @GroupId
    public static String provideGroupId(GroupProvider groupProvider) {
        return groupProvider.provideGroupId();
    }
}
