package com.bimber.base.group;

import com.bimber.base.fragments.BaseFragment;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

/**
 * Created by maciek on 19.08.17.
 */

public class BaseGroupFragment extends BaseFragment {

    @Arg
    public String groupId;
}
