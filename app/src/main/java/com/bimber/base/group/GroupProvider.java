package com.bimber.base.group;

/**
 * Created by maciek on 14.05.17.
 */

public interface GroupProvider {
    String provideGroupId();
}
