package com.bimber.base.chat;

import com.bimber.data.entities.Chattable;

import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 15.04.17.
 */
@Module
public class ChatModule {

    @Provides
    @ChatId
    public static String provideChatId(ChatProvider chatProvider) {
        return chatProvider.provideChatId();
    }

    @Provides
    @ChatType
    public static Chattable.ChatType provideChatType(ChatProvider chatProvider) {
        return chatProvider.provideChatType();
    }
}
