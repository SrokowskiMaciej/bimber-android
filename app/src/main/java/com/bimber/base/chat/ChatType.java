package com.bimber.base.chat;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by maciek on 15.04.17.
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface ChatType {
}
