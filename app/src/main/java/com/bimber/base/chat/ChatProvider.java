package com.bimber.base.chat;

import com.bimber.data.entities.Chattable;

/**
 * Created by maciek on 25.04.17.
 */

public interface ChatProvider {

    String provideChatId();

    Chattable.ChatType provideChatType();
}
