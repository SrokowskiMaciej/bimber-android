package com.bimber.base;

import android.net.Uri;
import android.support.v4.content.FileProvider;

/**
 * Created by maciek on 18.05.17.
 */

public class ShareProvider extends FileProvider {

    public static final String SHARE_MIME_TYPE_IMAGE_JPEG = "image/jpeg";

    @Override
    public String getType(Uri uri) {
        return "image/jpeg";
    }
}
