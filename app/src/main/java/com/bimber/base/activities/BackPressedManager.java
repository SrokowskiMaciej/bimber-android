package com.bimber.base.activities;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by maciek on 17.02.17.
 */

public class BackPressedManager {

    private final Map<String, WeakReference<BackPressedListener>> backPressedListeners = new HashMap<>();
    private final PublishSubject<BackPress> backPressPublishSubject = PublishSubject.create();

    public boolean shouldBackButtonProceed() {
        backPressPublishSubject.onNext(BackPress.BACK_PRESS);
        for (String backPressedListenerKey : backPressedListeners.keySet()) {
            BackPressedListener backPressedListener = backPressedListeners.get(backPressedListenerKey).get();
            if (backPressedListener != null && !backPressedListener.shouldBackButtonProceed()) return false;
        }
        return true;
    }

    public void registerBackPressedListener(BackPressedListener backPressedListener) {
        backPressedListeners.put(backPressedListener.key(), new WeakReference<>(backPressedListener));
    }

    public Observable<BackPress> onBackButtonPressed() {
        return backPressPublishSubject;
    }

    public enum BackPress {
        BACK_PRESS
    }
}
