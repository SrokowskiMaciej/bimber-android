package com.bimber.base.activities;

/**
 * Created by maciek on 14.04.17.
 */

public interface BackPressedListener {
    String key();

    boolean shouldBackButtonProceed();
}
