package com.bimber.base.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.bimber.base.application.BimberApplication;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by srokowski.maciej@gmail.com on 22.10.16.
 */

public abstract class BaseActivity extends AppCompatActivity implements HasFragmentInjector, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> supportFragmentInjector;
    @Inject
    DispatchingAndroidInjector<android.app.Fragment> frameworkFragmentInjector;
    @Inject
    FirebaseAnalytics firebaseAnalytics;
    @Inject
    BackPressedManager backPressedManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BimberApplication.getRefWatcher(this).watch(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAnalytics.setCurrentScreen(this, getClass().getSimpleName(), null);
    }

    @Override
    public void onBackPressed() {
        if (backPressedManager.shouldBackButtonProceed()) {
            super.onBackPressed();
        }
    }

    public AndroidInjector<Fragment> supportFragmentInjector() {
        return this.supportFragmentInjector;
    }

    public AndroidInjector<android.app.Fragment> fragmentInjector() {
        return this.frameworkFragmentInjector;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
