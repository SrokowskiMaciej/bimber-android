package com.bimber.base.glide;

import android.content.Context;
import android.util.Log;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by maciek on 23.08.17.
 */
@GlideModule
public class BimberGlideModule extends AppGlideModule{

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
//        builder.setLogLevel(Log.VERBOSE);
        super.applyOptions(context, builder);
    }
}
