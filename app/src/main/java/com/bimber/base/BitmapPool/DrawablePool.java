package com.bimber.base.BitmapPool;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.WeakHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by maciek on 23.08.17.
 */

@Singleton
public class DrawablePool {

    private final WeakHashMap<String, Drawable> drawablePool = new WeakHashMap<>();

    @Inject
    public DrawablePool(){

    }

    public void putDrawable(String key, Drawable drawable) {
        drawablePool.put(key, drawable);
    }

    public void putDrawable(List<String> collageKey, Drawable drawable) {
        drawablePool.put(hashCollage(collageKey), drawable);
    }

    @Nullable
    public Drawable getDrawable(String key) {
        return drawablePool.get(key);
    }

    @Nullable
    public Drawable getDrawable(List<String> collageKey) {
        return drawablePool.get(hashCollage(collageKey));
    }

    public static String hashCollage(List<String> images){
        StringBuilder hashBuilder = new StringBuilder();
        for (String image : images) {
            hashBuilder.append(image);
        }
        return hashBuilder.toString();
    }

}
