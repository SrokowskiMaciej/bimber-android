package com.bimber.base.application;

import com.bimber.base.ViewModelModule;
import com.bimber.features.messaging.CloudMessagingInstanceIdService;
import com.bimber.features.messaging.CloudMessagingModule;
import com.bimber.features.messaging.CloudMessagingService;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {ApplicationModule.class, AndroidInjectionModule.class, AndroidSupportInjectionModule.class, AndroidComponentsModule
        .class, CloudMessagingModule.class, ViewModelModule.class})
public interface ApplicationComponent extends GlobalDependencies {

    void inject(BimberApplication bimberApplication);

    void inject(CloudMessagingInstanceIdService instanceIdService);

    void inject(CloudMessagingService cloudMessagingService);

}

