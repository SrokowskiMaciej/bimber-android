package com.bimber.base.application;

import android.arch.persistence.room.Room;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

import com.bimber.BuildConfig;
import com.bimber.base.activities.BackPressedManager;
import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.ServerValues;
import com.bimber.data.cache.BimberDatabase;
import com.bimber.data.deeplinking.DeeplinkShortenerService;
import com.bimber.data.deeplinking.model.AndroidInfo;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.geocoding.ReverseGeocodingService;
import com.bimber.data.sources.discovery.groups.NearbyGroupsNetworkSource;
import com.bimber.data.sources.discovery.groups.GroupDeepLinkRepository;
import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.data.repositories.MessagingTokenRepository;
import com.bimber.data.repositories.PhotoStorageRepository;
import com.bimber.data.repositories.Subdirectory;
import com.bimber.data.sources.chat.messages.MessagesLocalSource;
import com.bimber.data.sources.chat.messages.aggregated.MessagesWithSenderDataLocalSource;
import com.bimber.data.sources.discovery.users.NearbyDiscoverableUsersNetworkSource;
import com.bimber.data.sources.profile.aggregated.ProfileDataLocalSource;
import com.bimber.data.sources.profile.photos.UsersPhotosLocalSource;
import com.bimber.data.sources.profile.places.FavouritePlacesLocalSource;
import com.bimber.data.sources.profile.treats.FavouriteTreatsLocalSource;
import com.bimber.data.sources.profile.user.UsersLocalSource;
import com.bimber.domain.group.SetGroupPictureUseCase;
import com.bimber.domain.messages.SendImageMessageUseCase;
import com.bimber.domain.photos.UploadProfilePhotosUseCase;
import com.bimber.features.deeplinking.DeeplinkNavigator;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.zelory.compressor.Compressor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Provide application-level dependencies. Mainly singleton object that can be injected from
 * anywhere in the app.
 */
@Module
public class ApplicationModule {
    private final BimberApplication application;

    public ApplicationModule(BimberApplication application) {
        this.application = application;
    }

    @Singleton
    @Provides
    BimberDatabase database() {
        return Room.databaseBuilder(application, BimberDatabase.class, "bimber-databse")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    MessagesLocalSource messagesLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.messagesCacheDao();
    }

    @Singleton
    @Provides
    UsersLocalSource usersLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.usersLocalSource();
    }

    @Singleton
    @Provides
    UsersPhotosLocalSource usersPhotosLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.usersPhotosLocalSource();
    }

    @Singleton
    @Provides
    FavouritePlacesLocalSource favouritePlacesLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.favouritePlacesLocalSource();
    }

    @Singleton
    @Provides
    FavouriteTreatsLocalSource favouriteTreatsLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.favouriteTreatsLocalSource();
    }

    @Singleton
    @Provides
    ProfileDataLocalSource profileDataLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.profileDataLocalSource();
    }

    @Singleton
    @Provides
    MessagesWithSenderDataLocalSource messagesWithSenderDataLocalSource(BimberDatabase bimberDatabase) {
        return bimberDatabase.messagesWithSenderDataLocalSource();
    }

    @Singleton
    @Provides
    FirebaseAuth firebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Singleton
    @Provides
    FirebaseStorage firebaseStorage() {
        return FirebaseStorage.getInstance();
    }

    @Singleton
    @Provides
    FirebaseAnalytics firebaseAnalytics() {
        return FirebaseAnalytics.getInstance(application);
    }

    @Singleton
    @Provides
    FirebaseAuthService firebaseAuthService(FirebaseAuth firebaseAuth) {
        return new FirebaseAuthService(firebaseAuth);
    }

    @Singleton
    @Provides
    FirebaseDatabase firebaseDatabase() {
        final FirebaseDatabase instance = FirebaseDatabase.getInstance();
        //TODO LOL, they forgot to setDiscoverableUsers proguard rule for this
//        instance.setLogLevel(Logger.Level.NONE);
        instance.setPersistenceEnabled(false);
        return instance;
    }

    @Singleton
    @Subdirectory(UploadProfilePhotosUseCase.PROFILE_PHOTOS_STORAGE_SUBDIRECTORY)
    @Provides
    PhotoStorageRepository userPhotoStorageService(FirebaseStorage firebaseStorage, FirebaseDatabase firebaseDatabase, Compressor compressor) {
        return new PhotoStorageRepository(firebaseStorage, firebaseDatabase, compressor, UploadProfilePhotosUseCase.PROFILE_PHOTOS_STORAGE_SUBDIRECTORY);
    }

    @Singleton
    @Subdirectory(SendImageMessageUseCase.MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY)
    @Provides
    PhotoStorageRepository messagesPhotoStorageService(FirebaseStorage firebaseStorage, FirebaseDatabase firebaseDatabase, Compressor compressor) {
        return new PhotoStorageRepository(firebaseStorage, firebaseDatabase, compressor, SendImageMessageUseCase.MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY);
    }

    @Singleton
    @Subdirectory(SetGroupPictureUseCase.GROUPS_PHOTOS_STORAGE_SUBDIRECTORY)
    @Provides
    PhotoStorageRepository groupsPhotoStorageService(FirebaseStorage firebaseStorage, FirebaseDatabase firebaseDatabase, Compressor compressor) {
        return new PhotoStorageRepository(firebaseStorage, firebaseDatabase, compressor, SetGroupPictureUseCase.GROUPS_PHOTOS_STORAGE_SUBDIRECTORY);
    }

    @Singleton
    @Provides
    Compressor compressor() {
        return new Compressor.Builder(application)
                .setBitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    @Singleton
    @Provides
    NearbyGroupsNetworkSource groupLocationGeolocationRepository(FirebaseDatabase firebaseDatabase) {
        return new NearbyGroupsNetworkSource(firebaseDatabase);
    }

    @Singleton
    @Provides
    MessagingTokenRepository messagingTokenService(FirebaseDatabase firebaseDatabase) {
        return new MessagingTokenRepository(firebaseDatabase);
    }

    @Singleton
    @Provides
    RxSharedPreferences rxSharedPreferences(BimberApplication bimberApplication) {
        return RxSharedPreferences.create(PreferenceManager.getDefaultSharedPreferences(bimberApplication));
    }

    @Singleton
    @Provides
    GroupDeepLinkRepository forcedEvaluationRepository(RxSharedPreferences rxSharedPreferences) {
        return new GroupDeepLinkRepository(rxSharedPreferences);
    }

    @Singleton
    @Provides
    BimberApplication provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    BackPressedManager backPressedManager() {
        return new BackPressedManager();
    }

    @Provides
    @Singleton
    DefaultValues defaultValues(BimberApplication bimberApplication) {
        return new DefaultValues(bimberApplication);
    }

    @Singleton
    @Provides
    DeeplinkShortenerService deeplinkShortenerService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        return new Retrofit.Builder()
                .baseUrl("https://firebasedynamiclinks.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(DeeplinkShortenerService.class);
    }



    @Singleton
    @Provides
    ReverseGeocodingService reverseGeocodingService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        return new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ReverseGeocodingService.class);
    }

    @Singleton
    @Provides
    NearbyDiscoverableUsersNetworkSource nearbyDiscoverableUsersNetworkSource() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BIMER_API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(NearbyDiscoverableUsersNetworkSource.class);
    }

    @Singleton
    @Provides
    DeeplinkNavigator deeplinkNavigator() {
        return new DeeplinkNavigator();
    }

    @Singleton
    @Provides
    ServerValues values() {
        return new ServerValues(application);
    }

    @Singleton
    @Provides
    AndroidInfo androidInfo() {
        return AndroidInfo.create(BuildConfig.APPLICATION_ID);
    }
}
