package com.bimber.base.application;

import com.bimber.base.ActivityScope;
import com.bimber.base.BroadcastScope;
import com.bimber.base.FragmentScope;
import com.bimber.base.auth.LoggedInModule;
import com.bimber.base.group.GroupModule;
import com.bimber.base.profile.ProfileModule;
import com.bimber.features.chat.activity.ChatActivity;
import com.bimber.features.chat.activity.ChatActivityModule;
import com.bimber.features.chat.di.ChatViewFragmentModule;
import com.bimber.features.chat.dialogs.changegroupdescription.ChangeGroupDescriptionFragmentModule;
import com.bimber.features.chat.dialogs.changegrouplocation.ChangeGroupLocationFragmentModule;
import com.bimber.features.chat.dialogs.changegroupname.ChangeGroupNameFragmentModule;
import com.bimber.features.chat.dialogs.changegrouptime.ChangeGroupTimeModule;
import com.bimber.features.chat.dialogs.managegroupusers.ManageGroupUsersModule;
import com.bimber.features.chat.dialogs.managegroupusers.activity.ManageGroupUsersActivity;
import com.bimber.features.chat.dialogs.managegroupusers.activity.ManageGroupUsersActivityModule;
import com.bimber.features.chat.dialogs.setgrouppicture.SetGroupPictureFragmentModule;
import com.bimber.features.chatlist.di.PeopleChatListFragmentModule;
import com.bimber.features.creategroup.activity.CreateGroupActivity;
import com.bimber.features.creategroup.activity.CreateGroupActivityModule;
import com.bimber.features.creategroup.activity.CreatePartyInitializeActivity;
import com.bimber.features.creategroup.di.CreateGroupFragmentModule;
import com.bimber.features.gdprassurance.GdprAssuranceView;
import com.bimber.features.groupcandidates.activity.GroupJoinCandidatesActivity;
import com.bimber.features.groupcandidates.activity.GroupJoinCandidatesActivityModule;
import com.bimber.features.groupcandidates.di.GroupJoinCandidatesFragmentModule;
import com.bimber.features.groupdetails.publicvisibility.activity.PublicGroupActivity;
import com.bimber.features.groupdetails.publicvisibility.activity.PublicGroupActivityModule;
import com.bimber.features.groupdetails.publicvisibility.di.PublicGroupDetailsFragmentModule;
import com.bimber.features.groupdetails.restrictedvisibility.activity.RestrictedGroupActivity;
import com.bimber.features.groupdetails.restrictedvisibility.activity.RestrictedGroupActivityModule;
import com.bimber.features.home.activity.HomeActivity;
import com.bimber.features.home.activity.HomeActivityModule;
import com.bimber.features.home.di.HomeFragmentModule;
import com.bimber.features.launch.LauncherActivity;
import com.bimber.features.location.activity.LocationPickerActivity;
import com.bimber.features.location.activity.LocationPickerActivityModule;
import com.bimber.features.location.di.LocationPickerModule;
import com.bimber.features.messaging.groupmeeting.GroupMeetingNotificationReceiver;
import com.bimber.features.messaging.groupmeeting.di.GroupMeetingNotificationReceiverModule;
import com.bimber.features.messaging.groupparticipation.GroupParticipationNotificationReceiver;
import com.bimber.features.messaging.groupparticipation.di.GroupParticipationNotificationReceiverModule;
import com.bimber.features.messaging.groupparticipation.di.GroupParticipationSnackbarFragmentModule;
import com.bimber.features.messaging.match.MatchNotificationReceiver;
import com.bimber.features.messaging.match.di.MatchNotificationReceiverModule;
import com.bimber.features.messaging.match.di.MatchSnackbarFragmentModule;
import com.bimber.features.messaging.mute.MuteNotificationActivity;
import com.bimber.features.messaging.mute.MuteNotificationActivityModule;
import com.bimber.features.messaging.newchatmessages.InlineReplyNotificationReceiver;
import com.bimber.features.messaging.newchatmessages.NewChatMessageNotificationReceiver;
import com.bimber.features.messaging.newchatmessages.di.InlineReplyBroadcastReceiverModule;
import com.bimber.features.messaging.newchatmessages.di.NewChatMessageBroadcastReceiverModule;
import com.bimber.features.messaging.newchatmessages.di.NewChatMessageSnackbarFragmentModule;
import com.bimber.features.photogallery.chat.ChatPhotoGalleryFragmentModule;
import com.bimber.features.photogallery.chat.activity.ChatPhotoGalleryActivity;
import com.bimber.features.photogallery.chat.activity.ChatPhotoGalleryActivityModule;
import com.bimber.features.photogallery.edit.EditUserPhotoGalleryModule;
import com.bimber.features.photogallery.edit.activity.PhotoGalleryActivity;
import com.bimber.features.photogallery.edit.activity.PhotoGalleryActivityModule;
import com.bimber.features.photogallery.profile.ProfilePhotoGalleryFragmentModule;
import com.bimber.features.photogallery.profile.activity.ProfilePhotoGalleryActivity;
import com.bimber.features.photogallery.profile.activity.ProfilePhotoGalleryActivityModule;
import com.bimber.features.pictureeditor.EditImageActivity;
import com.bimber.features.profiledetails.activity.ProfileActivity;
import com.bimber.features.profiledetails.activity.ProfileActivityModule;
import com.bimber.features.share.group.ShareGroupFragmentModule;
import com.bimber.features.share.group.ShareGroupView;
import com.bimber.features.share.profile.ShareProfileFragmentModule;
import com.bimber.features.share.profile.ShareProfileView;
import com.bimber.features.sharereceiver.ShareReceiverActivity;
import com.bimber.features.sharereceiver.ShareReceiverActivityModule;
import com.bimber.features.signin.SignInActivity;
import com.bimber.features.signin.auth.AuthView;
import com.bimber.features.signin.gdprconsent.GdprConsentView;
import com.bimber.features.signin.welcome.WelcomeView;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maciek on 10.05.17.
 */
@Module
public abstract class AndroidComponentsModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract SignInActivity contributeSignInActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract GdprAssuranceView contributeGdprAssuranceView();

    @FragmentScope
    @ContributesAndroidInjector
    abstract WelcomeView contributeWelcomeView();

    @FragmentScope
    @ContributesAndroidInjector
    abstract GdprConsentView contributeGdprConsentView();

    @FragmentScope
    @ContributesAndroidInjector
    abstract AuthView contributeAuthView();

    @ActivityScope
    @ContributesAndroidInjector
    abstract LauncherActivity contributeLauncherActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {HomeActivityModule.class, LoggedInModule.class, PublicGroupDetailsFragmentModule.class,
            HomeFragmentModule.class,
            NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class, GroupParticipationSnackbarFragmentModule.class})
    abstract HomeActivity contributeMainScreenActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ChatActivityModule.class, LoggedInModule.class, ChatViewFragmentModule.class,
            ChatPhotoGalleryFragmentModule.class,
            NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class, GroupParticipationSnackbarFragmentModule.class})
    abstract ChatActivity contributeChatActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ChatPhotoGalleryActivityModule.class, LoggedInModule.class, ChatPhotoGalleryFragmentModule
            .class})
    abstract ChatPhotoGalleryActivity contributeChatPhotoGalleryActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ProfilePhotoGalleryActivityModule.class, LoggedInModule.class, ProfilePhotoGalleryFragmentModule
            .class})
    abstract ProfilePhotoGalleryActivity contributeProfilePhotoGalleryActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract EditImageActivity contributeEditImageActivity();


    @ActivityScope
    @ContributesAndroidInjector(modules = {LocationPickerActivityModule.class, LoggedInModule.class, LocationPickerModule.class})
    abstract LocationPickerActivity contributeLocationPickerActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ProfileActivityModule.class, LoggedInModule.class,
            NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class, GroupParticipationSnackbarFragmentModule.class})
    abstract ProfileActivity contributeProfileActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {RestrictedGroupActivityModule.class, LoggedInModule.class, ChangeGroupTimeModule.class,
            ChangeGroupNameFragmentModule.class, ChangeGroupDescriptionFragmentModule.class, ChangeGroupLocationFragmentModule.class,
            SetGroupPictureFragmentModule.class, NewChatMessageSnackbarFragmentModule.class})
    abstract RestrictedGroupActivity contributeRestrictedGroupActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {PublicGroupActivityModule.class, LoggedInModule.class})
    abstract PublicGroupActivity contributePublicGroupActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {PhotoGalleryActivityModule.class, LoggedInModule.class, EditUserPhotoGalleryModule.class,
            NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class, GroupParticipationSnackbarFragmentModule.class})
    abstract PhotoGalleryActivity contributePhotoGalleryActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {GroupJoinCandidatesActivityModule.class, LoggedInModule.class,
            GroupJoinCandidatesFragmentModule.class, NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class,
            GroupParticipationSnackbarFragmentModule.class})
    abstract GroupJoinCandidatesActivity contributeGroupJoinRequestsActivity();

    @ActivityScope
    @ContributesAndroidInjector()
    abstract CreatePartyInitializeActivity contributeCreatePartyInitializeActivity();


    @ActivityScope
    @ContributesAndroidInjector(modules = {CreateGroupActivityModule.class, LoggedInModule.class, CreateGroupFragmentModule.class,
            NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class, GroupParticipationSnackbarFragmentModule.class})
    abstract CreateGroupActivity contributeCreateGroupActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ManageGroupUsersActivityModule.class, LoggedInModule.class,
            ManageGroupUsersModule.class, NewChatMessageSnackbarFragmentModule.class, MatchSnackbarFragmentModule.class,
            GroupParticipationSnackbarFragmentModule.class})
    abstract ManageGroupUsersActivity contributeManageGroupUsersActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ShareReceiverActivityModule.class, LoggedInModule.class, PeopleChatListFragmentModule.class})
    abstract ShareReceiverActivity contributeShareReceiverActivity();

    @BroadcastScope
    @ContributesAndroidInjector(modules = {MatchNotificationReceiverModule.class, LoggedInModule.class})
    abstract MatchNotificationReceiver contributeMatchNotificationReceiver();


    @BroadcastScope
    @ContributesAndroidInjector(modules = {NewChatMessageBroadcastReceiverModule.class, LoggedInModule.class})
    abstract NewChatMessageNotificationReceiver contributeNewChatMessageNotificationReceiver();

    @BroadcastScope
    @ContributesAndroidInjector(modules = {InlineReplyBroadcastReceiverModule.class, LoggedInModule.class})
    abstract InlineReplyNotificationReceiver contributeInlineReplyReceiver();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MuteNotificationActivityModule.class, LoggedInModule.class})
    abstract MuteNotificationActivity contributeMuteNotificationActivity();


    @BroadcastScope
    @ContributesAndroidInjector(modules = {GroupParticipationNotificationReceiverModule.class, LoggedInModule.class})
    abstract GroupParticipationNotificationReceiver contributeGroupParticipationReceiver();

    @BroadcastScope
    @ContributesAndroidInjector(modules = {GroupMeetingNotificationReceiverModule.class, LoggedInModule.class})
    abstract GroupMeetingNotificationReceiver contributeGroupMeetingReceiver();

    @FragmentScope
    @ContributesAndroidInjector(modules = {ShareProfileFragmentModule.class, ProfileModule.class})
    abstract ShareProfileView contributeShareProfileView();

    @FragmentScope
    @ContributesAndroidInjector(modules = {ShareGroupFragmentModule.class, GroupModule.class})
    abstract ShareGroupView contributeShareGroupView();

}
