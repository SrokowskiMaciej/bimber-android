package com.bimber.base.application;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;

import com.bimber.BuildConfig;
import com.bimber.R;
import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.data.repositories.MessagingTokenRepository;
import com.bimber.features.messaging.CloudMessagingInstanceIdService;
import com.bimber.utils.crashlitics.CrashlyticsTree;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.google.GoogleEmojiProvider;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasBroadcastReceiverInjector;
import dagger.android.HasServiceInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.fabric.sdk.android.Fabric;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import rx.plugins.RxJavaHooks;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by srokowski.maciej@gmail.com on 22.10.16.
 */

public class BimberApplication extends Application implements HasActivityInjector, HasSupportFragmentInjector,
        HasBroadcastReceiverInjector, HasServiceInjector {

    @Inject
    FirebaseDatabase firebaseDatabase;
    @Inject
    FirebaseAuthService firebaseAuthService;
    @Inject
    RxSharedPreferences sharedPreferences;
    @Inject
    MessagingTokenRepository messagingTokenRepository;
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingFragmentInjector;
    @Inject
    DispatchingAndroidInjector<BroadcastReceiver> dispatchingBroadcastReceiverInjector;
    @Inject
    DispatchingAndroidInjector<Service> dispatchingServiceInjector;

    public static final String DEFAULT_FONT_ASSET_PATH = "fonts/Nunito-SemiBold.ttf";
    private static ApplicationComponent applicationComponent;
    private static Typeface defaultTypeface;
    private RefWatcher refWatcher;
    public static final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
            .create();

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        refWatcher = LeakCanary.install(this);
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsTree());
        }
        RxJavaHooks.setOnError(throwable -> Timber.e(throwable, "Caughts by global RxJava error handler"));
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);


        defaultTypeface = Typeface.createFromAsset(getAssets(), DEFAULT_FONT_ASSET_PATH);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(DEFAULT_FONT_ASSET_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build());
        com.bimber.utils.emoji.EmojiManager.init(this);
        EmojiManager.install(new GoogleEmojiProvider());
        registerGlobalActions();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static RefWatcher getRefWatcher(Context context) {
        BimberApplication application = (BimberApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    public static ApplicationComponent getAppComponent() {
        return applicationComponent;
    }

    public static Typeface getDefaultTypeface() {
        return defaultTypeface;
    }

    private void registerGlobalActions() {
        final Observable<FirebaseUser> loggedInUser = firebaseAuthService.onUserLoggedIn().share();

        loggedInUser
                .subscribe(new Observer<FirebaseUser>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(FirebaseUser firebaseUser) {
                        firebaseDatabase.goOnline();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Failed to go online on sign in, retry");
                        firebaseDatabase.goOnline();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        loggedInUser
                .flatMapCompletable(firebaseUser -> {
                    String token = CloudMessagingInstanceIdService.getTokenFromPrefs();
                    return messagingTokenRepository.setToken(firebaseUser.getUid(), token);
                })
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Timber.d("New token inserted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Failed to go online on sign in, retry");
                        firebaseDatabase.goOnline();
                    }
                });
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    @Override
    public DispatchingAndroidInjector<BroadcastReceiver> broadcastReceiverInjector() {
        return dispatchingBroadcastReceiverInjector;
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingFragmentInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return dispatchingServiceInjector;
    }
}
