package com.bimber.base.application;

import com.bimber.base.activities.BackPressedManager;
import com.bimber.data.deeplinking.DeeplinkShortenerService;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.sources.profile.location.UserLocationNetworkSource;
import com.bimber.data.sources.profile.places.FavouritePlacesNetworkSource;
import com.bimber.data.sources.profile.treats.FavouriteTreatsNetworkSource;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.sources.discovery.groups.GroupDeepLinkRepository;
import com.bimber.data.repositories.DialogRepository;
import com.bimber.data.repositories.FirebaseAuthService;
import com.bimber.data.sources.discovery.groups.NearbyGroupsNetworkSource;
import com.bimber.data.repositories.GroupJoinRequestRepository;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.repositories.LastSeenMessagesRepository;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.repositories.MessagingTokenRepository;
import com.bimber.data.repositories.PersonEvaluationRepository;
import com.bimber.data.sources.profile.photos.UsersPhotosNetworkSource;
import com.bimber.data.repositories.PhotoStorageRepository;
import com.bimber.data.repositories.Subdirectory;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.data.sources.profile.user.UserNetworkSource;
import com.bimber.domain.photos.UploadProfilePhotosUseCase;
import com.bimber.features.messaging.CloudMessageDataHandler;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.Set;

/**
 * Created by maciek on 07.02.17.
 */
public interface GlobalDependencies {

    BimberApplication application();

    //Exposed to sub-graphs
    FirebaseAuth firebaseAuth();

    FirebaseStorage firebaseStorage();

    FirebaseAuthService firebaseAuthService();

    FirebaseDatabase firebaseDatabase();

    UserNetworkSource userService();

    UsersPhotosNetworkSource photoService();

    @Subdirectory(UploadProfilePhotosUseCase.PROFILE_PHOTOS_STORAGE_SUBDIRECTORY)
    PhotoStorageRepository photoStorageRepository();

    FavouriteTreatsNetworkSource alcoholService();

    FavouritePlacesNetworkSource favouritePlacesService();

    UserLocationNetworkSource locationService();

    MessagingTokenRepository messagingTokenService();

    Set<CloudMessageDataHandler> messagingDataHandlers();

    UserChatsRepository userChatsRepository();

    ChatMemberRepository chatMemberRepository();

    DialogRepository matchRepository();

    GroupRepository groupRepository();

    MessageRepository messageRepository();

    PersonEvaluationRepository evaluatedUsersService();

    GroupJoinRequestRepository groupJoinRequestRepository();

    LastSeenMessagesRepository lastSeenMessagesRepository();

    BackPressedManager backPressManager();

    NearbyGroupsNetworkSource groupGeolocationRepository();

    DeeplinkShortenerService deeplinkShortenerService();

    RxSharedPreferences rxSharedPreferences();

    GroupDeepLinkRepository forcedEvaluationRepository();

    DefaultValues defaultValues();
}
