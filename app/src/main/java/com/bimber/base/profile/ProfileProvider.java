package com.bimber.base.profile;

/**
 * Created by maciek on 14.05.17.
 */

public interface ProfileProvider {
    String provideProfileId();
}
