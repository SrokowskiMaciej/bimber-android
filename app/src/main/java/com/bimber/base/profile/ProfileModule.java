package com.bimber.base.profile;

import dagger.Module;
import dagger.Provides;

/**
 * Created by maciek on 15.04.17.
 */
@Module
public class ProfileModule {

    @Provides
    @ProfileId
    public static String provideProfileId(ProfileProvider profileProvider) {
        return profileProvider.provideProfileId();
    }
}
