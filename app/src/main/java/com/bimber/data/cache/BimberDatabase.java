package com.bimber.data.cache;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.sources.chat.messages.MessagesLocalSource;
import com.bimber.data.sources.chat.messages.aggregated.MessagesWithSenderDataLocalSource;
import com.bimber.data.sources.profile.aggregated.ProfileDataLocalSource;
import com.bimber.data.sources.profile.photos.UsersPhotosLocalSource;
import com.bimber.data.sources.profile.places.FavouritePlacesLocalSource;
import com.bimber.data.sources.profile.treats.FavouriteTreatsLocalSource;
import com.bimber.data.sources.profile.user.UsersLocalSource;

/**
 * Created by maciek on 23.11.17.
 */

@Database(entities = {Message.class, User.class, UserPhoto.class, FavouritePlace.class, FavouriteTreat.class}, version = 2)
public abstract class BimberDatabase extends RoomDatabase {
    public abstract MessagesLocalSource messagesCacheDao();

    public abstract UsersLocalSource usersLocalSource();

    public abstract UsersPhotosLocalSource usersPhotosLocalSource();

    public abstract FavouritePlacesLocalSource favouritePlacesLocalSource();

    public abstract FavouriteTreatsLocalSource favouriteTreatsLocalSource();

    public abstract ProfileDataLocalSource profileDataLocalSource();

    public abstract MessagesWithSenderDataLocalSource messagesWithSenderDataLocalSource();
}
