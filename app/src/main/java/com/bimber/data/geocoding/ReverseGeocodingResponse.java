package com.bimber.data.geocoding;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by maciek on 04.09.17.
 */
@AutoValue
public abstract class ReverseGeocodingResponse {


    public abstract String status();
    public abstract List<Result> results();

    public static TypeAdapter<ReverseGeocodingResponse> typeAdapter(Gson gson) {
        return new AutoValue_ReverseGeocodingResponse.GsonTypeAdapter(gson);
    }

    @AutoValue
    public abstract static class AddressComponent {
        @SerializedName("long_name")
        public abstract String longName();

        @SerializedName("short_name")
        public abstract String shortName();

        public abstract List<String> types();

        public static TypeAdapter<AddressComponent> typeAdapter(Gson gson) {
            return new AutoValue_ReverseGeocodingResponse_AddressComponent.GsonTypeAdapter(gson);
        }
    }

    @AutoValue
    public abstract static class Result {
        @SerializedName("address_components")
        public abstract List<AddressComponent> addressComponents();

        public static TypeAdapter<Result> typeAdapter(Gson gson) {
            return new AutoValue_ReverseGeocodingResponse_Result.GsonTypeAdapter(gson);
        }
    }
}
