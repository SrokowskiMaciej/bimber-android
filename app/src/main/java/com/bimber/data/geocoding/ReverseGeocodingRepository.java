package com.bimber.data.geocoding;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.bimber.R;
import com.bimber.data.geocoding.ReverseGeocodingResponse.AddressComponent;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by maciek on 04.09.17.
 */
@Singleton
public class ReverseGeocodingRepository {

    private final ReverseGeocodingService reverseGeocodingService;

    @Inject
    public ReverseGeocodingRepository(ReverseGeocodingService reverseGeocodingService) {
        this.reverseGeocodingService = reverseGeocodingService;
    }


    public Single<String> geocode(Context context, double latitude, double longitude) {
        return geocoderResult(context, latitude, longitude)
                .onErrorResumeNext(playServicesResult(context, latitude, longitude))
                .onErrorResumeNext(defaultResult(context));
    }

    private Single<String> geocoderResult(Context context, double latitude, double longitude) {
        return Single.fromCallable(() -> new Geocoder(context).getFromLocation(latitude, longitude, 1))
                .subscribeOn(Schedulers.io())
                .flatMap(addresses -> formatGeocoderResult(context, addresses));
    }

    private Single<String> playServicesResult(Context context, double latitude, double longitude) {
        return reverseGeocodingService.geocode(String.format("%s,%s", latitude, longitude),
                context.getString(R.string.geocoding_fallback_key))
                .subscribeOn(Schedulers.io())
                .flatMap(reverseGeocodingResponse -> {
                    if (reverseGeocodingResponse.status().equals("OK")) {
                        return Observable.fromIterable(reverseGeocodingResponse.results())
                                .firstOrError()
                                .flatMap(result -> formatPlayServicesResult(context, result.addressComponents()));
                    } else {
                        return Single.error(new IllegalArgumentException("Failed to obtain geocoding"));
                    }
                });
    }

    private Single<String> defaultResult(Context context) {
        return Single.fromCallable(() -> {
            String city = context.getString(R.string.view_location_picker_unknown_city_default);
            String country = context.getString(R.string.view_location_picker_unknown_country_default);
            return String.format(context.getString(R.string.view_location_picker_formatted_location_format), city, country);
        });
    }

    private Single<String> formatPlayServicesResult(Context context, List<AddressComponent> addresses) {
        Single<String> citySingle = Observable.fromIterable(addresses)
                .filter(addressComponent -> addressComponent.types().contains("locality"))
                .map(AddressComponent::longName)
                .single(context.getString(R.string.view_location_picker_unknown_city_default));

        Single<String> countrySingle = Observable.fromIterable(addresses)
                .filter(addressComponent -> addressComponent.types().contains("country"))
                .map(AddressComponent::longName)
                .single(context.getString(R.string.view_location_picker_unknown_country_default));

        return Single.zip(citySingle, countrySingle, (city, country) -> String.format(context
                .getString(R.string.view_location_picker_formatted_location_format), city, country));
    }

    private Single<String> formatGeocoderResult(Context context, List<Address> addresses) {
        Single<String> citySingle = Single.fromCallable(() -> addresses.get(0).getLocality())
                .onErrorReturnItem(context.getString(R.string.view_location_picker_unknown_city_default));

        Single<String> countrySingle = Single.fromCallable(() -> addresses.get(0).getCountryName())
                .onErrorReturnItem(context.getString(R.string.view_location_picker_unknown_country_default));

        return Single.zip(citySingle, countrySingle, (city, country) -> String.format(context
                .getString(R.string.view_location_picker_formatted_location_format), city, country));
    }
}
