package com.bimber.data.geocoding;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by maciek on 04.09.17.
 */

public interface ReverseGeocodingService {

    @GET("maps/api/geocode/json")
    Single<ReverseGeocodingResponse> geocode(@Query("latlng") String latlng, @Query("key") String apiKey);
}
