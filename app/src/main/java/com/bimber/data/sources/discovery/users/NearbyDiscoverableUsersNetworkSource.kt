package com.bimber.data.sources.discovery.users

import com.bimber.data.entities.discovery.NearbyDiscoverableUsers
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Maciek on 26/02/2018.
 */
interface NearbyDiscoverableUsersNetworkSource {

    @GET("nearbyDiscoverableUsers/{uId}")
    @Headers("Content-Type: application/json")
    fun nearbyDiscoverableUsers(@Path("uId") uId: String,
                                @Query("latitude") latitude: Double,
                                @Query("longitude") longitude: Double,
                                @Query("range") range: Int): Single<NearbyDiscoverableUsers>;
}