package com.bimber.data.sources.discovery.users

import com.bimber.data.entities.discovery.NearbyDiscoverableUsers
import com.bimber.utils.rx.Entity
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciejsrokowski on 3/14/18.
 */
@Singleton
class NearbyDiscoverableUsersRepository
@Inject constructor(private val nearbyDiscoverableUsersNetworkSource: NearbyDiscoverableUsersNetworkSource,
                    private val nearbyDiscoverableUsersInMemorySource: NearbyDiscoverableUsersInMemorySource) {

    private val locationRequest = BehaviorProcessor.create<Request>()
    private val moreUsersRequest = BehaviorProcessor.create<Entity>()
    private val nearbyDiscoverableUsers = Flowable.combineLatest(locationRequest.serialize(), moreUsersRequest.serialize(),
            BiFunction<Request, Entity, Request> { location, _ -> location })
            .switchMap {
                nearbyDiscoverableUsersNetworkSource.nearbyDiscoverableUsers(it.uId, it.latitude, it.longitude, it.range)
                        .toFlowable()
                        .map<NetworkResult> { users -> NetworkResult.Success(users) }
                        .onErrorReturn { error -> NetworkResult.Error(error) }
            }
            .switchMap { result ->
                when (result) {
                    is NetworkResult.Loading -> {
                        nearbyDiscoverableUsersInMemorySource.onDiscoverableUsers()
                                .map { Result.Loading(it) }
                    }
                    is NetworkResult.Success -> {
                        val changedInMemoryContents = nearbyDiscoverableUsersInMemorySource.addUsers(result.users.discoverableUsers)
                        if (!changedInMemoryContents) {
                            nearbyDiscoverableUsersInMemorySource.markDepleted()
                        }
                        nearbyDiscoverableUsersInMemorySource.onDiscoverableUsers()
                                .map { Result.Success(it, !changedInMemoryContents) }
                    }
                    is NetworkResult.Error -> {
                        nearbyDiscoverableUsersInMemorySource.onDiscoverableUsers()
                                .map { Result.Error(it, result.error) }
                    }
                }
            }
            .replay(1)

    fun onNearbyDiscoverableUsers(): Flowable<Result> {
        return nearbyDiscoverableUsers.distinctUntilChanged()
    }

    fun requestUsersAtLocation(request: Request) {
        nearbyDiscoverableUsers.connect()
        this.locationRequest.onNext(request)
    }

    fun requestMoreUsers() {
        nearbyDiscoverableUsers.connect()
        this.moreUsersRequest.onNext(Entity.INSTANCE)
    }

    fun removeUser(uId: String): Int {
        return nearbyDiscoverableUsersInMemorySource.removeUser(uId)
    }

    fun isDepleted(): Flowable<Boolean> {
        return nearbyDiscoverableUsersInMemorySource.isDepleted()
    }

    fun clear() {
        nearbyDiscoverableUsersInMemorySource.clear()
    }


    data class Request(val uId: String,
                       val latitude: Double,
                       val longitude: Double,
                       val range: Int)

    sealed class Result {
        abstract val users: Set<String>

        data class Loading(override val users: Set<String>) : Result()
        data class Success(override val users: Set<String>, val noMoreUsers: Boolean) : Result()
        data class Error(override val users: Set<String>, val error: Throwable) : Result()
    }

    private sealed class NetworkResult {
        class Loading : NetworkResult()
        data class Success(val users: NearbyDiscoverableUsers) : NetworkResult()
        data class Error(val error: Throwable) : NetworkResult()

    }

}