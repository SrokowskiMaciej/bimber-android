package com.bimber.data.sources.chat.messages

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.chat.membership.ChatMembership
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus.EXPIRED
import com.bimber.data.entities.chat.messages.common.ContentType
import com.bimber.data.entities.chat.messages.common.image.ImageContent
import com.bimber.data.entities.chat.messages.local.Message
import com.bimber.data.entities.chat.messages.network.MessageModel
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class MessageRepository @Inject
constructor(private val messagesNetworkSource: MessagesNetworkSource,
            private val messagesLocalSource: MessagesLocalSource,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    /**
     * WARNING We shouldn't let people just remove messages, update it with empty text or something instead. So that moving pagination
     * window won't start or end at the content that is not there
     */

    fun <T> getNetworkToCachePipe(chatId: String, messageId: String? = null, limitLast: Int = 1, contentType: ContentType? = null,
                                  onMessage: (Message) -> Flowable<T> = { Flowable.empty<T>() }): Flowable<Entity> {
        if (messageId != null) {
            return messagesNetworkSource.onChatMessage(chatId = chatId, messageId = messageId)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .switchMap {
                        //TODO Hmmm, shouldn't we subscribe before inserting to db. Just an Optimization tip
                        val message = Message(chatId, it)
                        messagesLocalSource.insertMessage(message)
                        onMessage(message)
                    }
                    .map { _ -> Entity.INSTANCE }
        } else {
            return messagesNetworkSource.onChatMessages(chatId = chatId,
                    limitLast = limitLast,
                    contentType = contentType,
                    onMessage = { messageModel -> onMessage(Message(chatId, messageModel)) })
                    .map { messages -> messagesLocalSource.insertMessages(messages.map { Message(chatId, it) }) }
                    .map { _ -> Entity.INSTANCE }
        }
    }

    @JvmOverloads
    fun onChatMessages(chatMembership: ChatMembership, limitLast: Int, contentType: ContentType? = null): Flowable<List<Message>> {
        if (chatMembership.membershipStatus() == EXPIRED) {
            return Flowable.just(emptyList())
        }
        val pipe = getNetworkToCachePipe<Entity>(chatId = chatMembership.chatId(), limitLast = limitLast, contentType = contentType)
                .listen()
        val cacheSource = if (contentType == null) {
            messagesLocalSource.onMessages(chatMembership.chatId(), limitLast)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        } else {
            messagesLocalSource.onMessages(chatMembership.chatId(), contentType.toString(), limitLast)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        }
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    fun message(chatMembership: ChatMembership, messageId: String): Maybe<Message> {
        if (chatMembership.membershipStatus() == EXPIRED) {
            return Maybe.empty()
        }
        //Don't dispose, let it download whatever it wants to download
        getNetworkToCachePipe<Entity>(chatMembership.chatId(), messageId)
                .firstOrError()
                .subscribeBy(onError = { Timber.e(it, "Failed to fetch messages") })

        return messagesLocalSource.onMessage(chatMembership.chatId(), messageId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .firstElement()
    }

    fun insertMessage(chatMembership: ChatMembership, message: MessageModel): Single<String> {
        return if (chatMembership.membershipStatus() == EXPIRED) {
            Single.error(IllegalArgumentException("Chat member ship status is expierd, can't send messages"))
        } else {
            messagesNetworkSource.sendMessage(chatMembership.chatId(), message)
        }
    }

    fun removeMessage(chatMembership: ChatMembership, messageId: String): Completable {
        return if (chatMembership.membershipStatus() == EXPIRED) {
            Completable.error(IllegalArgumentException("Chat member ship status is expierd, can't send messages"))
        } else {
            messagesNetworkSource.removeMessage(chatMembership.chatId(), messageId)
        }
    }

    fun updateImageMessageUploadStatus(chatMembership: ChatMembership,
                                       messageId: String,
                                       uploadStatus: ImageContent.UploadStatus,
                                       uri: String?): Completable {
        return if (chatMembership.membershipStatus() == EXPIRED) {
            Completable.error(IllegalArgumentException("Chat member ship status is expierd, can't send messages"))
        } else {
            messagesNetworkSource.updateImageMessageUploadStatus(chatMembership.chatId(), messageId, uploadStatus, uri)
        }
    }


}
