package com.bimber.data.sources.chat.messages.aggregated

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.chat.membership.ChatMembership
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus.EXPIRED
import com.bimber.data.entities.chat.messages.common.ContentType
import com.bimber.data.entities.chat.messages.local.aggregated.MessageWithSenderData
import com.bimber.data.sources.chat.messages.MessageRepository
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository
import com.bimber.utils.rx.listen
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class MessagesWithSenderDataRepository @Inject
constructor(private val messageRepository: MessageRepository,
            private val profileDataRepository: ProfileDataRepository,
            private val messagesWithSenderDataLocalSource: MessagesWithSenderDataLocalSource,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    @JvmOverloads
    fun onChatMessagesWithSender(chatMembership: ChatMembership, limitLast: Int, contentType: ContentType? = null): Flowable<List<MessageWithSenderData>> {
        if (chatMembership.membershipStatus() == EXPIRED) {
            return Flowable.just(emptyList())
        }
        val chatId = chatMembership.chatId()
        val pipe = messageRepository.getNetworkToCachePipe(chatId = chatMembership.chatId(),
                limitLast = limitLast,
                contentType = contentType,
                onMessage = { message -> profileDataRepository.getThumbnailNetworkToCachePipe(message.userId) })
                .listen()

        val cacheSource = if (contentType == null) {
            messagesWithSenderDataLocalSource.onMessages(chatId, limitLast)
                    .distinctUntilChanged()
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        } else {
            messagesWithSenderDataLocalSource.onMessages(chatMembership.chatId(), contentType.toString(), limitLast)
                    .distinctUntilChanged()
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        }
        return Flowable.combineLatest(pipe, cacheSource,
                BiFunction { _, values -> values })
    }
}
