package com.bimber.data.sources.profile.lastSeen

import com.bimber.utils.firebase.Timestamp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ServerValue
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by srokowski.maciej@gmail.com on 30.11.16.
 */
@Singleton
class LastUserActivityNetworkSource @Inject
constructor(private val firebaseDatabase: FirebaseDatabase) {

    private fun lastSeenNode(): DatabaseReference {
        return firebaseDatabase.getReference(LAST_SEEN_NODE)
    }

    private fun userLastSeenNode(uid: String): DatabaseReference {
        return lastSeenNode().child(uid)
    }

    fun lastSeen(uid: String): Single<Long> {
        return RxFirebaseDatabase.observeSingleValueEvent(userLastSeenNode(uid),
                { if (it.exists()) it.getValue(Long::class.java) else 0 })
    }

    fun updateLastSeen(userId: String): Completable {
        return RxFirebaseDatabase.setValue(userLastSeenNode(userId), ServerValue.TIMESTAMP)
    }

    companion object {
        private val LAST_SEEN_NODE = "last_user_activity"
    }
}
