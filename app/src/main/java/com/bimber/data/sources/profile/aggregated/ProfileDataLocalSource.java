package com.bimber.data.sources.profile.aggregated;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;

import io.reactivex.Flowable;

/**
 * Created by Maciek on 25/01/2018.
 */
@Dao
public interface ProfileDataLocalSource {

    @Query("SELECT * FROM users, users_photos WHERE uId = :userId AND photoUserId = :userId ORDER BY userPhotoIndex  LIMIT 1")
    Flowable<ProfileDataThumbnail> onUserProfileDataThumbnail(String userId);

    @Transaction
    @Query("SELECT * FROM users WHERE uId = :userId")
    Flowable<ProfileDataFull> onUserProfileDataFull(String userId);
}
