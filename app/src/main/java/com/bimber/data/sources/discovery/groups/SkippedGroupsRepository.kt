package com.bimber.data.sources.discovery.groups

import com.bimber.data.BackgroundSchedulerProvider
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciejsrokowski on 3/24/18.
 */
@Singleton
class SkippedGroupsRepository
@Inject
constructor(backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    private val skippedGroupProcessor = BehaviorProcessor.create<SkippableGroupAction>()
    private val skippedGroups = skippedGroupProcessor
            .observeOn(backgroundSchedulerProvider.backgroundScheduler())
            .serialize()
            .scan(mutableListOf<SkippedGroup>(), { accumulator, action ->
                when (action) {
                    is SkippableGroupAction.SkipGroupAction -> {
                        val skippedGroup = accumulator.find { it.groupId == action.groupId }
                        if (skippedGroup != null) {
                            accumulator.remove(skippedGroup)
                            accumulator.add(SkippedGroup(skippedGroup.groupId, skippedGroup.skipCounter + 1))
                        } else {
                            accumulator.add(SkippedGroup(action.groupId, 1))
                        }
                    }
                    is SkippableGroupAction.ClearGroupAction -> {
                        accumulator.removeAll { it.groupId == action.groupId }
                    }
                    is SkippableGroupAction.ClearAllAction -> {
                        accumulator.clear()
                    }
                }
                accumulator
            })
            .map { it.toList() }

    fun addSkippedGroup(group: String) {
        skippedGroupProcessor.onNext(SkippableGroupAction.SkipGroupAction(group))
    }

    fun clearSkippedGroup(group: String) {
        skippedGroupProcessor.onNext(SkippableGroupAction.ClearGroupAction(group))
    }

    fun clear() {
        skippedGroupProcessor.onNext(SkippableGroupAction.ClearAllAction())
    }

    fun onSkippedGroups(): Flowable<List<SkippedGroup>> {
        return skippedGroups
    }


    private sealed class SkippableGroupAction {
        data class SkipGroupAction(val groupId: String) : SkippableGroupAction()
        data class ClearGroupAction(val groupId: String) : SkippableGroupAction()
        class ClearAllAction : SkippableGroupAction()
    }

    data class SkippedGroup(val groupId: String, val skipCounter: Int)

}