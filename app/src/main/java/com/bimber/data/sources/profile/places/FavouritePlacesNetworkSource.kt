package com.bimber.data.sources.profile.places

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.network.FavouritePlaceModel
import com.bimber.data.entities.profile.network.FavouriteTreatModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Singleton
class FavouritePlacesNetworkSource
@Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {


    fun favouritePlacesNode(): DatabaseReference {
        return firebaseDatabase.getReference(FAVOURITE_PLACES_NODE)
    }

    fun userFavouritePlacesNode(uid: String): DatabaseReference {
        return favouritePlacesNode().child(uid)
    }

    fun onUserFavouritePlacesEvents(uid: String): Flowable<List<Key<FavouritePlaceModel>>> {
        val query = userFavouritePlacesNode(uid).orderByChild(FavouriteTreatModel.INDEX_FIELD_NAME)
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper { FavouritePlaceModel.create(it) })
    }

    fun userFavouritePlaces(uid: String): Single<List<Key<FavouritePlaceModel>>> {
        val query = userFavouritePlacesNode(uid).orderByChild(FavouriteTreatModel.INDEX_FIELD_NAME)
        return RxFirebaseDatabase.observeSingleValueEvent(query, RxFirebaseUtils.childListMapper { FavouritePlaceModel.create(it) })
    }

    fun insertFavouritePlace(uid: String, place: FavouritePlaceModel): Completable {
        val push = userFavouritePlacesNode(uid).push()
        return RxFirebaseDatabase.setValue(push, place.toFirebaseValue())
    }

    fun updateFavouritePlaces(uid: String, favouritePlaces: List<Key<FavouritePlaceModel>>): Completable {
        val mappedFavouritePlaces = HashMap<String, Any>(favouritePlaces.size)
        for (favouritePlace in favouritePlaces) {
            mappedFavouritePlaces[favouritePlace.key()] = favouritePlace.value().toFirebaseValue()
        }
        return RxFirebaseDatabase.setValue(userFavouritePlacesNode(uid), mappedFavouritePlaces)
    }

    companion object {
        val FAVOURITE_PLACES_NODE = "favourite_places"
    }


}
