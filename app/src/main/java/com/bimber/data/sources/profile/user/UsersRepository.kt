package com.bimber.data.sources.profile.user

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.DefaultValues
import com.bimber.data.entities.profile.local.User
import com.bimber.data.entities.profile.network.Gender
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import com.jakewharton.rx.ReplayingShare
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class UsersRepository @Inject
constructor(private val userNetworkSource: UserNetworkSource,
            private val usersLocalSource: UsersLocalSource,
            private val defaultValues: DefaultValues,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    //FIXME As Single requests more then 1 item from upstream this will request from network nevertheless
    private val inMemoryCache: Cache<String, Flowable<Entity>> = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.SECONDS)
            .maximumSize(100)
            .build<String, Flowable<Entity>>()


    fun getNetworkToCachePipe(uId: String): Flowable<Entity> {
        //TODO Remove after version 1.04 in not used by anyone and we can safely update security rules
        if (uId.isEmpty()) {
            return Flowable.just(Entity.INSTANCE)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .doOnNext { usersLocalSource.insertUser(defaultValues.defaultUser(uId)) }
                    .onErrorReturnItem(Entity.INSTANCE)
        }
        val inMemoryCachedPipe = inMemoryCache.getIfPresent(uId)
        if (inMemoryCachedPipe != null) {
            return inMemoryCachedPipe
        } else {
            val pipe = userNetworkSource.onUserEvent(uId)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .doOnNext {
                        if (it.isEmpty()) {
                            usersLocalSource.insertUser(defaultValues.deletedUser(uId))
                        } else {
                            usersLocalSource.insertUser(User(it[0]))
                        }
                    }
                    .doOnError { usersLocalSource.insertUser(defaultValues.defaultUser(uId)) }
                    .map { _ -> Entity.INSTANCE }
                    .retry()
                    .compose(ReplayingShare.instance())
            inMemoryCache.put(uId, pipe)
            return pipe
        }
    }

    fun onUser(uId: String): Flowable<User> {
        val pipe = getNetworkToCachePipe(uId)
                .listen()

        val cacheSource = usersLocalSource.onUser(uId)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    fun user(uId: String): Single<User> {
        getNetworkToCachePipe(uId)
                .firstOrError()
                .subscribeBy(onError = { Timber.e(it, "Failed to fetch user") })

        return usersLocalSource.onUser(uId)
                .firstOrError()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
    }

    fun updateUserName(userId: String, name: String): Completable {
        return userNetworkSource.updateUserName(userId, name)
    }

    fun updateUserAbout(userId: String, about: String): Completable {
        return userNetworkSource.updateUserAbout(userId, about)
    }

    fun updateUserGender(userId: String, gender: Gender): Completable {
        return userNetworkSource.updateUserGender(userId, gender)
    }

    fun updateUserAge(userId: String, age: Int): Completable {
        return userNetworkSource.updateUserAge(userId, age)
    }
}
