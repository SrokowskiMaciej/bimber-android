package com.bimber.data.sources.discovery.groups

import com.f2prateek.rx.preferences2.RxSharedPreferences

import java.util.ArrayList
import java.util.HashSet

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Observable

/**
 * Created by maciek on 17.04.17.
 */
@Singleton
class GroupDeepLinkRepository @Inject
constructor(private val rxSharedPreferences: RxSharedPreferences) {

    fun putDeepLinkGroupId(groupId: String) {
        val profileDeepLinks = HashSet<String>(1)
        profileDeepLinks.add(groupId)
        rxSharedPreferences.getStringSet(DEEPLINK_GROUP_KEY).set(profileDeepLinks)
    }

    fun removeDeepLinkGroupId() {
        rxSharedPreferences.getStringSet(DEEPLINK_GROUP_KEY).set(HashSet())
    }

    fun onDeepLinkGroupId(): Observable<List<String>> {
        return rxSharedPreferences.getStringSet(DEEPLINK_GROUP_KEY)
                .asObservable()
                .map({ ArrayList(it) })
    }

    companion object {

        private val DEEPLINK_GROUP_KEY = "DEEPLINK_GROUP_KEY"
    }
}
