package com.bimber.data.sources.profile.places;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bimber.data.entities.profile.local.FavouritePlace;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
public interface FavouritePlacesLocalSource {

    @Query("SELECT * FROM users_favourite_places WHERE favouritePlaceUserId = :userId ORDER BY favouritePlaceIndex")
    Flowable<List<FavouritePlace>> onUserFavouritePlaces(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FavouritePlace favouritePlace);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<FavouritePlace> favouritePlaces);

    @Query("DELETE FROM users_favourite_places WHERE favouritePlaceUserId = :userId AND favouritePlaceIndex >= :size")
    void removeFavouritePlacesAboveSize(String userId, int size);

    @Query("DELETE FROM users_favourite_places WHERE favouritePlaceUserId = :userId")
    void removeFavouritePlaces(String userId);
}
