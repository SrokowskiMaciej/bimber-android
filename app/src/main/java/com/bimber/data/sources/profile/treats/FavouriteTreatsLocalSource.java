package com.bimber.data.sources.profile.treats;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bimber.data.entities.profile.local.FavouriteTreat;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
public interface FavouriteTreatsLocalSource {

    @Query("SELECT * FROM users_favourite_treats WHERE favouriteTreatUserId = :userId ORDER BY favouriteTreatIndex")
    Flowable<List<FavouriteTreat>> onUserFavouriteTreats(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FavouriteTreat favouriteTreat);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<FavouriteTreat> favouriteTreats);

    @Query("DELETE FROM users_favourite_treats WHERE favouriteTreatUserId = :userId AND favouriteTreatIndex >= :size")
    void removeFavouriteTreatsAboveSize(String userId, int size);

    @Query("DELETE FROM users_favourite_treats WHERE favouriteTreatUserId = :userId")
    void removeFavouriteTreats(String userId);
}
