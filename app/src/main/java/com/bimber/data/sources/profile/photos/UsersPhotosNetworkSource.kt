package com.bimber.data.sources.profile.photos

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.network.UploadStatus
import com.bimber.data.entities.profile.network.UserPhotoModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Singleton
class UsersPhotosNetworkSource
@Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    private fun photosNode(): DatabaseReference {
        return firebaseDatabase.getReference(PHOTOS_NODE)
    }

    private fun userPhotosNode(uid: String): DatabaseReference {
        return photosNode().child(uid)
    }

    public fun generatePhotoId(uid: String): String {
        return userPhotosNode(uid).push().key
    }

    fun onUserPhotos(uid: String, limit: Int? = null): Flowable<List<Key<UserPhotoModel>>> {
        var query = userPhotosNode(uid)
                .orderByChild(UserPhotoModel.INDEX_FIELD_NAME)
        if (limit != null) {
            query = query.limitToFirst(limit)
        }
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper { UserPhotoModel.create(it) });
    }

    fun insertUserPhoto(uid: String, photoId: String, uri: String, index: Long): Completable {
        val userPhotoModelToInsert = UserPhotoModel.create(index, uri, UploadStatus.FINISHED)
        val push = userPhotosNode(uid).child(photoId)
        return RxFirebaseDatabase.setValue(push, userPhotoModelToInsert.toFirebaseValue())
    }

    fun removePhoto(uid: String, photoId: String): Completable {
        val reference = userPhotosNode(uid).child(photoId)
        return RxFirebaseDatabase.setValue(reference, null)
    }

    fun reorderPhotos(uid: String, photos: Map<String, Int>): Completable {
        if (photos.isEmpty()) {
            return Completable.complete()
        }
        val update = HashMap<String, Any>(photos.size)
        for (photo in photos) {
            update[photo.key + "/" + UserPhotoModel.INDEX_FIELD_NAME] = photo.value
        }
        return RxFirebaseDatabase.updateChildren(userPhotosNode(uid), update)
    }

    companion object {
        val PHOTOS_NODE = "photos"
    }

}
