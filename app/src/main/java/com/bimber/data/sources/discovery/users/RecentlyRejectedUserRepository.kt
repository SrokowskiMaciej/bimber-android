package com.bimber.data.sources.discovery.users

import com.bimber.domain.model.NearbyUserData
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciejsrokowski on 3/24/18.
 */
@Singleton
class RecentlyRejectedUserRepository
@Inject
constructor() {

    private val recentlyRejectedUser = BehaviorProcessor.createDefault<List<ReversibleUser>>(emptyList())

    fun setRecentlyRejectedUser(rejectedUser: NearbyUserData) {
        recentlyRejectedUser.onNext(Collections.singletonList(ReversibleUser.RejectedUser(rejectedUser)))
    }

    fun setRevertedUser(rejectedUser: NearbyUserData) {
        recentlyRejectedUser.onNext(Collections.singletonList(ReversibleUser.RevertedUser(rejectedUser)))
    }

    fun clear() {
        recentlyRejectedUser.onNext(emptyList())
    }

    fun onReversibleUser(): Flowable<List<ReversibleUser>> {
        return recentlyRejectedUser
    }


    sealed class ReversibleUser(open val user: NearbyUserData) {
        data class RejectedUser(override val user: NearbyUserData) : ReversibleUser(user)
        data class RevertedUser(override val user: NearbyUserData) : ReversibleUser(user)
    }

}