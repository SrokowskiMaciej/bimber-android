package com.bimber.data.sources.discovery.users

import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciejsrokowski on 3/14/18.
 */
@Singleton
class NearbyDiscoverableUsersInMemorySource
@Inject
constructor() {

    private val discoverabilityChange = BehaviorProcessor.create<MutableMap<String, Discoverability>>()
    private val depleted = BehaviorProcessor.createDefault(false)

    private val discoverableUsers = Collections.synchronizedMap(mutableMapOf<String, Discoverability>())

    fun addUsers(uIds: List<String>): Boolean {
        var changed = false
        uIds.forEach {
            if (!discoverableUsers.containsKey(it)) {
                discoverableUsers[it] = Discoverability.DISCOVERABLE
                changed = true
            }
        }
        discoverabilityChange.onNext(discoverableUsers)
        return changed
    }

    fun removeUser(uId: String): Int {
        discoverableUsers[uId] = Discoverability.NON_DISCOVERABLE
        discoverabilityChange.onNext(discoverableUsers)
        return discoverableUsers.size
    }

    fun markDepleted() {
        depleted.onNext(true)
    }

    fun clear() {
        discoverableUsers.clear()
        depleted.onNext(false)
        discoverabilityChange.onNext(discoverableUsers)
    }


    fun onDiscoverableUsers(): Flowable<Set<String>> {
        return discoverabilityChange.map { it.filter { it.value == Discoverability.DISCOVERABLE }.keys }
    }

    fun isDepleted(): Flowable<Boolean> {
        return depleted
    }

    enum class Discoverability {
        DISCOVERABLE,
        NON_DISCOVERABLE
    }

    data class DiscoverabilityChange(val uId: String, val discoverability: Discoverability)
}