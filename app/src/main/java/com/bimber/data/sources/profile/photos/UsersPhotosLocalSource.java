package com.bimber.data.sources.profile.photos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.bimber.data.entities.profile.local.UserPhoto;
import com.bimber.data.entities.profile.network.UploadStatus;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
@TypeConverters(UserPhoto.UploadStatusConverter.class)
public interface UsersPhotosLocalSource {
    @Query("SELECT * FROM users_photos WHERE photoUserId = :userId AND userPhotoUploadStatus = :uploadStatus ORDER BY userPhotoIndex LIMIT 1")
    Flowable<UserPhoto> onUserProfilePhoto(String userId, UploadStatus uploadStatus);

    @Query("SELECT * FROM users_photos WHERE photoUserId = :userId AND userPhotoUploadStatus = :uploadStatus ORDER BY userPhotoIndex")
    Flowable<List<UserPhoto>> onUserPhotos(String userId, UploadStatus uploadStatus);

    @Query("SELECT * FROM users_photos WHERE photoUserId = :userId ORDER BY userPhotoIndex")
    Flowable<List<UserPhoto>> onUserPhotos(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserPhoto userPhoto);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<UserPhoto> userPhotos);

    @Query("DELETE FROM users_photos WHERE photoUserId = :userId AND userPhotoIndex >= :size")
    void removePhotosAboveSize(String userId, int size);
}
