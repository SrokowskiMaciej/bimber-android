package com.bimber.data.sources.chat.messages.aggregated;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.bimber.data.entities.chat.messages.local.aggregated.MessageWithSenderData;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
public interface MessagesWithSenderDataLocalSource {

    //FIXME Remove photo index = 0 and add min
    @Transaction
    @Query("SELECT * FROM " +
            "(SELECT * FROM chat_messages " +
            "INNER JOIN users ON userId = users.uId " +
            "INNER JOIN users_photos ON userId = users_photos.photoUserId " +
            "WHERE chatId = :chatId AND userPhotoIndex = 0 " +
            "ORDER BY timestamp DESC LIMIT :limit) " +
            "GROUP BY messageId " +
            "ORDER BY timestamp ASC LIMIT :limit")
    Flowable<List<MessageWithSenderData>> onMessages(String chatId, int limit);

    //FIXME Remove photo index = 0 and add min
    @Transaction
    @Query("SELECT * FROM " +
            "(SELECT * FROM chat_messages " +
            "INNER JOIN users ON userId = users.uId " +
            "INNER JOIN users_photos ON userId = users_photos.photoUserId " +
            "WHERE chatId = :chatId AND userPhotoIndex = 0 AND contentType = :contentType " +
            "ORDER BY timestamp DESC LIMIT :limit) " +
            "GROUP BY messageId " +
            "ORDER BY timestamp ASC LIMIT :limit")
    Flowable<List<MessageWithSenderData>> onMessages(String chatId, String contentType, int limit);
}
