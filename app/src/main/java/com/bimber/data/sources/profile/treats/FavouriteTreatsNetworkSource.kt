package com.bimber.data.sources.profile.treats

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.network.FavouriteTreatModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Singleton
class FavouriteTreatsNetworkSource
@Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {


    fun alcoholsNode(): DatabaseReference {
        return firebaseDatabase.getReference(ALCOHOLS_NODE)
    }

    fun userAlcoholsNode(uid: String): DatabaseReference {
        return alcoholsNode().child(uid)
    }

    fun onUserAlcoholsEvents(uid: String): Flowable<List<Key<FavouriteTreatModel>>> {
        val query = userAlcoholsNode(uid).orderByChild(FavouriteTreatModel.INDEX_FIELD_NAME)
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper({ FavouriteTreatModel.create(it) }))
    }

    fun alcohols(uid: String): Single<List<Key<FavouriteTreatModel>>> {
        val query = userAlcoholsNode(uid).orderByChild(FavouriteTreatModel.INDEX_FIELD_NAME)
        return RxFirebaseDatabase.observeSingleValueEvent(query, RxFirebaseUtils.childListMapper({ FavouriteTreatModel.create(it) }))
    }

    fun insertAlcohol(uid: String, favouriteTreatModel: FavouriteTreatModel): Completable {
        val push = userAlcoholsNode(uid).push()
        return RxFirebaseDatabase.setValue(push, favouriteTreatModel.toFirebaseValue())
    }


    fun removeAlcohol(uid: String, alcoholKey: String): Completable {
        return RxFirebaseDatabase.setValue(userAlcoholsNode(uid).child(alcoholKey), null)
    }

    fun updateFavouriteTreats(uid: String, favouriteTreats: List<Key<FavouriteTreatModel>>): Completable {
        val mappedFavouriteTreats = HashMap<String, Any>(favouriteTreats.size)
        for (favouritePlace in favouriteTreats) {
            mappedFavouriteTreats[favouritePlace.key()] = favouritePlace.value().toFirebaseValue()
        }
        return RxFirebaseDatabase.setValue(userAlcoholsNode(uid), mappedFavouriteTreats)
    }

    companion object {
        val ALCOHOLS_NODE = "alcohols"
    }
}
