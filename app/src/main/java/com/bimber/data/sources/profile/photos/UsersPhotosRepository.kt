package com.bimber.data.sources.profile.photos

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.DefaultValues
import com.bimber.data.entities.profile.local.UserPhoto
import com.bimber.data.entities.profile.network.UploadStatus
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import com.jakewharton.rx.ReplayingShare
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class UsersPhotosRepository @Inject
constructor(private val usersPhotosNetworkSource: UsersPhotosNetworkSource,
            private val usersPhotosLocalSource: UsersPhotosLocalSource,
            private val defaultValues: DefaultValues,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    //FIXME As Single requests more then 1 item from upstream this will request from network nevertheless
    private val profilePhotosInMemoryCache: Cache<String, Flowable<Entity>> = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.SECONDS)
            .maximumSize(100)
            .build<String, Flowable<Entity>>()

    fun getProfilePhotoNetworkToCachePipe(uId: String): Flowable<Entity> {
        val inMemoryCachedPipe = profilePhotosInMemoryCache.getIfPresent(uId)
        if (inMemoryCachedPipe != null) {
            return inMemoryCachedPipe
        } else {
            val pipe = usersPhotosNetworkSource.onUserPhotos(uId, 1)
                    .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                    .map { photos -> photos.filter { it.value().uploadStatus() == UploadStatus.FINISHED } }
                    .doOnNext {
                        if (it.isEmpty()) {
                            //TODO Well, the idea with default photo uri in the database is not quite good
                            usersPhotosLocalSource.insert(defaultValues.defaultUserPhoto(uId))
                        } else {
                            usersPhotosLocalSource.insert(it.mapIndexed { index, photo -> UserPhoto(uId, photo, index) })
                        }
                    }
                    .doOnError {
                        usersPhotosLocalSource.insert(defaultValues.defaultUserPhoto(uId))
                    }
                    .map { _ -> Entity.INSTANCE }
                    .retry()
                    .compose(ReplayingShare.instance())
            profilePhotosInMemoryCache.put(uId, pipe)
            return pipe
        }
    }

    fun getAllPhotosNetworkToCachePipe(uId: String): Flowable<Entity> {
        return usersPhotosNetworkSource.onUserPhotos(uId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                .map { photos -> photos.filter { it.value().uploadStatus() == UploadStatus.FINISHED } }
                .doOnNext {
                    if (it.isEmpty()) {
                        //TODO Well, the idea with default photo uri in the database is not quite good
                        usersPhotosLocalSource.removePhotosAboveSize(uId, 1)
                        usersPhotosLocalSource.insert(defaultValues.defaultUserPhoto(uId))
                    } else {
                        usersPhotosLocalSource.removePhotosAboveSize(uId, it.size)
                        usersPhotosLocalSource.insert(it.mapIndexed { index, photo -> UserPhoto(uId, photo, index) })
                    }
                }
                .doOnError {
                    usersPhotosLocalSource.removePhotosAboveSize(uId, 1)
                    usersPhotosLocalSource.insert(defaultValues.defaultUserPhoto(uId))
                }
                .map { _ -> Entity.INSTANCE }
    }

    fun onUserProfilePhoto(uId: String): Flowable<UserPhoto> {
        val pipe = getProfilePhotoNetworkToCachePipe(uId)
                .listen()

        val cacheSource = usersPhotosLocalSource.onUserProfilePhoto(uId, UploadStatus.FINISHED)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    fun userProfilePhoto(uId: String): Single<UserPhoto> {
        getProfilePhotoNetworkToCachePipe(uId)
                .firstOrError()
                .subscribeBy(onError = { Timber.e(it, "Failed to fetch user photo") })

        return usersPhotosLocalSource.onUserProfilePhoto(uId, UploadStatus.FINISHED)
                .firstOrError()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
    }


    fun onUploadedUserPhotos(uId: String): Flowable<List<UserPhoto>> {
        val pipe = getProfilePhotoNetworkToCachePipe(uId)
                .listen()

        val cacheSource = usersPhotosLocalSource.onUserPhotos(uId, UploadStatus.FINISHED)
                .distinctUntilChanged()
                .filter { it.isNotEmpty() }
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    //FIXME The only difference between this and continuous listening method is not disposing and
    //FIXME listening to network only once
    fun uploadedUserPhotos(uId: String): Single<List<UserPhoto>> {
        getProfilePhotoNetworkToCachePipe(uId)
                .firstOrError()
                .subscribeBy(onError = { Timber.e(it, "Failed to fetch user photo") })

        return usersPhotosLocalSource.onUserPhotos(uId, UploadStatus.FINISHED)
                .filter { it.isNotEmpty() }
                .firstOrError()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
    }

    fun onAllUserPhotos(uId: String): Flowable<List<UserPhoto>> {
        val pipe = usersPhotosNetworkSource.onUserPhotos(uId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                .doOnNext {
                    if (it.isEmpty()) {
                        //TODO Well, the idea with default photo uri in the database is not quite good
                        usersPhotosLocalSource.removePhotosAboveSize(uId, 1)
                        usersPhotosLocalSource.insert(defaultValues.defaultUserPhoto(uId))
                    } else {
                        usersPhotosLocalSource.removePhotosAboveSize(uId, it.size)
                        usersPhotosLocalSource.insert(it.mapIndexed { index, photo -> UserPhoto(uId, photo, index) })
                    }
                }
                .listen()

        val cacheSource = usersPhotosLocalSource.onUserPhotos(uId)
                .distinctUntilChanged()
                .filter { it.isNotEmpty() }
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    fun insertUserPhoto(uid: String, photoId: String, uri: String, index: Long): Completable {
        return usersPhotosNetworkSource.insertUserPhoto(uid, photoId, uri, index)
    }

    fun removePhoto(uid: String, photoId: String): Completable {
        return usersPhotosNetworkSource.removePhoto(uid, photoId)
    }

    fun reorderPhotos(uid: String, photos: Map<String, Int>): Completable {
        return usersPhotosNetworkSource.reorderPhotos(uid, photos)
    }
}
