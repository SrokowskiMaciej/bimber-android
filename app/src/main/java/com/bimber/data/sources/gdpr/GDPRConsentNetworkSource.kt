package com.bimber.data.sources.gdpr

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.utils.firebase.relation.RxFirebaseDb
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GDPRConsentNetworkSource
@Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    fun userGdprConsentStatus(uId: String): Observable<GDPRConsentStatus> {
        return RxFirebaseDatabase.observeValueEvent(firebaseDatabase.reference.child(GDPR_CONSENT_NODE).child(uId),
                backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper { it.getValue(String::class.java) })
                .map {
                    val consent = it.firstOrNull()
                    if (consent != null) {
                        GDPRConsentStatus.valueOf(consent)
                    } else {
                        GDPRConsentStatus.NONE
                    }
                }
                .toObservable()
                .distinctUntilChanged()
    }

    fun setGDPRConsentStatus(uId: String, status: GDPRConsentStatus) {
        firebaseDatabase.reference.child(GDPR_CONSENT_NODE).child(uId).setValue(status.toString())
    }

    enum class GDPRConsentStatus {
        NONE,
        REFUSED,
        AGREED_V1
    }

    companion object {
        const val GDPR_CONSENT_NODE = "gdpr_consents"
    }
}