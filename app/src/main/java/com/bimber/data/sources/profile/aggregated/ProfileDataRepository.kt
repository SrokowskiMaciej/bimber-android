package com.bimber.data.sources.profile.aggregated

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail
import com.bimber.data.sources.profile.photos.UsersPhotosRepository
import com.bimber.data.sources.profile.places.FavouritePlacesRepository
import com.bimber.data.sources.profile.treats.FavouriteTreatsRepository
import com.bimber.data.sources.profile.user.UsersRepository
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function4
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 23.02.17.
 */
@Singleton
class ProfileDataRepository @Inject
constructor(private val backgroundSchedulerProvider: BackgroundSchedulerProvider,
            private val profileDataLocalSource: ProfileDataLocalSource,
            private val usersRepository: UsersRepository,
            private val usersPhotosRepository: UsersPhotosRepository,
            private val favouriteTreatsRepository: FavouriteTreatsRepository,
            private val favouritePlacesRepository: FavouritePlacesRepository) {

    fun getThumbnailNetworkToCachePipe(userId: String): Flowable<Entity> {
        return Flowable.combineLatest<Entity, Entity, Entity>(
                usersRepository.getNetworkToCachePipe(userId),
                usersPhotosRepository.getProfilePhotoNetworkToCachePipe(userId),
                BiFunction { _, _ -> Entity.INSTANCE })
    }

    fun getFullDataNetworkToCachePipe(userId: String): Flowable<Entity> {
        return Flowable.combineLatest<Entity, Entity, Entity, Entity, Entity>(
                usersRepository.getNetworkToCachePipe(userId),
                usersPhotosRepository.getAllPhotosNetworkToCachePipe(userId),
                favouriteTreatsRepository.getNetworkToCachePipe(userId),
                favouritePlacesRepository.getNetworkToCachePipe(userId),
                Function4 { _, _, _, _ -> Entity.INSTANCE })
    }

    fun onUserProfileDataFull(userId: String): Flowable<ProfileDataFull> {
        val pipe = getFullDataNetworkToCachePipe(userId)
                .listen()
        val cacheSource = profileDataLocalSource.onUserProfileDataFull(userId)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())

        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    fun userProfileDataThumbnail(userId: String): Single<ProfileDataThumbnail> {
        getThumbnailNetworkToCachePipe(userId)
                .firstOrError()
                .subscribeBy(onError = { Timber.e(it, "Failed to fetch user profile data") })

        return profileDataLocalSource.onUserProfileDataThumbnail(userId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .firstOrError()
    }

    fun onUserProfileDataThumbnail(userId: String): Flowable<ProfileDataThumbnail> {
        val pipe = getThumbnailNetworkToCachePipe(userId)
                .listen()
        val cacheSource = profileDataLocalSource.onUserProfileDataThumbnail(userId)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }
}
