package com.bimber.data.sources.profile.user

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.network.Gender
import com.bimber.data.entities.profile.network.UserModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by srokowski.maciej@gmail.com on 30.11.16.
 */
@Singleton
class UserNetworkSource @Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    private fun usersNode(): DatabaseReference {
        return firebaseDatabase.getReference(USERS_NODE)
    }

    private fun userNode(uid: String): DatabaseReference {
        return usersNode().child(uid)
    }


    fun onUserEvent(uid: String): Flowable<List<Key<UserModel>>> {
        return RxFirebaseDatabase.observeValueEvent(userNode(uid), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper { UserModel.create(it) })
    }

    fun updateUserName(userId: String, name: String): Completable {
        return RxFirebaseDatabase.setValue(userNode(userId).child(UserModel.USERNAME_FIELD), name)
    }

    fun updateUserAbout(userId: String, about: String): Completable {
        return RxFirebaseDatabase.setValue(userNode(userId).child(UserModel.ABOUT_FIELD), about)
    }

    fun updateUserGender(userId: String, gender: Gender): Completable {
        return RxFirebaseDatabase.setValue(userNode(userId).child(UserModel.GENDER_FIELD), gender)
    }

    fun updateUserAge(userId: String, age: Int): Completable {
        return RxFirebaseDatabase.setValue(userNode(userId).child(UserModel.AGE_FIELD), age)
    }

    companion object {
        private val USERS_NODE = "users"
    }
}
