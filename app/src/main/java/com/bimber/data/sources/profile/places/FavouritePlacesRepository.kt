package com.bimber.data.sources.profile.places

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.local.FavouritePlace
import com.bimber.data.entities.profile.network.FavouritePlaceModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class FavouritePlacesRepository @Inject
constructor(private val favouritePlacesNetworkSource: FavouritePlacesNetworkSource,
            private val favouritePlacesLocalSource: FavouritePlacesLocalSource,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    fun getNetworkToCachePipe(uId: String): Flowable<Entity> {
        return favouritePlacesNetworkSource.onUserFavouritePlacesEvents(uId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                .map {
                    if (it.isEmpty()) {
                        favouritePlacesLocalSource.removeFavouritePlaces(uId)
                    } else {
                        favouritePlacesLocalSource.removeFavouritePlacesAboveSize(uId, it.size)
                        favouritePlacesLocalSource.insert(it.map { FavouritePlace(uId, it) })
                    }
                }
                .map { _ -> Entity.INSTANCE }
    }

    fun onFavouritePlaces(uId: String): Flowable<List<FavouritePlace>> {
        val pipe = getNetworkToCachePipe(uId)
                .listen()

        val cacheSource = favouritePlacesLocalSource.onUserFavouritePlaces(uId)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    //FIXME The only difference between this and continuous listening method is not disposing and
    //FIXME listening to network only once
    fun favouritePlaces(uId: String): Single<List<FavouritePlace>> {
        getNetworkToCachePipe(uId)
                .firstOrError()
                .subscribeBy(onError = {
                    Timber.e(it, "Failed to fetch favourite places")
                })

        return favouritePlacesLocalSource.onUserFavouritePlaces(uId)
                .firstOrError()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
    }

    fun insertFavouritePlace(uid: String, place: FavouritePlaceModel): Completable {
        return favouritePlacesNetworkSource.insertFavouritePlace(uid, place)
    }

    fun updateFavouritePlaces(uId: String, favouritePlaces: List<FavouritePlace>): Completable {
        val favouritePlacesModels = favouritePlaces.map {
            Key.of(it.favouritePlaceId, FavouritePlaceModel.create(it.favouritePlaceIndex.toLong(),
                    it.name, it.address, it.googleMapsId, it.longitude, it.latitude))
        }
        Single.just(favouritePlaces)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .map {
                    favouritePlacesLocalSource.removeFavouritePlacesAboveSize(uId, it.size)
                    favouritePlacesLocalSource.insert(it)
                }
                .subscribeBy(onError = { Timber.e(it, "Failed to rearrange favourite places") })
        return favouritePlacesNetworkSource.updateFavouritePlaces(uId, favouritePlacesModels)
    }

}
