package com.bimber.data.sources.profile.location;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserLocation;

import io.reactivex.Flowable;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
public interface UsersLocationsLocalSource {
    @Query("SELECT * FROM users WHERE uId = :userId LIMIT 1")
    Flowable<User> onUserLocation(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserLocation userLocation);
}
