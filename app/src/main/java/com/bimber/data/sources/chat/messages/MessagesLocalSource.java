package com.bimber.data.sources.chat.messages;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bimber.data.entities.chat.messages.local.Message;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by maciek on 23.11.17.
 */

@Dao
public interface MessagesLocalSource {

    @Query("SELECT * FROM " +
            "( SELECT * FROM chat_messages WHERE chatId = :chatId ORDER BY timestamp DESC LIMIT :limit)" +
            " ORDER BY timestamp ASC LIMIT :limit")
    Flowable<List<Message>> onMessages(String chatId, int limit);

    @Query("SELECT * FROM " +
            "( SELECT * FROM chat_messages WHERE" +
            " chatId = :chatId AND contentType = :contentType" +
            " ORDER BY timestamp DESC LIMIT :limit)" +
            " ORDER BY timestamp ASC LIMIT :limit")
    Flowable<List<Message>> onMessages(String chatId, String contentType, int limit);

    @Query("SELECT * FROM chat_messages WHERE chatId = :chatId AND messageId = :messageId LIMIT 1")
    Maybe<Message> getMessage(String chatId, String messageId);

    @Query("SELECT * FROM chat_messages WHERE chatId = :chatId AND messageId = :messageId LIMIT 1")
    Flowable<Message> onMessage(String chatId, String messageId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMessage(Message message);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMessages(List<Message> messages);
}
