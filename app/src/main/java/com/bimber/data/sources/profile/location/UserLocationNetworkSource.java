package com.bimber.data.sources.profile.location;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by srokowski.maciej@gmail.com on 31.10.16.
 */
@Singleton
public class UserLocationNetworkSource {


    public static final String LOCATIONS_NODE = "user_locations";

    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public UserLocationNetworkSource(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference locationsNode() {
        return firebaseDatabase.getReference(LOCATIONS_NODE);
    }

    public Flowable<List<UserLocationModel>> onLocationEvents(String uid) {
        return RxFirebaseDatabase.observeValueEvent(locationsNode().child(uid), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(UserLocationModel::create));
    }

    public Maybe<UserLocationModel> location(String uid) {
        DatabaseReference query = locationsNode().child(uid);
        return RxFirebaseDatabase.observeSingleValueEvent(query, UserLocationModel::create, RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);

    }

    public Single<Key<UserLocationModel>> setLocation(String uId, UserLocationModel userLocationModel) {
        DatabaseReference reference = locationsNode().child(uId);
        return RxFirebaseDatabase.setValue(reference, userLocationModel.toFirebaseValue()).toSingleDefault(Key.of(reference.getKey(), userLocationModel));
    }

    public Completable removeLocation(String uId) {
        DatabaseReference databaseReference = locationsNode().child(uId);
        return RxFirebaseDatabase.setValue(databaseReference, null);
    }

    public Single<Boolean> userHasAnyLocations(String uid) {
        DatabaseReference databaseReference = locationsNode().child(uid);
        return RxFirebaseDatabase.observeSingleValueEvent(databaseReference)
                .map(DataSnapshot::hasChildren);
    }

}
