package com.bimber.data.sources.chat.messages

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.chat.messages.common.ContentType
import com.bimber.data.entities.chat.messages.common.MessageContent
import com.bimber.data.entities.chat.messages.common.image.ImageContent
import com.bimber.data.entities.chat.messages.common.removed.RemovedContent
import com.bimber.data.entities.chat.messages.network.MessageFirebaseValue
import com.bimber.data.entities.chat.messages.network.MessageModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.firebase.relation.Relation
import com.bimber.utils.rx.listen
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Maciek on 09/01/2018.
 */
@Singleton
class MessagesNetworkSource @Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {


    private fun messagesNode(): DatabaseReference {
        return firebaseDatabase.getReference(MESSAGES_NODE)
    }

    private fun chatMessagesNode(chatId: String): DatabaseReference {
        return messagesNode().child(chatId)
    }

    fun <T> onChatMessages(chatId: String,
                           limitLast: Int,
                           contentType: ContentType? = null,
                           onMessage: (Key<MessageModel>) -> Flowable<T> = { Flowable.empty<T>() }): Flowable<List<Key<MessageModel>>> {
        val query = chatMessagesNode(chatId)
                .let {
                    if (contentType != null) {
                        it.orderByChild(MessageFirebaseValue.MESSAGE_CONTENT_FIELD + "/" + MessageContent.CONTENT_TYPE_FIELD)
                    } else {
                        it.orderByChild(MessageFirebaseValue.MESSAGE_TIMESTAMP_FIELD)
                    }
                }
                .limitToLast(limitLast)

        return Relation.Builder(query)
                .relation { dataSnapshot ->
                    Flowable.defer {
                        val messageModel = MessageModel.create(dataSnapshot)
                        onMessage(messageModel)
                                .listen()
                                .map { _ -> messageModel }
                    }
                }
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
    }

    fun onChatMessage(chatId: String,
                      messageId: String): Flowable<Key<MessageModel>> {
        val query = chatMessagesNode(chatId)
                .child(messageId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(), { snapshot -> MessageModel.create(snapshot) })
    }

    fun sendMessage(chatId: String, message: MessageModel): Single<String> {
        val push = chatMessagesNode(chatId).push()
        push.setValue(message.toFirebaseValue())
        return Single.just(push.key)
    }

    fun updateImageMessageUploadStatus(chatId: String,
                                       messageId: String,
                                       uploadStatus: ImageContent.UploadStatus,
                                       uri: String?): Completable {
        val reference = chatMessagesNode(chatId)
                .child(messageId)
                .child(MessageFirebaseValue.MESSAGE_CONTENT_FIELD)
        val update = HashMap<String, Any?>(2)
        update.put(ImageContent.UPLOAD_STATUS_FIELD_NAME, uploadStatus.toString())
        update.put(ImageContent.URI_FIELD_NAME, uri)
        return RxFirebaseDatabase.updateChildren(reference, update)
    }

    fun removeMessage(chatId: String,
                      messageId: String): Completable {
        val reference = chatMessagesNode(chatId)
                .child(messageId)
                .child(MessageFirebaseValue.MESSAGE_CONTENT_FIELD)
        return RxFirebaseDatabase.setValue(reference, RemovedContent.create().toFirebaseValue())
    }

    companion object {
        private val MESSAGES_NODE = "messages"
    }
}
