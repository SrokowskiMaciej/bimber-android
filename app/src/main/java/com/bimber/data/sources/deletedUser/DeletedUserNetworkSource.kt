package com.bimber.data.sources.deletedUser

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.sources.gdpr.GDPRConsentNetworkSource
import com.bimber.utils.rx.RxFirebaseUtils
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeletedUserNetworkSource
@Inject
constructor(private val firebaseDatabase: FirebaseDatabase,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    fun isUserDeleted(uId: String): Observable<Boolean> {
        return RxFirebaseDatabase.observeValueEvent(firebaseDatabase.reference.child(DELETED_USERS_NODE).child(uId),
                backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper { it.getValue(Boolean::class.java) })
                .map {
                    val userDeleted = it.firstOrNull()
                    userDeleted != null && userDeleted
                }
                .toObservable()
    }

    companion object {
        const val DELETED_USERS_NODE = "deleted_users"
    }
}