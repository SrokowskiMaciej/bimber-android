package com.bimber.data.sources.discovery.users

import com.f2prateek.rx.preferences2.RxSharedPreferences

import java.util.ArrayList
import java.util.HashSet

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Observable

/**
 * Created by maciek on 17.04.17.
 */
@Singleton
class UserDeepLinkRepository @Inject
constructor(private val rxSharedPreferences: RxSharedPreferences) {

    fun setDeepLinkUser(profileId: String) {
        val profileDeepLinks = HashSet<String>(1)
        profileDeepLinks.add(profileId)
        rxSharedPreferences.getStringSet(DEEPLINK_PROFILE_KEY).set(profileDeepLinks)
    }

    fun clearDeepLinkUser() {
        rxSharedPreferences.getStringSet(DEEPLINK_PROFILE_KEY).set(HashSet())
    }

    fun onDeepLinkUser(): Observable<List<String>> {
        return rxSharedPreferences.getStringSet(DEEPLINK_PROFILE_KEY)
                .asObservable()
                .map({ ArrayList(it) })
    }

    companion object {
        private const val DEEPLINK_PROFILE_KEY = "DEEPLINK_PROFILE_KEY"
    }
}
