package com.bimber.data.sources.profile.treats

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.profile.local.FavouriteTreat
import com.bimber.data.entities.profile.network.FavouriteTreatModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.rx.Entity
import com.bimber.utils.rx.listen
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 03.03.17.
 */
@Singleton
class FavouriteTreatsRepository @Inject
constructor(private val favouriteTreatsNetworkSource: FavouriteTreatsNetworkSource,
            private val favouriteTreatsLocalSource: FavouriteTreatsLocalSource,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {


    fun getNetworkToCachePipe(uId: String): Flowable<Entity> {
        return favouriteTreatsNetworkSource.onUserAlcoholsEvents(uId)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                .map {
                    if (it.isEmpty()) {
                        favouriteTreatsLocalSource.removeFavouriteTreats(uId)
                    } else {
                        favouriteTreatsLocalSource.removeFavouriteTreatsAboveSize(uId, it.size)
                        favouriteTreatsLocalSource.insert(it.map { FavouriteTreat(uId, it) })
                    }
                }
                .map { _ -> Entity.INSTANCE }
    }

    fun onFavouriteTreats(uId: String): Flowable<List<FavouriteTreat>> {
        val pipe = getNetworkToCachePipe(uId)
                .listen()
        val cacheSource = favouriteTreatsLocalSource.onUserFavouriteTreats(uId)
                .distinctUntilChanged()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        return Flowable.combineLatest(pipe, cacheSource, BiFunction { _, values -> values })
    }

    //FIXME The only difference between this and continuous listening method is not disposing and
    //FIXME listening to network only once
    fun favouriteTreats(uId: String): Single<List<FavouriteTreat>> {
        getNetworkToCachePipe(uId)
                .firstOrError()
                .subscribeBy(onError = {
                    Timber.e(it, "Failed to fetch favourite treats")
                })

        return favouriteTreatsLocalSource.onUserFavouriteTreats(uId)
                .firstOrError()
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
    }

    fun insertFavouriteTreat(uid: String, favouriteTreat: FavouriteTreatModel): Completable {
        return favouriteTreatsNetworkSource.insertAlcohol(uid, favouriteTreat)
    }

    fun updateFavouriteTreats(uId: String, favouriteTreats: List<FavouriteTreat>): Completable {
        val favouritePlacesModels = favouriteTreats.map {
            Key.of(it.favouriteTreatId, FavouriteTreatModel.create(it.favouriteTreatIndex.toLong(), it.favouriteTreatName))
        }
        Single.just(favouriteTreats)
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
                .map {
                    favouriteTreatsLocalSource.removeFavouriteTreatsAboveSize(uId, it.size)
                    favouriteTreatsLocalSource.insert(it)
                }
                .subscribeBy(onError = { Timber.e(it, "Failed to rearrange favourite treats") })
        return favouriteTreatsNetworkSource.updateFavouriteTreats(uId, favouritePlacesModels)
    }

}
