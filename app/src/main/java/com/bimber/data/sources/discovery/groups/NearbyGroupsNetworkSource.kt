package com.bimber.data.sources.discovery.groups

import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable


/**
 * Created by srokowski.maciej@gmail.com on 30.12.16.
 */

class NearbyGroupsNetworkSource(firebaseDatabase: FirebaseDatabase) {
    private val geoFire: GeoFire

    init {
        geoFire = GeoFire(firebaseDatabase.getReference(GEOMATCHING_LOCATIONS_NODE).child(GEOMATCHING_GROUP_LOCATION_SUBDIRECTORY))
    }

    fun onGeolocationInRange(latitude: Double, longitude: Double, range: Int): Flowable<List<String>> {
        val geolocationSource = Flowable.create<GeolocationEvent>({ flowableEmitter ->

            val listener = object : GeoQueryEventListener {
                override fun onKeyEntered(key: String, location: GeoLocation) {

                    if (!flowableEmitter.isCancelled) {
                        flowableEmitter.onNext(GeolocationEvent.Entered(key, location))
                    }
                }

                override fun onKeyExited(key: String) {
                    if (!flowableEmitter.isCancelled) {
                        flowableEmitter.onNext(GeolocationEvent.Exited(key))
                    }
                }

                override fun onKeyMoved(key: String, location: GeoLocation) {
                    if (!flowableEmitter.isCancelled) {
                        flowableEmitter.onNext(GeolocationEvent.Moved(key, location))
                    }
                }

                override fun onGeoQueryReady() {
                    if (!flowableEmitter.isCancelled) {
                        flowableEmitter.onNext(GeolocationEvent.QueryReady())
                    }
                }

                override fun onGeoQueryError(error: DatabaseError) {
                    if (!flowableEmitter.isCancelled) {
                        flowableEmitter.tryOnError(error.toException())
                    }
                }
            }
            val geoQuery = geoFire.queryAtLocation(GeoLocation(latitude, longitude), range.toDouble())
            flowableEmitter.setCancellable { geoQuery.removeGeoQueryEventListener(listener) }
            geoQuery.addGeoQueryEventListener(listener)
        }, BackpressureStrategy.BUFFER)
        return geolocationSource
                .scan(GeoQuery(mutableSetOf(), false), { accumulator, event ->
                    when (event) {
                        is GeolocationEvent.Entered -> GeoQuery(accumulator.geolocations.plus(event.key), accumulator.ready)
                        is GeolocationEvent.Exited -> GeoQuery(accumulator.geolocations.minus(event.key), accumulator.ready)
                        is GeolocationEvent.QueryReady -> GeoQuery(accumulator.geolocations, true)
                        is GeolocationEvent.Moved -> accumulator
                    }
                })
                .skipWhile { !it.ready }
                .map { it.geolocations.toList() }
    }

    companion object {
        const val GEOMATCHING_LOCATIONS_NODE = "geomatching"
        const val GEOMATCHING_GROUP_LOCATION_SUBDIRECTORY = "group_locations"
    }
}

private sealed class GeolocationEvent {
    data class Entered(val key: String, val geolocation: GeoLocation) : GeolocationEvent()
    data class Exited(val key: String) : GeolocationEvent()
    data class Moved(val key: String, val geolocation: GeoLocation) : GeolocationEvent()
    class QueryReady : GeolocationEvent()
}

private class GeoQuery(val geolocations: Set<String>, val ready: Boolean)