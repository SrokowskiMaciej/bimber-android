package com.bimber.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by maciek on 05.10.17.
 */
@Singleton
public class BackgroundSchedulerProvider {

    @Inject
    public BackgroundSchedulerProvider() {

    }

    public Scheduler backgroundScheduler() {
        return Schedulers.io();
    }
}
