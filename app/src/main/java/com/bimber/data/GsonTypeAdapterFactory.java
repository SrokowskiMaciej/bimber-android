package com.bimber.data;

import com.google.gson.TypeAdapterFactory;

/**
 * Created by maciek on 15.04.17.
 */
@com.ryanharter.auto.value.gson.GsonTypeAdapterFactory
public abstract class GsonTypeAdapterFactory implements TypeAdapterFactory {

    public static GsonTypeAdapterFactory create() {
        return new AutoValueGson_GsonTypeAdapterFactory();
    }

}
