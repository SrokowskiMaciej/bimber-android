package com.bimber.data.deeplinking.model;

import android.net.Uri;

import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 15.04.17.
 */
@AutoValue
public abstract class Deeplink {

    public static final String HTTPS_SCHEME = "https";

    public static Deeplink create(String deeplinkDomain, String... pathSegments) {
        Uri.Builder builder = new Uri.Builder()
                .scheme(HTTPS_SCHEME)
                .authority(deeplinkDomain);
        for (String pathSegment : pathSegments) {
            builder.appendPath(pathSegment);
        }
        return new AutoValue_Deeplink(builder
                .build()
                .toString());
    }

    public abstract String link();

}
