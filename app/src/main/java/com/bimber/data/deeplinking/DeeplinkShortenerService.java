package com.bimber.data.deeplinking;

import com.bimber.data.deeplinking.model.DeeplinkRequest;
import com.bimber.data.deeplinking.model.ShortDeeplinkResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by maciek on 14.04.17.
 */
public interface DeeplinkShortenerService {

    @POST("v1/shortLinks")
    @Headers("Content-Type: application/json")
    Single<ShortDeeplinkResponse> shortenDeeplink(@Body DeeplinkRequest deeplinkRequest, @Query("key") String apiKey);
}
