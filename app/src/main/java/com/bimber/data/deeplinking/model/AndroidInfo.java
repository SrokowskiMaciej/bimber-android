package com.bimber.data.deeplinking.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class AndroidInfo {


    public static AndroidInfo create(String packageName) {
        return new AutoValue_AndroidInfo(packageName);
    }

    @SerializedName("androidPackageName")
    public abstract String androidPackageName();

    public static TypeAdapter<AndroidInfo> typeAdapter(Gson gson) {
        return new AutoValue_AndroidInfo.GsonTypeAdapter(gson);
    }
}
