package com.bimber.data.deeplinking.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class DeeplinkRequest {


    public static DeeplinkRequest create(AndroidInfo androidInfo, String dynamicLinkDomain, Deeplink deeplink, SocialMetaTagInfo
            socialMetaTagInfo) {
        return new AutoValue_DeeplinkRequest(DynamicLinkInfo.create(androidInfo, dynamicLinkDomain, deeplink, socialMetaTagInfo), Suffix
                .create());
    }

    @SerializedName("dynamicLinkInfo")
    public abstract DynamicLinkInfo dynamicLinkInfo();

    @SerializedName("suffix")
    public abstract Suffix suffix();

    public static TypeAdapter<DeeplinkRequest> typeAdapter(Gson gson) {
        return new AutoValue_DeeplinkRequest.GsonTypeAdapter(gson);
    }
}