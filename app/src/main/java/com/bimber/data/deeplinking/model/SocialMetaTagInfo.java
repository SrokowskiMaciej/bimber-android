package com.bimber.data.deeplinking.model;

import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class SocialMetaTagInfo {

    public static SocialMetaTagInfo create(String socialTitle, String socialImageLink, String socialDescription) {
        return new AutoValue_SocialMetaTagInfo(socialTitle, socialImageLink, socialDescription);
    }

    @SerializedName("socialTitle")
    public abstract String socialTitle();

    @Nullable
    @SerializedName("socialImageLink")
    public abstract String socialImageLink();

    @SerializedName("socialDescription")
    public abstract String socialDescription();

    public static TypeAdapter<SocialMetaTagInfo> typeAdapter(Gson gson) {
        return new AutoValue_SocialMetaTagInfo.GsonTypeAdapter(gson);
    }
}