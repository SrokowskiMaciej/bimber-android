package com.bimber.data.deeplinking;

import com.bimber.data.deeplinking.model.DeeplinkRequest;

/**
 * Created by maciek on 05.06.17.
 */

public interface DeeplinkRequestFactory<T> {

    DeeplinkRequest buildDeeplinkRequest(T info);
}
