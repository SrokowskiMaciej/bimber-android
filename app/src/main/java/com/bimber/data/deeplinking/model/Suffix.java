package com.bimber.data.deeplinking.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class Suffix {

    public static Suffix create() {
        return new AutoValue_Suffix("SHORT");
    }

    @SerializedName("option")
    public abstract String option();

    public static TypeAdapter<Suffix> typeAdapter(Gson gson) {
        return new AutoValue_Suffix.GsonTypeAdapter(gson);
    }
}