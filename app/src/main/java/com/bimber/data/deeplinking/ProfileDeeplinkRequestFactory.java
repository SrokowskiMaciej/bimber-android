package com.bimber.data.deeplinking;

import com.bimber.data.ServerValues;
import com.bimber.data.deeplinking.model.AndroidInfo;
import com.bimber.data.deeplinking.model.Deeplink;
import com.bimber.data.deeplinking.model.DeeplinkRequest;
import com.bimber.data.deeplinking.model.SocialMetaTagInfo;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by maciek on 05.06.17.
 */
@Singleton
public class ProfileDeeplinkRequestFactory implements DeeplinkRequestFactory<ProfileDataThumbnail> {


    public static final String PROFILE_DEEPLINK_PATH = "profile";

    private final AndroidInfo androidInfo;
    private final ServerValues serverValues;

    @Inject
    public ProfileDeeplinkRequestFactory(AndroidInfo androidInfo, ServerValues serverValues) {
        this.androidInfo = androidInfo;
        this.serverValues = serverValues;
    }

    @Override
    public DeeplinkRequest buildDeeplinkRequest(ProfileDataThumbnail info) {
        Deeplink deeplink = Deeplink.create(serverValues.getDeeplinkDomain(), PROFILE_DEEPLINK_PATH, info.user.uId);
        SocialMetaTagInfo socialMetaTagInfo = SocialMetaTagInfo.create(info.user.displayName, info.photo.userPhotoUri, info.user.about);
        return DeeplinkRequest.create(androidInfo, serverValues.getFirebaseDeeplinkDomain(), deeplink, socialMetaTagInfo);
    }
}
