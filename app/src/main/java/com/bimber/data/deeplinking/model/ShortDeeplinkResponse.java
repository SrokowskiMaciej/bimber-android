package com.bimber.data.deeplinking.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class ShortDeeplinkResponse {

    @SerializedName("previewLink")
    public abstract String previewLink();

    @SerializedName("shortLink")
    public abstract String shortLink();

    public static TypeAdapter<ShortDeeplinkResponse> typeAdapter(Gson gson) {
        return new AutoValue_ShortDeeplinkResponse.GsonTypeAdapter(gson);
    }
}