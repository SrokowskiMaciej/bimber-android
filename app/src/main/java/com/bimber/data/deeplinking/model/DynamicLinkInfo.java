package com.bimber.data.deeplinking.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class DynamicLinkInfo {

    public static DynamicLinkInfo create(AndroidInfo androidInfo, String dynamicLinkDomain, Deeplink deeplink, SocialMetaTagInfo
            socialMetaTagInfo) {
        return new AutoValue_DynamicLinkInfo(androidInfo, dynamicLinkDomain, deeplink.link(), socialMetaTagInfo);
    }

    @SerializedName("androidInfo")
    public abstract AndroidInfo androidInfo();

    @SerializedName("dynamicLinkDomain")
    public abstract String dynamicLinkDomain();

    @SerializedName("link")
    public abstract String link();

    @SerializedName("socialMetaTagInfo")
    public abstract SocialMetaTagInfo socialMetaTagInfo();

    public static TypeAdapter<DynamicLinkInfo> typeAdapter(Gson gson) {
        return new AutoValue_DynamicLinkInfo.GsonTypeAdapter(gson);
    }
}