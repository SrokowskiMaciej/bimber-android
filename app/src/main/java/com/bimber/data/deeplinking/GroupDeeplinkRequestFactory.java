package com.bimber.data.deeplinking;

import com.bimber.data.ServerValues;
import com.bimber.data.deeplinking.model.AndroidInfo;
import com.bimber.data.deeplinking.model.Deeplink;
import com.bimber.data.deeplinking.model.DeeplinkRequest;
import com.bimber.data.deeplinking.model.SocialMetaTagInfo;
import com.bimber.data.entities.Group;
import com.bimber.utils.firebase.Key;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by maciek on 05.06.17.
 */
@Singleton
public class GroupDeeplinkRequestFactory implements DeeplinkRequestFactory<Key<Group>> {
    public static final String GROUP_DEEPLINK_PATH = "group";

    private final AndroidInfo androidInfo;
    private final ServerValues serverValues;

    @Inject
    public GroupDeeplinkRequestFactory(AndroidInfo androidInfo, ServerValues serverValues) {
        this.androidInfo = androidInfo;
        this.serverValues = serverValues;
    }

    @Override
    public DeeplinkRequest buildDeeplinkRequest(Key<Group> info) {
        Deeplink deeplink = Deeplink.create(serverValues.getDeeplinkDomain(), GROUP_DEEPLINK_PATH, info.key());
        SocialMetaTagInfo socialMetaTagInfo = SocialMetaTagInfo.create(info.value().name(), info.value().image(), info.value()
                .description());
        return DeeplinkRequest.create(androidInfo, serverValues.getFirebaseDeeplinkDomain(), deeplink, socialMetaTagInfo);
    }
}
