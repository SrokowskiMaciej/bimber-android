package com.bimber.data.analytics

import android.os.Bundle
import com.bimber.data.entities.Chattable
import com.bimber.data.entities.Chattable.ChatType.DIALOG
import com.bimber.data.entities.Chattable.ChatType.GROUP
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Maciek on 29/01/2018.
 */
@Singleton
class BimberAnalytics
@Inject
constructor(private val firebaseAnalytics: FirebaseAnalytics) {


    enum class CreatePartyEntryPoint {
        DISCOVER_PEOPLE,
        DISCOVER_PARTIES,
        CHAT
    }

    enum class CreatePartyStage {
        CREATE_PARTY_INIT,
        CREATE_PARTY_SET_NAME,
        CREATE_PARTY_SET_DESCRIPTION,
        CREATE_PARTY_SET_LOCATION,
        CREATE_PARTY_SET_TIME,
        CREATE_PARTY_SET_PARTICIPANTS,
        CREATE_PARTY_FINAL
    }

    enum class ShareContentType {
        PERSON,
        PARTY
    }

    enum class ShareMethod {
        DEEPLINK
    }

    enum class Rating {
        LIKE,
        DISLIKE
    }

    enum class RatingContentType {
        PERSON,
        PARTY
    }

    enum class MessageType {
        TEXT,
        PICTURE
    }

    fun logPartyCreationInit(entryPoint: CreatePartyEntryPoint) {
        val bundle = Bundle()
        bundle.putString("entry_point", entryPoint.name.toLowerCase())
        firebaseAnalytics.logEvent(CreatePartyStage.CREATE_PARTY_INIT.name.toLowerCase(), bundle)
        firebaseAnalytics.setUserProperty("party_starter", "curious")
    }

    fun logPartyCreationStage(createPartyStage: CreatePartyStage) {
        firebaseAnalytics.logEvent(createPartyStage.name.toLowerCase(), Bundle())
        firebaseAnalytics.setUserProperty("party_starter", "determined")
    }

    fun logPartyCreationFinal(participantsCount: Int,
                              locationName: String,
                              latitude: Double,
                              longitude: Double,
                              success: Boolean) {
        val bundle = Bundle()
        bundle.putInt("participants_count", participantsCount)
        bundle.putString("location_name", locationName)
        bundle.putDouble("latitude", latitude)
        bundle.putDouble("longitude", longitude)
        bundle.putBoolean("success", success)
        firebaseAnalytics.logEvent(CreatePartyStage.CREATE_PARTY_FINAL.name.toLowerCase(), bundle)
        firebaseAnalytics.setUserProperty("party_starter", "determined")
    }

    fun logLocationSet(locationType: String, locationName: String) {
        val bundle = Bundle()
        bundle.putString("location_type", locationType)
        bundle.putString("location_name", locationName)
        firebaseAnalytics.logEvent("location_set", bundle)
    }

    fun logShare(contentId: String, contentName: String, contentType: ShareContentType, method: ShareMethod) {
        val bundle = Bundle()
        bundle.putString("content_id", contentId)
        bundle.putString("content_name", contentName)
        bundle.putString("content_type", contentType.name.toLowerCase())
        bundle.putString("method", method.name.toLowerCase())
        firebaseAnalytics.logEvent("share", bundle)

    }

    fun logRate(rating: Rating, contentType: RatingContentType) {
        val bundle = Bundle()
        bundle.putString("rating", rating.name.toLowerCase())
        bundle.putString("content_type", contentType.name.toLowerCase())
        firebaseAnalytics.logEvent("rate", bundle)
    }

    fun logMatch() {
        firebaseAnalytics.logEvent("match", Bundle())
        firebaseAnalytics.setUserProperty("friendly", "matched")
    }


    fun logSendMessage(messageType: MessageType, chatType: Chattable.ChatType) {
        val bundle = Bundle()
        bundle.putString("type", messageType.name.toLowerCase())
        when (chatType) {
            DIALOG -> bundle.putString("chat_type", "match")
            GROUP -> bundle.putString("chat_type", "party")
        }
        firebaseAnalytics.logEvent("send_message", bundle)
        firebaseAnalytics.setUserProperty("messenger", "sent_message")
    }

}


