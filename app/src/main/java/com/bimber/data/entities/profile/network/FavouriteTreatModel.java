package com.bimber.data.entities.profile.network;

import android.os.Parcelable;

import com.bimber.data.entities.IndexedData;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@AutoValue
@FirebaseValue
public abstract class FavouriteTreatModel implements IndexedData, Parcelable {

    public static FavouriteTreatModel create(long index, String name) {
        return new AutoValue_FavouriteTreatModel(index, name);
    }


    public static Key<FavouriteTreatModel> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_FavouriteTreatModel.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_FavouriteTreatModel.FirebaseValue(this);
    }

    public abstract String name();
}
