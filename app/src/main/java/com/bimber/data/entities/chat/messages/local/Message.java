package com.bimber.data.entities.chat.messages.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.bimber.data.entities.chat.messages.network.MessageModel;
import com.bimber.utils.firebase.Key;

import java.io.Serializable;

/**
 * Created by maciek on 23.11.17.
 */

@Entity(tableName = "chat_messages",
        primaryKeys = {"chatId", "messageId"},
        indices = {@Index(value = {"chatId", "timestamp", "contentType"})})
@TypeConverters(value = {MessageContentTypeConverter.class})
public class Message implements Serializable {

    @NonNull
    private String messageId;

    @NonNull
    private String chatId;

    private long timestamp;

    @NonNull
    private String userId;

    @Nullable
    private String userName;

    @NonNull
    private ContentType contentType;

    @NonNull
    private MessageContent content;

    public Message(@NonNull String messageId, @NonNull String chatId, long timestamp, @NonNull String userId,
                   @Nullable String userName, @NonNull ContentType contentType,
                   @NonNull MessageContent content) {
        this.messageId = messageId;
        this.chatId = chatId;
        this.timestamp = timestamp;
        this.userId = userId;
        this.userName = userName;
        this.contentType = contentType;
        this.content = content;
    }

    public Message(String chatId, Key<MessageModel> messageModel) {
        this(messageModel.key(), chatId, messageModel.value().timestamp().value(),
                messageModel.value().userId(), messageModel.value().userName(),
                messageModel.value().content().contentType(), messageModel.value().content());
    }

    @NonNull
    public String getMessageId() {
        return messageId;
    }

    @NonNull
    public String getChatId() {
        return chatId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @Nullable
    public String getUserName() {
        return userName;
    }

    @NonNull
    public ContentType getContentType() {
        return contentType;
    }

    @NonNull
    public MessageContent getContent() {
        return content;
    }

    public void setMessageId(@NonNull String messageId) {
        this.messageId = messageId;
    }

    public void setChatId(@NonNull String chatId) {
        this.chatId = chatId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public void setUserName(@Nullable String userName) {
        this.userName = userName;
    }

    public void setContentType(@NonNull ContentType contentType) {
        this.contentType = contentType;
    }

    public void setContent(@NonNull MessageContent content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (timestamp != message.timestamp) return false;
        if (!messageId.equals(message.messageId)) return false;
        if (!chatId.equals(message.chatId)) return false;
        if (!userId.equals(message.userId)) return false;
        if (userName != null ? !userName.equals(message.userName) : message.userName != null)
            return false;
        if (contentType != message.contentType) return false;
        return content.equals(message.content);
    }

    @Override
    public int hashCode() {
        int result = messageId.hashCode();
        result = 31 * result + chatId.hashCode();
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + userId.hashCode();
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + contentType.hashCode();
        result = 31 * result + content.hashCode();
        return result;
    }
}
