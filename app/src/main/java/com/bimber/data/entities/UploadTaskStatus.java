package com.bimber.data.entities;

import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

/**
 * Created by srokowski.maciej@gmail.com on 28.12.16.
 */
@AutoValue
public abstract class UploadTaskStatus {

    public abstract TaskStatus taskStatus();

    @Nullable
    public abstract Uri uri();

    public static UploadTaskStatus create(TaskStatus uploadStatus, Uri uri) {
        return new AutoValue_UploadTaskStatus(uploadStatus, uri);
    }

    public enum TaskStatus {
        PENDING,
        FINISHED
    }
}
