package com.bimber.data.entities.profile.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.network.LocationType;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */
@Entity(tableName = "users_locations")
public abstract class UserLocation implements Serializable {

    @PrimaryKey
    @ForeignKey(entity = User.class,
            parentColumns = "uId",
            childColumns = "locationUserId")
    @NonNull
    public final String locationUserId;

    @TypeConverters(LocationTypeConverter.class)
    @NonNull
    public final LocationType userLocationType;

    public final double userLocationLatitude;

    public final double userLocationLongitude;

    public final int userLocationRange;

    @NonNull
    public final String userLocationName;

    protected UserLocation(@NonNull String locationUserId, @NonNull LocationType userLocationType, double userLocationLatitude,
                           double userLocationLongitude, int userLocationRange, @NonNull String userLocationName) {
        this.locationUserId = locationUserId;
        this.userLocationType = userLocationType;
        this.userLocationLatitude = userLocationLatitude;
        this.userLocationLongitude = userLocationLongitude;
        this.userLocationRange = userLocationRange;
        this.userLocationName = userLocationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocation that = (UserLocation) o;

        if (Double.compare(that.userLocationLatitude, userLocationLatitude) != 0) return false;
        if (Double.compare(that.userLocationLongitude, userLocationLongitude) != 0) return false;
        if (userLocationRange != that.userLocationRange) return false;
        if (!locationUserId.equals(that.locationUserId)) return false;
        if (userLocationType != that.userLocationType) return false;
        return userLocationName.equals(that.userLocationName);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = locationUserId.hashCode();
        result = 31 * result + userLocationType.hashCode();
        temp = Double.doubleToLongBits(userLocationLatitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(userLocationLongitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + userLocationRange;
        result = 31 * result + userLocationName.hashCode();
        return result;
    }

    public static class LocationTypeConverter {
        @TypeConverter
        public LocationType fromString(String value) {
            return LocationType.valueOf(value);
        }

        @TypeConverter
        public String toString(LocationType value) {
            return value.toString();
        }
    }
}
