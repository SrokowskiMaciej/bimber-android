package com.bimber.data.entities.profile.network;

/**
 * Created by Maciek on 16/01/2018.
 */
public enum UploadStatus {
    FINISHED,
    PENDING,
    NOT_STARTED
}
