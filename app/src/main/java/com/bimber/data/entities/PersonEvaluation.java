package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.google.gson.Gson;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 28.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class PersonEvaluation implements Parcelable {


    public static final String STATUS_FIELD = "status";

    /**
     * Id of a dialog if users were matched with each other. This will return null if users were never matched with each other so with
     * status() returning LIKED or DISLIKED. It can however return a valid groupId on LIKED and DISLIKED when users were matched with
     * each other in the past but then one of them left the conversation.
     *
     * @return groupId or null
     */
    @Nullable
    public abstract String dialogId();

    @PropertyName(STATUS_FIELD)
    @FirebaseAdapter(PersonEvaluationStatusTypeAdapter.class)
    public abstract PersonEvaluationStatus status();

    public enum PersonEvaluationStatus {
        //NONE status is not settable on client side. It means that user is available for discovery as same as user without evaluation
        // status
        NONE,
        LIKED,
        DISLIKED,
        //MATCHED status is not settable on client side. Backend takes care of that
        MATCHED
    }

    public static PersonEvaluation create(PersonEvaluationStatus status) {
        return new AutoValue_PersonEvaluation(null, status);
    }

    public static Key<PersonEvaluation> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_PersonEvaluation.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_PersonEvaluation.FirebaseValue(this);
    }


    public static class PersonEvaluationStatusTypeAdapter implements TypeAdapter<PersonEvaluationStatus, String> {
        @Override
        public PersonEvaluationStatus fromFirebaseValue(String value) {
            return PersonEvaluationStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(PersonEvaluationStatus value) {
            return value.toString();
        }
    }

    public static com.google.gson.TypeAdapter<PersonEvaluation> typeAdapter(Gson gson) {
        return new AutoValue_PersonEvaluation.GsonTypeAdapter(gson);
    }

}
