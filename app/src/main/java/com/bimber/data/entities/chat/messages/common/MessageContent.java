package com.bimber.data.entities.chat.messages.common;

import com.google.firebase.database.PropertyName;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;

/**
 * Created by maciek on 26.04.17.
 */

public interface MessageContent {

    String CONTENT_TYPE_FIELD = "contentType";

    //Ups, create separate entity just for local cache usage so that it is not mixed with network models
    @PropertyName(CONTENT_TYPE_FIELD)
    @FirebaseAdapter(ContentType.ContentTypeAdapter.class)
    ContentType contentType();

    Object toFirebaseValue();
}
