package com.bimber.data.entities.chat.messages.network;

import android.support.annotation.Nullable;

import com.google.firebase.database.PropertyName;

/**
 * Created by maciek on 26.04.17.
 */

public class MessageFirebaseValue {

    public static final String MESSAGE_TIMESTAMP_FIELD = "timestamp";
    public static final String MESSAGE_CONTENT_FIELD = "content";
    public static final String MESSAGE_UID_FIELD = "userId";
    public static final String MESSAGE_USER_NAME_FIELD = "userName";

    @PropertyName(MESSAGE_CONTENT_FIELD)
    public Object content;
    @PropertyName(MESSAGE_TIMESTAMP_FIELD)
    public Object timestamp;
    @PropertyName(MESSAGE_UID_FIELD)
    public String userId;
    @PropertyName(MESSAGE_USER_NAME_FIELD)
    @Nullable
    public String userName;

    public MessageFirebaseValue() {
    }

    public MessageFirebaseValue(Object content, Object timestamp, String userId, String userName) {
        this.content = content;
        this.timestamp = timestamp;
        this.userId = userId;
        this.userName = userName;
    }
}
