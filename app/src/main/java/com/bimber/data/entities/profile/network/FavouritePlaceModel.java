package com.bimber.data.entities.profile.network;

import android.net.Uri;
import android.os.Parcelable;

import com.bimber.data.entities.IndexedData;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.maps.PlacePickerUtils;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Exclude;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */

@AutoValue
@FirebaseValue
public abstract class FavouritePlaceModel implements Parcelable, IndexedData {

    public static final String GOOGLE_MAPS_SEARCH_URI = "https://www.google.com/maps/search/?api=1&query=%s&query_place_id=%s";
    public static final String GOOGLE_MAPS_DIRECTIONS_URI = "https://www.google.com/maps/dir/?api=1&destination=%s&destination_place_id=%s";



    public static FavouritePlaceModel create(long index, String name, String address, String id, double longitude, double latitude) {
        return new AutoValue_FavouritePlaceModel(index, name, address, id, latitude, longitude);
    }

    public static Key<FavouritePlaceModel> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_FavouritePlaceModel.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_FavouritePlaceModel.FirebaseValue(this);
    }

    public abstract String name();

    public abstract String address();

    public abstract String id();

    public abstract double latitude();

    public abstract double longitude();

    @Exclude
    public Uri getGoogleMapsSearchUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_SEARCH_URI, Uri.encode(latitude() + "," + longitude()), id()));
    }

    @Exclude
    public Uri getGoogleMapsDirectionsUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_DIRECTIONS_URI, Uri.encode(latitude() + "," + longitude()), id()));
    }

    @Exclude
    public boolean isPlaceNameHumanReadable() {
        return !PlacePickerUtils.isPlaceNameLatLngEncoded(name());
    }

    @Exclude
    public boolean isPlaceAddressHumanReadable() {
        return !address().isEmpty();
    }

    @Exclude
    public String getNormalizedPlaceName() {
        if (isPlaceNameHumanReadable() || !isPlaceAddressHumanReadable()) {
            return name();
        } else {
            return address();
        }
    }
}
