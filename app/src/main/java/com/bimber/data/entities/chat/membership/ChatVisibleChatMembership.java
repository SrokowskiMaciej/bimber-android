package com.bimber.data.entities.chat.membership;

import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 05.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class ChatVisibleChatMembership implements ChatMembership, Serializable {

    public static final String LAST_INTERACTION_FIELD_NAME = "lastInteraction";

    /**
     * User buttonVisible interaction tells use when did user did something to change status of a chat. For example he has written a message
     * is a chat
     *
     * @return last interaction timestamp
     */
    @PropertyName(LAST_INTERACTION_FIELD_NAME)
    public abstract long lastInteraction();

    public static ChatVisibleChatMembership create(String uId, String chatId, ChatType chatType, MembershipStatus membershipStatus, Timestamp leaveTimestamp, long lastInteraction) {
        return new AutoValue_ChatVisibleChatMembership(uId, chatId, chatType, membershipStatus, leaveTimestamp, lastInteraction);
    }

    public static ChatVisibleChatMembership create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_ChatVisibleChatMembership.FirebaseValue.class).toAutoValue();
    }
}
