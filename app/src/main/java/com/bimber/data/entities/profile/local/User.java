package com.bimber.data.entities.profile.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.network.Gender;
import com.bimber.data.entities.profile.network.UserModel;
import com.bimber.utils.firebase.Key;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 02.11.16.
 */

@Entity(tableName = "users")
public class User implements Serializable{

    @PrimaryKey
    @NonNull
    public final String uId;

    @NonNull
    public final String displayName;

    @NonNull
    public final String fullName;

    @NonNull
    public final String about;

    @TypeConverters({GenderConverter.class})
    @NonNull
    public final Gender gender;

    public final int age;

    public final int friends;

    public final int parties;

    public User(@NonNull String uId, @NonNull String displayName, @NonNull String fullName, @NonNull String about,
                @NonNull Gender gender, int age, int friends, int parties) {
        this.uId = uId;
        this.displayName = displayName;
        this.fullName = fullName;
        this.about = about;
        this.gender = gender;
        this.age = age;
        this.friends = friends;
        this.parties = parties;
    }

    public User(Key<UserModel> userModel) {
        this(userModel.key(), userModel.value().displayName(), userModel.value().fullName(), userModel.value().about(),
                userModel.value().gender(), userModel.value().age(), userModel.value().friends(), userModel.value().parties());
    }

    @NonNull
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (age != user.age) return false;
        if (friends != user.friends) return false;
        if (parties != user.parties) return false;
        if (!displayName.equals(user.displayName)) return false;
        if (!fullName.equals(user.fullName)) return false;
        if (!about.equals(user.about)) return false;
        return gender == user.gender;
    }

    @Override
    public int hashCode() {
        int result = displayName.hashCode();
        result = 31 * result + fullName.hashCode();
        result = 31 * result + about.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + age;
        result = 31 * result + friends;
        result = 31 * result + parties;
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "displayName='" + displayName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", about='" + about + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", friends=" + friends +
                ", parties=" + parties +
                '}';
    }

    public static class GenderConverter {

        @TypeConverter
        public Gender fromString(String value) {
            return Gender.valueOf(value);
        }

        @TypeConverter
        public String toString(Gender value) {
            return value.toString();
        }

    }
}