package com.bimber.data.entities.chat.messages.local.aggregated;

import android.arch.persistence.room.Embedded;
import android.support.annotation.NonNull;

import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;

/**
 * Created by Maciek on 27/01/2018.
 */

public class MessageWithSenderData {

    @Embedded
    @NonNull
    public final Message message;

    @Embedded
    @NonNull
    public final ProfileDataThumbnail senderData;

    public MessageWithSenderData(@NonNull Message message, @NonNull ProfileDataThumbnail senderData) {
        this.message = message;
        this.senderData = senderData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageWithSenderData that = (MessageWithSenderData) o;

        if (!message.equals(that.message)) return false;
        return senderData.equals(that.senderData);
    }

    @Override
    public int hashCode() {
        int result = message.hashCode();
        result = 31 * result + senderData.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MessageWithSenderData{" +
                "message=" + message +
                ", senderData=" + senderData +
                '}';
    }
}
