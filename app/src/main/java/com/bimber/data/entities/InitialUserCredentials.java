package com.bimber.data.entities;

import com.google.auto.value.AutoValue;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 30.10.17.
 */

@AutoValue
@FirebaseValue
public abstract class InitialUserCredentials {


    public abstract String providerType();

    public abstract String token();

    public static InitialUserCredentials create(String providerType, String token) {
        return new AutoValue_InitialUserCredentials(providerType, token);
    }

    public Object toFirebaseValue() {
        return new AutoValue_InitialUserCredentials.FirebaseValue(this);
    }
}
