package com.bimber.data.entities.chat.membership;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 05.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class UserVisibleChatMembership implements ChatMembership {

    public static final String LAST_INTERACTION_FIELD_NAME = "lastInteraction";

    /**
     * User buttonVisible interaction tells use when did chat become updated in a way that is meaningful to user. E.g. this or ther user
     * has written a message to the chat
     *
     * @return last interaction timestamp
     */
    @PropertyName(LAST_INTERACTION_FIELD_NAME)
    public abstract long lastInteraction();


    //Only for dummy values, usually chat memberships are created on backend side
    public static UserVisibleChatMembership create(String uId, String chatId, ChatType chatType, MembershipStatus membershipStatus,
                                                   Timestamp leaveTimestamp, long lastInteraction) {
        return new AutoValue_UserVisibleChatMembership(uId, chatId, chatType, membershipStatus, leaveTimestamp, lastInteraction);
    }

    public static UserVisibleChatMembership create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_UserVisibleChatMembership.FirebaseValue.class).toAutoValue();
    }
}
