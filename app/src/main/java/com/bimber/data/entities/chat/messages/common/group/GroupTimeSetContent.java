package com.bimber.data.entities.chat.messages.common.group;

import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 25.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class GroupTimeSetContent implements MessageContent {
    public abstract long timestamp();

    public static GroupTimeSetContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_GroupTimeSetContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_GroupTimeSetContent.FirebaseValue(this);
    }

    public static TypeAdapter<GroupTimeSetContent> typeAdapter(Gson gson) {
        return new AutoValue_GroupTimeSetContent.GsonTypeAdapter(gson);
    }
}
