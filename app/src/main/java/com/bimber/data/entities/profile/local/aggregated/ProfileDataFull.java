package com.bimber.data.entities.profile.local.aggregated;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.local.FavouritePlace;
import com.bimber.data.entities.profile.local.FavouriteTreat;
import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;

import java.util.List;

/**
 * Created by srokowski.maciej@gmail.com on 03.01.17.
 */
public class ProfileDataFull {

    public ProfileDataFull() {
    }

    public ProfileDataFull(@NonNull User user, @NonNull List<UserPhoto> photos, @NonNull List<FavouriteTreat> alcohols,
                           @NonNull List<FavouritePlace> favouritePlaces) {
        this.user = user;
        this.photos = photos;
        this.alcohols = alcohols;
        this.favouritePlaces = favouritePlaces;
    }

    @Embedded
    @NonNull
    public User user;

    @Relation(entity = UserPhoto.class,
            entityColumn = "photoUserId",
            parentColumn = "uId")
    @NonNull
    public List<UserPhoto> photos;

    @Relation(entity = FavouriteTreat.class,
            entityColumn = "favouriteTreatUserId",
            parentColumn = "uId")
    public List<FavouriteTreat> alcohols;

    @Relation(entity = FavouritePlace.class,
            entityColumn = "favouritePlaceUserId",
            parentColumn = "uId")
    public List<FavouritePlace> favouritePlaces;
}
