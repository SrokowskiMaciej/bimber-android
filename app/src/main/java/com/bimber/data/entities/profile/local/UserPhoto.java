package com.bimber.data.entities.profile.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bimber.data.entities.profile.network.UploadStatus;
import com.bimber.data.entities.profile.network.UserPhotoModel;
import com.bimber.utils.firebase.Key;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Entity(tableName = "users_photos",
        primaryKeys = {"photoUserId", "userPhotoIndex"},
        indices = {@Index(value = "photoId")})
public class UserPhoto implements Serializable {

    @ForeignKey(entity = User.class,
            parentColumns = "uId",
            childColumns = "photoUserId")
    @NonNull
    public final String photoUserId;

    @NonNull
    public final String photoId;

    public final int userPhotoIndex;

    @Nullable
    public final String userPhotoUri;

    @Nullable
    public final String userPhotoThumb;

    @TypeConverters(UploadStatusConverter.class)
    @NonNull
    public final UploadStatus userPhotoUploadStatus;

    public UserPhoto(@NonNull String photoUserId, @NonNull String photoId, int userPhotoIndex,
                     @Nullable String userPhotoUri, @Nullable String userPhotoThumb, @NonNull UploadStatus userPhotoUploadStatus) {
        this.photoUserId = photoUserId;
        this.photoId = photoId;
        this.userPhotoIndex = userPhotoIndex;
        this.userPhotoUri = userPhotoUri;
        this.userPhotoThumb = userPhotoThumb;
        this.userPhotoUploadStatus = userPhotoUploadStatus;
    }

    public UserPhoto(@NonNull String uId, @NonNull Key<UserPhotoModel> userPhotoModel) {
        this(uId, userPhotoModel.key(), (int) userPhotoModel.value().index(), userPhotoModel.value().uri(),
                userPhotoModel.value().thumb(), userPhotoModel.value().uploadStatus());
    }

    public UserPhoto(@NonNull String uId, @NonNull Key<UserPhotoModel> userPhotoModel, int index) {
        this(uId, userPhotoModel.key(), index, userPhotoModel.value().uri(),
                userPhotoModel.value().thumb(), userPhotoModel.value().uploadStatus());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPhoto userPhoto = (UserPhoto) o;

        if (userPhotoIndex != userPhoto.userPhotoIndex) return false;
        if (!photoUserId.equals(userPhoto.photoUserId)) return false;
        if (!photoId.equals(userPhoto.photoId)) return false;
        if (userPhotoUri != null ? !userPhotoUri.equals(userPhoto.userPhotoUri) : userPhoto.userPhotoUri != null) return false;
        if (userPhotoThumb != null ? !userPhotoThumb.equals(userPhoto.userPhotoThumb) : userPhoto.userPhotoThumb != null) return false;
        return userPhotoUploadStatus == userPhoto.userPhotoUploadStatus;
    }

    @Override
    public int hashCode() {
        int result = photoUserId.hashCode();
        result = 31 * result + photoId.hashCode();
        result = 31 * result + userPhotoIndex;
        result = 31 * result + (userPhotoUri != null ? userPhotoUri.hashCode() : 0);
        result = 31 * result + (userPhotoThumb != null ? userPhotoThumb.hashCode() : 0);
        result = 31 * result + userPhotoUploadStatus.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserPhoto{" +
                "photoUserId='" + photoUserId + '\'' +
                ", photoId='" + photoId + '\'' +
                ", index=" + userPhotoIndex +
                ", uri='" + userPhotoUri + '\'' +
                ", thumb='" + userPhotoThumb + '\'' +
                ", uploadStatus=" + userPhotoUploadStatus +
                '}';
    }

    public static class UploadStatusConverter {
        @TypeConverter
        public UploadStatus fromString(String value) {
            return UploadStatus.valueOf(value);
        }

        @TypeConverter
        public String toString(UploadStatus value) {
            return value.toString();
        }
    }
}
