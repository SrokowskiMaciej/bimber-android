package com.bimber.data.entities.profile.network;

/**
 * Created by Maciek on 16/01/2018.
 */
public enum LocationType {
    CUSTOM_LOCATION,
    CURRENT_LOCATION
}
