package com.bimber.data.entities.chat.messages.common.participation;

import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 25.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class ParticipationPersonAddedContent implements MessageContent {
    public abstract String addedPersonName();

    public abstract String addedPersonId();

    public static ParticipationPersonAddedContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_ParticipationPersonAddedContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_ParticipationPersonAddedContent.FirebaseValue(this);
    }

    public static TypeAdapter<ParticipationPersonAddedContent> typeAdapter(Gson gson) {
        return new AutoValue_ParticipationPersonAddedContent.GsonTypeAdapter(gson);
    }
}