package com.bimber.data.entities.profile.network;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.data.entities.IndexedData;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@AutoValue
@FirebaseValue
public abstract class UserPhotoModel implements IndexedData, Parcelable, Serializable {

    public static final String UPLOAD_STATUS_FIELD_NAME = "uploadStatus";
    public static final String URI_FIELD_NAME = "uri";

    public static UserPhotoModel create(long index, String uri, UploadStatus uploadStatus) {
        return new AutoValue_UserPhotoModel(index, uri, null, uploadStatus);
    }

    public static Key<UserPhotoModel> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_UserPhotoModel.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_UserPhotoModel.FirebaseValue(this);
    }

    @Nullable
    @PropertyName(URI_FIELD_NAME)
    public abstract String uri();

    @Nullable
    public abstract String thumb();

    @FirebaseAdapter(UploadStatusTypeAdapter.class)
    @PropertyName(UPLOAD_STATUS_FIELD_NAME)
    public abstract UploadStatus uploadStatus();

    public static class UploadStatusTypeAdapter implements TypeAdapter<UploadStatus, String> {
        @Override
        public UploadStatus fromFirebaseValue(String value) {
            return UploadStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(UploadStatus value) {
            return value.toString();
        }
    }
}
