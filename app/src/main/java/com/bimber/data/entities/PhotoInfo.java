package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by maciek on 02.10.17.
 */
@AutoValue
public abstract class PhotoInfo implements Parcelable {

    public abstract String sourceId();

    @Nullable
    public abstract String photoUri();

    public static PhotoInfo create(String sourceId, String photoUri) {
        return new AutoValue_PhotoInfo(sourceId, photoUri);
    }

    public static TypeAdapter<PhotoInfo> typeAdapter(Gson gson) {
        return new AutoValue_PhotoInfo.GsonTypeAdapter(gson);
    }
}
