package com.bimber.data.entities.chat.messages.common.participation;

import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 25.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class ParticipationPersonRemovedContent implements MessageContent {
    public abstract String removedPersonName();

    public abstract String removedPersonId();

    public static ParticipationPersonRemovedContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_ParticipationPersonRemovedContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_ParticipationPersonRemovedContent.FirebaseValue(this);
    }

    public static TypeAdapter<ParticipationPersonRemovedContent> typeAdapter(Gson gson) {
        return new AutoValue_ParticipationPersonRemovedContent.GsonTypeAdapter(gson);
    }
}