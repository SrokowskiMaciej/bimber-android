package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 22.01.17.
 */
@AutoValue
@FirebaseValue
public abstract class Group implements Chattable, Parcelable, Serializable {


    public static final String GROUP_OWNER_ID_PROPERTY_NAME = "groupOwnerId";
    public static final String GROUP_LAST_EDITOR_ID_PROPERTY_NAME = "lastEditorId";
    public static final String GROUP_NAME_PROPERTY_NAME = "name";
    public static final String GROUP_DESCRIPTION_PROPERTY_NAME = "description";
    public static final String GROUP_LOCATION_PROPERTY_NAME = "location";
    public static final String GROUP_MEETING_TIME_PROPERTY_NAME = "meetingTime";
    public static final String GROUP_IMAGE_PROPERTY_NAME = "image";
    public static final String GROUP_SECURE_PROPERTY_NAME = "secure";

    public Object toFirebaseValue() {
        return new AutoValue_Group.FirebaseValue(this);
    }

    @PropertyName(GROUP_NAME_PROPERTY_NAME)
    public abstract String name();

    @PropertyName(GROUP_DESCRIPTION_PROPERTY_NAME)
    public abstract String description();

    @PropertyName(GROUP_LOCATION_PROPERTY_NAME)
    public abstract Place location();

    @FirebaseAdapter(Timestamp.Adapter.class)
    public abstract Timestamp groupCreationTimestamp();

    @PropertyName(GROUP_MEETING_TIME_PROPERTY_NAME)
    @FirebaseAdapter(Timestamp.Adapter.class)
    public abstract Timestamp meetingTime();

    @PropertyName(GROUP_OWNER_ID_PROPERTY_NAME)
    public abstract String groupOwnerId();


    @PropertyName(GROUP_LAST_EDITOR_ID_PROPERTY_NAME)
    public abstract String lastEditorId();

    @Nullable
    @PropertyName(GROUP_IMAGE_PROPERTY_NAME)
    public abstract String image();

    public abstract int membersCount();

    @PropertyName(GROUP_SECURE_PROPERTY_NAME)
    public abstract boolean secure();

    public abstract boolean discoverable();

    public abstract boolean activated();

    public abstract boolean meetingReminderSent();

    public abstract boolean meetingNoticeSent();

    public abstract boolean deactivated();

    public static Group create(String name, String description, Place place, Timestamp groupCreationTimestamp,
                               Timestamp meetingTime, String lastEditorId, String groupOwnerId, boolean secure) {
        return new AutoValue_Group(ChatType.GROUP, name, description, place, groupCreationTimestamp, meetingTime,
                groupOwnerId, lastEditorId, null, 0, secure, false, false, false, false, false);
    }

    public static Key<Group> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_Group.FirebaseValue.class).toAutoValue());
    }

}


