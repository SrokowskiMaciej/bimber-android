package com.bimber.data.entities;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

/**
 * Created by maciek on 27.07.17.
 */
@AutoValue
public abstract class GroupMeetingPushMessage implements Parcelable {

    public abstract String groupId();

    public abstract String groupName();

    public abstract List<PhotoInfo> photos();

    public abstract NotificationType notificationType();

    public abstract long activateTime();

    public abstract long reminderTime();

    public abstract long noticeTime();

    public abstract long deactivateTime();

    // The public static method returning a TypeAdapter<Foo> is what
    // tells auto-value-gson to create a TypeAdapter for Foo.
    public static TypeAdapter<GroupMeetingPushMessage> typeAdapter(Gson gson) {
        return new AutoValue_GroupMeetingPushMessage.GsonTypeAdapter(gson);
    }

    public enum NotificationType {
        REMINDER,
        NOTICE,
        DEACTIVATION
    }
}

