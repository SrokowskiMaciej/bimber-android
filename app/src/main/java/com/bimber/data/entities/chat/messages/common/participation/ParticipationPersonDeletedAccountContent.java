package com.bimber.data.entities.chat.messages.common.participation;

import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 25.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class ParticipationPersonDeletedAccountContent implements MessageContent {
    public abstract String personDeletedAccountName();

    public abstract String personDeletedAccountId();

    public static ParticipationPersonDeletedAccountContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_ParticipationPersonDeletedAccountContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_ParticipationPersonDeletedAccountContent.FirebaseValue(this);
    }

    public static TypeAdapter<ParticipationPersonDeletedAccountContent> typeAdapter(Gson gson) {
        return new AutoValue_ParticipationPersonDeletedAccountContent.GsonTypeAdapter(gson);
    }
}