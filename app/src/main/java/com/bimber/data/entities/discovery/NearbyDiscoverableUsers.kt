package com.bimber.data.entities.discovery

/**
 * Created by Maciek on 26/02/2018.
 */
data class NearbyDiscoverableUsers(val discoverableUsers: List<String>,
                                   val limit: Int)