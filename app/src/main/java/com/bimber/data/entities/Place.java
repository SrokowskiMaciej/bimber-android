package com.bimber.data.entities;

import android.net.Uri;
import android.os.Parcelable;

import com.bimber.utils.maps.PlacePickerUtils;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Exclude;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 26.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class Place implements Parcelable, Serializable {

    public static final String GOOGLE_MAPS_SEARCH_URI = "https://www.google.com/maps/search/?api=1&query=%s&query_place_id=%s";
    public static final String GOOGLE_MAPS_DIRECTIONS_URI = "https://www.google.com/maps/dir/?api=1&destination=%s&destination_place_id=%s";


    public static Place create(String name, String address, String id, double longitude, double latitude) {
        return new AutoValue_Place(name, address, id, latitude, longitude);
    }

    public static Place create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_Place.FirebaseValue.class).toAutoValue();
    }

    public Object toFirebaseValue() {
        return new AutoValue_Place.FirebaseValue(this);
    }

    public abstract String name();

    public abstract String address();

    public abstract String id();

    public abstract double latitude();

    public abstract double longitude();

    @Exclude
    public Uri getGoogleMapsSearchUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_SEARCH_URI, Uri.encode(latitude() + "," + longitude()), id()));
    }

    @Exclude
    public Uri getGoogleMapsDirectionsUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_DIRECTIONS_URI, Uri.encode(latitude() + "," + longitude()), id()));
    }

    @Exclude
    public boolean isPlaceNameHumanReadable() {
        return !PlacePickerUtils.isPlaceNameLatLngEncoded(name());
    }

    @Exclude
    public boolean isPlaceAddressHumanReadable() {
        return !address().isEmpty();
    }

    @Exclude
    public String getNormalizedPlaceName() {
        if (isPlaceNameHumanReadable() || !isPlaceAddressHumanReadable()) {
            return name();
        } else {
            return address();
        }
    }

    public static TypeAdapter<Place> typeAdapter(Gson gson) {
        return new AutoValue_Place.GsonTypeAdapter(gson);
    }
}
