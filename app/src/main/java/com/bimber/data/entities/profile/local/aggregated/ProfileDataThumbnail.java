package com.bimber.data.entities.profile.local.aggregated;

import android.arch.persistence.room.Embedded;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.local.User;
import com.bimber.data.entities.profile.local.UserPhoto;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 23.01.17.
 */
public class ProfileDataThumbnail implements Serializable {

    public ProfileDataThumbnail(@NonNull User user, @NonNull UserPhoto photo) {
        this.user = user;
        this.photo = photo;
    }

    @Embedded
    @NonNull
    public final User user;

    @Embedded
    @NonNull
    public final UserPhoto photo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileDataThumbnail that = (ProfileDataThumbnail) o;

        if (!user.equals(that.user)) return false;
        return photo.equals(that.photo);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + photo.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ProfileDataThumbnail{" +
                "user=" + user +
                ", photo=" + photo +
                '}';
    }
}
