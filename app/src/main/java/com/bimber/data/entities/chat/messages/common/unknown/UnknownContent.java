package com.bimber.data.entities.chat.messages.common.unknown;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 26.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class UnknownContent implements MessageContent {

    public static UnknownContent create() {
        return new AutoValue_UnknownContent(ContentType.UNKNOWN);
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_UnknownContent.FirebaseValue(this);
    }

    public static TypeAdapter<UnknownContent> typeAdapter(Gson gson) {
        return new AutoValue_UnknownContent.GsonTypeAdapter(gson);
    }
}
