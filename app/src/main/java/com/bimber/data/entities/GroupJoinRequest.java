package com.bimber.data.entities;

import android.os.Parcelable;

import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.google.gson.Gson;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 27.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class GroupJoinRequest implements Parcelable {


    public static final String GROUP_JOIN_REQUEST_STATUS_FIELD = "status";

    @FirebaseAdapter(GroupJoinRequestStatusTypeAdapter.class)
    @PropertyName(GROUP_JOIN_REQUEST_STATUS_FIELD)
    public abstract GroupJoinStatus status();

    public abstract String lastEditorId();

    public static GroupJoinRequest create(GroupJoinStatus status, String lastEditorId) {
        return new AutoValue_GroupJoinRequest(status, lastEditorId);
    }

    public static Key<GroupJoinRequest> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_GroupJoinRequest.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_GroupJoinRequest.FirebaseValue(this);
    }


    public enum GroupJoinStatus {
        //NONE status is not settable on client side. It means that group is available for discovery as same as group without evaluation
        // status
        NONE,
        //Can be set only when creating the group by group owner
        INITIAL,
        NOT_INTERESTED,
        MEMBERSHIP_REQUESTED,
        ACCEPTED,
        REJECTED,
        REMOVED,
        LEFT,
    }


    public static class GroupJoinRequestStatusTypeAdapter implements TypeAdapter<GroupJoinStatus, String> {
        @Override
        public GroupJoinStatus fromFirebaseValue(String value) {
            return GroupJoinStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(GroupJoinStatus value) {
            return value.toString();
        }
    }

    public static com.google.gson.TypeAdapter<GroupJoinRequest> typeAdapter(Gson gson) {
        return new AutoValue_GroupJoinRequest.GsonTypeAdapter(gson);
    }
}
