package com.bimber.data.entities;

import android.os.Parcelable;

import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 22.01.17.
 */
@AutoValue
@FirebaseValue
public abstract class Dialog implements Chattable, Parcelable {


    public static Dialog create(Timestamp timestamp) {
        return new AutoValue_Dialog(ChatType.DIALOG, timestamp);
    }

    public static Key<Dialog> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_Dialog.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_Dialog.FirebaseValue(this);
    }

    @FirebaseAdapter(Timestamp.Adapter.class)
    public abstract Timestamp dialogCreationTimestamp();
}
