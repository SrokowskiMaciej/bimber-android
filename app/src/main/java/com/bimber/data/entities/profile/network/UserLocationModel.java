package com.bimber.data.entities.profile.network;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 17.01.17.
 */

@AutoValue
@FirebaseValue
public abstract class UserLocationModel implements Parcelable {

    public static final String UID_FIELD_NAME = "personUid";

    public static UserLocationModel create(LocationType type, String uid, double latitude, double longitude, int range, String locationName) {
        return new AutoValue_UserLocationModel(type, uid, latitude, longitude, range, locationName);
    }

    public static UserLocationModel create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_UserLocationModel.FirebaseValue.class).toAutoValue();
    }

    public Object toFirebaseValue() {
        return new AutoValue_UserLocationModel.FirebaseValue(this);
    }

    @FirebaseAdapter(LocationTypeAdapter.class)
    public abstract LocationType type();

    @PropertyName(UID_FIELD_NAME)
    public abstract String personUid();

    public abstract double latitude();

    public abstract double longitude();

    public abstract int range();

    public abstract String locationName();

    public static class LocationTypeAdapter implements TypeAdapter<LocationType, String> {

        @Override
        public LocationType fromFirebaseValue(String value) {
            return LocationType.valueOf(value);
        }

        @Override
        public String toFirebaseValue(LocationType value) {
            return value.toString();
        }
    }
}
