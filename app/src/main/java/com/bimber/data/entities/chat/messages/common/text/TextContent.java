package com.bimber.data.entities.chat.messages.common.text;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 25.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class TextContent implements MessageContent {
    public abstract String text();

    public static TextContent create(String text) {
        return new AutoValue_TextContent(ContentType.TEXT, text);
    }

    public static TextContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_TextContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_TextContent.FirebaseValue(this);
    }

    public static TypeAdapter<TextContent> typeAdapter(Gson gson) {
        return new AutoValue_TextContent.GsonTypeAdapter(gson);
    }
}
