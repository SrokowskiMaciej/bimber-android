package com.bimber.data.entities.chat.messages.local;

import android.arch.persistence.room.TypeConverter;

import com.bimber.base.application.BimberApplication;
import com.bimber.data.entities.chat.messages.common.ContentDeserializer;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;

/**
 * Created by maciek on 23.11.17.
 */

public class MessageContentTypeConverter {

    @TypeConverter
    public static MessageContent fromJson(String value) {
        return ContentDeserializer.deserialize(value);
    }

    @TypeConverter
    public static String toJson(MessageContent messageContent) {
        return BimberApplication.gson.toJson(messageContent);
    }

    @TypeConverter
    public static ContentType fromString(String value) {
        return ContentType.valueOf(value);
    }

    @TypeConverter
    public static String fromEnum(ContentType contentType) {
        return contentType.name();
    }
}
