package com.bimber.data.entities;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by maciek on 23.08.17.
 */
@AutoValue
public abstract class NotificationMuteState {

    public abstract State state();

    public abstract long mutedTimestamp();

    public static NotificationMuteState create(State state, long mutedTimestamp) {
        return new AutoValue_NotificationMuteState(state, mutedTimestamp);
    }


    public enum State {
        NORMAL,
        NO_SOUND,
        MUTE_1_HOUR,
        MUTED_INDEFINITELY;

        public static final State values[] = values();

        public int position() {
            return ordinal();
        }

        public static State fromPosition(int position) {
            return values[position];
        }
    }

    public static TypeAdapter<NotificationMuteState> typeAdapter(Gson gson) {
        return new AutoValue_NotificationMuteState.GsonTypeAdapter(gson);
    }

}
