package com.bimber.data.entities.chat.messages.common.image;

import android.support.annotation.Nullable;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.google.gson.Gson;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 16.05.17.
 */
@AutoValue
@FirebaseValue
public abstract class ImageContent implements MessageContent, Serializable {

    public static final String UPLOAD_STATUS_FIELD_NAME = "uploadStatus";
    public static final String URI_FIELD_NAME = "uri";


    @Nullable
    @PropertyName(URI_FIELD_NAME)
    public abstract String uri();


    public abstract float dimensionRatio();


    @FirebaseAdapter(UploadStatusTypeAdapter.class)
    @PropertyName(UPLOAD_STATUS_FIELD_NAME)
    public abstract UploadStatus uploadStatus();


    @Override
    public Object toFirebaseValue() {
        return new AutoValue_ImageContent.FirebaseValue(this);
    }

    public static ImageContent create(String uri, float dimensionRatio, UploadStatus uploadStatus) {
        return new AutoValue_ImageContent(ContentType.IMAGE, uri, dimensionRatio, uploadStatus);
    }


    public static ImageContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_ImageContent.FirebaseValue.class).toAutoValue();
    }


    public enum UploadStatus {
        NOT_STARTED,
        PENDING,
        FINISHED
    }


    public static class UploadStatusTypeAdapter implements TypeAdapter<UploadStatus, String> {
        @Override
        public UploadStatus fromFirebaseValue(String value) {
            return UploadStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(UploadStatus value) {
            return value.toString();
        }
    }

    public static com.google.gson.TypeAdapter<ImageContent> typeAdapter(Gson gson) {
        return new AutoValue_ImageContent.GsonTypeAdapter(gson);
    }
}
