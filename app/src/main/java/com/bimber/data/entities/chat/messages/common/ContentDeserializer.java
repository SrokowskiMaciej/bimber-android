package com.bimber.data.entities.chat.messages.common;

import com.bimber.base.application.BimberApplication;
import com.bimber.data.entities.chat.messages.common.group.GroupLocationSetContent;
import com.bimber.data.entities.chat.messages.common.group.GroupDescriptionSetContent;
import com.bimber.data.entities.chat.messages.common.group.GroupImageSetContent;
import com.bimber.data.entities.chat.messages.common.group.GroupNameSetContent;
import com.bimber.data.entities.chat.messages.common.group.GroupTimeSetContent;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.data.entities.chat.messages.common.initial.InitialMessageContent;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonAddedContent;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonDeletedAccountContent;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonLeftContent;
import com.bimber.data.entities.chat.messages.common.participation.ParticipationPersonRemovedContent;
import com.bimber.data.entities.chat.messages.common.removed.RemovedContent;
import com.bimber.data.entities.chat.messages.common.text.TextContent;
import com.bimber.data.entities.chat.messages.common.unknown.UnknownContent;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by maciek on 27.04.17.
 */

public class ContentDeserializer {

    public static MessageContent deserialize(ContentType contentType, DataSnapshot dataSnapshot) {
        switch (contentType) {
            case IMAGE:
                return ImageContent.create(dataSnapshot);
            case INITIAL_MESSAGE:
                return InitialMessageContent.create(dataSnapshot);
            case TEXT:
                return TextContent.create(dataSnapshot);
            case REMOVED:
                return RemovedContent.create(dataSnapshot);
            case PARTICIPATION_PERSON_ADDED:
                return ParticipationPersonAddedContent.create(dataSnapshot);
            case PARTICIPATION_PERSON_REMOVED:
                return ParticipationPersonRemovedContent.create(dataSnapshot);
            case PARTICIPATION_PERSON_LEFT:
                return ParticipationPersonLeftContent.create(dataSnapshot);
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return ParticipationPersonDeletedAccountContent.create(dataSnapshot);
            case GROUP_TIME_SET:
                return GroupTimeSetContent.create(dataSnapshot);
            case GROUP_LOCATION_SET:
                return GroupLocationSetContent.create(dataSnapshot);
            case GROUP_NAME_SET:
                return GroupNameSetContent.create(dataSnapshot);
            case GROUP_DESCRIPTION_SET:
                return GroupDescriptionSetContent.create(dataSnapshot);
            case GROUP_IMAGE_SET:
                return GroupImageSetContent.create(dataSnapshot);
            case UNKNOWN:
            default:
                return UnknownContent.create();
        }
    }

    public static MessageContent deserialize(String json) {
        JsonObject jsonElement = BimberApplication.gson.fromJson(json, JsonObject.class);
        JsonElement jsonElement1 = jsonElement.get(MessageContent.CONTENT_TYPE_FIELD);
        ContentType contentType = ContentType.valueOf(jsonElement1.getAsString());
        switch (contentType) {
            case IMAGE:
                return BimberApplication.gson.fromJson(json, ImageContent.class);
            case INITIAL_MESSAGE:
                return BimberApplication.gson.fromJson(json, InitialMessageContent.class);
            case TEXT:
                return BimberApplication.gson.fromJson(json, TextContent.class);
            case REMOVED:
                return BimberApplication.gson.fromJson(json, RemovedContent.class);
            case PARTICIPATION_PERSON_ADDED:
                return BimberApplication.gson.fromJson(json, ParticipationPersonAddedContent.class);
            case PARTICIPATION_PERSON_REMOVED:
                return BimberApplication.gson.fromJson(json, ParticipationPersonRemovedContent.class);
            case PARTICIPATION_PERSON_LEFT:
                return BimberApplication.gson.fromJson(json, ParticipationPersonLeftContent.class);
            case PARTICIPATION_PERSON_DELETED_ACCOUNT:
                return BimberApplication.gson.fromJson(json, ParticipationPersonDeletedAccountContent.class);
            case GROUP_TIME_SET:
                return BimberApplication.gson.fromJson(json, GroupTimeSetContent.class);
            case GROUP_LOCATION_SET:
                return BimberApplication.gson.fromJson(json, GroupLocationSetContent.class);
            case GROUP_NAME_SET:
                return BimberApplication.gson.fromJson(json, GroupNameSetContent.class);
            case GROUP_DESCRIPTION_SET:
                return BimberApplication.gson.fromJson(json, GroupDescriptionSetContent.class);
            case GROUP_IMAGE_SET:
                return BimberApplication.gson.fromJson(json, GroupImageSetContent.class);
            case UNKNOWN:
            default:
                return UnknownContent.create();
        }
    }
}
