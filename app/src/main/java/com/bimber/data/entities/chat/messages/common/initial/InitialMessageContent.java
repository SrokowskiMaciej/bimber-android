package com.bimber.data.entities.chat.messages.common.initial;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 26.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class InitialMessageContent implements MessageContent {


    @Override
    public Object toFirebaseValue() {
        return new AutoValue_InitialMessageContent.FirebaseValue(this);
    }

    public static InitialMessageContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_InitialMessageContent.FirebaseValue.class).toAutoValue();
    }

    public static InitialMessageContent create() {
        return new AutoValue_InitialMessageContent(ContentType.INITIAL_MESSAGE);
    }

    public static TypeAdapter<InitialMessageContent> typeAdapter(Gson gson) {
        return new AutoValue_InitialMessageContent.GsonTypeAdapter(gson);
    }
}
