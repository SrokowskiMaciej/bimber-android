package com.bimber.data.entities.chat.messages.network;

import android.support.annotation.Nullable;

import com.bimber.data.entities.chat.messages.network.AutoValue_MessageModel;
import com.bimber.data.entities.chat.messages.common.ContentDeserializer;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 22.01.17.
 */
@AutoValue
public abstract class MessageModel implements Serializable {

    private static final Timestamp.Adapter TIEMSTAMP_ADAPTER = new Timestamp.Adapter();

    public static Key<MessageModel> create(DataSnapshot dataSnapshot) {
        ContentType contentType = dataSnapshot
                .child(MessageFirebaseValue.MESSAGE_CONTENT_FIELD)
                .child(MessageContent.CONTENT_TYPE_FIELD)
                .getValue(ContentType.class);

        MessageContent messageContent = ContentDeserializer.deserialize(contentType, dataSnapshot.child(MessageFirebaseValue
                .MESSAGE_CONTENT_FIELD));
        String uId = dataSnapshot.child(MessageFirebaseValue.MESSAGE_UID_FIELD).getValue(String.class);
        String userName = dataSnapshot.child(MessageFirebaseValue.MESSAGE_USER_NAME_FIELD).getValue(String.class);
        Timestamp timestamp = Timestamp.create(dataSnapshot.child(MessageFirebaseValue.MESSAGE_TIMESTAMP_FIELD).getValue(Long.class));
        return Key.of(dataSnapshot.getKey(), create(messageContent, timestamp, uId, userName));
    }

    public Object toFirebaseValue() {
        return new MessageFirebaseValue(content().toFirebaseValue(), TIEMSTAMP_ADAPTER.toFirebaseValue(timestamp()), userId(), userName());
    }

    public abstract MessageContent content();

    public abstract Timestamp timestamp();

    public abstract String userId();

    @Nullable
    public abstract String userName();

    public static MessageModel create(MessageContent content, Timestamp timestamp, String userId, String userName) {
        return new AutoValue_MessageModel(content, timestamp, userId, userName);
    }

}
