package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

/**
 * Created by maciek on 06.04.17.
 */
@AutoValue
public abstract class GroupParticipationPushMessage implements Parcelable {

    public abstract String groupId();

    public abstract String groupName();

    public abstract List<PhotoInfo> photos();

    public abstract String participantId();

    @Nullable
    public abstract String participantName();

    @Nullable
    public abstract String participantPhoto();

    public abstract String actingUserId();

    @Nullable
    public abstract String actingUserName();

    @Nullable
    public abstract String actingUserPhoto();

    public abstract GroupJoinStatus status();

    @Deprecated
    public abstract GroupJoinRequest groupJoinRequest();

    // The public static method returning a TypeAdapter<Foo> is what
    // tells auto-value-gson to create a TypeAdapter for Foo.
    public static TypeAdapter<GroupParticipationPushMessage> typeAdapter(Gson gson) {
        return new AutoValue_GroupParticipationPushMessage.GsonTypeAdapter(gson);
    }
}
