package com.bimber.data.entities.chat.messages.common;

import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;

/**
 * Created by maciek on 25.04.17.
 */
public enum ContentType {
    IMAGE,
    //VIDEO,
    //RECORDING,
    //MAP,
    //STICKER,
    //FILE
    //TYPING, ???
    INITIAL_MESSAGE,
    TEXT,
    REMOVED,
    PARTICIPATION_PERSON_ADDED,
    PARTICIPATION_PERSON_REMOVED,
    PARTICIPATION_PERSON_LEFT,
    PARTICIPATION_PERSON_DELETED_ACCOUNT,
    GROUP_TIME_SET,
    GROUP_LOCATION_SET,
    GROUP_NAME_SET,
    GROUP_DESCRIPTION_SET,
    GROUP_IMAGE_SET,
    UNKNOWN;


    public static class ContentTypeAdapter implements TypeAdapter<ContentType, String> {
        @Override
        public ContentType fromFirebaseValue(String value) {
            try {
                return valueOf(value);
            } catch (IllegalArgumentException exception) {
                return UNKNOWN;
            }
        }

        @Override
        public String toFirebaseValue(ContentType value) {
            return value.toString();
        }
    }
}
