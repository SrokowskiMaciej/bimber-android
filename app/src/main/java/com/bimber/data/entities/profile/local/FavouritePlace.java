package com.bimber.data.entities.profile.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.network.FavouritePlaceModel;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.maps.PlacePickerUtils;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Entity(tableName = "users_favourite_places",
        primaryKeys = {"favouritePlaceUserId", "favouritePlaceIndex"},
        indices = @Index(value = "favouritePlaceUserId"))
public class FavouritePlace implements Serializable {

    private static final String GOOGLE_MAPS_SEARCH_URI = "https://www.google.com/maps/search/?api=1&query=%s&query_place_id=%s";
    private static final String GOOGLE_MAPS_DIRECTIONS_URI = "https://www.google.com/maps/dir/?api=1&destination=%s&destination_place_id=%s";


    @NonNull
    public final String favouritePlaceId;

    @ForeignKey(entity = User.class,
            parentColumns = "uId",
            childColumns = "favouritePlaceId")
    @NonNull
    public final String favouritePlaceUserId;

    public final int favouritePlaceIndex;

    @NonNull
    public final String name;

    @NonNull
    public final String address;

    @NonNull
    public final String googleMapsId;

    public final double latitude;

    public final double longitude;

    public FavouritePlace(@NonNull String favouritePlaceId, @NonNull String favouritePlaceUserId,
                          int favouritePlaceIndex, @NonNull String name, @NonNull String address,
                          @NonNull String googleMapsId, double latitude, double longitude) {
        this.favouritePlaceId = favouritePlaceId;
        this.favouritePlaceUserId = favouritePlaceUserId;
        this.favouritePlaceIndex = favouritePlaceIndex;
        this.name = name;
        this.address = address;
        this.googleMapsId = googleMapsId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public FavouritePlace(@NonNull String uId, @NonNull Key<FavouritePlaceModel> favouritePlaceModel) {
        this(favouritePlaceModel.key(), uId, (int) favouritePlaceModel.value().index(), favouritePlaceModel.value().name(),
                favouritePlaceModel.value().address(), favouritePlaceModel.value().id(), favouritePlaceModel.value().latitude(),
                favouritePlaceModel.value().longitude());
    }

    @Ignore
    public Uri getGoogleMapsSearchUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_SEARCH_URI, Uri.encode(latitude + "," + longitude), googleMapsId));
    }

    @Ignore
    public Uri getGoogleMapsDirectionsUri() {
        return Uri.parse(String.format(GOOGLE_MAPS_DIRECTIONS_URI, Uri.encode(latitude + "," + longitude), googleMapsId));
    }

    @Ignore
    public boolean isPlaceNameHumanReadable() {
        return !PlacePickerUtils.isPlaceNameLatLngEncoded(name);
    }

    @Ignore
    public boolean isPlaceAddressHumanReadable() {
        return !address.isEmpty();
    }

    @Ignore
    public String getNormalizedPlaceName() {
        if (isPlaceNameHumanReadable() || !isPlaceAddressHumanReadable()) {
            return name;
        } else {
            return address;
        }
    }
}
