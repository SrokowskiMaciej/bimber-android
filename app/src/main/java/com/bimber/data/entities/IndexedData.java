package com.bimber.data.entities;

import com.google.firebase.database.PropertyName;

/**
 * Created by srokowski.maciej@gmail.com on 21.12.16.
 */

public interface IndexedData {
    String INDEX_FIELD_NAME = "index";

    @PropertyName(INDEX_FIELD_NAME)
    long index();
}
