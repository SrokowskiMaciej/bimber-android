package com.bimber.data.entities.profile.network;

/**
 * Created by Maciek on 12/01/2018.
 */
public enum Gender {
    MALE,
    FEMALE
}
