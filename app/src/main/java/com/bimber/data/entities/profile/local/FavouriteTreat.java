package com.bimber.data.entities.profile.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import com.bimber.data.entities.profile.network.FavouriteTreatModel;
import com.bimber.utils.firebase.Key;

import java.io.Serializable;

/**
 * Created by srokowski.maciej@gmail.com on 15.12.16.
 */
@Entity(tableName = "users_favourite_treats",
        primaryKeys = {"favouriteTreatUserId", "favouriteTreatIndex"},
        indices = @Index(value = "favouriteTreatUserId"))
public class FavouriteTreat implements Serializable {


    @NonNull
    public final String favouriteTreatId;

    @ForeignKey(entity = User.class,
            parentColumns = "uId",
            childColumns = "favouriteTreatId")
    @NonNull
    public final String favouriteTreatUserId;

    public final int favouriteTreatIndex;

    public final String favouriteTreatName;

    public FavouriteTreat(@NonNull String favouriteTreatId, @NonNull String favouriteTreatUserId,
                          int favouriteTreatIndex, String favouriteTreatName) {
        this.favouriteTreatId = favouriteTreatId;
        this.favouriteTreatUserId = favouriteTreatUserId;
        this.favouriteTreatIndex = favouriteTreatIndex;
        this.favouriteTreatName = favouriteTreatName;
    }

    public FavouriteTreat(@NonNull String uId, @NonNull Key<FavouriteTreatModel> favouriteTreatModel) {
        this(favouriteTreatModel.key(), uId, (int) favouriteTreatModel.value().index(), favouriteTreatModel.value().name());
    }
}
