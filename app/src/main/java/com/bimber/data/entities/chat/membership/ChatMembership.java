package com.bimber.data.entities.chat.membership;

import android.support.annotation.Nullable;

import com.bimber.data.entities.Chattable;
import com.bimber.utils.firebase.Timestamp;
import com.google.firebase.database.PropertyName;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;

/**
 * Created by maciek on 05.04.17.
 */
public interface ChatMembership {

    String CHAT_TYPE_FIELD_NAME = "chatType";
    String MEMBERSHIP_STATUS_FIELD_NAME = "membershipStatus";

    String uId();

    String chatId();

    @FirebaseAdapter(Chattable.ChatTypeAdapter.class)
    @PropertyName(CHAT_TYPE_FIELD_NAME)
    Chattable.ChatType chatType();

    @FirebaseAdapter(MembershipStatusAdapter.class)
    @PropertyName(MEMBERSHIP_STATUS_FIELD_NAME)
    MembershipStatus membershipStatus();

    @Nullable
    @FirebaseAdapter(Timestamp.Adapter.class)
    Timestamp leaveTimestamp();

    public enum MembershipStatus {
        ACTIVE,
        EXPIRED
    }


    class MembershipStatusAdapter implements TypeAdapter<MembershipStatus, String> {
        @Override
        public MembershipStatus fromFirebaseValue(String value) {
            return MembershipStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(MembershipStatus value) {
            return value.toString();
        }
    }
}
