package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.data.entities.PersonEvaluation.PersonEvaluationStatus;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

/**
 * Created by srokowski.maciej@gmail.com on 15.01.17.
 */
@AutoValue
public abstract class MatchPushMessage implements Parcelable {

    public abstract String dialogId();

    public abstract String likingUserId();
    @Nullable
    public abstract String likingUserName();
    @Nullable
    public abstract String likingUserPhoto();

    public abstract String likedUserId();
    @Nullable
    public abstract String likedUserName();
    @Nullable
    public abstract String likedUserPhoto();

    public abstract PersonEvaluationStatus evaluationStatus();

    @Deprecated
    public abstract PersonEvaluation personEvaluation();

    // The public static method returning a TypeAdapter<Foo> is what
    // tells auto-value-gson to create a TypeAdapter for Foo.
    public static TypeAdapter<MatchPushMessage> typeAdapter(Gson gson) {
        return new AutoValue_MatchPushMessage.GsonTypeAdapter(gson);
    }

}
