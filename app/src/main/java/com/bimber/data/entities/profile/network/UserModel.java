package com.bimber.data.entities.profile.network;

import android.os.Parcelable;

import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by srokowski.maciej@gmail.com on 02.11.16.
 */

@AutoValue
@FirebaseValue
public abstract class UserModel implements Parcelable, Serializable {

    public static final String USERNAME_FIELD = "displayName";
    public static final String ABOUT_FIELD = "about";
    public static final String GENDER_FIELD = "gender";
    public static final String AGE_FIELD = "age";

    @PropertyName(USERNAME_FIELD)
    public abstract String displayName();

    public abstract String fullName();

    @PropertyName(ABOUT_FIELD)
    public abstract String about();

    @PropertyName(GENDER_FIELD)
    @FirebaseAdapter(GenderTypeAdapter.class)
    public abstract Gender gender();

    @PropertyName(AGE_FIELD)
    public abstract int age();

    public abstract int friends();

    public abstract int parties();

    public Object toFirebaseValue() {
        return new AutoValue_UserModel.FirebaseValue(this);
    }

    public static UserModel create(String displayName, String fullName, String about, Gender gender, int age) {
        return new AutoValue_UserModel(displayName, fullName, about, gender, age, 0, 0);
    }

    public static Key<UserModel> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_UserModel.FirebaseValue.class).toAutoValue());
    }

    public static class GenderTypeAdapter implements TypeAdapter<Gender, String> {

        @Override
        public Gender fromFirebaseValue(String value) {
            return Gender.valueOf(value);
        }

        @Override
        public String toFirebaseValue(Gender value) {
            return value.toString();
        }
    }
}