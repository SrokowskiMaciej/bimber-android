package com.bimber.data.entities

import android.content.Context
import com.bimber.R
import com.bimber.data.entities.profile.local.FavouritePlace
import com.bimber.data.entities.profile.local.FavouriteTreat
import com.bimber.data.entities.profile.local.User
import com.bimber.data.entities.profile.local.UserPhoto
import com.bimber.data.entities.profile.local.aggregated.ProfileDataFull
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail
import com.bimber.data.entities.profile.network.Gender
import com.bimber.data.entities.profile.network.UploadStatus
import com.bimber.data.entities.profile.network.UserPhotoModel
import com.bimber.utils.firebase.Key
import com.bimber.utils.images.ResourceUtils


/**
 * Created by maciek on 21.03.17.
 */

class DefaultValues(context: Context) {

    val dummyPhoto: Key<UserPhotoModel>
    val deletedUser: User
    val defaultUser: User
    val defaultUserPhoto: UserPhoto
    val defaultPartyName: String
    val defaultPartyPhoto: String

    init {
        //FIXME Well, saving this to the database seems like an extra poor idea
        val defaultPhotoLocalUri = ResourceUtils.resourceUri(context, R.drawable.ic_default_profile).toString()
        dummyPhoto = Key.of(UNDEFINED_KEY, UserPhotoModel.create(0, defaultPhotoLocalUri,
                UploadStatus.FINISHED))
        defaultPartyPhoto = ResourceUtils.resourceUri(context, R.drawable.ic_default_group).toString()
        val deletedAccountName = context.getString(R.string.deleted_user_name)
        deletedUser = User(UNDEFINED_KEY, deletedAccountName, deletedAccountName, "", Gender.MALE, 25, 0, 0)
        val defaultAccountName = context.getString(R.string.unknown_user_name)
        defaultUser = User(UNDEFINED_KEY, defaultAccountName, defaultAccountName, "", Gender.MALE, 25, 0, 0)
        defaultUserPhoto = UserPhoto(UNDEFINED_KEY, UNDEFINED_KEY, 0, defaultPhotoLocalUri, "", UploadStatus.FINISHED)
        defaultPartyName = context.getString(R.string.unknown_group_name)
    }

    fun deletedUser(uId: String): User {
        return User(uId, deletedUser.displayName, deletedUser.fullName, deletedUser.about, deletedUser.gender, deletedUser.age,
                deletedUser.friends, deletedUser.parties);
    }

    fun defaultUser(uId: String): User {
        return User(uId, defaultUser.displayName, defaultUser.fullName, defaultUser.about, defaultUser.gender, defaultUser.age,
                defaultUser.friends, defaultUser.parties);
    }

    fun defaultUserPhoto(uId: String): UserPhoto {
        return UserPhoto(uId, defaultUserPhoto.photoId, defaultUserPhoto.userPhotoIndex, defaultUserPhoto.userPhotoUri,
                defaultUserPhoto.userPhotoThumb, defaultUserPhoto.userPhotoUploadStatus);
    }

    fun defaultPartyPhoto(): String {
        return defaultPartyPhoto
    }

    fun defaultProfileDataThumbnail(uId: String): ProfileDataThumbnail {
        return ProfileDataThumbnail(defaultUser(uId), defaultUserPhoto(uId))
    }

    fun deletedProfileDataThumbnail(): ProfileDataThumbnail {
        return ProfileDataThumbnail(deletedUser, defaultUserPhoto)
    }

    fun defaultProfileDataFull(uId: String): ProfileDataFull {
        return ProfileDataFull(defaultUser(uId), listOf(defaultUserPhoto(uId)), emptyList<FavouriteTreat>(), emptyList<FavouritePlace>())
    }

    fun deletedProfileDataFull(): ProfileDataFull {
        return ProfileDataFull(deletedUser, listOf(defaultUserPhoto), emptyList<FavouriteTreat>(), emptyList<FavouritePlace>())
    }

    companion object {
        //As is set on the server side
        val UNKNOWN_USER_NAME = "Unknown user"
        val UNDEFINED_KEY = "undefined"
    }
}

fun UserPhoto.isDefault(): Boolean {
    return this.photoId.equals(DefaultValues.UNDEFINED_KEY)
}
