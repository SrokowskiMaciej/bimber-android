package com.bimber.data.entities;

import com.google.firebase.database.PropertyName;

import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;

/**
 * Created by maciek on 21.02.17.
 */

public interface Chattable {

    String CHAT_TYPE_FIELD_NAME = "chatType";

    @FirebaseAdapter(ChatTypeAdapter.class)
    @PropertyName(CHAT_TYPE_FIELD_NAME)
    ChatType chatType();

    enum ChatType {
        DIALOG,
        GROUP
    }

    class ChatTypeAdapter implements TypeAdapter<ChatType, String> {
        @Override
        public ChatType fromFirebaseValue(String value) {
            return ChatType.valueOf(value);
        }

        @Override
        public String toFirebaseValue(ChatType value) {
            return value.toString();
        }
    }
}
