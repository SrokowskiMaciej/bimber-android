package com.bimber.data.entities.chat.messages.common.removed;

import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.MessageContent;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by maciek on 26.04.17.
 */
@AutoValue
@FirebaseValue
public abstract class RemovedContent implements MessageContent {

    public static RemovedContent create() {
        return new AutoValue_RemovedContent(ContentType.REMOVED);
    }

    public static RemovedContent create(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(AutoValue_RemovedContent.FirebaseValue.class).toAutoValue();
    }

    @Override
    public Object toFirebaseValue() {
        return new AutoValue_RemovedContent.FirebaseValue(this);
    }

    public static TypeAdapter<RemovedContent> typeAdapter(Gson gson) {
        return new AutoValue_RemovedContent.GsonTypeAdapter(gson);
    }
}
