package com.bimber.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

/**
 * Created by srokowski.maciej@gmail.com on 15.01.17.
 */
@AutoValue
public abstract class NewChatMessagePushMessage implements Parcelable {

    public abstract String chatId();

    @Nullable
    public abstract String chatName();

    public abstract ChatType chatType();

    public abstract List<PhotoInfo> photos();

    public abstract List<MessageInfo> messages();

    @Deprecated
    public abstract String messageId();

    @Deprecated
    public abstract ContentType contentType();

    @Deprecated
    public abstract String sendingUserId();

    public static NewChatMessagePushMessage create(String chatId, String chatName, ChatType chatType, List<PhotoInfo> photos,
                                                   List<MessageInfo> messages, String messageId, ContentType contentType, String sendingUserId) {
        return new AutoValue_NewChatMessagePushMessage(chatId, chatName, chatType, photos, messages, messageId, contentType, sendingUserId);
    }

    // The public static method returning a TypeAdapter<Foo> is what
    // tells auto-value-gson to create a TypeAdapter for Foo.
    public static TypeAdapter<NewChatMessagePushMessage> typeAdapter(Gson gson) {
        return new AutoValue_NewChatMessagePushMessage.GsonTypeAdapter(gson);
    }

    @AutoValue
    public abstract static class MessageInfo implements Parcelable {

        public abstract String messageId();

        @Nullable
        public abstract String userName();

        public abstract long messageTimestamp();

        public static MessageInfo create(String messageId, String userName, long messageTimestamp) {
            return new AutoValue_NewChatMessagePushMessage_MessageInfo(messageId, userName, messageTimestamp);
        }

        public static TypeAdapter<MessageInfo> typeAdapter(Gson gson) {
            return new AutoValue_NewChatMessagePushMessage_MessageInfo.GsonTypeAdapter(gson);
        }
    }

}
