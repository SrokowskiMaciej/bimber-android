package com.bimber.data.repositories;

import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import durdinapps.rxfirebase2.RxFirebaseAuth;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by srokowski.maciej@gmail.com on 28.11.16.
 */

public class FirebaseAuthService {

    private FirebaseAuth firebaseAuth;

    public FirebaseAuthService(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    public void logOut() {
        firebaseAuth.signOut();
    }

    public Single<FirebaseUser> currentUser() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser == null) {
            return Single.error(new IllegalAccessException("No user signed in"));
        } else {
            return Single.just(currentUser);
        }
    }

    public Observable<FirebaseUser> onUserLoggedIn() {
        return RxFirebaseAuth.observeAuthState(firebaseAuth)
                .filter(firebaseAuth -> firebaseAuth.getCurrentUser() != null)
                .map(FirebaseAuth::getCurrentUser);
    }

    public Observable<String> onUserLoggedInUid() {
        return onUserLoggedIn().map(FirebaseUser::getUid);
    }

    public Observable<RxFirebaseUtils.Event> onUserLoggedOut() {
        return RxFirebaseAuth.observeAuthState(firebaseAuth)
                .filter(firebaseAuth -> firebaseAuth.getCurrentUser() == null)
                .map(firebaseAuth -> RxFirebaseUtils.Event.INSTANCE);
    }
}
