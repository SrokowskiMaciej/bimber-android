package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by maciek on 29.03.17.
 */
@Singleton
public class GroupJoinRequestRepository {

    private final static String GROUP_JOIN_REQUESTS_NODE = "group_join_requests";

    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public GroupJoinRequestRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference groupJoinRequestsNode() {
        return firebaseDatabase.getReference(GROUP_JOIN_REQUESTS_NODE);
    }


    public DatabaseReference chatGroupJoinRequestsNode(String chatId) {
        return groupJoinRequestsNode().child(chatId);
    }

    public Flowable<List<Key<GroupJoinRequest>>> onGroupJoinRequestsEvents(String chatId) {
        return RxFirebaseDatabase.observeValueEvent(chatGroupJoinRequestsNode(chatId), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(GroupJoinRequest::create));
    }

    public Flowable<List<Key<GroupJoinRequest>>> onGroupJoinRequestsEvents(String chatId, GroupJoinStatus
            groupJoinStatus) {
        Query query = chatGroupJoinRequestsNode(chatId)
                .orderByChild(GroupJoinRequest.GROUP_JOIN_REQUEST_STATUS_FIELD)
                .equalTo(groupJoinStatus.toString());
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(GroupJoinRequest::create));
    }

    public Flowable<Key<GroupJoinRequest>> onGroupJoinRequestEvents(String chatId, String uId) {
        Query query = chatGroupJoinRequestsNode(chatId).child(uId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(GroupJoinRequest::create))
                .map(keys -> {
                    if (keys.isEmpty()) {
                        return Key.of(uId, GroupJoinRequest.create(GroupJoinStatus.NONE, uId));
                    } else {
                        return keys.get(0);
                    }
                });
    }

    public Maybe<Key<GroupJoinRequest>> groupJoinRequest(String chatId, String uid) {
        DatabaseReference query = chatGroupJoinRequestsNode(chatId).child(uid);
        return RxFirebaseDatabase.observeSingleValueEvent(query, GroupJoinRequest::create, RxFirebaseUtils
                .EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Completable setGroupJoinRequest(String currentUserId, String chatId, String uId,
                                           GroupJoinRequest.GroupJoinStatus groupJoinStatus) {
        GroupJoinRequest groupJoinRequest = GroupJoinRequest.create(groupJoinStatus, currentUserId);
        return RxFirebaseDatabase.setValue(chatGroupJoinRequestsNode(chatId).child(uId), groupJoinRequest.toFirebaseValue());
    }

    public Completable setGroupJoinRequests(String currentUserId, String chatId, List<Key<GroupJoinRequest.GroupJoinStatus>>
            groupJoinRequestStatuses) {
        HashMap<String, Object> mappedGroupJoinRequests = new HashMap<>(groupJoinRequestStatuses.size());
        for (Key<GroupJoinRequest.GroupJoinStatus> groupJoinRequestStatus : groupJoinRequestStatuses) {
            GroupJoinRequest groupJoinRequest = GroupJoinRequest.create(groupJoinRequestStatus.value(), currentUserId);
            mappedGroupJoinRequests.put(groupJoinRequestStatus.key(), groupJoinRequest.toFirebaseValue());
        }
        return RxFirebaseDatabase.updateChildren(chatGroupJoinRequestsNode(chatId), mappedGroupJoinRequests);
    }

    public void setKeepSynced(String groupId, String uId) {
        chatGroupJoinRequestsNode(groupId).child(uId).keepSynced(true);
    }
}
