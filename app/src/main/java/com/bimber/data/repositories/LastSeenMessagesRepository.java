package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by maciek on 09.04.17.
 */
@Singleton
public class LastSeenMessagesRepository {


    private final static String LAST_SEEN_MESSAGES_NODE = "last_seen_messages";

    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public LastSeenMessagesRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference lastSeenMessagesNode() {
        return firebaseDatabase.getReference(LAST_SEEN_MESSAGES_NODE);
    }


    public DatabaseReference chatLastSeenMessagesNode(String chatId) {
        return lastSeenMessagesNode().child(chatId);
    }

    public Flowable<List<Key<String>>> onChatLastSeenMessagesEvents(String chatId) {
        return RxFirebaseDatabase.observeValueEvent(chatLastSeenMessagesNode(chatId), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(dataSnapshot -> Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(String.class))));
    }

    public Flowable<List<Key<String>>> onChatUserLastSeenMessageEvents(String chatId, String userId) {
        DatabaseReference query = chatLastSeenMessagesNode(chatId).child(userId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(dataSnapshot -> Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(String.class))));
    }

    public Maybe<Key<String>> chatUserLastSeenMessage(String chatId, String userId) {
        DatabaseReference query = chatLastSeenMessagesNode(chatId).child(userId);
        return RxFirebaseDatabase.observeSingleValueEvent(query, dataSnapshot -> Key.of(dataSnapshot.getKey(),
                dataSnapshot.getValue(String.class)), RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Completable setChatUserLastSeenMessage(String chatId, String uId, String messageId) {
        return RxFirebaseDatabase.setValue(chatLastSeenMessagesNode(chatId).child(uId), messageId);
    }
}
