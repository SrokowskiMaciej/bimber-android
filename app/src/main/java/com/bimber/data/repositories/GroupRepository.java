package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Group;
import com.bimber.data.entities.Place;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by maciek on 21.02.17.
 */
@Singleton
public class GroupRepository {

    public static final String GROUPS_NODE = "groups";
    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public GroupRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference groupsNode() {
        return firebaseDatabase.getReference(GROUPS_NODE);
    }

    public Flowable<List<Key<Group>>> onGroupEvents(String groupId) {
        return RxFirebaseDatabase.observeValueEvent(groupsNode().child(groupId), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(Group::create));
    }

    public Flowable<List<Key<Group>>> onAdministratedGroupsEvents(String adminId) {
        Query query = groupsNode().orderByChild(Group.GROUP_OWNER_ID_PROPERTY_NAME).equalTo(adminId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(Group::create));
    }

    public Maybe<Key<Group>> group(String groupId) {
        return RxFirebaseDatabase.observeSingleValueEvent(groupsNode().child(groupId), Group::create,
                RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Single<Key<Group>> insertGroup(Group group) {
        DatabaseReference push = groupsNode().push();
        return RxFirebaseDatabase.setValue(push, group.toFirebaseValue()).toSingleDefault(Key.of(push.getKey(), group));
    }

    //DETAILS
    public Maybe<String> groupOwnerId(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_OWNER_ID_PROPERTY_NAME);
        return RxFirebaseDatabase.observeSingleValueEvent(query, String.class);
    }


    public Maybe<String> groupName(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_NAME_PROPERTY_NAME);
        return RxFirebaseDatabase.observeSingleValueEvent(query, String.class);
    }

    public Completable setGroupName(String currentUserId, String groupId, String newGroupName) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_NAME_PROPERTY_NAME, newGroupName);
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }

    public Maybe<String> groupDescription(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_DESCRIPTION_PROPERTY_NAME);
        return RxFirebaseDatabase.observeSingleValueEvent(query, String.class);
    }

    public Completable setGroupDescription(String currentUserId, String groupId, String newGroupDescription) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_DESCRIPTION_PROPERTY_NAME, newGroupDescription);
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }

    public Maybe<Place> groupLocation(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_LOCATION_PROPERTY_NAME);
        return RxFirebaseDatabase.observeSingleValueEvent(query, Place::create, RxFirebaseUtils.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Completable setGroupLocation(String currentUserId, String groupId, Place newGroupLocation) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_LOCATION_PROPERTY_NAME, newGroupLocation.toFirebaseValue());
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }

    public Flowable<Boolean> isGroupSecure(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_SECURE_PROPERTY_NAME);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                dataSnapshot -> {
                    //Legacy support
                    if (dataSnapshot.exists()) {
                        return dataSnapshot.getValue(Boolean.class);
                    } else {
                        return false;
                    }
                });
    }

    public Completable setGroupSecurity(String currentUserId, String groupId, boolean secure) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_SECURE_PROPERTY_NAME, secure);
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }


    public Maybe<Timestamp> groupMeetingTime(String groupId) {
        DatabaseReference query = groupsNode().child(groupId).child(Group.GROUP_MEETING_TIME_PROPERTY_NAME);
        return RxFirebaseDatabase.observeSingleValueEvent(query, dataSnapshot -> Timestamp.create(dataSnapshot.getValue(Long.class)),
                RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Completable setGroupMeetingTime(String currentUserId, String groupId, Timestamp newGroupMeetingTime) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_MEETING_TIME_PROPERTY_NAME, newGroupMeetingTime.value());
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }

    public Completable setGroupPicture(String currentUserId, String groupId, String newGroupPictureUri) {
        HashMap<String, Object> update = new HashMap<>();
        update.put(Group.GROUP_LAST_EDITOR_ID_PROPERTY_NAME, currentUserId);
        update.put(Group.GROUP_IMAGE_PROPERTY_NAME, newGroupPictureUri);
        return RxFirebaseDatabase.updateChildren(groupsNode().child(groupId), update);
    }

}
