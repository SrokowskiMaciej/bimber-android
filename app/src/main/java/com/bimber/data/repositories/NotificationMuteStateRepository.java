package com.bimber.data.repositories;

import com.bimber.data.GsonTypeAdapterFactory;
import com.bimber.data.entities.NotificationMuteState;
import com.bimber.data.entities.NotificationMuteState.State;
import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by maciek on 23.08.17.
 */
@Singleton
public class NotificationMuteStateRepository {

    private static final NotificationMuteState DEFAULT_NOTIFICATION_MUTE_STATE = NotificationMuteState.create(State.NORMAL, 0);
    private static final int ONE_HOUR = 60 * 60 * 1000;

    private final RxSharedPreferences rxSharedPreferences;
    private static final String EMPTY_PREFERENCE = "EMPTY_PREFERENCE";
    private static final String MUTE_PREFERENCE_KEY = "mute_";


    @Inject
    public NotificationMuteStateRepository(RxSharedPreferences rxSharedPreferences) {
        this.rxSharedPreferences = rxSharedPreferences;
    }

    public void storeState(String chatId, NotificationMuteState notificationMuteState) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                .create();
        getPreference(chatId).set(gson.toJson(notificationMuteState));
    }

    public Observable<NotificationMuteState> onState(String chatId) {
        return getPreference(chatId).asObservable()
                .map(serializedState -> {
                    if (EMPTY_PREFERENCE.equals(serializedState)) {
                        return NotificationMuteState.create(State.NORMAL, System.currentTimeMillis());
                    } else {
                        Gson gson = new GsonBuilder()
                                .registerTypeAdapterFactory(GsonTypeAdapterFactory.create())
                                .create();
                        NotificationMuteState notificationMuteState = gson.fromJson(serializedState, NotificationMuteState.class);
                        if (notificationMuteState.state() == State.MUTE_1_HOUR &&
                                System.currentTimeMillis() > notificationMuteState.mutedTimestamp() + ONE_HOUR) {
                            getPreference(chatId).delete();
                            return DEFAULT_NOTIFICATION_MUTE_STATE;
                        } else {
                            return notificationMuteState;
                        }
                    }
                });
    }

    private Preference<String> getPreference(String chatId) {
        return rxSharedPreferences.getString(MUTE_PREFERENCE_KEY + chatId, EMPTY_PREFERENCE);
    }

}
