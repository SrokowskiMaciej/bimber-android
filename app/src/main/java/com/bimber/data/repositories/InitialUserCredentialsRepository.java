package com.bimber.data.repositories;

import com.bimber.data.entities.InitialUserCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by maciek on 30.10.17.
 */
@Singleton
public class InitialUserCredentialsRepository {

    public static final String INITIAL_USER_CREDENTIALS_NODE = "initial_user_credentials";

    private final FirebaseDatabase firebaseDatabase;

    @Inject
    public InitialUserCredentialsRepository(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference initialUserCredentialsNode(String uId) {
        return firebaseDatabase.getReference(INITIAL_USER_CREDENTIALS_NODE).child(uId);
    }

    public void setInitialUserCredentials(String uId, InitialUserCredentials initialUserCredentials) {
        initialUserCredentialsNode(uId).setValue(initialUserCredentials.toFirebaseValue());
    }
}
