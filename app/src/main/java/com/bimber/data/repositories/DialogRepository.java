package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Dialog;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by srokowski.maciej@gmail.com on 14.01.17.
 */
@Singleton
public class DialogRepository {

    public static final String DIALOGS_NODE = "dialogs";
    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public DialogRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference dialogsNode() {
        return firebaseDatabase.getReference(DIALOGS_NODE);
    }

    public Flowable<List<Key<Dialog>>> onDialogEvents(String dialogId) {
        return RxFirebaseDatabase.observeValueEvent(dialogsNode().child(dialogId), backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(Dialog::create));
    }

    public Maybe<Key<Dialog>> dialog(String dialogId) {
        return RxFirebaseDatabase.observeSingleValueEvent(dialogsNode().child(dialogId), Dialog::create, RxFirebaseUtils
                .EXISTING_DATA_SNAPSHOT_PREDICATE);
    }
}
