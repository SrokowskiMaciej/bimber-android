package com.bimber.data.repositories;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by maciek on 28.03.17.
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface Subdirectory {
    String value();
}
