package com.bimber.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by maciek on 23.06.17.
 */
@Singleton
public class DbConnectionService {

    private final FirebaseDatabase firebaseDatabase;

    @Inject
    public DbConnectionService(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference connectionReference() {
        return firebaseDatabase.getReference(".info/connected");
    }

    public Single<Boolean> connected() {
        return RxFirebaseDatabase.observeSingleValueEvent(connectionReference(), Boolean.class)
                .toSingle();
    }

    public Completable connectedThrow() {
        return RxFirebaseDatabase.observeSingleValueEvent(connectionReference(), Boolean.class)
                .filter(Boolean::booleanValue)
                .toSingle()
                .toCompletable();
    }


    public Flowable<Boolean> onConnected() {
        return RxFirebaseDatabase.observeValueEvent(connectionReference(), Boolean.class);
    }
}
