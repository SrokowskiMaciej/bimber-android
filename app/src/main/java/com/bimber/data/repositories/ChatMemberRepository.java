package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by maciek on 22.02.17.
 */
@Singleton
public class ChatMemberRepository {

    public static final String MEMBERSHIPS_CHAT_AGGREGATED = "memberships_chat_aggregated";

    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public ChatMemberRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference membersNode() {
        return firebaseDatabase.getReference(MEMBERSHIPS_CHAT_AGGREGATED);
    }

    public DatabaseReference chatMembersNode(String chatId) {
        return membersNode().child(chatId);
    }

    public Flowable<List<ChatVisibleChatMembership>> onChatMembershipsEvents(String chatId) {
        return RxFirebaseDatabase.observeValueEvent(chatMembersNode(chatId),
                RxFirebaseUtils.childListMapper(ChatVisibleChatMembership::create),
                BackpressureStrategy.BUFFER);
    }

    public Flowable<List<ChatVisibleChatMembership>> onChatMembershipsEvents(String chatId, MembershipStatus membershipStatus) {
        Query query = chatMembersNode(chatId)
                .orderByChild(ChatVisibleChatMembership.MEMBERSHIP_STATUS_FIELD_NAME)
                .equalTo(membershipStatus.toString());
        return RxFirebaseDatabase.observeValueEvent(query,
                RxFirebaseUtils.childListMapper(ChatVisibleChatMembership::create),
                BackpressureStrategy.BUFFER);
    }

    public Flowable<List<ChatVisibleChatMembership>> onChatMembershipEvents(String chatId, String chatMemberId) {
        Query query = chatMembersNode(chatId).child(chatMemberId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(ChatVisibleChatMembership::create));
    }

    public Single<List<ChatVisibleChatMembership>> chatMemberships(String chatId) {
        return RxFirebaseDatabase.observeSingleValueEvent(chatMembersNode(chatId), RxFirebaseUtils.childListMapper
                (ChatVisibleChatMembership::create));
    }

    public Single<List<ChatVisibleChatMembership>> chatMemberships(String chatId, MembershipStatus membershipStatus) {
        Query query = chatMembersNode(chatId)
                .orderByChild(ChatVisibleChatMembership.MEMBERSHIP_STATUS_FIELD_NAME)
                .equalTo(membershipStatus.toString());
        return RxFirebaseDatabase.observeSingleValueEvent(query, RxFirebaseUtils.childListMapper(ChatVisibleChatMembership::create));
    }

    public Maybe<ChatVisibleChatMembership> chatMembership(String chatId, String chatMemberId) {
        Query query = chatMembersNode(chatId).child(chatMemberId);
        return RxFirebaseDatabase.observeSingleValueEvent(query, ChatVisibleChatMembership::create, RxFirebaseUtils
                .EXISTING_DATA_SNAPSHOT_PREDICATE);
    }
}
