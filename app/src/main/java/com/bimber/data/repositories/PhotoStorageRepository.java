package com.bimber.data.repositories;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.bimber.data.entities.UploadTaskStatus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import hu.akarnokd.rxjava.interop.RxJavaInterop;
import id.zelory.compressor.Compressor;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import timber.log.Timber;


/**
 * Created by srokowski.maciej@gmail.com on 17.12.16.
 */

public class PhotoStorageRepository {

    public static final String PHOTOS_NODE = "photos";
    private final FirebaseStorage firebaseStorage;
    private final FirebaseDatabase firebaseDatabase;
    private final Compressor compressor;
    private final String subdirectory;

    public PhotoStorageRepository(FirebaseStorage firebaseStorage, FirebaseDatabase firebaseDatabase, Compressor compressor, String
            subdirectory) {
        this.firebaseStorage = firebaseStorage;
        this.firebaseDatabase = firebaseDatabase;
        this.compressor = compressor;
        this.subdirectory = subdirectory;
    }


    private StorageReference photosNode() {
        return firebaseStorage.getReference(PHOTOS_NODE).child(subdirectory);
    }

    public StorageReference aggregatedPhotosNode(String aggregationId) {
        return photosNode().child(aggregationId);
    }

    public Flowable<UploadTaskStatus> insertPhoto(String aggregationId, File file) {
        StorageReference reference = aggregatedPhotosNode(aggregationId).child(firebaseDatabase.getReference().push().getKey());
        //TODO Wait for maven of Compressor lib with RxJava 2 support
        //TODO Make RxFirebase lib properly handle onProgress listeners of firebase
        return RxJavaInterop.toV2Single(compressor.compressToFileAsObservable(file).toSingle())
                .subscribeOn(Schedulers.computation())
                .map(Uri::fromFile)
                .observeOn(Schedulers.io())
                .flatMapPublisher(uri -> uploadTaskObservable(reference.putFile(uri)))
                .publish()
                .autoConnect();
    }

    public Flowable<UploadTaskStatus> insertPhoto(String aggregationId, String photoId, File file) {
        StorageReference reference = aggregatedPhotosNode(aggregationId).child(photoId);
        //TODO Wait for maven of Compressor lib with RxJava 2 support
        //TODO Make RxFirebase lib properly handle onProgress listeners of firebase
        return RxJavaInterop.toV2Single(compressor.compressToFileAsObservable(file).toSingle())
                .subscribeOn(Schedulers.computation())
                .map(Uri::fromFile)
                .observeOn(Schedulers.io())
                .flatMapPublisher(uri -> uploadTaskObservable(reference.putFile(uri)))
                .publish()
                .autoConnect();
    }

    public Completable deletePhoto(String aggregationId, String photoId) {
        return RxFirebaseStorage.delete(aggregatedPhotosNode(aggregationId).child(photoId));
    }

    private Flowable<UploadTaskStatus> uploadTaskObservable(UploadTask task) {
        ReplaySubject<UploadTaskStatus> replaySubject = ReplaySubject.create();
        task.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Timber.i("Upload photos progress");
                if (taskSnapshot.getUploadSessionUri() != null) {
                    replaySubject.onNext(UploadTaskStatus.create(UploadTaskStatus.TaskStatus.PENDING, taskSnapshot.getUploadSessionUri()));
                    task.removeOnProgressListener(this);
                }
            }
        });
        task.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    replaySubject.onNext(UploadTaskStatus.create(UploadTaskStatus.TaskStatus.FINISHED, task.getResult().getDownloadUrl()));
                    replaySubject.onComplete();
                } else {
                    replaySubject.onError(task.getException());
                }
            }
        });
        return replaySubject.toFlowable(BackpressureStrategy.BUFFER);
    }

}
