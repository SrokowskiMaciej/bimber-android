package com.bimber.data.repositories;

import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by srokowski.maciej@gmail.com on 16.01.17.
 */

public class MessagingTokenRepository {

    public static final String MESSAGING_TOKENS_NODE = "messaging_tokens";
    private final FirebaseDatabase firebaseDatabase;

    public MessagingTokenRepository(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference messagingTokensNode() {
        return firebaseDatabase.getReference(MESSAGING_TOKENS_NODE);
    }

    public Completable setToken(String uid, String token) {
        return Single.just(messagingTokensNode())
                .map(databaseReference -> databaseReference.child(uid).child(token))
                .flatMapCompletable(databaseReference -> RxFirebaseDatabase.setValue(databaseReference, false));
    }

    public void setTokenBlocking(String uid, String token) {
        messagingTokensNode().child(uid).child(token).setValue(false);
    }

    private Maybe<List<Key<Boolean>>> tokens(String uid) {
        return RxFirebaseDatabase.observeSingleValueEvent(messagingTokensNode().child(uid),
                RxFirebaseUtils.childListMapper(dataSnapshot -> Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(Boolean.class))),
                RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }
}
