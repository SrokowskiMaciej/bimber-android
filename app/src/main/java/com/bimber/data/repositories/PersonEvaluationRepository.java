package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.PersonEvaluation;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by srokowski.maciej@gmail.com on 02.01.17.
 */
@Singleton
public class PersonEvaluationRepository {

    public static final String EVALUATED_PERSONS_NODE = "evaluated_persons";
    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public PersonEvaluationRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference evaluatedPersonsNode() {
        return firebaseDatabase.getReference(EVALUATED_PERSONS_NODE);
    }

    public DatabaseReference evaluatedPersonsNodeForUid(String currentUserId) {
        return evaluatedPersonsNode().child(currentUserId);
    }

    public Flowable<Key<PersonEvaluation>> onEvaluatedPerson(String currentUserId, String evaluatedPersonUid) {
        return RxFirebaseDatabase.observeValueEvent(evaluatedPersonsNodeForUid(currentUserId).child(evaluatedPersonUid),
                backgroundSchedulerProvider.backgroundScheduler(), RxFirebaseUtils.singletonListMapper(PersonEvaluation::create))
                .map(keys -> {
                    if (keys.isEmpty()) {
                        return Key.of(evaluatedPersonUid, PersonEvaluation.create(PersonEvaluation.PersonEvaluationStatus.NONE));
                    } else {
                        return keys.get(0);
                    }
                });
    }

    public Maybe<Key<PersonEvaluation>> evaluatedPerson(String currentUserId, String evaluatedPersonUid) {
        return RxFirebaseDatabase.observeSingleValueEvent(evaluatedPersonsNodeForUid(currentUserId).child(evaluatedPersonUid),
                PersonEvaluation::create, RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }

    public Completable setEvaluatedPerson(String currentUserId, String evaluatedPersonUid,
                                          PersonEvaluation.PersonEvaluationStatus evaluationStatus) {
        DatabaseReference reference = evaluatedPersonsNodeForUid(currentUserId)
                .child(evaluatedPersonUid)
                .child(PersonEvaluation.STATUS_FIELD);
        return RxFirebaseDatabase.setValue(reference, evaluationStatus.toString());
    }

    public void setKeepSynced(String currentUserId, String evaluatedPersonUid) {
        evaluatedPersonsNodeForUid(currentUserId).child(evaluatedPersonUid).keepSynced(true);
    }

    public void unsync(String currentUserId, String evaluatedPersonUid) {
        evaluatedPersonsNodeForUid(currentUserId).child(evaluatedPersonUid).keepSynced(false);
    }
}
