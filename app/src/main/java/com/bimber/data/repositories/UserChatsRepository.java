package com.bimber.data.repositories;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.membership.ChatMembership;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.utils.rx.RxFirebaseUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by maciek on 21.02.17.
 */
@Singleton
public class UserChatsRepository {

    public static final String MEMBERSHIPS_USER_AGGREGATED = "memberships_user_aggregated";
    private final FirebaseDatabase firebaseDatabase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public UserChatsRepository(FirebaseDatabase firebaseDatabase, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.firebaseDatabase = firebaseDatabase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public DatabaseReference chatsNode() {
        return firebaseDatabase.getReference(MEMBERSHIPS_USER_AGGREGATED);
    }


    public DatabaseReference userChatsNode(String uid) {
        return chatsNode().child(uid);
    }

    public Flowable<List<UserVisibleChatMembership>> onUserChatsEvents(String uid, int limit) {
        Query query = userChatsNode(uid)
                .orderByChild(UserVisibleChatMembership.LAST_INTERACTION_FIELD_NAME)
                .limitToLast(limit);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(UserVisibleChatMembership::create));
    }

    public Flowable<List<UserVisibleChatMembership>> onUserChatsEvents(String uId, ChatType chatType) {
        Query query = userChatsNode(uId).orderByChild(ChatMembership.CHAT_TYPE_FIELD_NAME).equalTo(chatType.toString());
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(UserVisibleChatMembership::create));
    }


    public Flowable<List<UserVisibleChatMembership>> onUserChatsEvents(String uId, ChatType chatType, MembershipStatus membershipStatus) {
        Query query = userChatsNode(uId).orderByChild(ChatMembership.CHAT_TYPE_FIELD_NAME).equalTo(chatType.toString());
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.childListMapper(UserVisibleChatMembership::create))
                .flatMapSingle(chatMemberships -> Flowable.fromIterable(chatMemberships)
                        .filter(chatMembership -> chatMembership.membershipStatus().equals(membershipStatus))
                        .toList());
    }

    public Flowable<List<UserVisibleChatMembership>> onUserChatEvents(String uId, String chatId) {
        Query query = userChatsNode(uId).child(chatId);
        return RxFirebaseDatabase.observeValueEvent(query, backgroundSchedulerProvider.backgroundScheduler(),
                RxFirebaseUtils.singletonListMapper(UserVisibleChatMembership::create));
    }

    public Maybe<UserVisibleChatMembership> userChat(String uid, String chatId) {
        return RxFirebaseDatabase.observeSingleValueEvent(userChatsNode(uid).child(chatId), UserVisibleChatMembership::create,
                RxFirebaseDatabase.EXISTING_DATA_SNAPSHOT_PREDICATE);
    }
}
