package com.bimber.data;

import android.content.Context;

import com.bimber.R;

/**
 * Created by maciek on 14.05.17.
 */

public class ServerValues {
    private final Context context;

    public ServerValues(Context context) {
        this.context = context;
    }

    public String getFirebaseWebApiToken() {
        return context.getString(R.string.firebase_web_api_token);
    }

    public String getFirebaseDeeplinkDomain() {
        return context.getString(R.string.firebase_deeplink_domain);
    }

    public String getDeeplinkDomain() {
        return context.getString(R.string.deeplink_domain);
    }
}
