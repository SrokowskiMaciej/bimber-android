package com.bimber.domain.group;

import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.domain.chats.GetChatMembersDataUseCase;
import com.bimber.domain.model.GroupData;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

/**
 * Created by maciek on 29.03.17.
 */
@Singleton
public class GetGroupDataUseCase {

    private final GroupRepository groupRepository;
    private final GetChatMembersDataUseCase getChatMembersDataUseCase;

    @Inject
    public GetGroupDataUseCase(GroupRepository groupRepository, GetChatMembersDataUseCase getChatMembersDataUseCase) {
        this.groupRepository = groupRepository;
        this.getChatMembersDataUseCase = getChatMembersDataUseCase;
    }

    public Flowable<List<GroupData>> onGroupData(String chatId) {
        return Flowable.combineLatest(
                groupRepository.onGroupEvents(chatId),
                getChatMembersDataUseCase.onChatMembersThumbnails(chatId, MembershipStatus.ACTIVE),
                (groupList, chatMembersProfilesData) -> Observable.fromIterable(groupList)
                        .map(group -> GroupData.create(group, chatMembersProfilesData))
                        .toList())
                .flatMapSingle(groupData -> groupData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to restricted group data, for chat with id: %s. " +
                        "Consuming error and delivering empty list", chatId))
                .onErrorReturnItem(Collections.emptyList());
    }

    public Flowable<List<GroupData>> onGroupDataOptimized(String chatId) {
        return Flowable.combineLatest(
                groupRepository.onGroupEvents(chatId),
                getChatMembersDataUseCase.onChatMembers(chatId),
                (groupList, chatMembersProfilesData) -> Observable.fromIterable(groupList)
                        .map(group -> GroupData.create(group, chatMembersProfilesData))
                        .toList())
                .flatMapSingle(groupData -> groupData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to restricted group data, for chat with id: %s. " +
                        "Consuming error and delivering empty list", chatId))
                .onErrorReturnItem(Collections.emptyList());
    }

    public Flowable<List<GroupData>> onGroupDataOptimized(String chatId, int limitMembers) {
        return Flowable.combineLatest(
                groupRepository.onGroupEvents(chatId),
                getChatMembersDataUseCase.onChatMembers(chatId, limitMembers),
                (groupList, chatMembersProfilesData) -> Observable.fromIterable(groupList)
                        .map(group -> GroupData.create(group, chatMembersProfilesData))
                        .toList())
                .flatMapSingle(groupData -> groupData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to restricted group data, for chat with id: %s. " +
                        "Consuming error and delivering empty list", chatId))
                .onErrorReturnItem(Collections.emptyList());
    }

    public Single<GroupData> groupData(String chatId) {
        return Single.zip(
                groupRepository.group(chatId).toSingle(),
                getChatMembersDataUseCase.chatMembersThumbnails(chatId),
                GroupData::create)
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch group data, propagating error downstream"));
    }

    public Single<GroupData> groupData(String chatId, MembershipStatus membershipStatus) {
        return Single.zip(
                groupRepository.group(chatId).toSingle(),
                getChatMembersDataUseCase.chatMembersThumbnails(chatId, membershipStatus),
                GroupData::create)
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch group data, propagating error downstream"));
    }
}
