package com.bimber.domain.group;

import com.bimber.data.entities.UploadTaskStatus;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.repositories.PhotoStorageRepository;
import com.bimber.data.repositories.Subdirectory;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * Created by maciek on 15.08.17.
 */

public class SetGroupPictureUseCase {

    public static final String GROUPS_PHOTOS_STORAGE_SUBDIRECTORY = "groups";

    private final GroupRepository groupRepository;
    private final PhotoStorageRepository photoStorageRepository;

    @Inject
    public SetGroupPictureUseCase(GroupRepository groupRepository, @Subdirectory(GROUPS_PHOTOS_STORAGE_SUBDIRECTORY)
            PhotoStorageRepository photoStorageRepository) {
        this.groupRepository = groupRepository;
        this.photoStorageRepository = photoStorageRepository;
    }

    public Completable setGroupPicture(String currentUserId, String groupId, File file) {
        return photoStorageRepository.insertPhoto(groupId, file)
                .filter(uploadTaskStatus -> uploadTaskStatus.taskStatus() == UploadTaskStatus.TaskStatus.FINISHED)
                .singleOrError()
                .flatMapCompletable(uploadTaskStatus -> groupRepository.setGroupPicture(currentUserId, groupId, uploadTaskStatus.uri()
                        .toString()));
    }
}
