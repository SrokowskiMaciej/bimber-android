package com.bimber.domain.friends;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.membership.ChatMembership;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.chats.GetChatMembersDataUseCase;
import com.bimber.domain.chats.dialog.GetUserDialogChatsDataUseCase;
import com.bimber.domain.chats.group.GetUserGroupChatsDataUseCase;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.domain.model.Friend;
import com.bimber.utils.firebase.relation.Relation;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.Query;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

import static com.bimber.data.entities.Chattable.ChatType.DIALOG;

/**
 * Created by maciek on 15.03.17.
 */
@Singleton
public class GetFriendsDataUseCase {

    private final UserChatsRepository userChatsRepository;
    private final GetChatMembersDataUseCase getChatMembersDataUseCase;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public GetFriendsDataUseCase(UserChatsRepository userChatsRepository, GetChatMembersDataUseCase getChatMembersDataUseCase,
                                 BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.userChatsRepository = userChatsRepository;
        this.getChatMembersDataUseCase = getChatMembersDataUseCase;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }


    public Flowable<List<ChatMemberData>> onUserFriends(String uId) {
        Query query = userChatsRepository.userChatsNode(uId)
                .orderByChild(ChatMembership.CHAT_TYPE_FIELD_NAME)
                .equalTo(Chattable.ChatType.DIALOG.toString());

        return new Relation.Builder(query)
                .relation(dataSnapshot -> Flowable.defer(() -> onChatData(uId, UserVisibleChatMembership.create(dataSnapshot))))
                .withProgressiveInitialQuery(true)
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle(lists -> Flowable.fromIterable(lists)
                        .flatMap(Flowable::fromIterable)
                        .sorted((o1, o2) -> Long.compare(o1.chatMembership().lastInteraction(), o2.chatMembership().lastInteraction()))
                        .distinct(chatMemberProfileData -> chatMemberProfileData.profileData().user.uId)
                        .toList());
    }

    private Flowable<List<ChatMemberData>> onChatData(String currentUserId, UserVisibleChatMembership
            userVisibleChatMembership) {
        return getChatMembersDataUseCase.onChatMembersFiltered(userVisibleChatMembership.chatId(), currentUserId);
    }

    @AutoValue
    public abstract static class Friends {
        public abstract List<ChatMemberData> immediateFriends();

        public abstract List<ChatMemberData> groupFriends();

        public static Friends create(List<ChatMemberData> immediateFriends, List<ChatMemberData> groupFriends) {
            return new AutoValue_GetFriendsDataUseCase_Friends(immediateFriends, groupFriends);
        }


    }
}
