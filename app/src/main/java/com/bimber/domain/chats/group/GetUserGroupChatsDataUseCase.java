package com.bimber.domain.chats.group;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.model.GroupChatData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import timber.log.Timber;

/**
 * Created by maciek on 21.02.17.
 */
@Singleton
public class GetUserGroupChatsDataUseCase {

    public static final Function<Object[], List<GroupChatData>> GROUP_DATA_COMBINER = args -> {
        List<GroupChatData> groups = new ArrayList<>(args.length);
        for (Object match : args) groups.addAll((List<GroupChatData>) match);
        return groups;
    };
    public static final java.util.Comparator<GroupChatData> GROUP_DATA_LAST_INTERACTION_TIME_COMPARATOR =
            (groupDataThumbnail, groupDataThumbnail2) -> Long.valueOf(groupDataThumbnail2.currentUserChatMembership().lastInteraction())
                    .compareTo((groupDataThumbnail.currentUserChatMembership().lastInteraction()));
    private final UserChatsRepository userChatsRepository;
    public final GetGroupChatDataUseCase getGroupChatDataUseCase;

    @Inject
    public GetUserGroupChatsDataUseCase(UserChatsRepository userChatsRepository, GetGroupChatDataUseCase getGroupChatDataUseCase) {
        this.getGroupChatDataUseCase = getGroupChatDataUseCase;
        this.userChatsRepository = userChatsRepository;
    }

    public Flowable<List<GroupChatData>> onUserGroupsEvents(String uid) {
        return userChatsRepository.onUserChatsEvents(uid, ChatType.GROUP, MembershipStatus.ACTIVE)
                .switchMap(this::onUserGroupsEvents);
    }

    private Flowable<List<GroupChatData>> onUserGroupsEvents(List<UserVisibleChatMembership> userVisibleChatMemberships) {
        return Flowable.combineLatestDelayError(createMappings(userVisibleChatMemberships), GROUP_DATA_COMBINER)
                .switchIfEmpty(Flowable.just(Collections.emptyList()))
                .flatMapSingle(groupDataThumbnails -> Observable.fromIterable(groupDataThumbnails)
                        .toSortedList(GROUP_DATA_LAST_INTERACTION_TIME_COMPARATOR))
                .doOnError(throwable -> Timber.e(throwable, "Listening to group data for chat memberships interrupted." +
                        " Error should be indefinitely delayed and only valid data delivered"))
                .map(Collections::unmodifiableList);
    }

    //TODO Think of something smart. For now with every new data from upstream we are throwing all listeners away. We should reuse the
    // ones with matching keys or something
    private List<Flowable<List<GroupChatData>>> createMappings(List<UserVisibleChatMembership> userVisibleChatMemberships) {
        List<Flowable<List<GroupChatData>>> groupsDataSources = new ArrayList<>(userVisibleChatMemberships.size());
        for (UserVisibleChatMembership userVisibleChatMembership : userVisibleChatMemberships) {
            groupsDataSources.add(getGroupChatDataUseCase.onGroupChatData(userVisibleChatMembership));
        }
        return groupsDataSources;
    }

}
