package com.bimber.domain.chats.dialog;

import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.model.DialogChatData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import timber.log.Timber;

/**
 * Created by maciek on 21.02.17.
 */
@Singleton
public class GetUserDialogChatsDataUseCase {

    //Added here for optimization, better to have on static function then to create it every time we get new data
    private static final Function<Object[], List<DialogChatData>> DIALOG_DATA_COMBINER = args -> {
        List<DialogChatData> matches = new ArrayList<>(args.length);
        for (Object match : args) matches.addAll((List<DialogChatData>) match);
        return matches;
    };
    private static final java.util.Comparator<DialogChatData> DIALOG_DATA_LAST_INTERACTION_TIME_COMPARATOR =
            (dialogData1, dialogData2) -> Long.valueOf(dialogData2.currentUserChatMembership().lastInteraction())
                    .compareTo((dialogData1.currentUserChatMembership().lastInteraction()));

    private final UserChatsRepository userChatsRepository;
    private final GetDialogChatDataUseCase getDialogChatDataUseCase;

    @Inject
    public GetUserDialogChatsDataUseCase(UserChatsRepository userChatsRepository, GetDialogChatDataUseCase
            getDialogChatDataUseCase) {
        this.getDialogChatDataUseCase = getDialogChatDataUseCase;
        this.userChatsRepository = userChatsRepository;
    }

    public Flowable<List<DialogChatData>> onUserDialogsEvents(String uid) {
        return userChatsRepository.onUserChatsEvents(uid, Chattable.ChatType.DIALOG)
                .switchMap(this::onMapChatMembershipsToDialogsData);
    }

    private Flowable<List<DialogChatData>> onMapChatMembershipsToDialogsData(List<UserVisibleChatMembership>
                                                                                     userVisibleChatMemberships) {
        return Flowable.combineLatestDelayError(createMappings(userVisibleChatMemberships), DIALOG_DATA_COMBINER)
                .switchIfEmpty(Flowable.just(Collections.emptyList()))
                // Sorting resulting dialog datas by last interaction time
                .flatMapSingle(groupDataThumbnails -> Observable.fromIterable(groupDataThumbnails)
                        .toSortedList(DIALOG_DATA_LAST_INTERACTION_TIME_COMPARATOR))
                .doOnError(throwable -> Timber.e(throwable, "Listening to dialog data for chat memberships interrupted." +
                        " Error should be indefinitely delayed and only valid data delivered"))
                .map(Collections::unmodifiableList);
    }

    //TODO Think of something smart. For now with every new data from upstream we are throwing all listeners away. We should reuse the
    // ones with matching keys or something
    private List<Flowable<List<DialogChatData>>> createMappings(List<UserVisibleChatMembership> userVisibleChatMemberships) {
        List<Flowable<List<DialogChatData>>> dialogDataSources = new ArrayList<>(userVisibleChatMemberships.size());
        for (UserVisibleChatMembership chatVisibleChatMembership : userVisibleChatMemberships) {
            dialogDataSources.add(getDialogChatDataUseCase.onDialogChatData(chatVisibleChatMembership));
        }
        return dialogDataSources;
    }

}
