package com.bimber.domain.chats.dialog;

import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.DialogRepository;
import com.bimber.domain.chats.GetChatMembersDataUseCase;
import com.bimber.domain.model.DialogChatData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;


/**
 * Created by maciek on 29.03.17.
 */
@Singleton
public class GetDialogChatDataUseCase {

    private final DialogRepository dialogRepository;
    private final GetChatMembersDataUseCase getChatMembersDataUseCase;

    @Inject
    public GetDialogChatDataUseCase(DialogRepository dialogRepository, GetChatMembersDataUseCase getChatMembersDataUseCase) {
        this.dialogRepository = dialogRepository;
        this.getChatMembersDataUseCase = getChatMembersDataUseCase;
    }

    public Single<DialogChatData> dialogChatData(UserVisibleChatMembership membership) {
        return Single.zip(dialogRepository.dialog(membership.chatId()).toSingle(),
                getChatMembersDataUseCase.chatMembersThumbnails(membership.chatId()),
                (dialog, membersData) -> DialogChatData.create(membership, dialog, membersData))
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch dialog data, propagating error downstream"));
    }

    public Flowable<List<DialogChatData>> onDialogChatData(UserVisibleChatMembership membership) {
        return Flowable.zip(dialogRepository.onDialogEvents(membership.chatId()),
                getChatMembersDataUseCase.onChatMembersThumbnails(membership.chatId()),
                (dialogIterable, chatMemberProfileDatas) -> Observable.fromIterable(dialogIterable)
                        .map(dialogKey -> DialogChatData.create(membership, dialogKey, chatMemberProfileDatas))
                        .toList())
                .flatMapSingle(dialogDataIterable -> dialogDataIterable)
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch dialog data, propagating error downstream"));
    }

}
