package com.bimber.domain.chats;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.DefaultValues;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.PersonEvaluationRepository;
import com.bimber.domain.model.ChatMemberData;
import com.bimber.domain.model.ChatMemberDataEvaluated;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.utils.firebase.relation.Relation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.BiFunction;
import timber.log.Timber;


/**
 * Created by maciek on 29.03.17.
 */
@Singleton
public class GetChatMembersDataUseCase {

    private static final BiFunction<DataSnapshot, ChatMemberData, Boolean> SUBSCRIPTION_REUSING_CONDITION = (dataSnapshot,
                                                                                                             chatMemberProfileData) ->
            chatMemberProfileData.chatMembership().membershipStatus() == ChatVisibleChatMembership.create(dataSnapshot).membershipStatus();
    public static final BiFunction<DataSnapshot, ChatMemberData, ChatMemberData> SUBSCRIPTION_REUSING = (dataSnapshot,
                                                                                                         chatMemberProfileData) ->
            chatMemberProfileData.toBuilder().chatMembership(ChatVisibleChatMembership.create(dataSnapshot)).build();

    private final DefaultValues defaultValues;
    private final ChatMemberRepository chatMemberRepository;
    private final ProfileDataRepository profileDataRepository;
    private final PersonEvaluationRepository personEvaluationRepository;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public GetChatMembersDataUseCase(DefaultValues defaultValues, ChatMemberRepository chatMemberRepository, ProfileDataRepository profileDataRepository,
                                     PersonEvaluationRepository personEvaluationRepository, BackgroundSchedulerProvider
                                             backgroundSchedulerProvider) {
        this.defaultValues = defaultValues;
        this.chatMemberRepository = chatMemberRepository;
        this.profileDataRepository = profileDataRepository;
        this.personEvaluationRepository = personEvaluationRepository;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public Flowable<List<ChatMemberData>> onChatMembersThumbnails(String chatId) {
        return chatMemberRepository.onChatMembershipsEvents(chatId)
                //TODO Optimize data normalization instead of thorwing away previous subscribtions with every upstream change
                .flatMapSingle(this::mapChatMembershipsToProfiles)
                .doOnError(throwable -> Timber.e(throwable, "Error when listening to chat memberships data, propagating error downstream"));
    }

    public Flowable<List<ChatMemberData>> onChatMembersThumbnails(String chatId, MembershipStatus membershipStatus) {
        return chatMemberRepository.onChatMembershipsEvents(chatId, membershipStatus)
                //TODO Optimize data normalization instead of thorwing away previous subscribtions with every upstream change
                .flatMapSingle(this::mapChatMembershipsToProfiles)
                .doOnError(throwable -> Timber.e(throwable, "Error when listening to chat memberships data, propagating error downstream"));
    }

    public Flowable<List<ChatMemberData>> onChatMembers(String chatId) {
        Query query = chatMemberRepository.membersNode().child(chatId)
                .orderByChild(ChatVisibleChatMembership.MEMBERSHIP_STATUS_FIELD_NAME)
                .equalTo(MembershipStatus.ACTIVE.toString());
        return onChatMembers(query);
    }

    public Flowable<List<ChatMemberData>> onChatMembers(String chatId, int limit) {
        Query query = chatMemberRepository.membersNode().child(chatId)
                .orderByChild(ChatVisibleChatMembership.LAST_INTERACTION_FIELD_NAME)
                .limitToLast(limit);
        return onChatMembers(query);
    }

    private Flowable<List<ChatMemberData>> onChatMembers(Query query) {
        return new Relation.Builder(query)
                .relation(dataSnapshot -> Single.defer(() -> Single.just(ChatVisibleChatMembership.create(dataSnapshot)))
                        .flatMap(this::chatMember)
                        .toFlowable()
                        .observeOn(backgroundSchedulerProvider.backgroundScheduler()))
                .withSubscriptionReusing(SUBSCRIPTION_REUSING)
                .withSubscriptionReusingCondition(SUBSCRIPTION_REUSING_CONDITION)
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle(chatMemberProfileDatas -> Observable.fromIterable(chatMemberProfileDatas)
                        .filter(mamberData -> mamberData.chatMembership().membershipStatus() == MembershipStatus.ACTIVE)
                        .sorted((o1, o2) -> Long.valueOf(o2.chatMembership().lastInteraction()).compareTo(o1.chatMembership()
                                .lastInteraction()))
                        .toList());
    }

    public Flowable<List<ChatMemberData>> onChatMembersFiltered(String chatId, String filteredUId) {
        Query query = chatMemberRepository.membersNode().child(chatId)
                .orderByChild(ChatVisibleChatMembership.LAST_INTERACTION_FIELD_NAME);

        return new Relation.Builder(query)
                .relation(dataSnapshot -> Single.defer(() -> Single.just(ChatVisibleChatMembership.create(dataSnapshot)))
                        .filter(chatVisibleChatMembership -> !chatVisibleChatMembership.uId().equals(filteredUId))
                        .flatMapSingleElement(this::chatMember)
                        .toFlowable()
                        .observeOn(backgroundSchedulerProvider.backgroundScheduler()))
                .withProgressiveInitialQuery(true)
                .withSubscriptionReusing(SUBSCRIPTION_REUSING)
                .withSubscriptionReusingCondition(SUBSCRIPTION_REUSING_CONDITION)
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle(chatMemberProfileDatas -> Observable.fromIterable(chatMemberProfileDatas)
                        .filter(mamberData -> mamberData.chatMembership().membershipStatus() == MembershipStatus.ACTIVE)
                        .sorted((o1, o2) -> Long.valueOf(o2.chatMembership().lastInteraction()).compareTo(o1.chatMembership()
                                .lastInteraction()))
                        .toList());
    }


    public Flowable<List<ChatMemberDataEvaluated>> onChatMembersEvaluated(String chatId, String currentUserId) {
        Query query = chatMemberRepository.membersNode().child(chatId)
                .orderByChild(ChatVisibleChatMembership.MEMBERSHIP_STATUS_FIELD_NAME)
                .equalTo(MembershipStatus.ACTIVE.toString());
        return onChatMembersEvaluated(query, currentUserId);
    }

    public Flowable<List<ChatMemberDataEvaluated>> onChatMembersEvaluated(String chatId, String currentUserId, int limit) {
        Query query = chatMemberRepository.membersNode().child(chatId)
                .orderByChild(ChatVisibleChatMembership.LAST_INTERACTION_FIELD_NAME)
                .limitToLast(limit);
        return onChatMembersEvaluated(query, currentUserId);
    }

    public Flowable<List<ChatMemberDataEvaluated>> onChatMembersEvaluated(Query query, String currentUserId) {
        return new Relation.Builder(query)
                .relation(dataSnapshot -> Single.defer(() -> Single.just(ChatVisibleChatMembership.create(dataSnapshot)))
                        .flatMapPublisher(membership -> onChatMemberEvaluated(membership, currentUserId))
                        .observeOn(backgroundSchedulerProvider.backgroundScheduler()))
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle(chatMemberProfileDatas -> Observable.fromIterable(chatMemberProfileDatas)
                        .filter(mamberData -> mamberData.chatMembership().membershipStatus() == MembershipStatus.ACTIVE)
                        .sorted((o1, o2) -> Long.valueOf(o2.chatMembership().lastInteraction()).compareTo(o1.chatMembership()
                                .lastInteraction()))
                        .toList());
    }

    public Single<List<ChatMemberData>> chatMembersThumbnails(String chatId) {
        return chatMemberRepository.chatMemberships(chatId)
                .flatMap(this::mapChatMembershipsToProfiles)
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch chat memberships data, propagating error downstream"));
    }

    public Single<List<ChatMemberData>> chatMembersThumbnails(String chatId, MembershipStatus membershipStatus) {
        return chatMemberRepository.chatMemberships(chatId, membershipStatus)
                .flatMap(this::mapChatMembershipsToProfiles)
                .doOnError(throwable -> Timber.e(throwable, "Couldn't fetch chat memberships data, propagating error downstream"));
    }

    private Single<List<ChatMemberData>> mapChatMembershipsToProfiles(List<ChatVisibleChatMembership> chatVisibleChatMemberships) {
        return Flowable.mergeDelayError(Flowable.fromIterable(chatVisibleChatMemberships)
                .map((chatMembership) -> chatMember(chatMembership).toFlowable()))
                .doOnError(throwable -> Timber.e(throwable, "Error when processing mapping chat memberships to users." +
                        " Propagating successfully mapped data and ignoring error "))
                .onErrorResumeNext(Flowable.empty())
                .toList();
    }

    private Single<ChatMemberData> chatMember(ChatVisibleChatMembership chatVisibleChatMembership) {
        return profileDataRepository.userProfileDataThumbnail(chatVisibleChatMembership.uId())
                .onErrorReturnItem(defaultValues.defaultProfileDataThumbnail(chatVisibleChatMembership.uId()))
                .map(profileDataThumbnail -> ChatMemberData.create(chatVisibleChatMembership, profileDataThumbnail))
                .doOnError(throwable -> Timber.e(throwable, "Couldn't map profile data to chat membership, propagating error downstream"));
    }

    private Flowable<ChatMemberDataEvaluated> onChatMemberEvaluated(ChatVisibleChatMembership chatVisibleChatMembership, String
            currentUserId) {
        return Flowable.combineLatest(profileDataRepository.onUserProfileDataThumbnail(chatVisibleChatMembership.uId())
                        .onErrorReturnItem(defaultValues.defaultProfileDataThumbnail(chatVisibleChatMembership.uId())),
                personEvaluationRepository.onEvaluatedPerson(currentUserId, chatVisibleChatMembership.uId()),
                (profileDataThumbnail, personEvaluation) -> ChatMemberDataEvaluated.create(chatVisibleChatMembership,
                        profileDataThumbnail, personEvaluation.value()))
                .doOnError(throwable -> Timber.e(throwable, "Couldn't map profile data to chat membership with evaluation status, " +
                        "propagating error downstream"));
    }
}
