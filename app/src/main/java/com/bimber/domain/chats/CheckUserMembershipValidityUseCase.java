package com.bimber.domain.chats;

import com.bimber.data.repositories.ChatMemberRepository;

import javax.inject.Inject;

import io.reactivex.Flowable;
import timber.log.Timber;

/**
 * Created by maciek on 22.05.17.
 */

public class CheckUserMembershipValidityUseCase {

    private final ChatMemberRepository chatMemberRepository;

    @Inject
    public CheckUserMembershipValidityUseCase(ChatMemberRepository chatMemberRepository) {
        this.chatMemberRepository = chatMemberRepository;
    }

    public Flowable<Boolean> onUserActiveChatMemberStatus(String chatId, String uId) {
        return chatMemberRepository.onChatMembershipEvents(chatId, uId)
                .concatMap(chatMemberships -> Flowable.fromIterable(chatMemberships)
                        .filter(chatMembership -> chatMembership.uId().equals(uId))
                        .map(chatMemberProfileData -> {
                            switch (chatMemberProfileData.membershipStatus()) {
                                case ACTIVE:
                                    return true;
                                default:
                                    return false;
                            }
                        })
                        .switchIfEmpty(Flowable.just(false)))
                .doOnError(throwable -> Timber.e(throwable, "Error while listening to active chat membership. Returning false value and " +
                        "consuming error"))
                .onErrorReturnItem(false);

    }
}
