package com.bimber.domain.chats.search;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.domain.model.GroupData;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.firebase.relation.Relation;
import com.google.auto.value.AutoValue;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.firebase.database.Query;
import com.jakewharton.rx.ReplayingShare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by maciek on 13.10.17.
 */
@Singleton
public class SearchForChatsUseCase {


    private final UserChatsRepository userChatsRepository;
    private final ChatMemberRepository chatMemberRepository;
    private final GroupRepository groupRepository;
    private final UsersRepository usersRepository;
    private final BackgroundSchedulerProvider backgroundSchedulerProvider;
    private final GetGroupDataUseCase getGroupDataUseCase;
    private final ProfileDataRepository profileDataRepository;
    private final Cache<String, Flowable<List<SearchableTerm>>> searchableTermsCache = CacheBuilder.newBuilder()
            .maximumSize(1)
            .build();

    private final Cache<String, Flowable<List<SearchResult>>> searchResultsCache = CacheBuilder.newBuilder()
            .maximumSize(1)
            .build();

    @Inject

    public SearchForChatsUseCase(UserChatsRepository userChatsRepository, ChatMemberRepository chatMemberRepository, GroupRepository
            groupRepository, UsersRepository usersRepository, BackgroundSchedulerProvider backgroundSchedulerProvider, GetGroupDataUseCase
                                         getGroupDataUseCase, ProfileDataRepository profileDataRepository) {
        this.userChatsRepository = userChatsRepository;
        this.chatMemberRepository = chatMemberRepository;
        this.groupRepository = groupRepository;
        this.usersRepository = usersRepository;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
        this.getGroupDataUseCase = getGroupDataUseCase;
        this.profileDataRepository = profileDataRepository;
    }

    public Flowable<ChatSearchResults> searchForChats(String uId, Flowable<String> searchPhrasesStream) {
        Flowable<List<SearchableTerm>> searchableTermsCache = this.searchableTermsCache.getIfPresent(uId);

        if (searchableTermsCache == null) {
            searchableTermsCache = createSearchableWordsStore(uId)
                    .compose(ReplayingShare.instance());
            this.searchableTermsCache.put(uId, searchableTermsCache);
        }
        Flowable<List<SearchableTerm>> searchableTermsShared = searchableTermsCache.replay(1).refCount();

        Flowable<String> searchPhrasesShared = searchPhrasesStream.replay(1).refCount();
        Flowable<List<SearchResult>> searchResultsCache = this.searchResultsCache.getIfPresent(uId);
        Flowable<List<SearchResult>> searchResultsSafeCache = searchResultsCache != null ? searchResultsCache : Flowable.empty();
        Flowable<List<SearchResult>> searchResultsSource = searchPhrasesShared
                .observeOn(backgroundSchedulerProvider.backgroundScheduler())
                .flatMap(searchTerm -> searchableTermsShared.flatMap(Flowable::fromIterable)
                        .filter(searchableTerm -> containsIgnoreCase(searchableTerm.searchableTerm(), searchTerm)))
                .distinct(SearchableTerm::sourceId)
                .flatMap(searchableTerm -> searchResultsSafeCache.flatMap(Flowable::fromIterable)
                        .filter(searchResult -> searchResult.searchableTerm().sourceId().equals(searchableTerm.sourceId()))
                        .switchIfEmpty(fetchSearchResult(searchableTerm).toFlowable()))

                //TODO Figure out why distinct is not working
//                .distinct(searchResult -> searchResult.searchableTerm().sourceId())
                .scan((Collections.synchronizedMap(new HashMap<String, SearchResult>())), (searchResults, searchResult) -> {
                    searchResults.put(searchResult.searchableTerm().sourceId(), searchResult);
                    return searchResults;
                })
                .map(searchResults -> ((List<SearchResult>) new ArrayList<>(searchResults.values())))
                .compose(ReplayingShare.instance())
                .replay(1)
                .refCount();

        this.searchResultsCache.put(uId, searchResultsSource);

        return searchPhrasesShared.switchMap(searchTerm -> {
            if (searchTerm.isEmpty()) {
                return Flowable.just(ChatSearchResults.create(searchTerm, Collections.emptyList()));
            } else {
                return searchResultsSource
                        .flatMapSingle(searchResults -> Flowable.fromIterable(searchResults)
                                .filter(searchResult -> containsIgnoreCase(searchResult.searchableTerm().searchableTerm(), searchTerm))
                                .toList()
                                .map(filteredSearchResults -> ChatSearchResults.create(searchTerm, filteredSearchResults)));
            }
        });

    }

    private Flowable<List<SearchableTerm>> createSearchableWordsStore(String uId) {
        Query query = userChatsRepository.userChatsNode(uId)
                .orderByChild(UserVisibleChatMembership.LAST_INTERACTION_FIELD_NAME);
        return new Relation.Builder(query)
                .relation(dataSnapshot -> Single.defer(() -> Single.just(UserVisibleChatMembership.create(dataSnapshot)))
                        .flatMap(userVisibleChatMembership -> {
                            switch (userVisibleChatMembership.chatType()) {
                                case DIALOG:
                                    return chatMemberRepository.chatMemberships(userVisibleChatMembership.chatId())
                                            .flatMapObservable(Observable::fromIterable)
                                            .filter(chatVisibleChatMembership -> !chatVisibleChatMembership.uId().equals(uId))
                                            .flatMapSingle(chatVisibleChatMembership -> usersRepository.user(chatVisibleChatMembership.uId())
                                                    .map(user -> SearchableTerm.create(userVisibleChatMembership, chatVisibleChatMembership.uId(), user.displayName)))
                                            .toList();
                                case GROUP:
                                    return groupRepository.groupName(userVisibleChatMembership.chatId()).toObservable()
                                            .map(groupName -> SearchableTerm.create(userVisibleChatMembership, userVisibleChatMembership
                                                    .chatId(), groupName))
                                            .toList();
                                default:
                                    throw new IllegalArgumentException();
                            }
                        })
                        .toFlowable()
                        .onErrorReturnItem(Collections.emptyList()))
                .withProgressiveInitialQuery(true)
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle((source) -> Flowable.fromIterable(source)
                        .flatMap(Flowable::fromIterable)
                        .toList());
    }

    private Maybe<? extends SearchResult> fetchSearchResult(SearchableTerm searchableTerm) {
        switch (searchableTerm.chatMembership().chatType()) {
            case DIALOG:
                return profileDataRepository.userProfileDataThumbnail(searchableTerm.sourceId())
                        .map(profileDataThumbnail -> DialogSearchResult.create(searchableTerm, profileDataThumbnail))
                        .toMaybe()
                        .onErrorComplete();
            case GROUP:
                return getGroupDataUseCase.onGroupDataOptimized(searchableTerm.sourceId(), 3)
                        .firstOrError()
                        .toFlowable()
                        .flatMap(Flowable::fromIterable)
                        .map(groupData -> GroupSearchResult.create(searchableTerm, groupData))
                        .firstElement()
                        .onErrorComplete();
            default:
                throw new IllegalArgumentException();
        }
    }


    @AutoValue
    public static abstract class SearchableTerm {
        public abstract UserVisibleChatMembership chatMembership();

        public abstract String sourceId();

        public abstract String searchableTerm();

        public static SearchableTerm create(UserVisibleChatMembership chatMembership, String surceId, String searchableTerm) {
            return new AutoValue_SearchForChatsUseCase_SearchableTerm(chatMembership, surceId, searchableTerm);
        }
    }

    public interface SearchResult<T> {
        String chatId();

        ChatType chatType();

        SearchableTerm searchableTerm();

        T result();

    }

    @AutoValue
    public static abstract class DialogSearchResult implements SearchResult<ProfileDataThumbnail> {

        public abstract SearchableTerm searchableTerm();

        public abstract ProfileDataThumbnail result();

        public static DialogSearchResult create(SearchableTerm searchableTerm, ProfileDataThumbnail profileDataThumbnail) {
            return new AutoValue_SearchForChatsUseCase_DialogSearchResult(searchableTerm.chatMembership().chatId(), ChatType.DIALOG,
                    searchableTerm, profileDataThumbnail);
        }
    }


    @AutoValue
    public static abstract class GroupSearchResult implements SearchResult<GroupData> {

        public abstract SearchableTerm searchableTerm();

        public abstract GroupData result();

        public static GroupSearchResult create(SearchableTerm searchableTerm, GroupData groupData) {
            return new AutoValue_SearchForChatsUseCase_GroupSearchResult(searchableTerm.chatMembership().chatId(), ChatType.GROUP,
                    searchableTerm, groupData);
        }
    }

    @AutoValue
    public static abstract class ChatSearchResults {

        public abstract String searchPhrase();

        public abstract List<SearchResult> searchResults();

        public static ChatSearchResults create(String searchPhrase, List<SearchResult> searchResults) {
            return new AutoValue_SearchForChatsUseCase_ChatSearchResults(searchPhrase, searchResults);
        }
    }

    private static boolean containsIgnoreCase(String src, String what) {
        final int length = what.length();
        if (length == 0)
            return true; // Empty string is contained

        final char firstLo = Character.toLowerCase(what.charAt(0));
        final char firstUp = Character.toUpperCase(what.charAt(0));

        for (int i = src.length() - length; i >= 0; i--) {
            // Quick check before calling the more expensive regionMatches() method:
            final char ch = src.charAt(i);
            if (ch != firstLo && ch != firstUp)
                continue;

            if (src.regionMatches(true, i, what, 0, length))
                return true;
        }

        return false;
    }
}
