package com.bimber.domain.chats.group;

import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.repositories.GroupRepository;
import com.bimber.domain.chats.GetChatMembersDataUseCase;
import com.bimber.domain.group.GetGroupDataUseCase;
import com.bimber.domain.model.GroupChatData;
import com.bimber.domain.model.GroupData;
import com.bimber.domain.model.GroupDataWithEvaluations;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import timber.log.Timber;

/**
 * Created by maciek on 29.03.17.
 */
@Singleton
public class GetGroupChatDataUseCase {

    private final GetGroupDataUseCase getGroupDataUseCase;
    private final GroupRepository groupRepository;
    private final GetChatMembersDataUseCase getChatMembersDataUseCase;

    @Inject
    public GetGroupChatDataUseCase(GetGroupDataUseCase getGroupDataUseCase, GroupRepository groupRepository, GetChatMembersDataUseCase
            getChatMembersDataUseCase) {
        this.getGroupDataUseCase = getGroupDataUseCase;
        this.groupRepository = groupRepository;
        this.getChatMembersDataUseCase = getChatMembersDataUseCase;
    }

    public Flowable<List<GroupChatData>> onGroupChatData(UserVisibleChatMembership userVisibleChatMembership) {
        return getGroupDataUseCase.onGroupData(userVisibleChatMembership.chatId())
                .map(mapGroupDataToGroupChatData(userVisibleChatMembership));
    }

    public Flowable<List<GroupChatData>> onGroupChatDataOptimized(UserVisibleChatMembership userVisibleChatMembership) {
        return getGroupDataUseCase.onGroupDataOptimized(userVisibleChatMembership.chatId(), 3)
                .map(mapGroupDataToGroupChatData(userVisibleChatMembership));
    }

    public Flowable<List<GroupDataWithEvaluations>> onGroupDataWithEvaluations(String currentUserId, String chatId) {
        return Flowable.combineLatest(
                groupRepository.onGroupEvents(chatId),
                getChatMembersDataUseCase.onChatMembersEvaluated(chatId, currentUserId),
                (groupList, chatMembersProfilesData) -> Observable.fromIterable(groupList)
                        .map(group -> GroupDataWithEvaluations.create(group, chatMembersProfilesData))
                        .toList())
                .flatMapSingle(groupData -> groupData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to public group data with evaluated users, for chat with id:" +
                        " %s. Consuming error and delivering empty list", chatId))
                .onErrorReturnItem(Collections.emptyList());
    }

    public Flowable<List<GroupDataWithEvaluations>> onGroupDataWithEvaluations(String currentUserId, String chatId, int
            limitMembers) {
        return Flowable.combineLatest(
                groupRepository.onGroupEvents(chatId),
                getChatMembersDataUseCase.onChatMembersEvaluated(chatId, currentUserId, limitMembers),
                (groupList, chatMembersProfilesData) -> Observable.fromIterable(groupList)
                        .map(group -> GroupDataWithEvaluations.create(group, chatMembersProfilesData))
                        .toList())
                .flatMapSingle(groupData -> groupData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to public group data with evaluated users, for chat with id:" +
                        " %s. Consuming error and delivering empty list", chatId))
                .onErrorReturnItem(Collections.emptyList());
    }

    @android.support.annotation.NonNull
    private Function<List<GroupData>, List<GroupChatData>> mapGroupDataToGroupChatData(final UserVisibleChatMembership
                                                                                               userVisibleChatMembership) {
        return groupDatas -> {
            if (groupDatas.isEmpty()) {
                return Collections.emptyList();
            } else {
                return Collections.singletonList(GroupChatData.create(userVisibleChatMembership, groupDatas.get(0).group(),
                        groupDatas.get(0).membersData()));
            }
        };
    }
}
