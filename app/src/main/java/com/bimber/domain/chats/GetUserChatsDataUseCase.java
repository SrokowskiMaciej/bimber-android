package com.bimber.domain.chats;

import android.support.annotation.NonNull;

import com.bimber.data.BackgroundSchedulerProvider;
import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.NotificationMuteState;
import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.chat.membership.ChatMembership;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.entities.chat.messages.common.ContentType;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.data.repositories.GroupJoinRequestRepository;
import com.bimber.data.repositories.LastSeenMessagesRepository;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.repositories.NotificationMuteStateRepository;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.chats.dialog.GetDialogChatDataUseCase;
import com.bimber.domain.chats.group.GetGroupChatDataUseCase;
import com.bimber.domain.model.ChatData;
import com.bimber.domain.model.DialogChatData;
import com.bimber.domain.model.GroupChatData;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.relation.Relation;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import timber.log.Timber;

import static com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED;

/**
 * Created by maciek on 19.06.17.
 */
@Singleton
public class GetUserChatsDataUseCase {

    public static final BiFunction<DataSnapshot, List<DetailedChatData>, List<DetailedChatData>> SUBSCRIPTION_REUSING =
            (dataSnapshot, detailedChatDatas) -> {
                if (detailedChatDatas.isEmpty()) {
                    return detailedChatDatas;
                } else {
                    UserVisibleChatMembership userVisibleChatMembership = UserVisibleChatMembership.create(dataSnapshot);
                    DetailedChatData detailedChatData = detailedChatDatas.get(0);
                    ChatData newChatData;
                    if (detailedChatData.data().chatType() == ChatType.DIALOG) {
                        newChatData = DialogChatData.create(userVisibleChatMembership,
                                detailedChatData.data().chat(),
                                detailedChatData.data().membersData());
                    } else {
                        newChatData = GroupChatData.create(userVisibleChatMembership,
                                detailedChatData.data().chat(),
                                detailedChatData.data().membersData());
                    }
                    return Collections.singletonList(DetailedChatData.create(newChatData, detailedChatData.lastMessages()
                            , detailedChatData.currentUserLastSeenMessage(), detailedChatData.notificationMuteState(), detailedChatData
                                    .hasJoinRequests()));
                }
            };
    public static final BiFunction<DataSnapshot, List<DetailedChatData>, Boolean> SUBSCRIPTION_REUSING_CONDITION = (dataSnapshot,
                                                                                                                    detailedChatDatas) -> {
        MembershipStatus newStatus = UserVisibleChatMembership.create(dataSnapshot).membershipStatus();
        return !detailedChatDatas.isEmpty() &&
                newStatus == detailedChatDatas.get(0).data().currentUserChatMembership().membershipStatus();
    };
    private final UserChatsRepository userChatsRepository;
    private final GetDialogChatDataUseCase getDialogChatDataUseCase;
    private final GetGroupChatDataUseCase getGroupChatDataUseCase;
    private final MessageRepository messageRepository;
    private final LastSeenMessagesRepository lastSeenMessagesRepository;
    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final NotificationMuteStateRepository notificationMuteStateRepository;

    private final BackgroundSchedulerProvider backgroundSchedulerProvider;

    @Inject
    public GetUserChatsDataUseCase(UserChatsRepository userChatsRepository, GetDialogChatDataUseCase getDialogChatDataUseCase,
                                   GetGroupChatDataUseCase getGroupChatDataUseCase, MessageRepository messageRepository,
                                   LastSeenMessagesRepository lastSeenMessagesRepository, GroupJoinRequestRepository
                                           groupJoinRequestRepository, NotificationMuteStateRepository
                                           notificationMuteStateRepository, BackgroundSchedulerProvider backgroundSchedulerProvider) {
        this.userChatsRepository = userChatsRepository;
        this.getDialogChatDataUseCase = getDialogChatDataUseCase;
        this.getGroupChatDataUseCase = getGroupChatDataUseCase;
        this.messageRepository = messageRepository;
        this.lastSeenMessagesRepository = lastSeenMessagesRepository;
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.notificationMuteStateRepository = notificationMuteStateRepository;
        this.backgroundSchedulerProvider = backgroundSchedulerProvider;
    }

    public Flowable<List<DetailedChatData>> onUserChatsEventsOptimized(String uid, int limit) {
        Query query = userChatsRepository.userChatsNode(uid)
                .orderByChild(UserVisibleChatMembership.LAST_INTERACTION_FIELD_NAME)
                .limitToLast(limit);

        return new Relation.Builder(query)
                .relation(mapUserMembershipToChatData())
                .withSubscriptionReusing(SUBSCRIPTION_REUSING)
                .withSubscriptionReusingCondition(SUBSCRIPTION_REUSING_CONDITION)
                .onScheduler(backgroundSchedulerProvider.backgroundScheduler())
                .observe()
                .flatMapSingle(restrictedChatDataWithMutes -> Observable.fromIterable(restrictedChatDataWithMutes)
                        .filter(restrictedChatDataWithMute -> !restrictedChatDataWithMute.isEmpty())
                        .map(restrictedChatDataWithMute -> restrictedChatDataWithMute.get(0))
                        .sorted((o1, o2) -> Long.valueOf(o2.data().currentUserChatMembership().lastInteraction())
                                .compareTo(o1.data().currentUserChatMembership().lastInteraction()))
                        .toList());
    }

    @NonNull
    private Function<DataSnapshot, Flowable<List<DetailedChatData>>> mapUserMembershipToChatData() {
        return dataSnapshot -> Flowable.defer(() -> Flowable.just(UserVisibleChatMembership.create(dataSnapshot)))
                .flatMap(this::onDetailedChatData)
                .doOnError(throwable -> Timber.e(throwable, "Error listening to restricted group data, for chat " +
                        "Consuming error and delivering empty list"))
                .onErrorReturnItem(Collections.emptyList())
                .observeOn(backgroundSchedulerProvider.backgroundScheduler());
    }

    private Flowable<? extends List<? extends ChatData>> onChatData(UserVisibleChatMembership userVisibleChatMembership) {
        switch (userVisibleChatMembership.chatType()) {
            case DIALOG:
                return getDialogChatDataUseCase.onDialogChatData(userVisibleChatMembership);
            case GROUP:
                return getGroupChatDataUseCase.onGroupChatDataOptimized(userVisibleChatMembership);
            default:
                throw new IllegalArgumentException();
        }
    }

    private Flowable<List<DetailedChatData>> onDetailedChatData(UserVisibleChatMembership membership) {
        return Flowable.combineLatest(
                onChatData(membership),
                getLastShowableMessages(membership),
                lastSeenMessagesRepository.onChatUserLastSeenMessageEvents(membership.chatId(), membership.uId()),
                notificationMuteStateRepository.onState(membership.chatId()).toFlowable(BackpressureStrategy.DROP),
                onJoinRequestsAvailable(membership), (chatDataIterable, lastMessage, lastSeenMessage, notificationMuteState,
                                                      hasJoinRequestsAvailable) ->
                        Observable.fromIterable(chatDataIterable)
                                .map(chatData -> DetailedChatData.create(chatData, lastMessage, lastSeenMessage, notificationMuteState,
                                        hasJoinRequestsAvailable))
                                .toList())
                .flatMapSingle(groupData -> groupData);
    }

    private Flowable<List<Message>> getLastShowableMessages(ChatMembership chatMembership) {
        return messageRepository.onChatMessages(chatMembership, 3)
                .flatMapSingle(lastMessage -> Observable.fromIterable(lastMessage)
                        .filter(message -> !(message.getContentType().equals(ContentType.IMAGE) &&
                                ((ImageContent) message.getContent()).uploadStatus() != ImageContent.UploadStatus.FINISHED))
                        .toList())
                .doOnError(throwable -> Timber.e(throwable, "Error obtaining message for given chat, resuming with empty list"))
                .onErrorReturnItem(Collections.emptyList());
    }

    private Flowable<Boolean> onJoinRequestsAvailable(UserVisibleChatMembership userVisibleChatMembership) {
        switch (userVisibleChatMembership.chatType()) {
            case DIALOG:
                return Flowable.just(false);

            case GROUP:
                return groupJoinRequestRepository.onGroupJoinRequestsEvents(userVisibleChatMembership.chatId(), MEMBERSHIP_REQUESTED)
                        .map(joinRequests -> !joinRequests.isEmpty());
            default:
                throw new IllegalArgumentException();
        }
    }

    @AutoValue
    public static abstract class DetailedChatData {

        public abstract ChatData data();

        public abstract List<Message> lastMessages();

        public abstract List<Key<String>> currentUserLastSeenMessage();

        public abstract NotificationMuteState notificationMuteState();

        public abstract boolean hasJoinRequests();

        public static DetailedChatData create(ChatData data, List<Message> lastMessages, List<Key<String>>
                currentUserLastSeenMessage, NotificationMuteState notificationMuteState, boolean hasJoinRequests) {

            return new AutoValue_GetUserChatsDataUseCase_DetailedChatData(data, lastMessages, currentUserLastSeenMessage,
                    notificationMuteState, hasJoinRequests);
        }

    }

}
