package com.bimber.domain.discovery.users

import com.bimber.data.entities.DefaultValues
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail
import com.bimber.data.entities.profile.network.UserLocationModel
import com.bimber.data.sources.discovery.users.NearbyDiscoverableUsersRepository
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository
import com.bimber.data.sources.profile.location.UserLocationNetworkSource
import com.bimber.domain.model.NearbyUserData
import com.bimber.utils.rx.aggregateFrom
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 22.06.17.
 */
@Singleton
class GetNearbyDiscoverableUsersUseCase @Inject
constructor(private val defaultValues: DefaultValues,
            private val profileDataRepository: ProfileDataRepository,
            private val userLocationNetworkSource: UserLocationNetworkSource,
            private val nearbyDiscoverableUsersRepository: NearbyDiscoverableUsersRepository) {

    fun onDiscoverableUsers(currentUserId: String): Flowable<NearbyUsersResult> {
        return userLocationNetworkSource.onLocationEvents(currentUserId)
                .switchMap { locations ->
                    if (locations.isEmpty()) {
                        Flowable.just(NearbyUsersResult.NoLocationChosen())
                    } else {
                        onNearbyDiscoverableUsers(currentUserId, locations[0])
                                .sample(1, TimeUnit.SECONDS)
                                .startWith(NearbyUsersResult.Loading(emptyList()))
                    }
                }
    }

    private fun onNearbyDiscoverableUsers(currentUserId: String, location: UserLocationModel): Flowable<NearbyUsersResult> {
        val request = NearbyDiscoverableUsersRepository.Request(currentUserId, location.latitude(), location.longitude(), location.range())

        nearbyDiscoverableUsersRepository.clear()
        nearbyDiscoverableUsersRepository.requestUsersAtLocation(request)
        nearbyDiscoverableUsersRepository.requestMoreUsers()

        return nearbyDiscoverableUsersRepository.onNearbyDiscoverableUsers()
                .aggregateFrom(unpack = { it.users },
                        keySelector = { it },
                        mapping = { userDiscoverabilityData(it, location) },
                        repack = { input, outputs ->
                            when (input) {
                                is NearbyDiscoverableUsersRepository.Result.Success -> {
                                    if (input.noMoreUsers) {
                                        NearbyUsersResult.NoMoreUsersInTheArea(outputs)
                                    } else {
                                        NearbyUsersResult.Success(outputs)
                                    }
                                }
                                is NearbyDiscoverableUsersRepository.Result.Loading -> NearbyUsersResult.Loading(outputs)
                                is NearbyDiscoverableUsersRepository.Result.Error -> NearbyUsersResult.Error(outputs)
                            }
                        })
    }


    private fun userDiscoverabilityData(uId: String, referenceUserLocation: UserLocationModel): Flowable<NearbyUserData> {
        return Flowable.zip(
                profileDataRepository.userProfileDataThumbnail(uId).toFlowable(),
                userLocationNetworkSource.location(uId).toFlowable(),
                BiFunction<ProfileDataThumbnail, UserLocationModel, NearbyUserData> { profile, userLocation ->
                    NearbyUserData.create(profile, userLocation, referenceUserLocation)
                })
                .filter { it -> it.profileData().user.uId != DefaultValues.UNDEFINED_KEY }
                .filter({ it -> it.profileData().user.displayName != DefaultValues.UNKNOWN_USER_NAME })
                //TODO Poor idea, it wont work when user changes language
                .filter({ it -> it.profileData().user.displayName != defaultValues.defaultUser.displayName })
                .filter({ it -> it.profileData().user.displayName != defaultValues.deletedUser.displayName })
                .doOnError { throwable -> Timber.e(throwable, "Error while fetching user data for non evaluated user: %s. " + "Consuming error and not showing this specific user", uId) }
                .onErrorResumeNext(Flowable.empty<NearbyUserData>())
    }

    sealed class NearbyUsersResult {
        class NoLocationChosen : NearbyUsersResult()
        data class Loading(val remainingUsers: List<NearbyUserData>) : NearbyUsersResult()
        data class Success(val nearbyUsers: List<NearbyUserData>) : NearbyUsersResult()
        data class NoMoreUsersInTheArea(val remainingUsers: List<NearbyUserData>) : NearbyUsersResult()
        data class Error(val remainingUsers: List<NearbyUserData>) : NearbyUsersResult()
    }
}
