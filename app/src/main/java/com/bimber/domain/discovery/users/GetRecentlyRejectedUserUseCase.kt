package com.bimber.domain.discovery.users

import com.bimber.data.sources.discovery.users.RecentlyRejectedUserRepository
import com.bimber.domain.discovery.users.GetRecentlyRejectedUserUseCase.RecentlyRejectedUserResult.*
import com.bimber.domain.model.NearbyUserData
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 22.06.17.
 */

@Singleton
class GetRecentlyRejectedUserUseCase
@Inject
constructor(private val recentlyRejectedUserRepository: RecentlyRejectedUserRepository) {

    fun onRecentlyRejectedUser(): Flowable<RecentlyRejectedUserResult> {
        return recentlyRejectedUserRepository.onReversibleUser()
                .map {
                    if (it.isEmpty()) {
                        NoRecentlyRejectedUserResult()
                    } else when (it[0]) {
                        is RecentlyRejectedUserRepository.ReversibleUser.RejectedUser -> HasRejectedUserResult(it[0].user)
                        is RecentlyRejectedUserRepository.ReversibleUser.RevertedUser -> HasRevertedUserResult(it[0].user)
                    }
                }
    }

    sealed class RecentlyRejectedUserResult {
        class NoRecentlyRejectedUserResult : RecentlyRejectedUserResult()
        data class HasRejectedUserResult(val rejectedUser: NearbyUserData) : RecentlyRejectedUserResult()
        data class HasRevertedUserResult(val revertedUser: NearbyUserData) : RecentlyRejectedUserResult()
    }
}
