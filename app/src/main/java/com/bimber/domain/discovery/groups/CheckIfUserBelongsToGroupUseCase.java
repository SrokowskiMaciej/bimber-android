package com.bimber.domain.discovery.groups;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.domain.model.GroupData;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by maciek on 22.06.17.
 */
@Singleton
public class CheckIfUserBelongsToGroupUseCase {

    @Inject
    public CheckIfUserBelongsToGroupUseCase() {
        super();
    }

    public Single<Boolean> checkIfUserBelongsToGroup(String currentUserId, GroupData publicGroupData) {
        return Observable.fromIterable(publicGroupData.membersData())
                .filter(chatMemberProfileData -> chatMemberProfileData.profileData().user.uId.equals(currentUserId))
                .filter(chatMemberProfileData -> chatMemberProfileData.chatMembership().membershipStatus()
                        == ChatVisibleChatMembership.MembershipStatus.ACTIVE)
                .isEmpty()
                .map(userDoesntBelongToGroup -> !userDoesntBelongToGroup);
    }
}
