package com.bimber.domain.discovery.users;

import com.bimber.data.entities.PersonEvaluation.PersonEvaluationStatus;
import com.bimber.data.repositories.PersonEvaluationRepository;
import com.bimber.utils.rx.RxFirebaseUtils;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Maybe;
import timber.log.Timber;

/**
 * Created by maciek on 22.06.17.
 */

@Singleton
public class GetMatchChatIdUseCase {

    private final PersonEvaluationRepository personEvaluationRepository;

    @Inject
    public GetMatchChatIdUseCase(PersonEvaluationRepository personEvaluationRepository) {
        this.personEvaluationRepository = personEvaluationRepository;
    }


    public Maybe<String> getMatchChatId(String currentUserId, String userToCheckId) {
        return personEvaluationRepository.evaluatedPerson(currentUserId, userToCheckId)
//                .compose(RxFirebaseUtils.logMaybe("getMatchChatId", true))
                .filter(personEvaluationStatus -> personEvaluationStatus.value().status() == PersonEvaluationStatus.MATCHED)
                .map(personEvaluationStatus -> personEvaluationStatus.value().dialogId())
                .doOnError(throwable -> Timber.e(throwable, "Error while fetching evaluation status for user with id %s." +
                        " Propagating error downstream to notify that this deeplink cannot be shown.", userToCheckId));
    }
}
