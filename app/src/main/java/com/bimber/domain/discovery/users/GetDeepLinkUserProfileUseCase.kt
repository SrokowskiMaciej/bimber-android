package com.bimber.domain.discovery.users

import com.bimber.data.sources.discovery.users.UserDeepLinkRepository
import com.bimber.domain.model.NearbyUserData
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 22.06.17.
 */

@Singleton
class GetDeepLinkUserProfileUseCase
@Inject
constructor(private val userDeepLinkRepository: UserDeepLinkRepository,
            private val profileDataRepository: ProfileDataRepository,
            private val getMatchChatIdUseCase: GetMatchChatIdUseCase) {

    fun onDeepLinkProfile(currentUserId: String): Flowable<DeepLinkUserResult> {
        return userDeepLinkRepository.onDeepLinkUser()
                .toFlowable(BackpressureStrategy.BUFFER)
                .switchMap {
                    when {
                        it.isEmpty() -> Flowable.just(DeepLinkUserResult.NoDeepLinkUserResult())
                        it[0] == currentUserId -> Flowable.just(DeepLinkUserResult.CurrentUserDeepLink())
                        else -> resolveUserProfile(currentUserId, it[0])
                                .startWith(DeepLinkUserResult.LoadingDeepLinkUserResult())
                                .onErrorReturn { error -> DeepLinkUserResult.ErrorDeepLinkUserResult(error) }
                    }
                }
                .doOnError { throwable -> Timber.e(throwable, "Error while fetching profile data from deeplink," + " propagating error downstream") }
    }

    private fun resolveUserProfile(currentUserId: String, deepLinkUserId: String): Flowable<DeepLinkUserResult> {
        return getMatchChatIdUseCase.getMatchChatId(currentUserId, deepLinkUserId)
                .map { DeepLinkUserResult.AlreadyMatchedUserDeepLink(it) as DeepLinkUserResult }
                .toFlowable()
                .switchIfEmpty(profileDataRepository.onUserProfileDataThumbnail(deepLinkUserId)
                        .map { NearbyUserData.create(it, null, null) }
                        .map { DeepLinkUserResult.SuccessDeepLinkUserResult(it) })
    }


    sealed class DeepLinkUserResult {
        class NoDeepLinkUserResult : DeepLinkUserResult()
        class CurrentUserDeepLink : DeepLinkUserResult()
        class LoadingDeepLinkUserResult : DeepLinkUserResult()
        data class AlreadyMatchedUserDeepLink(val dialogId: String) : DeepLinkUserResult()
        data class SuccessDeepLinkUserResult(val deepLinkUser: NearbyUserData) : DeepLinkUserResult()
        data class ErrorDeepLinkUserResult(val error: Throwable) : DeepLinkUserResult()
    }
}
