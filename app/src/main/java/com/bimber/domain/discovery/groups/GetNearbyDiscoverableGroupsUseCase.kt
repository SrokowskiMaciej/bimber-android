package com.bimber.domain.discovery.groups

import android.support.annotation.DrawableRes
import com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus
import com.bimber.data.entities.profile.network.UserLocationModel
import com.bimber.data.repositories.GroupJoinRequestRepository
import com.bimber.data.sources.discovery.groups.NearbyGroupsNetworkSource
import com.bimber.data.sources.discovery.groups.SkippedGroupsRepository
import com.bimber.data.sources.profile.location.UserLocationNetworkSource
import com.bimber.domain.group.GetGroupDataUseCase
import com.bimber.domain.model.GroupData
import com.bimber.features.discoverpeople.CardBackgroundRandomizer
import com.bimber.utils.rx.aggregate
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by maciek on 28.03.17.
 */
@Singleton
class GetNearbyDiscoverableGroupsUseCase
@Inject
constructor(private val nearbyGroupsNetworkSource: NearbyGroupsNetworkSource,
            private val skippedGroupsRepository: SkippedGroupsRepository,
            private val getGroupDataUseCase: GetGroupDataUseCase,
            private val groupJoinRequestRepository: GroupJoinRequestRepository,
            private val userLocationNetworkSource: UserLocationNetworkSource) {

    fun onDiscoverableGroups(currentUserId: String): Flowable<NearbyGroupsResult> {
        return userLocationNetworkSource.onLocationEvents(currentUserId)
                .switchMap { locations ->
                    if (locations.isEmpty()) {
                        Flowable.just(NearbyGroupsResult.NoLocationChosen())
                    } else {
                        onNearbyGroups(currentUserId, locations.get(0))
                                .map { nearbyUsers -> NearbyGroupsResult.Success(nearbyUsers) }
                    }
                }
    }

    private fun onNearbyGroups(currentUserId: String, userLocationModel: UserLocationModel): Flowable<List<NearbyGroupData>> {
        skippedGroupsRepository.clear()
        val nearbyDiscoverableGroups = nearbyGroupsNetworkSource.onGeolocationInRange(userLocationModel.latitude(), userLocationModel.longitude(), userLocationModel.range())
                .aggregate({ it }, { onDiscoverability(currentUserId, it) })
                .map { it.filter { it is Discoverability.Discoverable }.map { it.key } }

        val skippedGroups = skippedGroupsRepository.onSkippedGroups()

        return Flowable.combineLatest(nearbyDiscoverableGroups, skippedGroups,
                BiFunction<List<String>, List<SkippedGroupsRepository.SkippedGroup>, List<SkippedGroupsRepository.SkippedGroup>> { nearby, skipped ->
                    val filteredNearby = nearby.filter { nearbyGroup -> !skipped.any { it.groupId == nearbyGroup } }
                            .map { SkippedGroupsRepository.SkippedGroup(it, 0) }
                    filteredNearby + skipped
                })
                .aggregate({ it.groupId }, { onGroupData(it, userLocationModel) }, postMapping = { skippedGroup, result -> NearbyGroupData(result, userLocationModel, skippedGroup.skipCounter) })
    }

    private fun onGroupData(groupId: String, referenceUserLocationModel: UserLocationModel): Flowable<GroupData> {
        return getGroupDataUseCase.onGroupDataOptimized(groupId, 3)
                .flatMap { Flowable.fromIterable(it) }
                .filter { it.group().value().membersCount() > 0 }
                .distinctUntilChanged()
                .doOnError { throwable -> Timber.e(throwable, "Error while fetching groupId data for groupId: %s", groupId) }
                .onErrorResumeNext(Flowable.empty())
    }

    private fun onDiscoverability(currentUserId: String, groupId: String): Flowable<Discoverability> {
        return groupJoinRequestRepository.onGroupJoinRequestEvents(groupId, currentUserId)
                .map {
                    if (it.value().status() == GroupJoinStatus.NONE) {
                        Discoverability.Discoverable(groupId)
                    } else {
                        Discoverability.NonDiscoverable(groupId)
                    }
                }
                .distinctUntilChanged()
                .doOnError { throwable -> Timber.e(throwable, "Error while fetching group data for group: %s", groupId) }
                .onErrorResumeNext(Flowable.empty())

    }

    sealed class NearbyGroupsResult {
        class NoLocationChosen : NearbyGroupsResult()
        data class Success(val nearbyGroups: List<NearbyGroupData>) : NearbyGroupsResult();
    }

    data class NearbyGroupData(val groupData: GroupData, val referenceLocation: UserLocationModel?, val skipCounter: Int) {
        @DrawableRes
        val backgroundResource: Int = CardBackgroundRandomizer.randomizeCardBackground()

        val skippedGroupId: Long = (groupData.group().key().hashCode() + skipCounter).toLong()
    }

    private sealed class Discoverability(open val key: String) {
        data class Discoverable(override val key: String) : Discoverability(key)
        data class NonDiscoverable(override val key: String) : Discoverability(key)
    }
}

