package com.bimber.domain.discovery.groups

import com.bimber.data.sources.discovery.groups.GroupDeepLinkRepository
import com.bimber.domain.discovery.groups.GetNearbyDiscoverableGroupsUseCase.NearbyGroupData
import com.bimber.domain.group.GetGroupDataUseCase

import javax.inject.Inject
import javax.inject.Singleton

import timber.log.Timber

import io.reactivex.*

/**
 * Created by maciek on 22.06.17.
 */
@Singleton
class GetDeepLinkGroupDataUseCase @Inject
constructor(private val groupDeepLinkRepository: GroupDeepLinkRepository,
            private val getGroupDataUseCase: GetGroupDataUseCase,
            private val checkIfUserBelongsToGroupUseCase: CheckIfUserBelongsToGroupUseCase) {


    fun onDeepLinkGroup(currentUserId: String): Flowable<DeepLinkGroupResult> {
        return groupDeepLinkRepository.onDeepLinkGroupId()
                .toFlowable(BackpressureStrategy.BUFFER)
                .switchMap {
                    when {
                        it.isEmpty() -> Flowable.just(DeepLinkGroupResult.NoDeepLinkGroupResult())
                        else -> resolveGroupData(currentUserId, it[0])
                                .startWith(DeepLinkGroupResult.LoadingDeepLinkGroupResult())
                                .onErrorReturn { error -> DeepLinkGroupResult.ErrorDeepLinkGroupResult(error) }
                    }
                }
                .doOnError { throwable -> Timber.e(throwable, "Error while fetching profile data from deeplink," + " propagating error downstream") }
    }

    fun resolveGroupData(currentUserId: String, groupId: String): Flowable<DeepLinkGroupResult> {
        return getGroupDataUseCase.onGroupDataOptimized(groupId)
                .switchMap { groupDataList ->
                    Observable.fromIterable(groupDataList)
                            .flatMapSingle { groupData ->
                                checkIfUserBelongsToGroupUseCase.checkIfUserBelongsToGroup(currentUserId, groupData)
                                        .map {
                                            if (it) {
                                                DeepLinkGroupResult.AlreadyGroupMemberDeepLinkResult(groupId)
                                            } else {
                                                DeepLinkGroupResult.SuccessDeepLinkGroupResult(NearbyGroupData(groupData, null, 0))
                                            }
                                        }
                            }
                            .single(DeepLinkGroupResult.ErrorDeepLinkGroupResult(IllegalArgumentException("No such group")))
                            .toFlowable()

                }
                .doOnError({ throwable -> Timber.e(throwable, "Error while fetching group data from deeplink, propagating error downstream") })
    }

    sealed class DeepLinkGroupResult {
        class NoDeepLinkGroupResult : DeepLinkGroupResult()
        class LoadingDeepLinkGroupResult : DeepLinkGroupResult()
        data class AlreadyGroupMemberDeepLinkResult(val groupId: String) : DeepLinkGroupResult()
        data class SuccessDeepLinkGroupResult(val deepLinkGroup: NearbyGroupData) : DeepLinkGroupResult()
        data class ErrorDeepLinkGroupResult(val error: Throwable) : DeepLinkGroupResult()
    }

}
