package com.bimber.domain.groupjoin;

import com.bimber.data.entities.Chattable;
import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.chat.membership.ChatMembership.MembershipStatus;
import com.bimber.data.repositories.GroupJoinRequestRepository;
import com.bimber.data.repositories.UserChatsRepository;
import com.bimber.domain.chats.group.GetUserGroupChatsDataUseCase;
import com.bimber.domain.model.GroupChatData;
import com.bimber.domain.model.GroupJoinCandidates;
import com.bimber.domain.model.GroupJoinCandidatesThumbnail;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import timber.log.Timber;


/**
 * Created by maciek on 31.03.17.
 */
@Singleton
public class GetUserGroupsJoinCandidatesUseCase {

    private static final Function<Object[], List<GroupJoinCandidates>> GROUP_JOIN_REQUESTS_COMBINER = args -> {
        List<GroupJoinCandidates> userGroupsJoinRequests = new ArrayList<>(args.length);
        for (Object groupJoinRequestsObject : args) {
            GroupJoinCandidates groupJoinCandidates = (GroupJoinCandidates) groupJoinRequestsObject;
            //We don't want group join requests with no candidates
            if (!groupJoinCandidates.joinCandidates().isEmpty()) {
                userGroupsJoinRequests.add(groupJoinCandidates);
            }
        }
        return userGroupsJoinRequests;
    };

    private static final Function<Object[], List<GroupJoinCandidatesThumbnail>> USER_GROUPS_JOIN_REQUESTS_THUMBNAIL_COMBINER = objects -> {
        List<GroupJoinCandidatesThumbnail> joinCandidatesThumbnails = new ArrayList<>();
        for (Object groupJoinCandidatesObject : objects) {
            GroupJoinCandidatesThumbnail groupJoinCandidates = (GroupJoinCandidatesThumbnail) groupJoinCandidatesObject;
            if (!groupJoinCandidates.joinCandidates().isEmpty()) {
                joinCandidatesThumbnails.add(groupJoinCandidates);
            }
        }
        return joinCandidatesThumbnails;
    };
    private final UserChatsRepository userChatsRepository;
    private final GetUserGroupChatsDataUseCase getUserGroupChatsDataUseCase;
    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final ProfileDataRepository profileDataRepository;

    @Inject
    public GetUserGroupsJoinCandidatesUseCase(UserChatsRepository userChatsRepository, GetUserGroupChatsDataUseCase getUserGroupChatsDataUseCase,
                                              GroupJoinRequestRepository
                                                      groupJoinRequestRepository, ProfileDataRepository profileDataRepository) {
        this.userChatsRepository = userChatsRepository;
        this.getUserGroupChatsDataUseCase = getUserGroupChatsDataUseCase;
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.profileDataRepository = profileDataRepository;
    }

    public Flowable<List<GroupJoinCandidates>> onUserGroupsJoinCandidates(String uid) {
        return getUserGroupChatsDataUseCase.onUserGroupsEvents(uid)
                .switchMap(userGroups -> Flowable.combineLatestDelayError(createJoinRequestsMappings(userGroups),
                        GROUP_JOIN_REQUESTS_COMBINER)
                        .switchIfEmpty(Flowable.just(Collections.emptyList())));
    }

    public Flowable<List<GroupJoinCandidatesThumbnail>> onUserGroupsJoinCandidatesThumbnails(String uid) {
        return userChatsRepository.onUserChatsEvents(uid, Chattable.ChatType.GROUP, MembershipStatus.ACTIVE)
                .switchMap(userVisibleChatMemberships -> Flowable.fromIterable(userVisibleChatMemberships)
                        .map(userVisibleChatMembership -> groupJoinRequestRepository.onGroupJoinRequestsEvents(userVisibleChatMembership
                                .chatId(), GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED)
                                .map(joinRequests -> GroupJoinCandidatesThumbnail.create(userVisibleChatMembership, joinRequests)))
                        .toList()
                        .flatMapPublisher(mappings -> Flowable.combineLatestDelayError(mappings,
                                USER_GROUPS_JOIN_REQUESTS_THUMBNAIL_COMBINER)));
    }

    //TODO Think of something smart. For now with every new data from upstream we are throwing all listeners away. We should reuse the
    // ones with matching keys or something
    private List<Flowable<GroupJoinCandidates>> createJoinRequestsMappings(List<GroupChatData> groupsData) {
        List<Flowable<GroupJoinCandidates>> groupsJoinRequestsSources = new ArrayList<>(groupsData.size());
        for (GroupChatData groupData : groupsData) {
            groupsJoinRequestsSources.add(groupJoinRequestRepository.onGroupJoinRequestsEvents(groupData.chat().key(),
                    GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED)
                    .doOnError(throwable -> Timber.e(throwable, "Error getting join requests for group %s." +
                            " Consuming error and returning empty list"))
                    .onErrorReturnItem(Collections.emptyList())
                    .flatMapSingle(pendingJoinRequests -> Flowable.fromIterable(pendingJoinRequests)
                            .flatMapMaybe(joinRequest -> profileDataRepository.userProfileDataThumbnail(joinRequest.key())
                                    .toMaybe()
                                    .doOnError(throwable -> Timber.e(throwable, "Error while getting profile data %s for join request for" +
                                                    " " +
                                                    "group %s. Consume error, this particular user won't be buttonVisible in result",
                                            joinRequest.value(), groupData.chat().key()))
                                    .onErrorResumeNext(Maybe.empty())
                            )
                            .toList()
                            .map(joinCandidates -> GroupJoinCandidates.create(groupData, joinCandidates))));
        }
        return groupsJoinRequestsSources;
    }

    //TODO Think of something smart. For now with every new data from upstream we are throwing all listeners away. We should reuse the
    // ones with matching keys or something
    private List<Flowable<Integer>> createJoinRequestsCountMappings(List<GroupChatData> userGroups) {
        List<Flowable<Integer>> groupsJoinRequestsSources = new ArrayList<>(userGroups.size());
        for (GroupChatData group : userGroups) {
            groupsJoinRequestsSources.add(groupJoinRequestRepository.onGroupJoinRequestsEvents(group.chat().key(),
                    GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED)
                    .doOnError(throwable -> Timber.e(throwable, "Error getting join requests for group %s." +
                            " Consuming error and returning empty list to count it as zero", group.chat().key()))
                    .onErrorReturnItem(Collections.emptyList())
                    .map(List::size));
        }
        return groupsJoinRequestsSources;
    }
}
