package com.bimber.domain.groupjoin;

import com.bimber.data.repositories.GroupJoinRequestRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;

import static com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus;


/**
 * Created by maciek on 28.03.17.
 */
@Singleton
public class RequestGroupJoinUseCase {

    private final GroupJoinRequestRepository groupJoinRequestRepository;

    @Inject
    public RequestGroupJoinUseCase(GroupJoinRequestRepository groupJoinRequestRepository) {
        this.groupJoinRequestRepository = groupJoinRequestRepository;
    }

    public Completable likeGroup(String currentUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, currentUserId, GroupJoinStatus.MEMBERSHIP_REQUESTED);
    }

    public Completable dislikeGroup(String currentUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, currentUserId, GroupJoinStatus.NOT_INTERESTED);
    }
}
