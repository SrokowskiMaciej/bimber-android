package com.bimber.domain.groupjoin;

import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupJoinRequestRepository;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.rx.RxFirebaseUtils;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by maciek on 09.05.17.
 */
@Singleton
public class EvaluateGroupCandidateUseCase {


    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final ChatMemberRepository chatMemberRepository;

    @Inject
    public EvaluateGroupCandidateUseCase(GroupJoinRequestRepository groupJoinRequestRepository, ChatMemberRepository chatMemberRepository) {
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.chatMemberRepository = chatMemberRepository;
    }

    public Completable acceptGroupJoinRequest(String currentUserId, String candidateUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, candidateUserId, GroupJoinRequest.GroupJoinStatus
                .ACCEPTED)
                .toSingleDefault(RxFirebaseUtils.Event.INSTANCE)
                .toFlowable()
                .flatMap(evaluationResult -> chatMemberRepository.onChatMembershipEvents(groupId, candidateUserId))
                .flatMap(Flowable::fromIterable)
                .filter(chatMembership -> chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.ACTIVE)
                .take(1)
                .ignoreElements();
    }

    public Completable setInitialGroupJoinRequests(String currentUserId, String groupId, List<String> candidatesUids) {
        return Observable.fromIterable(candidatesUids)
                .map(uId -> Key.of(uId, GroupJoinRequest.GroupJoinStatus.INITIAL))
                .toList()
                .flatMapCompletable(groupJoinRequests -> groupJoinRequestRepository.setGroupJoinRequests(currentUserId, groupId,
                        groupJoinRequests));
    }

    public Completable rejectGroupJoinRequest(String currentUserId, String candidateUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, candidateUserId, GroupJoinRequest.GroupJoinStatus
                .REJECTED)
                .toSingleDefault(RxFirebaseUtils.Event.INSTANCE)
                .toFlowable()
                .flatMap(evaluationResult -> chatMemberRepository.onChatMembershipEvents(groupId, candidateUserId))
                .flatMap(Flowable::fromIterable)
                //Just to keep it the same as accept counterpart. REJECTED status doesn't mean that user wan't a part of a group
                // previously. He might have been rejected after leaving or being thrown out of a group.
                .filter(chatMembership -> true)
                .singleOrError()
                .toCompletable();
    }
}
