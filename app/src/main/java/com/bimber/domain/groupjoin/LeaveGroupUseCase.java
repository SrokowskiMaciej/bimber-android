package com.bimber.domain.groupjoin;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupJoinRequestRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;

import static com.bimber.data.entities.GroupJoinRequest.GroupJoinStatus;

/**
 * Created by maciek on 11.05.17.
 */
@Singleton
public class LeaveGroupUseCase {
    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final ChatMemberRepository chatMemberRepository;

    @Inject
    public LeaveGroupUseCase(GroupJoinRequestRepository groupJoinRequestRepository, ChatMemberRepository chatMemberRepository) {
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.chatMemberRepository = chatMemberRepository;
    }

    public Completable leaveGroup(String currentUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, currentUserId, GroupJoinStatus.LEFT)
                .andThen(chatMemberRepository.onChatMembershipEvents(groupId, currentUserId))
                .flatMap(Flowable::fromIterable)
                .filter(chatMembership -> chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.EXPIRED)
                .take(1)
                .ignoreElements();
    }
}
