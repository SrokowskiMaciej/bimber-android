package com.bimber.domain.groupjoin;

import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.repositories.GroupJoinRequestRepository;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import timber.log.Timber;


/**
 * Created by maciek on 31.03.17.
 */
@Singleton
public class GetGroupJoinCandidatesUseCase {

    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final ProfileDataRepository profileDataRepository;

    @Inject
    public GetGroupJoinCandidatesUseCase(GroupJoinRequestRepository groupJoinRequestRepository, ProfileDataRepository
            profileDataRepository) {
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.profileDataRepository = profileDataRepository;
    }

    public Flowable<List<ProfileDataThumbnail>> onGroupJoinCandidates(String groupId) {
        return groupJoinRequestRepository.onGroupJoinRequestsEvents(groupId, GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED)
                .flatMapSingle(pendingJoinRequests -> Flowable.fromIterable(pendingJoinRequests)
                        .flatMapMaybe(joinRequest -> profileDataRepository.userProfileDataThumbnail(joinRequest.key())
                                .toMaybe()
                                .doOnError(throwable -> Timber.e(throwable, "Error while getting profile data %s for join request for " +
                                        "group %s. Consume error, this particular user won't be buttonVisible in result", joinRequest
                                        .value(), groupId))
                                .onErrorResumeNext(Maybe.empty())
                        )
                        .toList())
                .doOnError(throwable -> Timber.e(throwable, "Error getting join requests for group %s." +
                        " Propagating error"));
    }

    public Flowable<Integer> onGroupJoinRequestsCount(String groupId) {
        return groupJoinRequestRepository.onGroupJoinRequestsEvents(groupId, GroupJoinRequest.GroupJoinStatus.MEMBERSHIP_REQUESTED)
                .map(List::size)
                .doOnError(throwable -> Timber.e(throwable, "Error getting join requests countfor group %s. Propagating error"));
    }
}
