package com.bimber.domain.groupjoin;

import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.GroupJoinRequestRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * Created by maciek on 11.05.17.
 */
@Singleton
public class RemoveUserFromGroupUseCase {
    private final GroupJoinRequestRepository groupJoinRequestRepository;
    private final ChatMemberRepository chatMemberRepository;

    @Inject
    public RemoveUserFromGroupUseCase(GroupJoinRequestRepository groupJoinRequestRepository, ChatMemberRepository chatMemberRepository) {
        this.groupJoinRequestRepository = groupJoinRequestRepository;
        this.chatMemberRepository = chatMemberRepository;
    }

    public Completable removeUserFromGroup(String currentUserId, String removedUserId, String groupId) {
        return groupJoinRequestRepository.setGroupJoinRequest(currentUserId, groupId, removedUserId, GroupJoinRequest.GroupJoinStatus
                .REMOVED)
                .andThen(chatMemberRepository.onChatMembershipEvents(groupId, removedUserId))
                .flatMap(Flowable::fromIterable)
                .filter(chatMembership -> chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.EXPIRED)
                .take(1)
                .ignoreElements();
    }
}
