package com.bimber.domain.model;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

import java.io.Serializable;

/**
 * Created by maciek on 31.03.17.
 */
@AutoValue
public abstract class ChatMemberData implements Serializable {

    public abstract ChatVisibleChatMembership chatMembership();

    public abstract ProfileDataThumbnail profileData();

    public static ChatMemberData create(ChatVisibleChatMembership chatMembership, ProfileDataThumbnail profileData) {
        return builder()
                .chatMembership(chatMembership)
                .profileData(profileData)
                .build();
    }

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_ChatMemberData.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder chatMembership(ChatVisibleChatMembership chatMembership);

        public abstract Builder profileData(ProfileDataThumbnail profileData);

        public abstract ChatMemberData build();
    }
}
