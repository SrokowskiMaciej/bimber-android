package com.bimber.domain.model;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.Group;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.images.BitmapUtils;
import com.google.auto.value.AutoValue;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Created by maciek on 22.02.17.
 */
@AutoValue
public abstract class GroupChatData implements ChatData<Group, ChatMemberData> {

    public static GroupChatData create(UserVisibleChatMembership currentUserChatMembership, Key<Group> group, List<ChatMemberData>
            membersData) {
        return new AutoValue_GroupChatData(ChatType.GROUP, currentUserChatMembership, group, membersData);
    }

    @Override
    public Observable<String> photosUris() {
        return Maybe.fromCallable(() -> chat().value().image())
                .toObservable()
                .switchIfEmpty(photosUrisByLastMessage().take(3));
    }

    public Observable<BitmapUtils.ThumbnailedPhoto> photos() {
        if (chat().value().image() != null) {
            return Observable.just(BitmapUtils.ThumbnailedPhoto.create(chat().value().image(), null));
        } else {
            return photosByLastMessage().take(3);
        }
    }

    private Observable<String> photosUrisByLastMessage() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> chatMemberProfileData.profileData().photo.userPhotoUri);
    }

    private Observable<BitmapUtils.ThumbnailedPhoto> photosByLastMessage() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> BitmapUtils.ThumbnailedPhoto.create(
                        chatMemberProfileData.profileData().photo.userPhotoUri,
                        chatMemberProfileData.profileData().photo.userPhotoThumb));
    }
}
