package com.bimber.domain.model;

import com.bimber.data.entities.chat.messages.local.Message;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 18.07.17.
 */
@AutoValue
public abstract class AggregatedMessage {
    public abstract Message message();

    public abstract MessagesGroupPosition groupPosition();

    public abstract ProfileDataThumbnail sendingUserProfile();

    public static AggregatedMessage create(Message message, MessagesGroupPosition groupPosition, ProfileDataThumbnail
            sendingUserProfile) {
        return new AutoValue_AggregatedMessage(message, groupPosition, sendingUserProfile);
    }

    public enum MessagesGroupPosition {
        NON_AGGREGABLE,
        SINGLE,
        LEADING,
        MIDDLE,
        TRAILING;

        public static MessagesGroupPosition get(boolean leadingInGroup, boolean trailingInGroup) {
            if (leadingInGroup && trailingInGroup) return SINGLE;
            if (leadingInGroup) return LEADING;
            if (trailingInGroup) return TRAILING;
            return MIDDLE;
        }
    }
}
