package com.bimber.domain.model;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

import java.util.List;

/**
 * Created by maciek on 31.03.17.
 */
@AutoValue
public abstract class GroupJoinCandidates {

    public static GroupJoinCandidates create(GroupChatData groupData, List<ProfileDataThumbnail> pendingJoinRequests) {
        return new AutoValue_GroupJoinCandidates(groupData, pendingJoinRequests);
    }

    public abstract GroupChatData groupData();

    public abstract List<ProfileDataThumbnail> joinCandidates();
}
