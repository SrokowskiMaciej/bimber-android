package com.bimber.domain.model;

import com.bimber.data.entities.PersonEvaluation;
import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 31.03.17.
 */
@AutoValue
public abstract class ChatMemberDataEvaluated {

    public abstract ChatVisibleChatMembership chatMembership();

    public abstract ProfileDataThumbnail profileData();

    public abstract PersonEvaluation personEvaluation();

    public static ChatMemberDataEvaluated create(ChatVisibleChatMembership chatMembership, ProfileDataThumbnail profileData, PersonEvaluation personEvaluation) {
        return builder()
                .chatMembership(chatMembership)
                .profileData(profileData)
                .personEvaluation(personEvaluation)
                .build();
    }

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_ChatMemberDataEvaluated.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder chatMembership(ChatVisibleChatMembership chatMembership);

        public abstract Builder profileData(ProfileDataThumbnail profileData);

        public abstract Builder personEvaluation(PersonEvaluation personEvaluation);

        public abstract ChatMemberDataEvaluated build();
    }
}
