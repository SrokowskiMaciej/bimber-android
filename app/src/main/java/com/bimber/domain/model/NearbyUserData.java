package com.bimber.domain.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.data.entities.profile.network.UserLocationModel;
import com.bimber.features.discoverpeople.CardBackgroundRandomizer;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 24.03.17.
 */
@AutoValue
public abstract class NearbyUserData {

    public static NearbyUserData create(ProfileDataThumbnail profileData, @Nullable UserLocationModel userUserLocationModel,
                                        @Nullable UserLocationModel referenceUserLocationModel) {
        return new AutoValue_NearbyUserData(profileData, userUserLocationModel, referenceUserLocationModel, CardBackgroundRandomizer.randomizeCardBackground());
    }

    public String key() {
        return profileData().user.uId;
    }

    public abstract ProfileDataThumbnail profileData();

    @Nullable
    public abstract UserLocationModel userLocation();

    @Nullable
    public abstract UserLocationModel referenceLocation();

    @DrawableRes
    public abstract int backgroundResource();

}
