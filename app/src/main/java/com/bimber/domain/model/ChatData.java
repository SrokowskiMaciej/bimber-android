package com.bimber.domain.model;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.utils.firebase.Key;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by maciek on 18.06.17.
 */

public interface ChatData<T, D> {

    ChatType chatType();

    UserVisibleChatMembership currentUserChatMembership();

    Key<T> chat();

    List<D> membersData();

    Observable<String> photosUris();
}
