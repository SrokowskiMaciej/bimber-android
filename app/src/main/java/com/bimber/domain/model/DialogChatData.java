package com.bimber.domain.model;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.Dialog;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by maciek on 11.04.17.
 */
@AutoValue
public abstract class DialogChatData implements ChatData<Dialog, ChatMemberData> {

    public static DialogChatData create(UserVisibleChatMembership currentUserChatMembership, Key<Dialog> dialog, List<ChatMemberData> membersData) {
        return new AutoValue_DialogChatData(ChatType.DIALOG, currentUserChatMembership, dialog, membersData);
    }

    @Override
    public Observable<String> photosUris() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> chatMemberProfileData.profileData().photo.userPhotoUri);
    }
}
