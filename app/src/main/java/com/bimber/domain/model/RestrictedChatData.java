package com.bimber.domain.model;

import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.data.entities.chat.messages.network.MessageModel;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.firebase.Timestamp;

import java.util.List;

/**
 * Created by maciek on 18.06.17.
 */

public interface RestrictedChatData {

    UserVisibleChatMembership currentUserChatMembership();

    List<Key<MessageModel>> lastMessages();

    List<Key<String>> currentUserLastSeenMessage();

    Timestamp lastInteraction();
}
