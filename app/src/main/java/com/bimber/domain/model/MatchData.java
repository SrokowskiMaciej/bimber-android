package com.bimber.domain.model;

import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 13.02.17.
 */

@AutoValue
public abstract class MatchData {

    public static MatchData create(ProfileDataThumbnail matchingProfileDataThumbnail, ProfileDataThumbnail matchedProfileDataThumbnail) {
        return new AutoValue_MatchData(matchingProfileDataThumbnail, matchedProfileDataThumbnail);
    }

    public abstract ProfileDataThumbnail matchingUser();

    public abstract ProfileDataThumbnail matchedUser();
}
