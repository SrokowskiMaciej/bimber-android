package com.bimber.domain.model;

import com.bimber.data.entities.Group;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Created by maciek on 22.02.17.
 */
@AutoValue
public abstract class GroupDataWithEvaluations {

    public static GroupDataWithEvaluations create(Key<Group> group, List<ChatMemberDataEvaluated> membersData) {
        return new AutoValue_GroupDataWithEvaluations(group, membersData);
    }

    public abstract Key<Group> chat();

    public abstract List<ChatMemberDataEvaluated> membersData();

    public final Observable<String> photosUris() {
        return Maybe.fromCallable(() -> chat().value().image())
                .toObservable()
                .switchIfEmpty(photosByLastMessage());
    }

    private Observable<String> photosByLastMessage() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> chatMemberProfileData.profileData().photo.userPhotoUri);
    }
}
