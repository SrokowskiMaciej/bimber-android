package com.bimber.domain.model;

import com.bimber.data.entities.GroupJoinRequest;
import com.bimber.data.entities.chat.membership.UserVisibleChatMembership;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;

import java.util.List;

/**
 * Created by maciek on 31.03.17.
 */
@AutoValue
public abstract class GroupJoinCandidatesThumbnail {

    public static GroupJoinCandidatesThumbnail create(UserVisibleChatMembership chatMembership, List<Key<GroupJoinRequest>>
            joinCandidates) {
        return new AutoValue_GroupJoinCandidatesThumbnail(chatMembership, joinCandidates);
    }

    public abstract UserVisibleChatMembership chatMembership();

    public abstract List<Key<GroupJoinRequest>> joinCandidates();
}
