package com.bimber.domain.model;

import com.bimber.data.entities.Group;
import com.bimber.utils.firebase.Key;
import com.bimber.utils.images.BitmapUtils.ThumbnailedPhoto;
import com.google.auto.value.AutoValue;

import java.io.Serializable;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Created by maciek on 22.02.17.
 */
@AutoValue
public abstract class GroupData implements Serializable {

    public static GroupData create(Key<Group> group, List<ChatMemberData> membersData) {
        return new AutoValue_GroupData(group, membersData);
    }

    public abstract Key<Group> group();

    public abstract List<ChatMemberData> membersData();

    public Observable<ThumbnailedPhoto> photos() {
        if (group().value().image() != null) {
            return Observable.just(ThumbnailedPhoto.create(group().value().image(), null));
        } else {
            return photosByLastMessage().take(3);
        }
    }

    public Observable<String> photosUris() {
        return Maybe.fromCallable(() -> group().value().image())
                .toObservable()
                .switchIfEmpty(photosUrisByLastMessage().take(3));
    }

    private Observable<ThumbnailedPhoto> photosByLastMessage() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> ThumbnailedPhoto.create(
                        chatMemberProfileData.profileData().photo.userPhotoUri,
                        chatMemberProfileData.profileData().photo.userPhotoThumb));
    }


    private Observable<String> photosUrisByLastMessage() {
        return Observable.fromIterable(membersData())
                .map(chatMemberProfileData -> chatMemberProfileData.profileData().photo.userPhotoUri);
    }
}
