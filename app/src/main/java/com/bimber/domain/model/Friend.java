package com.bimber.domain.model;

import com.bimber.data.entities.Chattable.ChatType;
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail;
import com.bimber.utils.firebase.Key;
import com.google.auto.value.AutoValue;

/**
 * Created by maciek on 16.03.17.
 */
@AutoValue
public abstract class Friend {

    public static Friend create(ProfileDataThumbnail profileData, Key<ChatType> friendshipOrigin, long lastInteraction) {
        return new AutoValue_Friend(profileData, friendshipOrigin, lastInteraction);
    }

    public abstract ProfileDataThumbnail profileData();

    public abstract Key<ChatType> friendshipOrigin();

    public abstract long lastInteraction();
}
