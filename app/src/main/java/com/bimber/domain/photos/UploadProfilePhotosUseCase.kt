package com.bimber.domain.photos

import android.net.Uri
import com.bimber.data.entities.UploadTaskStatus.TaskStatus.FINISHED
import com.bimber.data.entities.isDefault
import com.bimber.data.entities.profile.local.UserPhoto
import com.bimber.data.entities.profile.network.UploadStatus
import com.bimber.data.repositories.PhotoStorageRepository
import com.bimber.data.repositories.Subdirectory
import com.bimber.data.sources.profile.photos.UsersPhotosNetworkSource
import com.bimber.data.sources.profile.photos.UsersPhotosRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by maciek on 15.05.17.
 */
@Singleton
class UploadProfilePhotosUseCase @Inject
constructor(private val usersPhotosNetworkSource: UsersPhotosNetworkSource,
            private val usersPhotosRepository: UsersPhotosRepository,
            @param:Subdirectory(PROFILE_PHOTOS_STORAGE_SUBDIRECTORY)
            private val photoStorageRepository: PhotoStorageRepository) {

    private val pendingUploads = HashMap<String, UserPhoto>()
    private val pendingUploadsProcessor: BehaviorProcessor<List<UserPhoto>> = BehaviorProcessor.createDefault(emptyList())


    private fun addPendingUpload(photoId: String, pendingPhoto: UserPhoto) {
        pendingUploads[photoId] = pendingPhoto
        pendingUploadsProcessor.onNext(pendingUploads.toList().map { it.second })
    }

    private fun removePendingUpload(photoId: String) {
        pendingUploads.remove(photoId)
        pendingUploadsProcessor.onNext(pendingUploads.toList().map { it.second })
    }

    fun insertUserPhoto(uid: String, file: File) {
        val newPhotoId = usersPhotosNetworkSource.generatePhotoId(uid)
        val pendingPhoto = UserPhoto(uid, newPhotoId, 0, Uri.fromFile(file).toString(), null, UploadStatus.PENDING)
        addPendingUpload(newPhotoId, pendingPhoto)
        photoStorageRepository.insertPhoto(uid, newPhotoId, file)
                .filter { it.taskStatus() == FINISHED && it.uri() != null }
                .flatMapCompletable { finishedUploadTask ->
                    usersPhotosRepository.onAllUserPhotos(uid)
                            .firstOrError()
                            .map { photos -> photos.filter { !it.isDefault() } }
                            .reorderWithFirstIndexEmpty()
                            .flatMapCompletable { reorderedPhotos -> usersPhotosRepository.reorderPhotos(uid, reorderedPhotos) }
                            .andThen(usersPhotosRepository.insertUserPhoto(uid, newPhotoId, finishedUploadTask.uri().toString(), 0))

                }
                //LEAK IT, we don;t want to unsubscribe this task as long as the app lives
                .subscribeBy(onComplete = { removePendingUpload(newPhotoId) },
                        onError = { error ->
                            Timber.e(error, "Failed to upload message")
                            removePendingUpload(newPhotoId)
                        })
    }

    fun onUploadingPhotos(): Flowable<List<UserPhoto>> {
        return pendingUploadsProcessor
    }

    private fun Single<List<UserPhoto>>.reorderWithFirstIndexEmpty(): Single<HashMap<String, Int>> {
        return this.map {
            it.foldIndexed(HashMap<String, Int>()) { index, acc, photo ->
                acc.put(photo.photoId, index + 1)
                acc
            }
        }
    }

    companion object {

        const val PROFILE_PHOTOS_STORAGE_SUBDIRECTORY = "profiles"
    }
}
