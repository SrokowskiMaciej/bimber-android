package com.bimber.domain.photos

import com.bimber.data.entities.isDefault
import com.bimber.data.sources.profile.photos.UsersPhotosRepository
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by maciek on 15.05.17.
 */
@Singleton
class RemoveProfilePhotoUseCase @Inject
constructor(private val usersPhotosRepository: UsersPhotosRepository) {
    fun removeUserPhoto(uid: String, photoId: String): Completable {
        return usersPhotosRepository.onAllUserPhotos(uid)
                .firstOrError()
                .map { photos ->
                    photos.filter { !it.equals(photoId) }
                            .filter { !it.isDefault() }
                            .foldIndexed(HashMap<String, Int>()) { index, acc, photo ->
                                acc.put(photo.photoId, index)
                                acc
                            }
                }
                .flatMapCompletable { reorderedPhotos ->
                    usersPhotosRepository.removePhoto(uid, photoId)
                            .andThen(usersPhotosRepository.reorderPhotos(uid, reorderedPhotos))
                }
    }

}
