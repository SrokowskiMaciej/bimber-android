package com.bimber.domain.messages;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.chat.messages.common.image.ImageContent;
import com.bimber.data.entities.chat.messages.common.image.ImageContent.UploadStatus;
import com.bimber.data.entities.chat.messages.network.MessageModel;
import com.bimber.data.repositories.PhotoStorageRepository;
import com.bimber.data.repositories.Subdirectory;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.bimber.utils.firebase.Timestamp;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Created by maciek on 16.05.17.
 */

public class SendImageMessageUseCase {
    public static final String MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY = "messages";

    private final MessageRepository messageRepository;
    private final UsersRepository usersRepository;
    private final PhotoStorageRepository photoStorageRepository;

    @Inject
    public SendImageMessageUseCase(MessageRepository messageRepository, UsersRepository usersRepository, @Subdirectory(MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY)
            PhotoStorageRepository photoStorageRepository) {
        this.usersRepository = usersRepository;
        this.photoStorageRepository = photoStorageRepository;
        this.messageRepository = messageRepository;
    }

    public Flowable<UploadStatus> sendImageMessage(ChatVisibleChatMembership chatVisibleChatMembership, String
            currentUserId, File image, float
                                                           dimensionRatio) {
        return usersRepository.user(currentUserId)
                .map(user -> MessageModel.create(ImageContent.create(image.getPath(), dimensionRatio, UploadStatus.NOT_STARTED),
                        Timestamp.now(), currentUserId, user.displayName))
                .flatMap(imageMessage -> messageRepository.insertMessage(chatVisibleChatMembership, imageMessage))
                .flatMapPublisher(newMessageId -> photoStorageRepository.insertPhoto(chatVisibleChatMembership.chatId(), newMessageId,
                        image)
                        .flatMap(uploadTaskStatus -> {
                            String uri = uploadTaskStatus.uri() != null ? uploadTaskStatus.uri().toString() : null;
                            switch (uploadTaskStatus.taskStatus()) {
                                case PENDING:
                                    return messageRepository.updateImageMessageUploadStatus(chatVisibleChatMembership, newMessageId,
                                            UploadStatus.PENDING, uri)
                                            .andThen(Flowable.just(UploadStatus.PENDING));
                                case FINISHED:
                                    return messageRepository.updateImageMessageUploadStatus(chatVisibleChatMembership, newMessageId,
                                            UploadStatus.FINISHED, uri)
                                            .andThen(Flowable.just(UploadStatus.FINISHED));
                                default:
                                    throw new Error("We should never get here");
                            }
                        }))
                //Leaking on purpose here, we always want an upload to finish
                .publish()
                .autoConnect();
    }

}
