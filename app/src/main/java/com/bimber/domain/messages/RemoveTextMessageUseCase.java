package com.bimber.domain.messages;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.sources.chat.messages.MessageRepository;

import javax.inject.Inject;

import io.reactivex.Completable;


/**
 * Created by maciek on 16.05.17.
 */

public class RemoveTextMessageUseCase {

    private final MessageRepository messageRepository;

    @Inject
    public RemoveTextMessageUseCase(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Completable removeTextMessage(ChatVisibleChatMembership chatVisibleChatMembership, String messageId) {
        return messageRepository.removeMessage(chatVisibleChatMembership, messageId);
    }
}
