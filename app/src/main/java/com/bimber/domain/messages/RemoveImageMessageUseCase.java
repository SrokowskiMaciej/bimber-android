package com.bimber.domain.messages;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.repositories.PhotoStorageRepository;
import com.bimber.data.repositories.Subdirectory;

import javax.inject.Inject;

import io.reactivex.Completable;

import static com.bimber.domain.messages.SendImageMessageUseCase.MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY;

/**
 * Created by maciek on 16.05.17.
 */

public class RemoveImageMessageUseCase {

    private final MessageRepository messageRepository;
    private final PhotoStorageRepository photoStorageRepository;


    @Inject
    public RemoveImageMessageUseCase(MessageRepository messageRepository, @Subdirectory(MESSAGES_PHOTOS_STORAGE_SUBDIRECTORY)
            PhotoStorageRepository photoStorageRepository) {
        this.messageRepository = messageRepository;
        this.photoStorageRepository = photoStorageRepository;
    }

    public Completable removeImageMessage(ChatVisibleChatMembership chatVisibleChatMembership, String currentUserId,
                                          String messageId) {
        return messageRepository.removeMessage(chatVisibleChatMembership, messageId)
                .andThen(photoStorageRepository.deletePhoto(chatVisibleChatMembership.chatId(), messageId));
    }
}
