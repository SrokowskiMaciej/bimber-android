package com.bimber.domain.messages

import com.bimber.data.entities.chat.membership.ChatMembership
import com.bimber.data.entities.chat.messages.common.ContentType
import com.bimber.data.entities.chat.messages.common.image.ImageContent
import com.bimber.data.entities.chat.messages.local.Message
import com.bimber.data.sources.chat.messages.aggregated.MessagesWithSenderDataRepository
import com.bimber.domain.model.AggregatedMessage
import com.bimber.domain.model.AggregatedMessage.MessagesGroupPosition
import io.reactivex.Flowable
import io.reactivex.functions.BiConsumer
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by maciek on 18.07.17.
 */
@Singleton
class GetAggregatedMessagesUseCase @Inject
constructor(private val messagesWithSenderDataRepository: MessagesWithSenderDataRepository) {

    fun onAggregatedMessages(chatMembership: ChatMembership, messagesCount: Int): Flowable<List<AggregatedMessage>> {
        return messagesWithSenderDataRepository.onChatMessagesWithSender(chatMembership, messagesCount)
                .flatMapSingle { messages ->
                    Flowable.fromIterable(messages)
                            .filter { message -> !isUnfinishedIncomingImageMessage(chatMembership.uId(), message.message) }
                            .collectInto(arrayListOf<AggregatedMessage>(), BiConsumer { aggregatedMessages, messageWithSenderData ->
                                val message = messageWithSenderData.message
                                val sendingUser = messageWithSenderData.senderData

                                if (!GROUPABLE_CONTENT_TYPES.contains(message.getContentType())) {
                                    aggregatedMessages.add(AggregatedMessage.create(message, MessagesGroupPosition
                                            .NON_AGGREGABLE, sendingUser))
                                    return@BiConsumer
                                }
                                if (aggregatedMessages.isEmpty()) {
                                    aggregatedMessages.add(AggregatedMessage.create(message, MessagesGroupPosition.SINGLE,
                                            sendingUser))
                                    return@BiConsumer
                                }
                                val previousMessageIndex = aggregatedMessages.size - 1
                                val previousMessage = aggregatedMessages.get(previousMessageIndex)
                                if (previousMessage.groupPosition() == MessagesGroupPosition.NON_AGGREGABLE) {
                                    aggregatedMessages.add(AggregatedMessage.create(message, MessagesGroupPosition.SINGLE,
                                            sendingUser))
                                    return@BiConsumer
                                }

                                val messageGroupTimeElapsed = previousMessage.message().getTimestamp() < message.getTimestamp() - SHOW_MESSAGE_TIME_THRESHOLD_MILLIS

                                if (previousMessage.message().getUserId() == message.getUserId() && !messageGroupTimeElapsed) {
                                    val previousMessageChangedPosition = if (previousMessage.groupPosition() == MessagesGroupPosition
                                                    .SINGLE)
                                        MessagesGroupPosition.LEADING
                                    else if (previousMessage.groupPosition() == MessagesGroupPosition.TRAILING)
                                        MessagesGroupPosition.MIDDLE
                                    else
                                        MessagesGroupPosition.NON_AGGREGABLE

                                    aggregatedMessages.set(previousMessageIndex, AggregatedMessage.create(previousMessage.message(),
                                            previousMessageChangedPosition, sendingUser))
                                    aggregatedMessages.add(AggregatedMessage.create(message, MessagesGroupPosition.TRAILING,
                                            sendingUser))
                                } else {
                                    aggregatedMessages.add(AggregatedMessage.create(message, MessagesGroupPosition.SINGLE,
                                            sendingUser))
                                }
                            })
                }
    }

    private fun isUnfinishedIncomingImageMessage(currentUserId: String, message: Message): Boolean {
        return message.userId != currentUserId &&
                message.contentType == ContentType.IMAGE &&
                (message.content as ImageContent).uploadStatus() != ImageContent.UploadStatus.FINISHED
    }

    companion object {

        val SHOW_MESSAGE_TIME_THRESHOLD_MILLIS = 5 * 60 * 1000
        val GROUPABLE_CONTENT_TYPES = EnumSet.of(ContentType.IMAGE, ContentType.REMOVED, ContentType.TEXT,
                ContentType.UNKNOWN)
    }
}
