package com.bimber.domain.messages;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.entities.chat.messages.common.text.TextContent;
import com.bimber.data.entities.chat.messages.network.MessageModel;
import com.bimber.data.sources.chat.messages.MessageRepository;
import com.bimber.data.sources.profile.user.UsersRepository;
import com.bimber.utils.firebase.Timestamp;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by maciek on 16.05.17.
 */

public class SendTextMessageUseCase {

    private final MessageRepository messageRepository;
    private final UsersRepository usersRepository;

    @Inject
    public SendTextMessageUseCase(MessageRepository messageRepository, UsersRepository usersRepository) {
        this.messageRepository = messageRepository;
        this.usersRepository = usersRepository;
    }

    public Single<String> sendTextMessage(ChatVisibleChatMembership chatVisibleChatMembership, String currentUserId, String text) {
        return usersRepository.user(currentUserId)
                .map(user -> MessageModel.create(TextContent.create(text), Timestamp.now(), currentUserId, user.displayName))
                .flatMap(textMessage -> messageRepository.insertMessage(chatVisibleChatMembership, textMessage));
    }
}
