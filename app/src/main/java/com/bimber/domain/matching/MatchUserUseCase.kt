package com.bimber.domain.matching

import com.bimber.data.BackgroundSchedulerProvider
import com.bimber.data.entities.PersonEvaluation
import com.bimber.data.entities.PersonEvaluation.PersonEvaluationStatus
import com.bimber.data.entities.profile.local.aggregated.ProfileDataThumbnail
import com.bimber.data.repositories.PersonEvaluationRepository
import com.bimber.data.sources.discovery.users.NearbyDiscoverableUsersRepository
import com.bimber.data.sources.profile.aggregated.ProfileDataRepository
import com.bimber.domain.model.MatchData
import com.bimber.utils.firebase.Key

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

/**
 * Created by maciek on 11.05.17.
 */
@Singleton
class MatchUserUseCase @Inject
constructor(private val personEvaluationRepository: PersonEvaluationRepository,
            private val profileDataRepository: ProfileDataRepository,
            private val nearbyDiscoverableUsersRepository: NearbyDiscoverableUsersRepository,
            private val backgroundSchedulerProvider: BackgroundSchedulerProvider) {

    fun likeUser(currentUserId: String, evaluatedUserId: String): Maybe<MatchData> {

        shouldRequestMoreUsers(evaluatedUserId)
                .subscribeBy(onError = { throwable ->
                    Timber.e(throwable, "Failed to decide if more users should be requested")
                })

        return personEvaluationRepository.setEvaluatedPerson(currentUserId, evaluatedUserId, PersonEvaluationStatus.LIKED)
                .andThen<Key<PersonEvaluation>>(personEvaluationRepository.evaluatedPerson(evaluatedUserId, currentUserId))
                .filter { currentUserEvaluationStatus -> currentUserEvaluationStatus.value().status() == PersonEvaluationStatus.LIKED }
                .flatMap { _ ->
                    Single.zip<ProfileDataThumbnail, ProfileDataThumbnail, MatchData>(profileDataRepository.userProfileDataThumbnail(currentUserId),
                            profileDataRepository.userProfileDataThumbnail(evaluatedUserId), BiFunction<ProfileDataThumbnail, ProfileDataThumbnail, MatchData> { matchingProfileDataThumbnail, matchedProfileDataThumbnail -> MatchData.create(matchingProfileDataThumbnail, matchedProfileDataThumbnail) })
                            .doOnError { throwable -> Timber.e(throwable, "Error fetching data for matched user %s." + " Propagating errro downstream", evaluatedUserId) }
                            .toMaybe()
                }
    }

    fun dislikeUser(currentUserId: String, evaluatedUserId: String): Completable {
        shouldRequestMoreUsers(evaluatedUserId)
                .subscribeBy(onSuccess = { requestMoreUsers ->
                    if (requestMoreUsers) {
                        nearbyDiscoverableUsersRepository.requestMoreUsers()
                    }
                },
                        onError = { throwable ->
                            Timber.e(throwable, "Failed to decide if more users should be requested")
                        })

        return personEvaluationRepository.setEvaluatedPerson(currentUserId, evaluatedUserId, PersonEvaluationStatus.DISLIKED)
    }

    private fun shouldRequestMoreUsers(evaluatedUserId: String): Single<Boolean> {
        val remainingUsersCount = Single.fromCallable { nearbyDiscoverableUsersRepository.removeUser(evaluatedUserId) }
                .subscribeOn(backgroundSchedulerProvider.backgroundScheduler())
        val areUsersDepletedInThisLocation = nearbyDiscoverableUsersRepository.isDepleted().firstOrError()
        val lastRequest = nearbyDiscoverableUsersRepository.onNearbyDiscoverableUsers().firstOrError()

        return Single.zip<Int, Boolean, NearbyDiscoverableUsersRepository.Result, Boolean>(remainingUsersCount, areUsersDepletedInThisLocation, lastRequest, Function3 { nearbyUsers, isSourceDepleted, lastResult ->
            nearbyUsers < 5
                    && !isSourceDepleted
                    && lastResult is NearbyDiscoverableUsersRepository.Result.Success
        })
    }
}
