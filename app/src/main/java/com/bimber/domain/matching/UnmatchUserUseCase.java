package com.bimber.domain.matching;

import com.bimber.data.entities.chat.membership.ChatVisibleChatMembership;
import com.bimber.data.repositories.ChatMemberRepository;
import com.bimber.data.repositories.PersonEvaluationRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;

import static com.bimber.data.entities.PersonEvaluation.PersonEvaluationStatus;

/**
 * Created by maciek on 11.05.17.
 */
@Singleton
public class UnmatchUserUseCase {

    private final PersonEvaluationRepository personEvaluationRepository;
    private final ChatMemberRepository chatMemberRepository;

    @Inject
    public UnmatchUserUseCase(PersonEvaluationRepository personEvaluationRepository, ChatMemberRepository chatMemberRepository) {
        this.personEvaluationRepository = personEvaluationRepository;
        this.chatMemberRepository = chatMemberRepository;
    }

    public Completable unmatchUserFromDialog(String currentUserId, String chatId) {
        return chatMemberRepository.chatMemberships(chatId)
                .flatMapPublisher(Flowable::fromIterable)
                //There should be only one such membership. The one of the user we want to unmatch
                .filter(chatMembership -> !chatMembership.uId().equals(currentUserId))
                .singleOrError()
                .flatMapCompletable(chatMembership -> personEvaluationRepository.setEvaluatedPerson(currentUserId, chatMembership.uId(),
                        PersonEvaluationStatus.DISLIKED))
                .andThen(chatMemberRepository.onChatMembershipEvents(chatId, currentUserId))
                .flatMap(Flowable::fromIterable)
                .filter(chatMembership -> chatMembership.membershipStatus() == ChatVisibleChatMembership.MembershipStatus.EXPIRED)
                .take(1)
                .ignoreElements();
    }
}
