package com.bimber.utils.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;

/**
 * Created by srokowski.maciej@gmail.com on 16.11.16.
 */

public class ViewIdRestorer {

    private static final String TAG_ID_MAP_KEY = "TAG_ID_MAP_KEY";

    private HashMap<String, Integer> tagIdMap = new HashMap<>();

    public int getId(String tag) {
        if (tagIdMap.containsKey(tag)) {
            return tagIdMap.get(tag);
        } else {
            final int viewId = ViewIdGenerator.generateViewId();
            tagIdMap.put(tag, viewId);
            return viewId;
        }
    }

    public boolean hasTag(String tag) {
        return tagIdMap.containsKey(tag);
    }


    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            final HashMap<String, Integer> savedTagIdMap = (HashMap<String, Integer>) savedInstanceState.getSerializable(TAG_ID_MAP_KEY);
            tagIdMap.putAll(savedTagIdMap);
        }
    }

    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        final HashMap<String, Integer> savedTagIdMap = (HashMap<String, Integer>) savedInstanceState.getSerializable(TAG_ID_MAP_KEY);
        tagIdMap.putAll(savedTagIdMap);
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(TAG_ID_MAP_KEY, tagIdMap);
    }


}
