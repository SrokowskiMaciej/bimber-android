package com.bimber.utils.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by maciek on 12.07.17.
 */

public class TouchConsumerView extends View {


    public TouchConsumerView(Context context) {
        super(context);
    }

    public TouchConsumerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
