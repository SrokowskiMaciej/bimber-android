package com.bimber.utils.view;

import android.os.Bundle;
import android.os.Parcelable;

/**
 * Created by srokowski.maciej@gmail.com on 09.12.16.
 */

public class ViewSuperStateSaver {

    private static final String SUPER_STATE = "SUPER_STATE";

    public static Bundle packStateIntoBundle(Parcelable state) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SUPER_STATE, state);
        return bundle;
    }

    public static Bundle unpackBundle(Parcelable state) {
        if (state instanceof Bundle) {
            return (Bundle) state;
        } else {
            throw new IllegalStateException("State was not saved as a Bundle");
        }
    }

    public static Parcelable unpackStateFromBundleBundle(Bundle bundle) {
        return bundle.getParcelable(SUPER_STATE);
    }
}
