package com.bimber.utils.view.recyclerviewpager;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by maciek on 27.03.17.
 */

public abstract class PagerSnapChangeListener extends RecyclerView.OnScrollListener implements OnPageChangeListener {

    private int currentPage;

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch (newState) {
            case RecyclerView.SCROLL_STATE_DRAGGING:
                onPageScrollStateChanged(RecyclerView.SCROLL_STATE_DRAGGING);
                break;
            case RecyclerView.SCROLL_STATE_IDLE:
                onPageScrollStateChanged(RecyclerView.SCROLL_STATE_IDLE);
                int firstCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
                        .findFirstCompletelyVisibleItemPosition();
                if (firstCompletelyVisibleItemPosition != RecyclerView.NO_POSITION) {
                    currentPage = firstCompletelyVisibleItemPosition;
                    onPageSelected(currentPage);
                }
                break;
            case RecyclerView.SCROLL_STATE_SETTLING:
                onPageScrollStateChanged(RecyclerView.SCROLL_STATE_SETTLING);
                break;
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        int horizontalScrollExtent = recyclerView.computeHorizontalScrollExtent();
        int positionOffsetPixels = recyclerView.computeHorizontalScrollOffset() - currentPage * horizontalScrollExtent;
        float positionOffset = (float) positionOffsetPixels / horizontalScrollExtent;
        positionOffset = Math.min(positionOffset, 0.99f);
        onPageScrolled(currentPage, positionOffset, positionOffsetPixels);

    }

}
