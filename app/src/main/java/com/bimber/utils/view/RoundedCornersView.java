package com.bimber.utils.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by maciek on 17.05.17.
 */

public class RoundedCornersView extends FrameLayout {

    private Path path = new Path();
    private RectF rect = new RectF();
    private float[] corners = new float[8];

    public RoundedCornersView(@NonNull Context context) {
        super(context);
    }

    public RoundedCornersView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // compute the path
        path.reset();
        rect.set(0, 0, w, h);
        path.addRoundRect(rect, corners, Path.Direction.CW);
        path.close();

    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(path);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }


    public void setCorners(int cornerLeftTop, int cornerRightTop, int cornerRightBottom, int cornerLeftBottom) {
        corners[0] = cornerLeftTop;
        corners[1] = cornerLeftTop;
        corners[2] = cornerRightTop;
        corners[3] = cornerRightTop;
        corners[4] = cornerLeftBottom;
        corners[5] = cornerLeftBottom;
        corners[6] = cornerRightBottom;
        corners[7] = cornerRightBottom;
        path.reset();
        rect.set(0, 0, getWidth(), getHeight());
        path.addRoundRect(rect, corners, Path.Direction.CW);
        path.close();
        invalidate();
    }

}
