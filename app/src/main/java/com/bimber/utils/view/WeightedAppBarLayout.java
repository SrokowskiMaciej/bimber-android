package com.bimber.utils.view;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by maciek on 21.04.17.
 */
@CoordinatorLayout.DefaultBehavior(AppBarLayout.Behavior.class)
public class WeightedAppBarLayout extends AppBarLayout {
    private float weight = 0.8f;

    public WeightedAppBarLayout(Context context) {
        super(context);
    }

    public WeightedAppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int mode = MeasureSpec.getMode(widthMeasureSpec);
        int size = (int) (MeasureSpec.getSize(widthMeasureSpec) * weight);
        super.onMeasure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(size, mode));
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isExpanded() {
        return getHeight() - getBottom() == 0;
    }

    public void addSingleOnExpandedListener(OnExpandedListener onExpandedListener) {
        addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    removeOnOffsetChangedListener(this);
                    onExpandedListener.onExtended();
                }
            }
        });
    }

    public void expand(OnExpandedListener onExpandedListener) {
        if (isExpanded()) {
            onExpandedListener.onExtended();
        } else {
            addSingleOnExpandedListener(onExpandedListener);
            setExpanded(true);
        }
    }

    public interface OnExpandedListener {
        void onExtended();
    }
}
