package com.bimber.utils.view;

import android.support.v7.widget.RecyclerView;

/**
 * Created by maciek on 10.11.17.
 */

public abstract class RichOnScrollListener extends RecyclerView.OnScrollListener {

    private boolean isUserEvent;

    @Deprecated
    @Override
    final public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
            isUserEvent = true;
            onScrollStateChanged(recyclerView, newState, isUserEvent);
        } else if (isUserEvent && (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_SETTLING)) {
            onScrollStateChanged(recyclerView, newState, isUserEvent);
            isUserEvent = false;
        }

    }

    @Deprecated
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        onScrolled(recyclerView, dx, dy, isUserEvent);
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int newState, boolean isUserInitiated) {
    }

    public void onScrolled(RecyclerView recyclerView, int dx, int dy, boolean isUserInitiated) {
    }
}
