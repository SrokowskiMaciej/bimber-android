package com.bimber.utils.view;

/**
 * Created by maciek on 25.05.17.
 */

public enum SharedElementTransitionSource {
    NONE,
    FRAGMENT,
    ACTIVITY;

    public final static String SHARED_ELEMENT_TRANSITION_KEY = "SHARED_ELEMENT_TRANSITION_KEY";
}
