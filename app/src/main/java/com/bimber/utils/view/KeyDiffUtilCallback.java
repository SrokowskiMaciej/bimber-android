package com.bimber.utils.view;

import android.support.v7.util.DiffUtil;

import com.bimber.utils.firebase.Key;

import java.util.List;

/**
 * Created by maciek on 10.02.17.
 */

public class KeyDiffUtilCallback extends DiffUtil.Callback {

    private final List<? extends Key<?>> oldData;
    private final List<? extends Key<?>> newData;

    public KeyDiffUtilCallback(List<? extends Key<?>> oldData, List<? extends Key<?>> newData) {
        this.oldData = oldData;
        this.newData = newData;
    }

    @Override
    public int getOldListSize() {
        return oldData.size();
    }

    @Override
    public int getNewListSize() {
        return newData.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldData.get(oldItemPosition).key().equals(newData.get(newItemPosition).key());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldData.get(oldItemPosition).value().equals(newData.get(newItemPosition).value());
    }
}
