package com.bimber.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;

/**
 * Created by srokowski.maciej@gmail.com on 05.01.17.
 */

public class ShrinkableImageButton extends ImageButton {
    public ShrinkableImageButton(Context context) {
        super(context);
    }

    public ShrinkableImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            animate().scaleX(0.8f).setDuration(100).setInterpolator(new DecelerateInterpolator(1.0f)).start();
            animate().scaleY(0.8f).setDuration(100).setInterpolator(new DecelerateInterpolator(1.0f)).start();
        } else if (event.getAction() == MotionEvent.ACTION_UP ||
                event.getAction() == MotionEvent.ACTION_CANCEL ||
                event.getAction() == MotionEvent.ACTION_OUTSIDE ||
                event.getAction() == MotionEvent.ACTION_BUTTON_RELEASE) {
            animate().scaleX(1.0f).setDuration(100).setInterpolator(new OvershootInterpolator(3.0f)).start();
            animate().scaleY(1.0f).setDuration(100).setInterpolator(new OvershootInterpolator(3.0f)).start();
        }
        return super.onTouchEvent(event);
    }
}
