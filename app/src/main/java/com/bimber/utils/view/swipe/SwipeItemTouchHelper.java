package com.bimber.utils.view.swipe;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.OvershootInterpolator;

import com.bimber.R;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Maciek on 15/12/2017.
 */

public class SwipeItemTouchHelper extends RecyclerView.ItemDecoration
        implements RecyclerView.OnChildAttachStateChangeListener,
        RecyclerView.OnItemTouchListener {


    public static final SwipeDirectionsProvider LEFT_RIGHT_DIRECTIONS_PROVIDER = new LeftRightDirectionsProvider();
    public static final SwipeDirectionsProvider LEFT_RIGHT_BOTTOM_DIRECTIONS_PROVIDER = new LeftRightBottomDirectionsProvider();

    private static final int ACTIVE_POINTER_ID_NONE = -1;
    private static final int PIXELS_PER_SECOND = 1000;
    private static final int DEFAULT_SWIPE_DURATION = 1000;
    private static final int DEFAULT_SWIPE_LEFT_ROTATION = -60;
    private static final int DEFAULT_SWIPE_TOP_ROTATION = 0;
    private static final int DEFAULT_SWIPE_RIGHT_ROTATION = 60;
    private static final int DEFAULT_SWIPE_BOTTOM_ROTATION = 0;
    private static final float SWIPE_DISTANCE_THRESHOLD = 0.7f;


    private static final float MIN_SCALE = 0.85f;
    private static final float MAX_SCALE = 1.0f;

    private RecyclerView recyclerView;
    private RecyclerView.ViewHolder selectedItem;

    private float initialTouchX;
    private float initialTouchY;
    private float dX;
    private float dY;

    private int activePointerId = ACTIVE_POINTER_ID_NONE;

    private OnSwipeListener onSwipeListener;

    private SwipeDirectionsProvider swipeDirectionsProvider;

    private VelocityTracker velocityTracker;
    private float swipeEscapeVelocity;
    private float maxSwipeVelocity;

    private Map<Long, SwipeAnimation> animations = new HashMap<>();
    private Set<Long> swipedItemsIds = new HashSet<>();

    public SwipeItemTouchHelper(SwipeDirectionsProvider swipeDirectionsProvider) {
        this.swipeDirectionsProvider = swipeDirectionsProvider;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        View lastSwipedView = null;
        View firstNonSwipedView = null;
        for (int childPosition = parent.getChildCount() - 1; childPosition >= 0; childPosition--) {
            View view = parent.getChildAt(childPosition);
            RecyclerView.ViewHolder viewHolder = parent.getChildViewHolder(view);
            SwipeAnimation animation = animations.get(viewHolder.getItemId());
            if (selectedItem != null && selectedItem.itemView == view) {
                lastSwipedView = view;
                view.setTranslationX(dX);
                view.setTranslationY(dY);
                view.setRotation(getRotation(dX));
                signalSwipeProgress(viewHolder);
            } else if (animation != null) {
                lastSwipedView = view;
                animation.update();
                view.setTranslationX(animation.currentTranslationX);
                view.setTranslationY(animation.currentTranslationY);
                view.setRotation(animation.currentRotation);
                if (animation.ended) {
                    if (animation.swipingOut) {
                        if (onSwipeListener != null &&
                                !swipedItemsIds.contains(viewHolder.getItemId())) {
                            swipedItemsIds.add(viewHolder.getItemId());
                            onSwipeListener.onItemSwiped(viewHolder, getSwipeDirection(viewHolder));
                        }
                    } else {
                        animations.remove(viewHolder.getItemId());
                        if (onSwipeListener != null) {
                            onProgressMiddle(viewHolder);
                        }
                    }
                } else {
                    parent.invalidate();
                    if (onSwipeListener != null) {
                        signalSwipeProgress(viewHolder, animation.swipeDirection, animation
                                .currentProgress);
                    }
                }
            } else {
                view.setTranslationX(0.0f);
                view.setTranslationY(0.0f);
                view.setRotation(0.0f);
                if (firstNonSwipedView == null) {
                    firstNonSwipedView = view;
                    if (lastSwipedView == null) {
                        view.setScaleX(MAX_SCALE);
                        view.setScaleY(MAX_SCALE);
                    } else {
                        float progress = getProgress(lastSwipedView.getTranslationX(),
                                lastSwipedView.getTranslationY());
                        view.setScaleX(MIN_SCALE + (MAX_SCALE - MIN_SCALE) * progress);
                        view.setScaleY(MIN_SCALE + (MAX_SCALE - MIN_SCALE) * progress);
                    }
                } else {
                    view.setScaleX(MIN_SCALE);
                    view.setScaleY(MIN_SCALE);
                }
            }
        }
    }

    public void attachToRecyclerView(@Nullable RecyclerView recyclerView) {
        if (this.recyclerView == recyclerView) {
            return; // nothing to do
        }
        if (this.recyclerView != null) {
            this.recyclerView.requestDisallowInterceptTouchEvent(false);
            destroyCallbacks();
        }
        this.recyclerView = recyclerView;
        if (recyclerView != null) {
            this.recyclerView.requestDisallowInterceptTouchEvent(true);
            final Resources resources = recyclerView.getResources();
            swipeEscapeVelocity = resources.getDimension(R.dimen
                    .swipe_touch_helper_swipe_escape_velocity);
            maxSwipeVelocity = resources.getDimension(R.dimen
                    .swipe_touch_helper_swipe_escape_max_velocity);
            setupCallbacks();
        }
    }

    public void setOnSwipeListener(@Nullable OnSwipeListener onSwipeListener) {
        this.onSwipeListener = onSwipeListener;
    }

    public void swipe(SwipeDirection direction) {
        if (recyclerView == null || recyclerView.getAdapter() == null) {
            return;
        }

        if (!swipeDirectionsProvider.isAllowed(direction)) {
            return;
        }

        for (int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(i);
            if (viewHolder == null) {
                continue;
            }
            SwipeAnimation animation = animations.get(viewHolder.getItemId());
            if (animation != null) {
                if (animation.ended) {
                    //If animation is finished check next item
                    continue;
                } else {
                    //If animation is in progress don't swipe
                    break;
                }
            }

            float parentDiagonal = (float) Math.sqrt(recyclerView.getHeight() * recyclerView
                    .getHeight() + recyclerView.getWidth() * recyclerView.getWidth());
            switch (direction) {
                case LEFT:
                    startSwipe(viewHolder, direction, -parentDiagonal, 0.0f,
                            DEFAULT_SWIPE_LEFT_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
                case TOP:
                    startSwipe(viewHolder, direction, 0.0f, -parentDiagonal,
                            DEFAULT_SWIPE_TOP_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
                case RIGHT:
                    startSwipe(viewHolder, direction, parentDiagonal, 0.0f,
                            DEFAULT_SWIPE_RIGHT_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
                case BOTTOM:
                    startSwipe(viewHolder, direction, 0.0f, parentDiagonal,
                            DEFAULT_SWIPE_BOTTOM_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
            }
            break;

        }
    }

    public void revert(String itemId) {
        for (int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
            View child = recyclerView.getChildAt(i);
            long childHash = itemId.hashCode();
            if (recyclerView.getChildItemId(child) == childHash) {
                swipedItemsIds.remove(childHash);
                RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(child);
                startReturn(childViewHolder, getProgress(childViewHolder.itemView.getTranslationX(),
                        childViewHolder.itemView.getTranslationY()));
                break;
            }
        }
    }

    public void setSwipeDirectionsProvider(SwipeDirectionsProvider swipeDirectionsProvider) {
        this.swipeDirectionsProvider = swipeDirectionsProvider;
    }

    private void setupCallbacks() {
        recyclerView.addItemDecoration(this);
        recyclerView.addOnItemTouchListener(this);
        recyclerView.addOnChildAttachStateChangeListener(this);
    }

    private void destroyCallbacks() {
        recyclerView.removeItemDecoration(this);
        recyclerView.removeOnItemTouchListener(this);
        recyclerView.removeOnChildAttachStateChangeListener(this);
        releaseVelocityTracker();
    }


    private void obtainVelocityTracker() {
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        velocityTracker = VelocityTracker.obtain();
    }

    private void releaseVelocityTracker() {
        if (velocityTracker != null) {
            velocityTracker.recycle();
            velocityTracker = null;
        }
    }

    @Override
    public void onChildViewAttachedToWindow(View view) {
        RecyclerView.ViewHolder containingViewHolder = recyclerView.findContainingViewHolder(view);
        if (containingViewHolder != null) {
            int layoutPosition = containingViewHolder.getAdapterPosition();
            if (layoutPosition == 0) {
                view.setScaleY(1.0f);
                view.setScaleX(1.0f);
            } else {
                view.setScaleY(MIN_SCALE);
                view.setScaleX(MIN_SCALE);
            }
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            view.setRotationX(0.0f);
            onProgressMiddle(containingViewHolder);
        }
        recyclerView.invalidate();
    }

    @Override
    public void onChildViewDetachedFromWindow(View view) {
        RecyclerView.ViewHolder containingViewHolder = recyclerView.findContainingViewHolder(view);

        if (containingViewHolder != null && animations.containsKey(containingViewHolder.getItemId())) {
            long itemId = containingViewHolder.getItemId();
            SwipeAnimation animation = animations.get(itemId);
            animation.cancel();
            animations.remove(itemId);
            swipedItemsIds.remove(itemId);
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            view.setRotationX(0.0f);
            view.setScaleY(MIN_SCALE);
            view.setScaleX(MIN_SCALE);
        }
        recyclerView.invalidate();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent event) {
        if ((activePointerId != event.getPointerId(0)) &&
                (activePointerId != ACTIVE_POINTER_ID_NONE)) {
            return false;
        }
        final int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                View view = recyclerView.findChildViewUnder(event.getX(), event.getY());
                if (view != null) {
                    RecyclerView.ViewHolder containingViewHolder = recyclerView
                            .findContainingViewHolder(view);
                    initialTouchX = event.getX();
                    initialTouchY = event.getY();
                    obtainVelocityTracker();
                    activePointerId = event.getPointerId(0);
                    select(recyclerView, containingViewHolder);
                    //THIS IS EXTRA IMPORTANT onDraw can be called between onInterceptTouchEvent
                    // and onTouchEvent
                    updateDxDy(event);
                } else {
                    select(recyclerView, null);
                    activePointerId = ACTIVE_POINTER_ID_NONE;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (velocityTracker != null) {
                    velocityTracker.addMovement(event);
                }
                updateDxDy(event);
                this.recyclerView.invalidate();
                //All right, we are taking over, no more events for children views. Values taken from SimpleGestureDetector
                int touchSlop = ViewConfiguration.get(recyclerView.getContext()).getScaledTouchSlop();
                return dX * dX + dY * dY > touchSlop * touchSlop;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                activePointerId = ACTIVE_POINTER_ID_NONE;
                checkSwipeResult();
                if (velocityTracker != null) {
                    velocityTracker.clear();
                }
                select(recyclerView, null);
                this.recyclerView.invalidate();
                break;
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView recyclerView, MotionEvent event) {
        if ((activePointerId != event.getPointerId(0)) &&
                (activePointerId != ACTIVE_POINTER_ID_NONE)) {
            return;
        }

        final int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                if (velocityTracker != null) {
                    velocityTracker.addMovement(event);
                }
                updateDxDy(event);
                this.recyclerView.invalidate();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                activePointerId = ACTIVE_POINTER_ID_NONE;
                checkSwipeResult();
                if (velocityTracker != null) {
                    velocityTracker.clear();
                }
                select(recyclerView, null);
                this.recyclerView.invalidate();
                break;
        }
    }

    private float getRotation(float x) {
        float opposite = x;
        float adjacent = recyclerView.getHeight() * 2.0f;
        return (float) Math.toDegrees(Math.tan(opposite / adjacent));
    }

    private SwipeDirection getSwipeDirection(RecyclerView.ViewHolder viewHolder) {
        return getSwipeDirection(viewHolder.itemView.getTranslationX(), viewHolder.itemView.getTranslationY());
    }

    private SwipeDirection getSwipeDirection(float x, float y) {
        return this.swipeDirectionsProvider.getSwipeDirection(x, y);
    }

    private void signalSwipeProgress(RecyclerView.ViewHolder holder) {
        signalSwipeProgress(holder, getSwipeDirection(holder));
    }

    private void signalSwipeProgress(RecyclerView.ViewHolder holder, SwipeDirection
            swipeDirection) {
        switch (swipeDirection) {
            case LEFT:
            case RIGHT:
                signalSwipeProgress(holder, swipeDirection, getCurrentHorizontalProgress());
                break;
            case TOP:
            case BOTTOM:
                signalSwipeProgress(holder, swipeDirection, getCurrentVerticalProgress());
                break;
        }
    }

    private void signalSwipeProgress(RecyclerView.ViewHolder holder, SwipeDirection
            swipeDirection, float progress) {
        if (onSwipeListener != null && recyclerView != null) {
            switch (swipeDirection) {
                case LEFT:
                    onProgressLeft(holder, progress);
                    break;
                case TOP:
                    onProgressTop(holder, progress);
                    break;
                case RIGHT:
                    onProgressRight(holder, progress);
                    break;
                case BOTTOM:
                    onProgressBottom(holder, progress);
                    break;
            }
        }
    }


    private float getCurrentProgress() {
        return getProgress(dX, dY);
    }

    private float getProgress(float dX, float dY) {
        float parentHalfWidth = recyclerView.getWidth() / 2;
        float x = Math.abs(dX);
        float y = Math.abs(dY);
        ;
        return (float) Math.min(Math.sqrt(x * x + y * y) / parentHalfWidth, 1.0f);
    }

    private float getCurrentHorizontalProgress() {
        return getHorizontalProgress(dX);
    }

    private float getHorizontalProgress(float x) {
        float parentHalfWidth = recyclerView.getWidth() / 2;
        return Math.min(Math.abs(x) / parentHalfWidth, 1.0f);
    }

    private float getCurrentVerticalProgress() {
        return getVerticalProgress(dY);
    }

    private float getVerticalProgress(float y) {
        float parentHalfHeight = recyclerView.getHeight() / 2;
        return Math.min(Math.abs(y) / parentHalfHeight, 1.0f);
    }

    private void onProgressMiddle(RecyclerView.ViewHolder viewHolder) {
        if (recyclerView != null && onSwipeListener != null) {
            onSwipeListener.onSwipeProgress(viewHolder, 0.0f);
            onSwipeListener.onSwipeProgressLeft(viewHolder, 0.0f);
            onSwipeListener.onSwipeProgressTop(viewHolder, 0.0f);
            onSwipeListener.onSwipeProgressRight(viewHolder, 0.0f);
            onSwipeListener.onSwipeProgressBottom(viewHolder, 0.0f);
        }
    }

    private void onProgressTop(RecyclerView.ViewHolder viewHolder, float topProgress) {
        onSwipeListener.onSwipeProgress(viewHolder, getCurrentProgress());
        onSwipeListener.onSwipeProgressLeft(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressTop(viewHolder, topProgress);
        onSwipeListener.onSwipeProgressRight(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressBottom(viewHolder, 0.0f);
    }

    private void onProgressRight(RecyclerView.ViewHolder viewHolder, float rightProgress) {
        onSwipeListener.onSwipeProgress(viewHolder, getCurrentProgress());
        onSwipeListener.onSwipeProgressLeft(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressTop(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressRight(viewHolder, rightProgress);
        onSwipeListener.onSwipeProgressBottom(viewHolder, 0.0f);
    }

    private void onProgressLeft(RecyclerView.ViewHolder viewHolder, float leftProgress) {
        onSwipeListener.onSwipeProgress(viewHolder, getCurrentProgress());
        onSwipeListener.onSwipeProgressLeft(viewHolder, leftProgress);
        onSwipeListener.onSwipeProgressTop(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressRight(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressBottom(viewHolder, 0.0f);
    }

    private void onProgressBottom(RecyclerView.ViewHolder viewHolder, float bottomProgress) {
        onSwipeListener.onSwipeProgress(viewHolder, getCurrentProgress());
        onSwipeListener.onSwipeProgressLeft(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressTop(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressRight(viewHolder, 0.0f);
        onSwipeListener.onSwipeProgressBottom(viewHolder, bottomProgress);
    }

    private void checkSwipeResult() {
        if (onSwipeListener != null && recyclerView != null && selectedItem != null) {
            if (startSwipeIfVelocityReachesThreshold()) {
                return;
            } else if (startSwipeIfDistanceReachesThreshold()) {
                return;
            }
            float progress = getProgress(selectedItem.itemView.getTranslationX(),
                    selectedItem.itemView.getTranslationY());
            startReturn(selectedItem, progress);
        }
    }

    private boolean startSwipeIfVelocityReachesThreshold() {
        velocityTracker.computeCurrentVelocity(PIXELS_PER_SECOND, maxSwipeVelocity);
        float xVelocity = velocityTracker.getXVelocity();
        float yVelocity = velocityTracker.getYVelocity();

        double diagonalVelocity = Math.sqrt(xVelocity * xVelocity + yVelocity * yVelocity);
        double parentDiagonal = Math.sqrt(recyclerView.getHeight() * recyclerView.getHeight() +
                recyclerView.getWidth() * recyclerView.getWidth());

        double diagonalPathPredictedDuration = (parentDiagonal / diagonalVelocity);

        float translationXTarget = (float) (selectedItem.itemView.getTranslationX() + xVelocity *
                diagonalPathPredictedDuration);
        float translationYTarget = (float) (selectedItem.itemView.getTranslationY() + yVelocity *
                diagonalPathPredictedDuration);

        SwipeDirection velocityDirection = getSwipeDirection(xVelocity, yVelocity);
        SwipeDirection positionDirection = getSwipeDirection(selectedItem);

        if (!swipeDirectionsProvider.isAllowed(velocityDirection)) {
            return false;
        }

        if (velocityDirection == positionDirection && diagonalVelocity >= swipeEscapeVelocity) {
            float rotationAnimation = getRotation(translationXTarget);
//            Timber.d("maxSwipeVelocity: %s\n" +
//                    "swipeEscapeVelocity: %s\n" +
//                    "diagonalPathPredictedDuration: %s\n" +
//                    "xVelocity: %s\n" +
//                    "yVelocity: %s\n" +
//                    "translationXTarget: %s\n" +
//                    "translationYTarget: %s\n" +
//                    "rotationAnimation: %s", maxSwipeVelocity, swipeEscapeVelocity,
// diagonalPathPredictedDuration, xVelocity, yVelocity, translationXTarget, translationYTarget,
// rotationAnimation);

            float progress = getProgress(selectedItem.itemView.getTranslationX(),
                    selectedItem.itemView.getTranslationY());
            startSwipe(selectedItem, velocityDirection, translationXTarget, translationYTarget,
                    rotationAnimation, progress, DEFAULT_SWIPE_DURATION);
            return true;
        } else {
            return false;
        }
    }

    private boolean startSwipeIfDistanceReachesThreshold() {
        float translationX = selectedItem.itemView.getTranslationX();
        float translationY = selectedItem.itemView.getTranslationY();
        SwipeDirection direction = getSwipeDirection(translationX, translationY);

        if (!swipeDirectionsProvider.isAllowed(direction)) {
            return false;
        }

        boolean isDistanceThresholdReached = Math.abs(translationX) > recyclerView.getWidth() * SWIPE_DISTANCE_THRESHOLD;
        float progress = getProgress(translationX, translationY);
        float parentDiagonal = (float) Math.sqrt(recyclerView.getHeight() * recyclerView
                .getHeight() + recyclerView.getWidth() * recyclerView.getWidth());
        if (isDistanceThresholdReached) {
            switch (direction) {
                case LEFT:
                    startSwipe(selectedItem, direction, -parentDiagonal, 0.0f,
                            DEFAULT_SWIPE_LEFT_ROTATION, progress, DEFAULT_SWIPE_DURATION);
                    return true;
                case TOP:
                    startSwipe(selectedItem, direction, 0.0f, -parentDiagonal,
                            DEFAULT_SWIPE_TOP_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
                case RIGHT:
                    startSwipe(selectedItem, direction, parentDiagonal, 0.0f,
                            DEFAULT_SWIPE_RIGHT_ROTATION, progress, DEFAULT_SWIPE_DURATION);
                    return true;
                case BOTTOM:
                    startSwipe(selectedItem, direction, 0.0f, parentDiagonal,
                            DEFAULT_SWIPE_BOTTOM_ROTATION, 0.0f, DEFAULT_SWIPE_DURATION);
                    break;
            }
        }
        return false;
    }

    private void select(@NonNull RecyclerView recyclerView, @Nullable RecyclerView.ViewHolder
            selectedItem) {
        this.selectedItem = selectedItem;
        if (selectedItem != null) {
            SwipeAnimation swipeAnimation = animations.get(selectedItem.getItemId());
            if (swipeAnimation != null) {
                swipeAnimation.cancel();
                animations.remove(selectedItem.getItemId());
            }
        }
    }

    private void startSwipe(@NonNull RecyclerView.ViewHolder viewHolder, SwipeDirection
            swipeDirection,
                            float translationXTarget, float translationYTarget, float
                                    rotationTarget, float initialProgress, long duration) {
        SwipeAnimation previousAnimation = animations.get(viewHolder.getItemId());
        if (previousAnimation != null) {
            previousAnimation.cancel();
            animations.remove(selectedItem.getItemId());
        }
        SwipeAnimation swipeAnimation = new SwipeAnimation(viewHolder, swipeDirection, true,
                viewHolder.itemView.getTranslationX(), viewHolder.itemView.getTranslationY(),
                viewHolder.itemView.getRotation(), initialProgress,
                translationXTarget, translationYTarget, rotationTarget, 1.0f);
        animations.put(viewHolder.getItemId(), swipeAnimation);
        swipeAnimation.start();
        recyclerView.invalidate();
    }

    private void startReturn(@NonNull RecyclerView.ViewHolder viewHolder, float initialProgress) {
        SwipeDirection currentSwipeDirection = getSwipeDirection(viewHolder);
        SwipeAnimation previousAnimation = animations.get(viewHolder.getItemId());
        if (previousAnimation != null) {
            previousAnimation.cancel();
            animations.remove(viewHolder.getItemId());
        }
        SwipeAnimation returnAnimation = new SwipeAnimation(viewHolder, currentSwipeDirection,
                false,
                viewHolder.itemView.getTranslationX(), viewHolder.itemView.getTranslationY(),
                viewHolder.itemView.getRotation(), initialProgress,
                0, 0, 0, 0);
        animations.put(viewHolder.getItemId(), returnAnimation);
        returnAnimation.start();
        recyclerView.invalidate();
    }

    private void updateDxDy(MotionEvent ev) {
        dX = (int) (ev.getX() - initialTouchX);
        dY = (int) (ev.getY() - initialTouchY);
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public enum SwipeDirection {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    }

    public interface SwipeDirectionsProvider {
        boolean isAllowed(SwipeDirection swipeDirection);

        SwipeDirection getSwipeDirection(float x, float y);
    }

    public static class LeftRightDirectionsProvider implements SwipeDirectionsProvider {

        @Override
        public boolean isAllowed(SwipeDirection swipeDirection) {
            switch (swipeDirection) {
                case RIGHT:
                case LEFT:
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public SwipeDirection getSwipeDirection(float x, float y) {
            if (x > 0) {
                return SwipeDirection.RIGHT;
            } else {
                return SwipeDirection.LEFT;
            }
        }
    }

    public static class LeftRightBottomDirectionsProvider implements SwipeDirectionsProvider {


        @Override
        public boolean isAllowed(SwipeDirection swipeDirection) {
            switch (swipeDirection) {
                case RIGHT:
                case LEFT:
                case BOTTOM:
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public SwipeDirection getSwipeDirection(float x, float y) {
            if (x > y) {
                if (x > 0) {
                    return SwipeDirection.RIGHT;
                } else {
                    return SwipeDirection.LEFT;
                }
            } else {
                if (x < -y) {
                    return SwipeDirection.LEFT;
                } else {
                    return SwipeDirection.BOTTOM;
                }
            }
        }
    }

    public interface OnSwipeListener {
        void onSwipeProgress(RecyclerView.ViewHolder viewHolder, float swipeProgress);

        void onSwipeProgressLeft(RecyclerView.ViewHolder viewHolder, float swipeProgress);

        void onSwipeProgressTop(RecyclerView.ViewHolder viewHolder, float swipeProgress);

        void onSwipeProgressRight(RecyclerView.ViewHolder viewHolder, float swipeProgress);

        void onSwipeProgressBottom(RecyclerView.ViewHolder viewHolder, float swipeProgress);

        void onItemSwiped(RecyclerView.ViewHolder viewHolder, SwipeDirection swipeDirection);
    }

    private static class SwipeAnimation implements Animator.AnimatorListener {

        final float startTranslationX;

        final float startTranslationY;

        final float startRotation;

        private float startProgress;

        final float endTranslationX;

        final float endTranslationY;

        final float endRotation;

        private float endProgress;

        final RecyclerView.ViewHolder viewHolder;

        final SwipeDirection swipeDirection;

        final boolean swipingOut;

        private final ValueAnimator valueAnimator;
        float currentTranslationX;

        float currentTranslationY;

        float currentRotation;

        float currentProgress;

        boolean ended = false;

        private float fraction;

        SwipeAnimation(RecyclerView.ViewHolder viewHolder, SwipeDirection swipeDirection,
                       boolean swipingOut,
                       float startTranslationX, float startTranslationY, float startRotation,
                       float startProgress,
                       float endTranslationX, float endTranslationY, float endRotation,
                       float endProgress) {
            this.viewHolder = viewHolder;

            this.swipeDirection = swipeDirection;
            this.swipingOut = swipingOut;

            this.startTranslationX = startTranslationX;
            this.startTranslationY = startTranslationY;
            this.startRotation = startRotation;
            this.startProgress = startProgress;

            this.currentTranslationX = startTranslationX;
            this.currentTranslationY = startTranslationY;
            this.currentRotation = startRotation;
            this.currentProgress = startProgress;

            this.endTranslationX = endTranslationX;
            this.endTranslationY = endTranslationY;
            this.endRotation = endRotation;
            this.endProgress = endProgress;


            this.valueAnimator = ValueAnimator.ofFloat(0f, 1f);
            if (!swipingOut) {
                valueAnimator.setInterpolator(new OvershootInterpolator());
            }
            this.valueAnimator.addUpdateListener(animation -> setFraction(animation
                    .getAnimatedFraction()));
            valueAnimator.setTarget(viewHolder.itemView);
            valueAnimator.addListener(this);
            setFraction(0f);
        }

        public void setDuration(long duration) {
            valueAnimator.setDuration(duration);
        }

        public void start() {
            valueAnimator.start();
        }

        public void cancel() {
            valueAnimator.cancel();
        }

        public void setFraction(float fraction) {
            this.fraction = fraction;
        }

        /**
         * We run updates on onDraw method but use the fraction from animator callback.
         * This way, we can sync translate x/y values w/ the animators to avoid one-off frames.
         */
        public void update() {
            if (startTranslationX != endTranslationX) {
                currentTranslationX = startTranslationX + fraction * (endTranslationX -
                        startTranslationX);
            }

            if (startTranslationY != endTranslationY) {
                currentTranslationY = startTranslationY + fraction * (endTranslationY -
                        startTranslationY);
            }

            if (startRotation != endRotation) {
                currentRotation = startRotation + fraction * (endRotation - startRotation);
            }

            if (startProgress != endProgress) {
                currentProgress = startProgress + fraction * (endProgress - startProgress);
            }
        }

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            ended = true;
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            setFraction(1f); //make sure we recover the view's state.
        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }
}
