package com.bimber.utils.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by maciek on 31.07.17.
 */

public class TouchInterceptConstraintLayout extends ConstraintLayout {

    private OnTouchListener onTouchListener;

    public TouchInterceptConstraintLayout(Context context) {
        super(context);
    }

    public TouchInterceptConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(onTouchListener!=null){
            return onTouchListener.onTouch(this, ev) && super.onInterceptTouchEvent(ev);
        }
        return super.onInterceptTouchEvent(ev);
    }

    public void setOnTouchInterceptListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }
}
