package com.bimber.utils.view;

import android.animation.Animator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by maciek on 16.02.17.
 */

public class PaddingTransition extends Transition {
    private static final String PROPNAME_PADDING_LEFT = "alexjlockwood:transition:paddingleft";
    private static final String PROPNAME_PADDING_TOP = "alexjlockwood:transition:paddingtop";
    private static final String PROPNAME_PADDING_RIGHT = "alexjlockwood:transition:paddingright";
    private static final String PROPNAME_PADDING_BOTTOM = "alexjlockwood:transition:paddingbottom";
    private static final String[] TRANSITION_PROPERTIES = {PROPNAME_PADDING_LEFT, PROPNAME_PADDING_TOP, PROPNAME_PADDING_RIGHT,
            PROPNAME_PADDING_BOTTOM};

    public PaddingTransition() {
    }

    public PaddingTransition(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public String[] getTransitionProperties() {
        return TRANSITION_PROPERTIES;
    }

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    private void captureValues(TransitionValues transitionValues) {
        transitionValues.values.put(PROPNAME_PADDING_LEFT, transitionValues.view.getPaddingLeft());
        transitionValues.values.put(PROPNAME_PADDING_TOP, transitionValues.view.getPaddingTop());
        transitionValues.values.put(PROPNAME_PADDING_RIGHT, transitionValues.view.getPaddingRight());
        transitionValues.values.put(PROPNAME_PADDING_BOTTOM, transitionValues.view.getPaddingBottom());
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues,
                                   TransitionValues endValues) {
        if (startValues == null || endValues == null) {
            return null;
        }

        Integer paddingLeftStart = (Integer) startValues.values.get(PROPNAME_PADDING_LEFT);
        Integer paddingTopStart = (Integer) startValues.values.get(PROPNAME_PADDING_TOP);
        Integer paddingRightStart = (Integer) startValues.values.get(PROPNAME_PADDING_RIGHT);
        Integer paddingBottomStart = (Integer) startValues.values.get(PROPNAME_PADDING_BOTTOM);

        Integer paddingLeftEnd = (Integer) endValues.values.get(PROPNAME_PADDING_LEFT);
        Integer paddingTopEnd = (Integer) endValues.values.get(PROPNAME_PADDING_TOP);
        Integer paddingRightEnd = (Integer) endValues.values.get(PROPNAME_PADDING_RIGHT);
        Integer paddingBottomEnd = (Integer) endValues.values.get(PROPNAME_PADDING_BOTTOM);

        View view = endValues.view;
//        view.setPadding(paddingLeftStart, paddingTopStart, paddingRightStart, paddingBottomStart);
        view.setPadding(paddingLeftEnd, paddingTopEnd, paddingRightEnd, paddingBottomEnd);
        PropertyValuesHolder propertyValuesHolderLeft = PropertyValuesHolder.ofInt(PROPNAME_PADDING_LEFT, paddingLeftStart, paddingLeftEnd);
        PropertyValuesHolder propertyValuesHolderTop = PropertyValuesHolder.ofInt(PROPNAME_PADDING_TOP, paddingTopStart, paddingTopEnd);
        PropertyValuesHolder propertyValuesHolderRight = PropertyValuesHolder.ofInt(PROPNAME_PADDING_RIGHT, paddingRightStart,
                paddingRightEnd);
        PropertyValuesHolder propertyValuesHolderBottom = PropertyValuesHolder.ofInt(PROPNAME_PADDING_BOTTOM, paddingBottomStart,
                paddingBottomEnd);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(propertyValuesHolderLeft, propertyValuesHolderTop,
                propertyValuesHolderRight, propertyValuesHolderBottom);
        animator.addUpdateListener(animation1 -> view.setPadding(
                (Integer) animation1.getAnimatedValue(PROPNAME_PADDING_LEFT),
                (Integer) animation1.getAnimatedValue(PROPNAME_PADDING_TOP),
                (Integer) animation1.getAnimatedValue(PROPNAME_PADDING_RIGHT),
                (Integer) animation1.getAnimatedValue(PROPNAME_PADDING_BOTTOM)));
        return animator;
    }
}