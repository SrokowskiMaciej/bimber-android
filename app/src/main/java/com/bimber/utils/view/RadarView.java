package com.bimber.utils.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.bimber.R;
import com.github.kevelbreh.androidunits.AndroidUnit;

/**
 * Created by srokowski.maciej@gmail.com on 10.01.17.
 */

public class RadarView extends View {

    public static final int MARGIN = 25;
    public static final double STEP_DEGREE = 1.4;
    private final int POINT_ARRAY_SIZE = 60;

    private int fps = 50;

    float alpha = 0;
    private Point latestPoint[] = new Point[POINT_ARRAY_SIZE];
    private int latestAlpha[] = new int[POINT_ARRAY_SIZE];

    private Paint paint = new Paint();
    private Handler mHandler = new Handler();

    private Runnable mTick = new Runnable() {
        @Override
        public void run() {
            invalidate();
            mHandler.postDelayed(this, 1000 / fps);
        }
    };

    public RadarView(Context context) {
        this(context, null);
    }

    public RadarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public RadarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        paint.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(AndroidUnit.DENSITY_PIXELS.toPixels(3));
        paint.setAlpha(0);

        int alpha_step = 255 / POINT_ARRAY_SIZE;
        for (int i = 0; i < latestAlpha.length; i++) {
            latestAlpha[i] = 255 - (i * alpha_step);
        }
        for (int i = 0; i < latestPoint.length; i++) {
            latestPoint[i] = new Point(0, 0);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        stopAnimation();
        super.onDetachedFromWindow();
    }

    public void startAnimation() {
        mHandler.removeCallbacks(mTick);
        mHandler.post(mTick);
    }

    public void stopAnimation() {
        mHandler.removeCallbacks(mTick);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        int width = getWidth() - MARGIN * 2;
        int height = getHeight() - MARGIN * 2;

        int r = (Math.min(width, height)) / 2;

        int i = r + MARGIN;
        int j = r - 1;
        paint.setAlpha(latestAlpha[0]);
        canvas.drawCircle(i, i, j, paint);
        alpha -= STEP_DEGREE;
        if (alpha < -360) alpha = 0;
        double angle = Math.toRadians(alpha);
        int offsetX = (int) (i + (float) (r * Math.cos(angle)));
        int offsetY = (int) (i - (float) (r * Math.sin(angle)));

        latestPoint[0].set(offsetX, offsetY);

        for (int x = POINT_ARRAY_SIZE - 1; x > 0; x--) {
            Point previousPoint = latestPoint[x - 1];
            latestPoint[x].set(previousPoint.x, previousPoint.y);
        }

        for (int x = 0; x < POINT_ARRAY_SIZE; x++) {
            Point point = latestPoint[x];
            if (point.x != 0 && point.y != 0) {
                paint.setAlpha(latestAlpha[x]);
                canvas.drawLine(i, i, point.x, point.y, paint);
            }
        }
    }

}
