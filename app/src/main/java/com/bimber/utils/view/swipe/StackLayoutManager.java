package com.bimber.utils.view.swipe;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Maciek on 11.12.2017.
 */

public class StackLayoutManager extends RecyclerView.LayoutManager {

    private int stackSize = 5;

    private Set<View> movedViews = new HashSet<>();

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        detachAndScrapAttachedViews(recycler);
        int itemCount = state.getItemCount();
        for (int childIndex = 0; childIndex < itemCount && childIndex < stackSize; childIndex++) {
            View view = recycler.getViewForPosition(childIndex);
            if (view != null) {
                addView(view, 0);
                measureChildWithMargins(view, 0, 0);
                final RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
                layoutDecoratedWithMargins(view, 0, 0, getWidth(), view.getMeasuredHeight() +
                        lp.topMargin + lp.bottomMargin);
            }
        }
    }

//    @Override
//    public void onItemsMoved(RecyclerView recyclerView, int from, int to, int itemCount) {
//        super.onItemsMoved(recyclerView, from, to, itemCount);
//        for (int i = 0; i < itemCount; i++) {
//            if (to < from) {
//                detachViewAt(from + i);
//            } else {
//                detachViewAt(to + i);
//            }
//        }
//
//    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return true;
    }

    public void setStackSize(int stackSize) {
        this.stackSize = stackSize;
    }

    public boolean canScrollHorizontally() {
        return false;
    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }


}
