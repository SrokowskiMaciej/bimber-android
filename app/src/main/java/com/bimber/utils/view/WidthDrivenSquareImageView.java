package com.bimber.utils.view;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by srokowski.maciej@gmail.com on 24.12.16.
 */

public class WidthDrivenSquareImageView extends AppCompatImageView {
    public WidthDrivenSquareImageView(Context context) {
        super(context);
    }

    public WidthDrivenSquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
