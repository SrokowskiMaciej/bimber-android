package com.bimber.utils.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.bimber.utils.maps.SphericalUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by maciek on 13.03.17.
 */

public class ViewUtils {


    private static final int RESOURCE_NOT_FOUND = 0;

    public static void hideKeyboard(Context context) {
        try {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if ((((Activity) context).getCurrentFocus() != null) && (((Activity) context).getCurrentFocus().getWindowToken() != null)) {
                ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(((Activity)
                        context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }


    public static void setStatusBarColor(Activity activity, @ColorRes int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(activity, color));
        }
    }

    public static void setStautsBarInsetMargins(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.topMargin = getStatusBarHeight(view.getContext());
        }
    }

    public static void setNavbarInsetMargins(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.bottomMargin = getNavbarHeight(view.getContext());
        }
    }

    public static int getStatusBarHeight(Context context) {
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        return resourceId != RESOURCE_NOT_FOUND ? context.getResources().getDimensionPixelSize(resourceId) : 0;
    }

    public static int getNavbarHeight(@NonNull Context context) {
        Resources res = context.getResources();
        int navBarIdentifier = res.getIdentifier("navigation_bar_height", "dimen", "android");
        return navBarIdentifier != RESOURCE_NOT_FOUND
                ? res.getDimensionPixelSize(navBarIdentifier) : 0;
    }

    //FIXME app:drawableTint should be included in AppCompatButton. Wait for fix for https://code.google
    // .com/p/android/issues/detail?id=226605&thanks=226605&ts=1477770853 issue
    public static void applyFixForDrawableLeftTint(TextView view, @ColorRes int color) {
        Drawable drawable = view.getCompoundDrawables()[0];
        if (drawable != null) {
            drawable.mutate().setColorFilter(ContextCompat.getColor(view.getContext(), color), PorterDuff.Mode.SRC_IN);
        }
    }

    public static void applyFixForDrawableRightTint(TextView view, @ColorRes int color) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = view.getCompoundDrawables()[2];
            if (drawable != null) {
                drawable.mutate().setColorFilter(ContextCompat.getColor(view.getContext(), color), PorterDuff.Mode.SRC_IN);
            }
        }
    }

    public static void applyFixForDrawableRightTintColor(TextView view, @ColorInt int color) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = view.getCompoundDrawables()[2];
            if (drawable != null) {
                drawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
            }
        }
    }

    public static void applyFixForDrawableTopTint(TextView view, @ColorRes int color) {
        Drawable drawable = view.getCompoundDrawables()[1];
        if (drawable != null) {
            drawable.mutate().setColorFilter(ContextCompat.getColor(view.getContext(), color), PorterDuff.Mode.SRC_IN);
        }
    }

    public static LatLngBounds calculateBounds(LatLng center, double radius) {
        return new LatLngBounds.Builder().
                include(SphericalUtil.computeOffset(center, radius, 0)).
                include(SphericalUtil.computeOffset(center, radius, 90)).
                include(SphericalUtil.computeOffset(center, radius, 180)).
                include(SphericalUtil.computeOffset(center, radius, 270)).build();
    }

    public static boolean hitTest(View v, int x, int y) {
        final int tx = (int) (ViewCompat.getTranslationX(v) + 0.5f);
        final int ty = (int) (ViewCompat.getTranslationY(v) + 0.5f);
        final int left = v.getLeft() + tx;
        final int right = v.getRight() + tx;
        final int top = v.getTop() + ty;
        final int bottom = v.getBottom() + ty;

        return (x >= left) && (x <= right) && (y >= top) && (y <= bottom);
    }

    public static void finishActivitySafely(@Nullable Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            activity.finish();
        }
    }

    public static int[] getViewCenter(View view) {
        int[] center = new int[2];
        view.getLocationOnScreen(center);
        center[0] = center[0] + view.getWidth() / 2;
        center[1] = center[1] - getStatusBarHeight(view.getContext()) + view.getHeight() / 2;
        return center;
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }
}
