package com.bimber.utils;

import timber.log.Timber;

/**
 * Created by maciek on 23.06.17.
 */

public class ThreadUtils {

    public static long getThreadId() {
        Thread t = Thread.currentThread();
        return t.getId();
    }

    public static String getThreadSignature() {
        Thread t = Thread.currentThread();
        long l = t.getId();
        String name = t.getName();
        long p = t.getPriority();
        String gname = t.getThreadGroup().getName();
        return (name
                + ":(id)" + l
                + ":(priority)" + p
                + ":(group)" + gname);
    }

    public static String getThreadName() {
        return Thread.currentThread().getName();
    }

    public static void logThreadSignature() {
        Timber.d("Thread: %s", getThreadSignature());
    }
}
