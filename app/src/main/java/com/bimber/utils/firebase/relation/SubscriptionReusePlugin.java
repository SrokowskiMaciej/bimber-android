package com.bimber.utils.firebase.relation;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent.EventType;
import com.google.firebase.database.DataSnapshot;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.FlowableTransformer;
import io.reactivex.functions.BiFunction;

import static com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent;

/**
 * Created by maciek on 06.10.17.
 */
class SubscriptionReusePlugin<I> implements Relation.Plugin<I> {
    private final ResultsCollection<I> resultsCollection;
    private final BiFunction<DataSnapshot, I, Boolean> shouldReuseSubscription;
    private final BiFunction<DataSnapshot, I, I> reuseSubscriptionMapper;
    private final Map<String, DataSnapshot> changedParentValues;

    SubscriptionReusePlugin(ResultsCollection<I> resultsCollection,
                            BiFunction<DataSnapshot, I, Boolean> shouldReuseSubscription,
                            BiFunction<DataSnapshot, I, I> reuseSubscriptionMapper) {
        this.resultsCollection = resultsCollection;
        this.shouldReuseSubscription = shouldReuseSubscription;
        this.reuseSubscriptionMapper = reuseSubscriptionMapper;
        changedParentValues = Collections.synchronizedMap(new HashMap<>());
    }

    @Override
    public FlowableTransformer<ChildEvent, ChildEvent> transformEvents() {
        return upstream -> upstream.map(childEvent -> {

            switch (childEvent.eventType) {
                case CHANGED:
                    ChildValue<I> result = resultsCollection.getResult(childEvent.key);
                    if (result != null &&
                            result.val != null &&
                            SubscriptionReusePlugin.this.shouldReuseSubscription.apply(childEvent.snapshot, result.val)) {
                        this.changedParentValues.put(childEvent.key, childEvent.snapshot);
                    } else {
                        this.changedParentValues.remove(childEvent.key);
                    }
                    break;
                case REMOVED:
                    this.changedParentValues.remove(childEvent.key);
                    break;
            }
            return childEvent;

        });
    }

    public boolean modifyChildSubscriptionsPolicy(boolean defaultPolicyResult, ChildEvent childEvent) {
        //Basically we don't want to resubscribe on changed event but want to emmit data instead
        if (childEvent.eventType == EventType.CHANGED && this.changedParentValues.containsKey(childEvent.key)) {
            return false;
        } else {
            return defaultPolicyResult;
        }
    }

    public boolean modifyValuesEmissionsPolicy(boolean defaultPolicyResult, ChildEvent childEvent) {
        //Basically we don't want to resubscribe on changed event but want to emmit data instead
        if (childEvent.eventType == EventType.CHANGED) {
            return true;
        } else {
            return defaultPolicyResult;
        }
    }

    @Override
    public FlowableTransformer<ChildValue<I>, ChildValue<I>> transformOutputResult() {
        return upstream -> upstream.map(result -> {
            DataSnapshot dataSnapshot = SubscriptionReusePlugin.this.changedParentValues.get(result.childEvent.key);
            if (dataSnapshot != null) {
                return new ChildValue<>(result.childEvent, SubscriptionReusePlugin.this.reuseSubscriptionMapper.apply(dataSnapshot,
                        result.val));
            } else {
                return result;
            }
        });
    }

    @Override
    public FlowableTransformer<ChildValue<List<I>>, ChildValue<List<I>>> transformOutputResults() {
        return null;
    }
}
