package com.bimber.utils.firebase.relation;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent;
import com.google.firebase.database.DataSnapshot;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.FlowableOperator;
import io.reactivex.FlowableSubscriber;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.util.AtomicThrowable;
import rx.internal.util.ExceptionsUtils;
import rx.plugins.RxJavaHooks;

/**
 * Created by maciek on 06.10.17.
 */
class SubscriptionsCoordinator<D> implements FlowableOperator<ChildValue<D>, ChildEvent> {

    private final Function<DataSnapshot, Flowable<D>> of;

    SubscriptionsCoordinator(Function<DataSnapshot, Flowable<D>> of) {
        this.of = of;
    }

    @Override
    public Subscriber<? super ChildEvent> apply(@NonNull Subscriber<? super ChildValue<D>> observer) throws Exception {
        return new Op<D>(observer, of);
    }

    private static class Op<D> implements FlowableSubscriber<ChildEvent>, Subscription {

        private final Subscriber<? super ChildValue<D>> downstream;
        private final Function<DataSnapshot, Flowable<D>> of;
        private final Map<String, Disposable> disposables = Collections.synchronizedMap(new HashMap<>());
        private final AtomicThrowable terminalError = new AtomicThrowable();

        private Subscription subscription;

        private Op(Subscriber<? super ChildValue<D>> downstream, Function<DataSnapshot, Flowable<D>> of) {
            this.downstream = downstream;
            this.of = of;
        }

        @Override
        public void onSubscribe(@NonNull Subscription s) {
            this.subscription = s;
            downstream.onSubscribe(this);
        }

        @Override
        public void onNext(ChildEvent childEvent) {
            switch (childEvent.eventType) {
                case ADDED:
                    addChildPublisher(childEvent);
                    break;
                case CHANGED:
                    refreshChildPublisher(childEvent);
                    break;
                case REMOVED:
                    removeChildPublisher(childEvent);
                    break;
            }
        }

        @Override
        public void onError(Throwable t) {
            innerError(t);
        }

        private void innerError(Throwable throwable) {

            if (!ExceptionsUtils.addThrowable(this.terminalError, throwable)) {
                RxJavaHooks.onError(throwable);
            } else {
                Throwable ex = ExceptionsUtils.terminate(this.terminalError);
                if (!ExceptionsUtils.isTerminated(ex)) {
                    //FIXME No idea why but it crashes
//                    downstream.onError(ex);
                }
                disposeAllChildSubscribtions();
            }
        }

        @Override
        public void onComplete() {
            downstream.onComplete();
            disposeAllChildSubscribtions();
        }

        @Override
        public void request(long n) {
            subscription.request(n);
        }

        @Override
        public void cancel() {
            subscription.cancel();
            disposeAllChildSubscribtions();
        }

        private synchronized void addChildPublisher(ChildEvent childEvent) {
            if (disposables.containsKey(childEvent.key)) {
                innerError(new IllegalStateException(String.format("Child with id: %s has already been added", childEvent.key)));
                disposeAllChildSubscribtions();
                return;
            }
            Flowable<ChildValue<D>> childPublisher = null;
            try {
                childPublisher = of.apply(childEvent.snapshot).map(d -> new ChildValue<>(childEvent, d));
            } catch (Exception e) {
                innerError(e);
                disposeAllChildSubscribtions();
                return;
            }
            Disposable disposable = childPublisher.subscribe(downstream::onNext, this::innerError);
            disposables.put(childEvent.key, disposable);
        }

        private synchronized void removeChildPublisher(ChildEvent childEvent) {
            if (!disposables.containsKey(childEvent.key)) {
                innerError(new IllegalStateException(String.format("Child with id: %s has never been added", childEvent.key)));
                subscription.cancel();
                disposeAllChildSubscribtions();
                return;
            }
            Disposable disposable = disposables.get(childEvent.key);
            disposable.dispose();
            disposables.remove(childEvent.key);
        }

        private void refreshChildPublisher(ChildEvent childEvent) {
            removeChildPublisher(childEvent);
            addChildPublisher(childEvent);
        }

        private synchronized void disposeAllChildSubscribtions() {

            for (Disposable disposable : disposables.values()) {
                disposable.dispose();
            }
            disposables.clear();
        }
    }
}
