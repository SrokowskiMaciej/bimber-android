package com.bimber.utils.firebase;

/**
 * Created by maciek on 04.07.17.
 */

public enum Ack {
    ACK_NETWORK,
    ACK_CACHE
}
