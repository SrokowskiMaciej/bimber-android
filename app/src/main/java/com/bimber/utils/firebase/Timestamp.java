package com.bimber.utils.firebase;

import com.google.firebase.database.ServerValue;

import java.io.Serializable;

import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;

/**
 * Created by maciek on 28.02.17.
 */
public class Timestamp implements Serializable {

    public static class Adapter implements TypeAdapter<Timestamp, Object> {

        @Override
        public Timestamp fromFirebaseValue(Object value) {
            return new Timestamp(value); // value is a Long
        }

        @Override
        public Object toFirebaseValue(Timestamp value) {
            return value.mValue;
        }

    }

    public static Timestamp now() {
        return new Timestamp(ServerValue.TIMESTAMP);
    }

    public static Timestamp create(Long value) {
        return new Timestamp(value);
    }

    private final Object mValue;

    private Timestamp(Object value) {
        mValue = value;
    }

    public Long value() {
        return (Long) mValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timestamp timestamp = (Timestamp) o;

        return mValue != null ? mValue.equals(timestamp.mValue) : timestamp.mValue == null;

    }

    @Override
    public int hashCode() {
        return mValue != null ? mValue.hashCode() : 0;
    }
}