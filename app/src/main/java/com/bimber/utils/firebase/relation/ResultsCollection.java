package com.bimber.utils.firebase.relation;

import com.bimber.utils.ThreadUtils;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.Flowable;
import timber.log.Timber;

/**
 * Created by maciek on 13.10.17.
 */
class ResultsCollection<D> {

    //TODO Replace this with persistent collection. It is not urgent though, usually resulting collections won't be that big. And
    // maybe let it be some ordered map
    private final List<ChildValue<D>> values = new CopyOnWriteArrayList<>();

    ResultsCollection() {

    }


    synchronized RxFirebaseDb.ChildEvent processChild(RxFirebaseDb.ChildEvent childEvent) {
//        Timber.d("Relation - processChild parentId: %s previousChildName: %s, eventType: %s thread: %s", childEvent.key,
//                childEvent.previousChildKey, childEvent.eventType, ThreadUtils.getThreadName());
        switch (childEvent.eventType) {
            case ADDED:
                int addIndex = getIndexForKey(childEvent.key);
                if (addIndex > 0) {
                    throw new IllegalArgumentException();
                }
                if (childEvent.previousChildKey != null) {
                    int previousChildIndex = getIndexForKey(childEvent.previousChildKey);
                    if (previousChildIndex >= 0) {
                        values.add(addIndex + 1, new ChildValue<>(childEvent, null));
                    } else {
                        throw new IllegalStateException(String.format("No previous event with id %s for event with id %s", childEvent
                                .previousChildKey, childEvent.key));
                    }
                } else {
                    values.add(0, new ChildValue<>(childEvent, null));
                }
                break;
            case REMOVED:
                int removeIndex = getIndexForKey(childEvent.key);
                if (removeIndex >= 0) {
                    values.remove(removeIndex);
                } else {
                    throw new IllegalStateException();
                }
                break;
            case MOVED:
                int oldIndex = getIndexForKey(childEvent.key);
                ChildValue<D> value = values.get(oldIndex);
                if (oldIndex >= 0) {
                    values.remove(oldIndex);
                } else {
                    throw new IllegalStateException();
                }
                if (childEvent.previousChildKey != null) {
                    int previousChildIndex = getIndexForKey(childEvent.previousChildKey);
                    if (previousChildIndex >= 0) {
                        values.add(previousChildIndex + 1, value);
                    } else {
                        throw new IllegalStateException();
                    }
                } else {
                    values.add(0, value);
                }
                break;
        }
        return childEvent;
    }

    synchronized Flowable<ChildValue<D>> deliverChild(ChildValue<D> newValue) {
//        Timber.d("Relation - deliverChild parentId: %s threadName: %s", newValue.childEvent, ThreadUtils.getThreadName());
        int index = getIndexForKey(newValue.childEvent.key);
        // Child can not exist in case it was removed before it was fetched
        if (index >= 0) {
            values.set(index, newValue);
        }
        return existingValues();
    }

    public Flowable<ChildValue<D>> existingValues() {
        return Flowable.fromIterable(values)
                .filter(valueWithKey -> valueWithKey.val != null);
//                    .map(value -> {
//                        Timber.d("Relation - existingValues parentId: %s threadName: %s", value.key, ThreadUtils
//                                .getThreadName());
//                        return value;
//                    });
    }

    public ChildValue<D> getResult(String key) {
        int indexForKey = getIndexForKey(key);
        if (indexForKey < 0) {
            return null;
        } else {
            return values.get(indexForKey);
        }
    }

    private int getIndexForKey(String key) {
        int index = 0;
        for (ChildValue<D> value : values) {
            if (value.childEvent.key.equals(key)) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }
}
