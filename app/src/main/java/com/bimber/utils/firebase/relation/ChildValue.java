package com.bimber.utils.firebase.relation;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

/**
 * Created by maciek on 06.10.17.
 */
class ChildValue<T> {
    @NonNull
    public final ChildEvent childEvent;
    @Nullable
    public final T val;

    ChildValue(@NonNull ChildEvent childEvent, @Nullable T val) {
        this.childEvent = childEvent;
        this.val = val;
    }

    @Override
    public String toString() {
        return "ChildValue{" +
                "childEvent=" + childEvent +
                ", val=" + val +
                '}';
    }
}
