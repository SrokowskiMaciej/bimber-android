package com.bimber.utils.firebase.relation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent.EventType;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.functions.Cancellable;

/**
 * Created by maciek on 03.10.17.
 */

public class RxFirebaseDb {

    /**
     * Listener for for child events occurring at the given query location.
     *
     * @param query reference represents a particular location in your Database and can be used for reading or writing data to that
     *              Database location.
     * @return a {@link Flowable} which emits when a snapshot of a child int the database change on the given query.
     */
    @NonNull
    public static Flowable<ChildEvent> observeChildEvent(
            @NonNull final Query query) {
        return Flowable.create(new FlowableOnSubscribe<ChildEvent>() {
            @Override
            public void subscribe(final FlowableEmitter<ChildEvent> emitter) throws Exception {
                final ChildEventListener childEventListener = new ChildEventListener() {

                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                        emitter.onNext(new ChildEvent(dataSnapshot.getKey(), previousChildName, dataSnapshot, EventType.ADDED));
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                        emitter.onNext(new ChildEvent(dataSnapshot.getKey(), previousChildName, dataSnapshot, EventType.CHANGED));
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        emitter.onNext(new ChildEvent(dataSnapshot.getKey(), null, dataSnapshot, EventType.REMOVED));
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                        emitter.onNext(new ChildEvent(dataSnapshot.getKey(), previousChildName, dataSnapshot, EventType.MOVED));
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        if (!emitter.isCancelled())
                            emitter.tryOnError(error.toException());
                    }
                };
                final ValueEventListener queryCompleteListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        emitter.onNext(new ChildEvent(dataSnapshot.getKey(), null, dataSnapshot, EventType.QUERY_COMPLETE));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (!emitter.isCancelled())
                            emitter.tryOnError(databaseError.toException());
                    }
                };
                emitter.setCancellable(new Cancellable() {
                    @Override
                    public void cancel() throws Exception {
                        query.removeEventListener(childEventListener);
                        query.removeEventListener(queryCompleteListener);
                    }
                });
                query.addChildEventListener(childEventListener);
                query.addListenerForSingleValueEvent(queryCompleteListener);
            }
        }, BackpressureStrategy.BUFFER);
    }


    public static class ChildEvent {
        @NonNull
        public final EventType eventType;
        @NonNull
        public final DataSnapshot snapshot;
        @NonNull
        public final String key;
        @Nullable
        public final String previousChildKey;

        public ChildEvent(@NonNull String key,
                          @Nullable String previousChildKey,
                          @NonNull DataSnapshot snapshot,
                          @NonNull EventType eventType) {
            this.key = key;
            this.previousChildKey = previousChildKey;
            this.snapshot = snapshot;
            this.eventType = eventType;
        }

        public enum EventType {
            ADDED,
            CHANGED,
            REMOVED,
            MOVED,
            QUERY_COMPLETE
        }

        @Override
        public String toString() {
            return "ChildEvent{" +
                    "eventType=" + eventType +
                    ", snapshot=" + snapshot +
                    ", key='" + key + '\'' +
                    ", previousChildKey='" + previousChildKey + '\'' +
                    '}';
        }
    }
}
