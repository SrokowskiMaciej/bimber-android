package com.bimber.utils.firebase.relation;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;

/**
 * Created by maciek on 13.10.17.
 */

class InitialQueryPlugin<T> implements Relation.Plugin<T> {

    private final Set<String> nonDeliveredInitialQueryEvents;
    private volatile boolean emitting;
    private volatile boolean queryCompleted;

    public InitialQueryPlugin() {
        nonDeliveredInitialQueryEvents = Collections.synchronizedSet(new HashSet<>());
    }

    @Override
    public FlowableTransformer<ChildEvent, ChildEvent> transformEvents() {
        return upstream -> upstream.map(childEvent -> {
            switch (childEvent.eventType) {
                case ADDED:
                    if (!queryCompleted) {
                        nonDeliveredInitialQueryEvents.add(childEvent.key);
                    }
                    break;
                case QUERY_COMPLETE:
                    queryCompleted = true;
                    break;
            }
            return childEvent;
        });
    }


    @Override
    public FlowableTransformer<ChildValue<T>, ChildValue<T>> transformOutputResult() {
        return null;
    }

    @Override
    public FlowableTransformer<ChildValue<List<T>>, ChildValue<List<T>>> transformOutputResults() {
        return upstream -> upstream.concatMap(values -> {
            if (emitting) {
                return Flowable.just(values);
            } else {
                nonDeliveredInitialQueryEvents.remove(values.childEvent.key);
                if (queryCompleted && nonDeliveredInitialQueryEvents.isEmpty()) {
                    emitting = true;
                    return Flowable.just(values);
                } else {
                    return Flowable.empty();
                }
            }
        });
    }
}
