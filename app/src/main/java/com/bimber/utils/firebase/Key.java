package com.bimber.utils.firebase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srokowski.maciej@gmail.com on 16.11.16.
 */

public class Key<T> implements Parcelable, Serializable {

    private String key;
    private T value;

    public static <T> Key<T> of(String key, T value) {
        return new Key<>(key, value);
    }

    public static <T> Map<String, T> toMap(List<Key<T>> objects) {
        Map<String, T> map = new HashMap<>(objects.size());
        for (Key<T> object : objects) {
            map.put(object.key(), object.value());
        }
        return map;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        //TODO Not sure if any better way exists
        final Class<?> aClass = value.getClass();
        dest.writeSerializable(aClass);
        if (value instanceof Parcelable) {
            dest.writeParcelable((Parcelable) this.value, flags);
        } else if (value instanceof Serializable) {
            dest.writeSerializable((Serializable) this.value);
        } else {
            throw new IllegalArgumentException("Not supported type for serialization");
        }

    }

    public Key() {
    }

    public Key(String key, T value) {
        this.key = key;
        this.value = value;
    }

    public String key() {
        return key;
    }

    public T value() {
        return value;
    }

    protected Key(Parcel in) {
        this.key = in.readString();
        Class<?> tClass = (Class<?>) in.readSerializable();
        if (Parcelable.class.isAssignableFrom(tClass)) {
            this.value = in.readParcelable(tClass.getClassLoader());
        } else if (Serializable.class.isAssignableFrom(tClass)) {
            this.value = (T) in.readSerializable();
        } else {
            throw new IllegalArgumentException("Not supported type for deserialization");
        }

    }

    public static final Creator<Key> CREATOR = new Creator<Key>() {
        @Override
        public Key createFromParcel(Parcel source) {
            return new Key(source);
        }

        @Override
        public Key[] newArray(int size) {
            return new Key[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key<?> key1 = (Key<?>) o;

        if (key != null ? !key.equals(key1.key) : key1.key != null) return false;
        return value != null ? value.equals(key1.value) : key1.value == null;

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    public enum LocalKey {
        LOCAL_KEY
    }

    @Override
    public String toString() {
        return "Key{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
