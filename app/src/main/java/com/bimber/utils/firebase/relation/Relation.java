package com.bimber.utils.firebase.relation;

import com.bimber.utils.firebase.relation.RxFirebaseDb.ChildEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;

/**
 * Created by maciek on 03.10.17.
 */

public class Relation<D> extends Flowable<List<D>> {

    @NonNull
    private final Publisher<ChildEvent> source;
    @NonNull
    private final Function<DataSnapshot, Flowable<D>> relation;
    @Nullable
    private final BiFunction<DataSnapshot, D, Boolean> shouldReuseSubscription;
    @Nullable
    private final BiFunction<DataSnapshot, D, D> reuseSubscriptionMapper;
    private final boolean progressiveInitialQuery;
    @Nullable
    private final Scheduler scheduler;

    private Relation(Publisher<ChildEvent> source,
                     Function<DataSnapshot, Flowable<D>> relation,
                     BiFunction<DataSnapshot, D, Boolean> shouldReuseSubscription,
                     BiFunction<DataSnapshot, D, D> reuseSubscriptionMapper,
                     boolean progressiveInitialQuery,
                     Scheduler scheduler) {
        this.source = source;
        this.relation = relation;
        this.shouldReuseSubscription = shouldReuseSubscription;
        this.reuseSubscriptionMapper = reuseSubscriptionMapper;
        this.progressiveInitialQuery = progressiveInitialQuery;
        this.scheduler = scheduler;
    }

    @Override
    protected void subscribeActual(Subscriber<? super List<D>> s) {
        ResultsCollection<D> resultsCollection = new ResultsCollection<>();
        SubscriptionReusePlugin<D> reusePlugin = prepareReusePlugin(resultsCollection);
        InitialQueryPlugin<D> initialQueryPlugin = prepareInitialQueryPlugin();

        Flowable<ChildEvent> eventsSource = Flowable.fromPublisher(source)
                .serialize()
                .compose(upstream -> this.scheduler != null ? upstream.observeOn(this.scheduler) : upstream)
                .flatMapSingle(childEvent -> Single.just(resultsCollection.processChild(childEvent)))
                .compose(upstream -> initialQueryPlugin != null ? upstream.compose(initialQueryPlugin.transformEvents()) : upstream)
                .compose(upstream -> reusePlugin != null ? upstream.compose(reusePlugin.transformEvents()) : upstream)
                .share();

        Flowable<ChildEvent> deliveredChildValues = eventsSource
                .filter(childEvent -> {
                    boolean defaultPolicy = allowChildSubscriptions(childEvent);
                    return reusePlugin != null ? reusePlugin.modifyChildSubscriptionsPolicy(defaultPolicy, childEvent) : defaultPolicy;
                })
                .lift(new SubscriptionsCoordinator<>(relation))
                .map(childValue -> {
                    resultsCollection.deliverChild(childValue);
                    return childValue.childEvent;
                });

        Flowable<ChildEvent> deliveredChildEvents = eventsSource
                .filter(childEvent -> {
                    boolean defaultPolicy = allowValuesEmissions(childEvent);
                    return reusePlugin != null ? reusePlugin.modifyValuesEmissionsPolicy(defaultPolicy, childEvent) : defaultPolicy;
                });


        Flowable.merge(deliveredChildEvents, deliveredChildValues)
                .compose(upstream -> this.scheduler != null ? upstream.observeOn(this.scheduler) : upstream)
                .flatMap((newValue) -> resultsCollection.existingValues()
                        .compose(upstream -> reusePlugin != null ? upstream.compose(reusePlugin.transformOutputResult()) : upstream)
                        .map(value -> value.val)
                        .toList()
                        .map(values -> new ChildValue<>(newValue, values))
                        .toFlowable()
                        .compose(upstream -> initialQueryPlugin != null ? upstream.compose(initialQueryPlugin.transformOutputResults()) :
                                upstream)
                        .map(values -> values.val))
                .subscribe(s);
    }

    @Nullable
    private SubscriptionReusePlugin<D> prepareReusePlugin(ResultsCollection<D> resultsCollection) {
        if (this.shouldReuseSubscription != null && this.reuseSubscriptionMapper != null) {
            return new SubscriptionReusePlugin<>(resultsCollection, this.shouldReuseSubscription, this.reuseSubscriptionMapper);
        } else {
            return null;
        }
    }

    @Nullable
    private InitialQueryPlugin<D> prepareInitialQueryPlugin() {
        if (this.progressiveInitialQuery) {
            return null;
        } else {
            return new InitialQueryPlugin<D>();
        }
    }

    interface Plugin<T> {
        FlowableTransformer<ChildEvent, ChildEvent> transformEvents();

        FlowableTransformer<ChildValue<T>, ChildValue<T>> transformOutputResult();

        FlowableTransformer<ChildValue<List<T>>, ChildValue<List<T>>> transformOutputResults();
    }

    private static boolean allowChildSubscriptions(ChildEvent childEvent) {
        switch (childEvent.eventType) {
            case ADDED:
            case CHANGED:
            case REMOVED:
                //We should deliver child events to child subscription coordinator only on add, remove and changed events. From there
                // only added and changed events will be delivered
                return true;
            case MOVED:
            case QUERY_COMPLETE:
            default:
                return false;
        }
    }

    private static boolean allowValuesEmissions(ChildEvent childEvent) {
        switch (childEvent.eventType) {
            case MOVED:
            case REMOVED:
                //TODO Query complete is only here because of queries with 0 items. It will cause something to be emitted twice
            case QUERY_COMPLETE:
                //We should emmit values only on moved and removed events. Other cases will be covered by Child subscription coordinator
                return true;
            case ADDED:
            case CHANGED:

            default:
                return false;
        }
    }

    public static class Builder {
        private final Query query;

        public Builder(Query query) {
            this.query = query;
        }

        public <B> CompleteBuilder<B> relation(@NonNull Function<DataSnapshot, Flowable<B>> relation) {
            return new CompleteBuilder<B>(query, relation);
        }
    }

    public static class CompleteBuilder<B> {
        private final Query query;
        private final Function<DataSnapshot, Flowable<B>> relation;
        @Nullable
        private BiFunction<DataSnapshot, B, Boolean> shouldReuseSubscription;
        @Nullable
        private BiFunction<DataSnapshot, B, B> subscriptionReusing;
        private boolean progressiveInitialQuery;
        @Nullable
        private Scheduler scheduler;

        private CompleteBuilder(Query query, Function<DataSnapshot, Flowable<B>> relation) {
            this.query = query;
            this.relation = relation;
        }

        public CompleteBuilder<B> onScheduler(Scheduler scheduler) {
            this.scheduler = scheduler;
            return this;
        }

        public CompleteBuilder<B> withProgressiveInitialQuery(boolean progressiveInitialQuery) {
            this.progressiveInitialQuery = progressiveInitialQuery;
            return this;
        }

        public CompleteBuilder<B> withSubscriptionReusing(BiFunction<DataSnapshot, B, B> subscriptionReusing) {
            this.subscriptionReusing = subscriptionReusing;
            return this;
        }

        public CompleteBuilder<B> withSubscriptionReusingCondition(BiFunction<DataSnapshot, B, Boolean> shouldReuseSubscription) {
            this.shouldReuseSubscription = shouldReuseSubscription;
            return this;
        }

        public Flowable<List<B>> observe() {
            if (subscriptionReusing != null && shouldReuseSubscription == null) {
                shouldReuseSubscription = (dataSnapshot, b) -> true;
            }
            return RxFirebaseDb.observeChildEvent(query)
                    .compose(upstream -> new Relation<B>(upstream, relation, shouldReuseSubscription, subscriptionReusing,
                            progressiveInitialQuery, scheduler));
        }
    }
}
