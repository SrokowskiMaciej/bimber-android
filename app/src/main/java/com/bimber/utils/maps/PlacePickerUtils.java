package com.bimber.utils.maps;

import java.util.regex.Pattern;

/**
 * Created by maciek on 08.08.17.
 */

public class PlacePickerUtils {


    private static final String LAT_LNG_PLACE_NAME_REGEX = "(\\d+)(°)(\\d+)(')(\\d+)(.)(\\d+)(\")[NS](\\s)(\\d+)(°)(\\d+)(')(\\d+)(.)" +
            "(\\d+)(\")[EW]";
    private static final Pattern LAT_LNG_PLACE_NAME_PATTERN = Pattern.compile(LAT_LNG_PLACE_NAME_REGEX);

    public static boolean isPlaceNameLatLngEncoded(String placeName){
        return LAT_LNG_PLACE_NAME_PATTERN.matcher(placeName).matches();
    }
}
