package com.bimber.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.bimber.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by srokowski.maciej@gmail.com on 28.11.16.
 */

public class LocationUtils {

    /**
     * Gets address from given coordinates
     *
     * @param context Context
     * @param latLng  Coordinates
     * @return Address
     * @throws IOException
     */
    public static Address getAddress(Context context, LatLng latLng) throws IOException {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> fromLocation = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        if (fromLocation.size() > 0) {
            return fromLocation.get(0);
        } else {
            throw new IOException("Couldn't obtain locations address");
        }
    }
}
