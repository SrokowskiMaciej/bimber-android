package com.bimber.utils.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by srokowski.maciej@gmail.com on 09.11.16.
 */

public abstract class MvpAbstractStatePresenter<T> extends MvpAbstractPresenter<T> implements MvpStatePresenter<T> {


    protected abstract void onLoad(@Nullable Bundle savedInstanceState);

    protected abstract void onSave(@Nullable Bundle outState);

    @Override
    public void onRestoreInstanceState(@NonNull Bundle bundle) {
        onLoad(bundle);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        onSave(bundle);
    }
}
