package com.bimber.utils.mvp;

/**
 * Created by srokowski.maciej@gmail.com on 08.11.16.
 */

public interface ViewBinder<T> {
    void bindView(T view);

    void unbindView(T view);

    boolean isViewBound();

    T getView();
}
