package com.bimber.utils.mvp;

/**
 * Created by srokowski.maciej@gmail.com on 03.12.16.
 */

public interface MvpStatePresenter<T> extends MvpPresenter<T>, SavedInstanceStateConsumer {
}
