package com.bimber.utils.mvp;

import android.content.Intent;
import android.support.annotation.NonNull;

/**
 * Created by srokowski.maciej@gmail.com on 08.11.16.
 */

public abstract class MvpAbstractPresenter<T> implements MvpPresenter<T> {

    private T view;

    @Override
    public void bindView(@NonNull T view) {
        if (view == null || this.view != null)
            throw new IllegalStateException("Bound view cannot be null and no view must be currently bounded");
        this.view = view;
        viewAttached(this.view);
    }

    @Override
    public void unbindView(@NonNull T view) {
        if (view == null || this.view != view)
            throw new IllegalStateException("Unbound view cannot be null and must be tha same view that was bound previously");
        viewDetached(this.view);
        this.view = null;
    }

    @Override
    @NonNull
    public T getView() {
        if (!isViewBound())
            throw new IllegalStateException("No view bound at the time of calling getView()");
        return view;
    }

    @Override
    public boolean isViewBound() {
        return view != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Nothing so that extending classes doesn't have to implement it but can whenever they want
    }

    protected abstract void viewAttached(T view);

    protected abstract void viewDetached(T view);
}
