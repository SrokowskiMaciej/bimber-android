package com.bimber.utils.mvp;

import android.content.Intent;

/**
 * Created by srokowski.maciej@gmail.com on 03.12.16.
 */

public interface OnActivityResultConsumer {
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
