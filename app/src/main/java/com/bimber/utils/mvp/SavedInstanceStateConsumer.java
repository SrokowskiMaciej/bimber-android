package com.bimber.utils.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by srokowski.maciej@gmail.com on 09.11.16.
 */

public interface SavedInstanceStateConsumer {
    void onRestoreInstanceState(@NonNull Bundle savedInstanceState);

    void onSaveInstanceState(@NonNull Bundle outState);
}
