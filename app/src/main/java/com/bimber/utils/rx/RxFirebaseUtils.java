package com.bimber.utils.rx;

import com.bimber.BuildConfig;
import com.bimber.utils.ThreadUtils;
import com.google.firebase.database.DataSnapshot;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableTransformer;
import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Maybe;
import io.reactivex.MaybeTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import timber.log.Timber;


/**
 * Created by maciek on 31.03.17.
 */

public class RxFirebaseUtils {

    public static final Predicate<DataSnapshot> EXISTING_DATA_SNAPSHOT_PREDICATE = new Predicate<DataSnapshot>() {
        @Override
        public boolean test(@io.reactivex.annotations.NonNull DataSnapshot dataSnapshot) throws Exception {
            return dataSnapshot.exists();
        }
    };

//    public static <T, G> Observable.Transformer<List<T>, List<G>> observeValueListsEvents(Function<T, Observable<G>> treeNormalizer) {
//        return new Observable.Transformer<List<T>, List<G>>() {
//            @Override
//            public Observable<List<G>> call(Observable<List<T>> branch) {
//                return branch.flatMap(ts -> Observable.from(ts)
//                        .map(treeNormalizer)
//                        .toList()
//                        .concatMap(observables -> Observable.combineLatest(observables, new FuncN<List<G>>() {
//                            @Override
//                            public List<G> call(Object... args) {
//                                List<G> results = new ArrayList<>(args.length);
//                                for (Object match : args) results.setDiscoverableUsers((G) match);
//                                return results;
//                            }
//                        })));
//            }
//        };
//    }

    public static <T> Function<DataSnapshot, List<T>> childListMapper(Function<DataSnapshot, T> mapper) {
        return dataSnapshot -> {
            if (dataSnapshot.exists()) {
                ArrayList<T> list = new ArrayList<T>((int) dataSnapshot.getChildrenCount());
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    try {
                        list.add(mapper.apply(childSnapshot));
                    } catch (Exception e) {
                        Timber.e(e, "PARSING EXCEPTION");
                    }
                }
                return list;
            } else {
                return Collections.emptyList();
            }
        };
    }

    public static <T> Function<DataSnapshot, List<T>> singletonListMapper(Function<DataSnapshot, T> mapper) {
        return dataSnapshot -> {
            if (dataSnapshot.exists()) {
                return Collections.singletonList(mapper.apply(dataSnapshot));
            } else {
                return Collections.emptyList();
            }
        };

    }

    public enum Event {
        INSTANCE
    }

    public static <T> FlowableTransformer<T, T> logFlowable(String name, boolean enabled) {
        if (enabled && BuildConfig.DEBUG) {
            return new FlowableTransformer<T, T>() {
                @Override
                public Publisher<T> apply(@NonNull Flowable<T> upstream) {
                    final LongHolder longHolder = new LongHolder();
                    return upstream.doOnSubscribe(subscription -> longHolder.value = System.currentTimeMillis())
                            .doOnNext(o -> Timber.d("%s: OnNext - Time: %s, thread: %s, value: %s", name,
                                    elapsedMillis(longHolder.value), ThreadUtils.getThreadName(), o))
                            .doOnError(throwable -> Timber.e("%s: OnError - Time: %s, thread: %s, Error: %s", name,
                                    elapsedMillis(longHolder.value), ThreadUtils.getThreadName(), throwable.toString()))
                            .doOnComplete(() -> Timber.d("%s: OnComplete - time: %s, thread: %s", name, elapsedMillis(longHolder.value),
                                    ThreadUtils.getThreadName()));
                }
            };
        } else {
            return upstream -> upstream;
        }
    }


    public static <T> SingleTransformer<T, T> logSingle(String name, boolean enabled) {
        if (enabled && BuildConfig.DEBUG) {
            return new SingleTransformer<T, T>() {
                @Override
                public Single<T> apply(@NonNull Single<T> upstream) {
                    final LongHolder longHolder = new LongHolder();
                    return upstream.doOnSubscribe(subscription -> longHolder.value = System.currentTimeMillis())
                            .doOnSuccess(o -> Timber.d("%s: OnSuccess - Time: %s, thread: %s, value: %s", name, elapsedMillis(longHolder
                                    .value), ThreadUtils.getThreadName(), o.toString()))
                            .doOnError(throwable -> Timber.d("%s: Error: %s, time: %s, thread: %s", name, throwable.toString(),
                                    elapsedMillis(longHolder.value), ThreadUtils.getThreadName()));
                }
            };
        } else {
            return upstream -> upstream;
        }
    }

    public static <T> ObservableTransformer<T, T> logObservable() {
        return new ObservableTransformer<T, T>() {
            @Override
            public Observable<T> apply(@NonNull Observable<T> upstream) {
                final LongHolder longHolder = new LongHolder();
                return upstream.doOnSubscribe(subscription -> longHolder.value = System.currentTimeMillis())
                        .doOnNext(o -> Timber.d("OnNext - Time: %s, thread: %s, value: %s", elapsedMillis(longHolder.value), ThreadUtils
                                .getThreadName(), o.toString()))
                        .doOnError(throwable -> Timber.d("OnError - Time: %s, thread: %s, Error: %s", elapsedMillis
                                (longHolder.value), ThreadUtils.getThreadName(), throwable.toString()))
                        .doOnComplete(() -> Timber.d("OnComplete - time: %s, thread: %s", elapsedMillis(longHolder.value),
                                ThreadUtils.getThreadName()));
            }
        };
    }

    public static <T> MaybeTransformer<T, T> logMaybe(String name, boolean enabled) {
        if (enabled && BuildConfig.DEBUG) {
            return new MaybeTransformer<T, T>() {
                @Override
                public Maybe<T> apply(@NonNull Maybe<T> upstream) {
                    final LongHolder longHolder = new LongHolder();
                    return upstream.doOnSubscribe(subscription -> longHolder.value = System.currentTimeMillis())
                            .doOnSuccess(o -> Timber.d("%s: OnSuccess - Time: %s, thread: %s, value: %s", name, elapsedMillis(longHolder
                                            .value), ThreadUtils.getThreadName(), o.toString()))
                            .doOnError(throwable -> Timber.d("%s: Error: %s, time: %s, thread: %s", name, throwable.toString(),
                                    elapsedMillis(longHolder.value), ThreadUtils.getThreadName()))
                            .doOnComplete(() -> Timber.d("%s: OnComplete - time: %s, thread: %s", name, elapsedMillis(longHolder.value),
                                    ThreadUtils.getThreadName()));
                }
            };
        } else {
            return upstream -> upstream;
        }
    }

    public static CompletableTransformer logCompletable() {
        return new CompletableTransformer() {
            @Override
            public Completable apply(@NonNull Completable upstream) {
                final LongHolder longHolder = new LongHolder();
                return upstream.doOnSubscribe(subscription -> longHolder.value = System.currentTimeMillis())
                        .doOnError(throwable -> Timber.d("OnError - Time: %s, thread: %s, Error: %s", elapsedMillis
                                (longHolder.value), ThreadUtils.getThreadName(), throwable.toString()))
                        .doOnComplete(() -> Timber.d("OnComplete - time: %s, thread: %s", elapsedMillis(longHolder.value),
                                ThreadUtils.getThreadName()));
            }
        };
    }

    private static String elapsedMillis(long timestamp) {
        return String.valueOf(System.currentTimeMillis() - timestamp);
    }

    private static class LongHolder {
        public long value;
    }
}
