package com.bimber.utils.rx

import io.reactivex.Flowable
import io.reactivex.functions.BiFunction

/**
 * Created by Maciek on 26/01/2018.
 */
enum class Entity {
    INSTANCE
}


fun <T> Flowable<T>.listen(): Flowable<Entity> {
    return this.map { _ -> Entity.INSTANCE }
            .distinctUntilChanged()
            .startWith(Entity.INSTANCE)
}


fun <Value, Key, Result> Flowable<out Collection<Value>>.aggregate(keySelector: (Value) -> Key,
                                                                   mapping: (Key) -> Flowable<Result>): Flowable<List<Result>> {


    val multicastedSelf = this.publish().refCount()
    val availableMappedValues = multicastedSelf.flatMapIterable { it.asIterable() }
            .map(keySelector)
            .distinct()
            .flatMap { key -> mapping(key).map { ValueMapping(key, it) } }
            .scan(mutableMapOf<Key, Result>(), { collectedValues, newValue ->
                collectedValues[newValue.key] = newValue.value
                collectedValues
            })

    return Flowable.combineLatest(multicastedSelf, availableMappedValues, BiFunction { input, availableOutputs ->
        val results = ArrayList<Result>(input.size)
        for (value: Value in input) {
            val result = availableOutputs[keySelector(value)]
            if (result != null) {
                results.add(result)
            }
        }
        results
    })
}

//FIXME Default values with inference https://discuss.kotlinlang.org/t/type-inference-in-lambda-default-argument/4668/11
fun <Value, Key, Result, PostResult> Flowable<out Collection<Value>>.aggregate(keySelector: (Value) -> Key,
                                                                               mapping: (Key) -> Flowable<Result>,
                                                                               postMapping: (Value, Result) -> PostResult): Flowable<List<PostResult>> {
    val multicastedSelf = this.publish().refCount()
    val availableMappedValues = multicastedSelf.flatMapIterable { it.asIterable() }
            .map(keySelector)
            .distinct()
            .flatMap { inputValue -> mapping(inputValue).map { ValueMapping(inputValue, it) } }
            .scan(mutableMapOf<Key, Result>(), { collectedValues, values ->
                collectedValues[values.key] = values.value
                collectedValues
            })

    return Flowable.combineLatest(multicastedSelf, availableMappedValues, BiFunction { input, availableOutputs ->
        val results = ArrayList<PostResult>(input.size)
        for (value: Value in input) {
            val result = availableOutputs[keySelector(value)]
            if (result != null) {
                results.add(postMapping(value, result))
            }
        }
        results
    })
}

fun <Input, Value, Key, Result, Output> Flowable<Input>.aggregateFrom(unpack: (Input) -> Collection<Value>,
                                                                      keySelector: (Value) -> Key,
                                                                      mapping: (Value) -> Flowable<Result>,
                                                                      repack: (Input, List<Result>) -> Output): Flowable<Output> {

    val multicastedSelf = this.replay(1).refCount()
    val availableMappedValues = multicastedSelf.flatMapIterable { unpack(it).asIterable() }
            .distinct(keySelector)
            .flatMap { inputValue -> mapping(inputValue).map { ValueMapping(keySelector(inputValue), it) } }
            .scan(mutableMapOf<Key, Result>(), { collectedUsers, newUser ->
                collectedUsers[newUser.key] = newUser.value
                collectedUsers
            })

    return Flowable.combineLatest(multicastedSelf, availableMappedValues, BiFunction { input, availableOutputs ->
        val values = unpack(input)
        val result = ArrayList<Result>(values.size)
        for (user: Value in values) {
            val userData = availableOutputs[keySelector(user)]
            if (userData != null) {
                result.add(userData)
            }
        }
        repack(input, result)
    })
}

private data class ValueMapping<out Key, out Result>(val key: Key, val value: Result)