package com.bimber.utils;

import android.content.Context;

import com.bimber.R;

import java.util.concurrent.TimeUnit;

/**
 * Created by maciek on 30.07.17.
 */

public class TimeUtils {

    public static final int HOURS_PER_DAY = 24;
    public static final int MINUTES_PER_HOUR = 60;

    public static String formatDuration(Context context, long then, long now) {
        long duration = Math.abs(then - now);
        long days = TimeUnit.MILLISECONDS.toDays(duration);
        long hours = TimeUnit.MILLISECONDS.toHours(duration) - days * HOURS_PER_DAY;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) - hours * MINUTES_PER_HOUR;
        if (days > 0) {
            return String.format(context.getResources().getQuantityString(R.plurals.time_format_days, (int) days), days);
        } else if (hours > 0) {
            long hoursRounded = minutes > MINUTES_PER_HOUR / 2 ? hours + 1 : hours;
            return String.format(context.getResources().getQuantityString(R.plurals.time_format_hours, (int) hoursRounded), hoursRounded);
        } else {
            return String.format(context.getResources().getQuantityString(R.plurals.time_format_minutes, (int) minutes), minutes);
        }
    }
}
