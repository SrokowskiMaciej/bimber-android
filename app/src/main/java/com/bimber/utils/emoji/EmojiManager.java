package com.bimber.utils.emoji;

import android.content.Context;

import com.bimber.R;
import com.google.code.regexp.Pattern;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Loads emojis from resource bundle
 *
 * @author Krishna Chaitanya Thota
 */
public class EmojiManager {
    private static Pattern emoticonRegexPattern;

    private static List<Emoji> emojis;

    public static void init(Context context) {
        Single.fromCallable(() -> loadEmojis(context))
                .map(emojis -> new EmojiData(emojis, processEmoticonsToRegex(emojis)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(emojiData -> {
                    emojis = emojiData.emojis;
                    emoticonRegexPattern = emojiData.pattern;
                });
    }

    /**
     * Returns the complete emoji data
     *
     * @return List of emoji objects
     */
    public static List<Emoji> data() {
        return emojis;
    }

    /**
     * Returns the Regex which can match all emoticons in a string
     *
     * @return regex pattern for emoticons
     */
    public static Pattern getEmoticonRegexPattern() {
        return emoticonRegexPattern;
    }


    private static List<Emoji> loadEmojis(Context context) throws IOException {
        Type listType = new TypeToken<ArrayList<Emoji>>() {
        }.getType();
        InputStreamReader stream = new InputStreamReader(context.getResources().openRawResource(R.raw.emoji), "UTF-8");
        List<Emoji> emojiData = new Gson().fromJson(stream, listType);
        stream.close();
        return emojiData;
    }

    /**
     * Processes the Emoji data to emoticon regex
     */
    private static Pattern processEmoticonsToRegex(List<Emoji> emojiData) {

        List<String> emoticons = new ArrayList<>();

        for (Emoji e : emojiData) {
            if (e.getEmoticons() != null) {
                emoticons.addAll(e.getEmoticons());
            }
        }

        //List of emotions should be pre-processed to handle instances of subtrings like :-) :-
        //Without this pre-processing, emoticons in a string won't be processed properly
        for (int i = 0; i < emoticons.size(); i++) {
            for (int j = i + 1; j < emoticons.size(); j++) {
                String o1 = emoticons.get(i);
                String o2 = emoticons.get(j);

                if (o2.contains(o1)) {
                    String temp = o2;
                    emoticons.remove(j);
                    emoticons.add(i, temp);
                }
            }
        }


        StringBuilder sb = new StringBuilder();
        for (String emoticon : emoticons) {
            if (sb.length() != 0) {
                sb.append("|");
            }
            sb.append("(?<=\\s|^)" + java.util.regex.Pattern.quote(emoticon) + "(?=\\s|$)");
        }
        String regex = sb.toString();
        return Pattern.compile(regex);
    }

    private static class EmojiData {
        public final List<Emoji> emojis;
        public final Pattern pattern;

        private EmojiData(List<Emoji> emojis, Pattern pattern) {
            this.emojis = emojis;
            this.pattern = pattern;
        }
    }
}
