package com.bimber.utils.images;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * Created by maciek on 23.08.17.
 */

public class StartPostponedListener implements RequestListener<Drawable> {

    private final WeakReference<Activity> activity;

    public StartPostponedListener(Activity activity) {
        this.activity = new WeakReference<>(activity);
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
        Activity activity = this.activity.get();
        Timber.d("onLoadFailed, activity: %s", activity);
        if (activity != null) {
            ActivityCompat.startPostponedEnterTransition(activity);
        }
        return false;
    }

    @Override
    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean
            isFirstResource) {
        Activity activity = this.activity.get();
        Timber.d("onResourceReady, activity: %s", activity);
        if (activity != null) {
            ActivityCompat.startPostponedEnterTransition(activity);
        }
        return false;
    }
}
