package com.bimber.utils.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.v4.util.Pair;

import com.bimber.R;
import com.bimber.base.glide.GlideApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by maciek on 08.04.17.
 */

public class BitmapUtils {

    public static Observable<Bitmap> collage(Context context, List<ThumbnailedPhoto> photos, int width, int height) {

        Single<Bitmap> defaultGroupBitmap = Single.fromFuture(GlideApp.with(context)
                .asBitmap()
                .load(R.drawable.ic_default_group)
                .apply(new RequestOptions().centerCrop())
                .submit(width, height))
                .subscribeOn(Schedulers.computation());

        Single<Bitmap> urisBitmap = Observable.fromIterable(photos)
                .map(ThumbnailedPhoto::uri)
                .toList()
                .flatMap(photosUris -> bitmapCollage(context, photosUris, width, height, DiskCacheStrategy.ALL));

        Single<Bitmap> thumbnailsBitmap = Observable.fromIterable(photos)
                .map(ThumbnailedPhoto::thumbnail)
                .toList()
                .flatMap(photosUris -> bitmapCollage(context, photosUris, width, height));
        return Observable.concatArrayDelayError(defaultGroupBitmap.toObservable()
                        .onErrorResumeNext(Observable.empty()),
                thumbnailsBitmap.toObservable().onErrorResumeNext(Observable.empty()),
                urisBitmap.toObservable().onErrorResumeNext(Observable.empty()));
    }


    public static Single<Bitmap> bitmapCollage(Context context, List<String> uris, int width, int height) {
        return bitmapCollage(context, uris, width, height, DiskCacheStrategy.AUTOMATIC);
    }

    public static Single<Bitmap> bitmapCollage(Context context, List<String> uris, int width, int height,
                                               DiskCacheStrategy diskCacheStrategy) {
        int urisCount = uris.size();
        List<Pair<String, Rect>> positionedUris = new ArrayList<>(uris.size());
        if (urisCount == 1) {
            positionedUris.add(Pair.create(uris.get(0), new Rect(0, 0, width, height)));
        } else if (urisCount == 2) {
            positionedUris.add(Pair.create(uris.get(0), new Rect(0, 0, width / 2, height)));
            positionedUris.add(Pair.create(uris.get(1), new Rect(width / 2, 0, width, height)));
        } else if (urisCount == 3) {
            positionedUris.add(Pair.create(uris.get(0), new Rect(0, 0, width / 2, height)));
            positionedUris.add(Pair.create(uris.get(1), new Rect(width / 2, 0, width, height / 2)));
            positionedUris.add(Pair.create(uris.get(2), new Rect(width / 2, height / 2, width, height)));
        }
        return Observable.fromIterable(positionedUris)
                .observeOn(Schedulers.computation())
                .flatMap(poistionedUri -> Observable.fromFuture(Glide.with(context)
                        .asBitmap()
                        .load(poistionedUri.first)
                        .apply(new RequestOptions().centerCrop().diskCacheStrategy(diskCacheStrategy))
                        .submit(poistionedUri.second.width(), poistionedUri.second.height()))
                        .map(bitmap -> Pair.create(bitmap, poistionedUri.second))
                        .doOnError(throwable -> Timber.e("Couldn't load bitmap", throwable))
                        .onErrorResumeNext(Observable.empty())
                )
                .toList()
                .observeOn(Schedulers.computation())
                .map(new Function<List<Pair<Bitmap, Rect>>, Bitmap>() {
                    @Override
                    public Bitmap apply(@NonNull List<Pair<Bitmap, Rect>> pairs) throws Exception {
                        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                        Canvas canvas = new Canvas(result);
                        for (Pair<Bitmap, Rect> pair : pairs) {
                            canvas.drawBitmap(pair.first, pair.second.left, pair.second.top, null);
                        }
                        return result;
                    }
                });
    }


    public static Single<Bitmap> bitmap(Context context, String uri, int width, int height) {
        return Single.fromFuture(Glide.with(context)
                .asBitmap()
                .load(uri)
                .submit(width, height))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Single<Bitmap> bitmap(Context context, @DrawableRes int resourceId, int width, int height) {
        return Single.fromFuture(Glide.with(context)
                .asBitmap()
                .load(resourceId)
                .submit(width, height))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @AutoValue
    public static abstract class ThumbnailedPhoto implements Parcelable {

        @Nullable
        public abstract String uri();

        @Nullable
        public abstract String thumbnail();

        public static ThumbnailedPhoto create(String uri, String thumbnail) {
            return new AutoValue_BitmapUtils_ThumbnailedPhoto(uri, thumbnail);
        }
    }
}
