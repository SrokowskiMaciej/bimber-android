package com.bimber.utils.media;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import io.reactivex.Single;
import io.reactivex.exceptions.Exceptions;


/**
 * Created by maciek on 30.05.17.
 */

public class MediaUtils {


    public static Single<Uri> writeToMediaStore(Context inContext, Bitmap inImage, String imageTitle) {
        return Single.defer(() -> {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, imageTitle, null);
            return Single.just(Uri.parse(path));
        });
    }

    public static Single<Uri> writeToLocalTempFile(Context context, Uri uri) {
        return Single.defer(() -> {
            InputStream inputStream = null;
            if (uri.getAuthority() != null) {
                try {
                    inputStream = context.getContentResolver().openInputStream(uri); // context needed
                    File photoFile = createTemporalFileFrom(context, inputStream);
                    return Single.just(Uri.fromFile(photoFile));
                } catch (IOException e) {
                    throw Exceptions.propagate(e);
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        //Well, too bad
                    }
                }
            } else {
                throw new IllegalStateException("No uri authority");
            }
        });

    }

    private static File createTemporalFileFrom(Context context, InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = new File(context.getCacheDir(), UUID.randomUUID().toString());
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                //Well, too bad
            }
        }
        return targetFile;
    }
}
