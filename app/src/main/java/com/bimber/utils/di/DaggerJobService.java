package com.bimber.utils.di;

import com.firebase.jobdispatcher.JobService;

import dagger.android.AndroidInjection;

/**
 * Created by maciek on 03.07.17.
 */

public abstract class DaggerJobService extends JobService {

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }
}
